﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.Comunes.Librerias.IO;
using IDigitales.HIS.Web.Data.Utils;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Utils;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasBase : IDisposable
    {
        private const string _claseNombre = "ConsultasBase";

        private EntityController _ec;
        bool _disposed = false;

        public EntidadesPagina ctx
        {
            get { return _ec.Entities; }
        }


        public ConsultasBase()
        {
            _ec = new EntityController();
        }

        public ConsultasBase(ConsultasBase miBase)
        {
            _ec = miBase._ec;
        }



        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public static void DisposeIfNotNull(ConsultasBase consulta)
        {
            try
            {
                if (consulta != null)
                    consulta.Dispose();
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError(_claseNombre, "DisposeIfNotNull ", ex);
            }
        }

        public static bool Disponible(ConsultasBase consulta) 
        {
            try
            {
                if (consulta != null && consulta._disposed == false)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError(_claseNombre, "Disponible", ex);
            }
            return false;
        }

        public bool SonIguales(ConsultasBase consulta)
        {
            try
            {
                if (consulta != null && consulta == this)
                    return true;
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError(_claseNombre, "SonIguales", ex);
            }
            return false;
        }


        public void AttachEntity<T>(object entity) where T : class
        {
            try
            {
                if (entity != null && ctx.Entry(entity).State == EntityState.Detached)
                {
                    var type = entity.GetType();
                    var dbSet = ctx.Set<T>();
                    dbSet.Attach((T)entity);
                }
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError(_claseNombre, "AttachEntity", ex);
            }
        }

        public void ColeccionCarga(object miEntidad, string miColeccion)
        {
            try
            {
                ctx.Configuration.ProxyCreationEnabled = false;

                var bkpDetectChanges = ctx.Configuration.AutoDetectChangesEnabled;
                ctx.Configuration.AutoDetectChangesEnabled = false;


                if (!ctx.Entry(miEntidad).Collection(miColeccion).IsLoaded)
                    ctx.Entry(miEntidad).Collection(miColeccion).Load();

                ctx.Configuration.AutoDetectChangesEnabled = bkpDetectChanges;

            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError(_claseNombre, "ColeccionCarga", ex);
            }
        }

        public void ReferenciaCarga(object miEntidad, string miColeccion)
        {
            try
            {
                ctx.Configuration.ProxyCreationEnabled = false;

                var bkpDetectChanges = ctx.Configuration.AutoDetectChangesEnabled;
                ctx.Configuration.AutoDetectChangesEnabled = false;


                if (!ctx.Entry(miEntidad).Reference(miColeccion).IsLoaded)
                    ctx.Entry(miEntidad).Reference(miColeccion).Load();


                ctx.Configuration.AutoDetectChangesEnabled = bkpDetectChanges;
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError(_claseNombre, "ReferenciaCarga", ex);
            }
        }

        public void EjecutaScriptFile(string archivo)
        {
            DbContextTransaction transaccion = null;
            try
            {
                var sql = System.IO.File.ReadAllText(archivo);

                sql = sql.Replace("DELIMITER $$", "$$");
                sql = sql.Replace("$$ DELIMITER ;", string.Empty);

                var comandos = sql.Split(new string[] {"$$" }, StringSplitOptions.None);

                transaccion = ctx.Database.BeginTransaction();

                if (transaccion != null)
                {
                    foreach (var comando in comandos)
                    {
                        if (!string.IsNullOrEmpty(comando))
                            ctx.Database.ExecuteSqlCommand(comando);
                    }
                    transaccion.Commit();
                }
            }
            catch (Exception ex)
            {
                if (transaccion != null)
                    transaccion.Rollback();
                Bitacora.EscribeLineaError(_claseNombre, "EjecutaScriptFile", ex);
            }
        }

        public void SetContextInfo(string valor)
        {
            DbContextTransaction transaccion = null;
            try
            {
                transaccion = ctx.Database.BeginTransaction();

                if (transaccion != null)
                {
                    ctx.Database.ExecuteSqlCommand("SET @ContextInfo ='" + valor + "';");
                    transaccion.Commit();
                }
            }
            catch (Exception ex)
            {
                if (transaccion != null)
                    transaccion.Rollback();
                Bitacora.EscribeLineaError(_claseNombre, "SetContextInfo", ex);
            }
        }

        public void GuardaModificacionesNoTriggers()
        {
           // DbContextTransaction transaccion = null;
            try
            {
                //transaccion = ctx.Database.BeginTransaction();

                //if (transaccion != null)
                {
                    SetContextInfo("NO TRIGERRS");
                    ctx.SaveChanges();
                    //transaccion.Commit();
                }
            }
            catch (Exception ex)
            {
                //if (transaccion != null)
                  //  transaccion.Rollback();
                Bitacora.EscribeLineaError(_claseNombre, "GuardaModificacionesNoTriggers", ex);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (_ec != null)
                {
                    _ec.Dispose();
                    _ec = null;
                }
            }

            // Free any unmanaged objects here.
            //
            _disposed = true;
        }

        #region Comun
        public Rutas ObtenerRuta(Rutas.EnumRutaTipo tipo)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.Rutas.FirstOrDefault(x => x.Tipo == tipo);
        }

        internal async Task<ResultadoOperacion> AgregarActualizarElemento<T>(T elemento) where T : class, new()
        {
            var result = new ResultadoOperacion { Resultado = OperacionResultado.Error };

            try
            {
                ctx.Configuration.ProxyCreationEnabled = false;

                //Obtener iDS
                var strIdsElemento = ObtenerKey(elemento);
                var ids = strIdsElemento.Select(elemento.GetValor).ToList();

                //Buscar
                var ele = ctx.Set<T>().Find(ids.ToArray());

                //AgregarActualizar
                if (ele != null) //ya existe
                {
                    Objetos.CopiarObjeto(elemento, ele, true);
                }
                else
                {
                    ele = new T();
                    Objetos.CopiarObjeto(elemento, ele, true);
                    ctx.Set<T>().Add(ele);
                }
                await ctx.SaveChangesAsync();
                result.Id = (int)ele.GetValor(strIdsElemento[0]);
                result.Resultado = OperacionResultado.Ok;

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                result = GetResult(ex);
            }
            return result;
        }
        internal async Task<ResultadoOperacion> EliminarElemento<T>(T elemento) where T : class, new()
        {
            var result = new ResultadoOperacion { Resultado = OperacionResultado.Error };

            try
            {
                ctx.Configuration.ProxyCreationEnabled = false;

                //Obtener iDS
                var strIdsElemento = ObtenerKey(elemento);
                var ids = strIdsElemento.Select(elemento.GetValor).ToList();

                //Buscar
                var ele = ctx.Set<T>().Find(ids.ToArray());

                //Eliminar
                if (ele != null) //ya existe
                {
                    ctx.Entry(ele).State = EntityState.Deleted; //ctx.Set<T>().Remove(ele);
                    await ctx.SaveChangesAsync();
                    result.Id = (int)ele.GetValor(strIdsElemento[0]);
                    result.Resultado = OperacionResultado.Ok;
                }

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                result = GetResult(ex);
            }
            return result;
        }
        internal ResultadoOperacion GetResult(DbEntityValidationException ex)
        {
            var result = new ResultadoOperacion { Resultado = OperacionResultado.Error };
            var sb = new StringBuilder();
            foreach (var ve in ex.EntityValidationErrors.SelectMany(eve => eve.ValidationErrors))
            {
                sb.Append(ve.ErrorMessage + ",");
            }
            result.Mensaje = sb.ToString();
            Log.WriteNewEntry(ex, EventLogEntryType.Error);
            return result;
        }
        internal ResultadoOperacion GetResult(DbUpdateException ex)
        {
            var result = new ResultadoOperacion { Resultado = OperacionResultado.Error };
            var sb = new StringBuilder();
            if (ex.InnerException != null)
                if (ex.InnerException.InnerException != null) sb.Append(ex.InnerException.InnerException.Message);
            result.Mensaje = sb.ToString();
            Log.WriteNewEntry(ex, EventLogEntryType.Error);
            return result;
        }

        internal List<string> ObtenerKey<T>(T entity) where T : class
        {
            Type entityType = entity.GetType();
            var entitySetElementType =
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)ctx).ObjectContext.CreateObjectSet<T>()
                .EntitySet.ElementType;

            var ids = entitySetElementType.KeyProperties.Select(key => key.Name).ToList();
            return ids;
        }

        #endregion
    }
}
