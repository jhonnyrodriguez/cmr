﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.Comunes.Librerias.IO;

namespace IDigitales.Pagina.Web.Data
{
    internal class EntityController
    {

        public EntidadesPagina Entities { get; private set; }

        public EntityController()
        {
            Initialize();
        }
        
        public void Initialize()
        {
            try
            {
                Entities = new EntidadesPagina();
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError("EntityController", "Initialize", ex);
            }
        }

        private string ObtenerCadenaConexion()
        {
            var connString = @"server=localhost;database=SITIO_WEB;uid=root;pwd=database;";
            try
            {
                connString = ConfigurationManager.ConnectionStrings["Entidades"].ConnectionString;
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError("EntityController", "ObtenerCadenaConexion", ex);
            }
            return connString;
        }


        public void Dispose()
        {
            try
            {
                Entities.Dispose();
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError("EntityController", "Dispose", ex);
            }
        }
    }
}
