﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;

namespace IDigitales.HIS.Web.Data.Utils
{
    public static class Objetos
    {
        /// <summary>
        /// Clona un objeto sin incluir las clases
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objSource"></param>
        /// <returns></returns>
        public static T CloneObject<T>(T objSource)
        {
           
            //Get the type of source object and create a new instance of that type
            Type typeSource = objSource.GetType();
            var objTarget = (T)Activator.CreateInstance(typeSource);

            //Get all the properties of source object type
            PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Assign all source property to taget object 's properties
            foreach (PropertyInfo property in propertyInfo.Where(property => property.CanWrite))
            {
                //check whether property type is value type, enum or string type
                if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType == typeof(String) || property.PropertyType==typeof(Byte[]))
                {
                    property.SetValue(objTarget, property.GetValue(objSource, null), null);
                }
                else
                {
                    property.SetValue(objTarget, null, null);
                }
                //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                //else
                //{
                //    //object objPropertyValue = property.GetValue(objSource, null);
                //    //if (objPropertyValue == null)
                //    //{
                //    //    property.SetValue(objTarget, null, null);
                //    //}
                //    //else
                //    //{
                //    //    property.SetValue(objTarget, objSource, null);
                //    //}
                //}
            }
            return objTarget;
        }

        /// <summary>
        /// Copia los datos de un objeto a otro se utiliza para actualizar
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objSource"></param>
        /// <param name="objTarget"></param>
        /// <param name="copiarNulos"></param>
        /// <returns></returns>
        public static T CopiarObjeto<T>(T objSource, T objTarget, bool copiarNulos=false)
        {
            //Obtener el Tipo
            Type typeSource = objSource.GetType();

            //Obtener las propiedades del tipo de Objeto
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Asignar todas las propiedades
            foreach (var property in propertyInfo.Where(property => property.CanWrite))
            {
                //Checar si es un valor tipado, enum o string
                if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType == typeof(String))
                {
                    var valor = property.GetValue(objSource, null);
                    if (valor == null && !copiarNulos)
                    {
                        //no asignar si es nulo
                        continue;
                    }
                    else if (valor is int && (int)valor <= 0 && !copiarNulos)
                    {
                        //no asignar si es entero menor o igual a 0
                        continue;
                    }
                    else if (valor is String && (string.IsNullOrEmpty((string)valor) || (string)valor == "-1") && !copiarNulos)
                    {
                        //no asignar si es string y no tiene valor 
                        continue;
                    }
                    else
                    {
                        property.SetValue(objTarget, valor, null);
                    }
                }
            }
            return objTarget;
        }

        public static T2 CopiarObjetoDif<T,T2>(this T objSource, T2 objTarget, bool copiarNulos=false)
        {
            //Obtener el Tipo
            Type typeSource = objSource.GetType();

            var typeTarjet = objTarget.GetType();

            //Obtener las propiedades del tipo de Objeto
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var propiedades = typeTarjet.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Asignar todas las propiedades
            foreach (var property in propertyInfo.Where(property => property.CanWrite))
            {
                //Checar si es un valor tipado, enum o string
                if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType == typeof(String))
                {
                    var valor = property.GetValue(objSource, null);
                    if (valor == null && !copiarNulos)
                    {
                        //no asignar si es nulo
                        continue;
                    }
                    else if (valor is int && (int)valor <= 0 && !copiarNulos)
                    {
                        //no asignar si es entero menor o igual a 0
                        continue;
                    }
                    else if (valor is String && (string.IsNullOrEmpty((string)valor) || (string)valor == "-1") && !copiarNulos)
                    {
                        //no asignar si es string y no tiene valor 
                        continue;
                    }
                    else
                    {
                       var popiedad= propiedades.FirstOrDefault(p => p.CanWrite && p.Name == property.Name);
                        if (popiedad!=null)
                        {
                            popiedad.SetValue(objTarget, valor, null);
                        }
                    }
                }
            }
            return objTarget;
        }
       
        /// <summary>
        /// Obtiene el valor de un objeto apartir del nombre de la propiedad
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elemento"></param>
        /// <param name="strId"></param>
        /// <returns></returns>
        //public static object GetValor<T>(this T elemento, string strId)
        //{
        //    try
        //    {
        //        var tEnt = elemento.GetType();
        //        var propiedades = tEnt.GetProperties();
        //        var p = propiedades.FirstOrDefault(x => x.Name == strId);
        //        if (p != null)
        //            return p.GetValue(elemento, null);

        //    }
        //    catch { }
        //    return 0;
        //}

        /// <summary>
        /// Obtiene el valor de un objeto apartir del nombre de la propiedad
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elemento"></param>
        /// <param name="strId"></param>
        /// <returns></returns>
        public static object GetValor<T>(this T elemento, string strId)
        {
            try
            {
                var prop = strId.Split('.');
                object objeto = null;
               // if (strId.Contains("."))

                var tEnt = elemento.GetType();
                var propiedades = tEnt.GetProperties();
                var p = propiedades.FirstOrDefault(x => x.Name == prop[0]);
                if (p != null)
                    objeto= p.GetValue(elemento, null);

                if (prop.Length > 1)
                {
                    var nuevoId = strId.Replace(prop[0] + ".", "");
                    if (objeto == null)
                        return objeto;
                    var subPro= GetValor(objeto, nuevoId);
                    return subPro;
                }

                if (p != null)
                {
                    var res= p.GetValue(elemento, null);
                    return res;
                }
            }
            catch { }
            return 0;
        }
       

        /// <summary>
        ///  Establece el valor de una propiedad en un objeto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elemento"></param>
        /// <param name="strId"></param>
        /// <param name="id"></param>
        public static void SetValor<T>(this T elemento, string strId, object id)
        {
            try
            {
                var tEnt = elemento.GetType();
                var propiedades = tEnt.GetProperties();
                var p = propiedades.FirstOrDefault(x => x.Name.ToLower() == strId.ToLower());
                if (p != null)
                    p.SetValue(elemento, id, null);

            }
            catch { }
        }

        public static string ObtenerIdString<T>(T elemento)
        {
            var tEnt = elemento.GetType();
            var propiedades = tEnt.GetProperties();
            var p = propiedades.FirstOrDefault(x => x.Name.ToLower().Contains("id"));
            if (p != null)
                return p.Name;
            return "";
        }

        
        public static int ObtenerIdValor<T>(T elemento)
        {
            try
            {
                var tEnt = elemento.GetType();
                var propiedades = tEnt.GetProperties();
                var p = propiedades.FirstOrDefault(x => x.Name.ToLower().Contains("id"));
                if (p != null)
                    return (int) p.GetValue(elemento, null);

            }
            catch { }
            return 0;
        }

        public static bool HasPropMethod(this object obj, string metodoPropiedad)
        {
            var type = obj.GetType();
            if( type.GetProperty(metodoPropiedad) != null)return true;
            return type.GetMethod(metodoPropiedad) != null;
        }

        public static T ToObject<T>(this IDictionary<string, object> source)
            where T : class, new()
        {
            var someObject = new T();
            var someObjectType = someObject.GetType();

            foreach (var item in source)
            {
                someObjectType.GetProperty(item.Key).SetValue(someObject, item.Value, null);
            }

            return someObject;
        }

        public static IDictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null)
            );

        }


        public static T QuitarReferenciaCircular<T>(T objSource, List<string> includes = null, int maxStringLength = 0)
        {
            if (objSource == null) return objSource;
            //Obtener el Tipo            
            Type typeSource = objSource.GetType();
            var objTarget = (T)Activator.CreateInstance(typeSource);

            if (objSource is System.Collections.ICollection)
            {
                try
                {
                    var collection = (IEnumerable<object>)objSource;
                    if (collection.Any())
                    {
                        MethodInfo method = typeSource.GetMethod("Add");
                        //var collectionDestino = (ICollection<object>)objTarget;
                        foreach (var item in collection)
                        {
                            var nitem = QuitarReferenciaCircular(item, includes, maxStringLength);
                            method.Invoke(objTarget, new object[] { nitem });
                            //collectionDestino.Add(nitem);
                        }
                    }
                }
                catch (Exception err)
                {

                }
                return objTarget;
            }


            //Obtener las propiedades del tipo de Objeto
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Asignar todas las propiedades
            foreach (var property in propertyInfo.Where(property => property.CanWrite))
            {
                //Revisar si es valor enum string byte
                if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType == typeof(String) || property.PropertyType == typeof(Byte[]))
                {
                    var propertyValue = property.GetValue(objSource, null);
                    if (maxStringLength > 0 && property.PropertyType == typeof(String))
                    {
                        string propertyStringValue = propertyValue as String;
                        if (propertyStringValue != null && propertyStringValue.Length >= maxStringLength)
                        {
                            propertyValue = propertyStringValue.Substring(0, maxStringLength) + "...";
                        }
                    }
                    property.SetValue(objTarget, propertyValue, null);
                }
                else if (property.PropertyType.IsGenericType && includes != null)
                {
                    var props = includes.Where(x => x.Contains(property.Name) && x.Split('.').Any(s => s == property.Name)).ToList();

                    if (props != null && props.Any())
                    {
                        try
                        {


                            var collection = (IEnumerable<object>)property.GetValue(objSource, null);
                            Type tipoLista = collection.GetType();
                            var listaDes = (IEnumerable<object>)Activator.CreateInstance(tipoLista);
                            MethodInfo method = tipoLista.GetMethod("Add");

                            foreach (var prop in props)
                            {
                                var lista = new List<string>();
                                if (prop.Contains("."))
                                {
                                    var propHija = prop.Remove(0, prop.IndexOf(".") + 1);
                                    if (!string.IsNullOrEmpty(propHija))
                                    {
                                        lista.Add(propHija);
                                    }
                                }
                                foreach (var item in collection)
                                {
                                    var nitem = QuitarReferenciaCircular(item, lista, maxStringLength);
                                    method.Invoke(listaDes, new object[] { nitem });
                                }
                                property.SetValue(objTarget, listaDes, null);
                            }
                            //property.SetValue(objTarget, sinref, null);




                        }
                        catch (Exception err)
                        {

                        }
                    }
                    else
                    {
                        property.SetValue(objTarget, null, null);
                    }
                }
                else if (includes != null && includes.Any() && property.PropertyType.IsClass)
                {
                    var props = includes.Where(x => x.Contains(property.Name) && x.Split('.').Any(s => s == property.Name)).ToList();
                    //si no esta la propiedad no copiar objeto

                    if (props != null && props.Any())
                    {
                        try
                        {
                            //propiedad sin incluir objetos hijos
                            var objOrg = property.GetValue(objSource, null);
                            var sinref = QuitarReferenciaCircular(objOrg, null, maxStringLength);

                            foreach (var prop in props)
                            {
                                if (prop.Contains("."))
                                {
                                    var propHija = prop.Remove(0, prop.IndexOf(".") + 1);
                                    var propiedades = propHija.Split('.');
                                    if (!string.IsNullOrEmpty(propHija))
                                    {
                                        var hija = objOrg.GetValor(propiedades[0]);
                                        var lista = new List<string> { propHija };
                                        var src = QuitarReferenciaCircular(hija, lista, maxStringLength);
                                        sinref.SetValor(propiedades[0], src);
                                    }
                                }
                            }
                            property.SetValue(objTarget, sinref, null);
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    else
                    {
                        var objOrg = property.GetValue(objSource, null);
                        var sinref = SoloPopiedadesSimples(objOrg, null);
                        property.SetValue(objTarget, sinref, null);
                    }

                }
                else
                {
                    property.SetValue(objTarget, null, null);
                }
            }
            objSource = objTarget;
            return objTarget;
        }
        public static T SoloPopiedadesSimples<T>(T objSource, List<string> includes)
        {
            if (objSource == null) return objSource;
            //Obtener el Tipo            
            Type typeSource = objSource.GetType();
            var objTarget = (T)Activator.CreateInstance(typeSource);
            //Obtener las propiedades del tipo de Objeto
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Asignar todas las propiedades
            foreach (var property in propertyInfo.Where(property => property.CanWrite))
            {
                //Revisar si es valor enum string byte
                if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType == typeof(String) || property.PropertyType == typeof(Byte[]))
                {
                    property.SetValue(objTarget, property.GetValue(objSource, null), null);
                }
                else
                {
                    property.SetValue(objTarget, null, null);
                }
            }
            return objTarget;
        }
    }
   
}
