﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Web.Hosting;
using IDigitales.Comunes.Librerias.IO;

namespace IDigitales.Pagina.Web.Data.Utils
{
    public static class Log
    {
        public static bool Inicializada { get; set; }
        public static bool WriteNewEntry(Exception ex,EventLogEntryType type )
        {
            bool resultado = false;
            try
            {
                Inicializar();
                Bitacora.EscribeLineaError("Pagina.Web", "Application", ex);
                resultado = true;
            }
            catch (Exception ex2)
            {
                Trace.Write(ex2);
            }
            return resultado;
        }
        public static void WriteNewExp(Exception ex)
        {
            try
            {
                Bitacora.EscribeLineaError("Pagina.Web", "Application", ex);
            }
            catch (Exception ex2)
            {
                Trace.Write(ex2);
            }
          
        }

        public static void Inicializar()
        {
            if (Inicializada) return;
            BitacoraConfig.BitacorasInicializa();
            Inicializada = true;
        }

        
    }
    public class BitacoraConfig
    {

        public static void BitacorasInicializa()
        {
            try
            {
                var version = BitacorasVersion();
#if DEBUG
                Bitacora.InicializaGlobales(@"C:\PaginaWeb\logs\Sitio\", "MVCApplication", SourceLevels.All, version, false);
#else
                Bitacora.InicializaGlobales(@"C:\HisWeb\logs\Sitio\", "MVCApplication", SourceLevels.Error, version, false);
#endif
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError("Program BaseController", "BitacorasInicializa", ex);
            }
        }

        public static string BitacorasVersion()
        {
            string nombre = string.Empty;
            try
            {
                nombre = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                var temp = HostingEnvironment.SiteName;
                var ruta = HostingEnvironment.ApplicationVirtualPath ?? string.Empty;
                ruta = ruta.Replace(@"\", string.Empty).Replace(@"/", string.Empty);
                if (!String.IsNullOrEmpty(ruta))
                    ruta = "[" + ruta + "]";
                temp += ruta;
                temp = temp.Replace(@"\", string.Empty).Replace(@"/", string.Empty);
                if (!String.IsNullOrEmpty(temp))
                {
                    temp = temp.Replace(" ", string.Empty);
                    temp = temp.ToUpper();
                    temp = ", SITIO=" + temp;
                }
                nombre += temp;
            }
            catch (Exception)
            {
                //La excepcion no es importante debido a que aun 
                //no existen bitacoras para guardarlas

            }
            return nombre;
        }

    }
}
