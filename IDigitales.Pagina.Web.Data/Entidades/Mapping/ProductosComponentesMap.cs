using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{ 
public class ProductosComponentesMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<ProductosComponentes>
    {
        public ProductosComponentesMap()
            : this("dbo")
        {
        }

        public ProductosComponentesMap(string schema)
        {
            ToTable("ProductosComponentes", schema);
            HasKey(x => new { x.IdProducto, x.IdComponente });

            Property(x => x.IdProducto).HasColumnName(@"IdProducto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.IdComponente).HasColumnName(@"IdComponente").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.Cantidad).HasColumnName(@"Cantidad").HasColumnType("int").IsOptional();

            HasRequired(a => a.Productos_IdComponente).WithMany(b => b.ProductosComponentes_IdComponente).HasForeignKey(c => c.IdComponente).WillCascadeOnDelete(false);
            HasRequired(a => a.Productos_IdProducto).WithMany(b => b.ProductosComponentes_IdProducto).HasForeignKey(c => c.IdProducto).WillCascadeOnDelete(false);
        }
    }

}
