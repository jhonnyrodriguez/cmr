using IDigitales.Pagina.Web.Data.Entidades.Poco;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    internal partial class ConfiguracionesGeneralesMap : EntityTypeConfiguration<ConfiguracionesGenerales>
    {
        public ConfiguracionesGeneralesMap()
            : this("dbo")
        {
        }

        public ConfiguracionesGeneralesMap(string schema)
        {
            ToTable(schema + ".ConfiguracionesGenerales");
            HasKey(x => x.IdConfiguracion);

            Property(x => x.IdConfiguracion).HasColumnName("IdConfiguracion").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName("Nombre").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.Descripcion).HasColumnName("Descripcion").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.Valor).HasColumnName("Valor").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);           

            InitializePartial();
        }
        partial void InitializePartial();
    }

}
