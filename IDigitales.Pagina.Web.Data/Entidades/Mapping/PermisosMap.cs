using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
   
    public class PermisosMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Permisos>
    {
        public PermisosMap()
            : this("dbo")
        {
        }

        public PermisosMap(string schema)
        {
            ToTable("Permisos", schema);
            HasKey(x => x.IdPermiso);

            Property(x => x.IdPermiso).HasColumnName(@"IdPermiso").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("nvarchar").IsOptional().HasMaxLength(200);
            Property(x => x.Clave).HasColumnName(@"Clave").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Seccion).HasColumnName(@"Seccion").HasColumnType("int").IsOptional();
            HasMany(t => t.Usuarios).WithMany(t => t.Permisos).Map(m =>
            {
                m.ToTable("UsuariosPermisos", "dbo");
                m.MapLeftKey("IdPermisos");
                m.MapRightKey("IdUsuario");
            });
        }
    }

}
