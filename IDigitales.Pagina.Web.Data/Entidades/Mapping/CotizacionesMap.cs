
using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class CotizacionesMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Cotizaciones>
    {
        public CotizacionesMap()
            : this("dbo")
        {
        }

        public CotizacionesMap(string schema)
        {
            ToTable("Cotizaciones", schema);
            HasKey(x => x.IdCotizacion);

            Property(x => x.IdCotizacion).HasColumnName(@"IdCotizacion").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.IdLista).HasColumnName(@"IdLista").HasColumnType("int").IsRequired();
            Property(x => x.PorcentajeDescuento).HasColumnName(@"PorcentajeDescuento").HasColumnType("int").IsOptional();
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(100);
            Property(x => x.IdUsuario).HasColumnName(@"idUsuario").HasColumnType("int").IsOptional();

            HasOptional(a => a.Usuarios).WithMany(b => b.Cotizaciones).HasForeignKey(c => c.IdUsuario).WillCascadeOnDelete(false);
            HasRequired(a => a.ListasPrecios).WithMany(b => b.Cotizaciones).HasForeignKey(c => c.IdLista).WillCascadeOnDelete(false);
        }
    }

}
// </auto-generated>
