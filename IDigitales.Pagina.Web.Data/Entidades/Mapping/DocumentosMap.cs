using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class DocumentosMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Documentos>
    {
        public DocumentosMap()
            : this("dbo")
        {
        }

        public DocumentosMap(string schema)
        {
            ToTable("Documentos", schema);
            HasKey(x => x.IdDocumento);

            Property(x => x.IdDocumento).HasColumnName(@"IdDocumento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("nvarchar").IsOptional().HasMaxLength(200);
            Property(x => x.Categoria).HasColumnName(@"Categoria").HasColumnType("int").IsOptional();
            Property(x => x.SubCategoria).HasColumnName(@"SubCategoria").HasColumnType("int").IsOptional();
            Property(x => x.Ruta).HasColumnName(@"Ruta").HasColumnType("nvarchar").IsOptional().HasMaxLength(250);
            Property(x => x.Fecha).HasColumnName(@"Fecha").HasColumnType("datetime").IsOptional();
        }
    }

}
