
using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class ContactosMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Contactos>
    {
        public ContactosMap()
            : this("dbo")
        {
        }

        public ContactosMap(string schema)
        {
            ToTable("Contactos", schema);
            HasKey(x => x.IdContacto);

            Property(x => x.IdContacto).HasColumnName(@"IdContacto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(150);
            Property(x => x.Empresa).HasColumnName(@"Empresa").HasColumnType("nvarchar").IsOptional().HasMaxLength(150);
            Property(x => x.Email).HasColumnName(@"Email").HasColumnType("nvarchar").IsOptional().HasMaxLength(200);
        }
    }

}
