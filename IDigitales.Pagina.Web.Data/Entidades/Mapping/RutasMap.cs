using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class RutasMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Rutas>
    {
        public RutasMap()
            : this("dbo")
        {
        }

        public RutasMap(string schema)
        {
            ToTable("Rutas", schema);
            HasKey(x => x.Tipo);

            Property(x => x.Tipo).HasColumnName(@"Tipo").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("nvarchar").IsRequired().HasMaxLength(20);
            Property(x => x.Directorio).HasColumnName(@"Directorio").HasColumnType("nvarchar").IsRequired().HasMaxLength(120);
        }
    }

}
