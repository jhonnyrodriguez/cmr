using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class CotizacionesProductosMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<CotizacionesProductos>
    {
        public CotizacionesProductosMap()
            : this("dbo")
        {
        }

        public CotizacionesProductosMap(string schema)
        {
            ToTable("CotizacionesProductos", schema);
            HasKey(x => new { x.IdCotizacion, x.IdProducto });

            Property(x => x.IdCotizacion).HasColumnName(@"IdCotizacion").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.IdProducto).HasColumnName(@"IdProducto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.Cantidad).HasColumnName(@"Cantidad").HasColumnType("int").IsRequired();
            Property(x => x.PorcentajeDescuento).HasColumnName(@"PorcentajeDescuento").HasColumnType("int").IsOptional();

            HasRequired(a => a.Cotizaciones).WithMany(b => b.CotizacionesProductos).HasForeignKey(c => c.IdCotizacion);
            HasRequired(a => a.Productos).WithMany(b => b.CotizacionesProductos).HasForeignKey(c => c.IdProducto);
        }
    }

}
