using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class ListasPrecioProductosMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<ListasPrecioProductos>
    {
        public ListasPrecioProductosMap()
            : this("dbo")
        {
        }

        public ListasPrecioProductosMap(string schema)
        {
            ToTable("ListasPrecioProductos", schema);
            HasKey(x => new { x.IdLista, x.IdProducto });

            Property(x => x.IdLista).HasColumnName(@"IdLista").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.IdProducto).HasColumnName(@"IdProducto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.Porcentaje).HasColumnName(@"Porcentaje").HasColumnType("float").IsOptional();
            Property(x => x.PrecioFinal).HasColumnName(@"PrecioFinal").HasColumnType("decimal").IsOptional().HasPrecision(18,4);

            HasRequired(a => a.LIstasPrecios).WithMany(b => b.ListasPrecioProductos).HasForeignKey(c => c.IdLista).WillCascadeOnDelete(false);
            HasRequired(a => a.Productos).WithMany(b => b.ListasPrecioProductos).HasForeignKey(c => c.IdProducto).WillCascadeOnDelete(false);
        }
    }

}