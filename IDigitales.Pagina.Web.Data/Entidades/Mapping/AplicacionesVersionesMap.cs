
namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class AplicacionesVersionesMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<AplicacionesVersiones>
    {
        public AplicacionesVersionesMap()
            : this("dbo")
        {
        }

        public AplicacionesVersionesMap(string schema)
        {
            ToTable("AplicacionesVersiones", schema);
            HasKey(x => x.IdVersion);

            Property(x => x.IdVersion).HasColumnName(@"IdVersion").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Version).HasColumnName(@"Version").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Sistema).HasColumnName(@"Sistema").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Tamano).HasColumnName(@"Tamano").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Cambios).HasColumnName(@"Cambios").HasColumnType("text").IsOptional().IsUnicode(false).HasMaxLength(2147483647);
            Property(x => x.Ruta).HasColumnName(@"Ruta").HasColumnType("nvarchar").IsOptional().HasMaxLength(250);
            Property(x => x.IdAplicacion).HasColumnName(@"IdAplicacion").HasColumnType("int").IsOptional();
            Property(x => x.Fecha).HasColumnName(@"Fecha").HasColumnType("datetime").IsOptional();
            Property(x => x.CorreoEnviado).HasColumnName(@"CorreoEnviado").HasColumnType("bit").IsOptional();

            HasOptional(a => a.Aplicaciones).WithMany(b => b.AplicacionesVersiones).HasForeignKey(c => c.IdAplicacion).WillCascadeOnDelete(false);
        }
    }

}

