using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class AplicacionesMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Aplicaciones>
    {
        public AplicacionesMap()
            : this("dbo")
        {
        }

        public AplicacionesMap(string schema)
        {
            ToTable("Aplicaciones", schema);
            HasKey(x => x.IdAplicacion);

            Property(x => x.IdAplicacion).HasColumnName(@"IdAplicacion").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("nvarchar").IsOptional().HasMaxLength(200);
            Property(x => x.Icono).HasColumnName(@"Icono").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            HasMany(t => t.Contactos).WithMany(t => t.Aplicaciones).Map(m =>
            {
                m.ToTable("AplicacionesContactos", "dbo");
                m.MapLeftKey("IdAplicacion");
                m.MapRightKey("IdContacto");
            });
        }
    }

}
// </auto-generated>
