using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{

    public class ReportesCategoriasMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<ReportesCategorias>
    {
        public ReportesCategoriasMap()
            : this("dbo")
        {
        }

        public ReportesCategoriasMap(string schema)
        {
            ToTable("ReportesCategorias", schema);
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName(@"Id").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(100);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("nvarchar").IsOptional().HasMaxLength(250);
            Property(x => x.Grupo).HasColumnName(@"Grupo").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.EsAplicacion).HasColumnName(@"EsAplicacion").HasColumnType("bit").IsRequired();
        }
    }

}
