using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class AplicacionesManualesMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<AplicacionesManuales>
    {
        public AplicacionesManualesMap()
            : this("dbo")
        {
        }

        public AplicacionesManualesMap(string schema)
        {
            ToTable("AplicacionesManuales", schema);
            HasKey(x => x.IdManual);

            Property(x => x.IdManual).HasColumnName(@"IdManual").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Ruta).HasColumnName(@"Ruta").HasColumnType("nvarchar").IsOptional().HasMaxLength(250);
            Property(x => x.Fecha).HasColumnName(@"Fecha").HasColumnType("datetime").IsOptional();
            Property(x => x.IdVersion).HasColumnName(@"IdVersion").HasColumnType("int").IsOptional();

            HasOptional(a => a.AplicacionesVersiones).WithMany(b => b.AplicacionesManuales).HasForeignKey(c => c.IdVersion).WillCascadeOnDelete(false);
        }
    }

}
// </auto-generated>
