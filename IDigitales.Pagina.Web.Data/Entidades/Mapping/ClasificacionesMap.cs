using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{

    public class ClasificacionesMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Clasificaciones>
    {
        public ClasificacionesMap()
            : this("dbo")
        {
        }

        public ClasificacionesMap(string schema)
        {
            ToTable("Clasificaciones", schema);
            HasKey(x => x.IdClasificacion);

            Property(x => x.IdClasificacion).HasColumnName(@"IdClasificacion").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(100);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("nvarchar").IsOptional().HasMaxLength(250);
        }
    }

}
