using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class ListasPreciosMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<ListasPrecios>
    {
        public ListasPreciosMap()
            : this("dbo")
        {
        }

        public ListasPreciosMap(string schema)
        {
            ToTable("LIstasPrecios", schema);
            HasKey(x => x.IdLista);

            Property(x => x.IdLista).HasColumnName(@"IdLista").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Codigo).HasColumnName(@"Codigo").HasColumnType("nvarchar").IsOptional().HasMaxLength(10);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("nvarchar").IsOptional().HasMaxLength(200);
            Property(x => x.IdListaReferencia).HasColumnName(@"IdListaReferencia").HasColumnType("int").IsOptional();
            Property(x => x.EsBase).HasColumnName(@"EsBase").HasColumnType("bit").IsOptional();
            Property(x => x.Porcentaje).HasColumnName(@"Porcentaje").HasColumnType("float").IsOptional();
            Property(x => x.Iva).HasColumnName(@"IVA").HasColumnType("bit").IsOptional();
        }
    }

}
