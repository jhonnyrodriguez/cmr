using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class ProductosMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Productos>
    {
        public ProductosMap()
            : this("dbo")
        {
        }

        public ProductosMap(string schema)
        {
            ToTable("Productos", schema);
            HasKey(x => x.IdProducto);

            Property(x => x.IdProducto).HasColumnName(@"IdProducto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.EsInternacional).HasColumnName(@"EsInternacional").HasColumnType("bit").IsRequired();
            Property(x => x.Modelo).HasColumnName(@"Modelo").HasColumnType("nvarchar").IsRequired().HasMaxLength(50);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsRequired().HasMaxLength(150);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("ntext").IsRequired().IsMaxLength();
            Property(x => x.Clasificacion).HasColumnName(@"Clasificacion").HasColumnType("int").IsRequired();
            Property(x => x.Tipo).HasColumnName(@"Tipo").HasColumnType("int").IsRequired();
            Property(x => x.Precio).HasColumnName(@"Precio").HasColumnType("decimal").IsRequired().HasPrecision(10,2);
            Property(x => x.Moneda).HasColumnName(@"Moneda").HasColumnType("int").IsRequired();
            Property(x => x.NumeroParte).HasColumnName(@"NumeroParte").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.DatosCrudos).HasColumnName(@"DatosCrudos").HasColumnType("float").IsOptional();
            Property(x => x.DatosUsables).HasColumnName(@"DatosUsables").HasColumnType("float").IsOptional();
            Property(x => x.Ruta).HasColumnName(@"Ruta").HasColumnType("nvarchar").IsOptional().HasMaxLength(250);
            Property(x => x.Activo).HasColumnName(@"Activo").HasColumnType("bit").IsOptional();

            HasRequired(a => a.Clasificaciones).WithMany(b => b.Productos).HasForeignKey(c => c.Clasificacion).WillCascadeOnDelete(false);
        }
    }

}
