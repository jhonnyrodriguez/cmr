using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class VistasMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Vistas>
    {
        public VistasMap()
            : this("dbo")
        {
        }

        public VistasMap(string schema)
        {
            ToTable("Vistas", schema);
            HasKey(x => x.IdVista);

            Property(x => x.IdVista).HasColumnName(@"IdVista").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.Clave).HasColumnName(@"Clave").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
        }
    }

}
