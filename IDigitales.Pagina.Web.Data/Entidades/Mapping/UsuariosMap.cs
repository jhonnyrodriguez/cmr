using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class UsuariosMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Usuarios>
    {
        public UsuariosMap()
            : this("dbo")
        {
        }   

        public UsuariosMap(string schema)
        {
            ToTable("Usuarios", schema);
            HasKey(x => x.IdUsuario);

            Property(x => x.IdUsuario).HasColumnName(@"IdUsuario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Nombre).HasColumnName(@"Nombre").HasColumnType("nvarchar").IsRequired().HasMaxLength(20);
            Property(x => x.Password).HasColumnName(@"Password").HasColumnType("nvarchar").IsRequired().HasMaxLength(20);
            Property(x => x.Tipo).HasColumnName(@"Tipo").HasColumnType("int").IsRequired();
            Property(x => x.Activo).HasColumnName(@"Activo").HasColumnType("bit").IsOptional();
            HasMany(t => t.Vistas).WithMany(t => t.Usuarios).Map(m =>
            {
                m.ToTable("UsuariosVistas", "dbo");
                m.MapLeftKey("IdUsuario");
                m.MapRightKey("IdVista");
            });
            HasMany(t => t.Aplicaciones).WithMany(t => t.Usuarios).Map(m =>
            {
                m.ToTable("UsuariosAplicaciones", "dbo");
                m.MapLeftKey("IdUsuario");
                m.MapRightKey("IdAplicacion");
            });
            HasMany(t => t.ReportesCategorias_Responsables).WithMany(t => t.Usuarios_Responsables).Map(m =>
            {
                m.ToTable("ReportesCategoriasResposables", "dbo");
                m.MapLeftKey("IdResponsable");
                m.MapRightKey("IdReporteCategoria");
            });
            HasMany(t => t.ReportesCategorias).WithMany(t => t.Usuarios).Map(m =>
            {
                m.ToTable("UsuariosReportesCategorias", "dbo");
                m.MapLeftKey("IdUsuario");
                m.MapRightKey("IdReporteCategoria");
            });
        }
    }

}
