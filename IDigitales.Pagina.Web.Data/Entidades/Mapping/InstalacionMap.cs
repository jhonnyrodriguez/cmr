using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Entidades.Mapping
{
    public class InstalacionMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Instalacion>
    {
        public InstalacionMap()
            : this("dbo")
        {
        }

        public InstalacionMap(string schema)
        {
            ToTable("Instalaciones", schema);
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName(@"Id").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Tag).HasColumnName(@"Tag").HasColumnType("nvarchar").IsOptional().HasMaxLength(4);
            Property(x => x.Descripcion).HasColumnName(@"Descripcion").HasColumnType("nvarchar").IsOptional();
            Property(x => x.FechaCreacion).HasColumnName(@"FechaCreacion").HasColumnType("datetime").IsRequired();
            Property(x => x.Activo).HasColumnName(@"Activo").HasColumnType("bit").IsRequired();
            Property(x => x.EsProyecto).HasColumnName(@"EsProyecto").HasColumnType("bit").IsRequired();

            HasMany(x => x.Modulo).WithMany(t => t.Instalacion).Map(m =>
            {
                m.ToTable("InstalacionesModulos", "dbo");
                m.MapLeftKey("IdInstalacion");
                m.MapRightKey("IdModulo");
            });
            HasMany(x => x.Usuarios).WithMany(t => t.Proyectos).Map(m =>
            {
                m.ToTable("InstalacionUsuarios", "dbo");
                m.MapLeftKey("Instalacion_Id");
                m.MapRightKey("Usuarios_IdUsuario");
            });
            HasMany(x => x.ReportesCategorias).WithMany(t => t.Instalacion).Map(m =>
            {
                m.ToTable("InstalacionesReportesCategorias", "dbo");
                m.MapLeftKey("IdInstalacion");
                m.MapRightKey("IdReporteCategoria");
            });
        }
    }

}
// </auto-generated>
