using System;
using System.Data.Entity;
using System.Runtime.InteropServices;
using IDigitales.Pagina.Web.Data.Entidades.Mapping;
using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data
{
    public partial class EntidadesPagina : DbContext
    {
        public System.Data.Entity.DbSet<Aplicaciones> Aplicaciones { get; set; }
        public System.Data.Entity.DbSet<AplicacionesManuales> AplicacionesManuales { get; set; }
        public System.Data.Entity.DbSet<AplicacionesVersiones> AplicacionesVersiones { get; set; }
        public System.Data.Entity.DbSet<Clasificaciones> Clasificaciones { get; set; }
        public System.Data.Entity.DbSet<ConfiguracionesGenerales> ConfiguracionesGenerales { get; set; }
        public System.Data.Entity.DbSet<Contactos> Contactos { get; set; }
        public System.Data.Entity.DbSet<Cotizaciones> Cotizaciones { get; set; }
        public System.Data.Entity.DbSet<CotizacionesProductos> CotizacionesProductos { get; set; }
        public System.Data.Entity.DbSet<Documentos> Documentos { get; set; }
        public System.Data.Entity.DbSet<ListasPrecioProductos> ListasPrecioProductos { get; set; }
        public System.Data.Entity.DbSet<ListasPrecios> ListasPrecios { get; set; }
        public System.Data.Entity.DbSet<Permisos> Permisos { get; set; }
        public System.Data.Entity.DbSet<Productos> Productos { get; set; }
        public System.Data.Entity.DbSet<ProductosComponentes> ProductosComponentes { get; set; }
        public System.Data.Entity.DbSet<Rutas> Rutas { get; set; }
        public System.Data.Entity.DbSet<Usuarios> Usuarios { get; set; }
        public System.Data.Entity.DbSet<Vistas> Vistas { get; set; }
        public System.Data.Entity.DbSet<Instalacion> Instalaciones { get; set; }
        public System.Data.Entity.DbSet<Reporte> Reportes { get; set; }
        public System.Data.Entity.DbSet<ReportesCategorias> ReportesCategorias { get; set; }
        public System.Data.Entity.DbSet<Comentario> Comentarios { get; set; }
        public System.Data.Entity.DbSet<ConfiguracionInstalaciones> ConfiguracionInstalaciones { get; set; }
        public System.Data.Entity.DbSet<Modulo> Modulos { get; set; }
        public System.Data.Entity.DbSet<ReporteImportado> ReportesImportados { get; set; }
        public System.Data.Entity.DbSet<ConfiguracionMail> ConfiguracionMail { get; set; }
        public System.Data.Entity.DbSet<UsuariosNotificationsConfiguration> UsuariosNotificationsConfigurations { get; set; }

        static EntidadesPagina()
        {
            //descomentar para debuggear 
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //{
            //    System.Diagnostics.Debugger.Launch();
            //}

            //Database.SetInitializer<EntidadesPagina>(new DatabaseInitializer());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EntidadesPagina, DatabaseConfiguration>());

            //using (var db = new EntidadesPagina())
            //{
            //    if (!db.Database.Exists())
            //        db.Database.Initialize(true);
            //}

        }

        public EntidadesPagina() : base("Name=EntidadesPagina")
        {
            InitializePartial();
        }

        public EntidadesPagina(string connectionString) : base(connectionString)
        {
            InitializePartial();
        }

        public EntidadesPagina(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model)
            : base(connectionString, model)
        {
            InitializePartial();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new AplicacionesMap());
            modelBuilder.Configurations.Add(new AplicacionesManualesMap());
            modelBuilder.Configurations.Add(new AplicacionesVersionesMap());
            modelBuilder.Configurations.Add(new ClasificacionesMap());
            modelBuilder.Configurations.Add(new ConfiguracionesGeneralesMap());
            modelBuilder.Configurations.Add(new ContactosMap());
            modelBuilder.Configurations.Add(new CotizacionesMap());
            modelBuilder.Configurations.Add(new CotizacionesProductosMap());
            modelBuilder.Configurations.Add(new DocumentosMap());
            modelBuilder.Configurations.Add(new InstalacionMap());
            modelBuilder.Configurations.Add(new ListasPrecioProductosMap());
            modelBuilder.Configurations.Add(new ListasPreciosMap());
            modelBuilder.Configurations.Add(new PermisosMap());
            modelBuilder.Configurations.Add(new ProductosMap());
            modelBuilder.Configurations.Add(new ProductosComponentesMap());
            modelBuilder.Configurations.Add(new ReportesCategoriasMap());
            modelBuilder.Configurations.Add(new RutasMap());
            modelBuilder.Configurations.Add(new UsuariosMap());
            modelBuilder.Configurations.Add(new VistasMap());

            OnModelCreatingPartial(modelBuilder);
        }

        public static DbModelBuilder CreateModel(DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new AplicacionesMap(schema));
            modelBuilder.Configurations.Add(new AplicacionesManualesMap(schema));
            modelBuilder.Configurations.Add(new AplicacionesVersionesMap(schema));
            modelBuilder.Configurations.Add(new ClasificacionesMap(schema));
            modelBuilder.Configurations.Add(new ConfiguracionesGeneralesMap(schema));
            modelBuilder.Configurations.Add(new ContactosMap(schema));
            modelBuilder.Configurations.Add(new CotizacionesMap(schema));
            modelBuilder.Configurations.Add(new CotizacionesProductosMap(schema));
            modelBuilder.Configurations.Add(new DocumentosMap(schema));
            modelBuilder.Configurations.Add(new InstalacionMap(schema));
            modelBuilder.Configurations.Add(new ListasPrecioProductosMap(schema));
            modelBuilder.Configurations.Add(new ListasPreciosMap(schema));
            modelBuilder.Configurations.Add(new PermisosMap(schema));
            modelBuilder.Configurations.Add(new ProductosMap(schema));
            modelBuilder.Configurations.Add(new ProductosComponentesMap(schema));
            modelBuilder.Configurations.Add(new ReportesCategoriasMap(schema));
            modelBuilder.Configurations.Add(new RutasMap(schema));
            modelBuilder.Configurations.Add(new UsuariosMap(schema));
            modelBuilder.Configurations.Add(new VistasMap(schema));


            return modelBuilder;
        }

        partial void InitializePartial();
        partial void OnModelCreatingPartial(DbModelBuilder modelBuilder);

        // Stored Procedures
    }
}
