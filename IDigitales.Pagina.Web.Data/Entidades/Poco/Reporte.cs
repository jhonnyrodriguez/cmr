﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    [Table("Reportes")]
    public class Reporte
    {
        #region ".  Propiedades"

        public int Id { get; set; }

        [Display(Name = "Descripción")]
        [Required]
        public string Descripcion { get; set; }

        [Display(Name = "Reporte")]
        public DateTime FechaReporte { get; set; }

        [NotMapped]
        public string Tag
        {
            get
            {
                try
                {

                    if (Instalacion != null)
                    {
                        if (Modulo != null)
                            return Instalacion.Tag + "-" + Modulo.Aplicaciones.TAG + "-" + Id.ToString("D3");
                        else
                        {
                            if(ReportesCategorias != null)
                                return Instalacion.Tag + "-" + ReportesCategorias.Nombre.Substring(0, 3).ToUpper() + "-" + Id.ToString("D3");
                            else 
                                return Instalacion.Tag + "-" + "***" + "-" + Id.ToString("D3");
                        }                            
                    }
                }
                catch
                {
                    return string.Empty;
                }
                return string.Empty;
            }
        }
        public EstadoEnum Estado { get; set; }
        public PrioridadEnum Prioridad { get; set; }
        public int? ModuloId { get; set; }
        public Modulo Modulo { get; set; }
        public DateTime? FechaAsignacion { get; set; }
        public DateTime? FechaSolucion { get; set; }
        public int? VersionDeCorreccionId { get; set; }
        public AplicacionesVersiones VersionDeCorreccion
        {
            get
            {
                if(Modulo != null)
                {
                    if(Modulo.Aplicaciones != null)
                    {
                        if(VersionDeCorreccionId.HasValue)
                            return Modulo.Aplicaciones.AplicacionesVersiones.FirstOrDefault(v => v.IdVersion == VersionDeCorreccionId.Value);
                    }
                }
                return null;
            }
        }

        [Display(Name = "Versión actual")]
        public int? VersionActualId { get; set; }
        public AplicacionesVersiones VersionActual
        {
            get
            {
                if (Modulo != null)
                {
                    if (Modulo.Aplicaciones != null)
                    {
                        if(VersionActualId.HasValue)
                            return Modulo.Aplicaciones.AplicacionesVersiones.FirstOrDefault(v => v.IdVersion == VersionActualId);
                    }
                }
                return null;
            }
        }

        [Display(Name = "Título")]
        [Required]
        public string Titulo { get; set; }
        //public string Serial { get; set; }

        //foreign keys
        [ForeignKey("Instalacion")]
        [Display(Name = "Instalación")]
        public int InstalacionId { get; set; }
        public virtual Instalacion Instalacion { get; set; }

        [ForeignKey("QuienReporta")]
        [Display(Name = "¿Quién reporta?")]
        public int? QuienReportaId { get; set; }
        public virtual Usuarios QuienReporta { get; set; }

        [ForeignKey("DesarrolladorAsignado")]
        public int? AsignadoId { get; set; }
        public virtual Usuarios DesarrolladorAsignado { get; set; }

        [ForeignKey("VistoBueno")]
        public int? VistoBuenoId { get; set; }
        public virtual Usuarios VistoBueno { get; set; }

        public bool EnProcesoActual { get; set; }

        [ForeignKey("ReportePredecesor")]
        public int? ReportePredecesorId { get; set; }
        public Reporte ReportePredecesor { get; set; }
        public virtual ICollection<Reporte> ReporteSucesores { get; set; }


        public int? Progreso { get; set; }

        public DateTime? FechaProgramada { get; set; }

        [ForeignKey("ReportesCategorias")]
        public int? CategoriaId { get; set; }
        public virtual ReportesCategorias ReportesCategorias { get; set; }
        [MaxLength(250)]
        public string NumeroSerie { get; set; }

        public TimeSpan? TiempoCreacionATerminado
        {
            get
            {
                if (FechaSolucion.HasValue)
                {
                    return FechaSolucion - FechaReporte;
                }
                return null;
            }
        }
        public TimeSpan? TiempoCreacionAEnProceso
        {
            get
            {
                if (FechaAsignacion.HasValue)
                    return FechaAsignacion - FechaReporte;
                return null;
            }
        }

        public Comentario CambioReciente
        {
            get
            {
                if (Comentarios != null && Comentarios.Count > 0)
                {
                    return Comentarios.Where(c => c.Tipo == Comentario.LogEnum.ReporteCambioEstado || c.Tipo == Comentario.LogEnum.ReporteCreado).
                        OrderByDescending(o => o.FechaCreacion).FirstOrDefault();                    
                }

                return null;
            }
        }

        public virtual List<Comentario> Comentarios { get; set; }

        public bool RequiereEvidencia
        {
            get
            {
                switch (Estado)
                {
                    case EstadoEnum.Cancelado:
                        return true;
                    case EstadoEnum.EnProceso:
                        return false;
                    case EstadoEnum.Pendiente:
                        return true;
                    case EstadoEnum.PorValidar:
                        return true;
                    case EstadoEnum.Reportado:
                        return false;
                    case EstadoEnum.Requerimiento:
                        return false;
                    case EstadoEnum.Terminado:
                        return false;
                    case EstadoEnum.NoResuelto:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool RequiereVersionCorreccion
        {
            get
            {
                if (ReportesCategorias != null && ReportesCategorias.EsAplicacion && Modulo != null && Estado == EstadoEnum.PorValidar)
                {
                    return true;
                }
                return false;
            }
        }

        public bool EsReporteEnProcesoExedente
        {
            get
            {
                try
                {
                    if (Estado == EstadoEnum.EnProceso)
                    {
                        var db = new Consultas.ConsultasInstalaciones();
                        var ts = db.ObtenerConfiguracion().ProcesoExedenceTimeSpan;
                        if (DateTime.Now - FechaAsignacion.Value > ts)
                        {
                            return true;
                        }
                    }
                }
                catch
                {

                }

                return false;
            }
        }

        public ResultadoOperacion EsValidoParaCrear
        {
            get
            {
                ResultadoOperacion resultado = new ResultadoOperacion { Resultado = Comun.Enums.OperacionResultado.Ok };
                if (string.IsNullOrEmpty(Titulo))
                {
                    resultado.Resultado = Comun.Enums.OperacionResultado.Error;
                    resultado.Mensaje = "Campos necesarios: 'Título'";
                }
                if (string.IsNullOrEmpty(Descripcion))
                {
                    resultado.Resultado = Comun.Enums.OperacionResultado.Error;
                    resultado.Mensaje = "Campos necesarios: 'Descripción'";
                }
                //if (string.IsNullOrEmpty(Serial))
                //{
                //    resultado.Resultado = Comun.Enums.OperacionResultado.Error;
                 //   resultado.Mensaje = "Campos necesarios: 'Serial'";
                //}

                if (!CategoriaId.HasValue)
                {
                    resultado.Resultado = Comun.Enums.OperacionResultado.Error;
                    resultado.Mensaje = "Campos necesarios: 'Categoría'";
                }                

                return resultado;
            }
        }

        #endregion

        #region ".  Enums"

        public enum EstadoEnum
        {
            Reportado, Requerimiento, Pendiente, Cancelado, EnProceso, PorValidar, Terminado, NoResuelto
        }
        public static string EstadoDisplay(EstadoEnum e)
        {
            switch (e)
            {
                case EstadoEnum.Cancelado:
                    return "Cancelado";
                case EstadoEnum.EnProceso:
                    return "En proceso";
                case EstadoEnum.Pendiente:
                    return "Pendiente";
                case EstadoEnum.PorValidar:
                    return "Por validar";
                case EstadoEnum.Reportado:
                    return "Reportado";
                case EstadoEnum.Requerimiento:
                    return "Requerimiento";
                case EstadoEnum.Terminado:
                    return "Terminado";
                case EstadoEnum.NoResuelto:
                    return "No resuelto";
                default:
                    return "";
            }
        }
        public static EstadoEnum? EstadoFromString(string input)
        {
            input = input.Trim().ToLower();
            foreach (Reporte.EstadoEnum estado in (Reporte.EstadoEnum[])Enum.GetValues(typeof(Reporte.EstadoEnum)))
            {
                if(EstadoDisplay(estado).Trim().ToLower() == input)
                {
                    return estado;
                }
            }
            return null;
        }
        public static PrioridadEnum? PrioridadFromString(string input)
        {
            input = input.Trim().ToLower();
            foreach (PrioridadEnum prioridad in (PrioridadEnum[])Enum.GetValues(typeof(PrioridadEnum)))
            {
                if (prioridad.ToString().Trim().ToLower() == input)
                {
                    return prioridad;
                }
            }
            return null;
        }
        public static bool EstadoCompletado(EstadoEnum e)
        {
            switch (e)
            {
                case EstadoEnum.Cancelado:
                    return false;
                case EstadoEnum.EnProceso:
                    return false;
                case EstadoEnum.Pendiente:
                    return false;
                case EstadoEnum.PorValidar:
                    return true;
                case EstadoEnum.Reportado:
                    return false;
                case EstadoEnum.Requerimiento:
                    return false;
                case EstadoEnum.Terminado:
                    return true;
                case EstadoEnum.NoResuelto:
                    return false;
                default:
                    return false;
            }
        }
        public static bool EstadoAsignado(EstadoEnum e)
        {
            switch (e)
            {
                case EstadoEnum.Cancelado:
                    return false;
                case EstadoEnum.EnProceso:
                    return true;
                case EstadoEnum.Pendiente:
                    return false;
                case EstadoEnum.PorValidar:
                    return true;
                case EstadoEnum.Reportado:
                    return false;
                case EstadoEnum.Requerimiento:
                    return false;
                case EstadoEnum.Terminado:
                    return true;
                case EstadoEnum.NoResuelto:
                    return true;
                default:
                    return false;
            }
        }
        public List<EstadoEnum> EstadosSiguientesPermitidos
        {
            get
            {
                List<EstadoEnum> resultado = new List<EstadoEnum>();

                switch (Estado)
                {
                    case EstadoEnum.Cancelado:
                        return resultado;
                    case EstadoEnum.EnProceso:
                        resultado.Add(EstadoEnum.PorValidar);
                        return resultado;
                    case EstadoEnum.Pendiente:
                        resultado.Add(EstadoEnum.Reportado);
                        resultado.Add(EstadoEnum.Requerimiento);
                        return resultado;
                    case EstadoEnum.PorValidar:
                        resultado.Add(EstadoEnum.NoResuelto);
                        resultado.Add(EstadoEnum.Terminado);
                        return resultado;
                    case EstadoEnum.Reportado:
                    case EstadoEnum.Requerimiento:
                        if (VistoBueno != null)
                        {
                            resultado.Add(EstadoEnum.EnProceso);                            
                        }
                        resultado.Add(EstadoEnum.Cancelado);
                        resultado.Add(EstadoEnum.Pendiente);
                        return resultado;
                    case EstadoEnum.Terminado:
                        return resultado;
                    case EstadoEnum.NoResuelto:
                        resultado.Add(EstadoEnum.EnProceso);
                        return resultado;
                    default:
                        return resultado;
                }
            }
        }


        public bool EstadoSiguienteAutorizado(List<Permisos> permisos)
        {
            switch (Estado)
            {
                case EstadoEnum.Cancelado:
                    if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Cancelado))
                            return true;
                    return false;
                case EstadoEnum.EnProceso:
                    if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_ENPROCESO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.EnProceso))
                            return true;
                    return false;
                case EstadoEnum.Pendiente:
                    if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Pendiente))
                            return true;
                    return false;
                case EstadoEnum.PorValidar:
                    if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_PORVALIDAR") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.PorValidar))
                            return true;
                    return false;
                case EstadoEnum.Reportado:
                    if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Reportado))
                            return true;
                    return false;
                case EstadoEnum.Requerimiento:
                    if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Requerimiento))
                            return true;
                    return false;
                case EstadoEnum.Terminado:
                    if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_TERMINADO_NORESUELTO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Terminado))
                            return true;
                    return false;
                case EstadoEnum.NoResuelto:
                    if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_TERMINADO_NORESUELTO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.NoResuelto))
                            return true;
                    return false;
                default:
                    return false;
            }
        }

        public bool EstadoSiguienteAutorizado(List<string> permisos)
        {
            switch (Estado)
            {
                case EstadoEnum.Cancelado:
                    if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Cancelado))
                            return true;
                    return false;
                case EstadoEnum.EnProceso:
                    if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_ENPROCESO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.EnProceso))
                            return true;
                    return false;
                case EstadoEnum.Pendiente:
                    if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Pendiente))
                            return true;
                    return false;
                case EstadoEnum.PorValidar:
                    if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_PORVALIDAR") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.PorValidar))
                            return true;
                    return false;
                case EstadoEnum.Reportado:
                    if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Reportado))
                            return true;
                    return false;
                case EstadoEnum.Requerimiento:
                    if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Requerimiento))
                            return true;
                    return false;
                case EstadoEnum.Terminado:
                    if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_TERMINADO_NORESUELTO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Terminado))
                            return true;
                    return false;
                case EstadoEnum.NoResuelto:
                    if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_TERMINADO_NORESUELTO") != null)
                        if (EstadosSiguientesPermitidos.Contains(EstadoEnum.NoResuelto))
                            return true;
                    return false;
                default:
                    return false;
            }
        }

        public List<EstadoEnum> EstadosSiguientesAutorizados(List<string> permisos)
        {
            List<EstadoEnum> resultado = new List<EstadoEnum>();

            if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
            {
                if(EstadosSiguientesPermitidos.Contains(EstadoEnum.Pendiente))
                    resultado.Add(EstadoEnum.Pendiente);
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Reportado))
                    resultado.Add(EstadoEnum.Reportado);
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Cancelado))
                    resultado.Add(EstadoEnum.Cancelado);
            }

            if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_TERMINADO_NORESUELTO") != null)
            {
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Terminado))
                    resultado.Add(EstadoEnum.Terminado);
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.NoResuelto))
                    resultado.Add(EstadoEnum.NoResuelto);
            }

            if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_PORVALIDAR") != null)
            {
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.PorValidar))
                    resultado.Add(EstadoEnum.PorValidar);
            }

            if (permisos.FirstOrDefault(p => p == "REPORTES_CAMBIAR_ENPROCESO") != null)
            {
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.EnProceso))
                    resultado.Add(EstadoEnum.EnProceso);
            }

            return resultado;
        }

        public List<EstadoEnum> EstadosSiguientesAutorizados(List<Permisos> permisos)
        {
            List<EstadoEnum> resultado = new List<EstadoEnum>();

            if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO") != null)
            {
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Pendiente))
                    resultado.Add(EstadoEnum.Pendiente);
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Reportado))
                    resultado.Add(EstadoEnum.Reportado);
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Cancelado))
                    resultado.Add(EstadoEnum.Cancelado);
            }

            if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_TERMINADO_NORESUELTO") != null)
            {
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.Terminado))
                    resultado.Add(EstadoEnum.Terminado);
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.NoResuelto))
                    resultado.Add(EstadoEnum.NoResuelto);
            }

            if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_ENPROCESO") != null)
            {
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.EnProceso))
                    resultado.Add(EstadoEnum.EnProceso);
            }

            if (permisos.FirstOrDefault(p => p.Clave == "REPORTES_CAMBIAR_PORVALIDAR") != null)
            {
                if (EstadosSiguientesPermitidos.Contains(EstadoEnum.PorValidar))
                    resultado.Add(EstadoEnum.PorValidar);
            }

            return resultado;
        }

        public enum PrioridadEnum
        {
            Urgente, Alta, Media, Baja,
        }

        public static string PrioridadColor(PrioridadEnum p)
        {
            switch (p)
            {
                case PrioridadEnum.Alta:
                    return "warning";
                case PrioridadEnum.Baja:
                    return "success";
                case PrioridadEnum.Media:
                    return "info";
                case PrioridadEnum.Urgente:
                    return "danger";
                default: return "";
            }
        }

        #endregion

        #region ".   Constructores"

        public Reporte()
        {
            Comentarios = new List<Comentario>();
        }
        public Reporte(string titulo, string numeroserie, int? versionActualId, string descripcion, PrioridadEnum prioridad, int? moduloId, int instalacionId, int categoriaReporteId, int quienReporteId, bool esRequerimiento)
        {
            Descripcion = descripcion;
            Prioridad = prioridad;
            InstalacionId = instalacionId;
            QuienReportaId = quienReporteId;
            ModuloId = moduloId;
            Titulo = titulo;
            NumeroSerie = numeroserie;
            VersionActualId = versionActualId;
            CategoriaId = categoriaReporteId;

            FechaReporte = DateTime.Now;
            Estado = esRequerimiento ? EstadoEnum.Requerimiento : EstadoEnum.Reportado;
        }

        public Reporte(Modelos.ExcelViewViewModel.ReporteExcel excel)
        {
            Descripcion = excel.Descripcion.Value;
            Prioridad = excel.Prioridad.Value;
            Estado = excel.Estado.Value;
            QuienReportaId = excel.QuienReporta.Value.UsuariosId;
            
            FechaReporte = excel.FechaReporte.Value;
            FechaAsignacion = excel.FechaAsignacion.Value;
            FechaSolucion = excel.FechaSolucion.Value;

            //Asignado = excel.Asignado.Value;
            if(excel.Asignado.Value != null)
                AsignadoId = excel.Asignado.Value.UsuariosId;
            
            //VistoBueno = excel.VistoBueno.Value;
            if (excel.VistoBueno.Value != null)
                VistoBuenoId = excel.VistoBueno.Value.UsuariosId;
            
            InstalacionId = excel.InstalacionId;
            ModuloId = excel.ModuloId;
            Titulo = "Reporte importado";
        }

        #endregion

    }
}
