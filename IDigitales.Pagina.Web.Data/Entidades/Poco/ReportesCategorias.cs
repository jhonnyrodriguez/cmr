using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{

    public class ReportesCategorias
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Grupo { get; set; }
        public bool EsAplicacion { get; set; }

        [NotMapped]
        public bool Seleccionada { get; set; }


        [InverseProperty("ReportesCategorias")]
        public virtual ICollection<Reporte> Reporte { get; set; }
        public virtual ICollection<Usuarios> Usuarios_Responsables { get; set; }
        public virtual ICollection<Usuarios> Usuarios { get; set; }
        public virtual ICollection<Instalacion> Instalacion { get; set; }

        public ReportesCategorias()
        {
            Reporte = new List<Reporte>();
            Usuarios_Responsables = new List<Usuarios>();
            Usuarios = new List<Usuarios>();
            Instalacion = new List<Instalacion>();
        }
    }

}
