﻿using System.ComponentModel;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{    
    public partial class ConfiguracionesGenerales
    {
        public int IdConfiguracion { get; set; }
        public string Nombre { get; set; } 
        public string Descripcion { get; set; }
        public string Valor { get; set; }
          

        public ConfiguracionesGenerales()
        {
            InitializePartial();

        }

        partial void InitializePartial();    
               
    }

   
}
