
namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{

    public class Cotizaciones
    {
        public int IdCotizacion { get; set; }
        public string Nombre { get; set; }
        public int IdLista { get; set; }
        public int? PorcentajeDescuento { get; set; }
        public int? IdUsuario { get; set; }

        public virtual System.Collections.Generic.ICollection<CotizacionesProductos> CotizacionesProductos { get; set; }
        
        
        public virtual ListasPrecios ListasPrecios { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        public Cotizaciones()
        {
            CotizacionesProductos = new System.Collections.Generic.List<CotizacionesProductos>();
        }
    }

}
// </auto-generated>
