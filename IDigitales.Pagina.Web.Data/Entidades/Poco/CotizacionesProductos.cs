namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class CotizacionesProductos
    {
        public int IdCotizacion { get; set; }
        public int IdProducto { get; set; }
        public int Cantidad { get; set; }
        public int? PorcentajeDescuento { get; set; }

        public virtual Cotizaciones Cotizaciones { get; set; }

        public virtual Productos Productos { get; set; }
    }

}
// </auto-generated>
