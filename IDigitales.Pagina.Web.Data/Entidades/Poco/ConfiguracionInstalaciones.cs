﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    [Table(name: "ConfiguracaionInstalaciones")]
    public class ConfiguracionInstalaciones
    {
        public int Id { get; set; }

        [Display(Name = "Tiempo exedente 'En Proceso'")]
        [NotMapped]
        public TimeSpan ProcesoExedenceTimeSpan
        {
            get
            {
                return TimeSpan.FromMilliseconds(ProcesoExedente);
            }
            set
            {
                ProcesoExedente = value.TotalMilliseconds;
            }
        }

        [Display(Name = "Tiempo de inactividad 'Pendiente'")]
        [NotMapped]
        public TimeSpan InactividadTimeSpan
        {
            get
            {
                return TimeSpan.FromMilliseconds(Inactividad);
            }
            set
            {
                Inactividad = value.TotalMilliseconds;
            }
        }

        public double Inactividad { get; set; }
        public double ProcesoExedente { get; set; }    
        public string RutaEvidencias { get; set; }
        public string RutaReportesImportados { get; set; }

        public ConfiguracionInstalaciones() { }
        public ConfiguracionInstalaciones(int inactividad, int exedente, string rutaEvidencias, string rutaReportes)
        {
            InactividadTimeSpan = TimeSpan.FromDays(inactividad);
            ProcesoExedenceTimeSpan = TimeSpan.FromDays(exedente);
            RutaEvidencias = rutaEvidencias;
            RutaReportesImportados = rutaReportes;
        }
    }
}
