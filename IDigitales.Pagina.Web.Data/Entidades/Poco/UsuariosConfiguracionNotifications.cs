﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    [Table("UsuariosNotificationsConfigurations")]
    public class UsuariosNotificationsConfiguration
    {
        public int Id { get; set; }

        [Required]
        public virtual Usuarios Usuarios { get; set; }
        [Required]
        public int UsuariosId { get; set; }

        public NotificationsEnum NotificationConfiguration { get; set; }
        
        [Flags]
        public enum NotificationsEnum
        {
            None = 0,
            All = ~None,
			Creacion = 256,
            StateChange_EnProceso = 1,
            StateChange_Pendiente = 2,
            StateChange_PorValidar = 4,
            StateChange_Terminado = 8,
            StateChange_NoResuelto = 16,
            StateChange_Cancelado = 32,
            NewComments = 64,
            VistoBueno = 128,
        }

        public static string NotificationEnumDisplay(string e)
        {
            switch (e)
            {
                case "None":
                    return "Notificaciones";
                case "Creacion":
                    return "Creación";
                case "StateChange_EnProceso":
                    return "Cambio de estado: 'En proceso'";
                case "StateChange_Pendiente":
                    return "Cambio de estado: 'Pendiente'";
                case "StateChange_PorValidar":
                    return "Cambio de estado: 'Por validar'";
                case "StateChange_Terminado":
                    return "Cambio de estado: 'Terminado'";
                case "StateChange_NoResuelto":
                    return "Cambio de estado: 'No resuelto'";
                case "StateChange_Cancelado":
                    return "Cambio de estado: 'Cancelado'";
                case "NewComments":
                    return "Nuevo comentario";
                case "VistoBueno":
                    return "Visto bueno";
                default:
                    return "";
            }
        }

        public static UsuariosNotificationsConfiguration.NotificationsEnum StringToEnum(string e)
        {
            switch (e)
            {
                case "None":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.None;
                case "Creacion":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.Creacion;
                case "StateChange_EnProceso":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_EnProceso;
                case "StateChange_Pendiente":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_Pendiente;
                case "StateChange_PorValidar":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_PorValidar;
                case "StateChange_Terminado":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_Terminado;
                case "StateChange_NoResuelto":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_NoResuelto;
                case "StateChange_Cancelado":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_Cancelado;
                case "NewComments":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.NewComments;
                case "VistoBueno":
                    return UsuariosNotificationsConfiguration.NotificationsEnum.VistoBueno;
                default:
                    return UsuariosNotificationsConfiguration.NotificationsEnum.None;
            }
        }

        public static bool RequiresNotification(NotificationsEnum e, Usuarios user)
        {
            switch (e)
            {
                case NotificationsEnum.None:
                    switch (user.Tipo)
                    {
                        default: return true;
                    }
                case NotificationsEnum.StateChange_EnProceso:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Servicio:
                        case Usuarios.EnumUsuarioTipo.Desarrollo:
                            return true;
                        default: return false;
                    }
                case NotificationsEnum.StateChange_Pendiente:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Jefe:
                        case Usuarios.EnumUsuarioTipo.Servicio:
                            return true;
                        default: return false;
                    }
                case NotificationsEnum.StateChange_PorValidar:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Jefe:
                        case Usuarios.EnumUsuarioTipo.Servicio:
                        case Usuarios.EnumUsuarioTipo.Desarrollo:
                            return true;
                        default: return false;
                    }
                case NotificationsEnum.StateChange_Terminado:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Administrador:
                        case Usuarios.EnumUsuarioTipo.Desarrollo:
                            return true;
                        default: return false;
                    }
                case NotificationsEnum.StateChange_NoResuelto:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Administrador:
                            return true;
                        default: return false;
                    }
                case NotificationsEnum.StateChange_Cancelado:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Jefe:
                        case Usuarios.EnumUsuarioTipo.Servicio:
                            return true;
                        default: return false;
                    }
                case NotificationsEnum.NewComments:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Administrador:
                        case Usuarios.EnumUsuarioTipo.Jefe:
                        case Usuarios.EnumUsuarioTipo.Servicio:
                            return true;                            
                        default: return false;
                    }
                case NotificationsEnum.VistoBueno:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Administrador:
                            return true;
                        default: return false;
                    }
                default:
                    return false;
            }
        }
        public static bool RequiresNotification(string estring, Usuarios user)
        {
            var e = StringToEnum(estring);
            switch (e)
            {
                case NotificationsEnum.None:
                    return false;
                case NotificationsEnum.Creacion:
                    return true;
                case NotificationsEnum.StateChange_EnProceso:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Servicio:
                        case Usuarios.EnumUsuarioTipo.Desarrollo:
                            return true;
                        default: return true;
                    }
                case NotificationsEnum.StateChange_Pendiente:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Jefe:
                        case Usuarios.EnumUsuarioTipo.Servicio:
                            return true;
                        default: return true;
                    }
                case NotificationsEnum.StateChange_PorValidar:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Jefe:
                        case Usuarios.EnumUsuarioTipo.Servicio:
                            return true;
                        default: return true;
                    }
                case NotificationsEnum.StateChange_Terminado:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Administrador:
                            return true;
                        default: return true;
                    }
                case NotificationsEnum.StateChange_NoResuelto:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Administrador:
                            return true;
                        default: return true;
                    }
                case NotificationsEnum.StateChange_Cancelado:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Jefe:
                        case Usuarios.EnumUsuarioTipo.Servicio:
                            return true;
                        default: return true;
                    }
                case NotificationsEnum.NewComments:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Administrador:
                        case Usuarios.EnumUsuarioTipo.Jefe:
                        case Usuarios.EnumUsuarioTipo.Servicio:
                        case Usuarios.EnumUsuarioTipo.Desarrollo:
                            return true;
                        default: return true;
                    }
                case NotificationsEnum.VistoBueno:
                    switch (user.Tipo)
                    {
                        case Usuarios.EnumUsuarioTipo.Administrador:
                            return true;
                        default: return true;
                    }
                default:
                    return false;
            }
        }

        public UsuariosNotificationsConfiguration()
        {
            NotificationConfiguration = NotificationsEnum.All;
        }
    }
}
