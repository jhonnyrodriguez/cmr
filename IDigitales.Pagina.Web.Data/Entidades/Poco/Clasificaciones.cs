
namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{

    public class Clasificaciones
    {
        public int IdClasificacion { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public virtual System.Collections.Generic.ICollection<Productos> Productos { get; set; }

        public Clasificaciones()
        {
            Productos = new System.Collections.Generic.List<Productos>();
        }
    }

}
// </auto-generated>
