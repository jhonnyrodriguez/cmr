﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    [Table("Modulos")]
    public class Modulo
    {
        public int Id { get; set; }
        
        [ForeignKey("Aplicaciones")]
        public int AplicacionesId { get; set; }
        public Aplicaciones Aplicaciones { get; set; }

        public string Subcategoria { get; set; }
        public string Aliases { get; set; }

        // Reverse navigation
        public virtual List<Instalacion> Instalacion { get; set; }

        public Modulo() {
            Instalacion = new List<Instalacion>();
        }
        public Modulo(int aplicacionesId, string subcategoria)
        {
            AplicacionesId = aplicacionesId;
            Subcategoria = subcategoria;
        }
    }

    //[Table("Categorias")]
    //public class Categoria
    //{
    //    public int Id { get; set; }
    //    public string TAG { get; set; }
    //    public string Nombre { get; set; }
    //    public string Version { get; set; }
    //    public string NextVersion
    //    {
    //        get
    //        {
    //            string result = string.Empty;
    //            if (Version == null)
    //                Version = "0.0.0.0";

    //            var parts = Version.Split('.');
    //            if(int.TryParse(parts.Last(), out int last))
    //            {
    //                for(int i = 0; i < parts.Length - 1; i++)
    //                {
    //                    result += parts[i] + ".";
    //                }
    //                result += (last + 1).ToString();
    //            }
    //            return result;
    //        }
    //    }

    //    public virtual List<Modulo> Modulos { get; set; }

    //    public void SetNextVersion()
    //    {
    //        Version = NextVersion;
    //    }
    //}
}
