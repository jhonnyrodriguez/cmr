﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    [Table("Comentarios")]
    public class Comentario
    {
        public int Id { get; set; }
        public string Contenido { get; set; }
        public string ArchivoRutaRelativa { get; set; }
        public string Ruta
        {
            get
            {
                return "";
            }
        }
        public DateTime FechaCreacion { get; set; }
        public LogEnum Tipo { get; set; }
        public Reporte.EstadoEnum? NuevoEstado { get; set; }
        public string VersionDeCorreccion { get; set; }
        public string Log
        {
            get
            {
                switch (Tipo)
                {
                    case LogEnum.ReporteCreado:
                        return "Reporte creado por " + Usuario.Nombre;
                    case LogEnum.Asignado:
                        return "Reporte asignado a " + Reporte.DesarrolladorAsignado.Nombre;
                    case LogEnum.ReporteCambioEstado:
                        return Usuario.Nombre + " cambió el estado del reporte a " + Reporte.EstadoDisplay(NuevoEstado.Value);
                    case LogEnum.VersionDeCorreccion:
                        return "";                    
                    case LogEnum.ReporteImportado:
                        return "Reporte importado por " + Usuario.Nombre;
                    case LogEnum.Ninguno:
                    default:
                        return "";
                }
            }
        }

        [ForeignKey("Evidencia")]
        public int? EvidenciaId { get; set; }
        public Comentario Evidencia { get; set; }


        [ForeignKey("Usuario")]
        public int? UsuarioId { get; set; }
        public Usuarios Usuario { get; set; }


        [ForeignKey("Reporte")]
        public int ReporteId { get; set; }
        public Reporte Reporte { get; set; }

        public Comentario() { }
        public Comentario(int reporteId, string mensaje, int? usuarioId)
        {
            ReporteId = reporteId;
            Contenido = mensaje;
            FechaCreacion = DateTime.Now;
            UsuarioId = usuarioId;
            Tipo = LogEnum.Ninguno;
            NuevoEstado = null;
        }

        public Comentario(int reporteId, LogEnum log, int usuarioId)
        {
            Tipo = log;
            ReporteId = reporteId;
            UsuarioId = usuarioId;
            FechaCreacion = DateTime.Now;
        }

        public Comentario(int reporteId, Reporte.EstadoEnum nuevoEstado, int usuarioId)
        {
            Tipo = LogEnum.ReporteCambioEstado;
            ReporteId = reporteId;
            UsuarioId = usuarioId;
            FechaCreacion = DateTime.Now;
            NuevoEstado = nuevoEstado;
        }

        public Comentario(int reporteId)
        {
            Tipo = LogEnum.ReporteCambioEstado;
            ReporteId = reporteId;
            UsuarioId = null;
            FechaCreacion = DateTime.Now;
            NuevoEstado = Reporte.EstadoEnum.Pendiente;
        }

        public enum LogEnum
        {
            Ninguno, ReporteCreado, ReporteCambioEstado, Asignado, VersionDeCorreccion, ReporteImportado,
        }

        public enum TipoEvidenciaEnum
        {
            Ninguno, EnProceso, Cancelado, NoResuelto,
        }

        public static TipoEvidenciaEnum TipoEvidenciaPorEstado(Reporte.EstadoEnum e)
        {
            switch (e)
            {
                case Reporte.EstadoEnum.EnProceso:
                    return TipoEvidenciaEnum.EnProceso;
                case Reporte.EstadoEnum.Cancelado:
                    return TipoEvidenciaEnum.Cancelado;
                case Reporte.EstadoEnum.NoResuelto:
                    return TipoEvidenciaEnum.NoResuelto;
                default: return TipoEvidenciaEnum.Ninguno;
            }
        }
    }
}
