using System.ComponentModel;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class Permisos
    {

        public enum EnumPermisosSeccion
        {
            [Description("Documento")]
            Documento = 1,
            [Description("Listas de Precios")]
            ListasPrecios = 2,
            [Description("Existencias")]
            Existencias = 3,
            [Description("Proyectos Instalaciones")]
            ProyectosInstalaciones = 4,
            [Description("Proyectos Reportes")]
            ProyectosReportes = 5,
            [Description("Configurar")]
            Configurar = 6,
            [Description("Productos")]
            Productos = 7,
            [Description("Aplicaciones")]
            Aplicaciones = 8
        }

        public int IdPermiso { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Clave { get; set; }
        public EnumPermisosSeccion? Seccion { get; set; }

        public virtual System.Collections.Generic.ICollection<Usuarios> Usuarios { get; set; }

        public Permisos()
        {
            Usuarios = new System.Collections.Generic.List<Usuarios>();
        }
    }

}
