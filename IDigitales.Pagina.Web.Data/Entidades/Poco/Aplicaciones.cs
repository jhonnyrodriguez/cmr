
using System.Linq;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class Aplicaciones
    {
        public int IdAplicacion { get; set; }
        public string Nombre { get; set; }
        public string TAG { get; set; }
        public string Descripcion { get; set; }
        public string Icono { get; set; }

        // Reverse navigation
        public virtual System.Collections.Generic.ICollection<AplicacionesVersiones> AplicacionesVersiones { get; set; }
        public virtual System.Collections.Generic.ICollection<Contactos> Contactos { get; set; }
        public virtual System.Collections.Generic.ICollection<Modulo> Modulos { get; set; }
        public virtual System.Collections.Generic.ICollection<Usuarios> Usuarios { get; set; }


        public AplicacionesVersiones VersionDesarrollo
        {
            get
            {
                if(AplicacionesVersiones.Count > 0)
                {
                    return AplicacionesVersiones.Last();
                }

                return null;
            }
        }
        public AplicacionesVersiones VersionEstable
        {
            get
            {
                if (AplicacionesVersiones.Count > 1)
                {
                    return AplicacionesVersiones.Take(2).First();
                }

                return null;
            }
        }

        public Aplicaciones()
        {
            AplicacionesVersiones = new System.Collections.Generic.List<AplicacionesVersiones>();
            Usuarios = new System.Collections.Generic.List<Usuarios>();
        }
    }

}

