﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    [Table("Instalaciones")]
    public class Instalacion
    {
        public int Id { get; set; }

        [DataType(dataType: DataType.Text)]
        [StringLength(maximumLength: 4, ErrorMessage = "El tag debe consistir de entre 3 y 4 caracteres.", MinimumLength = 3)]
        public string Tag { get; set; }

        [Display(Name = "Descripción")]
        [StringLength(maximumLength: 60, ErrorMessage = "La descripción debe tener un máximo de 14 caracteres.")]
        [DataType(DataType.Text)]
        public string Descripcion { get; set; }

        [Display(Name = "Creación")]
        [DataType(DataType.DateTime)]
        public DateTime FechaCreacion { get; set; }

        public bool Activo { get; set; }
        public bool EsProyecto { get; set; }

        public virtual List<Reporte> Reportes { get; set; }

        public virtual List<Usuarios> Usuarios { get; set; }
        public virtual List<Modulo> Modulo { get; set; }
        public virtual List<ReportesCategorias> ReportesCategorias { get; set; }

        public Instalacion()
        {
            Reportes = new List<Reporte>();
            Usuarios = new List<Usuarios>();
            Modulo = new List<Modulo>();
            ReportesCategorias = new List<ReportesCategorias>();
            Activo = true;
        }
        public Instalacion(string tag, string descripcion, DateTime fecha) : this()
        {
            Tag = tag;
            FechaCreacion = fecha;
            Descripcion = descripcion;
        }
    }
}
