
namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{

    public class ListasPrecios
    {
        public int IdLista { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int? IdListaReferencia { get; set; }
        public bool? EsBase { get; set; }
        public double? Porcentaje { get; set; }
        public bool? Iva { get; set; }
        public virtual System.Collections.Generic.ICollection<Cotizaciones> Cotizaciones { get; set; }
        public virtual System.Collections.Generic.ICollection<ListasPrecioProductos> ListasPrecioProductos { get; set; }

        public ListasPrecios()
        {
            ListasPrecioProductos = new System.Collections.Generic.List<ListasPrecioProductos>();
        }
    }

}