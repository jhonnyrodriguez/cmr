
namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class Rutas
    {
        public enum EnumRutaTipo
        {
            Documentos = 1,
            Bitacoras = 2,
            Aplicaciones = 3,
            Temporales=4,
        }
        public EnumRutaTipo Tipo { get; set; }
        public string Descripcion { get; set; }
        public string Directorio { get; set; }
    }

}

