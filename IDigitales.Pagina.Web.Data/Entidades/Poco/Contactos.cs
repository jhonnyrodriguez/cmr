namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class Contactos
    {
        public int IdContacto { get; set; }
        public string Nombre { get; set; }
        public string Empresa { get; set; }
        public string Email { get; set; }

        public virtual System.Collections.Generic.ICollection<Aplicaciones> Aplicaciones { get; set; }

        public Contactos()
        {
            Aplicaciones = new System.Collections.Generic.List<Aplicaciones>();
        }
    }

}
