namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{

    public class ListasPrecioProductos
    {
        public int IdLista { get; set; }
        public int IdProducto { get; set; }
        public double? Porcentaje { get; set; }
        public decimal? PrecioFinal { get; set; }


        public virtual ListasPrecios LIstasPrecios { get; set; }

        public virtual Productos Productos { get; set; }
    }

}

