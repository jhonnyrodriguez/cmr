
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class Usuarios
    {
        public enum EnumUsuarioTipo
        {
            Administrador = 1,
            Jefe = 2,
            Vendedor = 3,
            Servicio = 4,
            Desarrollo = 5,
            Cliente = 6,
            Aplicaciones = 7

        }
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
        public EnumUsuarioTipo Tipo { get; set; }
        public bool? Activo { get; set; }

       

        public virtual UsuariosNotificationsConfiguration UsuariosNotificationsConfiguration { get; set; }
        public int UsuariosNotificationsConfigurationId { get; set; }
        
        // Reverse navigation
        public virtual System.Collections.Generic.ICollection<Cotizaciones> Cotizaciones { get; set; }
        public virtual System.Collections.Generic.ICollection<Permisos> Permisos { get; set; }
        public virtual System.Collections.Generic.ICollection<Vistas> Vistas { get; set; }
        public virtual System.Collections.Generic.ICollection<Aplicaciones> Aplicaciones { get; set; }
        public virtual System.Collections.Generic.ICollection<ReportesCategorias> ReportesCategorias_Responsables { get; set; }
        public virtual System.Collections.Generic.ICollection<ReportesCategorias> ReportesCategorias { get; set; }

        [InverseProperty("DesarrolladorAsignado")]
        public virtual System.Collections.Generic.ICollection<Reporte> ReportesAsignados { get; set; }

        public virtual ICollection<Instalacion> Proyectos { get; set; }

        public Usuarios()
        {
            Proyectos = new HashSet<Instalacion>();
            Permisos = new System.Collections.Generic.List<Permisos>();
            Vistas = new System.Collections.Generic.List<Vistas>();
            Aplicaciones = new List<Aplicaciones>();
            ReportesCategorias_Responsables = new List<ReportesCategorias>();
            ReportesCategorias = new List<ReportesCategorias>();
        }

        public bool TienePermiso(string clave)
        {
            if(Permisos.FirstOrDefault(u => u.Clave == clave) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TienePermiso(int proyectoId)
        {
            if (Proyectos.FirstOrDefault(p => p.Id == proyectoId) != null)
                return true;
            return false;
        }
    }

}
