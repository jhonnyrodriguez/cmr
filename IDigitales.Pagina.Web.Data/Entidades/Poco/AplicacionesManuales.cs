
namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class AplicacionesManuales
    {
        public int IdManual { get; set; }
        public string Nombre { get; set; }
        public string Ruta { get; set; }
        public System.DateTime? Fecha { get; set; }
        public int? IdVersion { get; set; }

        public virtual AplicacionesVersiones AplicacionesVersiones { get; set; }
    }

}

