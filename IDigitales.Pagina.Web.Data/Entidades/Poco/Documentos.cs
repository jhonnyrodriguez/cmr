using System.ComponentModel;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public enum EnumDocumentosCategoria
    {
        [Description("Imagenes")]
        Imagenes = 1,
        [Description("Presentaciones")]
        Presentaciones = 2,
        [Description("Catálogos")]
        Catalogos = 3,
    }
    public enum EnumDocumentosSunCategoria
    {
        [Description("HIS")]
        His = 1,
        [Description("PACS")]
        Pacs = 2,
        [Description("Digitalización")]
        Digitalizacion = 3,
        [Description("Mesas Radiográficas")]
        MesasRadiográficas = 4,
        [Description("Mesas Fluroscopicas")]
        MesasFluroscopicas =5
    }

    public class Documentos
    {
        public int IdDocumento { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public EnumDocumentosCategoria? Categoria { get; set; }
        public EnumDocumentosSunCategoria? SubCategoria { get; set; }
        public string Ruta { get; set; }
        public System.DateTime? Fecha { get; set; }
    }

}
// </auto-generated>
