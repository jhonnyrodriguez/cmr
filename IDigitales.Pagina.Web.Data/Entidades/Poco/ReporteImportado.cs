﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    [Table("ReportesImportados")]
    public class ReporteImportado
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaImportacion { get; set; }
        public string RutaOriginal { get; set; }
        public string RutaResultadoSobrantes
        {
            get
            {
                if(Estado == ImportadoEstadoEnum.Terminado)
                {
                    var parts = RutaOriginal.Split('.');
                    return parts[0] + "_sobrante." + parts[1];
                }
                else
                {
                    return null;
                }
            }
        }
        public int LineasSobrantesCount { get; set; }
        public string ReportesImportadosDict { get; set; }
        public int ReportesImportadosCount { get; set; }
        public ImportadoEstadoEnum Estado { get; set; }

        public int InstalacionId { get; set; }
        public Instalacion Instalacion { get; set; }


        [ForeignKey("ReporteImportadoFuente")]
        public int? ReporteImportadoFuenteId { get; set; }
        public ReporteImportado ReporteImportadoFuente { get; set; }
        public virtual ICollection<ReporteImportado> Children { get; set; }


        public enum ImportadoEstadoEnum
        {
            Iniciado, Terminado, Error,
        }

        public void SetReportesImportadosDict(IEnumerable<Modelos.ExcelViewViewModel.ReporteExcel> reportes)
        {
            List<Dictionary<string, int>> list = new List<Dictionary<string, int>>();
            foreach(var reporte in reportes)
            {
                Dictionary<string, int> dict = new Dictionary<string, int>();
                dict.Add("SheetId", reporte.SheetId);
                dict.Add("Row", reporte.Row);

                list.Add(dict);
            }
            ReportesImportadosDict = JsonConvert.SerializeObject(list);
        }

        public List<Modelos.ExcelViewViewModel.ReporteExcel> GetReportesImportadosDict()
        {
            List<Modelos.ExcelViewViewModel.ReporteExcel> result = null;
            try
            {
                var list = JsonConvert.DeserializeObject<List<Dictionary<string, int>>>(ReportesImportadosDict);
                result = new List<Modelos.ExcelViewViewModel.ReporteExcel>();
                foreach(var item in list)
                {
                    result.Add(new Modelos.ExcelViewViewModel.ReporteExcel { SheetId = item["SheetId"], Row = item["Row"], });
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }
    }
}
