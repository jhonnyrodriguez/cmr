﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    [Table("ConfiguracionMail")]
    public class ConfiguracionMail
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public int Puerto { get; set; }
        public string Sender { get; set; }
        public string Servidor { get; set; }

        public bool HabilitaSSL { get; set; }
        public EnumModoSSL ModoSSL { get; set; }
        public EnumVersionSSL VersionSSL { get; set; }
    }

    public enum EnumModoSSL
    {
        [Description("Modo Explícito")]
        Explicito = 1,
        [Description("Modo Implícito")]
        Implicito = 2,
        [Description("Modo Manual")]
        Manual = 3
    }

    public enum EnumVersionSSL
    {
        [Description("Todos")]
        Todos = 1,
        [Description("Ninguno")]
        Ninguno = 2,
        [Description("SSL30")]
        SSL30 = 3,
        [Description("TLS10")]
        TLS10 = 4,
        [Description("TLS11")]
        TLS11 = 5,
        [Description("TLS12")]
        TLS12 = 6
    }
}
