
using System.Linq;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public partial class AplicacionesVersiones
    {


       
        public int IdVersion { get; set; }
        public string Version { get; set; }
        public string NextVersion
        {
            get
            {
                string result = string.Empty;
                if (Version == null)
                    Version = "0.0.0.0";

                var parts = Version.Split('.');
                if (int.TryParse(parts.Last(), out int last))
                {
                    for (int i = 0; i < parts.Length - 1; i++)
                    {
                        result += parts[i] + ".";
                    }
                    result += (last + 1).ToString();
                }
                return result;
            }
        }
        public string Sistema { get; set; }
        public string Tamano { get; set; }
        public string Cambios { get; set; }
        public string Ruta { get; set; }
        public int? IdAplicacion { get; set; }
        public System.DateTime? Fecha { get; set; }
        public bool? CorreoEnviado { get; set; }

        public virtual System.Collections.Generic.ICollection<AplicacionesManuales> AplicacionesManuales { get; set; }


        public virtual Aplicaciones Aplicaciones { get; set; }

        public AplicacionesVersiones()
        {
            AplicacionesManuales = new System.Collections.Generic.List<AplicacionesManuales>();
        }
    }

}

