namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class Vistas
    {
        public int IdVista { get; set; }
        public string Nombre { get; set; }
        public string Clave { get; set; }

        public virtual System.Collections.Generic.ICollection<Usuarios> Usuarios { get; set; }

        public Vistas()
        {
            Usuarios = new System.Collections.Generic.List<Usuarios>();
        }
    }

}
