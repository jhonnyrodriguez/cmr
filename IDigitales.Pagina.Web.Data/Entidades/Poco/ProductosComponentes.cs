namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public class ProductosComponentes
    {
        public int IdProducto { get; set; }
        public int IdComponente { get; set; }
        public int? Cantidad { get; set; }


        public virtual Productos Productos_IdComponente { get; set; }

        public virtual Productos Productos_IdProducto { get; set; }
    }

}
// </auto-generated>
