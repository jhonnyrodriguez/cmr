namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{

    public class Productos
    {
        public enum EnumProductoTipos
        {
            Estructura = 1,
            Producto = 2,
            Licencia = 3
        }

        public enum EnumMonedas
        {
            MN = 1,
            USD = 2
        }
        public int IdProducto { get; set; }
        public bool EsInternacional { get; set; }
        public string Modelo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Clasificacion { get; set; }
        public EnumProductoTipos Tipo { get; set; }
        public decimal Precio { get; set; }
        public EnumMonedas Moneda { get; set; }
        public string NumeroParte { get; set; }
        public double? DatosCrudos { get; set; }
        public double? DatosUsables { get; set; }
        public string Ruta { get; set; }
        public bool? Activo { get; set; }
        public virtual System.Collections.Generic.ICollection<CotizacionesProductos> CotizacionesProductos { get; set; }
        public virtual System.Collections.Generic.ICollection<ListasPrecioProductos> ListasPrecioProductos { get; set; }
        public virtual System.Collections.Generic.ICollection<ProductosComponentes> ProductosComponentes_IdComponente { get; set; }
        public virtual System.Collections.Generic.ICollection<ProductosComponentes> ProductosComponentes_IdProducto { get; set; }
        public virtual Clasificaciones Clasificaciones { get; set; }
        

        public Productos()
        {
            CotizacionesProductos = new System.Collections.Generic.List<CotizacionesProductos>();
            ListasPrecioProductos = new System.Collections.Generic.List<ListasPrecioProductos>();
            ProductosComponentes_IdComponente = new System.Collections.Generic.List<ProductosComponentes>();
            ProductosComponentes_IdProducto = new System.Collections.Generic.List<ProductosComponentes>();
        }
    }

}
// </auto-generated>
