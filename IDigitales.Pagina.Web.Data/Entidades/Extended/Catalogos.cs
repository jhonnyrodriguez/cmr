﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.HIS.Web.Data.Utils;
using IDigitales.Pagina.Web.Comun.Enums;

namespace IDigitales.Pagina.Web.Data.Entidades
{
    public class CatalogoBusqueda
    {
        public string id { get; set; }
        public string valor { get; set; }
        public string codigo { get; set; }
        public object elemento { get; set; }

    }
    public class Catalogos
    {
        public string Id { get; set; }
        public string Valor { get; set; }
        public string Codigo { get; set; }
        public string Tipo { get; set; }
        public object Elemento { get; set; }
    }
    public class ResultadoOperacion
    {
        public OperacionResultado Resultado { get; set; }
        public string Mensaje { get; set; }
        public int Id { get; set; }
        public int Id2 { get; set; }
        public int Estado { get; set; }
        public Object Elemento { get; set; }
        public int Nuevo { get; set; }
        public int Anterior { get; set; }
        public bool SinPermiso { get; set; }

        public ResultadoOperacion Otro { get; set; }
        public ResultadoOperacion Otro2 { get; set; }
    }
    public class GridResult<T>
    {

        public List<T> data { get; set; }

        public int totalCount { get; set; }

        public int id { get; set; }

        public string error { get; set; }

        public int maxStringLegnth { get; set; }

        public GridResult(List<T> datos, int total)
        {
            totalCount = total;
            data = datos;
            maxStringLegnth = 0;
        }
        public GridResult(List<T> datos)
        {
            totalCount = datos.Count();
            data = datos;
            maxStringLegnth = 0;
        }
        public GridResult()
        {
            data = new List<T>();
            maxStringLegnth = 0;

        }

        public GridResult(Exception exc)
        {
            data = new List<T>();
            error = exc.Message;
        }

        public void QuitarReferenciasCirculares(List<string> incluir)
        {
            var lista = new List<T>();
            foreach (var el in data)
            {
                lista.Add(Objetos.QuitarReferenciaCircular<T>(el, incluir, maxStringLegnth));
            }
            data = lista;
        }

    }
}
