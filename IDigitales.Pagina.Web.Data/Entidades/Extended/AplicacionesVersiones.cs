﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Entidades.Poco
{
    public partial class AplicacionesVersiones
    {
       [NotMapped]
        public List<AplicacionesVersionesArchivo> Archivos { get; set; }
    }

    public  class AplicacionesVersionesArchivo
    {
        public string Nombre { get; set; }
        public string Tamano { get; set; }
        public string Tipo { get; set; }
        public string Url { get; set; }
    }
}
