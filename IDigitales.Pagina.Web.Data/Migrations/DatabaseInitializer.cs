using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data
{

    public class DatabaseInitializer : CreateDatabaseIfNotExists<EntidadesPagina>
    {
        protected override void Seed(EntidadesPagina context)
        {
            SeedUsuarios(context);
            SeedCategorias(context);
            SeedProductos(context);
            SeedEstrucutras(context);
            SeedListasPrecio(context);
            SeedRutas(context);
            SeedVistas(context);
            SeedPermisos(context);
            SeedUsuariosVistasPermisos(context);
            SeedAplicaciones(context);
            SeedContactos(context);
            SeedContactosAplicaciones(context);
            SeedInstalaciones(context);
        }

        private void SeedInstalaciones(EntidadesPagina context)
        {
            context.ConfiguracionMail.AddOrUpdate(
                new ConfiguracionMail { Servidor = "smtp.cmr3.com.mx", Sender = "contacto@cmr3.com.mx", Puerto = 587, Password = "ct2016Mx@", HabilitaSSL = false, ModoSSL = EnumModoSSL.Explicito, VersionSSL = EnumVersionSSL.Ninguno}
                );
            context.Instalaciones.AddOrUpdate(
                new Instalacion("DEV", "Desarrollo", DateTime.Now)
                );
            context.SaveChanges();
            
        }

        private void SeedUsuarios(EntidadesPagina context)
        {
            context.Usuarios.AddOrUpdate(
               u => u.IdUsuario,
               new Usuarios { IdUsuario = 1, Nombre = "Admin1", Password = "Admin1", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Administrador, Activo = true },
               new Usuarios { IdUsuario = 2, Nombre = "gmonroy", Password = "Cmr2016$2", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Jefe, Activo = true },
               new Usuarios { IdUsuario = 3, Nombre = "lheredia", Password = "Cmr2016$3", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Vendedor, Activo = true },
               new Usuarios { IdUsuario = 4, Nombre = "mnoriega", Password = "Cmr2016$4", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Jefe, Activo = true },
               new Usuarios { IdUsuario = 5, Nombre = "emachorro", Password = "Cmr2016$5", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Jefe, Activo = true },
               new Usuarios { IdUsuario = 6, Nombre = "cguzman", Password = "Cmr2016$6", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Jefe, Activo = true },
               new Usuarios { IdUsuario = 7, Nombre = "msalinas", Password = "Cmr2016$7", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Servicio, Activo = true },
               new Usuarios { IdUsuario = 8, Nombre = "tmonroy", Password = "Cmr2016$8", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Servicio, Activo = true },
               new Usuarios { IdUsuario = 9, Nombre = "acazares", Password = "Cmr2016$9", Email = "cmonroyp@eymsa.com", Tipo = Usuarios.EnumUsuarioTipo.Administrador, Activo = true }
            );
        }

        private void SeedCategorias(EntidadesPagina context)
        {
            context.Clasificaciones.AddOrUpdate(
                c => c.IdClasificacion,
                new Clasificaciones { IdClasificacion = 1, Nombre = "HARDWARE HIS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 2, Nombre = "HARDWARE PACS CORE", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 3, Nombre = "HARDWARE PACS ENCORE", Descripcion = string.Empty },
             
                new Clasificaciones { IdClasificacion = 4, Nombre = "HARDWARE PARA ESTACIONES", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 5, Nombre = "DISCOS DUROS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 6, Nombre = "MONITORES DIAGNOSTICOS/CLINICOS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 7, Nombre = "MONITORES ", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 8, Nombre = "MONITORES REFACCIONES", Descripcion = string.Empty },


                new Clasificaciones { IdClasificacion = 9, Nombre = "PUBLICADORES", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 10, Nombre = "SISTEMAS DE TURNOS", Descripcion = string.Empty },

                new Clasificaciones { IdClasificacion = 11, Nombre = "ACCESORIOS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 12, Nombre = "TERMINALES ADICIONALES", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 13, Nombre = "DISPOSITIVOS EXTERNOS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 14, Nombre = "CONSUMIBLES", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 15, Nombre = "UPS (Sistema de Alimentaci�n Ininterrumpida)", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 16, Nombre = "MOBILIARIO", Descripcion = string.Empty },

                new Clasificaciones { IdClasificacion = 17, Nombre = "HARDWARE PARA MODALIDADES", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 18, Nombre = "MODALIDADES REFACCIONES", Descripcion = string.Empty },

                new Clasificaciones { IdClasificacion = 19, Nombre = "PACS ENCORE (Crecimiento en Unidades de Almacenamiento)", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 20, Nombre = "PACS ENCORE (Incremento en Unidades de Almacenamiento)", Descripcion = string.Empty },

                //Licencias
                new Clasificaciones { IdClasificacion = 21, Nombre = "LICENCIAS ADMINISTRADOR DE SERVIDORES", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 22, Nombre = "LICENCIAS VISOR MULTIMODALIDAD PARA RADI�LOGOS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 23, Nombre = "LICENCIAS VISOR WEB", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 24, Nombre = "LICENCIAS SISTEMA DE INFORMACI�N RADI�LOGICA", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 25, Nombre = "LICENCIAS MODULOS EXTRAS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 26, Nombre = "LICENCIAS ANTIVIRUS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 27, Nombre = "LICENCIAS RECONOCIMIENTO DE VOZ", Descripcion = string.Empty },

                //Servicio
                new Clasificaciones { IdClasificacion = 28, Nombre = "SERVICIOS PROFESIONALES", Descripcion = string.Empty },

                //Generales
                new Clasificaciones { IdClasificacion = 29, Nombre = "SISTEMAS MOVILES", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 30, Nombre = "SISTEMAS DE FLUROSCOP�A", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 31, Nombre = "SISTEMAS RADIOGR�FICOS", Descripcion = string.Empty },
                new Clasificaciones { IdClasificacion = 32, Nombre = "SISTEMAS Y APLICACIONES", Descripcion = string.Empty }
              );
        }

        private void SeedProductos(EntidadesPagina context)
        {
            context.Productos.AddOrUpdate(
               p => p.IdProducto,

            #region "1. HARDWARE HIS"

             new Productos
             {
                 IdProducto = 100,
                 Modelo = "G0130-1T 512",
                 Nombre = "Servidor para Sistema HIS",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor Adicional de Aplicaciones* HIS</b>. Servidor m�nimo con <b>512GB</b> de memoria RAM, 1TB de almacenamiento en RAID 5 y procesador Intel Xeon 48 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante, monitor plano de 19� *Requerido solo en el caso de tener m�s de 300 usuarios concurrentes utilizando el sistema.</label></body></html>",
                 Clasificacion = 1,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 22600M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 101,
                 Modelo = "G0130-1T 512-CPU",
                 Nombre = "Servidor para Sistema HIS (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU Servidor para Sistema HIS.</label></body></html>",
                 Clasificacion = 1,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 21500M,
                 EsInternacional = true
             },

            #endregion

            #region "2. HARDWARE PACS CORE"

              
             new Productos
             {
                 IdProducto = 201,
                 Modelo = "224-4856",
                 Nombre = "Servidor para PACS CORE B�sico",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servidor para PACS CORE. Procesador Intel Xeon Gold 5218 Deca Core 64bits (16 n�cleos) 2.3 a 3.9 GHz. Memoria de <b>16</b>GB DDR4 (1 x 16GB) Dual Ranked RDIMMs. Tarjeta contradora BOSS con 2 x Discos M.2 de <b>240</b>GB (en RAID 1) para el SO. 3 x Discos Duro de 2.0TB <b>7.200</b> RPM, Hot-Plug. LECTOR DVD-ROM 8x, SATA. Tarjeta Doble de red integrada 10Gb Broadcom NetXtreme II 57416. Sin Monitor. Sin Tarjeta de acceso remoto. Fuente de Poder no Redundante de Alta Potencia de Alta Potencia (750W).</label></body></html>",
                 Clasificacion = 2,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 7577M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 202,
                 Modelo = "224-4856-I",
                 Nombre = "Servidor para PACS CORE Intermedio",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servidor para PACS CORE. Procesador Intel Xeon Gold 5218 Deca Core 64bits (16 n�cleos) 2.3 a 3.9 GHz. Memoria de <b>32</b>GB DDR4 (1 x <b>32</b>GB) Dual Ranked RDIMMs. 2 x Discos SSD de <b>400</b>GB, Escritura Intensiva SAS, 12 Gbps, Hot-Plug. 3 x Discos Duro de 2.4TB <b>10.000</b> RPM, Hot-Plug. Lector DVD-ROM 8x SATA. Tarjeta Doble de red integrada 10Gb Broadcom NetXtreme II 57416. Sin Monitor. Tarjeta de acceso remoto Dell 9na Generaci�n iDRAC/9. Fuente de Poder Redundante de Alta Potencia (750W).</label></body></html>",
                 Clasificacion = 2,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 0M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 203,
                 Modelo = "224-4856-A",
                 Nombre = "Servidor para PACS CORE Avanzado",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servidor para PACS CORE. 2 Procesadores Intel Xeon Gold 5218 Deca Core 64bits (32 n�cleos) 2.3 a 3.9GHz. Memoria de <b>32</b>GB DDR4 (2 x <b>16</b>GB) Dual Ranked RDIMMs. 2 x Discos SSD de <b>400</b>GB, Escritura Intensiva SAS, 12 Gbps, Hot-Plug. 3 x Discos Duro de 2.4TB <b>10.000</b> RPM, Hot-Plug. Lector DVD-ROM 8x SATA. Tarjeta Doble de red integrada 10Gb Broadcom NetXtreme II 57416. Sin Monitor. Tarjeta de acceso remoto Dell 9na Generaci�n iDRAC/9. Fuente de Poder Redundante de Alta Potencia (750W).</label></body></html>",
                 Clasificacion = 2,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 0M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 204,
                 Modelo = "G0200-4TB",
                 Nombre = "Servidor de sistema PACS CORE 4TB",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de sistema PACS CORE 4TB 7,200RPM.</b> Servidor Torre con m�nimo con <b>16GB</b> de memoria RAM y procesador Intel Xeon Gold 16 n�cleos, tarjeta doble de red integrada 10GB y lector de DVD-ROM. Sistema operativo superior Microsoft Windows Server 2019 Standard Edition. Almacenamiento 3 x Discos Duro de 2.0TB SAS de 7,200 RPM  con f�cil expansi�n en terabytes hasta 16 Ranuras para disco duro. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye monitor, UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery).  �ptimo desempe�o con un m�ximo de 15000 estudios anuales.</label></body></html>",
                 Clasificacion = 2,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 8900M,
                 EsInternacional = true,
                 DatosCrudos = 4,
                 DatosUsables = 3
             },
            new Productos
            {
                IdProducto = 205,
                Modelo = "G0200-4TB-I",
                Nombre = "Servidor de sistema PACS CORE 4TB INTERMEDIO",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de sistema PACS CORE Intermedio 4.8TB 10,000RPM.</b> Servidor Torre con m�nimo con <b>32GB</b> de memoria RAM y procesador Intel Xeon Gold 16 n�cleos, tarjeta doble de red integrada 10GB, fuente de poder redundante de alta potencia (750W), lector de DVD-ROM y tarjeta de acceso remoto iDRAC. Sistema operativo superior Microsoft Windows Server 2019 Standard Edition en 2x discos de SSD 400GB en RAID1. Almacenamiento 3 x Discos Duro de 2.4TB SAS de 10,000 RPM con f�cil expansi�n en terabytes hasta 16 Ranuras para disco duro. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye monitor, UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery).</label></body></html>",
                Clasificacion = 2,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 14700M,
                EsInternacional = true,
                DatosCrudos = 4,
                DatosUsables = 3
            },
            new Productos
            {
                IdProducto = 206,
                Modelo = "G0200-4TB-A",
                Nombre = "Servidor de sistema PACS CORE 4TB AVANZADO",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de sistema PACS CORE Avanzado 4.8TB 10,000RPM</b> Servidor Torre con m�nimo con <b>64GB</b> de memoria RAM y procesador Intel Xeon Gold 32 n�cleos, tarjeta doble de red integrada 10GB, fuente de poder redundante de alta potencia (750W), lector de DVD-ROM y tarjeta de acceso remoto iDRAC. Sistema operativo superior Microsoft Windows Server 2019 Standard Edition en 2x discos de SSD 400GB en RAID1. Almacenamiento 3 x Discos Duro de 2.4TB SAS de 10,000 RPM con f�cil expansi�n en terabytes hasta 16 Ranuras para disco duro. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye monitor, UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery).</label></body></html>",
                Clasificacion = 2,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 19000M,
                EsInternacional = true,
                DatosCrudos = 4,
                DatosUsables = 3
            },
            #endregion

            #region "3. HARDWARE PACS ENCORE"

             new Productos
             {
                 IdProducto = 300,
                 Modelo = "G0199-0TB",
                 Nombre = "Servidor de aplicaciones ENCORE",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor Adicional de Aplicaciones* ENCORE.</b> Servidor m�nimo con <b>128GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante, monitor plano de 19� *Requerido solo en el caso de tener m�s de 50 usuarios concurrentes utilizando el sistema.</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 17159.90M,
                 EsInternacional = true, 
             },
             new Productos
             {
                 IdProducto = 301,
                 Modelo = "G0199-0TB-CPU",
                 Nombre = "Servidor de aplicaciones ENCORE (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU de aplicaciones Encore.</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 16060M,
                 EsInternacional = true
             },

               new Productos
               {
                   IdProducto = 302,
                   Modelo = "G0199-4TB",
                   Nombre = "Servidor de Almacenamiento ENCORE 4TB",
                   Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de Almacenamiento ENCORE 4TB</b>.<br>Servidor m�nimo con <b>32GB</b> de memoria RAM y procesador Intel Xeon <b>32 n�cleos 3.90GHz</b>, Tarjeta Cu�druple de red integrada, Fuente de Poder Redundante, monitor plano de 19�, Discos duros SSD Escritura Intensiva (Sistema Operativo). Almacenamiento de 4T 10.000 RPM expandible 24 r�nuras. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes(Disaster Recovery). Microsoft Windows Server 2019. * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB.</label></body></html>",
                   Clasificacion = 3,
                   Tipo = Productos.EnumProductoTipos.Estructura,
                   Moneda = Productos.EnumMonedas.USD,
                   Precio = 16643M,
                   EsInternacional = true,
                   DatosCrudos=4,
                   DatosUsables=3.2
               },
              new Productos
              {
                  IdProducto = 303,
                  Modelo = "G0199-4TB-CPU",
                  Nombre = "Servidor de Almacenamiento ENCORE 4TB (CPU)",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servidor de Almacenamiento ENCORE 4TB (CPU).</label></body></html>",
                  Clasificacion = 3,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 9900M,
                  EsInternacional = true
              },
             new Productos
             {
                 IdProducto = 304,
                 Modelo = "G0199-CPU-PRO",
                 Nombre = "Servidor de Almacenamiento ENCORE PRO (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>PACS ENCORE PRO (CPU).</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 0M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 305,
                 Modelo = "G0199-CPU",
                 Nombre = "Servidor de Almacenamiento ENCORE BASE (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>PACS ENCORE BASE (CPU).</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 0M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 306,
                 Modelo = "G0199-8TB",
                 Nombre = "Servidor de Almacenamiento ENCORE 8TB",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de Almacenamiento ENCORE 8TB.</b> Servidor m�nimo con <b>64GB</b> de memoria RAM y procesador Intel Xeon 20 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante, monitor plano de 19�. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB.</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 18000M,
                 EsInternacional = true,
                 DatosCrudos=8,
                 DatosUsables=6.4
             },
             new Productos
             {
                 IdProducto = 307,
                 Modelo = "G0199-8TB-CPU",
                 Nombre = "Servidor de Almacenamiento ENCORE 8TB (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU servidor de almacenamiento ENCORE 8TB.</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 12000M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 308,
                 Modelo = "G0199-12TB",
                 Nombre = "Servidor de Almacenamiento ENCORE 12TB",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de Almacenamiento ENCORE 12TB.</b> Servidor m�nimo con <b>128GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante, monitor plano de 19�. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB. En caso de m�s de 10 modalidades utilizar versi�n PRO.</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 22000M,
                 EsInternacional = true,
                 DatosCrudos=12,
                 DatosUsables=9.6
             },
             new Productos
             {
                 IdProducto = 309,
                 Modelo = "G0199-12TB-CPU",
                 Nombre = "Servidor de Almacenamiento ENCORE 12TB (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic,Century Gothic'><label>Servidor de Almacenamiento ENCORE 12TB (CPU).</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 16000M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 310,
                 Modelo = "G0199-12TBPRO",
                 Nombre = "Servidor de Almacenamiento ENCORE PRO 12TB",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de Almacenamiento ENCORE PRO 12TB.</b> Servidor m�nimo con <b>256GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). Enfocado a modalidades multicorte que requieren acceso simultaneo para almacenamiento. * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB.</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 26000M,
                 EsInternacional = true,
                 DatosCrudos=12,
                 DatosUsables=9.6
             },
             new Productos
             {
                 IdProducto = 311,
                 Modelo = "G0199-12TB PRO-CPU",
                 Nombre = "Servidor de Almacenamiento ENCORE PRO 12TB (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servidor de Almacenamiento ENCORE PRO 12TB (CPU).</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 20000M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 313,
                 Modelo = "G0199-16TB",
                 Nombre = "Servidor de Almacenamiento ENCORE 16TB",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de Almacenamiento ENCORE 16TB.</b> Servidor m�nimo con <b>128GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB. En caso de m�s de 10 modalidades utilizar versi�n PRO.</label></body></html>",
                 Clasificacion = 3,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 26200M,
                 EsInternacional = true,
                 DatosCrudos=16,
                 DatosUsables=12.8
             },
            new Productos
            {
                IdProducto = 314,
                Modelo = "G0199-16TB-CPU",
                Nombre = "Servidor de Almacenamiento ENCORE 16TB (CPU)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servidor de Almacenamiento ENCORE 16TB (CPU).</label></body></html>",
                Clasificacion = 3,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 20200M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 315,
                Modelo = "G0199-16TBPRO",
                Nombre = "Servidor de Almacenamiento ENCORE PRO 16TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor de Almacenamiento ENCORE PRO 16TB.</b> Servidor m�nimo con <b>256GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). Enfocado a modalidades multicorte que requieren acceso simultaneo para almacenamiento. * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB.</label></body></html>",
                Clasificacion = 3,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 28200M,
                EsInternacional = true,
                DatosCrudos=16,
                DatosUsables=12.8
            },
            new Productos
            {
                IdProducto = 316,
                Modelo = "G0199-16TB PRO-CPU",
                Nombre = "Servidor de Almacenamiento ENCORE PRO 16TB (CPU)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servidor de Almacenamiento ENCORE PRO 16TB (CPU).</label></body></html>",
                Clasificacion = 3,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 22200M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 317,
                Modelo = "G0199-VOZ",
                Nombre = "Servidor Adicional Reconocimiento de voz ENCORE",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Servidor Adicional Reconocimiento de Voz ENCORE.</b> Servidor m�nimo con <b>8GB</b> de memoria RAM y procesador Intel Xeon 4 n�cleos. *Requerido solo en el caso de necesitar la instalaci�n de motor de reconocimiento local. (Opci�n disponible s�lo en la compra m�nima de 5 usuarios concurrentes en la instalaci�n).</label></body></html>",
                Clasificacion = 3,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 3548M,
                EsInternacional = true 
            },
            

            #endregion

            #region "4. HARDWARE PARA ESTACIONES"

             new Productos
             {
                 IdProducto = 400,
                 Modelo = "D0068-H",
                 Nombre = "Estaci�n de Diagn�stico DiagRX SMART",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Estaci�n de env�os para Radi�logos (DIAG SMART) con caracter�sticas de: 3rd Gen Intel Core i3-3220, <b>2GB</b> 600MHZ DDR3, 1 disco de 256GB SSD, Tarjeta de video integrada, Monitor plano de 19�, 16X DVD+/-RW SATA,Data Only, Microsoft Windows 10PRO, 64bits. Incluye UPS.</label></body></html>",
                 Clasificacion = 4,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 938M,
                 EsInternacional = true
             },
              new Productos
              {
                  IdProducto = 401,
                  Modelo = "D0054-H",
                  Nombre = "DiagRX ALFA",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Estaci�n de revisi�n para Radi�logos (DIAG ALFA) con caracter�sticas de: 3rd Gen Intel Core i7-3770, <b>4GB</b> 600MHZ DDR3, 1 disco de 256GB SSD, Tarjeta de video integrada,16X DVD+/-RW SATA,Data Only, Microsoft Windows 10PRO, 64bits. Incluye UPS. (S�lo monitores cl�nicos).</label></body></html>",
                  Clasificacion = 4,
                  Tipo = Productos.EnumProductoTipos.Estructura,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 1260M,
                  EsInternacional = true
              },
             new Productos
             {
                 IdProducto = 402,
                 Modelo = "D0051-H2",
                 Nombre = "Estaci�n de Diagn�stico DiagRX S/tarjeta video",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Estaci�n de diagn�stico para Radi�logos (DIAGRX) con caracter�sticas de: Hexa Core XEON 3.2GHz, <b>8GB</b> DDR3 RDIMM 1600, DD de 256GB SSD SATA, Monitor plano de 19� 16X DVD+/-RW Dual Layer SATA, Microsoft Windows 10PRO, 64bits. Incluye UPS.</label></body></html>",
                 Clasificacion = 4,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 3000M,
                 EsInternacional = true
             },
            new Productos
            {
                IdProducto = 403,
                Modelo = "D0051-H3",
                Nombre = "Estaci�n de Diagn�stico DiagRX S/tarjeta video",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Estaci�n de diagn�stico para Radi�logos (DIAGRX) con caracter�sticas de: OCTA Core I9 5.00GHZ, <b>8GB</b> DDR3, 1 disco 512GB SSD, 2x Puertos Red Gigabit , Unidad de 8x CD-DVD+/-RW. Microsoft Windows 10PRO, 64bits. Monitor plano de 19�. Incluye UPS</label></body></html>",
                Clasificacion = 4,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2063M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 404,
                Modelo = "D0061-H",
                Nombre = "Estaci�n de Diagn�stico DiagRX ALFA MASTO",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Estaci�n de diagn�stico para Radi�logos (DIAGRX ALFA MASTO) con caracter�sticas de: Hexa Core XEON 3.2GHz, <b>16GB</b> DDR3 RDIMM 1600, DD de 256GB SSD SATA, 16X DVD+/-RW Dual Layer SATA, Microsoft Windows 10PRO, 64bits. Incluye UPS.</label></body></html>",
                Clasificacion = 4,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 3840M,
                EsInternacional = true
            },
            
           
             new Productos
             {
                 IdProducto = 410,
                 Modelo = "224-6874",
                 Nombre = "Estaci�n de Diagn�stico DiagRX Alfa (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU Modelo: Optiplex 7050 Marca: Dell.</label></body></html>",
                 Clasificacion = 4,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 900M,
                 EsInternacional = true
             }, 
             new Productos
             {
                 IdProducto = 412,
                 Modelo = "DIAG SMART CPU",
                 Nombre = "Estaci�n de Diagn�stico DiagRX SMART (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU Dell para Sistema Diag Smart.</label></body></html>",
                 Clasificacion = 4,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 979M,
                 EsInternacional = true
             },            
             new Productos
             {
                 IdProducto = 414,
                 Modelo = "799501-2",
                 Nombre = "Estaci�n de Diagn�stico S/Tarjeta de Video (CPU) XEON",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU Modelo: WORKSTATION DELL PRECISION 7810 825W TORRE Marca: DELL. XEON.</label></body></html>",
                 Clasificacion = 4,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 2600M,
                 EsInternacional = true
             }, 
            new Productos
            {
                IdProducto = 416,
                Modelo = "799501-3",
                Nombre = "Estaci�n de Diagn�stico S/Tarjeta de Video (CPU) I9",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU WORKSTATION DELL Procesador Intel Core i9-9900K con Intel VT-x (Octa Core) Sin Tarjeta de Video Sin Monitor. XEONI9</label></body></html>",
                Clasificacion = 4,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1647M,
                EsInternacional = true
            },                   

            #endregion

            #region "5. DISCOS DUROS"

            new Productos
            {
                IdProducto = 500,
                Modelo = "G0199-4TB-INC8TB",
                Nombre = "Discos Duros Servidor Incremento 4TB 10,000RPM",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Discos Duros Servidor Incremento 4TB 10,000RPM.</label></body></html>",
                Clasificacion = 5,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 0M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 501,
                Modelo = "3335318",
                Nombre = "Disco estado s�lido 2.5� de 1TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Disco estado s�lido 2.5� de 1TB.</label></body></html>",
                Clasificacion = 5,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 368M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 502,
                Modelo = "3335313",
                Nombre = "Disco mec�nico de 1TB 3.5� 7,500 RPM",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Disco mec�nico de 1TB 3.5� 7,500 RPM.</label></body></html>",
                Clasificacion = 5,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 105M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 503,
                Modelo = "3335314",
                Nombre = "Disco estado s�lido 2.5� de 500GB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Disco estado s�lido de 2.5� de 500GB.</label></body></html>",
                Clasificacion = 5,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 159M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 504,
                Modelo = "G0199-HD1TB",
                Nombre = "Disco duro DELL de 1.8TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Disco Duro DELL de <b>1.8TB, SAS, 12Gbps, 2.5�</b>, 10.000 RPM Compatible con Servidor Dell PowerEdge R720/R720xd/R730/R730xd</label></body></html>",
                Clasificacion = 5,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 716M,
                EsInternacional = true 
            },
            new Productos
            {
                 IdProducto = 505,
                 Modelo = "G0199-HD2TB",
                 Nombre = "Disco duro DELL de 2TB",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Discos Duro de <b>2TB, Near-Line SAS</b>, 12Gbps, <b>2.5</b>�, 7.200 RPM, Hot-Plug *Compatible solo con servidor Torre</label></body></html>",
                 Clasificacion = 5,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 767M,
                 EsInternacional = true
            },
            new Productos
            {
                  IdProducto = 506,
                  Modelo = "G0199-HD1.21TB",
                  Nombre = "Disco duro DELL",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Disco Duro DELL de <b>1.2TB, SAS, 12Gbps, 2.5</b>�, 10.000 RPM Compatible con Servidor Dell PowerEdge R720/R720xd/R730/R730xd</label></body></html>",
                  Clasificacion = 5,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 716M,
                  EsInternacional = true
            }, 

            #endregion

            #region "6. MONITORES DIAGNOSTICOS / CLINICOS"

            new Productos
            {
                IdProducto = 600,
                Modelo = "K9301808A",
                Nombre = "1 Monitor BARCO Color LED 2MP grado cl�nico 24\" Color sin tarjeta de video MDRC2224BMKII",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>2MP</b> grado <b>cl�nico</b>, software de calibraci�n y 3 a�os de garant�a. 24� COLOR. Modelo MDRC2224BMKII.</label></body></html>",
                Clasificacion = 6,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1012M,
                EsInternacional = true
            },
             new Productos
             {
                 IdProducto = 601,
                 Modelo = "K9602524",
                 Nombre = "1 Monitor BARCO Color Led 2MP con tarjeta gr�fica MDNC-2221",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>2MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-2221.</label></body></html>",
                 Clasificacion = 6,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 2986M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 602,
                 Modelo = "K9602530",
                 Nombre = "2 Monitores BARCO Color Led 2MP con tarjeta gr�fica MDNC-2221",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>2</b> Monitores <b>BARCO</b> COLOR LED <b>2MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-2221.</label></body></html>",
                 Clasificacion = 6,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 4730M,
                 EsInternacional = true
             },            

             new Productos
             {
                 IdProducto = 603,
                 Modelo = "K9602854",
                 Nombre = "1 Monitor BARCO Color LED 3MP con Tarjeta Gr�fica MDNC-3321",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>3MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-3321.</label></body></html>",
                 Clasificacion = 6,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 4750M,
                 EsInternacional = true
             }, 
            
             
             new Productos
             {
                 IdProducto = 604,
                 Modelo = "K9602876",
                 Nombre = "1 Monitor BARCO NIO COLOR LED 3MP EM MDNC-3321",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BARCO</b> NIO COLOR LED <b>3MP</b> grado <b>m�dico</b>, EM. Market sin cubierta frontal, con tarjeta de video, QAWeb software de calibraci�n y 5 a�os de garant�a.</label></body></html>",
                 Clasificacion = 6,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 3285M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 605,
                 Modelo = "K9602858",
                 Nombre = "2 Monitores Barco NIO LED 3MP MDNC-3321",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>2</b> Monitores <b>BARCO</b> NIO LED <b>3MP</b> grado <b>m�dico</b> con tarjeta de video MXRT-5600, QAWeb software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-3421</label></body></html>",
                 Clasificacion = 6,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 8245M,
                 EsInternacional = true
             },
              
              new Productos
              {
                  IdProducto = 606,
                  Modelo = "K9602730",
                  Nombre = "1 Monitor BARCO Color LED 5MP con Tarjeta Gr�fica MDNG5221",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BARCO</b> MONOCROM�TICO LED <b>5MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a.</label> Modelo MDNG5221.</body></html>",
                  Clasificacion = 6,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 6522M,
                  EsInternacional = true
              },
             new Productos
             {
                 IdProducto = 607,
                 Modelo = "K9602734",
                 Nombre = "2 Monitores BARCO Color LED 5MP con Tarjeta Gr�fica MDNG5221",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>2</b> Monitores <b>BARCO</b> MONOCROM�TICOS LED <b>5MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDNG5221.</label></body></html>",
                 Clasificacion = 6,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 12200M,
                 EsInternacional = true
             },
            new Productos
            {
                IdProducto = 608,
                Modelo = "K9602869",
                Nombre = "1 Monitor BARCO Nio Color LED 5.8 MP MDNC6121",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>5.8MP</b> grado <b>m�dico</b> con tarjeta de video MXRT-5600, QAWeb software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-6121.</label></body></html>",
                Clasificacion = 6,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 7375M,
                EsInternacional = true
            },
              new Productos
              {
                  IdProducto = 609,
                  Modelo = "K9602743",
                  Nombre = "1 Monitor BARCO Color LED 6MP con Tarjeta Gr�fica MDCC-6330",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>6MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDCC-6330.</label></body></html>",
                  Clasificacion = 6,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 10500M,
                  EsInternacional = true
              },
               new Productos
               { // Se pidio que no se incluyera en las listas de precio
                   IdProducto = 610,
                   Modelo = "G11S",
                   Nombre = "1 Monitor BEACON G11S 19 Monocrom�tico 1MP",
                   Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BEACON</b> G11S 19 Monocrom�tico <b>1MP</b></label></body></html>",
                   Clasificacion = 6,
                   Tipo = Productos.EnumProductoTipos.Producto,
                   Moneda = Productos.EnumMonedas.USD,
                   Precio = 1111M,
                   EsInternacional = true
               },
              new Productos
              {
                  IdProducto = 611,
                  Modelo = "HL2416SH",
                  Nombre = "1 Monitor BEACON-BIGTIDE 24� LED",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BEACON-BIGTIDE</b> de 24� <b>2.4MP</b> grado <b>cl�nico</b></label></body></html>",
                  Clasificacion = 6,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 580M,
                  EsInternacional = true
              },            
            new Productos
            {
                IdProducto = 613,
                Modelo = "C32SP+",
                Nombre = "1 Monitor BEACON 3MP Color",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BEACON</b> de 21.3� <b>3MP</b> Color grado <b>m�dico</b> con pantalla protectora y sensor frontal. Incluye adaptador de ca/cc. Modelo GSM120A12.</label></body></html>",
                Clasificacion = 6,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2380M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 614,
                Modelo = "G52SP+",
                Nombre = "1 Monitor BEACON 5MP Grayscale",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BEACON</b> de 21.3� <b>5MP</b> Grayscale grado <b>m�dico</b> con pantalla protectora y sensor frontal. Incluye adaptador de ca/cc. Modelo GSM90A12.</label></body></html>",
                Clasificacion = 6,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 4880M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 615,
                Modelo = "C53SP+",
                Nombre = "1 Monitor BEACON 5MP Color",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>1</b> Monitor <b>BEACON</b> de 21.3� <b>5MP</b> Color grado <b>m�dico</b> con pantalla protectora y sensor frontal. Incluye adaptador de ca/cc. Modelo GSM90A12</label></body></html>",
                Clasificacion = 6,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 5080M,
                EsInternacional = true
            },
            

            #endregion

            #region "7. MONITORES" 

            new Productos
            {
                IdProducto = 700,
                Modelo = "E413198",
                Nombre = "Elo Touch X - Serie 15�",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Pantalla LED T�ctil de 15� con procesador Intel Core i3, almacenaje 138GB, Memoria interna 4GB, 9 puertos USB 2.0, Peso aproximado 7.4 kg, Altura 34.7 cm, Ancho 36.5cm Profundidad 6.6cm.</label></body></html>",
                Clasificacion = 7,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1351M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 701,
                Modelo = "1509L",
                Nombre = "Monitor Elo 15�",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Monitor ELOTOUCH de 15�.</label></body></html>",
                Clasificacion = 7,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 307M,
                EsInternacional = true
            },
            
             new Productos
             {
                 IdProducto = 703,
                 Modelo = "3335706",
                 Nombre = "Monitor panel plano led digital 23",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>MONITOR PANEL PLANO LED DIGITAL 21� FULLHD Resoluci�n 1920 x 1080. Especial para aplicaciones HIS/RIS.</label></body></html>",
                 Clasificacion = 7,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 156M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 704,
                 Modelo = "320-1092",
                 Nombre = "Monitor 19� 1MP P1914S",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Monitor Marca DELL de 19�.</label></body></html>",
                 Clasificacion = 7,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 209M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 705,
                 Modelo = "320-4687",
                 Nombre = "Monitor Dell 19� Ultra Sharp",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Monitor Dell 19� Ultra Sharp.</label></body></html>",
                 Clasificacion = 7,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 94M,
                 EsInternacional = true
             },             

            #endregion

            #region "8. MONITORES REFACCIONES"
            #endregion

            #region "9. PUBLICADORES"

            new Productos
            {
                IdProducto = 900,
                Modelo = "3335113",
                Nombre = "CPU Robot Quemador",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU Robot Quemador Marca DELL.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 790M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 901,
                Modelo = "D0060",
                Nombre = "Robot Quemador Lite 20 CDS Rimage",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Robot quemador con capacidad de alimentaci�n de 20 discos (CD/DVD).</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2724M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 902,
                Modelo = "3335209",
                Nombre = "Publicador Discos Allegro20",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Publicador autom�tico de discos marca Rimage modelo Allegro 20, 1 a�o de garant�a.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2144M,
                EsInternacional = true
            },


            new Productos
            {
                IdProducto = 903,
                Modelo = "3335504",
                Nombre = "Allegro20 Mantenimiento Anual",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>P�liza de mantenimiento correctivo 12 meses a partir del 2�. a�o para Allegro20.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 0M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 904,
                Modelo = "D0074-1T",
                Nombre = "Robot Quemador Lite 100 CD / DVD Rimage",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Robot quemador con capacidad de alimentaci�n de 100 discos (CD/DVD) con una tinta.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2956M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 905,
                Modelo = "3335210",
                Nombre = "Publicador Discos Allegro100",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Publicador autom�tico de discos marca Rimage modelo Allegro 100, 1 a�o de garant�a.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 3686M,
                EsInternacional = true
            },


            new Productos
            {
                IdProducto = 906,
                Modelo = "3335505",
                Nombre = "Allegro100 Mantenimiento Anual",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>P�liza de mantenimiento correctivo 12 meses a partir del 2�. a�o para Allegro100.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 0M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 907,
                Modelo = "3335211",
                Nombre = "Publicador Discos Catalyst 6000N",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Publicador autom�tico de discos Catalyst 6000N, 3 bandejas de 50 discos de entrada cada una, computadora incluida y paquete de 100 CD's.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 9578M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 908,
                Modelo = "3335506",
                Nombre = "Catalyst 6000N Mantenimiento Anual",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>P�liza de mantenimiento correctivo 12 meses a partir del 2�. a�o para Catalyst 6000N.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 0M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 909,
                Modelo = "3335207",
                Nombre = "Publicador Epson PP-50",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Publicador autom�tico de discos marca Epson modelo PP-50, 1 a�o de garant�a.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2221M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 910,
                Modelo = "3335208",
                Nombre = "Publicador Epson PP-100II",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Publicador autom�tico de discos marca Epson modelo PP-100II, 1 a�o de garant�a.</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 3172M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 911,
                Modelo = "D0060-R",
                Nombre = "Publicador Rimage  20 discos (CD/DVD)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Robot quemador Allegro con capacidad de alimentaci�n de 20 discos (CD/DVD) una sola tinta modo impresi�n Inyecci�n. Incluye CPU, Nobreak, 600 CDs Imprimibles y una tinta de cada cartucho</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 4996M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 912,
                Modelo = "D0060-50E",
                Nombre = "Publicador Epson 50 discos (CD/DVD)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Robot quemador PP-50 con capacidad de alimentaci�n de 50 discos (CD/DVD) m�ltiples tintas modo Impresi�n Inyecci�n. Incluye CPU, Nobreak, 600 CDs Imprimibles y una tinta de cada cartucho</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 5000M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 913,
                Modelo = "D0074-1T-R",
                Nombre = "Publicador Rimage 100 discos (CD/DVD)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Robot quemador Allegro con capacidad de alimentaci�n de 100 discos (CD/DVD), una sola tinta modo impresi�n Inyecci�n. Incluye CPU, Nobreak, 600 CDs Imprimibles y una tinta de cada cartucho</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 5761M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 914,
                Modelo = "D0074-1T-E",
                Nombre = "Publicador Epson 100 discos (CD/DVD)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Robot quemador PP-100II con capacidad de alimentaci�n de 100 discos (CD/DVD) m�ltiples tintas modo Impresi�n Inyecci�n. Incluye CPU, Nobreak, 600 CDs Imprimibles y una tinta de cada cartucho</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 5761M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 915,
                Modelo = "D0074-1T-150R",
                Nombre = "Publicador Rimage 150 discos (CD/DVD)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Robot quemador Catalyst 6000N con capacidad de alimentaci�n de 100 discos (CD/DVD) Impresi�n Transferencia t�rmica. Incluye CPU, Nobreak, 1000 CDs Imprimibles y dos cintas color para transferencia t�rmica</label></body></html>",
                Clasificacion = 9,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 14000M,
                EsInternacional = true
            },

            #endregion

            #region "10. SISTEMAS DE TURNOS"

             new Productos
             {
                 IdProducto = 1000,
                 Modelo = "A0168",
                 Nombre = "Sistema de Turnos Kiosko Completo",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Sistema de Turnos Kiosko Completo.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 4999M,
                 EsInternacional = true
             }, 
             new Productos
             {
                 IdProducto = 1001,
                 Modelo = "D0216",
                 Nombre = "Sistema Turnos Kiosko Impresora",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Sistema de Turnos Kiosko Impresora <b>TTP2030.</b></label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 2940M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 1002,
                 Modelo = "A0160",
                 Nombre = "Zebra TTP 2030 Kiosk Printer USB",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Zebra TTP 2030 Kiosk Printer USB.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 640M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1003,
                 Modelo = "A0163",
                 Nombre = "Zebra Kiosk Print Station",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Impresora t�rmica.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 533M,
                 EsInternacional = true
             }, 
             new Productos
             {
                 IdProducto = 1004,
                 Modelo = "D0218",
                 Nombre = "Sistema Turnos Kiosko Lector",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Sistema de Turnos Kiosko Lector.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 2241M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 1008,
                 Modelo = "D0217",
                 Nombre = "Sistema Turnos Controlador TV",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Sistema de Turnos Controlador TV.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 1199M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1009,
                 Modelo = "NEO-Z83-4",
                 Nombre = "Computadora Minix",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Computadora Minix.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 349M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1010,
                 Modelo = "3330216",
                 Nombre = "Cable HDMI - HDMI 15m",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cable HDMI 15m.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 50M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1011,
                 Modelo = "177146",
                 Nombre = "Video Spliter HDMI 1in:4out",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Accesorio especializado para dividir una se�al HDMI Marca Manhattan.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 154M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1012,
                 Modelo = "USB2VGAE2",
                 Nombre = "Cable USB - VGA",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cable USB - VGA.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 56M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 1013,
                 Modelo = "D0090",
                 Nombre = "Sistema Turnos Controlador TV Simple",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Sistema Turnos Controlador TV Lite.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 502M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1014,
                 Modelo = "D0227",
                 Nombre = "Sistema Turnos Terminal T�cnico Pared",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Sistema de Turnos Terminal T�cnico Pared.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 879M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1015,
                 Modelo = "200645",
                 Nombre = "Caja Soporte Monitor",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Caja Soporte Monitor.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 159M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1016,
                 Modelo = "D0228",
                 Nombre = "Sistema Turnos Terminal T�cnico Mesa",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Sistema Turnos Terminal T�cnico Mesa.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 879M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1017,
                 Modelo = "200670",
                 Nombre = "Base de Sistema de Turnos para Mesa",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Base de Sistema de Turnos para Mesa.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 0M,
                 EsInternacional = true
             },

            #endregion

            #region "11. ACCESORIOS"

             new Productos
             {
                 IdProducto = 1100,
                 Modelo = "FX 2000",
                 Nombre = "Tarjeta de video Nvidia k2000/k2200",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Tarjeta Video 2 GB Doble salida display port 1.4.</label></body></html>",
                 Clasificacion = 11,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 200M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 1101,
                 Modelo = "799508",
                 Nombre = "Etiqueta de identificaci�n DIAG-RX",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Etiqueta de identificaci�n DIAG-RX.</label></body></html>",
                 Clasificacion = 11,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 0M,
                 EsInternacional = true
             },



             new Productos
             {
                 IdProducto = 1102,
                 Modelo = "3330127",
                 Nombre = "Convertidor HUBELL",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Convertidor HUBELL.</label></body></html>",
                 Clasificacion = 11,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 0M,
                 EsInternacional = true
             },


             new Productos
             {
                 IdProducto = 1103,
                 Modelo = "3330104",
                 Nombre = "Cable USB - Impresora",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cable USB - Impresora.</label></body></html>",
                 Clasificacion = 11,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 0M,
                 EsInternacional = true
             },           
             new Productos
              {
                  IdProducto = 1105,
                  Modelo = "SDOCK2U313",
                  Nombre = "Unidad para respaldos baja productividad",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Unidad para respaldos baja productividad</label></body></html>",
                  Clasificacion = 11,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 101M,
                  EsInternacional = true
             },

             new Productos
             {
                IdProducto = 1106,
                Modelo = "WX5100-8GB",
                Nombre = "Tarjeta Video Radeon Pro WX5100 8GB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Tarjeta Video Radeon Pro WX5100 8GB</label></body></html>",
                Clasificacion = 11,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio =1600M,
                EsInternacional = true
             },

             new Productos
             {
                IdProducto = 1107,
                Modelo = "PYN4000-8G",
                Nombre = "Tarjeta Video NVIDIA PYN4000 8GB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Tarjeta Video NVIDIA PYN4000 8GB</label></body></html>",
                Clasificacion = 11,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 805M,
                EsInternacional = true
             },

             new Productos
             {
                IdProducto = 1108,
                Modelo = "K9306036",
                Nombre = "Controlador de pantalla PCle 3D de 4 cabezales de medio alcance MXRT-5600 4GB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Controlador de pantalla PCle 3D de 4 cabezales de medio alcance MXRT-5600 4GB</label></body></html>",
                Clasificacion = 11,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 750M,
                EsInternacional = true
             },

             new Productos
             {
                IdProducto = 1109,
                Modelo = "K9306041",
                Nombre = "Controlador de pantalla compacto 3D PCle MXRT-2600 2GB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Controlador de pantalla compacto 3D PCle MXRT-2600 2GB</label></body></html>",
                Clasificacion = 11,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 380M,
                EsInternacional = true
             },

            #endregion

            #region "12. TERMINALES ADICIONALES"

            new Productos
            {
                IdProducto = 1200,
                Modelo = "G0136",
                Nombre = "Terminal adicional para RIS / WebServex",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Terminal adicional para RIS / WebServex.</label></body></html>",
                Clasificacion = 12,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1013M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 1201,
                Modelo = "A0137",
                Nombre = "Terminal adicional para Quir�fano",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Terminal adicional para Quir�fano.</label></body></html>",
                Clasificacion = 12,
                Tipo = Productos.EnumProductoTipos.Estructura,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1260M,
                EsInternacional = true
            },
             new Productos
             {
                 IdProducto = 1202,
                 Modelo = "224-7707",
                 Nombre = "Terminal adicional para RIS / WebServex (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>CPU RIS/WEBSERVEX.</label></body></html>",
                 Clasificacion = 12,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 895M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1203,
                 Modelo = "210-AATO",
                 Nombre = "Terminal adicional para Quir�fano (CPU)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Estaci�n de Quir�fano.</label></body></html>",
                 Clasificacion = 12,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 874M,
                 EsInternacional = true
             },
            new Productos
            {
                IdProducto = 1204,
                Modelo = "D0095",
                Nombre = "Tableta iPad 9.7� Retina",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Tableta iPad 9.7� Retina.</label></body></html>",
                Clasificacion = 12,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 458M,
                EsInternacional = true
            },

             new Productos
             {
                 IdProducto = 1205,
                 Modelo = "D0097",
                 Nombre = "Tableta iPad 12.9� Retina",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Tableta iPad 12.9� Retina.</label></body></html>",
                 Clasificacion = 12,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 1250M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1206,
                 Modelo = "A0140",
                 Nombre = "Tableta Windows",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Tableta Windows 10�.</label></body></html>",
                 Clasificacion = 12,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 589M,
                 EsInternacional = true
             }, 

            #endregion

            #region "13. "DISPOSITIVOS EXTERNOS"

             new Productos
             {
                 IdProducto = 1300,
                 Modelo = "D0221",
                 Nombre = "Impresora Zebra GC420",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Impresora Zebra <b>GC420T</b>.</label></body></html>",
                 Clasificacion = 13,
                 Tipo = Productos.EnumProductoTipos.Estructura,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 724M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1301,
                 Modelo = "D0221-1",
                 Nombre = "Impresora Zebra GC420",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Impresora de transferencia t�rmica <b>GC420</b>.</label></body></html>",
                 Clasificacion = 13,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 338M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1302,
                 Modelo = "LS2208",
                 Nombre = "Lector de c�digo de barras pistola",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Lector de C�digo de Barras Pistola.</label></body></html>",
                 Clasificacion = 13,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 126M,
                 EsInternacional = true
             },
              new Productos
              {
                  IdProducto = 1303,
                  Modelo = "DS9208-SR4NNU21Z",
                  Nombre = "Lector C�digo de Barras Fijo",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Lector de C�digo de Barras Fijo.</label></body></html>",
                  Clasificacion = 10,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 291M,
                  EsInternacional = true
              },
             new Productos
             {
                 IdProducto = 1304,
                 Modelo = "DS9208-BASE",
                 Nombre = "Base para Lector C�digo de Barras Fijo",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Base Lector de C�digo de Barras Fijo.</label></body></html>",
                 Clasificacion = 10,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 10M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1305,
                 Modelo = "FPAEUSB2-S",
                 Nombre = "Pedal de grabaci�n para transcripci�n",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Pedal de grabaci�n para transcripci�n.</label></body></html>",
                 Clasificacion = 13,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 85M,
                 EsInternacional = true
             }, 
             new Productos
             {
                 IdProducto = 1306,
                 Modelo = "A0147",
                 Nombre = "Diadema de dictador de bluetooth",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Diadema bluetooth para dictado.</label></body></html>",
                 Clasificacion = 13,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 244M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 1307,
                 Modelo = "LFH5276",
                 Nombre = "Micr�fono para reconocimiento de voz speechmike",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Micr�fono para reconocimiento de voz SpeechMike.</label></body></html>",
                 Clasificacion = 13,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 300M,
                 EsInternacional = true
             }, 
             new Productos
             {
                 IdProducto = 1309,
                 Modelo = "HP2460",
                 Nombre = "Impresora Deskjet HP 2515",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Impresora de papel.</label></body></html>",
                 Clasificacion = 13,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 380M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1310,
                 Modelo = "G0141",
                 Nombre = "Esc�ner para RIS",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Esc�ner para RisX.</label></body></html>",
                 Clasificacion = 13,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 217M,
                 EsInternacional = true
             },

            new Productos
            {
                IdProducto = 1311,
                Modelo = "D0229",
                Nombre = "Impresora Zebra ZD510-HC Brazaletes",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Impresora Zebra <b>ZD510-HC</b> Brazaletes.</label></body></html>",
                Clasificacion = 13,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 558M,
                EsInternacional = true
            }, 

            #endregion

            #region "14. CONSUMIBLES"

             new Productos
             {
                 IdProducto = 1400,
                 Modelo = "D0062",
                 Nombre = "Discos de 500 GB (2 pzas) Para Disaster Recovery",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Discos de 500GB (2 pzas.) para dispositivo externo de almacenamiento para im�genes(Disaster Recovery).</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 370M,
                 EsInternacional = true
             },


             new Productos
             {
                 IdProducto = 1401,
                 Modelo = "3335950",
                 Nombre = "Cartucho de tinta All in one para robot publicador Allegro 20 y Allegro 100",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cartucho de tinta All in one para robot publicador Allegro 20 y Allegro 100.</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 72M,
                 EsInternacional = true
             },

              new Productos
              {
                  IdProducto = 1402,
                  Modelo = "3335951",
                  Nombre = "Paquete 100 CDs Inyecci�n Tinta (Servimex)",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Paquete 100 CDs Inyecci�n Tinta (Servimex).</label></body></html>",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 30M,
                  EsInternacional = true
              },

              new Productos
              {
                  IdProducto = 1403,
                  Modelo = "3335901",
                  Nombre = "Paquete 100 DVDs Inyecci�n Tinta (Servimex)",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Paquete 100 DVDs Inyecci�n Tinta (Servimex).</label></body></html>",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio =30M,
                  EsInternacional = true
              },


              new Productos
              {
                  IdProducto = 1404,
                  Modelo = "3335902",
                  Nombre = "Paquete 1,000 CDs Catalyst 6000N",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>2x cintas para impresi�n t�rmica a color, 2 cintas transparentes para el acabado y 1,000 CD's t�rmicos.</label></body></html>",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 833M,
                  EsInternacional = true
              }, 
              new Productos
              {
                  IdProducto = 1405,
                  Modelo = "3335903",
                  Nombre = "Paquete 1,000 DVDs Catalyst 6000N",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>2x cintas para impresi�n t�rmica a color, 2 cintas transparentes para el acabado y 1,000 DVD�s t�rmicos.</label></body></html>",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 879M,
                  EsInternacional = true
              },
              new Productos
              {
                  IdProducto = 1406,
                  Modelo = "C13S020447",
                  Nombre = "Cartucho Epson Cyan",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cartucho Epson Lantana <b>Cyan</b>.</label></form></body></html><br>Compatible con: PP50 y PP-100.",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 39M,
                  EsInternacional = true
              },
              new Productos
              {
                  IdProducto = 1407,
                  Modelo = "C13S020448",
                  Nombre = "Cartucho Epson Cyan Light",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cartucho Epson Lantana <b>Cyan Light</b>.</label></body></html><br>Compatible con: PP50 y PP-100.",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 39M,
                  EsInternacional = true
              },
              new Productos
              {
                  IdProducto = 1408,
                  Modelo = "C13S020449",
                  Nombre = "Cartucho Epson Magenta Light",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cartucho Epson Lantana <b>Magenta Light</b>.</label></body></html><br>Compatible con: PP50 y PP-100.",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 39M,
                  EsInternacional = true
              },
              new Productos
              {
                  IdProducto = 1409,
                  Modelo = "C13S020450",
                  Nombre = "Cartucho Epson Magenta",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cartucho Epson Lantana <b>Magenta</b>.</label></body></html><br>Compatible con: PP50 y PP-100.",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 39M,
                  EsInternacional = true
              },
              new Productos
              {
                  IdProducto = 1410,
                  Modelo = "C13S020451",
                  Nombre = "Cartucho Epson Amarillo",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cartucho Epson Lantana <b>Amarillo</b>.</label></body></html><br>Compatible con: PP50 y PP-100.",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 39M,
                  EsInternacional = true
              },
              new Productos
              {
                  IdProducto = 1411,
                  Modelo = "C13S020452",
                  Nombre = "Cartucho Epson Negro",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cartucho Epson Lantana <b>Negro</b>.</label></body></html><br>Compatible con: PP50 y PP-100.",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 39M,
                  EsInternacional = true
              },
             new Productos
             {
                 IdProducto = 1412,
                 Modelo = "D0222",
                 Nombre = "8 x Rollos de Etiquetas Adheribles Zebra GC420 (50.8x25.4mm)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>8x Rollos Etiquetas Adheribles Impresora Zebra <b>GC420T</b> de 50.8x25.4mm. 16,800 Total Etiquetas / 2100 por Rollo.</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 200M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1413,
                 Modelo = "D0223",
                 Nombre = "12x Rollos Impresi�n Zebra GC420 Zebra GC420 (63.5mmx70m)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>12x Rollos Impresi�n Impresora Zebra <b>GC420T</b> para etiquetas adheribles de (63.5mmx70m).</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 100M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 1414,
                 Modelo = "10005851",
                 Nombre = "6x Rollos de Etiquetas Adheribles Zebra GC420 (101.6x50.8mm)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>6x Rollos de Etiquetas Adheribles (101.6x50.8mm). Transferencia T�rmica 7,920 Total Etiquetas / 1320 por Rollo.</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 98M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1415,
                 Modelo = "06000GS11007",
                 Nombre = "6x Rollos Impresi�n Zebra GC420 (110mmx70m)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>6x Rollos Tinta Impresora (110mmx70m).</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 53M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1416,
                 Modelo = "10005850",
                 Nombre = "6x Rollos de Etiqueta Adheribles Zebra GC420 (50.8x25.4mm)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>6x Rollos de Etiqueta Adheribles (50.8x25.4mm). Transferencia T�rmica 14,940 Total Etiquetas / 2490 por Rollo.</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 98M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1417,
                 Modelo = "10010063",
                 Nombre = "6x Rollos de Etiqueta Adheribles Zebra GC420 (57.2x31.8mm)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>6x Rollos de Etiqueta Adheribles (57.2x31.8mm), Ultra resistente. Transferencia T�rmica 12,000 Total Etiquetas / 2000 por Rollo.</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 98M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1418,
                 Modelo = "06000GS06407",
                 Nombre = "6x Rollos Impresi�n Zebra GC420 (64mmx70m)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>6x Rollos Tinta Impresora (64mmx70m).</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 53M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1419,
                 Modelo = "A0164",
                 Nombre = "Zebra TTP Rollo Perform 1000d 3.5",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>8x Rollos Zebra <b>TTP2030</b> 1000D 3.5 MIL RECEIPT PAPER (Consumible para D0218 Sistema de Turnos Kiosko Impresora).</label></body></html>",
                 Clasificacion = 14,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 135M,
                 EsInternacional = true
             },
             new Productos
              {
                  IdProducto = 1420,
                  Modelo = "3335204",
                  Nombre = "Unidad de CD/DVD para Robot Quemador Bravo",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Unidad de CD/DVD para Robot Quemador Bravo.</label></body></html>",
                  Clasificacion = 14,
                  Tipo = Productos.EnumProductoTipos.Producto,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 162M,
                  EsInternacional = true
              },
            new Productos
            {
                IdProducto = 1421,
                Modelo = "D0229-B",
                Nombre = "Brazaletes impresora Zebra ZD510-HC",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Brazaletes para impresi�n t�rmica con adhesivo permanente de 1''x11''para impresora Zebra <b>ZD510-HC</b>.</label></body></html>",
                Clasificacion = 14,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 371M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 1422,
                Modelo = "63722-CABEZAL4T",
                Nombre = "Cabezal de impresora robot 4100",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Cabezal de impresora robot 4100 Series</label></body></html>",
                Clasificacion = 14,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 71M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 1423,
                Modelo = "HDDCASE25BK",
                Nombre = "1 Caja protectora para discos SSD",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>1 Caja protectora para discos SSD</label></body></html>",
                Clasificacion = 14,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 6.5M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 1424,
                Modelo = "223-4678",
                Nombre = "Unidad de Disco Extra�ble USB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Unidad para almacenamiento externo de im�genes USB PowerVault RD1000 alta productividad con disco (Disaster Recovery).</label></body></html>",
                Clasificacion = 14,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 3016M,
                EsInternacional = true
            },

            #endregion

            #region "15. UPS"

            new Productos
            {
                IdProducto = 1500,
                Modelo = "INTERNET750V",
                Nombre = "No break tripplite 750v",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>NO Break TRIPPLITE 750V Modelo: INTERNET750U Marca: Tripp Litte.</label></body></html>",
                Clasificacion = 15,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 111M,
                EsInternacional = true
            },
             new Productos
             {
                 IdProducto = 1501,
                 Modelo = "SUA2200RM2U",
                 Nombre = "No Break APC 2200VA",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>NO-BREAK SMART-UPS APC 2200VA / 1980W /.</label></body></html>",
                 Clasificacion = 15,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 1100M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1502,
                 Modelo = "3333103",
                 Nombre = "No Break Tripplite 1.5 kVA",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>No Break TRIPPLITE 1.5KVA 8 contactos.</label></body></html>",
                 Clasificacion = 15,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 204M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1503,
                 Modelo = "PW9130L200 0T-XL",
                 Nombre = "UPS PW-9130 2000VA",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>UPS PW-9130 2000VA.</label></body></html>",
                 Clasificacion = 15,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 1073M,
                 EsInternacional = true
             },

            #endregion

            #region "16. MOBILIARIO"

            new Productos
            {
                IdProducto = 1600,
                Modelo = "A0103",
                Nombre = "Escritorio para estaci�n de diagn�stico DIAGRX",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Escritorio para estaci�n de diagn�stico DiagRX.</label></body></html>",
                Clasificacion = 16,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 350M,
                EsInternacional = true
            },
             new Productos
             {
                 IdProducto = 1601,
                 Modelo = "AZ-TASK",
                 Nombre = "Silla",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Silla ejecutiva ergon�mica con ruedas.</label></body></html>",
                 Clasificacion = 16,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 150M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 1602,
                 Modelo = "G0143",
                 Nombre = "Smart Rack 4 posters, 19�, 25U Tripplite",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Rack para servidores 4 POSTES 19�, 25U.</label></body></html>",
                 Clasificacion = 16,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 944M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 1603,
                 Modelo = "D0231",
                 Nombre = "Rack para PACS Enterprise PRO DELL",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Rack ENTERPRISE PRO DELL para servidores 4 POSTES.</label></body></html>",
                 Clasificacion = 16,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 2779M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1604,
                 Modelo = "D0232",
                 Nombre = "Consola para RACK PRO con Monitor y Teclado",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Consola para RACK PRO con Monitor y Teclado.</label></body></html>",
                 Clasificacion = 16,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 1345M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1605,
                 Modelo = "A0101",
                 Nombre = "Mesa chica para terminal RISX",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Mesa chica para terminal RISX.</label></body></html>",
                 Clasificacion = 16,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 180M,
                 EsInternacional = true
             },
             new Productos
             {
                 IdProducto = 1606,
                 Modelo = "D0069",
                 Nombre = "Rack para PACS ENTREPRISE",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Rack para servidores 4 POSTES 19�.</label></body></html>",
                 Clasificacion = 16,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 1800M,
                 EsInternacional = true
             },

            #endregion

            #region "19. PACS ENCORE (Crecimiento en Unidades de Almacenamiento)"

            new Productos
            {
                 IdProducto = 1900,
                 Modelo = "D0066-8TB",
                 Nombre = "Caja de almacenamiento con capacidad de 8TB",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Caja de almacenamiento</b> para incremento de la capacidad del almacenamiento <b>8TB</b>. Incluye software para crecimiento de la base existente.</label></body></html>",
                 Clasificacion = 19,
                 Tipo = Productos.EnumProductoTipos.Producto,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 15489M, 
                 EsInternacional = true,
                 DatosCrudos=8,
                 DatosUsables=6.4
            },
            new Productos
            {
                IdProducto = 1901,
                Modelo = "D0066-14TB",
                Nombre = "Caja de almacenamiento con capacidad de 14TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Caja de almacenamiento</b> para incremento de la capacidad del almacenamiento <b>14TB</b>. Incluye software para crecimiento de la base existente.</label></body></html>",
                Clasificacion = 19,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 18488M,
                EsInternacional = true,
                DatosCrudos = 14,
                DatosUsables = 11.2
            },
            new Productos
            {
                IdProducto = 1902,
                Modelo = "D0066-20TB",
                Nombre = "Caja de almacenamiento con capacidad de 20TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Caja de almacenamiento</b> para incremento de la capacidad del almacenamiento <b>20TB</b>. Incluye software para crecimiento de la base existente.</label></body></html>",
                Clasificacion = 19,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 24488M,
                EsInternacional = true,
                DatosCrudos = 20,
                DatosUsables = 16.0
            },
            new Productos
            {
                IdProducto = 1903,
                Modelo = "D0066-28TB",
                Nombre = "Caja de almacenamiento con capacidad de 28TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label><b>Caja de almacenamiento</b> para incremento de la capacidad del almacenamiento <b>28TB</b>. Incluye software para crecimiento de la base existente.</label></body></html>",
                Clasificacion = 19,
                Tipo = Productos.EnumProductoTipos.Producto,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 28488M,
                EsInternacional = true,
                DatosCrudos = 28,
                DatosUsables = 22.4
            },
            #endregion

            #region "21. LICENCIAS ADMINISTRADOR DE SERVIDORES"

            new Productos
            {
                IdProducto = 2100,
                Modelo = "SG0199-4TB",
                Nombre = "Licencia servidor SERVEX 4TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia servidor SERVEX 4TB.</label></body></html>",
                Clasificacion = 21,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 3300M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2101,
                Modelo = "SG0199-8TB",
                Nombre = "Licencia servidor SERVEX 8TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia servidor SERVEX 8TB.</label></body></html>",
                Clasificacion = 21,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 4050M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2102,
                Modelo = "SG0199-12TB",
                Nombre = "Licencia servidor SERVEX 12TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia servidor SERVEX 12TB.</label></body></html>",
                Clasificacion = 21,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 4800M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2103,
                Modelo = "SG0199-16TB",
                Nombre = "Licencia servidor SERVEX 16TB",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia servidor SERVEX 16TB.</label></body></html>",
                Clasificacion = 21,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 5550M,
                EsInternacional = true
            },
            new Productos
            {
                 IdProducto = 2104,
                 Modelo = "SG199-NUBE",
                 Nombre = "Licencia servidor Teleradiolog�a/Nube",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia servidor Teleradiolog�a/Nube.</label></body></html>",
                 Clasificacion = 21,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 263M,
                 EsInternacional = true 
            },
            new Productos
            {
                 IdProducto = 2105,
                 Modelo = "CAD-001",
                 Nombre = "Licencia de Software CadOne",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia servidor detecci�n asistida por computadora para estudios de mastograf�a. *Sin l�mite de modalidades..</label></body></html>",
                 Clasificacion = 21,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 11071M,
                 EsInternacional = true
            }, 

            #endregion

            #region "22. LICENCIAS VISOR MULTIMODALIDAD PARA RADI�LOGOS"

            new Productos
            {
                IdProducto = 2200,
                Modelo = "W-DIAGRX1",
                Nombre = "Licencia simple concurrente para visor de radiolog�a",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia simple concurrente para visor de radiolog�a multimodalidad. Incluye todas las herramientas de post-procesamiento, protocolos de carga, software para mastograf�a, sincronizaci�n de series, MPR , MIP y 3D b�sico para tomograf�a, m�dulo de reporte con plantillas y m�dulo de dictado con transcripci�n.</label></body></html>",
                Clasificacion = 22,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 3750M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2201,
                Modelo = "W-3D-VAS",
                Nombre = "Licencia concurrente de paquete Vascular Avanzado",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia concurrente de paquete <b>Vascular Avanzado.</b> Visualizaci�n de vistas en m�ltiples planos y volumen avanzado (Volume Rendering) con modelos tridimensionales, plantillas, herramientas de an�lisis vascular como segmentaci�n de vasos y detecci�n de estenosis.</label></body></html>",
                Clasificacion = 22,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 5271M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2202,
                Modelo = "W-3D-VASMA",
                Nombre = "Soporte y mantenimiento anual para paquete Vascular Avanzado",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Soporte y mantenimiento anual para paquete Vascular Avanzado posterior al primer a�o de garant�a.</label></body></html>",
                Clasificacion = 22,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 750M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2203,
                Modelo = "W-3D-NUC",
                Nombre = "Licencia concurrente de paquete Nuclear Avanzado",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia concurrente de paquete <b>Nuclear Avanzado.</b> Fusi�n de tomograf�a computada (CT) con gammagraf�a o bien tomograf�a de emisi�n de positrones (PT) utilizando diferentes paletas de colores para facilitar an�lisis funcional de los �rganos.</label></body></html>",
                Clasificacion = 22,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2181M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2204,
                Modelo = "W-3D-NUCMA",
                Nombre = "Soporte y mantenimiento anual para paquete Nuclear",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Soporte y mantenimiento anual para paquete Nuclear Avanzado posterior al 1 a�o de garant�a.</label></body></html>",
                Clasificacion = 22,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 300M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2205,
                Modelo = "W-3D-CAR",
                Nombre = "Licencia concurrente Cardio 2D",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia concurrente <b>Cardio 2D.</b> Licencia concurrente de paquete CARDIO 2D. Incluye Visualizaci�n de Electrocardiograma de 12 derivaciones, general y ambulatorio, an�lisis coronario cuantitativo (QCA), an�lisis ventricular izquierdo (LVA) y la substracci�n digital (DSA).</label></body></html>",
                Clasificacion = 22,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 4393M,
                EsInternacional = true
            },
             new Productos
             {
                 IdProducto = 2206,
                 Modelo = "W-3D-CARMA",
                 Nombre = "Licencia CARDIO 2D Soporte Anual",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia CARDIO 2D Soporte Anual. Soporte y mantenimiento anual para licencia de CARDIO 2D posterior al 1 a�o de garant�a.</label></body></html>",
                 Clasificacion = 22,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 600M,
                 EsInternacional = true
             },
              new Productos
              {
                  IdProducto = 2207,
                  Modelo = "W-3D-DEN",
                  Nombre = "Licencia concurrente Dental",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia concurrente <b>Dental</b>. Licencia concurrente de paquete DENTAL. Incluye Generaci�n autom�tica de ortopantomograf�as, vistas paraxiales, posicionamiento del canal mandibular con marcado manual del nervio, reconstrucci�n con coloreado del Volumen 3D, herramientas de segmentaci�n avanzada y visualizaci�n de planos transversales a la ortopantomograf�a, proyecci�n MIP/Average.</label></body></html>",
                  Clasificacion = 22,
                  Tipo = Productos.EnumProductoTipos.Licencia,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 2181M,
                  EsInternacional = true
              },
             new Productos
             {
                 IdProducto = 2208,
                 Modelo = "W-3D-DENMA",
                 Nombre = "Licencia DENTAL Soporte Anual",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia DENTAL Soporte Anual. Soporte y mantenimiento anual para licencia de DENTAL posterior al 1 a�o de garant�a.</label></body></html>",
                 Clasificacion = 22,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 300M,
                 EsInternacional = true
             },
              new Productos
              {
                  IdProducto = 2209,
                  Modelo = "54300-90",
                  Nombre = "Licencia concurrente de paquete Planificaci�n Preoperatoria",
                  Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia concurrente de paquete <b>Planificaci�n Preoperatoria</b> para cirujanos ortop�dicos con colecci�n de plantillas de los fabricantes m�s comunes, m�dulos de planificaci�n y herramientas digitales para mediciones, fijaci�n de pr�tesis, simulaci�n de osteotom�as y reducci�n de fracturas mediante asistentes inteligentes.</label></body></html>",
                  Clasificacion = 22,
                  Tipo = Productos.EnumProductoTipos.Licencia,
                  Moneda = Productos.EnumMonedas.USD,
                  Precio = 11544M,
                  EsInternacional = true
              },
             new Productos
             {
                 IdProducto = 2210,
                 Modelo = "54300-90MA",
                 Nombre = "Licencia Planificaci�n Preoperatoria Soporte Anual",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Soporte y mantenimiento anual para paquete Planificaci�n Preoperatoria Avanzado posterior al primer a�o de garant�a.</label></body></html>",
                 Clasificacion = 22,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 1197M,
                 EsInternacional = true
             },

            #endregion

            #region "23. LICENCIAS VISOR WEB"

             new Productos
             {
                 IdProducto = 2300,
                 Modelo = "W-SERVEX",
                 Nombre = "Licenciamiento concurrente para visor web (multiplataforma) de m�dico referente",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licenciamiento concurrente para visor web (multiplataforma) de m�dico referente. (Sin l�mite de usuarios).</label></body></html>",
                 Clasificacion = 23,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 12750M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 2301,
                 Modelo = "W-SERVEXC",
                 Nombre = "Licenciamiento concurrente para visor web en PACS CORE",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licenciamiento concurrente para visor web en PACS CORE Torre 4TB (multiplataforma) de m�dico referente.</label></body></html>",
                 Clasificacion = 23,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 3750M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 2302,
                 Modelo = "W-NAVDIAG",
                 Nombre = "Licencia concurrente para visor web de revisi�n multimodalidad",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia concurrente para visor web de revisi�n multimodalidad. Incluye todas las herramientas de post-procesamiento, m�dulo de impresi�n, m�dulo de reporte con plantillas, m�dulo de dictado con transcripci�n.</label></body></html>",
                 Clasificacion = 23,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 900,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 2303,
                 Modelo = "W-PAC",
                 Nombre = "Licenciamiento concurrente para visor web en PACS ENCORE",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia concurrente para visor web de consulta de estudios multimodalidad, para paciente. *Se recomienda usar con este producto, servidor de aplicaciones. Solo disponible para PACS ENCORE.</label></body></html>",
                 Clasificacion = 23,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 12750M,
                 EsInternacional = true
             },

            #endregion

            #region "24. LICENCIAS SISTEMA DE INFORMACI�N RADI�LOGICA"

             new Productos
             {
                 IdProducto = 2400,
                 Modelo = "G0125",
                 Nombre = "Software de Sistema de Informaci�n Radiol�gica (RISX)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Software de Sistema de Informaci�n Radiol�gica (RISX). Incluye m�dulo completo para el flujo de la administraci�n de los datos demogr�ficos del paciente, agenda, lista de estudios, administraci�n de salas, lista de trabajo del t�cnico radi�logo, lista de trabajo de estudios realizados, estad�sticas de productividad y Modality Work List.</label></body></html>",
                 Clasificacion = 24,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 11250M,
                 EsInternacional = true
             },

             new Productos
             {
                 IdProducto = 2401,
                 Modelo = "D0057-L",
                 Nombre = "Software de Sistema de Informaci�n Radiol�gica (RisX Lite)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Software de Sistema de Informaci�n Radiol�gica (RisX Lite). Incluye agenda y lista de trabajo Modality Work List. (Para PACS Core).</label></body></html>",
                 Clasificacion = 24,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 3750M,
                 EsInternacional = true
             },

            #endregion

            #region "25. LICENCIAS MODULOS EXTRA"

            new Productos
            {
                IdProducto = 2500,
                Modelo = "G0125T",
                Nombre = "M�dulo de Turnos",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Turnos.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2250M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 2501,
                Modelo = "G0125S",
                Nombre = "M�dulo de Solicitudes electr�nicas",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Solicitudes electr�nicas.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1500M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 2502,
                Modelo = "G0125QRF",
                Nombre = "M�dulo de Quir�fano",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Quir�fano.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 15000M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2503,
                Modelo = "G0125URG",
                Nombre = "M�dulo de Urgencias",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Urgencias.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 15000M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2504,
                Modelo = "G0125HSP",
                Nombre = "M�dulo de Hospitalizaci�n",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Hospitalizaci�n.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 15000M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2505,
                Modelo = "G0125CON",
                Nombre = "M�dulo de Consulta Externa",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Consulta Externa.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 15000M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2506,
                Modelo = "G0125PRP",
                Nombre = "M�dulo de Portal de Pacientes",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Portal de Pacientes.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 15000M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2507,
                Modelo = "G0125LAB",
                Nombre = "M�dulo de Laboratorio",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Laboratorio.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 15000M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2508,
                Modelo = "G0125LAB-I",
                Nombre = "M�dulo de Integraci�n Equipo Laboratorio",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Integraci�n Equipo Laboratorio.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 7500M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2509,
                Modelo = "G0125PAT",
                Nombre = "M�dulo de Patolog�a",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>M�dulo de Patolog�a.</label></body></html>",
                Clasificacion = 25,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 15000M,
                EsInternacional = true
            },

            #endregion

            #region "26.LICENCIAS ANTIVIRUS"

            new Productos
            {
                IdProducto = 2600,
                Modelo = "D0108",
                Nombre = "Licencia Admin Nube Bit Defender para estaci�n",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia Admin Nube Bit Defender para estaci�n.</label></body></html>",
                Clasificacion = 26,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 54M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2601,
                Modelo = "D0107",
                Nombre = "Licencia Admin Nube Bit Defender para Servidor",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Licencia Admin Nube Bit Defender para Servidor.</label></body></html>",
                Clasificacion = 26,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 160M,
                EsInternacional = true
            },


            #endregion

            #region "27.LICENCIAS RECONOCIMIENTO DE VOZ"

            new Productos
            {
                 IdProducto = 2700,
                 Modelo = "D2019-N1",
                 Nombre = "Licencia nominal para reconocimiento de voz VOCALI en (SERVIDOR NUBE)",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>1 Licencia <b>nominal</b> para reconocimiento de voz VOCALI en <b>(SERVIDOR NUBE)</b>. Nota: Requiere acceso a Internet</label></body></html>",
                 Clasificacion = 27,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 1099M,
                 EsInternacional = true
            },

            new Productos
            {
                IdProducto = 2701,
                Modelo = "D2019-N5",
                Nombre = "Licencias concurrentes para reconocimiento de voz VOCALI en nube",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>5 Licencias <b>concurrentes</b> para reconocimiento de voz VOCALI en <b>(SERVIDOR NUBE)</b>. Nota: Requiere acceso a Internet</label></body></html>",
                Clasificacion = 27,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 8475M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 2702,
                Modelo = "D0220-N1",
                Nombre = "Soporte anual y mantenimiento para D2019-N1",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Soporte anual y manteniminento para D0219-N1 1 licencia <b>nominal</b> de reconocimiento de voz VOCALI en <b>(SERVIDOR NUBE)</b>.</label></body></html>",
                Clasificacion = 27,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 200M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 2703,
                Modelo = "D0220-N5",
                Nombre = "Soporte anual VOCALI en nube",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Soporte anual para 5 licencias <b>concurrentes</b> de reconocimiento de voz VOCALI D2019-N5 en <b>(SERVIDOR NUBE)</b>.</label></body></html>",
                Clasificacion = 27,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1650M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 2704,
                Modelo = "D0109",
                Nombre = "5 Licencias concurrentes para reconocimiento de voz VOCALI (LOCAL)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Paquete 5 Licencias <b>concurrentes</b> para reconocimiento de voz VOCALI <b>(SERVIDOR LOCAL)</b>. Nota: Para instalaci�n de motor de reconocimiento local requiere el servidor G0199-VOZ.</label></body></html>",
                Clasificacion = 27,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 8475M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2705,
                Modelo = "D0110",
                Nombre = "Soporte anual 5x VOCALI (LOCAL)",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Soporte anual y mantenimiento para paquete 5 licencias <b>concurrentes</b> de reconocimiento de voz D0109 <b>(SERVIDOR LOCAL)</b>.</label></body></html>",
                Clasificacion = 27,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1650M,
                EsInternacional = true
            },
            new Productos
            {
                IdProducto = 2706,
                Modelo = "D0109-E1",
                Nombre = "1 Licencia para reconocimiento de voz VOCALI",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>1 Licencia <b>concurrente</b> para reconocimiento de voz VOCALI <b>(ESTACION)</b>. Nota: La instalaci�n de motor de reconocimiento se realiza en una puesto de trabajo y se puede usar por m�ltiples usuarios.</label></body></html>",
                Clasificacion = 27,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 2500M,
                EsInternacional = true
            }, 
            new Productos
            {
                IdProducto = 2707,
                Modelo = "D0110-E1",
                Nombre = "Soporte anual y mantenimiento para 1 licencias de reconocimiento de voz",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Soporte anual y mantenimiento para <b>1 licencia</b> de reconocimiento de voz D0109�E1 <b>(ESTACION)</b>.</label></body></html>",
                Clasificacion = 27,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 500M,
                EsInternacional = true
            },
            #endregion

            #region "28.SERVICIOS PROFESIONALES"

            new Productos
            {
                IdProducto = 2800,
                Modelo = "SP-PACS",
                Nombre = "Servicios profesionales para sistemas",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servicios profesionales para sistemas. * El Precio debe multiplicarse por la productividad estimada de estudios por a�o.</label></body></html>",
                Clasificacion = 28,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1M,
                EsInternacional = true
            },

            new Productos
            {
                IdProducto = 2801,
                Modelo = "SP-PACS-INT",
                Nombre = "Servicios profesionales para sistemas",
                Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servicios profesionales para integraci�n de RIS a sistema administrativo. Incluye an�lisis de BD y trabajo de desarrollo en conjunto para el intercambio de mensajes HL7 o webservice.</label></body></html>",
                Clasificacion = 28,
                Tipo = Productos.EnumProductoTipos.Licencia,
                Moneda = Productos.EnumMonedas.USD,
                Precio = 1M,
                EsInternacional = true
            },

             new Productos
             {
                 IdProducto = 2802,
                 Modelo = "SP-PACS-NAC",
                 Nombre = "Servicios profesionales para sistemas",
                 Descripcion = @"<html><body><font size='2' face='Century Gothic'><label>Servicios profesionales para integraci�n de RIS a sistema administrativo. Incluye an�lisis de BD y trabajo de desarrollo en conjunto para el intercambio de mensajes HL7 o webservice.</label></body></html>",
                 Clasificacion = 28,
                 Tipo = Productos.EnumProductoTipos.Licencia,
                 Moneda = Productos.EnumMonedas.USD,
                 Precio = 4500M,
                 EsInternacional = true
             }

             #endregion

            );
        }

        private void SeedEstrucutras(EntidadesPagina context)
        {
            context.ProductosComponentes.AddOrUpdate(

            #region "1. HARDWARE HIS"

            //G0130-1T 512 Servidor para Sistema HIS            
            new ProductosComponentes { IdProducto = 100, IdComponente = 101, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 100, IdComponente = 1501, Cantidad = 1 }, 

            #endregion

            #region "2. HARDWARE PACS CORE"

            //G0200-4TB Servidor Sistema PACS CORE 4TB
            new ProductosComponentes { IdProducto = 204, IdComponente = 201, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 204, IdComponente = 501, Cantidad = 2 },
            new ProductosComponentes { IdProducto = 204, IdComponente = 1423, Cantidad = 2 },
            new ProductosComponentes { IdProducto = 204, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 204, IdComponente = 1502, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 204, IdComponente = 705, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 204, IdComponente = 1102, Cantidad = 1 },

            //G0200-4TB-I Servidor Sistema PACS CORE 4TB INTERMEDIO
            new ProductosComponentes { IdProducto = 205, IdComponente = 202, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 205, IdComponente = 501, Cantidad = 2 },
            new ProductosComponentes { IdProducto = 205, IdComponente = 1423, Cantidad = 2 },
            new ProductosComponentes { IdProducto = 205, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 205, IdComponente = 1502, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 205, IdComponente = 705, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 205, IdComponente = 1102, Cantidad = 1 },

            //G0200-4TB-A Servidor Sistema PACS CORE 4TB AVANZADO
            new ProductosComponentes { IdProducto = 206, IdComponente = 203, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 206, IdComponente = 501, Cantidad = 2 },
            new ProductosComponentes { IdProducto = 206, IdComponente = 1423, Cantidad = 2 },
            new ProductosComponentes { IdProducto = 206, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 206, IdComponente = 1502, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 206, IdComponente = 705, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 206, IdComponente = 1102, Cantidad = 1 },

                      
            #endregion

            #region "3. HARDWARE PACS ENCORE"

            //G0199-0TB Servidor de aplicaciones ENCORE            
            new ProductosComponentes { IdProducto = 300, IdComponente = 301, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 300, IdComponente = 1501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 300, IdComponente = 705, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 300, IdComponente = 1102, Cantidad = 1 },
            //G0199-4TB Servidor de Almacenamiento ENCORE 4TB
            new ProductosComponentes { IdProducto = 302, IdComponente = 501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 302, IdComponente = 1423, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 302, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 302, IdComponente = 303, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 303, IdComponente = 305, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 302, IdComponente = 1501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 302, IdComponente = 1102, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 302, IdComponente = 705, Cantidad = 1 },
            //G0199-8TB  Servidor de Almacenamiento ENCORE 8TB
            new ProductosComponentes { IdProducto = 306, IdComponente = 501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 306, IdComponente = 1423, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 306, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 306, IdComponente = 307, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 307, IdComponente = 305, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 307, IdComponente = 500, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 306, IdComponente = 1501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 306, IdComponente = 1102, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 306, IdComponente = 705, Cantidad = 1 },
            //G0199-12TB Servidor de Almacenamiento ENCORE 12TB
            new ProductosComponentes { IdProducto = 308, IdComponente = 501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 308, IdComponente = 1423, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 308, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 308, IdComponente = 309, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 309, IdComponente = 305, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 309, IdComponente = 500, Cantidad = 2 },
            new ProductosComponentes { IdProducto = 308, IdComponente = 1501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 308, IdComponente = 1102, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 308, IdComponente = 705, Cantidad = 1 },
            //G0199-12TBPRO Servidor de Almacenamiento ENCORE PRO 12TB
            new ProductosComponentes { IdProducto = 310, IdComponente = 501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 310, IdComponente = 1423, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 310, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 310, IdComponente = 311, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 311, IdComponente = 304, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 311, IdComponente = 500, Cantidad = 2 },
            new ProductosComponentes { IdProducto = 310, IdComponente = 1501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 310, IdComponente = 1102, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 310, IdComponente = 705, Cantidad = 1 },
            //G0199-16TB" Servidor de Almacenamiento ENCORE 16TB
            new ProductosComponentes { IdProducto = 313, IdComponente = 501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 313, IdComponente = 1423, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 313, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 313, IdComponente = 314, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 314, IdComponente = 305, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 314, IdComponente = 500, Cantidad = 3 },
            new ProductosComponentes { IdProducto = 313, IdComponente = 1501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 313, IdComponente = 1102, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 313, IdComponente = 705, Cantidad = 1 },
            //G0199-16TB PRO Servidor de Almacenamiento ENCORE PRO 16T
            new ProductosComponentes { IdProducto = 315, IdComponente = 501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 315, IdComponente = 1423, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 315, IdComponente = 1105, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 315, IdComponente = 316, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 316, IdComponente = 304, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 316, IdComponente = 500, Cantidad = 3 },
            new ProductosComponentes { IdProducto = 315, IdComponente = 1501, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 315, IdComponente = 1102, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 315, IdComponente = 705, Cantidad = 1 },
            
            #endregion

            #region "4. HARDWARE PARA ESTACIONES"

            

             //D0068-H Estaci�n de Diagn�stico DiagRX SMART (400)  - 412
             new ProductosComponentes { IdProducto = 400, IdComponente = 412, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 400, IdComponente = 1500, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 400, IdComponente = 704, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 400, IdComponente = 1101, Cantidad = 1 },

             //D0054-H Estaci�n de Diagn�stico DiagRX Alfa (401) - 410
             new ProductosComponentes { IdProducto = 401, IdComponente = 410, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 401, IdComponente = 1502, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 401, IdComponente = 1101, Cantidad = 1 },

             //D0051H2 Estaci�n de Diagn�stico DiagRX s / tarjeta vide (402)  - 414
             new ProductosComponentes { IdProducto = 402, IdComponente = 414, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 402, IdComponente = 1502, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 402, IdComponente = 1101, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 402, IdComponente = 704, Cantidad = 1 },

             //D0051H3 Estaci�n de Diagn�stico DiagRX s / tarjeta videO (403)
             new ProductosComponentes { IdProducto = 403, IdComponente = 416, Cantidad = 1 },             
             new ProductosComponentes { IdProducto = 403, IdComponente = 704, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 403, IdComponente = 1101, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 403, IdComponente = 1502, Cantidad = 1 },                                       

             //D0061-H Estaci�n de Diagn�stico DiagRX ALFA MASTO (404)  - 414
             new ProductosComponentes {  IdProducto = 404, IdComponente = 414, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 404, IdComponente = 1502, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 404, IdComponente = 1101, Cantidad = 1 },
             
            #endregion

            #region "9. PUBLICADORES"

            //D0060 Robot Quemador Lite 20 CDS Rimage
             new ProductosComponentes { IdProducto = 901, IdComponente = 900, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 901, IdComponente = 902, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 901, IdComponente = 903, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 901, IdComponente = 1402, Cantidad = 6 },
             new ProductosComponentes { IdProducto = 901, IdComponente = 1500, Cantidad = 1 },

             //D0074-1T Robot Quemador Lite 100 CD / DVD Rimage
             new ProductosComponentes { IdProducto = 904, IdComponente = 900, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 904, IdComponente = 905, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 904, IdComponente = 906, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 904, IdComponente = 1402, Cantidad = 6 },
             new ProductosComponentes { IdProducto = 904, IdComponente = 1500, Cantidad = 1 },

             //D0060-R Publicador Rimage  20 discos (CD/DVD).
             new ProductosComponentes { IdProducto = 911, IdComponente = 900, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 911, IdComponente = 902, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 911, IdComponente = 906, Cantidad = 2 },
             new ProductosComponentes { IdProducto = 911, IdComponente = 1401, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 911, IdComponente = 1402, Cantidad = 6 },
             new ProductosComponentes { IdProducto = 911, IdComponente = 1500, Cantidad = 1 },

             //D0060-50E Publicador Epson 50 discos (CD/DVD).
             new ProductosComponentes { IdProducto = 912, IdComponente = 900, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 912, IdComponente = 909, Cantidad = 1 }, 
             new ProductosComponentes { IdProducto = 912, IdComponente = 1402, Cantidad = 6 },
             new ProductosComponentes { IdProducto = 912, IdComponente = 1406, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 912, IdComponente = 1407, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 912, IdComponente = 1408, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 912, IdComponente = 1409, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 912, IdComponente = 1410, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 912, IdComponente = 1411, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 912, IdComponente = 1500, Cantidad = 1 },

             //D0074-1T-R Publicador Rimage 100 discos (CD/DVD).
             new ProductosComponentes { IdProducto = 913, IdComponente = 900, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 913, IdComponente = 905, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 913, IdComponente = 906, Cantidad = 2 },
             new ProductosComponentes { IdProducto = 913, IdComponente = 1401, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 913, IdComponente = 1402, Cantidad = 6 }, 
             new ProductosComponentes { IdProducto = 913, IdComponente = 1500, Cantidad = 1 },

             //D0074-1T-E Publicador Epson 100 discos (CD/DVD)
             new ProductosComponentes { IdProducto = 914, IdComponente = 900, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 910, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 1402, Cantidad = 6 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 1406, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 1407, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 1408, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 1409, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 1410, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 1411, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 914, IdComponente = 1500, Cantidad = 1 },

             //D0074-1T-150R Publicador Rimage 150 discos (CD/DVD)
             new ProductosComponentes { IdProducto = 915, IdComponente = 907, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 915, IdComponente = 908, Cantidad = 1 }, 
             new ProductosComponentes { IdProducto = 915, IdComponente = 1404, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 915, IdComponente = 1405, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 915, IdComponente = 1500, Cantidad = 1 },

            #endregion

            #region "10. SISTEMAS DE TURNOS"

           

            //D0216 Sistema Turnos Kiosko Impresora
            new ProductosComponentes { IdProducto = 1001, IdComponente = 700, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1001, IdComponente = 1002, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1001, IdComponente = 1003, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1001, IdComponente = 1419, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1001, IdComponente = 1500, Cantidad = 1 },

            //D0218 Sistema Turnos Kiosko Lector
            new ProductosComponentes { IdProducto = 1004, IdComponente = 700, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1004, IdComponente = 1003, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1004, IdComponente = 1303, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1004, IdComponente = 1304, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1004, IdComponente = 1500, Cantidad = 1 },

            //D0217 Sistema Turnos Controlador TV
            new ProductosComponentes { IdProducto = 1008, IdComponente = 1009, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1008, IdComponente = 1010, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1008, IdComponente = 1011, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1008, IdComponente = 1012, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1008, IdComponente = 701, Cantidad = 1 },
            new ProductosComponentes { IdProducto = 1008, IdComponente = 1500, Cantidad = 1 },

             //D0090 Sistema Turnos Controlador TV Simple
             new ProductosComponentes { IdProducto = 1013, IdComponente = 1009, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1013, IdComponente = 1010, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1013, IdComponente = 1500, Cantidad = 1 },

             //D0221 Impresora Zebra GC420
             new ProductosComponentes { IdProducto = 1300, IdComponente = 1301, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1300, IdComponente = 1103, Cantidad = 1 },             

             //D0227 Sistema Turnos Terminal T�cnico Pared
             new ProductosComponentes { IdProducto = 1014, IdComponente = 1009, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1014, IdComponente = 1012, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1014, IdComponente = 701, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1014, IdComponente = 1015, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1014, IdComponente = 1500, Cantidad = 1 },

             //D0228 Sistema Turnos Terminal T�cnico Mesa
             new ProductosComponentes { IdProducto = 1016, IdComponente = 1009, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1016, IdComponente = 1012, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1016, IdComponente = 701, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1016, IdComponente = 1017, Cantidad = 1 },
             new ProductosComponentes { IdProducto = 1016, IdComponente = 1500, Cantidad = 1 },

             #endregion

            #region "12. TERMINALES ADICIONALES"

              new ProductosComponentes { IdProducto = 1200, IdComponente = 1202, Cantidad = 1 },
              new ProductosComponentes { IdProducto = 1200, IdComponente = 1500, Cantidad = 1 },
              new ProductosComponentes { IdProducto = 1200, IdComponente = 704, Cantidad = 1 },

              new ProductosComponentes { IdProducto = 1201, IdComponente = 1203, Cantidad = 1 },
              new ProductosComponentes { IdProducto = 1201, IdComponente = 1500, Cantidad = 1 }

            #endregion


            );
        }

        private void SeedListasPrecio(EntidadesPagina context)
        {
            context.ListasPrecios.AddOrUpdate(
                new ListasPrecios { IdLista = 1, Codigo = "LB", Nombre = "Lista Base", Descripcion = "Lista Base", EsBase = true ,Iva = true},
                new ListasPrecios { IdLista = 2, Codigo = "LN", Nombre = "Lista Nacional", Descripcion = "Lista Nacional", EsBase = false, IdListaReferencia = 1, Iva = true },
                new ListasPrecios { IdLista = 3, Codigo = "LI", Nombre = "Lista Internacional", Descripcion = "Lista Internacional", EsBase = false, IdListaReferencia = 1, Iva = false },
                new ListasPrecios { IdLista = 4, Codigo = "LS", Nombre = "Lista Servicio", Descripcion = "Lista Servicio", EsBase = false, IdListaReferencia = 1 , Iva = true }
            );

            context.ListasPrecioProductos.AddOrUpdate(

            #region "HARDWARE HIS"

            #region "Lista Base"
                new ListasPrecioProductos { IdLista = 1, IdProducto = 100 },                
            #endregion

            #region "Lista Nacional"
                new ListasPrecioProductos { IdLista = 2, IdProducto = 100 },
            #endregion

            #region "Lista Internacional"
                new ListasPrecioProductos { IdLista = 3, IdProducto = 100 },
            #endregion

            #region "Lista Servicio"
                new ListasPrecioProductos { IdLista = 4, IdProducto = 100 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 101 },
            #endregion

            #endregion

            #region "HARDWARE PACS CORE"

            #region "Lista Base"

                new ListasPrecioProductos { IdLista = 1, IdProducto = 204 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 205 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 206 },

            #endregion

            #region "Lista Nacional"
            
                new ListasPrecioProductos { IdLista = 2, IdProducto = 204 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 205 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 206 },
            
            #endregion

            #region "Lista Internacional"
                
                new ListasPrecioProductos { IdLista = 3, IdProducto = 204 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 205 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 206 },
            
            #endregion

            #region "Lista Servicio"
               
                new ListasPrecioProductos { IdLista = 4, IdProducto = 201 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 202 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 203 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 204 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 205 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 206 },

            #endregion

            #endregion

            #region "HARDWARE PACS ENCORE"

            #region "Lista Base"

                new ListasPrecioProductos { IdLista = 1, IdProducto = 300 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 302 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 306 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 308 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 310 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 313 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 315 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 317 },                

                new ListasPrecioProductos { IdLista = 1, IdProducto = 2100 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2101 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2102 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2103 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2104 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2105 },

                new ListasPrecioProductos { IdLista = 1, IdProducto = 2200 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2201 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2202 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2203 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2204 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2205 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2206 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2207 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2208 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2209 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2210 },

                new ListasPrecioProductos { IdLista = 1, IdProducto = 2300 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2301 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2302 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2303 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2400 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2401 },

                new ListasPrecioProductos { IdLista = 1, IdProducto = 2500 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2501 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2502 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2503 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2504 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2505 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2506 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2507 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2508 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2509 },

                new ListasPrecioProductos { IdLista = 1, IdProducto = 2700 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2701 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2702 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2703 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2704 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2705 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2706 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 2707 },

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 300 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 302 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 306 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 308 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 310 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 313 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 315 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 317 },  

                new ListasPrecioProductos { IdLista = 2, IdProducto = 2100 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2101 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2102 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2103 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2104 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2105 },

                new ListasPrecioProductos { IdLista = 2, IdProducto = 2200 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2201 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2202 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2203 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2204 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2205 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2206 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2207 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2208 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2209 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2210 },

                new ListasPrecioProductos { IdLista = 2, IdProducto = 2300 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2301 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2302 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2303 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2400 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2401 },

                new ListasPrecioProductos { IdLista = 2, IdProducto = 2500 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2501 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2502 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2503 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2504 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2505 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2506 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2507 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2508 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2509 },

                new ListasPrecioProductos { IdLista = 2, IdProducto = 2700 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2701 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2702 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2703 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2704 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2705 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2706 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2707 },

                new ListasPrecioProductos { IdLista = 2, IdProducto = 2600 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2601 },

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 300 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 302 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 306 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 308 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 310 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 313 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 315 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 317 },  

                new ListasPrecioProductos { IdLista = 3, IdProducto = 2100 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2101 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2102 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2103 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2104 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2105 },

                new ListasPrecioProductos { IdLista = 3, IdProducto = 2200 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2201 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2202 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2203 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2204 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2205 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2206 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2207 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2208 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2209 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2210 },

                new ListasPrecioProductos { IdLista = 3, IdProducto = 2300 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2301 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2302 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2303 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2400 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2401 },

                new ListasPrecioProductos { IdLista = 3, IdProducto = 2500 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2501 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2502 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2503 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2504 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2505 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2506 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2507 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2508 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2509 },

                new ListasPrecioProductos { IdLista = 3, IdProducto = 2700 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2701 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2702 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2703 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2704 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2705 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2706 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2707 },

                new ListasPrecioProductos { IdLista = 3, IdProducto = 2600 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2601 },

            #endregion

            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 300 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 301 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 302 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 303 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 304 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 305 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 306 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 307 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 308 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 309 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 310 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 311 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 313 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 314 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 315 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 316 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 317 }, 

                new ListasPrecioProductos { IdLista = 4, IdProducto = 2100 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2101 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2102 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2103 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2104 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2105 },

                new ListasPrecioProductos { IdLista = 4, IdProducto = 2200 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2201 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2202 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2203 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2204 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2205 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2206 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2207 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2208 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2209 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2210 },

                new ListasPrecioProductos { IdLista = 4, IdProducto = 2300 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2301 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2302 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2303 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2400 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2401 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2500 },

                new ListasPrecioProductos { IdLista = 4, IdProducto = 2501 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2502 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2503 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2504 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2505 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2506 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2507 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2508 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2509 },

                new ListasPrecioProductos { IdLista = 4, IdProducto = 2600 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2601 },

                new ListasPrecioProductos { IdLista = 4, IdProducto = 2700 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2701 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2702 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2703 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2704 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2705 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2706 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2707 },

            #endregion

            #endregion

            #region "HARDWARE PACS ENCORE (Crecimiento en Unidades de Almacenamiento)"
               
            #region "Lista Base"
                
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1900 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1901 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1902 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1903 },

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 1900 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1901 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1902 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1903 },

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 1900 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1901 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1902 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1903 },

            #endregion

            #region "Lista Servicio"
                
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1900 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1901 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1902 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1903 },

            #endregion

            #endregion

            #region "HARDWARE PARA ESTACIONES"

            #region "Lista Base"
              
                new ListasPrecioProductos { IdLista = 1, IdProducto = 400 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 401 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 402 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 403 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 404 },

                new ListasPrecioProductos { IdLista = 1, IdProducto = 600 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 601 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 602 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 603 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 604 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 605 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 606 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 607 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 608 },  
                new ListasPrecioProductos { IdLista = 1, IdProducto = 609 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 611 },
                 
                new ListasPrecioProductos { IdLista = 1, IdProducto = 613 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 614 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 615 },  

                new ListasPrecioProductos { IdLista = 1, IdProducto = 703 },                

            #endregion
            
            #region "Lista Nacional"
                                                 
                new ListasPrecioProductos { IdLista = 2, IdProducto = 400 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 401 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 402 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 403 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 404 },

                new ListasPrecioProductos { IdLista = 2, IdProducto = 600 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 601 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 602 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 603 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 604 },                                
                new ListasPrecioProductos { IdLista = 2, IdProducto = 605 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 606 },  
                new ListasPrecioProductos { IdLista = 2, IdProducto = 607 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 608 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 609 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 611 },
                 
                new ListasPrecioProductos { IdLista = 2, IdProducto = 613 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 614 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 615 },
                
                
                new ListasPrecioProductos { IdLista = 2, IdProducto = 703 },

            #endregion

            #region "Lista Internacional"
                                                
                new ListasPrecioProductos { IdLista = 3, IdProducto = 400 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 401 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 402 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 403 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 404 },

                new ListasPrecioProductos { IdLista = 3, IdProducto = 600 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 601 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 602 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 603 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 604 },                                
                new ListasPrecioProductos { IdLista = 3, IdProducto = 605 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 606 },
              
                new ListasPrecioProductos { IdLista = 3, IdProducto = 607 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 608 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 609 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 611 },
                 
                new ListasPrecioProductos { IdLista = 3, IdProducto = 613 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 614 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 615 },     
           
                new ListasPrecioProductos { IdLista = 3, IdProducto = 703 },

            #endregion

            #region "Lista Servicio"


                new ListasPrecioProductos { IdLista = 4, IdProducto = 400 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 401 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 402 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 403 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 404 },               
                new ListasPrecioProductos { IdLista = 4, IdProducto = 410 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 412 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 414 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 416 },

                new ListasPrecioProductos { IdLista = 4, IdProducto = 600 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 601 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 602 },                
                new ListasPrecioProductos { IdLista = 4, IdProducto = 603 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 604 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 605 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 606 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 607 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 608 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 609 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 611 },
                 
                new ListasPrecioProductos { IdLista = 4, IdProducto = 613 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 614 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 615 },                

                new ListasPrecioProductos { IdLista = 4, IdProducto = 700 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 701 }, 
                new ListasPrecioProductos { IdLista = 4, IdProducto = 703 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 704 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 705 },

            #endregion

            #endregion
            
            #region "TERMINALES ADICIONALES"

            #region "Lista Base"

                new ListasPrecioProductos { IdLista = 1, IdProducto = 1200 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1201 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1204 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1205 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1206 }, 

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 1200 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1201 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1204 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1205 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1206 }, 

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 1200 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1201 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1204 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1205 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1206 }, 

            #endregion

            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 1200 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1201 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1202 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1203 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1204 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1205 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1206 }, 

            #endregion

            #endregion

            #region "PUBLICADORES"

            #region "Lista Base"

                new ListasPrecioProductos { IdLista = 1, IdProducto = 911 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 912 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 913 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 914 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 915 },

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 911 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 912 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 913 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 914 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 915 },

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 911 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 912 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 913 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 914 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 915 },

            #endregion

            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 900 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 901 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 902 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 903 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 904 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 905 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 906 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 907 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 908 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 909 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 910 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 911 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 912 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 913 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 914 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 915 },

            #endregion

            #endregion

            #region "DISPOSITIVOS EXTERNOS"

            #region "Lista Base"

                new ListasPrecioProductos { IdLista = 1, IdProducto = 1302 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1303 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1304 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1305 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1306 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1307 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1309 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1310 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1311 }, 

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 1302 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1303 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1304 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1305 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1306 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1307 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1309 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1310 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1311 },

            #endregion

            #region "Lista Internacional"
                
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1302 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1303 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1304 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1305 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1306 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1307 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1309 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1310 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1311 }, 

            #endregion

            #region "Lista Servicio"
             
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1301 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1302 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1303 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1304 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1305 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1306 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1307 }, 
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1309 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1310 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1311 },

            #endregion

            #endregion

            #region "CONSUMIBLES"

            #region "Lista Base"
                
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1400 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1401 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1402 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1403 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1404 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1405 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1406 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1407 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1408 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1409 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1410 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1411 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1414 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1415 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1416 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1417 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1418 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1420 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1421 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1422 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1423 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1424 },

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 1400 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1401 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1402 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1403 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1404 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1405 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1406 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1407 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1408 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1409 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1410 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1411 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1414 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1415 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1416 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1417 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1418 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1420 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1421 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1423 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1424 },

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 1400 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1401 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1402 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1403 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1404 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1405 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1406 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1407 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1408 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1409 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1410 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1411 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1414 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1415 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1416 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1417 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1418 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1420 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1421 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1423 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1424 },

            #endregion

            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 1400 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1401 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1402 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1403 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1404 }, 
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1405 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1406 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1407 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1408 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1409 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1410 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1411 }, 
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1414 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1415 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1416 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1417 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1418 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1420 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1421 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1422 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1423 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1424 },

            #endregion

            #endregion

            #region "SISTEMAS DE TURNOS"

            #region "Lista Base"

                new ListasPrecioProductos { IdLista = 1, IdProducto = 1000 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1001 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1014 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1016 }, 
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1004 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1008 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1013 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1300 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1412 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1413 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1419 },

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 1000 }, 
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1001 }, 
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1014 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1016 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1004 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1008 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1013 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1300 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1412 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1413 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1419 },

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 1000 }, 
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1001 }, 
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1014 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1016 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1004 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1008 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1013 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1300 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1412 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1413 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1419 },

            #endregion

            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 1000 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1001 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1002 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1003 }, 
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1004 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1008 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1009 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1010 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1011 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1012 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1013 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1014 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1016 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1015 },                
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1017 },
                
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1300 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1412 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1413 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1419 },

            #endregion

            #endregion                 

            #region "UPS"

            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 1500 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1501 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1502 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1503 },

            #endregion

            #endregion

            #region "DISCOS DUROS"

            #region "Lista Base"
                 
                new ListasPrecioProductos { IdLista = 1, IdProducto = 501 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 502 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 503 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 504 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 505 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 506 },

            #endregion

            #region "Lista Nacional"
                 
                new ListasPrecioProductos { IdLista = 2, IdProducto = 501 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 502 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 503 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 504 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 505 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 506 },
            #endregion

            #region "Lista Internacional"
                 
                new ListasPrecioProductos { IdLista = 3, IdProducto = 501 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 502 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 503 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 504 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 505 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 506 },

            #endregion

            #region "Lista Servicio"
                 
                new ListasPrecioProductos { IdLista = 4, IdProducto = 501 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 502 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 503 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 504 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 505 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 506 },

            #endregion

            #endregion

            #region "ACCESORIOS"

            #region "Lista Base"

                new ListasPrecioProductos { IdLista = 1, IdProducto = 1100 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1106 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1107 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1108 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1109 },

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 1100 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1106 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1107 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1108 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1109 },

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 1100 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1106 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1107 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1108 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1109 },

            #endregion

            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 1100 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1101 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1102 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1103 },                
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1105 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1106 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1107 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1108 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1109 },

            #endregion

            #endregion

            #region "MOBILIARIO"

            #region "Lista Base"

                new ListasPrecioProductos { IdLista = 1, IdProducto = 1600 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1601 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1602 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1603 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1604 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1605 },
                new ListasPrecioProductos { IdLista = 1, IdProducto = 1606 },                

            #endregion

            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 1600 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1601 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1602 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1603 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1604 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1605 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 1606 },

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 1600 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1601 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1602 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1603 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1604 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1605 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 1606 },

            #endregion

            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 1600 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1601 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1602 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1603 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1604 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1605 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 1606 },

            #endregion

            #endregion

            #region "SERVICIOS PROFESIONALES"
            
            #region "Lista Nacional"

                new ListasPrecioProductos { IdLista = 2, IdProducto = 2800 },
                new ListasPrecioProductos { IdLista = 2, IdProducto = 2802 },

            #endregion

            #region "Lista Internacional"

                new ListasPrecioProductos { IdLista = 3, IdProducto = 2800 },
                new ListasPrecioProductos { IdLista = 3, IdProducto = 2801 },

            #endregion
            
            #region "Lista Servicio"

                new ListasPrecioProductos { IdLista = 4, IdProducto = 2800 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2801 },
                new ListasPrecioProductos { IdLista = 4, IdProducto = 2802 }

            #endregion

            #endregion

            );
        }

        private void SeedRutas(EntidadesPagina context)
        {
            context.Rutas.AddOrUpdate(
               p => p.Tipo,

               new Rutas
               {
                   Tipo = Rutas.EnumRutaTipo.Bitacoras,
                   Descripcion = Rutas.EnumRutaTipo.Bitacoras.ToString(),
                   Directorio = @"C:\PaginaWeb\Logs\"
               },
               new Rutas
               {
                   Tipo = Rutas.EnumRutaTipo.Documentos,
                   Descripcion = Rutas.EnumRutaTipo.Documentos.ToString(),
                   Directorio = @"C:\PaginaWeb\Docs\"
               },
               new Rutas
               {
                   Tipo = Rutas.EnumRutaTipo.Aplicaciones,
                   Descripcion = Rutas.EnumRutaTipo.Aplicaciones.ToString(),
                   Directorio = @"C:\PaginaWeb\Aplicaciones\"
               },
                new Rutas
                {
                    Tipo = Rutas.EnumRutaTipo.Temporales,
                    Descripcion = Rutas.EnumRutaTipo.Temporales.ToString(),
                    Directorio = @"C:\PaginaWeb\Temp\"
                }
            );
        }

        private void SeedVistas(EntidadesPagina context)
        {
            context.Vistas.AddOrUpdate(
                p => p.IdVista,

                new Vistas
                {
                    Nombre = "Archivos",
                    Clave = "ARCHIVOS"
                },
                new Vistas
                {
                    Nombre = "Cotizaciones",
                    Clave = "COTIZACIONES"
                },
                
                new Vistas
                {
                    Nombre = "Productos",
                    Clave = "PRODUCTOS"
                },
                new Vistas
                {
                    Nombre = "Versiones",
                    Clave = "VERSIONES"
                },
                new Vistas
                {
                    Nombre = "Configuracion",
                    Clave = "CONFIGURACION"
                },
                 new Vistas
                 {
                     Nombre = "Instalaciones",
                     Clave = "INSTALACIONES"
                 }
            );
        }

        private void SeedPermisos(EntidadesPagina context)
        {
            context.Permisos.AddOrUpdate(
                x=>x.IdPermiso,
                new Permisos()
                {
                    Nombre = "Documentos Modificar",
                    Descripcion = "Este permiso permite agregar o eliminar archivos ",
                    Clave = "ARCHIVO_DOCUMENTOS_MODIFICAR"
                },
                new Permisos()
                {
                    Nombre = "Listas de Precios Imprimir",
                    Descripcion = "Este permiso permite imprimir listas de precios",
                    Clave = "GENERAL_LISTAPRECIOS_IMPRIMIR"
                },
                new Permisos()
                {
                    Nombre = "Listas de Precio Nacional Acceso",
                    Descripcion = "Este permiso permite visualizar y hacer operaciones con la lista de precios nacional",
                    Clave = "GENERAL_LISTAPRECIOS_NACIONAL"
                },
                new Permisos()
                {
                    Nombre = "Listas de Precios Internacional Acceso",
                    Descripcion = "Este permiso permite visualizar y hacer operaciones con la lista de precios internacional",
                    Clave = "GENERAL_LISTAPRECIOS_INTERNACIONAL"
                },
                new Permisos()
                {
                    Nombre = "Listas de Precios Servicio Acceso",
                    Descripcion = "Este permiso permite visualizar y hacer operaciones con la lista de precios servicio",
                    Clave = "GENERAL_LISTAPRECIOS_SERVICIO"
                },
                new Permisos()
                {
                    Nombre = "Listas de Precios Otras Acceso",
                    Descripcion = "Este permiso permite visualizar y hacer operaciones con la listas de precios generadas por el sistema",
                    Clave = "GENERAL_LISTAPRECIOS_OTRAS"
                },
                new Permisos()
                {
                    Nombre = "Imprimir existencias",
                    Descripcion = "Este permiso imprimir las existencias en las secciones de cotizaciones y productos",
                    Clave = "GENERAL_EXISTENCIAS_IMPRIMIR"
                },
                new Permisos()
                {
                    Nombre = "Crear instalaciones",
                    Descripcion = "Este permiso permite crear nuevas instalaciones",
                    Clave = "INSTALACIONES_CREAR"
                },
                new Permisos()
                {
                    Nombre = "Editar instalaciones",
                    Descripcion = "Este permiso permite editar instalaciones",
                    Clave = "INSTALACIONES_EDITAR"
                },
                new Permisos()
                {
                    Nombre = "Crear reportes",
                    Descripcion = "Este permiso permite crear nuevos reportes",
                    Clave = "REPORTES_CREAR"
                },
                new Permisos()
                {
                    Nombre = "Importar reportes",
                    Descripcion = "Este permiso permite importar reportes de archivos excel",
                    Clave = "REPORTES_IMPORTAR"
                },
                //new Permisos()
                //{
                //    Nombre = "Ver reportes propios",
                //    Descripcion = "Este permiso permite ver los reportes iniciados por uno mismo",
                //    Clave = "REPORTES_VER_PROPIOS"
                //},
                new Permisos()
                {
                    Nombre = "Ver reportes de terceros",
                    Descripcion = "Este permiso permite ver los reportes iniciados por terceros",
                    Clave = "REPORTES_VER_TERCEROS"
                },
                //new Permisos()
                //{
                //    Nombre = "Comentar reportes propios",
                //    Descripcion = "Este permiso permite comentar en los reportes iniciados por uno mismo",
                //    Clave = "REPORTES_COMENTAR_PROPIOS"
                //},
                new Permisos()
                {
                    Nombre = "Comentar en reportes de terceros",
                    Descripcion = "Este permiso permite comentar en los reportes iniciados por terceros",
                    Clave = "REPORTES_COMENTAR_TERCEROS"
                },
                new Permisos()
                {
                    Nombre = "Modificar reporte: TERMINADO,NORESUELTO",
                    Descripcion = "Este permiso permite cambiar el estado de un reporte a TERMINADO y NO RESUELTO",
                    Clave = "REPORTES_CAMBIAR_TERMINADO_NORESUELTO"
                },
                new Permisos()
                {
                    Nombre = "Modificar reporte: PENDIENTE,REPORTADO,CANCELADO",
                    Descripcion = "Este permiso permite cambiar el estado de un reporte a PENDIENTE, REPORTADO y CANCELADO",
                    Clave = "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO"
                },
                new Permisos()
                {
                    Nombre = "Modificar reporte: PORVALIDAR",
                    Descripcion = "Este permiso permite cambiar el estado de un reporte a POR VALIDAR",
                    Clave = "REPORTES_CAMBIAR_PORVALIDAR"
                },
                new Permisos()
                {
                    Nombre = "Modificar reporte: ENPROCESO",
                    Descripcion = "Este permiso permite cambiar el estado de un reporte a EN PROCESO",
                    Clave = "REPORTES_CAMBIAR_ENPROCESO"
                },
                new Permisos()
                {
                    Nombre = "Configuraci�n",
                    Descripcion = "Este permiso permite configurar el sistema",
                    Clave = "INSTALACIONES_CONFIGURACION"
                },
                new Permisos()
                {
                    Nombre = "Importar reportes",
                    Descripcion = "Este permiso permite importar documentos con reportes existentes",
                    Clave = "REPORTES_IMPORTAR"
                }
            );
        }

        private void SeedUsuariosVistasPermisos(EntidadesPagina context)
        {
            context.SaveChanges();

            foreach (var usuario in context.Usuarios)
            {
                var vistas = context.Vistas.ToList();
                var permisos = context.Permisos.ToList();

                if (usuario != null)
                {
                    foreach (var v in vistas)
                    {
                        usuario.Vistas.Add(v);
                    }

                    foreach (var p in permisos)
                    {
                        if(usuario.Tipo == Usuarios.EnumUsuarioTipo.Administrador)
                        {
                            usuario.Permisos.Add(p);
                        }
                        if(usuario.Tipo == Usuarios.EnumUsuarioTipo.Jefe)
                        {
                            if(p.Clave == "REPORTES_CREAR")
                                usuario.Permisos.Add(p);
                            if (p.Clave == "REPORTES_VER_TERCEROS")
                                usuario.Permisos.Add(p);
                            if (p.Clave == "REPORTES_CREAR")
                                usuario.Permisos.Add(p);
                            if (p.Clave == "REPORTES_COMENTAR_TERCEROS")
                                usuario.Permisos.Add(p);
                            if (p.Clave == "REPORTES_CAMBIAR_TERMINADO_NORESUELTO")
                                usuario.Permisos.Add(p);
                            if (p.Clave == "REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO")
                                usuario.Permisos.Add(p);
                            if (p.Clave == "REPORTES_IMPORTAR")
                                usuario.Permisos.Add(p);                            
                        }
                        if(usuario.Tipo == Usuarios.EnumUsuarioTipo.Servicio)
                        {
                            if (p.Clave == "REPORTES_CREAR")
                                usuario.Permisos.Add(p);
                        }
                        if(usuario.Tipo == Usuarios.EnumUsuarioTipo.Desarrollo)
                        {

                        }                        
                    }
                }
            }

        }

        private void SeedAplicaciones(EntidadesPagina context)
        {
            context.Aplicaciones.AddOrUpdate(
                u => u.IdAplicacion,
                new Aplicaciones { IdAplicacion = 1, TAG = "ARIX", Nombre = "ArixDRF", Descripcion = "Aplicaci�n para la captura de im�genes Radiogr�ficas y Fluroscopicas con opciones avanzadas de procesamientos.", Icono = "icon-arixdrf", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 2, TAG = "ARAD", Nombre = "ArixRAD", Descripcion = "Aplicaci�n para la captura de im�genes Radiogr�ficas con opciones avanzadas de procesamientos.", Icono = "icon-arixrad", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 3, TAG = "PND", Nombre = "PacsND", Descripcion = "Visualizador WEB XBAP para el postprocesamiento de im�genes y generaci�n de reportes remotos.", Icono = "icon-pacsND", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 4, TAG = "PQR", Nombre = "PacsQR", Descripcion = "Servicio Servidor Dicom (QR SCP)  para la consulta y recuperaci�n de clientes de im�genes del PACS.", Icono = "icon-pacsQR", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 5, TAG = "PACS", Nombre = "PacsST", Descripcion = "Servicio Servidor Dicom (SEND SCP) para el almacenamiento de im�genes en el PACS.", Icono = "icon-pacsST", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 6, TAG = "PWS", Nombre = "PacsWS", Descripcion = "Estaci�n de trabajo de r�diologos para el postprocesamiento avanzados de im�genes y generaci�n de reportes remotos.", Icono = "icon-pacsWD", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 7, TAG = "PWP", Nombre = "PacsWP", Descripcion = "Aplicaci�n para generaci�n de CD/DVD por medio de Robots Publicadores.", Icono = "icon-pacsWP", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 8, TAG = "WDU", Nombre = "WDUltimate", Descripcion = "Visualizador WEB HTML5 para el postprocesamiento de im�genes y generaci�n de reportes remotos.", Icono = "icon-WDultimate", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 9, TAG = "PORT", Nombre = "EMEsalud", Descripcion = "Aplicaciones Port�tiles para administraci�n de Expediente M�dico Electr�nico.", Icono = "icon-IconoEMEsalud_HIS", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 10, TAG = "HIS", Nombre = "INFOSalud", Descripcion = "Aplicaci�n para Administraci�n Hospitalaria con m�dulos para los servicios m�dicos y departamentos auxiliares de diagn�stico.", Icono = "icon-IconoINFOsalud_HIS", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 11, TAG = "MBI", Nombre = "MBI", Descripcion = "Aplicaci�n de Tony.", Icono = "icon-IconoINFOsalud_HIS", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } },
                new Aplicaciones { IdAplicacion = 12, TAG = "ALE", Nombre = "Sistema ALE (TBD)", Descripcion = "Sistema ALE de reporte de errores y requermientos.", Icono = "icon-IconoINFOsalud_HIS", AplicacionesVersiones = { new AplicacionesVersiones { Version = "0.0.0.0" } } }

                );
            context.SaveChanges();

            context.Modulos.AddOrUpdate(
                new Modulo { Aplicaciones = context.Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Agenda", },
                new Modulo { Aplicaciones = context.Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Recursos Humanos" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Patolog�a" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Almac�n" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Pacientes" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Farmacia" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Finanzas" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Imagenolog�a" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Laboratorio" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Banco de sangre" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Estad�sticas" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Urgencias" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Hospitalizaci�n" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Tareas" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Configuraci�n" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Consultas externas" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Quir�fanos", Aliases = "Quirofano, Quirofanos" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Visor HTML5", Aliases = "VisorHIS" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "Turnos" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "PacsND"), Subcategoria = "VOCALI" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "PacsND"), Subcategoria = "Robot" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "PacsND"), Subcategoria = "STService" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "PacsND"), Subcategoria = "WD, ND, WP, VL, WU, ALMA, Servex" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "PacsND"), Subcategoria = "Teleradiolog�a" },
                new Modulo { Aplicaciones = context .Aplicaciones.FirstOrDefault(c => c.Nombre == "INFOSalud"), Subcategoria = "HIS" },
                new Modulo { Aplicaciones = context.Aplicaciones.FirstOrDefault(c => c.Nombre == "MBI"), Subcategoria = "MBI" },
                new Modulo { Aplicaciones = context.Aplicaciones.FirstOrDefault(c => c.TAG == "ALE"), Subcategoria = "ALE" }
                );
            context.SaveChanges();
        }

        private void SeedContactos(EntidadesPagina context)
        {
            
            context.Contactos.AddOrUpdate(c => c.IdContacto,
                new Contactos() { IdContacto = 1, Nombre = "Israel Troncoso", Email = "itroncoso@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 2, Nombre = "Jesus Gracia", Email = "jgracia@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 3, Nombre = "Dulce Ya�ez", Email = "dyanez@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 4, Nombre = "Alexis Ba�os", Email = "rbanos@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 5, Nombre = "Margarita Noriega", Email = "mnoriega@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 6, Nombre = "Erika Machorro", Email = "mmachorro@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 7, Nombre = "Carlos Guzman", Email = "cguzman@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 8, Nombre = "Jocelyn Arenas", Email = "karenas@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 9, Nombre = "Ricardo Garcia", Email = "rgarciar@eymsa.com.mx", Empresa = "EYMSA" },
                new Contactos() { IdContacto = 10, Nombre = "Joan Carrasco", Email = "joan.carrasco@gmdominicana.com", Empresa = "Global Medica Dominicana" },
                new Contactos() { IdContacto = 11, Nombre = "Adderly Rivera", Email = "adderly.rivera@gmdominicana.com", Empresa = "Global Medica Dominicana" },
                new Contactos() { IdContacto = 12, Nombre = "Miguel Pimentel", Email = "miguel.pimentel@gmdominicana.com", Empresa = "Global Medica Dominicana" },
                new Contactos() { IdContacto = 13, Nombre = "Nicolas Lule", Email = "nlule@cmr3.com.mx", Empresa = "CMR" },
                new Contactos() { IdContacto = 14, Nombre = "Alfonso Rodriguez", Email = "arodriguez@cmr3.com.mx", Empresa = "CMR" },
                new Contactos() { IdContacto = 15, Nombre = "Mayra Salinas", Email = "msalinas@cmr3.com.mx", Empresa = "CMR" },
                new Contactos() { IdContacto = 16, Nombre = "Felipe Cuaranta", Email = "fcuaranta@cmr3.com.mx", Empresa = "CMR" },               
                new Contactos() { IdContacto = 17, Nombre = "Alejandra C�zares", Email = "acazares@cmr3.com.mx", Empresa = "CMR" },
                
                new Contactos() { IdContacto = 18, Nombre = "Vicente Rincon", Email = "vrincon@cmr3.com.mx", Empresa = "CMR" },
                new Contactos() { IdContacto = 19, Nombre = "Ivan Ortega", Email = "iortega@cmr3.com.mx", Empresa = "CMR" },

                new Contactos() { IdContacto = 20, Nombre = "Omar Landaverde", Email = "olandaverde@cmr3.com.mx", Empresa = "CMR" },
                new Contactos() { IdContacto = 21, Nombre = "Antonio Flores", Email = "aflores@cmr3.com.mx", Empresa = "CMR" }
                );
        }

        private void SeedContactosAplicaciones(EntidadesPagina context)
        {
            context.SaveChanges();


            var aplicacionesW = context.Aplicaciones.Where(x => !x.Nombre.Contains("Arix")).ToList();
            var contactos = context.Contactos.Take(17).ToList();
       
            if (aplicacionesW != null)
            {
                foreach (var a in aplicacionesW)
                {
                    a.Contactos = contactos;
                }
            }


            var aplicacionesRAD = context.Aplicaciones.Where(x => x.Nombre.Contains("ArixRAD")).ToList();
            var contactosRAD = context.Contactos.Where(x => x.IdContacto >= 18 && x.IdContacto <= 19).ToList();
            if (aplicacionesRAD != null)
            {
                foreach (var a in aplicacionesRAD)
                {
                    a.Contactos = contactosRAD;
                }
            }

            var aplicacionesDRF = context.Aplicaciones.Where(x => x.Nombre.Contains("ArixDRF")).ToList();
            var contactosDRF = context.Contactos.Where(x => x.IdContacto >= 20).ToList();
            if (aplicacionesDRF != null)
            {
                foreach (var a in aplicacionesDRF)
                {
                    a.Contactos = contactosDRF;
                }
            }
        }
    }

}
