namespace IDigitales.Pagina.Web.Data
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportesNumeroSerie : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reportes", "NumeroSerie", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reportes", "NumeroSerie");
        }
    }
}
