namespace IDigitales.Pagina.Web.Data
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TablaReportesCategorias : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReportesCategorias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 100),
                        Descripcion = c.String(maxLength: 250),
                        Grupo = c.String(maxLength: 50),
                        EsAplicacion = c.Boolean(nullable: false, defaultValue: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InstalacionesModulos",
                c => new
                    {
                        IdInstalacion = c.Int(nullable: false),
                        IdModulo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdInstalacion, t.IdModulo })
                .ForeignKey("dbo.Instalaciones", t => t.IdInstalacion, cascadeDelete: true)
                .ForeignKey("dbo.Modulos", t => t.IdModulo, cascadeDelete: true)
                .Index(t => t.IdInstalacion)
                .Index(t => t.IdModulo);
            
            CreateTable(
                "dbo.UsuariosReportesCategorias",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false),
                        IdReporteCategoria = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdUsuario, t.IdReporteCategoria })
                .ForeignKey("dbo.Usuarios", t => t.IdUsuario, cascadeDelete: true)
                .ForeignKey("dbo.ReportesCategorias", t => t.IdReporteCategoria, cascadeDelete: true)
                .Index(t => t.IdUsuario)
                .Index(t => t.IdReporteCategoria);
            
            CreateTable(
                "dbo.ReportesCategoriasResposables",
                c => new
                    {
                        IdResponsable = c.Int(nullable: false),
                        IdReporteCategoria = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdResponsable, t.IdReporteCategoria })
                .ForeignKey("dbo.Usuarios", t => t.IdResponsable, cascadeDelete: true)
                .ForeignKey("dbo.ReportesCategorias", t => t.IdReporteCategoria, cascadeDelete: true)
                .Index(t => t.IdResponsable)
                .Index(t => t.IdReporteCategoria);
            
            CreateTable(
                "dbo.InstalacionesReportesCategorias",
                c => new
                    {
                        IdInstalacion = c.Int(nullable: false),
                        IdReporteCategoria = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdInstalacion, t.IdReporteCategoria })
                .ForeignKey("dbo.Instalaciones", t => t.IdInstalacion, cascadeDelete: true)
                .ForeignKey("dbo.ReportesCategorias", t => t.IdReporteCategoria, cascadeDelete: true)
                .Index(t => t.IdInstalacion)
                .Index(t => t.IdReporteCategoria);
            
            AddColumn("dbo.Instalaciones", "EsProyecto", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.Reportes", "CategoriaId", c => c.Int());
            CreateIndex("dbo.Reportes", "CategoriaId");
            AddForeignKey("dbo.Reportes", "CategoriaId", "dbo.ReportesCategorias", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InstalacionesReportesCategorias", "IdReporteCategoria", "dbo.ReportesCategorias");
            DropForeignKey("dbo.InstalacionesReportesCategorias", "IdInstalacion", "dbo.Instalaciones");
            DropForeignKey("dbo.ReportesCategoriasResposables", "IdReporteCategoria", "dbo.ReportesCategorias");
            DropForeignKey("dbo.ReportesCategoriasResposables", "IdResponsable", "dbo.Usuarios");
            DropForeignKey("dbo.UsuariosReportesCategorias", "IdReporteCategoria", "dbo.ReportesCategorias");
            DropForeignKey("dbo.UsuariosReportesCategorias", "IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Reportes", "CategoriaId", "dbo.ReportesCategorias");
            DropForeignKey("dbo.InstalacionesModulos", "IdModulo", "dbo.Modulos");
            DropForeignKey("dbo.InstalacionesModulos", "IdInstalacion", "dbo.Instalaciones");
            DropIndex("dbo.InstalacionesReportesCategorias", new[] { "IdReporteCategoria" });
            DropIndex("dbo.InstalacionesReportesCategorias", new[] { "IdInstalacion" });
            DropIndex("dbo.ReportesCategoriasResposables", new[] { "IdReporteCategoria" });
            DropIndex("dbo.ReportesCategoriasResposables", new[] { "IdResponsable" });
            DropIndex("dbo.UsuariosReportesCategorias", new[] { "IdReporteCategoria" });
            DropIndex("dbo.UsuariosReportesCategorias", new[] { "IdUsuario" });
            DropIndex("dbo.InstalacionesModulos", new[] { "IdModulo" });
            DropIndex("dbo.InstalacionesModulos", new[] { "IdInstalacion" });
            DropIndex("dbo.Reportes", new[] { "CategoriaId" });
            DropColumn("dbo.Reportes", "CategoriaId");
            DropColumn("dbo.Instalaciones", "EsProyecto");
            DropTable("dbo.InstalacionesReportesCategorias");
            DropTable("dbo.ReportesCategoriasResposables");
            DropTable("dbo.UsuariosReportesCategorias");
            DropTable("dbo.InstalacionesModulos");
            DropTable("dbo.ReportesCategorias");
        }
    }
}
