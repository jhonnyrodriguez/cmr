namespace IDigitales.Pagina.Web.Data
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aplicaciones",
                c => new
                    {
                        IdAplicacion = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 50),
                        TAG = c.String(),
                        Descripcion = c.String(maxLength: 200),
                        Icono = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.IdAplicacion);
            
            CreateTable(
                "dbo.AplicacionesVersiones",
                c => new
                    {
                        IdVersion = c.Int(nullable: false, identity: true),
                        Version = c.String(maxLength: 50),
                        Sistema = c.String(maxLength: 50),
                        Tamano = c.String(maxLength: 50),
                        Cambios = c.String(unicode: false, storeType: "text"),
                        Ruta = c.String(maxLength: 250),
                        IdAplicacion = c.Int(),
                        Fecha = c.DateTime(),
                        CorreoEnviado = c.Boolean(),
                    })
                .PrimaryKey(t => t.IdVersion)
                .ForeignKey("dbo.Aplicaciones", t => t.IdAplicacion)
                .Index(t => t.IdAplicacion);
            
            CreateTable(
                "dbo.AplicacionesManuales",
                c => new
                    {
                        IdManual = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 50),
                        Ruta = c.String(maxLength: 250),
                        Fecha = c.DateTime(),
                        IdVersion = c.Int(),
                    })
                .PrimaryKey(t => t.IdManual)
                .ForeignKey("dbo.AplicacionesVersiones", t => t.IdVersion)
                .Index(t => t.IdVersion);
            
            CreateTable(
                "dbo.Contactos",
                c => new
                    {
                        IdContacto = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 150),
                        Empresa = c.String(maxLength: 150),
                        Email = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.IdContacto);
            
            CreateTable(
                "dbo.Modulos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AplicacionesId = c.Int(nullable: false),
                        Subcategoria = c.String(),
                        Aliases = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aplicaciones", t => t.AplicacionesId, cascadeDelete: true)
                .Index(t => t.AplicacionesId);
            
            CreateTable(
                "dbo.Clasificaciones",
                c => new
                    {
                        IdClasificacion = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 100),
                        Descripcion = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.IdClasificacion);
            
            CreateTable(
                "dbo.Productos",
                c => new
                    {
                        IdProducto = c.Int(nullable: false, identity: true),
                        EsInternacional = c.Boolean(nullable: false),
                        Modelo = c.String(nullable: false, maxLength: 50),
                        Nombre = c.String(nullable: false, maxLength: 150),
                        Descripcion = c.String(nullable: false, storeType: "ntext"),
                        Clasificacion = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Moneda = c.Int(nullable: false),
                        NumeroParte = c.String(maxLength: 50),
                        DatosCrudos = c.Double(),
                        DatosUsables = c.Double(),
                        Ruta = c.String(maxLength: 250),
                        Activo = c.Boolean(),
                    })
                .PrimaryKey(t => t.IdProducto)
                .ForeignKey("dbo.Clasificaciones", t => t.Clasificacion)
                .Index(t => t.Clasificacion);
            
            CreateTable(
                "dbo.CotizacionesProductos",
                c => new
                    {
                        IdCotizacion = c.Int(nullable: false),
                        IdProducto = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        PorcentajeDescuento = c.Int(),
                    })
                .PrimaryKey(t => new { t.IdCotizacion, t.IdProducto })
                .ForeignKey("dbo.Cotizaciones", t => t.IdCotizacion, cascadeDelete: true)
                .ForeignKey("dbo.Productos", t => t.IdProducto, cascadeDelete: true)
                .Index(t => t.IdCotizacion)
                .Index(t => t.IdProducto);
            
            CreateTable(
                "dbo.Cotizaciones",
                c => new
                    {
                        IdCotizacion = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 100),
                        IdLista = c.Int(nullable: false),
                        PorcentajeDescuento = c.Int(),
                        idUsuario = c.Int(),
                    })
                .PrimaryKey(t => t.IdCotizacion)
                .ForeignKey("dbo.LIstasPrecios", t => t.IdLista)
                .ForeignKey("dbo.Usuarios", t => t.idUsuario)
                .Index(t => t.IdLista)
                .Index(t => t.idUsuario);
            
            CreateTable(
                "dbo.LIstasPrecios",
                c => new
                    {
                        IdLista = c.Int(nullable: false, identity: true),
                        Codigo = c.String(maxLength: 10),
                        Nombre = c.String(maxLength: 50),
                        Descripcion = c.String(maxLength: 200),
                        IdListaReferencia = c.Int(),
                        EsBase = c.Boolean(),
                        Porcentaje = c.Double(),
                        IVA = c.Boolean(),
                    })
                .PrimaryKey(t => t.IdLista);
            
            CreateTable(
                "dbo.ListasPrecioProductos",
                c => new
                    {
                        IdLista = c.Int(nullable: false),
                        IdProducto = c.Int(nullable: false),
                        Porcentaje = c.Double(),
                        PrecioFinal = c.Decimal(precision: 18, scale: 4),
                    })
                .PrimaryKey(t => new { t.IdLista, t.IdProducto })
                .ForeignKey("dbo.LIstasPrecios", t => t.IdLista)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdLista)
                .Index(t => t.IdProducto);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                        Email = c.String(),
                        Password = c.String(nullable: false, maxLength: 20),
                        Tipo = c.Int(nullable: false),
                        Activo = c.Boolean(),
                        UsuariosNotificationsConfigurationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdUsuario);
            
            CreateTable(
                "dbo.Permisos",
                c => new
                    {
                        IdPermiso = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 50),
                        Descripcion = c.String(maxLength: 200),
                        Clave = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.IdPermiso);
            
            CreateTable(
                "dbo.Instalaciones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Tag = c.String(maxLength: 4),
                        Descripcion = c.String(maxLength: 60),
                        FechaCreacion = c.DateTime(nullable: false),
                        Activo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reportes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false),
                        FechaReporte = c.DateTime(nullable: false),
                        Estado = c.Int(nullable: false),
                        Prioridad = c.Int(nullable: false),
                        ModuloId = c.Int(nullable: false),
                        FechaAsignacion = c.DateTime(),
                        FechaSolucion = c.DateTime(),
                        VersionDeCorreccionId = c.Int(),
                        VersionActualId = c.Int(),
                        Titulo = c.String(nullable: false),
                        InstalacionId = c.Int(nullable: false),
                        QuienReportaId = c.Int(),
                        AsignadoId = c.Int(),
                        VistoBuenoId = c.Int(),
                        EnProcesoActual = c.Boolean(nullable: false),
                        ReportePredecesorId = c.Int(),
                        Progreso = c.Int(),
                        FechaProgramada = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Instalaciones", t => t.InstalacionId, cascadeDelete: true)
                .ForeignKey("dbo.Modulos", t => t.ModuloId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.QuienReportaId)
                .ForeignKey("dbo.Reportes", t => t.ReportePredecesorId)
                .ForeignKey("dbo.Usuarios", t => t.VistoBuenoId)
                .ForeignKey("dbo.Usuarios", t => t.AsignadoId)
                .Index(t => t.ModuloId)
                .Index(t => t.InstalacionId)
                .Index(t => t.QuienReportaId)
                .Index(t => t.AsignadoId)
                .Index(t => t.VistoBuenoId)
                .Index(t => t.ReportePredecesorId);
            
            CreateTable(
                "dbo.Comentarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Contenido = c.String(),
                        ArchivoRutaRelativa = c.String(),
                        FechaCreacion = c.DateTime(nullable: false),
                        Tipo = c.Int(nullable: false),
                        NuevoEstado = c.Int(),
                        VersionDeCorreccion = c.String(),
                        EvidenciaId = c.Int(),
                        UsuarioId = c.Int(),
                        ReporteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Comentarios", t => t.EvidenciaId)
                .ForeignKey("dbo.Reportes", t => t.ReporteId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId)
                .Index(t => t.EvidenciaId)
                .Index(t => t.UsuarioId)
                .Index(t => t.ReporteId);
            
            CreateTable(
                "dbo.UsuariosNotificationsConfigurations",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        UsuariosId = c.Int(nullable: false),
                        NotificationConfiguration = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuarios", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Vistas",
                c => new
                    {
                        IdVista = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 50),
                        Clave = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.IdVista);
            
            CreateTable(
                "dbo.ProductosComponentes",
                c => new
                    {
                        IdProducto = c.Int(nullable: false),
                        IdComponente = c.Int(nullable: false),
                        Cantidad = c.Int(),
                    })
                .PrimaryKey(t => new { t.IdProducto, t.IdComponente })
                .ForeignKey("dbo.Productos", t => t.IdComponente)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdProducto)
                .Index(t => t.IdComponente);
            
            CreateTable(
                "dbo.ConfiguracaionInstalaciones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Inactividad = c.Double(nullable: false),
                        ProcesoExedente = c.Double(nullable: false),
                        RutaEvidencias = c.String(),
                        RutaReportesImportados = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ConfiguracionMail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Puerto = c.Int(nullable: false),
                        Sender = c.String(),
                        Servidor = c.String(),
                        HabilitaSSL = c.Boolean(nullable: false),
                        ModoSSL = c.Int(nullable: false),
                        VersionSSL = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Documentos",
                c => new
                    {
                        IdDocumento = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 50),
                        Descripcion = c.String(maxLength: 200),
                        Categoria = c.Int(),
                        SubCategoria = c.Int(),
                        Ruta = c.String(maxLength: 250),
                        Fecha = c.DateTime(),
                    })
                .PrimaryKey(t => t.IdDocumento);
            
            CreateTable(
                "dbo.ReportesImportados",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        FechaImportacion = c.DateTime(nullable: false),
                        RutaOriginal = c.String(),
                        LineasSobrantesCount = c.Int(nullable: false),
                        ReportesImportadosDict = c.String(),
                        ReportesImportadosCount = c.Int(nullable: false),
                        Estado = c.Int(nullable: false),
                        InstalacionId = c.Int(nullable: false),
                        ReporteImportadoFuenteId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Instalaciones", t => t.InstalacionId, cascadeDelete: true)
                .ForeignKey("dbo.ReportesImportados", t => t.ReporteImportadoFuenteId)
                .Index(t => t.InstalacionId)
                .Index(t => t.ReporteImportadoFuenteId);
            
            CreateTable(
                "dbo.Rutas",
                c => new
                    {
                        Tipo = c.Int(nullable: false),
                        Descripcion = c.String(nullable: false, maxLength: 20),
                        Directorio = c.String(nullable: false, maxLength: 120),
                    })
                .PrimaryKey(t => t.Tipo);
            
            CreateTable(
                "dbo.AplicacionesContactos",
                c => new
                    {
                        IdAplicacion = c.Int(nullable: false),
                        IdContacto = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdAplicacion, t.IdContacto })
                .ForeignKey("dbo.Aplicaciones", t => t.IdAplicacion, cascadeDelete: true)
                .ForeignKey("dbo.Contactos", t => t.IdContacto, cascadeDelete: true)
                .Index(t => t.IdAplicacion)
                .Index(t => t.IdContacto);
            
            CreateTable(
                "dbo.UsuariosPermisos",
                c => new
                    {
                        IdPermisos = c.Int(nullable: false),
                        IdUsuario = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdPermisos, t.IdUsuario })
                .ForeignKey("dbo.Permisos", t => t.IdPermisos, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.IdUsuario, cascadeDelete: true)
                .Index(t => t.IdPermisos)
                .Index(t => t.IdUsuario);
            
            CreateTable(
                "dbo.InstalacionUsuarios",
                c => new
                    {
                        Instalacion_Id = c.Int(nullable: false),
                        Usuarios_IdUsuario = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Instalacion_Id, t.Usuarios_IdUsuario })
                .ForeignKey("dbo.Instalaciones", t => t.Instalacion_Id, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.Usuarios_IdUsuario, cascadeDelete: true)
                .Index(t => t.Instalacion_Id)
                .Index(t => t.Usuarios_IdUsuario);
            
            CreateTable(
                "dbo.UsuariosVistas",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false),
                        IdVista = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdUsuario, t.IdVista })
                .ForeignKey("dbo.Usuarios", t => t.IdUsuario, cascadeDelete: true)
                .ForeignKey("dbo.Vistas", t => t.IdVista, cascadeDelete: true)
                .Index(t => t.IdUsuario)
                .Index(t => t.IdVista);

            //DATOS INICIALES
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Aplicaciones]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Aplicaciones] ON " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(1, N'ArixDRF', N'ARIX', N'Aplicaci�n para la captura de im�genes Radiogr�ficas y Fluroscopicas con opciones avanzadas de procesamientos.', N'icon-arixdrf') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(2, N'ArixRAD', N'ARAD', N'Aplicaci�n para la captura de im�genes Radiogr�ficas con opciones avanzadas de procesamientos.', N'icon-arixrad') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(3, N'PacsND', N'PND', N'Visualizador WEB XBAP para el postprocesamiento de im�genes y generaci�n de reportes remotos.', N'icon-pacsND') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(4, N'PacsQR', N'PQR', N'Servicio Servidor Dicom (QR SCP)  para la consulta y recuperaci�n de clientes de im�genes del PACS.', N'icon-pacsQR') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(5, N'PacsST', N'PACS', N'Servicio Servidor Dicom (SEND SCP) para el almacenamiento de im�genes en el PACS.', N'icon-pacsST') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(6, N'PacsWS', N'PWS', N'Estaci�n de trabajo de r�diologos para el postprocesamiento avanzados de im�genes y generaci�n de reportes remotos.', N'icon-pacsWD') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(7, N'PacsWP', N'PWP', N'Aplicaci�n para generaci�n de CD/DVD por medio de Robots Publicadores.', N'icon-pacsWP') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(8, N'WDUltimate', N'WDU', N'Visualizador WEB HTML5 para el postprocesamiento de im�genes y generaci�n de reportes remotos.', N'icon-WDultimate') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(9, N'EMEsalud', N'PORT', N'Aplicaciones Port�tiles para administraci�n de Expediente M�dico Electr�nico.', N'icon-IconoEMEsalud_HIS') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(10, N'INFOSalud', N'HIS', N'Aplicaci�n para Administraci�n Hospitalaria con m�dulos para los servicios m�dicos y departamentos auxiliares de diagn�stico.', N'icon-IconoINFOsalud_HIS') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(11, N'MBI', N'MBI', N'Aplicaci�n de Tony.', N'icon-IconoINFOsalud_HIS') " +
                "INSERT[dbo].[Aplicaciones]([IdAplicacion], [Nombre], [TAG], [Descripcion], [Icono]) VALUES(12, N'Sistema ALE (TBD)', N'ALE', N'Sistema ALE de reporte de errores y requermientos.', N'icon-IconoINFOsalud_HIS') " +
                "SET IDENTITY_INSERT [dbo].[Aplicaciones] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[AplicacionesVersiones]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[AplicacionesVersiones] ON " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (1, N'0.0.0.0', NULL, NULL, NULL, NULL, 1, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (2, N'0.0.0.0', NULL, NULL, NULL, NULL, 2, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (3, N'0.0.0.0', NULL, NULL, NULL, NULL, 3, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (4, N'0.0.0.0', NULL, NULL, NULL, NULL, 4, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (5, N'0.0.0.0', NULL, NULL, NULL, NULL, 5, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (6, N'0.0.0.0', NULL, NULL, NULL, NULL, 6, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (7, N'0.0.0.0', NULL, NULL, NULL, NULL, 7, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (8, N'0.0.0.0', NULL, NULL, NULL, NULL, 8, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (9, N'0.0.0.0', NULL, NULL, NULL, NULL, 9, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (10, N'0.0.0.0', NULL, NULL, NULL, NULL, 10, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (11, N'0.0.0.0', NULL, NULL, NULL, NULL, 11, NULL, NULL) " +
                "INSERT [dbo].[AplicacionesVersiones] ([IdVersion], [Version], [Sistema], [Tamano], [Cambios], [Ruta], [IdAplicacion], [Fecha], [CorreoEnviado]) VALUES (12, N'0.0.0.0', NULL, NULL, NULL, NULL, 12, NULL, NULL) " +
                "SET IDENTITY_INSERT [dbo].[AplicacionesVersiones] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Modulos]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Modulos] ON " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (1, 10, N'Agenda', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (2, 10, N'Recursos Humanos', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (3, 10, N'Patolog�a', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (4, 10, N'Almac�n', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (5, 10, N'Pacientes', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (6, 10, N'Farmacia', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (7, 10, N'Finanzas', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (8, 10, N'Imagenolog�a', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (9, 10, N'Laboratorio', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (10, 10, N'Banco de sangre', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (11, 10, N'Estad�sticas', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (12, 10, N'Urgencias', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (13, 10, N'Hospitalizaci�n', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (14, 10, N'Tareas', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (15, 10, N'Configuraci�n', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (16, 10, N'Consultas externas', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (17, 10, N'Quir�fanos', N'Quirofano, Quirofanos') " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (18, 10, N'Visor HTML5', N'VisorHIS') " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (19, 10, N'Turnos', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (20, 3, N'VOCALI', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (21, 3, N'Robot', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (22, 3, N'STService', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (23, 3, N'WD, ND, WP, VL, WU, ALMA, Servex', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (24, 3, N'Teleradiolog�a', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (25, 10, N'HIS', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (26, 11, N'MBI', NULL) " +
                "INSERT [dbo].[Modulos] ([Id], [AplicacionesId], [Subcategoria], [Aliases]) VALUES (27, 12, N'ALE', NULL) " +
                "SET IDENTITY_INSERT [dbo].[Modulos] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Contactos]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Contactos] ON " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (1, N'Israel Troncoso', N'EYMSA', N'itroncoso@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (2, N'Jesus Gracia', N'EYMSA', N'jgracia@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (3, N'Dulce Ya�ez', N'EYMSA', N'dyanez@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (4, N'Alexis Ba�os', N'EYMSA', N'rbanos@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (5, N'Margarita Noriega', N'EYMSA', N'mnoriega@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (6, N'Erika Machorro', N'EYMSA', N'mmachorro@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (7, N'Carlos Guzman', N'EYMSA', N'cguzman@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (8, N'Jocelyn Arenas', N'EYMSA', N'karenas@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (9, N'Ricardo Garcia', N'EYMSA', N'rgarciar@eymsa.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (10, N'Joan Carrasco', N'Global Medica Dominicana', N'joan.carrasco@gmdominicana.com') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (11, N'Adderly Rivera', N'Global Medica Dominicana', N'adderly.rivera@gmdominicana.com') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (12, N'Miguel Pimentel', N'Global Medica Dominicana', N'miguel.pimentel@gmdominicana.com') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (13, N'Nicolas Lule', N'CMR', N'nlule@cmr3.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (14, N'Alfonso Rodriguez', N'CMR', N'arodriguez@cmr3.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (15, N'Mayra Salinas', N'CMR', N'msalinas@cmr3.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (16, N'Felipe Cuaranta', N'CMR', N'fcuaranta@cmr3.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (17, N'Alejandra C�zares', N'CMR', N'acazares@cmr3.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (18, N'Vicente Rincon', N'CMR', N'vrincon@cmr3.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (19, N'Ivan Ortega', N'CMR', N'iortega@cmr3.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (20, N'Omar Landaverde', N'CMR', N'olandaverde@cmr3.com.mx') " +
                "INSERT [dbo].[Contactos] ([IdContacto], [Nombre], [Empresa], [Email]) VALUES (21, N'Antonio Flores', N'CMR', N'aflores@cmr3.com.mx') " +
                "SET IDENTITY_INSERT [dbo].[Contactos] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[AplicacionesContactos]) = 0) " +
                "BEGIN " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 1) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 2) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 3) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 4) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 5) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 6) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 7) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 8) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 9) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 10) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 11) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 12) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 13) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 14) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 15) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 16) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (3, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (4, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (5, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (6, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (7, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (8, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (9, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (10, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (11, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (12, 17) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (2, 18) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (2, 19) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (1, 20) " +
                "INSERT [dbo].[AplicacionesContactos] ([IdAplicacion], [IdContacto]) VALUES (1, 21) " +
                "END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Usuarios]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Usuarios] ON " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (1, N'Admin1', N'cmonroyp@eymsa.com', N'Admin1', 1, 1, 0) " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (2, N'gmonroy', N'cmonroyp@eymsa.com', N'Cmr2016$2', 2, 1, 0) " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (3, N'lheredia', N'cmonroyp@eymsa.com', N'Cmr2016$3', 3, 1, 0) " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (4, N'mnoriega', N'cmonroyp@eymsa.com', N'Cmr2016$4', 2, 1, 0) " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (5, N'emachorro', N'cmonroyp@eymsa.com', N'Cmr2016$5', 2, 1, 0) " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (6, N'cguzman', N'cmonroyp@eymsa.com', N'Cmr2016$6', 2, 1, 0) " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (7, N'msalinas', N'cmonroyp@eymsa.com', N'Cmr2016$7', 4, 1, 0) " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (8, N'tmonroy', N'cmonroyp@eymsa.com', N'Cmr2016$8', 4, 1, 0) " +
                "INSERT [dbo].[Usuarios] ([IdUsuario], [Nombre], [Email], [Password], [Tipo], [Activo], [UsuariosNotificationsConfigurationId]) VALUES (9, N'acazares', N'cmonroyp@eymsa.com', N'Cmr2016$9', 1, 1, 0) " +
                "SET IDENTITY_INSERT [dbo].[Usuarios] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Instalaciones]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Instalaciones] ON " +
                "INSERT [dbo].[Instalaciones] ([Id], [Tag], [Descripcion], [FechaCreacion], [Activo]) VALUES (1, N'DEV', N'Desarrollo', CAST(0x0000AC4D0105B57C AS DateTime), 1) " +
                "SET IDENTITY_INSERT [dbo].[Instalaciones] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Clasificaciones]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Clasificaciones] ON " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (1, N'HARDWARE HIS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (2, N'HARDWARE PACS CORE', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (3, N'HARDWARE PACS ENCORE', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (4, N'HARDWARE PARA ESTACIONES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (5, N'DISCOS DUROS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (6, N'MONITORES DIAGNOSTICOS/CLINICOS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (7, N'MONITORES ', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (8, N'MONITORES REFACCIONES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (9, N'PUBLICADORES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (10, N'SISTEMAS DE TURNOS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (11, N'ACCESORIOS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (12, N'TERMINALES ADICIONALES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (13, N'DISPOSITIVOS EXTERNOS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (14, N'CONSUMIBLES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (15, N'UPS (Sistema de Alimentaci�n Ininterrumpida)', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (16, N'MOBILIARIO', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (17, N'HARDWARE PARA MODALIDADES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (18, N'MODALIDADES REFACCIONES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (19, N'PACS ENCORE (Crecimiento en Unidades de Almacenamiento)', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (20, N'PACS ENCORE (Incremento en Unidades de Almacenamiento)', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (21, N'LICENCIAS ADMINISTRADOR DE SERVIDORES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (22, N'LICENCIAS VISOR MULTIMODALIDAD PARA RADI�LOGOS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (23, N'LICENCIAS VISOR WEB', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (24, N'LICENCIAS SISTEMA DE INFORMACI�N RADI�LOGICA', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (25, N'LICENCIAS MODULOS EXTRAS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (26, N'LICENCIAS ANTIVIRUS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (27, N'LICENCIAS RECONOCIMIENTO DE VOZ', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (28, N'SERVICIOS PROFESIONALES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (29, N'SISTEMAS MOVILES', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (30, N'SISTEMAS DE FLUROSCOP�A', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (31, N'SISTEMAS RADIOGR�FICOS', N'') " +
                "INSERT [dbo].[Clasificaciones] ([IdClasificacion], [Nombre], [Descripcion]) VALUES (32, N'SISTEMAS Y APLICACIONES', N'') " +
                "SET IDENTITY_INSERT [dbo].[Clasificaciones] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Productos]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Productos] ON " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (1, 1, N'G0130-1T 512', N'Servidor para Sistema HIS', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor Adicional de Aplicaciones* HIS</b>. Servidor m�nimo con <b>512GB</b> de memoria RAM, 1TB de almacenamiento en RAID 5 y procesador Intel Xeon 48 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante, monitor plano de 19� *Requerido solo en el caso de tener m�s de 300 usuarios concurrentes utilizando el sistema.</label></body></html>', 1, 1, CAST(22600.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (2, 1, N'G0130-1T 512-CPU', N'Servidor para Sistema HIS (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU Servidor para Sistema HIS.</label></body></html>', 1, 2, CAST(21500.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (3, 1, N'224-4856', N'Servidor para PACS CORE B�sico', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servidor para PACS CORE. Procesador Intel Xeon Gold 5218 Deca Core 64bits (16 n�cleos) 2.3 a 3.9 GHz. Memoria de <b>16</b>GB DDR4 (1 x 16GB) Dual Ranked RDIMMs. Tarjeta contradora BOSS con 2 x Discos M.2 de <b>240</b>GB (en RAID 1) para el SO. 3 x Discos Duro de 2.0TB <b>7.200</b> RPM, Hot-Plug. LECTOR DVD-ROM 8x, SATA. Tarjeta Doble de red integrada 10Gb Broadcom NetXtreme II 57416. Sin Monitor. Sin Tarjeta de acceso remoto. Fuente de Poder no Redundante de Alta Potencia de Alta Potencia (750W).</label></body></html>', 2, 2, CAST(7577.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (4, 1, N'224-4856-I', N'Servidor para PACS CORE Intermedio', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servidor para PACS CORE. Procesador Intel Xeon Gold 5218 Deca Core 64bits (16 n�cleos) 2.3 a 3.9 GHz. Memoria de <b>32</b>GB DDR4 (1 x <b>32</b>GB) Dual Ranked RDIMMs. 2 x Discos SSD de <b>400</b>GB, Escritura Intensiva SAS, 12 Gbps, Hot-Plug. 3 x Discos Duro de 2.4TB <b>10.000</b> RPM, Hot-Plug. Lector DVD-ROM 8x SATA. Tarjeta Doble de red integrada 10Gb Broadcom NetXtreme II 57416. Sin Monitor. Tarjeta de acceso remoto Dell 9na Generaci�n iDRAC/9. Fuente de Poder Redundante de Alta Potencia (750W).</label></body></html>', 2, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (5, 1, N'224-4856-A', N'Servidor para PACS CORE Avanzado', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servidor para PACS CORE. 2 Procesadores Intel Xeon Gold 5218 Deca Core 64bits (32 n�cleos) 2.3 a 3.9GHz. Memoria de <b>32</b>GB DDR4 (2 x <b>16</b>GB) Dual Ranked RDIMMs. 2 x Discos SSD de <b>400</b>GB, Escritura Intensiva SAS, 12 Gbps, Hot-Plug. 3 x Discos Duro de 2.4TB <b>10.000</b> RPM, Hot-Plug. Lector DVD-ROM 8x SATA. Tarjeta Doble de red integrada 10Gb Broadcom NetXtreme II 57416. Sin Monitor. Tarjeta de acceso remoto Dell 9na Generaci�n iDRAC/9. Fuente de Poder Redundante de Alta Potencia (750W).</label></body></html>', 2, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (6, 1, N'G0200-4TB', N'Servidor de sistema PACS CORE 4TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de sistema PACS CORE 4TB 7,200RPM.</b> Servidor Torre con m�nimo con <b>16GB</b> de memoria RAM y procesador Intel Xeon Gold 16 n�cleos, tarjeta doble de red integrada 10GB y lector de DVD-ROM. Sistema operativo superior Microsoft Windows Server 2019 Standard Edition. Almacenamiento 3 x Discos Duro de 2.0TB SAS de 7,200 RPM  con f�cil expansi�n en terabytes hasta 16 Ranuras para disco duro. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye monitor, UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery).  �ptimo desempe�o con un m�ximo de 15000 estudios anuales.</label></body></html>', 2, 1, CAST(8900.00 AS Decimal(10, 2)), 2, NULL, 4, 3, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (7, 1, N'G0200-4TB-I', N'Servidor de sistema PACS CORE 4TB INTERMEDIO', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de sistema PACS CORE Intermedio 4.8TB 10,000RPM.</b> Servidor Torre con m�nimo con <b>32GB</b> de memoria RAM y procesador Intel Xeon Gold 16 n�cleos, tarjeta doble de red integrada 10GB, fuente de poder redundante de alta potencia (750W), lector de DVD-ROM y tarjeta de acceso remoto iDRAC. Sistema operativo superior Microsoft Windows Server 2019 Standard Edition en 2x discos de SSD 400GB en RAID1. Almacenamiento 3 x Discos Duro de 2.4TB SAS de 10,000 RPM con f�cil expansi�n en terabytes hasta 16 Ranuras para disco duro. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye monitor, UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery).</label></body></html>', 2, 1, CAST(14700.00 AS Decimal(10, 2)), 2, NULL, 4, 3, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (8, 1, N'G0200-4TB-A', N'Servidor de sistema PACS CORE 4TB AVANZADO', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de sistema PACS CORE Avanzado 4.8TB 10,000RPM</b> Servidor Torre con m�nimo con <b>64GB</b> de memoria RAM y procesador Intel Xeon Gold 32 n�cleos, tarjeta doble de red integrada 10GB, fuente de poder redundante de alta potencia (750W), lector de DVD-ROM y tarjeta de acceso remoto iDRAC. Sistema operativo superior Microsoft Windows Server 2019 Standard Edition en 2x discos de SSD 400GB en RAID1. Almacenamiento 3 x Discos Duro de 2.4TB SAS de 10,000 RPM con f�cil expansi�n en terabytes hasta 16 Ranuras para disco duro. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye monitor, UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery).</label></body></html>', 2, 1, CAST(19000.00 AS Decimal(10, 2)), 2, NULL, 4, 3, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (9, 1, N'G0199-0TB', N'Servidor de aplicaciones ENCORE', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor Adicional de Aplicaciones* ENCORE.</b> Servidor m�nimo con <b>128GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante, monitor plano de 19� *Requerido solo en el caso de tener m�s de 50 usuarios concurrentes utilizando el sistema.</label></body></html>', 3, 1, CAST(17159.90 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (10, 1, N'G0199-0TB-CPU', N'Servidor de aplicaciones ENCORE (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU de aplicaciones Encore.</label></body></html>', 3, 2, CAST(16060.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (11, 1, N'G0199-4TB', N'Servidor de Almacenamiento ENCORE 4TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de Almacenamiento ENCORE 4TB</b>.<br>Servidor m�nimo con <b>32GB</b> de memoria RAM y procesador Intel Xeon <b>32 n�cleos 3.90GHz</b>, Tarjeta Cu�druple de red integrada, Fuente de Poder Redundante, monitor plano de 19�, Discos duros SSD Escritura Intensiva (Sistema Operativo). Almacenamiento de 4T 10.000 RPM expandible 24 r�nuras. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes(Disaster Recovery). Microsoft Windows Server 2019. * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB.</label></body></html>', 3, 1, CAST(16643.00 AS Decimal(10, 2)), 2, NULL, 4, 3.2, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (12, 1, N'G0199-4TB-CPU', N'Servidor de Almacenamiento ENCORE 4TB (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servidor de Almacenamiento ENCORE 4TB (CPU).</label></body></html>', 3, 2, CAST(9900.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (13, 1, N'G0199-CPU-PRO', N'Servidor de Almacenamiento ENCORE PRO (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>PACS ENCORE PRO (CPU).</label></body></html>', 3, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (14, 1, N'G0199-CPU', N'Servidor de Almacenamiento ENCORE BASE (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>PACS ENCORE BASE (CPU).</label></body></html>', 3, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (15, 1, N'G0199-8TB', N'Servidor de Almacenamiento ENCORE 8TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de Almacenamiento ENCORE 8TB.</b> Servidor m�nimo con <b>64GB</b> de memoria RAM y procesador Intel Xeon 20 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante, monitor plano de 19�. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB.</label></body></html>', 3, 1, CAST(18000.00 AS Decimal(10, 2)), 2, NULL, 8, 6.4, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (16, 1, N'G0199-8TB-CPU', N'Servidor de Almacenamiento ENCORE 8TB (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU servidor de almacenamiento ENCORE 8TB.</label></body></html>', 3, 1, CAST(12000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (17, 1, N'G0199-12TB', N'Servidor de Almacenamiento ENCORE 12TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de Almacenamiento ENCORE 12TB.</b> Servidor m�nimo con <b>128GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante, monitor plano de 19�. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB. En caso de m�s de 10 modalidades utilizar versi�n PRO.</label></body></html>', 3, 1, CAST(22000.00 AS Decimal(10, 2)), 2, NULL, 12, 9.6, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (18, 1, N'G0199-12TB-CPU', N'Servidor de Almacenamiento ENCORE 12TB (CPU)', N'<html><body><font size=''2'' face=''Century Gothic,Century Gothic''><label>Servidor de Almacenamiento ENCORE 12TB (CPU).</label></body></html>', 3, 1, CAST(16000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (19, 1, N'G0199-12TBPRO', N'Servidor de Almacenamiento ENCORE PRO 12TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de Almacenamiento ENCORE PRO 12TB.</b> Servidor m�nimo con <b>256GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). Enfocado a modalidades multicorte que requieren acceso simultaneo para almacenamiento. * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB.</label></body></html>', 3, 1, CAST(26000.00 AS Decimal(10, 2)), 2, NULL, 12, 9.6, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (20, 1, N'G0199-12TB PRO-CPU', N'Servidor de Almacenamiento ENCORE PRO 12TB (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servidor de Almacenamiento ENCORE PRO 12TB (CPU).</label></body></html>', 3, 1, CAST(20000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (21, 1, N'G0199-16TB', N'Servidor de Almacenamiento ENCORE 16TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de Almacenamiento ENCORE 16TB.</b> Servidor m�nimo con <b>128GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB. En caso de m�s de 10 modalidades utilizar versi�n PRO.</label></body></html>', 3, 1, CAST(26200.00 AS Decimal(10, 2)), 2, NULL, 16, 12.8, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (22, 1, N'G0199-16TB-CPU', N'Servidor de Almacenamiento ENCORE 16TB (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servidor de Almacenamiento ENCORE 16TB (CPU).</label></body></html>', 3, 1, CAST(20200.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (23, 1, N'G0199-16TBPRO', N'Servidor de Almacenamiento ENCORE PRO 16TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor de Almacenamiento ENCORE PRO 16TB.</b> Servidor m�nimo con <b>256GB</b> de memoria RAM y procesador Intel Xeon 24 n�cleos, doble tarjeta de red, fuente de alimentaci�n redundante. Con f�cil expansi�n en terabytes. Sin limitaciones en cantidad de equipos para recibir estudios. Incluye UPS y dispositivo para almacenamiento externo de im�genes (Disaster Recovery). Enfocado a modalidades multicorte que requieren acceso simultaneo para almacenamiento. * En caso de m�s de 50 usuarios concurrentes se requiere adicionar G0199-0TB.</label></body></html>', 3, 1, CAST(28200.00 AS Decimal(10, 2)), 2, NULL, 16, 12.8, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (24, 1, N'G0199-16TB PRO-CPU', N'Servidor de Almacenamiento ENCORE PRO 16TB (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servidor de Almacenamiento ENCORE PRO 16TB (CPU).</label></body></html>', 3, 1, CAST(22200.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (25, 1, N'G0199-VOZ', N'Servidor Adicional Reconocimiento de voz ENCORE', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Servidor Adicional Reconocimiento de Voz ENCORE.</b> Servidor m�nimo con <b>8GB</b> de memoria RAM y procesador Intel Xeon 4 n�cleos. *Requerido solo en el caso de necesitar la instalaci�n de motor de reconocimiento local. (Opci�n disponible s�lo en la compra m�nima de 5 usuarios concurrentes en la instalaci�n).</label></body></html>', 3, 2, CAST(3548.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (26, 1, N'D0068-H', N'Estaci�n de Diagn�stico DiagRX SMART', N'<html><body><font size=''2'' face=''Century Gothic''><label>Estaci�n de env�os para Radi�logos (DIAG SMART) con caracter�sticas de: 3rd Gen Intel Core i3-3220, <b>2GB</b> 600MHZ DDR3, 1 disco de 256GB SSD, Tarjeta de video integrada, Monitor plano de 19�, 16X DVD+/-RW SATA,Data Only, Microsoft Windows 10PRO, 64bits. Incluye UPS.</label></body></html>', 4, 1, CAST(938.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (27, 1, N'D0054-H', N'DiagRX ALFA', N'<html><body><font size=''2'' face=''Century Gothic''><label>Estaci�n de revisi�n para Radi�logos (DIAG ALFA) con caracter�sticas de: 3rd Gen Intel Core i7-3770, <b>4GB</b> 600MHZ DDR3, 1 disco de 256GB SSD, Tarjeta de video integrada,16X DVD+/-RW SATA,Data Only, Microsoft Windows 10PRO, 64bits. Incluye UPS. (S�lo monitores cl�nicos).</label></body></html>', 4, 1, CAST(1260.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (28, 1, N'D0051-H2', N'Estaci�n de Diagn�stico DiagRX S/tarjeta video', N'<html><body><font size=''2'' face=''Century Gothic''><label>Estaci�n de diagn�stico para Radi�logos (DIAGRX) con caracter�sticas de: Hexa Core XEON 3.2GHz, <b>8GB</b> DDR3 RDIMM 1600, DD de 256GB SSD SATA, Monitor plano de 19� 16X DVD+/-RW Dual Layer SATA, Microsoft Windows 10PRO, 64bits. Incluye UPS.</label></body></html>', 4, 1, CAST(3000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (29, 1, N'D0051-H3', N'Estaci�n de Diagn�stico DiagRX S/tarjeta video', N'<html><body><font size=''2'' face=''Century Gothic''><label>Estaci�n de diagn�stico para Radi�logos (DIAGRX) con caracter�sticas de: OCTA Core I9 5.00GHZ, <b>8GB</b> DDR3, 1 disco 512GB SSD, 2x Puertos Red Gigabit , Unidad de 8x CD-DVD+/-RW. Microsoft Windows 10PRO, 64bits. Monitor plano de 19�. Incluye UPS</label></body></html>', 4, 1, CAST(2063.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (30, 1, N'D0061-H', N'Estaci�n de Diagn�stico DiagRX ALFA MASTO', N'<html><body><font size=''2'' face=''Century Gothic''><label>Estaci�n de diagn�stico para Radi�logos (DIAGRX ALFA MASTO) con caracter�sticas de: Hexa Core XEON 3.2GHz, <b>16GB</b> DDR3 RDIMM 1600, DD de 256GB SSD SATA, 16X DVD+/-RW Dual Layer SATA, Microsoft Windows 10PRO, 64bits. Incluye UPS.</label></body></html>', 4, 1, CAST(3840.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (31, 1, N'224-6874', N'Estaci�n de Diagn�stico DiagRX Alfa (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU Modelo: Optiplex 7050 Marca: Dell.</label></body></html>', 4, 2, CAST(900.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (32, 1, N'DIAG SMART CPU', N'Estaci�n de Diagn�stico DiagRX SMART (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU Dell para Sistema Diag Smart.</label></body></html>', 4, 2, CAST(979.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (33, 1, N'799501-2', N'Estaci�n de Diagn�stico S/Tarjeta de Video (CPU) XEON', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU Modelo: WORKSTATION DELL PRECISION 7810 825W TORRE Marca: DELL. XEON.</label></body></html>', 4, 2, CAST(2600.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (34, 1, N'799501-3', N'Estaci�n de Diagn�stico S/Tarjeta de Video (CPU) I9', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU WORKSTATION DELL Procesador Intel Core i9-9900K con Intel VT-x (Octa Core) Sin Tarjeta de Video Sin Monitor. XEONI9</label></body></html>', 4, 2, CAST(1647.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (35, 1, N'G0199-4TB-INC8TB', N'Discos Duros Servidor Incremento 4TB 10,000RPM', N'<html><body><font size=''2'' face=''Century Gothic''><label>Discos Duros Servidor Incremento 4TB 10,000RPM.</label></body></html>', 5, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (36, 1, N'3335318', N'Disco estado s�lido 2.5� de 1TB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Disco estado s�lido 2.5� de 1TB.</label></body></html>', 5, 2, CAST(368.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (37, 1, N'3335313', N'Disco mec�nico de 1TB 3.5� 7,500 RPM', N'<html><body><font size=''2'' face=''Century Gothic''><label>Disco mec�nico de 1TB 3.5� 7,500 RPM.</label></body></html>', 5, 2, CAST(105.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (38, 1, N'3335314', N'Disco estado s�lido 2.5� de 500GB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Disco estado s�lido de 2.5� de 500GB.</label></body></html>', 5, 2, CAST(159.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (39, 1, N'G0199-HD1TB', N'Disco duro DELL de 1.8TB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Disco Duro DELL de <b>1.8TB, SAS, 12Gbps, 2.5�</b>, 10.000 RPM Compatible con Servidor Dell PowerEdge R720/R720xd/R730/R730xd</label></body></html>', 5, 2, CAST(716.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (40, 1, N'G0199-HD2TB', N'Disco duro DELL de 2TB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Discos Duro de <b>2TB, Near-Line SAS</b>, 12Gbps, <b>2.5</b>�, 7.200 RPM, Hot-Plug *Compatible solo con servidor Torre</label></body></html>', 5, 2, CAST(767.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (41, 1, N'G0199-HD1.21TB', N'Disco duro DELL', N'<html><body><font size=''2'' face=''Century Gothic''><label>Disco Duro DELL de <b>1.2TB, SAS, 12Gbps, 2.5</b>�, 10.000 RPM Compatible con Servidor Dell PowerEdge R720/R720xd/R730/R730xd</label></body></html>', 5, 2, CAST(716.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (42, 1, N'K9301808A', N'1 Monitor BARCO Color LED 2MP grado cl�nico 24\" Color sin tarjeta de video MDRC2224BMKII', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>2MP</b> grado <b>cl�nico</b>, software de calibraci�n y 3 a�os de garant�a. 24� COLOR. Modelo MDRC2224BMKII.</label></body></html>', 6, 2, CAST(1012.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (43, 1, N'K9602524', N'1 Monitor BARCO Color Led 2MP con tarjeta gr�fica MDNC-2221', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>2MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-2221.</label></body></html>', 6, 2, CAST(2986.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (44, 1, N'K9602530', N'2 Monitores BARCO Color Led 2MP con tarjeta gr�fica MDNC-2221', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>2</b> Monitores <b>BARCO</b> COLOR LED <b>2MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-2221.</label></body></html>', 6, 2, CAST(4730.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (45, 1, N'K9602854', N'1 Monitor BARCO Color LED 3MP con Tarjeta Gr�fica MDNC-3321', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>3MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-3321.</label></body></html>', 6, 2, CAST(4750.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (46, 1, N'K9602876', N'1 Monitor BARCO NIO COLOR LED 3MP EM MDNC-3321', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BARCO</b> NIO COLOR LED <b>3MP</b> grado <b>m�dico</b>, EM. Market sin cubierta frontal, con tarjeta de video, QAWeb software de calibraci�n y 5 a�os de garant�a.</label></body></html>', 6, 2, CAST(3285.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (47, 1, N'K9602858', N'2 Monitores Barco NIO LED 3MP MDNC-3321', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>2</b> Monitores <b>BARCO</b> NIO LED <b>3MP</b> grado <b>m�dico</b> con tarjeta de video MXRT-5600, QAWeb software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-3421</label></body></html>', 6, 2, CAST(8245.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (48, 1, N'K9602730', N'1 Monitor BARCO Color LED 5MP con Tarjeta Gr�fica MDNG5221', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BARCO</b> MONOCROM�TICO LED <b>5MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a.</label> Modelo MDNG5221.</body></html>', 6, 2, CAST(6522.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (49, 1, N'K9602734', N'2 Monitores BARCO Color LED 5MP con Tarjeta Gr�fica MDNG5221', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>2</b> Monitores <b>BARCO</b> MONOCROM�TICOS LED <b>5MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDNG5221.</label></body></html>', 6, 2, CAST(12200.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (50, 1, N'K9602869', N'1 Monitor BARCO Nio Color LED 5.8 MP MDNC6121', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>5.8MP</b> grado <b>m�dico</b> con tarjeta de video MXRT-5600, QAWeb software de calibraci�n y 5 a�os de garant�a. Modelo MDNC-6121.</label></body></html>', 6, 2, CAST(7375.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (51, 1, N'K9602743', N'1 Monitor BARCO Color LED 6MP con Tarjeta Gr�fica MDCC-6330', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BARCO</b> COLOR LED <b>6MP</b> grado <b>m�dico</b> con tarjeta de video, software de calibraci�n y 5 a�os de garant�a. Modelo MDCC-6330.</label></body></html>', 6, 2, CAST(10500.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (52, 1, N'G11S', N'1 Monitor BEACON G11S 19 Monocrom�tico 1MP', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BEACON</b> G11S 19 Monocrom�tico <b>1MP</b></label></body></html>', 6, 2, CAST(1111.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (53, 1, N'HL2416SH', N'1 Monitor BEACON-BIGTIDE 24� LED', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BEACON-BIGTIDE</b> de 24� <b>2.4MP</b> grado <b>cl�nico</b></label></body></html>', 6, 2, CAST(580.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (54, 1, N'C32SP+', N'1 Monitor BEACON 3MP Color', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BEACON</b> de 21.3� <b>3MP</b> Color grado <b>m�dico</b> con pantalla protectora y sensor frontal. Incluye adaptador de ca/cc. Modelo GSM120A12.</label></body></html>', 6, 2, CAST(2380.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (55, 1, N'G52SP+', N'1 Monitor BEACON 5MP Grayscale', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BEACON</b> de 21.3� <b>5MP</b> Grayscale grado <b>m�dico</b> con pantalla protectora y sensor frontal. Incluye adaptador de ca/cc. Modelo GSM90A12.</label></body></html>', 6, 2, CAST(4880.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (56, 1, N'C53SP+', N'1 Monitor BEACON 5MP Color', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>1</b> Monitor <b>BEACON</b> de 21.3� <b>5MP</b> Color grado <b>m�dico</b> con pantalla protectora y sensor frontal. Incluye adaptador de ca/cc. Modelo GSM90A12</label></body></html>', 6, 2, CAST(5080.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (57, 1, N'E413198', N'Elo Touch X - Serie 15�', N'<html><body><font size=''2'' face=''Century Gothic''><label>Pantalla LED T�ctil de 15� con procesador Intel Core i3, almacenaje 138GB, Memoria interna 4GB, 9 puertos USB 2.0, Peso aproximado 7.4 kg, Altura 34.7 cm, Ancho 36.5cm Profundidad 6.6cm.</label></body></html>', 7, 2, CAST(1351.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (58, 1, N'1509L', N'Monitor Elo 15�', N'<html><body><font size=''2'' face=''Century Gothic''><label>Monitor ELOTOUCH de 15�.</label></body></html>', 7, 2, CAST(307.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (59, 1, N'3335706', N'Monitor panel plano led digital 23', N'<html><body><font size=''2'' face=''Century Gothic''><label>MONITOR PANEL PLANO LED DIGITAL 21� FULLHD Resoluci�n 1920 x 1080. Especial para aplicaciones HIS/RIS.</label></body></html>', 7, 2, CAST(156.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (60, 1, N'320-1092', N'Monitor 19� 1MP P1914S', N'<html><body><font size=''2'' face=''Century Gothic''><label>Monitor Marca DELL de 19�.</label></body></html>', 7, 2, CAST(209.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (61, 1, N'320-4687', N'Monitor Dell 19� Ultra Sharp', N'<html><body><font size=''2'' face=''Century Gothic''><label>Monitor Dell 19� Ultra Sharp.</label></body></html>', 7, 2, CAST(94.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (62, 1, N'3335113', N'CPU Robot Quemador', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU Robot Quemador Marca DELL.</label></body></html>', 9, 2, CAST(790.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (63, 1, N'D0060', N'Robot Quemador Lite 20 CDS Rimage', N'<html><body><font size=''2'' face=''Century Gothic''><label>Robot quemador con capacidad de alimentaci�n de 20 discos (CD/DVD).</label></body></html>', 9, 1, CAST(2724.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (64, 1, N'3335209', N'Publicador Discos Allegro20', N'<html><body><font size=''2'' face=''Century Gothic''><label>Publicador autom�tico de discos marca Rimage modelo Allegro 20, 1 a�o de garant�a.</label></body></html>', 9, 2, CAST(2144.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (65, 1, N'3335504', N'Allegro20 Mantenimiento Anual', N'<html><body><font size=''2'' face=''Century Gothic''><label>P�liza de mantenimiento correctivo 12 meses a partir del 2�. a�o para Allegro20.</label></body></html>', 9, 3, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (66, 1, N'D0074-1T', N'Robot Quemador Lite 100 CD / DVD Rimage', N'<html><body><font size=''2'' face=''Century Gothic''><label>Robot quemador con capacidad de alimentaci�n de 100 discos (CD/DVD) con una tinta.</label></body></html>', 9, 1, CAST(2956.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (67, 1, N'3335210', N'Publicador Discos Allegro100', N'<html><body><font size=''2'' face=''Century Gothic''><label>Publicador autom�tico de discos marca Rimage modelo Allegro 100, 1 a�o de garant�a.</label></body></html>', 9, 2, CAST(3686.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (68, 1, N'3335505', N'Allegro100 Mantenimiento Anual', N'<html><body><font size=''2'' face=''Century Gothic''><label>P�liza de mantenimiento correctivo 12 meses a partir del 2�. a�o para Allegro100.</label></body></html>', 9, 3, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (69, 1, N'3335211', N'Publicador Discos Catalyst 6000N', N'<html><body><font size=''2'' face=''Century Gothic''><label>Publicador autom�tico de discos Catalyst 6000N, 3 bandejas de 50 discos de entrada cada una, computadora incluida y paquete de 100 CD''s.</label></body></html>', 9, 2, CAST(9578.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (70, 1, N'3335506', N'Catalyst 6000N Mantenimiento Anual', N'<html><body><font size=''2'' face=''Century Gothic''><label>P�liza de mantenimiento correctivo 12 meses a partir del 2�. a�o para Catalyst 6000N.</label></body></html>', 9, 3, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (71, 1, N'3335207', N'Publicador Epson PP-50', N'<html><body><font size=''2'' face=''Century Gothic''><label>Publicador autom�tico de discos marca Epson modelo PP-50, 1 a�o de garant�a.</label></body></html>', 9, 2, CAST(2221.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (72, 1, N'3335208', N'Publicador Epson PP-100II', N'<html><body><font size=''2'' face=''Century Gothic''><label>Publicador autom�tico de discos marca Epson modelo PP-100II, 1 a�o de garant�a.</label></body></html>', 9, 2, CAST(3172.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (73, 1, N'D0060-R', N'Publicador Rimage  20 discos (CD/DVD)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Robot quemador Allegro con capacidad de alimentaci�n de 20 discos (CD/DVD) una sola tinta modo impresi�n Inyecci�n. Incluye CPU, Nobreak, 600 CDs Imprimibles y una tinta de cada cartucho</label></body></html>', 9, 1, CAST(4996.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (74, 1, N'D0060-50E', N'Publicador Epson 50 discos (CD/DVD)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Robot quemador PP-50 con capacidad de alimentaci�n de 50 discos (CD/DVD) m�ltiples tintas modo Impresi�n Inyecci�n. Incluye CPU, Nobreak, 600 CDs Imprimibles y una tinta de cada cartucho</label></body></html>', 9, 1, CAST(5000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (75, 1, N'D0074-1T-R', N'Publicador Rimage 100 discos (CD/DVD)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Robot quemador Allegro con capacidad de alimentaci�n de 100 discos (CD/DVD), una sola tinta modo impresi�n Inyecci�n. Incluye CPU, Nobreak, 600 CDs Imprimibles y una tinta de cada cartucho</label></body></html>', 9, 1, CAST(5761.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (76, 1, N'D0074-1T-E', N'Publicador Epson 100 discos (CD/DVD)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Robot quemador PP-100II con capacidad de alimentaci�n de 100 discos (CD/DVD) m�ltiples tintas modo Impresi�n Inyecci�n. Incluye CPU, Nobreak, 600 CDs Imprimibles y una tinta de cada cartucho</label></body></html>', 9, 1, CAST(5761.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (77, 1, N'D0074-1T-150R', N'Publicador Rimage 150 discos (CD/DVD)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Robot quemador Catalyst 6000N con capacidad de alimentaci�n de 100 discos (CD/DVD) Impresi�n Transferencia t�rmica. Incluye CPU, Nobreak, 1000 CDs Imprimibles y dos cintas color para transferencia t�rmica</label></body></html>', 9, 1, CAST(14000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (78, 1, N'A0168', N'Sistema de Turnos Kiosko Completo', N'<html><body><font size=''2'' face=''Century Gothic''><label>Sistema de Turnos Kiosko Completo.</label></body></html>', 10, 2, CAST(4999.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (79, 1, N'D0216', N'Sistema Turnos Kiosko Impresora', N'<html><body><font size=''2'' face=''Century Gothic''><label>Sistema de Turnos Kiosko Impresora <b>TTP2030.</b></label></body></html>', 10, 1, CAST(2940.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (80, 1, N'A0160', N'Zebra TTP 2030 Kiosk Printer USB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Zebra TTP 2030 Kiosk Printer USB.</label></body></html>', 10, 2, CAST(640.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (81, 1, N'A0163', N'Zebra Kiosk Print Station', N'<html><body><font size=''2'' face=''Century Gothic''><label>Impresora t�rmica.</label></body></html>', 10, 2, CAST(533.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (82, 1, N'D0218', N'Sistema Turnos Kiosko Lector', N'<html><body><font size=''2'' face=''Century Gothic''><label>Sistema de Turnos Kiosko Lector.</label></body></html>', 10, 1, CAST(2241.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (83, 1, N'D0217', N'Sistema Turnos Controlador TV', N'<html><body><font size=''2'' face=''Century Gothic''><label>Sistema de Turnos Controlador TV.</label></body></html>', 10, 1, CAST(1199.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (84, 1, N'NEO-Z83-4', N'Computadora Minix', N'<html><body><font size=''2'' face=''Century Gothic''><label>Computadora Minix.</label></body></html>', 10, 2, CAST(349.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (85, 1, N'3330216', N'Cable HDMI - HDMI 15m', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cable HDMI 15m.</label></body></html>', 10, 2, CAST(50.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (86, 1, N'177146', N'Video Spliter HDMI 1in:4out', N'<html><body><font size=''2'' face=''Century Gothic''><label>Accesorio especializado para dividir una se�al HDMI Marca Manhattan.</label></body></html>', 10, 2, CAST(154.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (87, 1, N'USB2VGAE2', N'Cable USB - VGA', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cable USB - VGA.</label></body></html>', 10, 2, CAST(56.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (88, 1, N'D0090', N'Sistema Turnos Controlador TV Simple', N'<html><body><font size=''2'' face=''Century Gothic''><label>Sistema Turnos Controlador TV Lite.</label></body></html>', 10, 1, CAST(502.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (89, 1, N'D0227', N'Sistema Turnos Terminal T�cnico Pared', N'<html><body><font size=''2'' face=''Century Gothic''><label>Sistema de Turnos Terminal T�cnico Pared.</label></body></html>', 10, 1, CAST(879.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (90, 1, N'200645', N'Caja Soporte Monitor', N'<html><body><font size=''2'' face=''Century Gothic''><label>Caja Soporte Monitor.</label></body></html>', 10, 2, CAST(159.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (91, 1, N'D0228', N'Sistema Turnos Terminal T�cnico Mesa', N'<html><body><font size=''2'' face=''Century Gothic''><label>Sistema Turnos Terminal T�cnico Mesa.</label></body></html>', 10, 1, CAST(879.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (92, 1, N'200670', N'Base de Sistema de Turnos para Mesa', N'<html><body><font size=''2'' face=''Century Gothic''><label>Base de Sistema de Turnos para Mesa.</label></body></html>', 10, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (93, 1, N'FX 2000', N'Tarjeta de video Nvidia k2000/k2200', N'<html><body><font size=''2'' face=''Century Gothic''><label>Tarjeta Video 2 GB Doble salida display port 1.4.</label></body></html>', 11, 2, CAST(200.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (94, 1, N'799508', N'Etiqueta de identificaci�n DIAG-RX', N'<html><body><font size=''2'' face=''Century Gothic''><label>Etiqueta de identificaci�n DIAG-RX.</label></body></html>', 11, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (95, 1, N'3330127', N'Convertidor HUBELL', N'<html><body><font size=''2'' face=''Century Gothic''><label>Convertidor HUBELL.</label></body></html>', 11, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (96, 1, N'3330104', N'Cable USB - Impresora', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cable USB - Impresora.</label></body></html>', 11, 2, CAST(0.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (97, 1, N'SDOCK2U313', N'Unidad para respaldos baja productividad', N'<html><body><font size=''2'' face=''Century Gothic''><label>Unidad para respaldos baja productividad</label></body></html>', 11, 2, CAST(101.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (98, 1, N'WX5100-8GB', N'Tarjeta Video Radeon Pro WX5100 8GB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Tarjeta Video Radeon Pro WX5100 8GB</label></body></html>', 11, 2, CAST(1600.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (99, 1, N'PYN4000-8G', N'Tarjeta Video NVIDIA PYN4000 8GB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Tarjeta Video NVIDIA PYN4000 8GB</label></body></html>', 11, 2, CAST(805.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (100, 1, N'K9306036', N'Controlador de pantalla PCle 3D de 4 cabezales de medio alcance MXRT-5600 4GB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Controlador de pantalla PCle 3D de 4 cabezales de medio alcance MXRT-5600 4GB</label></body></html>', 11, 2, CAST(750.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (101, 1, N'K9306041', N'Controlador de pantalla compacto 3D PCle MXRT-2600 2GB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Controlador de pantalla compacto 3D PCle MXRT-2600 2GB</label></body></html>', 11, 2, CAST(380.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (102, 1, N'G0136', N'Terminal adicional para RIS / WebServex', N'<html><body><font size=''2'' face=''Century Gothic''><label>Terminal adicional para RIS / WebServex.</label></body></html>', 12, 1, CAST(1013.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (103, 1, N'A0137', N'Terminal adicional para Quir�fano', N'<html><body><font size=''2'' face=''Century Gothic''><label>Terminal adicional para Quir�fano.</label></body></html>', 12, 1, CAST(1260.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (104, 1, N'224-7707', N'Terminal adicional para RIS / WebServex (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>CPU RIS/WEBSERVEX.</label></body></html>', 12, 2, CAST(895.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (105, 1, N'210-AATO', N'Terminal adicional para Quir�fano (CPU)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Estaci�n de Quir�fano.</label></body></html>', 12, 2, CAST(874.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (106, 1, N'D0095', N'Tableta iPad 9.7� Retina', N'<html><body><font size=''2'' face=''Century Gothic''><label>Tableta iPad 9.7� Retina.</label></body></html>', 12, 2, CAST(458.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (107, 1, N'D0097', N'Tableta iPad 12.9� Retina', N'<html><body><font size=''2'' face=''Century Gothic''><label>Tableta iPad 12.9� Retina.</label></body></html>', 12, 2, CAST(1250.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (108, 1, N'A0140', N'Tableta Windows', N'<html><body><font size=''2'' face=''Century Gothic''><label>Tableta Windows 10�.</label></body></html>', 12, 2, CAST(589.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (109, 1, N'D0221', N'Impresora Zebra GC420', N'<html><body><font size=''2'' face=''Century Gothic''><label>Impresora Zebra <b>GC420T</b>.</label></body></html>', 13, 1, CAST(724.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (110, 1, N'D0221-1', N'Impresora Zebra GC420', N'<html><body><font size=''2'' face=''Century Gothic''><label>Impresora de transferencia t�rmica <b>GC420</b>.</label></body></html>', 13, 2, CAST(338.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (111, 1, N'LS2208', N'Lector de c�digo de barras pistola', N'<html><body><font size=''2'' face=''Century Gothic''><label>Lector de C�digo de Barras Pistola.</label></body></html>', 13, 2, CAST(126.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (112, 1, N'DS9208-SR4NNU21Z', N'Lector C�digo de Barras Fijo', N'<html><body><font size=''2'' face=''Century Gothic''><label>Lector de C�digo de Barras Fijo.</label></body></html>', 10, 2, CAST(291.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (113, 1, N'DS9208-BASE', N'Base para Lector C�digo de Barras Fijo', N'<html><body><font size=''2'' face=''Century Gothic''><label>Base Lector de C�digo de Barras Fijo.</label></body></html>', 10, 2, CAST(10.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (114, 1, N'FPAEUSB2-S', N'Pedal de grabaci�n para transcripci�n', N'<html><body><font size=''2'' face=''Century Gothic''><label>Pedal de grabaci�n para transcripci�n.</label></body></html>', 13, 2, CAST(85.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (115, 1, N'A0147', N'Diadema de dictador de bluetooth', N'<html><body><font size=''2'' face=''Century Gothic''><label>Diadema bluetooth para dictado.</label></body></html>', 13, 2, CAST(244.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (116, 1, N'LFH5276', N'Micr�fono para reconocimiento de voz speechmike', N'<html><body><font size=''2'' face=''Century Gothic''><label>Micr�fono para reconocimiento de voz SpeechMike.</label></body></html>', 13, 2, CAST(300.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (117, 1, N'HP2460', N'Impresora Deskjet HP 2515', N'<html><body><font size=''2'' face=''Century Gothic''><label>Impresora de papel.</label></body></html>', 13, 2, CAST(380.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (118, 1, N'G0141', N'Esc�ner para RIS', N'<html><body><font size=''2'' face=''Century Gothic''><label>Esc�ner para RisX.</label></body></html>', 13, 2, CAST(217.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (119, 1, N'D0229', N'Impresora Zebra ZD510-HC Brazaletes', N'<html><body><font size=''2'' face=''Century Gothic''><label>Impresora Zebra <b>ZD510-HC</b> Brazaletes.</label></body></html>', 13, 2, CAST(558.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (120, 1, N'D0062', N'Discos de 500 GB (2 pzas) Para Disaster Recovery', N'<html><body><font size=''2'' face=''Century Gothic''><label>Discos de 500GB (2 pzas.) para dispositivo externo de almacenamiento para im�genes(Disaster Recovery).</label></body></html>', 14, 2, CAST(370.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (121, 1, N'3335950', N'Cartucho de tinta All in one para robot publicador Allegro 20 y Allegro 100', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cartucho de tinta All in one para robot publicador Allegro 20 y Allegro 100.</label></body></html>', 14, 2, CAST(72.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (122, 1, N'3335951', N'Paquete 100 CDs Inyecci�n Tinta (Servimex)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Paquete 100 CDs Inyecci�n Tinta (Servimex).</label></body></html>', 14, 2, CAST(30.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (123, 1, N'3335901', N'Paquete 100 DVDs Inyecci�n Tinta (Servimex)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Paquete 100 DVDs Inyecci�n Tinta (Servimex).</label></body></html>', 14, 2, CAST(30.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (124, 1, N'3335902', N'Paquete 1,000 CDs Catalyst 6000N', N'<html><body><font size=''2'' face=''Century Gothic''><label>2x cintas para impresi�n t�rmica a color, 2 cintas transparentes para el acabado y 1,000 CD''s t�rmicos.</label></body></html>', 14, 2, CAST(833.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (125, 1, N'3335903', N'Paquete 1,000 DVDs Catalyst 6000N', N'<html><body><font size=''2'' face=''Century Gothic''><label>2x cintas para impresi�n t�rmica a color, 2 cintas transparentes para el acabado y 1,000 DVD�s t�rmicos.</label></body></html>', 14, 2, CAST(879.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (126, 1, N'C13S020447', N'Cartucho Epson Cyan', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cartucho Epson Lantana <b>Cyan</b>.</label></form></body></html><br>Compatible con: PP50 y PP-100.', 14, 2, CAST(39.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (127, 1, N'C13S020448', N'Cartucho Epson Cyan Light', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cartucho Epson Lantana <b>Cyan Light</b>.</label></body></html><br>Compatible con: PP50 y PP-100.', 14, 2, CAST(39.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (128, 1, N'C13S020449', N'Cartucho Epson Magenta Light', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cartucho Epson Lantana <b>Magenta Light</b>.</label></body></html><br>Compatible con: PP50 y PP-100.', 14, 2, CAST(39.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (129, 1, N'C13S020450', N'Cartucho Epson Magenta', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cartucho Epson Lantana <b>Magenta</b>.</label></body></html><br>Compatible con: PP50 y PP-100.', 14, 2, CAST(39.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (130, 1, N'C13S020451', N'Cartucho Epson Amarillo', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cartucho Epson Lantana <b>Amarillo</b>.</label></body></html><br>Compatible con: PP50 y PP-100.', 14, 2, CAST(39.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (131, 1, N'C13S020452', N'Cartucho Epson Negro', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cartucho Epson Lantana <b>Negro</b>.</label></body></html><br>Compatible con: PP50 y PP-100.', 14, 2, CAST(39.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (132, 1, N'D0222', N'8 x Rollos de Etiquetas Adheribles Zebra GC420 (50.8x25.4mm)', N'<html><body><font size=''2'' face=''Century Gothic''><label>8x Rollos Etiquetas Adheribles Impresora Zebra <b>GC420T</b> de 50.8x25.4mm. 16,800 Total Etiquetas / 2100 por Rollo.</label></body></html>', 14, 2, CAST(200.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (133, 1, N'D0223', N'12x Rollos Impresi�n Zebra GC420 Zebra GC420 (63.5mmx70m)', N'<html><body><font size=''2'' face=''Century Gothic''><label>12x Rollos Impresi�n Impresora Zebra <b>GC420T</b> para etiquetas adheribles de (63.5mmx70m).</label></body></html>', 14, 2, CAST(100.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (134, 1, N'10005851', N'6x Rollos de Etiquetas Adheribles Zebra GC420 (101.6x50.8mm)', N'<html><body><font size=''2'' face=''Century Gothic''><label>6x Rollos de Etiquetas Adheribles (101.6x50.8mm). Transferencia T�rmica 7,920 Total Etiquetas / 1320 por Rollo.</label></body></html>', 14, 2, CAST(98.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (135, 1, N'06000GS11007', N'6x Rollos Impresi�n Zebra GC420 (110mmx70m)', N'<html><body><font size=''2'' face=''Century Gothic''><label>6x Rollos Tinta Impresora (110mmx70m).</label></body></html>', 14, 2, CAST(53.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (136, 1, N'10005850', N'6x Rollos de Etiqueta Adheribles Zebra GC420 (50.8x25.4mm)', N'<html><body><font size=''2'' face=''Century Gothic''><label>6x Rollos de Etiqueta Adheribles (50.8x25.4mm). Transferencia T�rmica 14,940 Total Etiquetas / 2490 por Rollo.</label></body></html>', 14, 2, CAST(98.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (137, 1, N'10010063', N'6x Rollos de Etiqueta Adheribles Zebra GC420 (57.2x31.8mm)', N'<html><body><font size=''2'' face=''Century Gothic''><label>6x Rollos de Etiqueta Adheribles (57.2x31.8mm), Ultra resistente. Transferencia T�rmica 12,000 Total Etiquetas / 2000 por Rollo.</label></body></html>', 14, 2, CAST(98.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (138, 1, N'06000GS06407', N'6x Rollos Impresi�n Zebra GC420 (64mmx70m)', N'<html><body><font size=''2'' face=''Century Gothic''><label>6x Rollos Tinta Impresora (64mmx70m).</label></body></html>', 14, 2, CAST(53.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (139, 1, N'A0164', N'Zebra TTP Rollo Perform 1000d 3.5', N'<html><body><font size=''2'' face=''Century Gothic''><label>8x Rollos Zebra <b>TTP2030</b> 1000D 3.5 MIL RECEIPT PAPER (Consumible para D0218 Sistema de Turnos Kiosko Impresora).</label></body></html>', 14, 2, CAST(135.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (140, 1, N'3335204', N'Unidad de CD/DVD para Robot Quemador Bravo', N'<html><body><font size=''2'' face=''Century Gothic''><label>Unidad de CD/DVD para Robot Quemador Bravo.</label></body></html>', 14, 2, CAST(162.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (141, 1, N'D0229-B', N'Brazaletes impresora Zebra ZD510-HC', N'<html><body><font size=''2'' face=''Century Gothic''><label>Brazaletes para impresi�n t�rmica con adhesivo permanente de 1''''x11''''para impresora Zebra <b>ZD510-HC</b>.</label></body></html>', 14, 2, CAST(371.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (142, 1, N'63722-CABEZAL4T', N'Cabezal de impresora robot 4100', N'<html><body><font size=''2'' face=''Century Gothic''><label>Cabezal de impresora robot 4100 Series</label></body></html>', 14, 2, CAST(71.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (143, 1, N'HDDCASE25BK', N'1 Caja protectora para discos SSD', N'<html><body><font size=''2'' face=''Century Gothic''><label>1 Caja protectora para discos SSD</label></body></html>', 14, 2, CAST(6.50 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (144, 1, N'223-4678', N'Unidad de Disco Extra�ble USB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Unidad para almacenamiento externo de im�genes USB PowerVault RD1000 alta productividad con disco (Disaster Recovery).</label></body></html>', 14, 2, CAST(3016.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (145, 1, N'INTERNET750V', N'No break tripplite 750v', N'<html><body><font size=''2'' face=''Century Gothic''><label>NO Break TRIPPLITE 750V Modelo: INTERNET750U Marca: Tripp Litte.</label></body></html>', 15, 2, CAST(111.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (146, 1, N'SUA2200RM2U', N'No Break APC 2200VA', N'<html><body><font size=''2'' face=''Century Gothic''><label>NO-BREAK SMART-UPS APC 2200VA / 1980W /.</label></body></html>', 15, 2, CAST(1100.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (147, 1, N'3333103', N'No Break Tripplite 1.5 kVA', N'<html><body><font size=''2'' face=''Century Gothic''><label>No Break TRIPPLITE 1.5KVA 8 contactos.</label></body></html>', 15, 2, CAST(204.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (148, 1, N'PW9130L200 0T-XL', N'UPS PW-9130 2000VA', N'<html><body><font size=''2'' face=''Century Gothic''><label>UPS PW-9130 2000VA.</label></body></html>', 15, 2, CAST(1073.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (149, 1, N'A0103', N'Escritorio para estaci�n de diagn�stico DIAGRX', N'<html><body><font size=''2'' face=''Century Gothic''><label>Escritorio para estaci�n de diagn�stico DiagRX.</label></body></html>', 16, 2, CAST(350.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (150, 1, N'AZ-TASK', N'Silla', N'<html><body><font size=''2'' face=''Century Gothic''><label>Silla ejecutiva ergon�mica con ruedas.</label></body></html>', 16, 2, CAST(150.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (151, 1, N'G0143', N'Smart Rack 4 posters, 19�, 25U Tripplite', N'<html><body><font size=''2'' face=''Century Gothic''><label>Rack para servidores 4 POSTES 19�, 25U.</label></body></html>', 16, 2, CAST(944.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (152, 1, N'D0231', N'Rack para PACS Enterprise PRO DELL', N'<html><body><font size=''2'' face=''Century Gothic''><label>Rack ENTERPRISE PRO DELL para servidores 4 POSTES.</label></body></html>', 16, 2, CAST(2779.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (153, 1, N'D0232', N'Consola para RACK PRO con Monitor y Teclado', N'<html><body><font size=''2'' face=''Century Gothic''><label>Consola para RACK PRO con Monitor y Teclado.</label></body></html>', 16, 2, CAST(1345.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (154, 1, N'A0101', N'Mesa chica para terminal RISX', N'<html><body><font size=''2'' face=''Century Gothic''><label>Mesa chica para terminal RISX.</label></body></html>', 16, 2, CAST(180.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (155, 1, N'D0069', N'Rack para PACS ENTREPRISE', N'<html><body><font size=''2'' face=''Century Gothic''><label>Rack para servidores 4 POSTES 19�.</label></body></html>', 16, 2, CAST(1800.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (156, 1, N'D0066-8TB', N'Caja de almacenamiento con capacidad de 8TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Caja de almacenamiento</b> para incremento de la capacidad del almacenamiento <b>8TB</b>. Incluye software para crecimiento de la base existente.</label></body></html>', 19, 2, CAST(15489.00 AS Decimal(10, 2)), 2, NULL, 8, 6.4, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (157, 1, N'D0066-14TB', N'Caja de almacenamiento con capacidad de 14TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Caja de almacenamiento</b> para incremento de la capacidad del almacenamiento <b>14TB</b>. Incluye software para crecimiento de la base existente.</label></body></html>', 19, 2, CAST(18488.00 AS Decimal(10, 2)), 2, NULL, 14, 11.2, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (158, 1, N'D0066-20TB', N'Caja de almacenamiento con capacidad de 20TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Caja de almacenamiento</b> para incremento de la capacidad del almacenamiento <b>20TB</b>. Incluye software para crecimiento de la base existente.</label></body></html>', 19, 2, CAST(24488.00 AS Decimal(10, 2)), 2, NULL, 20, 16, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (159, 1, N'D0066-28TB', N'Caja de almacenamiento con capacidad de 28TB', N'<html><body><font size=''2'' face=''Century Gothic''><label><b>Caja de almacenamiento</b> para incremento de la capacidad del almacenamiento <b>28TB</b>. Incluye software para crecimiento de la base existente.</label></body></html>', 19, 2, CAST(28488.00 AS Decimal(10, 2)), 2, NULL, 28, 22.4, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (160, 1, N'SG0199-4TB', N'Licencia servidor SERVEX 4TB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia servidor SERVEX 4TB.</label></body></html>', 21, 3, CAST(3300.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (161, 1, N'SG0199-8TB', N'Licencia servidor SERVEX 8TB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia servidor SERVEX 8TB.</label></body></html>', 21, 3, CAST(4050.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (162, 1, N'SG0199-12TB', N'Licencia servidor SERVEX 12TB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia servidor SERVEX 12TB.</label></body></html>', 21, 3, CAST(4800.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (163, 1, N'SG0199-16TB', N'Licencia servidor SERVEX 16TB', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia servidor SERVEX 16TB.</label></body></html>', 21, 3, CAST(5550.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (164, 1, N'SG199-NUBE', N'Licencia servidor Teleradiolog�a/Nube', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia servidor Teleradiolog�a/Nube.</label></body></html>', 21, 3, CAST(263.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (165, 1, N'CAD-001', N'Licencia de Software CadOne', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia servidor detecci�n asistida por computadora para estudios de mastograf�a. *Sin l�mite de modalidades..</label></body></html>', 21, 3, CAST(11071.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (166, 1, N'W-DIAGRX1', N'Licencia simple concurrente para visor de radiolog�a', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia simple concurrente para visor de radiolog�a multimodalidad. Incluye todas las herramientas de post-procesamiento, protocolos de carga, software para mastograf�a, sincronizaci�n de series, MPR , MIP y 3D b�sico para tomograf�a, m�dulo de reporte con plantillas y m�dulo de dictado con transcripci�n.</label></body></html>', 22, 3, CAST(3750.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (167, 1, N'W-3D-VAS', N'Licencia concurrente de paquete Vascular Avanzado', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia concurrente de paquete <b>Vascular Avanzado.</b> Visualizaci�n de vistas en m�ltiples planos y volumen avanzado (Volume Rendering) con modelos tridimensionales, plantillas, herramientas de an�lisis vascular como segmentaci�n de vasos y detecci�n de estenosis.</label></body></html>', 22, 3, CAST(5271.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (168, 1, N'W-3D-VASMA', N'Soporte y mantenimiento anual para paquete Vascular Avanzado', N'<html><body><font size=''2'' face=''Century Gothic''><label>Soporte y mantenimiento anual para paquete Vascular Avanzado posterior al primer a�o de garant�a.</label></body></html>', 22, 3, CAST(750.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (169, 1, N'W-3D-NUC', N'Licencia concurrente de paquete Nuclear Avanzado', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia concurrente de paquete <b>Nuclear Avanzado.</b> Fusi�n de tomograf�a computada (CT) con gammagraf�a o bien tomograf�a de emisi�n de positrones (PT) utilizando diferentes paletas de colores para facilitar an�lisis funcional de los �rganos.</label></body></html>', 22, 3, CAST(2181.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (170, 1, N'W-3D-NUCMA', N'Soporte y mantenimiento anual para paquete Nuclear', N'<html><body><font size=''2'' face=''Century Gothic''><label>Soporte y mantenimiento anual para paquete Nuclear Avanzado posterior al 1 a�o de garant�a.</label></body></html>', 22, 3, CAST(300.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (171, 1, N'W-3D-CAR', N'Licencia concurrente Cardio 2D', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia concurrente <b>Cardio 2D.</b> Licencia concurrente de paquete CARDIO 2D. Incluye Visualizaci�n de Electrocardiograma de 12 derivaciones, general y ambulatorio, an�lisis coronario cuantitativo (QCA), an�lisis ventricular izquierdo (LVA) y la substracci�n digital (DSA).</label></body></html>', 22, 3, CAST(4393.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (172, 1, N'W-3D-CARMA', N'Licencia CARDIO 2D Soporte Anual', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia CARDIO 2D Soporte Anual. Soporte y mantenimiento anual para licencia de CARDIO 2D posterior al 1 a�o de garant�a.</label></body></html>', 22, 3, CAST(600.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (173, 1, N'W-3D-DEN', N'Licencia concurrente Dental', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia concurrente <b>Dental</b>. Licencia concurrente de paquete DENTAL. Incluye Generaci�n autom�tica de ortopantomograf�as, vistas paraxiales, posicionamiento del canal mandibular con marcado manual del nervio, reconstrucci�n con coloreado del Volumen 3D, herramientas de segmentaci�n avanzada y visualizaci�n de planos transversales a la ortopantomograf�a, proyecci�n MIP/Average.</label></body></html>', 22, 3, CAST(2181.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (174, 1, N'W-3D-DENMA', N'Licencia DENTAL Soporte Anual', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia DENTAL Soporte Anual. Soporte y mantenimiento anual para licencia de DENTAL posterior al 1 a�o de garant�a.</label></body></html>', 22, 3, CAST(300.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (175, 1, N'54300-90', N'Licencia concurrente de paquete Planificaci�n Preoperatoria', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia concurrente de paquete <b>Planificaci�n Preoperatoria</b> para cirujanos ortop�dicos con colecci�n de plantillas de los fabricantes m�s comunes, m�dulos de planificaci�n y herramientas digitales para mediciones, fijaci�n de pr�tesis, simulaci�n de osteotom�as y reducci�n de fracturas mediante asistentes inteligentes.</label></body></html>', 22, 3, CAST(11544.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (176, 1, N'54300-90MA', N'Licencia Planificaci�n Preoperatoria Soporte Anual', N'<html><body><font size=''2'' face=''Century Gothic''><label>Soporte y mantenimiento anual para paquete Planificaci�n Preoperatoria Avanzado posterior al primer a�o de garant�a.</label></body></html>', 22, 3, CAST(1197.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (177, 1, N'W-SERVEX', N'Licenciamiento concurrente para visor web (multiplataforma) de m�dico referente', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licenciamiento concurrente para visor web (multiplataforma) de m�dico referente. (Sin l�mite de usuarios).</label></body></html>', 23, 3, CAST(12750.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (178, 1, N'W-SERVEXC', N'Licenciamiento concurrente para visor web en PACS CORE', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licenciamiento concurrente para visor web en PACS CORE Torre 4TB (multiplataforma) de m�dico referente.</label></body></html>', 23, 3, CAST(3750.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (179, 1, N'W-NAVDIAG', N'Licencia concurrente para visor web de revisi�n multimodalidad', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia concurrente para visor web de revisi�n multimodalidad. Incluye todas las herramientas de post-procesamiento, m�dulo de impresi�n, m�dulo de reporte con plantillas, m�dulo de dictado con transcripci�n.</label></body></html>', 23, 3, CAST(900.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (180, 1, N'W-PAC', N'Licenciamiento concurrente para visor web en PACS ENCORE', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia concurrente para visor web de consulta de estudios multimodalidad, para paciente. *Se recomienda usar con este producto, servidor de aplicaciones. Solo disponible para PACS ENCORE.</label></body></html>', 23, 3, CAST(12750.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (181, 1, N'G0125', N'Software de Sistema de Informaci�n Radiol�gica (RISX)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Software de Sistema de Informaci�n Radiol�gica (RISX). Incluye m�dulo completo para el flujo de la administraci�n de los datos demogr�ficos del paciente, agenda, lista de estudios, administraci�n de salas, lista de trabajo del t�cnico radi�logo, lista de trabajo de estudios realizados, estad�sticas de productividad y Modality Work List.</label></body></html>', 24, 3, CAST(11250.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (182, 1, N'D0057-L', N'Software de Sistema de Informaci�n Radiol�gica (RisX Lite)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Software de Sistema de Informaci�n Radiol�gica (RisX Lite). Incluye agenda y lista de trabajo Modality Work List. (Para PACS Core).</label></body></html>', 24, 3, CAST(3750.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (183, 1, N'G0125T', N'M�dulo de Turnos', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Turnos.</label></body></html>', 25, 3, CAST(2250.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (184, 1, N'G0125S', N'M�dulo de Solicitudes electr�nicas', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Solicitudes electr�nicas.</label></body></html>', 25, 3, CAST(1500.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (185, 1, N'G0125QRF', N'M�dulo de Quir�fano', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Quir�fano.</label></body></html>', 25, 3, CAST(15000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (186, 1, N'G0125URG', N'M�dulo de Urgencias', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Urgencias.</label></body></html>', 25, 3, CAST(15000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (187, 1, N'G0125HSP', N'M�dulo de Hospitalizaci�n', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Hospitalizaci�n.</label></body></html>', 25, 3, CAST(15000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (188, 1, N'G0125CON', N'M�dulo de Consulta Externa', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Consulta Externa.</label></body></html>', 25, 3, CAST(15000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (189, 1, N'G0125PRP', N'M�dulo de Portal de Pacientes', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Portal de Pacientes.</label></body></html>', 25, 3, CAST(15000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (190, 1, N'G0125LAB', N'M�dulo de Laboratorio', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Laboratorio.</label></body></html>', 25, 3, CAST(15000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (191, 1, N'G0125LAB-I', N'M�dulo de Integraci�n Equipo Laboratorio', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Integraci�n Equipo Laboratorio.</label></body></html>', 25, 3, CAST(7500.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (192, 1, N'G0125PAT', N'M�dulo de Patolog�a', N'<html><body><font size=''2'' face=''Century Gothic''><label>M�dulo de Patolog�a.</label></body></html>', 25, 3, CAST(15000.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (193, 1, N'D0108', N'Licencia Admin Nube Bit Defender para estaci�n', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia Admin Nube Bit Defender para estaci�n.</label></body></html>', 26, 3, CAST(54.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (194, 1, N'D0107', N'Licencia Admin Nube Bit Defender para Servidor', N'<html><body><font size=''2'' face=''Century Gothic''><label>Licencia Admin Nube Bit Defender para Servidor.</label></body></html>', 26, 3, CAST(160.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (195, 1, N'D2019-N1', N'Licencia nominal para reconocimiento de voz VOCALI en (SERVIDOR NUBE)', N'<html><body><font size=''2'' face=''Century Gothic''><label>1 Licencia <b>nominal</b> para reconocimiento de voz VOCALI en <b>(SERVIDOR NUBE)</b>. Nota: Requiere acceso a Internet</label></body></html>', 27, 3, CAST(1099.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (196, 1, N'D2019-N5', N'Licencias concurrentes para reconocimiento de voz VOCALI en nube', N'<html><body><font size=''2'' face=''Century Gothic''><label>5 Licencias <b>concurrentes</b> para reconocimiento de voz VOCALI en <b>(SERVIDOR NUBE)</b>. Nota: Requiere acceso a Internet</label></body></html>', 27, 3, CAST(8475.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (197, 1, N'D0220-N1', N'Soporte anual y mantenimiento para D2019-N1', N'<html><body><font size=''2'' face=''Century Gothic''><label>Soporte anual y manteniminento para D0219-N1 1 licencia <b>nominal</b> de reconocimiento de voz VOCALI en <b>(SERVIDOR NUBE)</b>.</label></body></html>', 27, 3, CAST(200.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (198, 1, N'D0220-N5', N'Soporte anual VOCALI en nube', N'<html><body><font size=''2'' face=''Century Gothic''><label>Soporte anual para 5 licencias <b>concurrentes</b> de reconocimiento de voz VOCALI D2019-N5 en <b>(SERVIDOR NUBE)</b>.</label></body></html>', 27, 3, CAST(1650.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (199, 1, N'D0109', N'5 Licencias concurrentes para reconocimiento de voz VOCALI (LOCAL)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Paquete 5 Licencias <b>concurrentes</b> para reconocimiento de voz VOCALI <b>(SERVIDOR LOCAL)</b>. Nota: Para instalaci�n de motor de reconocimiento local requiere el servidor G0199-VOZ.</label></body></html>', 27, 3, CAST(8475.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (200, 1, N'D0110', N'Soporte anual 5x VOCALI (LOCAL)', N'<html><body><font size=''2'' face=''Century Gothic''><label>Soporte anual y mantenimiento para paquete 5 licencias <b>concurrentes</b> de reconocimiento de voz D0109 <b>(SERVIDOR LOCAL)</b>.</label></body></html>', 27, 3, CAST(1650.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (201, 1, N'D0109-E1', N'1 Licencia para reconocimiento de voz VOCALI', N'<html><body><font size=''2'' face=''Century Gothic''><label>1 Licencia <b>concurrente</b> para reconocimiento de voz VOCALI <b>(ESTACION)</b>. Nota: La instalaci�n de motor de reconocimiento se realiza en una puesto de trabajo y se puede usar por m�ltiples usuarios.</label></body></html>', 27, 3, CAST(2500.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (202, 1, N'D0110-E1', N'Soporte anual y mantenimiento para 1 licencias de reconocimiento de voz', N'<html><body><font size=''2'' face=''Century Gothic''><label>Soporte anual y mantenimiento para <b>1 licencia</b> de reconocimiento de voz D0109�E1 <b>(ESTACION)</b>.</label></body></html>', 27, 3, CAST(500.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (203, 1, N'SP-PACS', N'Servicios profesionales para sistemas', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servicios profesionales para sistemas. * El Precio debe multiplicarse por la productividad estimada de estudios por a�o.</label></body></html>', 28, 3, CAST(1.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (204, 1, N'SP-PACS-INT', N'Servicios profesionales para sistemas', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servicios profesionales para integraci�n de RIS a sistema administrativo. Incluye an�lisis de BD y trabajo de desarrollo en conjunto para el intercambio de mensajes HL7 o webservice.</label></body></html>', 28, 3, CAST(1.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "INSERT [dbo].[Productos] ([IdProducto], [EsInternacional], [Modelo], [Nombre], [Descripcion], [Clasificacion], [Tipo], [Precio], [Moneda], [NumeroParte], [DatosCrudos], [DatosUsables], [Ruta], [Activo]) VALUES (205, 1, N'SP-PACS-NAC', N'Servicios profesionales para sistemas', N'<html><body><font size=''2'' face=''Century Gothic''><label>Servicios profesionales para integraci�n de RIS a sistema administrativo. Incluye an�lisis de BD y trabajo de desarrollo en conjunto para el intercambio de mensajes HL7 o webservice.</label></body></html>', 28, 3, CAST(4500.00 AS Decimal(10, 2)), 2, NULL, NULL, NULL, NULL, NULL) " +
                "SET IDENTITY_INSERT [dbo].[Productos] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[LIstasPrecios]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[LIstasPrecios] ON " +
                "INSERT [dbo].[LIstasPrecios] ([IdLista], [Codigo], [Nombre], [Descripcion], [IdListaReferencia], [EsBase], [Porcentaje], [IVA]) VALUES (1, N'LB', N'Lista Base', N'Lista Base', NULL, 1, NULL, 1) " +
                "INSERT [dbo].[LIstasPrecios] ([IdLista], [Codigo], [Nombre], [Descripcion], [IdListaReferencia], [EsBase], [Porcentaje], [IVA]) VALUES (2, N'LN', N'Lista Nacional', N'Lista Nacional', 1, 0, NULL, 1) " +
                "INSERT [dbo].[LIstasPrecios] ([IdLista], [Codigo], [Nombre], [Descripcion], [IdListaReferencia], [EsBase], [Porcentaje], [IVA]) VALUES (3, N'LI', N'Lista Internacional', N'Lista Internacional', 1, 0, NULL, 0) " +
                "INSERT [dbo].[LIstasPrecios] ([IdLista], [Codigo], [Nombre], [Descripcion], [IdListaReferencia], [EsBase], [Porcentaje], [IVA]) VALUES (4, N'LS', N'Lista Servicio', N'Lista Servicio', 1, 0, NULL, 1) " +
                "SET IDENTITY_INSERT [dbo].[LIstasPrecios] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[ListasPrecioProductos]) = 0) " +
                "BEGIN " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 1, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 6, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 7, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 8, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 9, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 11, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 15, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 17, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 19, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 21, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 23, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 25, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 26, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 27, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 28, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 29, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 30, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 36, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 37, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 38, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 39, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 40, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 41, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 42, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 43, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 44, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 45, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 46, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 47, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 48, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 49, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 50, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 51, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 53, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 54, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 55, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 56, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 59, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 73, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 74, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 75, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 76, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 77, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 78, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 79, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 82, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 83, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 88, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 89, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 91, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 93, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 98, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 99, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 100, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 101, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 102, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 103, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 106, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 107, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 108, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 109, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 111, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 112, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 113, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 114, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 115, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 116, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 117, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 118, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 119, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 120, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 121, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 122, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 123, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 124, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 125, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 126, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 127, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 128, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 129, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 130, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 131, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 132, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 133, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 134, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 135, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 136, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 137, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 138, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 139, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 140, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 141, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 142, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 143, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 144, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 149, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 150, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 151, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 152, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 153, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 154, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 155, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 156, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 157, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 158, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 159, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 160, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 161, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 162, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 163, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 164, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 165, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 166, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 167, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 168, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 169, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 170, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 171, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 172, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 173, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 174, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 175, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 176, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 177, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 178, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 179, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 180, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 181, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 182, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 183, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 184, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 185, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 186, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 187, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 188, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 189, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 190, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 191, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 192, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 195, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 196, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 197, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 198, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 199, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 200, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 201, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (1, 202, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 1, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 6, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 7, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 8, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 9, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 11, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 15, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 17, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 19, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 21, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 23, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 25, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 26, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 27, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 28, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 29, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 30, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 36, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 37, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 38, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 39, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 40, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 41, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 42, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 43, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 44, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 45, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 46, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 47, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 48, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 49, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 50, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 51, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 53, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 54, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 55, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 56, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 59, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 73, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 74, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 75, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 76, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 77, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 78, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 79, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 82, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 83, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 88, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 89, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 91, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 93, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 98, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 99, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 100, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 101, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 102, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 103, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 106, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 107, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 108, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 109, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 111, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 112, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 113, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 114, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 115, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 116, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 117, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 118, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 119, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 120, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 121, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 122, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 123, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 124, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 125, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 126, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 127, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 128, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 129, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 130, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 131, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 132, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 133, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 134, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 135, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 136, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 137, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 138, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 139, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 140, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 141, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 143, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 144, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 149, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 150, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 151, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 152, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 153, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 154, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 155, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 156, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 157, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 158, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 159, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 160, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 161, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 162, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 163, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 164, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 165, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 166, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 167, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 168, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 169, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 170, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 171, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 172, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 173, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 174, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 175, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 176, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 177, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 178, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 179, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 180, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 181, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 182, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 183, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 184, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 185, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 186, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 187, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 188, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 189, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 190, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 191, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 192, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 193, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 194, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 195, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 196, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 197, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 198, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 199, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 200, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 201, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 202, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 203, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (2, 205, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 1, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 6, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 7, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 8, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 9, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 11, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 15, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 17, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 19, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 21, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 23, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 25, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 26, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 27, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 28, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 29, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 30, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 36, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 37, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 38, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 39, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 40, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 41, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 42, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 43, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 44, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 45, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 46, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 47, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 48, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 49, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 50, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 51, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 53, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 54, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 55, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 56, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 59, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 73, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 74, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 75, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 76, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 77, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 78, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 79, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 82, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 83, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 88, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 89, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 91, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 93, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 98, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 99, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 100, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 101, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 102, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 103, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 106, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 107, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 108, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 109, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 111, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 112, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 113, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 114, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 115, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 116, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 117, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 118, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 119, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 120, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 121, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 122, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 123, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 124, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 125, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 126, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 127, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 128, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 129, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 130, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 131, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 132, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 133, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 134, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 135, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 136, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 137, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 138, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 139, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 140, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 141, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 143, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 144, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 149, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 150, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 151, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 152, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 153, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 154, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 155, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 156, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 157, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 158, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 159, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 160, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 161, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 162, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 163, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 164, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 165, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 166, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 167, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 168, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 169, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 170, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 171, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 172, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 173, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 174, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 175, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 176, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 177, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 178, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 179, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 180, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 181, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 182, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 183, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 184, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 185, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 186, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 187, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 188, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 189, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 190, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 191, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 192, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 193, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 194, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 195, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 196, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 197, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 198, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 199, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 200, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 201, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 202, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 203, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (3, 204, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 1, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 2, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 3, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 4, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 5, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 6, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 7, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 8, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 9, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 10, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 11, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 12, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 13, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 14, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 15, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 16, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 17, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 18, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 19, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 20, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 21, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 22, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 23, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 24, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 25, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 26, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 27, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 28, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 29, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 30, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 31, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 32, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 33, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 34, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 36, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 37, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 38, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 39, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 40, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 41, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 42, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 43, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 44, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 45, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 46, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 47, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 48, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 49, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 50, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 51, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 53, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 54, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 55, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 56, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 57, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 58, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 59, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 60, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 61, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 62, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 63, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 64, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 65, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 66, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 67, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 68, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 69, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 70, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 71, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 72, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 73, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 74, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 75, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 76, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 77, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 78, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 79, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 80, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 81, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 82, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 83, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 84, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 85, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 86, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 87, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 88, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 89, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 90, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 91, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 92, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 93, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 94, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 95, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 96, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 97, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 98, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 99, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 100, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 101, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 102, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 103, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 104, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 105, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 106, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 107, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 108, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 109, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 110, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 111, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 112, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 113, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 114, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 115, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 116, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 117, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 118, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 119, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 120, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 121, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 122, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 123, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 124, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 125, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 126, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 127, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 128, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 129, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 130, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 131, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 132, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 133, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 134, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 135, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 136, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 137, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 138, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 139, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 140, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 141, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 142, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 143, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 144, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 145, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 146, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 147, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 148, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 149, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 150, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 151, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 152, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 153, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 154, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 155, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 156, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 157, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 158, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 159, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 160, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 161, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 162, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 163, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 164, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 165, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 166, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 167, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 168, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 169, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 170, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 171, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 172, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 173, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 174, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 175, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 176, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 177, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 178, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 179, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 180, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 181, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 182, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 183, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 184, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 185, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 186, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 187, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 188, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 189, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 190, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 191, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 192, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 193, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 194, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 195, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 196, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 197, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 198, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 199, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 200, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 201, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 202, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 203, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 204, NULL, NULL) " +
                "INSERT [dbo].[ListasPrecioProductos] ([IdLista], [IdProducto], [Porcentaje], [PrecioFinal]) VALUES (4, 205, NULL, NULL) END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[ProductosComponentes]) = 0) " +
                "BEGIN " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (1, 2, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (1, 146, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (6, 3, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (6, 36, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (6, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (6, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (6, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (6, 143, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (6, 147, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (7, 4, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (7, 36, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (7, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (7, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (7, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (7, 143, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (7, 147, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (8, 5, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (8, 36, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (8, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (8, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (8, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (8, 143, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (8, 147, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (9, 10, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (9, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (9, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (9, 146, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (11, 12, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (11, 36, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (11, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (11, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (11, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (11, 143, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (11, 146, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (12, 14, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (15, 16, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (15, 36, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (15, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (15, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (15, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (15, 143, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (15, 146, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (16, 14, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (16, 35, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (17, 18, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (17, 36, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (17, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (17, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (17, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (17, 143, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (17, 146, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (18, 14, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (18, 35, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (19, 20, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (19, 36, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (19, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (19, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (19, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (19, 143, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (19, 146, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (20, 13, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (20, 35, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (21, 22, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (21, 36, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (21, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (21, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (21, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (21, 143, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (21, 146, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (22, 14, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (22, 35, 3) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (23, 24, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (23, 36, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (23, 61, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (23, 95, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (23, 97, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (23, 143, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (23, 146, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (24, 13, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (24, 35, 3) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (26, 32, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (26, 60, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (26, 94, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (26, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (27, 31, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (27, 94, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (27, 147, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (28, 33, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (28, 60, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (28, 94, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (28, 147, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (29, 34, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (29, 60, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (29, 94, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (29, 147, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (30, 33, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (30, 94, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (30, 147, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (63, 62, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (63, 64, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (63, 65, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (63, 122, 6) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (63, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (66, 62, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (66, 67, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (66, 68, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (66, 122, 6) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (66, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (73, 62, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (73, 64, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (73, 68, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (73, 121, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (73, 122, 6) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (73, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 62, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 71, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 122, 6) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 126, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 127, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 128, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 129, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 130, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 131, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (74, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (75, 62, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (75, 67, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (75, 68, 2) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (75, 121, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (75, 122, 6) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (75, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 62, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 72, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 122, 6) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 126, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 127, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 128, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 129, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 130, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 131, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (76, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (77, 69, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (77, 70, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (77, 124, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (77, 125, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (77, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (79, 57, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (79, 80, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (79, 81, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (79, 139, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (79, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (82, 57, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (82, 81, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (82, 112, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (82, 113, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (82, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (83, 58, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (83, 84, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (83, 85, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (83, 86, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (83, 87, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (83, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (88, 84, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (88, 85, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (88, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (89, 58, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (89, 84, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (89, 87, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (89, 90, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (89, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (91, 58, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (91, 84, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (91, 87, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (91, 92, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (91, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (102, 60, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (102, 104, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (102, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (103, 105, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (103, 145, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (109, 96, 1) " +
                "INSERT [dbo].[ProductosComponentes] ([IdProducto], [IdComponente], [Cantidad]) VALUES (109, 110, 1) END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Permisos]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Permisos] ON " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (1, N'Documentos Modificar', N'Este permiso permite agregar o eliminar archivos ', N'ARCHIVO_DOCUMENTOS_MODIFICAR') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (2, N'Listas de Precios Imprimir', N'Este permiso permite imprimir listas de precios', N'GENERAL_LISTAPRECIOS_IMPRIMIR') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (3, N'Listas de Precio Nacional Acceso', N'Este permiso permite visualizar y hacer operaciones con la lista de precios nacional', N'GENERAL_LISTAPRECIOS_NACIONAL') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (4, N'Listas de Precios Internacional Acceso', N'Este permiso permite visualizar y hacer operaciones con la lista de precios internacional', N'GENERAL_LISTAPRECIOS_INTERNACIONAL') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (5, N'Listas de Precios Servicio Acceso', N'Este permiso permite visualizar y hacer operaciones con la lista de precios servicio', N'GENERAL_LISTAPRECIOS_SERVICIO') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (6, N'Listas de Precios Otras Acceso', N'Este permiso permite visualizar y hacer operaciones con la listas de precios generadas por el sistema', N'GENERAL_LISTAPRECIOS_OTRAS') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (7, N'Imprimir existencias', N'Este permiso imprimir las existencias en las secciones de cotizaciones y productos', N'GENERAL_EXISTENCIAS_IMPRIMIR') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (8, N'Crear instalaciones', N'Este permiso permite crear nuevas instalaciones', N'INSTALACIONES_CREAR') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (9, N'Editar instalaciones', N'Este permiso permite editar instalaciones', N'INSTALACIONES_EDITAR') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (10, N'Crear reportes', N'Este permiso permite crear nuevos reportes', N'REPORTES_CREAR') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (11, N'Importar reportes', N'Este permiso permite importar reportes de archivos excel', N'REPORTES_IMPORTAR') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (12, N'Ver reportes de terceros', N'Este permiso permite ver los reportes iniciados por terceros', N'REPORTES_VER_TERCEROS') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (13, N'Comentar en reportes de terceros', N'Este permiso permite comentar en los reportes iniciados por terceros', N'REPORTES_COMENTAR_TERCEROS') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (14, N'Modificar reporte: TERMINADO,NORESUELTO', N'Este permiso permite cambiar el estado de un reporte a TERMINADO y NO RESUELTO', N'REPORTES_CAMBIAR_TERMINADO_NORESUELTO') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (15, N'Modificar reporte: PENDIENTE,REPORTADO,CANCELADO', N'Este permiso permite cambiar el estado de un reporte a PENDIENTE, REPORTADO y CANCELADO', N'REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (16, N'Modificar reporte: PORVALIDAR', N'Este permiso permite cambiar el estado de un reporte a POR VALIDAR', N'REPORTES_CAMBIAR_PORVALIDAR') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (17, N'Modificar reporte: ENPROCESO', N'Este permiso permite cambiar el estado de un reporte a EN PROCESO', N'REPORTES_CAMBIAR_ENPROCESO') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (18, N'Configuraci�n', N'Este permiso permite configurar el sistema', N'INSTALACIONES_CONFIGURACION') " +
                "INSERT [dbo].[Permisos] ([IdPermiso], [Nombre], [Descripcion], [Clave]) VALUES (19, N'Importar reportes', N'Este permiso permite importar documentos con reportes existentes', N'REPORTES_IMPORTAR') " +
                "SET IDENTITY_INSERT [dbo].[Permisos] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[UsuariosPermisos]) = 0) " +
                "BEGIN " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (1, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (2, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (3, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (4, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (5, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (6, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (7, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (8, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (9, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (10, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (11, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (12, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (13, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (14, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (15, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (16, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (17, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (18, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (19, 1) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (10, 2) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (11, 2) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (12, 2) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (13, 2) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (14, 2) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (15, 2) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (19, 2) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (10, 4) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (11, 4) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (12, 4) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (13, 4) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (14, 4) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (15, 4) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (19, 4) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (10, 5) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (11, 5) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (12, 5) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (13, 5) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (14, 5) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (15, 5) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (19, 5) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (10, 6) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (11, 6) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (12, 6) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (13, 6) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (14, 6) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (15, 6) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (19, 6) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (10, 7) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (10, 8) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (1, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (2, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (3, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (4, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (5, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (6, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (7, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (8, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (9, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (10, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (11, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (12, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (13, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (14, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (15, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (16, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (17, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (18, 9) " +
                "INSERT [dbo].[UsuariosPermisos] ([IdPermisos], [IdUsuario]) VALUES (19, 9) END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Vistas]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[Vistas] ON " +
                "INSERT [dbo].[Vistas] ([IdVista], [Nombre], [Clave]) VALUES (1, N'Archivos', N'ARCHIVOS') " +
                "INSERT [dbo].[Vistas] ([IdVista], [Nombre], [Clave]) VALUES (2, N'Cotizaciones', N'COTIZACIONES') " +
                "INSERT [dbo].[Vistas] ([IdVista], [Nombre], [Clave]) VALUES (3, N'Productos', N'PRODUCTOS') " +
                "INSERT [dbo].[Vistas] ([IdVista], [Nombre], [Clave]) VALUES (4, N'Versiones', N'VERSIONES') " +
                "INSERT [dbo].[Vistas] ([IdVista], [Nombre], [Clave]) VALUES (5, N'Configuracion', N'CONFIGURACION') " +
                "INSERT [dbo].[Vistas] ([IdVista], [Nombre], [Clave]) VALUES (6, N'Instalaciones', N'INSTALACIONES') " +
                "SET IDENTITY_INSERT [dbo].[Vistas] OFF END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[UsuariosVistas]) = 0) " +
                "BEGIN " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (1, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (2, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (3, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (4, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (5, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (6, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (7, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (8, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (9, 1) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (1, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (2, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (3, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (4, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (5, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (6, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (7, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (8, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (9, 2) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (1, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (2, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (3, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (4, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (5, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (6, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (7, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (8, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (9, 3) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (1, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (2, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (3, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (4, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (5, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (6, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (7, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (8, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (9, 4) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (1, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (2, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (3, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (4, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (5, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (6, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (7, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (8, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (9, 5) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (1, 6) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (2, 6) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (3, 6) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (4, 6) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (5, 6) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (6, 6) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (7, 6) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (8, 6) " +
                "INSERT [dbo].[UsuariosVistas] ([IdUsuario], [IdVista]) VALUES (9, 6) END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[ConfiguracionMail]) = 0) " +
                "BEGIN " +
                "SET IDENTITY_INSERT [dbo].[ConfiguracionMail] ON " +
                "INSERT [dbo].[ConfiguracionMail] ([Id], [Password], [Puerto], [Sender], [Servidor], [HabilitaSSL], [ModoSSL], [VersionSSL]) VALUES (1, N'ct2016Mx@', 587, N'contacto@cmr3.com.mx', N'smtp.cmr3.com.mx', 0, 1, 2)  " +
                "SET IDENTITY_INSERT [dbo].[ConfiguracionMail] OFF  END");
            Sql("IF((SELECT COUNT(*) FROM [dbo].[Rutas]) = 0) " +
                "BEGIN " +
                "INSERT [dbo].[Rutas] ([Tipo], [Descripcion], [Directorio]) VALUES (1, N'Documentos', N'C:\\PaginaWeb\\Docs\\') " +
                "INSERT [dbo].[Rutas] ([Tipo], [Descripcion], [Directorio]) VALUES (2, N'Bitacoras', N'C:\\PaginaWeb\\Logs\\') " +
                "INSERT [dbo].[Rutas] ([Tipo], [Descripcion], [Directorio]) VALUES (3, N'Aplicaciones', N'C:\\PaginaWeb\\Aplicaciones\\') " +
                "INSERT [dbo].[Rutas] ([Tipo], [Descripcion], [Directorio]) VALUES (4, N'Temporales', N'C:\\PaginaWeb\\Temp\\') END");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReportesImportados", "ReporteImportadoFuenteId", "dbo.ReportesImportados");
            DropForeignKey("dbo.ReportesImportados", "InstalacionId", "dbo.Instalaciones");
            DropForeignKey("dbo.ProductosComponentes", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.ProductosComponentes", "IdComponente", "dbo.Productos");
            DropForeignKey("dbo.CotizacionesProductos", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.CotizacionesProductos", "IdCotizacion", "dbo.Cotizaciones");
            DropForeignKey("dbo.Cotizaciones", "idUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.UsuariosVistas", "IdVista", "dbo.Vistas");
            DropForeignKey("dbo.UsuariosVistas", "IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.UsuariosNotificationsConfigurations", "Id", "dbo.Usuarios");
            DropForeignKey("dbo.Reportes", "AsignadoId", "dbo.Usuarios");
            DropForeignKey("dbo.InstalacionUsuarios", "Usuarios_IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.InstalacionUsuarios", "Instalacion_Id", "dbo.Instalaciones");
            DropForeignKey("dbo.Reportes", "VistoBuenoId", "dbo.Usuarios");
            DropForeignKey("dbo.Reportes", "ReportePredecesorId", "dbo.Reportes");
            DropForeignKey("dbo.Reportes", "QuienReportaId", "dbo.Usuarios");
            DropForeignKey("dbo.Reportes", "ModuloId", "dbo.Modulos");
            DropForeignKey("dbo.Reportes", "InstalacionId", "dbo.Instalaciones");
            DropForeignKey("dbo.Comentarios", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Comentarios", "ReporteId", "dbo.Reportes");
            DropForeignKey("dbo.Comentarios", "EvidenciaId", "dbo.Comentarios");
            DropForeignKey("dbo.UsuariosPermisos", "IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.UsuariosPermisos", "IdPermisos", "dbo.Permisos");
            DropForeignKey("dbo.Cotizaciones", "IdLista", "dbo.LIstasPrecios");
            DropForeignKey("dbo.ListasPrecioProductos", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.ListasPrecioProductos", "IdLista", "dbo.LIstasPrecios");
            DropForeignKey("dbo.Productos", "Clasificacion", "dbo.Clasificaciones");
            DropForeignKey("dbo.Modulos", "AplicacionesId", "dbo.Aplicaciones");
            DropForeignKey("dbo.AplicacionesContactos", "IdContacto", "dbo.Contactos");
            DropForeignKey("dbo.AplicacionesContactos", "IdAplicacion", "dbo.Aplicaciones");
            DropForeignKey("dbo.AplicacionesManuales", "IdVersion", "dbo.AplicacionesVersiones");
            DropForeignKey("dbo.AplicacionesVersiones", "IdAplicacion", "dbo.Aplicaciones");
            DropIndex("dbo.UsuariosVistas", new[] { "IdVista" });
            DropIndex("dbo.UsuariosVistas", new[] { "IdUsuario" });
            DropIndex("dbo.InstalacionUsuarios", new[] { "Usuarios_IdUsuario" });
            DropIndex("dbo.InstalacionUsuarios", new[] { "Instalacion_Id" });
            DropIndex("dbo.UsuariosPermisos", new[] { "IdUsuario" });
            DropIndex("dbo.UsuariosPermisos", new[] { "IdPermisos" });
            DropIndex("dbo.AplicacionesContactos", new[] { "IdContacto" });
            DropIndex("dbo.AplicacionesContactos", new[] { "IdAplicacion" });
            DropIndex("dbo.ReportesImportados", new[] { "ReporteImportadoFuenteId" });
            DropIndex("dbo.ReportesImportados", new[] { "InstalacionId" });
            DropIndex("dbo.ProductosComponentes", new[] { "IdComponente" });
            DropIndex("dbo.ProductosComponentes", new[] { "IdProducto" });
            DropIndex("dbo.UsuariosNotificationsConfigurations", new[] { "Id" });
            DropIndex("dbo.Comentarios", new[] { "ReporteId" });
            DropIndex("dbo.Comentarios", new[] { "UsuarioId" });
            DropIndex("dbo.Comentarios", new[] { "EvidenciaId" });
            DropIndex("dbo.Reportes", new[] { "ReportePredecesorId" });
            DropIndex("dbo.Reportes", new[] { "VistoBuenoId" });
            DropIndex("dbo.Reportes", new[] { "AsignadoId" });
            DropIndex("dbo.Reportes", new[] { "QuienReportaId" });
            DropIndex("dbo.Reportes", new[] { "InstalacionId" });
            DropIndex("dbo.Reportes", new[] { "ModuloId" });
            DropIndex("dbo.ListasPrecioProductos", new[] { "IdProducto" });
            DropIndex("dbo.ListasPrecioProductos", new[] { "IdLista" });
            DropIndex("dbo.Cotizaciones", new[] { "idUsuario" });
            DropIndex("dbo.Cotizaciones", new[] { "IdLista" });
            DropIndex("dbo.CotizacionesProductos", new[] { "IdProducto" });
            DropIndex("dbo.CotizacionesProductos", new[] { "IdCotizacion" });
            DropIndex("dbo.Productos", new[] { "Clasificacion" });
            DropIndex("dbo.Modulos", new[] { "AplicacionesId" });
            DropIndex("dbo.AplicacionesManuales", new[] { "IdVersion" });
            DropIndex("dbo.AplicacionesVersiones", new[] { "IdAplicacion" });
            DropTable("dbo.UsuariosVistas");
            DropTable("dbo.InstalacionUsuarios");
            DropTable("dbo.UsuariosPermisos");
            DropTable("dbo.AplicacionesContactos");
            DropTable("dbo.Rutas");
            DropTable("dbo.ReportesImportados");
            DropTable("dbo.Documentos");
            DropTable("dbo.ConfiguracionMail");
            DropTable("dbo.ConfiguracaionInstalaciones");
            DropTable("dbo.ProductosComponentes");
            DropTable("dbo.Vistas");
            DropTable("dbo.UsuariosNotificationsConfigurations");
            DropTable("dbo.Comentarios");
            DropTable("dbo.Reportes");
            DropTable("dbo.Instalaciones");
            DropTable("dbo.Permisos");
            DropTable("dbo.Usuarios");
            DropTable("dbo.ListasPrecioProductos");
            DropTable("dbo.LIstasPrecios");
            DropTable("dbo.Cotizaciones");
            DropTable("dbo.CotizacionesProductos");
            DropTable("dbo.Productos");
            DropTable("dbo.Clasificaciones");
            DropTable("dbo.Modulos");
            DropTable("dbo.Contactos");
            DropTable("dbo.AplicacionesManuales");
            DropTable("dbo.AplicacionesVersiones");
            DropTable("dbo.Aplicaciones");
        }
    }
}
