using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using System.Linq;

namespace IDigitales.Pagina.Web.Data
{

    sealed partial class DatabaseConfiguration : DbMigrationsConfiguration<EntidadesPagina>
    {
        public DatabaseConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }

}
