namespace IDigitales.Pagina.Web.Data
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TablaConfiguracionesGenerales : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConfiguracionesGenerales",
                c => new
                    {
                        IdConfiguracion = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 100),
                        Descripcion = c.String(maxLength: 250),
                        Valor = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.IdConfiguracion);

            Sql("IF NOT EXISTS (select * from [dbo].[ConfiguracionesGenerales] where [Nombre] = 'VersionListasPrecio') " +
                "BEGIN INSERT INTO[dbo].[ConfiguracionesGenerales]([Nombre], [Descripcion], [Valor]) " +
                "VALUES('VersionListasPrecio', 'Indica la versi�n de las listas de precio', '1.0') END " +
                "IF NOT EXISTS(select* from [dbo].[ConfiguracionesGenerales] where[Nombre] = 'VersionInventario') " +
                "BEGIN INSERT INTO[dbo].[ConfiguracionesGenerales]([Nombre], [Descripcion], [Valor]) " +
                "VALUES('VersionInventario', 'Indica la versi�n del inventario de productos', '1.0') END " +
                "IF NOT EXISTS(select* from [dbo].[ConfiguracionesGenerales] where[Nombre] = 'FechaListasPrecio') " +
                "BEGIN INSERT INTO[dbo].[ConfiguracionesGenerales]([Nombre], [Descripcion], [Valor]) " +
                "VALUES('FechaListasPrecio', 'Indica la fecha en que se actualiza la lista de precios', null) END " +
                "IF NOT EXISTS(select* from [dbo].[ConfiguracionesGenerales] where[Nombre] = 'FechaInventario') " +
                "BEGIN INSERT INTO[dbo].[ConfiguracionesGenerales]([Nombre], [Descripcion], [Valor]) " +
                "VALUES('FechaInventario', 'Indica la fecha en que se actualiza el inventario', null) END");

            Sql("IF NOT EXISTS (SELECT * FROM [dbo].[Permisos] WHERE Clave = 'GENERAL_LISTAPRECIOS_EDITAR') " +
                "BEGIN INSERT INTO[dbo].[Permisos]([Nombre],[Descripcion],[Clave],[Seccion]) VALUES('Listas de Precios Edici�n', 'Este permiso permite editar las listas de precio', 'GENERAL_LISTAPRECIOS_EDITAR',2) " +
                "INSERT INTO[dbo].[UsuariosPermisos] ([IdPermisos],[IdUsuario]) VALUES((SELECT IdPermiso FROM [dbo].[Permisos] WHERE Clave = 'GENERAL_LISTAPRECIOS_EDITAR'),1) END");

            Sql("IF((SELECT COUNT(*) FROM [dbo].[UsuariosAplicaciones]) = 0) " +
                "BEGIN INSERT INTO[dbo].[UsuariosAplicaciones]([IdUsuario], [IdAplicacion]) " +
                "SELECT 1, IdAplicacion FROM[dbo].[Aplicaciones] END");
        }
        
        public override void Down()
        {
            DropTable("dbo.ConfiguracionesGenerales");
        }
    }
}
