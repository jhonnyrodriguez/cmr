namespace IDigitales.Pagina.Web.Data
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportesModuloIdNulo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reportes", "ModuloId", "dbo.Modulos");
            DropIndex("dbo.Reportes", new[] { "ModuloId" });
            AlterColumn("dbo.Reportes", "ModuloId", c => c.Int());
            CreateIndex("dbo.Reportes", "ModuloId");
            AddForeignKey("dbo.Reportes", "ModuloId", "dbo.Modulos", "Id");

            Sql("IF NOT EXISTS (SELECT * FROM [dbo].[ReportesCategorias] WHERE Nombre='Servidores' AND Grupo='Reporte Hardware PACS') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Servidores', 'Servidores', 'Reporte Hardware PACS', 0) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Estaciones de diagn�stico' AND Grupo = 'Reporte Hardware PACS') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Estaciones de Diagn�stico','Estaciones de diagn�stico','Reporte Hardware PACS', 0) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Publicadores' AND Grupo = 'Reporte Hardware PACS') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Publicadores','Publicadores','Reporte Hardware PACS', 0) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Estaciones de quir�fano m�viles' AND Grupo = 'Reporte Hardware PACS') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Estaciones de quir�fano m�viles','Estaciones de quir�fano m�viles','Reporte Hardware PACS', 0) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Estaciones de quir�fano fijas' AND Grupo = 'Reporte Hardware PACS') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Estaciones de quir�fano fijas','Estaciones de quir�fano fijas','Reporte Hardware PACS', 0) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Terminales RIS' AND Grupo = 'Reporte Hardware PACS') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Terminales RIS','Terminales RIS','Reporte Hardware PACS', 0) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Digitales directos para radiolog�a' AND Grupo = 'Reporte Hardware DR') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Digitales directos para radiolog�a','Digitales directos para radiolog�a','Reporte Hardware DR', 0) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Digitalizadores' AND Grupo = 'Reporte Hardware CR') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Digitalizadores','Digitalizadores','Reporte Hardware CR', 0) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Soporte de software en general' AND Grupo = 'Reporte Software') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Soporte de software en general','Soporte de software en general','Reporte Software', 1) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[ReportesCategorias] WHERE Nombre = 'Soporte de aplicaciones en general' AND Grupo = 'Reporte Aplicaciones') BEGIN " +
                "INSERT INTO[dbo].[ReportesCategorias]([Nombre],[Descripcion],[Grupo],[EsAplicacion]) " +
                "VALUES('Soporte de aplicaciones en general','Soporte de aplicaciones en general','Reporte Aplicaciones', 0) END ");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reportes", "ModuloId", "dbo.Modulos");
            DropIndex("dbo.Reportes", new[] { "ModuloId" });
            AlterColumn("dbo.Reportes", "ModuloId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reportes", "ModuloId");
            AddForeignKey("dbo.Reportes", "ModuloId", "dbo.Modulos", "Id", cascadeDelete: true);
        }
    }
}
