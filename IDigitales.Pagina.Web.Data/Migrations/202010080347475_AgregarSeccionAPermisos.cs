namespace IDigitales.Pagina.Web.Data
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarSeccionAPermisos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UsuariosAplicaciones",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false),
                        IdAplicacion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdUsuario, t.IdAplicacion })
                .ForeignKey("dbo.Usuarios", t => t.IdUsuario, cascadeDelete: true)
                .ForeignKey("dbo.Aplicaciones", t => t.IdAplicacion, cascadeDelete: true)
                .Index(t => t.IdUsuario)
                .Index(t => t.IdAplicacion);
            
            AddColumn("dbo.Permisos", "Seccion", c => c.Int());

            //Agregar seccion a Permisos
            Sql("UPDATE Permisos SET Seccion=1 WHERE Clave='ARCHIVO_DOCUMENTOS_MODIFICAR' " +
                "UPDATE Permisos SET Seccion = 2 WHERE Clave = 'GENERAL_LISTAPRECIOS_IMPRIMIR' " +
                "UPDATE Permisos SET Seccion = 2 WHERE Clave = 'GENERAL_LISTAPRECIOS_NACIONAL' " +
                "UPDATE Permisos SET Seccion = 2 WHERE Clave = 'GENERAL_LISTAPRECIOS_INTERNACIONAL' " +
                "UPDATE Permisos SET Seccion = 2 WHERE Clave = 'GENERAL_LISTAPRECIOS_SERVICIO' " +
                "UPDATE Permisos SET Seccion = 2 WHERE Clave = 'GENERAL_LISTAPRECIOS_OTRAS' " +
                "UPDATE Permisos SET Seccion = 3 WHERE Clave = 'GENERAL_EXISTENCIAS_IMPRIMIR' " +
                "UPDATE Permisos SET Seccion = 4 WHERE Clave = 'INSTALACIONES_CREAR' " +
                "UPDATE Permisos SET Seccion = 4 WHERE Clave = 'INSTALACIONES_EDITAR' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_CREAR' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_IMPORTAR' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_VER_TERCEROS' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_COMENTAR_TERCEROS' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_CAMBIAR_TERMINADO_NORESUELTO' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_CAMBIAR_PORVALIDAR' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_CAMBIAR_ENPROCESO' " +
                "UPDATE Permisos SET Seccion = 5 WHERE Clave = 'REPORTES_IMPORTAR' " +
                "UPDATE Permisos SET Seccion = 6 WHERE Clave = 'INSTALACIONES_CONFIGURACION'");
            //Agrega permisos aplicaciones y versiones
            Sql("IF NOT EXISTS (SELECT * FROM [dbo].[Permisos] WHERE Clave = 'GENERAL_VERSIONES_EDITAR') " +
                "BEGIN INSERT INTO[dbo].[Permisos]([Nombre],[Descripcion],[Clave],[Seccion]) " +
                "VALUES('Editar versiones', 'Editar versiones', 'GENERAL_VERSIONES_EDITAR', 8)END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[UsuariosPermisos] WHERE[IdPermisos] = (SELECT IdPermiso FROM [dbo].[Permisos] WHERE Clave = 'GENERAL_VERSIONES_EDITAR')  AND[IdUsuario] = 1)  " +
                "BEGIN INSERT INTO[dbo].[UsuariosPermisos]([IdPermisos],[IdUsuario]) " +
                "VALUES((SELECT IdPermiso FROM [dbo].[Permisos] WHERE Clave = 'GENERAL_VERSIONES_EDITAR') ,1) END " +
                "IF NOT EXISTS(SELECT* FROM [dbo].[Permisos] WHERE Clave = 'GENERAL_APLICACIONES_EDITAR') " +
                "BEGIN INSERT INTO[dbo].[Permisos]([Nombre],[Descripcion],[Clave],[Seccion]) " +
                "VALUES('Editar aplicaciones','Editar aplicaciones','GENERAL_APLICACIONES_EDITAR',8) END");
            Sql("IF NOT EXISTS (SELECT * FROM [dbo].[Permisos] WHERE Clave = 'GENERAL_PRODUCTOS_EDITAR') " +
                "BEGIN INSERT INTO[dbo].[Permisos]([Nombre],[Descripcion],[Clave],[Seccion]) VALUES('Editar productos', 'Este permiso permite editar productos', 'GENERAL_PRODUCTOS_EDITAR', 7) " +
                "INSERT INTO[dbo].[UsuariosPermisos] ([IdPermisos],[IdUsuario]) VALUES((SELECT IdPermiso FROM [dbo].[Permisos] WHERE Clave = 'GENERAL_PRODUCTOS_EDITAR'),1) END");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsuariosAplicaciones", "IdAplicacion", "dbo.Aplicaciones");
            DropForeignKey("dbo.UsuariosAplicaciones", "IdUsuario", "dbo.Usuarios");
            DropIndex("dbo.UsuariosAplicaciones", new[] { "IdAplicacion" });
            DropIndex("dbo.UsuariosAplicaciones", new[] { "IdUsuario" });
            DropColumn("dbo.Permisos", "Seccion");
            DropTable("dbo.UsuariosAplicaciones");
        }
    }
}
