﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    public class ProductosModel
    {
        public int IdLista { get; set; }
        public int IdProducto { get; set; }
        public double? Porcentaje { get; set; }
        public decimal? Precio { get; set; }
        public string Modelo { get; set; }
        public string Nombre { get; set; }
        public int Cantidad { get; set; }

    }
    public class CotizacionModel
    {
        public List<ProductosModel> CotizacionesProductos { get; set; }
        public Cotizaciones Cotizacion { get; set; }
        public bool Iva { get; set; }

    }

}
