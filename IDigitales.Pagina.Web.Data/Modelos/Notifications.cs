﻿using IDigitales.Pagina.Web.Data.Entidades.Poco;
using Rebex.Mail;
using Rebex.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IDigitales.Pagina.Web.Data.Entidades.Poco.UsuariosNotificationsConfiguration;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    public class Notifications
    {

        #region ".  Convertidores"

        public static NotificationsEnum ConvierteTipo(Reporte.EstadoEnum estadoReporte)
        {
            NotificationsEnum resultado = NotificationsEnum.None;

            switch (estadoReporte)
            {
                case Reporte.EstadoEnum.Reportado:
                    resultado = NotificationsEnum.Creacion;
                    break;
                case Reporte.EstadoEnum.Requerimiento:
                    resultado = NotificationsEnum.Creacion;
                    break;
                case Reporte.EstadoEnum.Pendiente:
                    resultado = NotificationsEnum.StateChange_Pendiente;
                    break;
                case Reporte.EstadoEnum.Cancelado:
                    resultado = NotificationsEnum.StateChange_Cancelado;
                    break;
                case Reporte.EstadoEnum.EnProceso:
                    resultado = NotificationsEnum.StateChange_EnProceso;
                    break;
                case Reporte.EstadoEnum.PorValidar:
                    resultado = NotificationsEnum.StateChange_PorValidar;
                    break;
                case Reporte.EstadoEnum.Terminado:
                    resultado = NotificationsEnum.StateChange_Terminado;
                    break;
                case Reporte.EstadoEnum.NoResuelto:
                    resultado = NotificationsEnum.StateChange_NoResuelto;
                    break;
                default:
                    break;
            }

            return resultado;
        }

        #endregion

        #region ".  Notifications"

        /// <summary>
        /// Notifica reporte.
        /// </summary>
        /// <param name="reporte"></param>
        public static void NotifyAll(Reporte reporte, NotificationsEnum tipoNotificacion, Usuarios usuario = null, string comment = "", string redirectTo = "", string redirectTo2 = "")
        {
            Task.Run(() => NotifyAll_Task(reporte, tipoNotificacion, usuario, comment, redirectTo, redirectTo2));
        }

        private static void NotifyAll_Task(Reporte reporte, NotificationsEnum tipoNotificacion, Usuarios usuario, string comment, string redirectTo, string redirectTo2)
        {
            ConsultasConfiguracion dbc = null;
            ConsultasUsuarios dbu = null;

            try
            {

                dbu = new ConsultasUsuarios();
                dbc = new ConsultasConfiguracion();

                //1. Recupera configuración de correo saliente.
                var configuration = dbc.ObtenerConfiguracionMail();

                //2. Recupera lista de usuario con copia.
                var bbcAdmin = dbu.UsuariosRecuperaEmailJefes(reporte, tipoNotificacion);
                Console.WriteLine("Notification destinatario Admin: " + bbcAdmin);
                var mensaje = MessageBuilder.Generic(reporte, tipoNotificacion, EnumMessageBuilderPlantilla.Jefe, usuario, bbcAdmin, comment, redirectTo, redirectTo2);
                if (mensaje != null) SendMail(mensaje, configuration);

                var bbcPersonal = dbu.UsuariosRecuperaEmailAsignado(reporte, tipoNotificacion);
                Console.WriteLine("Notification destinatario Personal: " + bbcPersonal);
                mensaje = MessageBuilder.Generic(reporte, tipoNotificacion, EnumMessageBuilderPlantilla.Asignado, usuario, bbcPersonal, comment, redirectTo, redirectTo2);
                if (mensaje != null) SendMail(mensaje, configuration);

                var bbcCliente = dbu.UsuariosRecuperaEmailCliente(reporte, tipoNotificacion);
                Console.WriteLine("Notification destinatario Cliente: " + bbcCliente);
                mensaje = MessageBuilder.Generic(reporte, tipoNotificacion, EnumMessageBuilderPlantilla.Cliente, usuario, bbcCliente, comment, redirectTo, redirectTo2);
                if (mensaje != null) SendMail(mensaje, configuration);

            }
            catch (Exception)
            {
                //TODO BITACORAS.
            }
            finally
            {
                if (dbc != null)
                    dbc.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }

        }

        #endregion

        #region ".  Configuracion"

        private static TlsVersion ObtieneVersionConfigurada(EnumVersionSSL sslVersion)
        {
            TlsVersion version = TlsVersion.Any;
            try
            {
                switch (sslVersion)
                {
                    case EnumVersionSSL.Todos:
                        version = TlsVersion.Any;
                        break;
                    case EnumVersionSSL.SSL30:
                        version = TlsVersion.SSL30;
                        break;
                    case EnumVersionSSL.TLS10:
                        version = TlsVersion.TLS10;
                        break;
                    case EnumVersionSSL.TLS11:
                        version = TlsVersion.TLS11;
                        break;
                    case EnumVersionSSL.TLS12:
                        version = TlsVersion.TLS12;
                        break;
                    default:
                        version = TlsVersion.Any;
                        break;
                }
            }
            catch (Exception ex)
            {

            }
            return version;
        }

        #endregion

        #region ".  Mensajes (consts)"

        public enum EnumMessageBuilderPlantilla
        {
            Jefe,
            Asignado,
            Cliente,
            Generica
        }

        private static class MessageBuilder
        {

            public static MailMessage Generic(Reporte reporte, NotificationsEnum tipo, EnumMessageBuilderPlantilla plantilla, Usuarios currentUser, string to, string comment, string redirectTo, string secondP)
            {
                if (!string.IsNullOrEmpty(to))
                {

                    KeyValuePair<string, string>? textoNullable = null;

                    switch (tipo)
                    {
                        case NotificationsEnum.Creacion:
                            textoNullable = Creacion_ToAll(reporte, currentUser, plantilla);
                            break;
                        case NotificationsEnum.StateChange_EnProceso:
                            textoNullable = StateChange_EnProceso(reporte, plantilla);
                            break;
                        case NotificationsEnum.StateChange_Pendiente:
                            textoNullable = StateChange_Pendiente(reporte);
                            break;
                        case NotificationsEnum.StateChange_PorValidar:
                            textoNullable = StateChange_PorValidar(reporte);
                            break;
                        case NotificationsEnum.StateChange_Terminado:
                            textoNullable = StateChange_Terminado(reporte, currentUser);
                            break;
                        case NotificationsEnum.StateChange_NoResuelto:
                            textoNullable = StateChange_NoResuelto(reporte, currentUser, plantilla);
                            break;
                        case NotificationsEnum.StateChange_Cancelado:
                            textoNullable = StateChange_Cancelado(reporte, currentUser);
                            break;
                        case NotificationsEnum.NewComments:
                            textoNullable = NewComment_ToAll(reporte, currentUser, comment);
                            break;
                        case NotificationsEnum.VistoBueno:
                            textoNullable = VistoBueno(reporte, currentUser);
                            break;
                        default:
                            break;
                    }

                    if (textoNullable.HasValue)
                    {
                        var texto = textoNullable.Value;
                        //string b1 = $"<p>{texto.Value}</p>";
                        string b1 = $"{texto.Value}";
                        string p2 = $"<p>{secondP}</p>";
                        string btn = $"<a href='{redirectTo}' style='margin-top:16px;'>IR</a>";

                        var bodyHtml = string.Empty;
                        if (!string.IsNullOrEmpty(secondP))
                            bodyHtml =  /*h + */b1 + (string.IsNullOrEmpty(secondP) ? "" : secondP + (string.IsNullOrEmpty(redirectTo) ? "" : btn));
                        else
                            bodyHtml =  /*h + */b1 + (string.IsNullOrEmpty(redirectTo) ? "" : btn);

                        if (plantilla == EnumMessageBuilderPlantilla.Cliente)
                            bodyHtml += SoporteTecnico_HtmlFooter();

                        return new MailMessage
                        {
                            BodyHtml = bodyHtml,
                            Bcc = to,
                            Subject = texto.Key,
                        };
                    }
                }

                return null;
            }

            public static KeyValuePair<string, string> StateChange_Pendiente(Reporte reporte)
            {
                var resultado = new KeyValuePair<string, string>();

                string content = string.Empty;
                string title = string.Empty;

                // string content = @"<h1> </h1>";
                title = $"Notificación {reporte.Tag}, Nuevo Estado: Pendiente";
                content = $"<p><strong>Notificación:</strong> </br></br> El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}.</p>";
                content += $"<p>Ha sido marcado como <b>PENDIENTE</b>, es necesario que complemente la información de su reporte.</p>";
                content += $"<p> Favor de verificar los comentarios del administrador y añadir las evidencias solicitadas por el equipo de soporte.</p>";
                
                resultado = new KeyValuePair<string, string>(title, content);

                return resultado;
            }

            public static KeyValuePair<string, string> StateChange_EnProceso(Reporte reporte, EnumMessageBuilderPlantilla plantilla)
            {
                var resultado = new KeyValuePair<string, string>();

                string content = string.Empty;
                string title = string.Empty;

                switch (plantilla)
                {
                    case EnumMessageBuilderPlantilla.Jefe:
                        title = $"Notificación {reporte.Tag}, Nuevo Estado: En proceso";
                        content = $"<p><strong>Notificación:</strong> </br></br> Se ha asignado exitosamente el reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong>, reportado el {reporte.FechaReporte.ToLongDateString()}.</p>";
                        content +=$"<p>Ha sido marcado como <b>EN PROCESO</b>.</p>";
                        break;
                    case EnumMessageBuilderPlantilla.Asignado:
                        title = $"Notificación {reporte.Tag}";
                        content = $"Se te ha asignado el reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong>, reportado el {reporte.FechaReporte.ToLongDateString()}.";
                        break;
                    case EnumMessageBuilderPlantilla.Cliente:
                    default:
                        title = $"Notificación {reporte.Tag}";
                        content = $"<strong>Notificación:</strong> </br></br>El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong>, reportado el {reporte.FechaReporte.ToLongDateString()}.";
                        content += $"<p>Ha sido marcado como <b>EN PROCESO</b> y fue asignado a uno de nuestros expertos para su seguimiento.</p>";
                        break;
                }

                resultado = new KeyValuePair<string, string>(title, content);

                return resultado;
            }

            public static KeyValuePair<string, string> StateChange_PorValidar(Reporte reporte)
            {
                var resultado = new KeyValuePair<string, string>();
                
                string content = string.Empty;
                string title = string.Empty;

                title = $"Notificación {reporte.Tag}, Nuevo Estado: Por Validar";
                content = $"<strong>Notificación:</strong> </br></br>El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}.";
                content +=  $"<p>Ha sido terminado y ha sido marcado como <b>POR VALIDAR</b>.</p>";
                content += (reporte.VersionDeCorreccion != null) ? $"Favor de validar en la versión {reporte.VersionDeCorreccion}." : string.Empty;

                resultado = new KeyValuePair<string, string>(title, content);
                return resultado;
            }

            public static KeyValuePair<string, string> StateChange_Terminado(Reporte reporte, Usuarios currentUser)
            {
                var resultado = new KeyValuePair<string, string>();

                string content = string.Empty;
                string title = string.Empty;

                title = $"Notificación {reporte.Tag}, Nuevo Estado: Terminado";
                content = $"<strong>Notificación:</strong> </br></br> El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}.";
                content += $"<p>Ha sido cerrado satisfactoriamente por <b>{currentUser.Nombre}</b>  y ha sido marcado como <b>TERMINADO</b>.</p>";

                resultado = new KeyValuePair<string, string>(title, content);
                return resultado;
            }

            public static KeyValuePair<string, string> StateChange_NoResuelto(Reporte reporte, Usuarios currentUser, EnumMessageBuilderPlantilla plantilla)
            {
                var resultado = new KeyValuePair<string, string>();

                string content = string.Empty;
                string title = string.Empty;

                switch (plantilla)
                {
                    case EnumMessageBuilderPlantilla.Jefe:
                        title = $"Notificación {reporte.Tag}, Nuevo Estado: No resuelto";
                        content = $"<strong>Notificación:</strong> </br></br> El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}. ";
                        content+= $"<p>Ha sido marcado como <b>NO RESUELTO</b> por {currentUser.Nombre} y está listo en la plataforma para su reasignación.</p>";
                        break;
                    case EnumMessageBuilderPlantilla.Asignado:
                        title = $"Notificación {reporte.Tag}, Nuevo Estado: No resuelto";
                        content = $"<strong>Notificación:</strong> </br></br> El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}.";
                        content += $"<p>Ha sido marcado como <b>NO RESUELTO</b> por {currentUser.Nombre}, se reasignará para su reevaluación.</p>";
                        break;
                    case EnumMessageBuilderPlantilla.Cliente:
                    default:
                        title = $"Notificación {reporte.Tag}, Nuevo Estado: No resuelto";
                        content = $"<strong>Notificación:</strong> </br></br> El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}.";
                        content += $"<p>Ha sido marcado como <b>NO RESUELTO</b> por el usuario {currentUser.Nombre}, se reanalizará el reporte y se trabajará nuevamente por nuestros expertos para dar continuidad a su reporte.</p>";
                        break;
                }

                resultado = new KeyValuePair<string, string>(title, content);
                return resultado;
            }
            
            public static KeyValuePair<string, string> StateChange_Cancelado(Reporte reporte, Usuarios currentUser)
            {
                var resultado = new KeyValuePair<string, string>();

                string content = string.Empty;
                string title = string.Empty;

                title = $"Notificación {reporte.Tag}, Nuevo Estado: Cancelado";
                content = $"<strong>Notificación:</strong> </br></br> El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}.";
                content += $"<p>Ha sido marcado como <b>CANCELADO</b> por el usuario {currentUser.Nombre} el {DateTime.Now.ToLongDateString()} para más información favor de verificar el historial de comentarios de su reporte.</p>";

                resultado = new KeyValuePair<string, string>(title, content);
                return resultado;
            }

            public static KeyValuePair<string, string> VistoBueno(Reporte reporte, Usuarios user)
            {
                var resultado = new KeyValuePair<string, string>();

                string content = string.Empty;
                string title = string.Empty;

                title = $"Notificación {reporte.Tag}, Nuevo Estado: Visto Bueno";
                content = $"<strong>Notificación:</strong> </br></br> El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}.";
                content += $"<p>Ha sido validado y está listo para su asignación a alguno de nuestros expertos.</p>";
               
                resultado = new KeyValuePair<string, string>(title, content);
                return resultado;
            }

            public static KeyValuePair<string, string> NewComment_ToAll(Reporte reporte, Usuarios currentUser, string comment)
            {
                var resultado = new KeyValuePair<string, string>();

                string content = string.Empty;
                string title = string.Empty;

                title = $"Notificación {reporte.Tag}, Nuevo comentario";
                content = $"<strong>Notificación:</strong> </br></br> El usuario <b>{currentUser.Nombre}</b> agregó un nuevo comentario al reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> reportado el {reporte.FechaReporte.ToLongDateString()}.";

                resultado = new KeyValuePair<string, string>(title, content);
                return resultado;
            }

            public static KeyValuePair<string, string> Creacion_ToAll(Reporte reporte, Usuarios currentUser, EnumMessageBuilderPlantilla plantilla)
            {
                var resultado = new KeyValuePair<string, string>();

                string content = string.Empty;
                string title = string.Empty;

                switch (plantilla)
                {
                    case EnumMessageBuilderPlantilla.Jefe:
                        title = $"Notificación {reporte.Tag}, Creado";
                        content = $"<strong>Nuevo Reporte:</strong> </br></br> El reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong> ha sido reportado el {reporte.FechaReporte.ToLongDateString()}.";
                        content += $"<p>Ingrese a la plataforma para su validación y asignación a un responsable.</p>";
                        break;
                    case EnumMessageBuilderPlantilla.Cliente:
                    default:
                        title = $"Notificación {reporte.Tag}, Creado";
                        content = $"<strong>Notificación:</strong> </br></br> <strong>{currentUser.Nombre},</strong> su reporte <strong>{reporte.Tag}: {reporte.Titulo}</strong>.";
                        content += $"<p>Ha sido registrado exitosamente en el sistema el {reporte.FechaReporte.ToLongDateString()}. <br>Su reporte será enviado a los resposables y pronto sera asignado para su seguimiento. </br>Usted recibirá las actualizaciones posteriores por este mismo medio.</p>";
                        break;
                }

                resultado = new KeyValuePair<string, string>(title, content);
                return resultado;
            }


            public static string SoporteTecnico_HtmlFooter()
            {
                var resultado = "";

                resultado += $"<small><p></br></br>Para mayor información, ingrese a la plataforma www.cmr-rx.com/Members </p>";
                resultado += $"<p>Esta es una notificación de seguimiento, favor de no responder este correo electrónico.</p>";
                resultado += $"<p></br><center>Atentamente,</p>";
                resultado += $"<p>Soporte técnico de Compañía Mexicana de Radiología, CGR S.A. de C.V.</p></center></small>";
                
                return resultado;
            }


        }

        #endregion

        #region ".  Mail"

        private static void SendMail()
        {

        }

        private static bool SendMail(MailMessage mensaje, ConfiguracionMail configuracion)
        {
            Smtp smtp = null;

            try
            {
                if (configuracion != null)
                {
                    string password = configuracion.Password;
                    string server = configuracion.Servidor;
                    int port = configuracion.Puerto;
                    string sender = configuracion.Sender;

                    mensaje.From = sender;

                    smtp = new Smtp();
                    //Configuracion de la conexion
                    if (configuracion.HabilitaSSL)
                    {
                        switch (configuracion.ModoSSL)
                        {
                            case EnumModoSSL.Explicito:
                                smtp.Connect(server, SslMode.Explicit);
                                break;
                            case EnumModoSSL.Implicito:
                                smtp.Connect(server, SslMode.Implicit);
                                break;
                            case EnumModoSSL.Manual:
                                smtp.Settings.SslAllowedSuites = TlsCipherSuite.Secure;
                                smtp.Settings.SslAllowedVersions = ObtieneVersionConfigurada(configuracion.VersionSSL);
                                smtp.Settings.SslServerName = server;
                                smtp.Connect(server, SslMode.Implicit);
                                break;
                            default:
                                smtp.Connect(server, SslMode.Implicit);
                                break;
                        }
                    }
                    else
                    {
                        // connect SMTP
                        smtp.Connect(server, port);
                    }
                    // authenticate with your email address and password
                    smtp.Login(sender, password, SmtpAuthentication.Login);

                    // send mail
                    smtp.Send(mensaje);

                    // disconnect 
                    smtp.Disconnect();

                    return true;
                }
                else
                {
                    return false;
                    //Configuración incorrecta
                }                
            }
            catch
            {
                return false;
            }
            finally
            {
                if (smtp != null)
                    smtp.Dispose();
            }
        }


        #endregion
    }


   
}
