﻿using IDigitales.Pagina.Web.Data.Consultas;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    #region ".  Desarrollo"

    public class HomeDevViewModel
    {
        public List<Reporte> ReportesEnProceso { get; private set; }
        public List<Reporte> ReportesPorValidar { get; private set; }
        public List<Reporte> ReportesTerminados { get; private set; }
        public Usuarios User { get; private set; }

        public HomeDevViewModel(Usuarios user)
        {
            ReportesEnProceso = new List<Reporte>();
            ReportesPorValidar = new List<Reporte>();
            ReportesTerminados = new List<Reporte>();

            ReportesTerminados = user.ReportesAsignados.Where(r => r.Estado == Reporte.EstadoEnum.Terminado).ToList();
            ReportesEnProceso = user.ReportesAsignados.Where(r => r.Estado == Reporte.EstadoEnum.EnProceso).ToList();
            ReportesPorValidar = user.ReportesAsignados.Where(r => r.Estado == Reporte.EstadoEnum.PorValidar).ToList();

            User = user;
        }
    }

    #endregion        

    #region ".  Servicio"

    public class HomeSitioViewModel
    {
        public List<Reporte> ReportesPendientes { get; private set; }
        public List<Reporte> ReportesEnProcesoExedente { get; private set; }
        public List<Reporte> ReportesPorAsignar { get; private set; }
        public List<Reporte> ReportesCambioReciente { get; private set; }
        public Usuarios User { get; private set; }
        public List<Modulo> Categorias { get; private set; }
        public List<Instalacion> Instalaciones { get; private set; }

        Dictionary<Reporte.EstadoEnum, Dictionary<int, int>> _estadosTotales;
        Dictionary<Modulo, TimeSpan> _dpa;
        Dictionary<Modulo, int> _dpaCount;
        Dictionary<Modulo, TimeSpan> _dps;
        Dictionary<Modulo, int> _dpsCount;
        int _total;

        public HomeSitioViewModel() { }

        public HomeSitioViewModel(IEnumerable<Instalacion> instalaciones, Usuarios user)
        {
            _estadosTotales = new Dictionary<Reporte.EstadoEnum, Dictionary<int, int>>();
            _dpa = new Dictionary<Modulo, TimeSpan>();
            _dpaCount = new Dictionary<Modulo, int>();
            _dps = new Dictionary<Modulo, TimeSpan>();
            _dpsCount = new Dictionary<Modulo, int>();
            _total = 0;

            User = user;

            List<Reporte> reportes = new List<Reporte>();
            Instalaciones = new List<Instalacion>();
            foreach (var instalacion in instalaciones)
            {
                reportes.AddRange(instalacion.Reportes);
                Instalaciones.Add(instalacion);
            }

            if (!User.TienePermiso("REPORTES_VER_TERCEROS"))
                reportes = reportes.Where(r => r.QuienReportaId == User.IdUsuario).ToList();

            ReportesEnProcesoExedente = new List<Reporte>();
            ReportesPendientes = new List<Reporte>();
            ReportesPorAsignar = new List<Reporte>();
            ReportesCambioReciente = new List<Reporte>();

            foreach (var reporte in reportes)
            {
                if (reporte.EsReporteEnProcesoExedente)
                {
                    ReportesEnProcesoExedente.Add(reporte);
                }
                if (reporte.Estado == Reporte.EstadoEnum.Pendiente)
                {
                    ReportesPendientes.Add(reporte);
                }
                if (reporte.VistoBueno == null && (reporte.Estado == Reporte.EstadoEnum.Reportado || reporte.Estado == Reporte.EstadoEnum.Requerimiento))
                {
                    ReportesPorAsignar.Add(reporte);
                }
                if (reporte.TiempoCreacionAEnProceso.HasValue)
                {
                    if (!_dpaCount.ContainsKey(reporte.Modulo))
                    {
                        _dpaCount[reporte.Modulo] = 0;
                        _dpa[reporte.Modulo] = new TimeSpan();
                    }

                    _dpaCount[reporte.Modulo]++;
                    _dpa[reporte.Modulo] += reporte.TiempoCreacionAEnProceso.Value;
                }
                if (reporte.TiempoCreacionATerminado.HasValue)
                {
                    if (!_dpsCount.ContainsKey(reporte.Modulo))
                    {
                        _dpsCount[reporte.Modulo] = 0;
                        _dps[reporte.Modulo] = new TimeSpan();
                    }

                    _dpsCount[reporte.Modulo]++;
                    _dps[reporte.Modulo] += reporte.TiempoCreacionATerminado.Value;
                }
                if (reporte.CambioReciente != null)
                {
                    ReportesCambioReciente.Add(reporte);
                }
            }
            using (var db = new ConsultasInstalaciones())
            {
                Categorias = db.ObtenerModulos();
                foreach (Reporte.EstadoEnum estado in (Reporte.EstadoEnum[])Enum.GetValues(typeof(Reporte.EstadoEnum)))
                {
                    Dictionary<int, int> dictionary = new Dictionary<int, int>();
                    foreach (Modulo categoria in Categorias)
                    {
                        var rs = reportes.Where(r => r.Estado == estado && r.Modulo.Id == categoria.Id);
                        dictionary.Add(categoria.Id, rs != null ? rs.Count() : 0);
                        _total += rs != null ? rs.Count() : 0;
                    }
                    _estadosTotales.Add(estado, dictionary);
                }
            }
        }

        public int Total(Modulo categoria, Reporte.EstadoEnum estado)
        {
            return _estadosTotales[estado][categoria.Id];
        }
        public int Total(Modulo categoria)
        {
            int total = 0;
            foreach (Reporte.EstadoEnum estado in (Reporte.EstadoEnum[])Enum.GetValues(typeof(Reporte.EstadoEnum)))
            {
                total += _estadosTotales[estado][categoria.Id];
            }
            return total;
        }
        public int Total(Reporte.EstadoEnum estado)
        {
            int total = 0;
            using (var db = new ConsultasInstalaciones())
            {
                foreach (Modulo categoria in db.ObtenerModulos())
                {
                    total += _estadosTotales[estado][categoria.Id];
                }
            }

            return total;
        }
        public int Total()
        {
            return _total;
        }

        public double DPA(Modulo categoria)
        {
            if (!_dpaCount.ContainsKey(categoria) || _dpaCount[categoria] == 0)
            {
                return 0;
            }
            else
            {
                return _dpa[categoria].Days / _dpaCount[categoria];
            }
        }

        public double DPS(Modulo categoria)
        {
            if (!_dpsCount.ContainsKey(categoria) || _dpsCount[categoria] == 0)
            {
                return 0;
            }
            else
            {
                return _dps[categoria].Days / _dpsCount[categoria];
            }
        }
    }

    #endregion

    #region ".  Admin"

    public class HomeAdminViewModel
    {
        public List<Instalacion> InstalacionesReportesVistoBueno { get; private set; }
        public Usuarios User { get; private set; }
        public List<Usuarios> Desarroladores { get; private set; }
        
        public HomeAdminViewModel() { }

        public HomeAdminViewModel(IEnumerable<Instalacion> instalaciones, Usuarios self, List<Usuarios> users)
        {
            User = self;
            Desarroladores = users.Where(u => u.Tipo == Usuarios.EnumUsuarioTipo.Desarrollo).ToList();
            InstalacionesReportesVistoBueno = new List<Instalacion>();

            foreach (var instalacion in instalaciones)
            {
                instalacion.Reportes = instalacion.Reportes.Where(
                    r => r.Estado == Reporte.EstadoEnum.Reportado || r.Estado == Reporte.EstadoEnum.Requerimiento).Where(r => r.VistoBueno != null).ToList();
                InstalacionesReportesVistoBueno.Add(instalacion);
            }
        }        
    }

    #endregion
    
    #region ".  Excel"

    public class ExcelViewStartViewModel
    {
        public ReporteImportado ReporteImportado { get; set; }
        public List<ExcelSheet> Paginas { get; set; }

        public ExcelViewStartViewModel()
        {
            ReporteImportado = new ReporteImportado();
            Paginas = new List<ExcelSheet>();
        }
        public class ExcelSheet
        {
            public int Position { get; set; }
            public Modulo Categoria { get; set; }
            public bool Activo { get; set; }
            public bool Correcto { get; set; }
            public ExcelSheet(int position, Modulo categoria)
            {
                Position = position;
                Categoria = categoria;
                Activo = true;
                Correcto = true;
            }
            public ExcelSheet(string categoria)
            {
                Categoria = new Modulo { Aplicaciones = new Aplicaciones { Nombre = categoria, }, Subcategoria = categoria, };
                Activo = false;
                Correcto = false;
            }
            public ExcelSheet() { }
        }
    }

    public class ExcelViewViewModel
    {
        public ReporteImportado ReporteImportado { get; set; }
        public List<UsuarioPorCrear> UsuariosPorCrear { get; set; }
        public List<ReporteExcel> Reportes { get; set; }

        public ExcelViewViewModel()
        {
            UsuariosPorCrear = new List<UsuarioPorCrear>();
            Reportes = new List<ReporteExcel>();
        }

        public class UsuarioPorCrear
        {
            public string RawInput { get; set; }
            public string Nombre { get; set; }
            public string Password { get; set; }
            public int? UsuariosId { get; set; }
            public Usuarios.EnumUsuarioTipo Tipo { get; set; }

        }

        public class ReporteExcel
        {
            public int Row { get; set; }
            public int SheetId { get; set; }
            public bool Confirmed { get; set; }
            public int InstalacionId { get; set; }
            public int ModuloId { get; set; }
            public string ModuloNombre { get; set; }
            public ErrorValuePair<DateTime> FechaReporte { get; set; }
            public ErrorValuePair<DateTime?> FechaSolucion { get; set; }
            public ErrorValuePair<DateTime?> FechaAsignacion { get; set; }
            public ErrorValuePair<string> Descripcion { get; set; }
            public ErrorValuePair<List<string>> Comentarios { get; set; }
            public ErrorValuePair<List<string>> Links { get; set; }
            public ErrorValuePair<Reporte.EstadoEnum> Estado { get; set; }
            public ErrorValuePair<Reporte.PrioridadEnum> Prioridad { get; set; }
            public ErrorValuePair<UsuarioPorCrear> QuienReporta { get; set; }
            public ErrorValuePair<UsuarioPorCrear> VistoBueno { get; set; }
            public ErrorValuePair<UsuarioPorCrear> Asignado { get; set; }

            public bool IsValid
            {
                get
                {
                    if ((FechaReporte == null || FechaReporte.ReporteExcelType == ReporteExcelTypeEnum.Error) &&
                        (Descripcion == null || Descripcion.ReporteExcelType == ReporteExcelTypeEnum.Error) &&
                        QuienReporta == null || QuienReporta.ReporteExcelType == ReporteExcelTypeEnum.Error
                        )
                        return false;

                    return true;
                }
            }

            public bool RequiereAtencion
            {
                get
                {
                    if (FechaReporte.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || FechaReporte.ReporteExcelType == ReporteExcelTypeEnum.Error ||
                        FechaSolucion.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || FechaSolucion.ReporteExcelType == ReporteExcelTypeEnum.Error ||
                        FechaAsignacion.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || FechaAsignacion.ReporteExcelType == ReporteExcelTypeEnum.Error ||
                        Descripcion.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || Descripcion.ReporteExcelType == ReporteExcelTypeEnum.Error ||
                        Estado.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || Estado.ReporteExcelType == ReporteExcelTypeEnum.Error ||
                        Prioridad.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || Prioridad.ReporteExcelType == ReporteExcelTypeEnum.Error ||
                        QuienReporta.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || QuienReporta.ReporteExcelType == ReporteExcelTypeEnum.Error ||
                        VistoBueno.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || VistoBueno.ReporteExcelType == ReporteExcelTypeEnum.Error ||
                        Asignado.ReporteExcelType == ReporteExcelTypeEnum.MissingSomething || Asignado.ReporteExcelType == ReporteExcelTypeEnum.Error
                        )
                        return true;
                    return false;
                }
            }

            public ReporteExcel()
            {
                QuienReporta = new ErrorValuePair<UsuarioPorCrear>();
                Asignado = new ErrorValuePair<UsuarioPorCrear>();
                VistoBueno = new ErrorValuePair<UsuarioPorCrear>();
            }

            public class ErrorValuePair<T>
            {
                public T Value
                {
                    get
                    {
                        if (_value == null)
                        {
                            return default(T);
                        }
                        else
                        {
                            return _value;
                        }
                    }
                    set
                    {
                        _value = value;
                    }
                }

                public bool CreaNuevoObjeto { get; set; }

                private T _value;

                public string RawValue { get; set; }
                public string Error { get; set; }
                public ReporteExcelTypeEnum ReporteExcelType { get; set; }

                public ErrorValuePair()
                {

                }

                public ErrorValuePair(string input)
                {
                    RawValue = input;
                    CreaNuevoObjeto = false;
                    Error = "No se encontró una celda con este contenido";
                    ReporteExcelType = ReporteExcelTypeEnum.MissingSomething;
                }
            }
            public enum ReporteExcelTypeEnum
            {
                Valid, ValidAutomatic, MissingSomething, Error,
            }
        }
    }

    #endregion

    #region ".  Notificaciones"

    public class ConfiguracionNotificationsViewModel
    {
        public bool? None { get; set; }
        public bool? Creacion { get; set; }
        public bool? StateChange_EnProceso { get; set; }
        public bool? StateChange_Pendiente { get; set; }
        public bool? StateChange_PorValidar { get; set; }
        public bool? StateChange_Terminado { get; set; }
        public bool? StateChange_NoResuelto { get; set; }
        public bool? StateChange_Cancelado { get; set; }
        public bool? NewComments { get; set; }
        public bool? VistoBueno { get; set; }

        private ushort _None
        {
            get
            {
                if (None.HasValue && None.Value)
                    return 0;
                else return 0;
            }
        }
        private ushort _StateChange_EnProceso
        {
            get
            {
                if (StateChange_EnProceso.HasValue && StateChange_EnProceso.Value)
                    return 1;
                else return 0;
            }
        }
        private ushort _StateChange_Pendiente
        {
            get
            {
                if (StateChange_Pendiente.HasValue && StateChange_Pendiente.Value)
                    return 2;
                else return 0;
            }
        }
        private ushort _StateChange_PorValidar
        {
            get
            {
                if (StateChange_PorValidar.HasValue && StateChange_PorValidar.Value)
                    return 4;
                else return 0;
            }
        }
        private ushort _StateChange_Terminado
        {
            get
            {
                if (StateChange_Terminado.HasValue && StateChange_Terminado.Value)
                    return 8;
                else return 0;
            }
        }
        private ushort _StateChange_NoResuelto
        {
            get
            {
                if (StateChange_NoResuelto.HasValue && StateChange_NoResuelto.Value)
                    return 16;
                else return 0;
            }
        }
        private ushort _StateChange_Cancelado
        {
            get
            {
                if (StateChange_Cancelado.HasValue && StateChange_Cancelado.Value)
                    return 32;
                else return 0;
            }
        }
        private ushort _NewComments
        {
            get
            {
                if (NewComments.HasValue && NewComments.Value)
                    return 64;
                else return 0;
            }
        }
        private ushort _VistoBueno
        {
            get
            {
                if (VistoBueno.HasValue && VistoBueno.Value)
                    return 128;
                else return 0;
            }
        }

        private ushort _Creacion
        {
            get
            {
                if (Creacion.HasValue && Creacion.Value)
                    return 256;
                else return 0;
            }
        }

        public ushort Total
        {
            get
            {
                return (ushort)(_None + _StateChange_EnProceso + _StateChange_Pendiente + _StateChange_PorValidar +
                    _StateChange_Terminado + _StateChange_NoResuelto + _StateChange_Cancelado + _NewComments + _VistoBueno + _Creacion);
            }
        }

        public UsuariosNotificationsConfiguration.NotificationsEnum Flag
        {
            get
            {
                return (UsuariosNotificationsConfiguration.NotificationsEnum)(_None | _StateChange_EnProceso | _StateChange_Pendiente | _StateChange_PorValidar |
                    _StateChange_Terminado | _StateChange_NoResuelto | _StateChange_Cancelado | _NewComments | _VistoBueno | _Creacion);
            }
        }

        public ConfiguracionNotificationsViewModel() { }
        public ConfiguracionNotificationsViewModel(UsuariosNotificationsConfiguration configuration)
        {
            None = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.None);
            Creacion = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.Creacion);
            StateChange_EnProceso = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_EnProceso);
            StateChange_Pendiente = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_Pendiente);
            StateChange_PorValidar = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_PorValidar);
            StateChange_Terminado = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_Terminado);
            StateChange_NoResuelto = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_NoResuelto);
            StateChange_Cancelado = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.StateChange_Cancelado);
            NewComments = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.NewComments);
            VistoBueno = configuration.NotificationConfiguration.HasFlag(UsuariosNotificationsConfiguration.NotificationsEnum.VistoBueno);
        }

        public static string ConfigurationPropertyHelp(string property)
        {
            switch (property)
            {
                case "None":
                    return "";
                case "Creacion":
                    return "Recibe una notificación cuando un reporte en el que participes haya sido creado";
                case "StateChange_EnProceso":
                    return "Recibe una notificación cuando un reporte en el que participes haya sido asignado a un desarrollador";
                case "StateChange_Pendiente":
                    return "recibe una notificación cuando un reporte en el que participes haya sido marcado como pendiente";
                case "StateChange_PorValidar":
                    return "Recibe una notificación cuando un reporte en el que participes haya sido marcado para validar por el desarrollador asignado";
                case "StateChange_Terminado":
                    return "Recibe una notificación cuando un reporte en el que participes haya concluido";
                case "StateChange_NoResuelto":
                    return "Recibe una notificación cuando un reporte en el que participes en espera de validación haya sido marcado como no resuelto";
                case "StateChange_Cancelado":
                    return "Recibe una notificación cuando un reporte en el que participes esté cancelado";
                case "NewComments":
                    return "Recibe una notificación cuando se agregue un comentario o evidencia en un reporte";
                case "VistoBueno":
                    return "Recibe una notificación cuando un reporte en el que participes haya sido marcado con visto bueno";
                default:
                    return "";
            }
        }
    }

    #endregion

    #region ".  Reportes"

    public class ReportesTableViewModel
    {
        public List<Reporte> ReportesPendientes { get; private set; }
        public List<Reporte> ReportesEnProcesoExedente { get; private set; }
        public List<Reporte> ReportesPorAsignar { get; private set; }
        public List<Reporte> ReportesCambioReciente { get; private set; }
        public List<ReportesCategorias> Categorias { get; private set; }

        Dictionary<Reporte.EstadoEnum, Dictionary<int, int>> _estadosTotales;
        Dictionary<ReportesCategorias, TimeSpan> _dpa;
        Dictionary<ReportesCategorias, int> _dpaCount;
        Dictionary<ReportesCategorias, TimeSpan> _dps;
        Dictionary<ReportesCategorias, int> _dpsCount;
        int _total;

        public ReportesTableViewModel(IEnumerable<Instalacion> instalaciones, Usuarios self, List<Usuarios> users, List<ReportesCategorias> categorias)
        {
            _estadosTotales = new Dictionary<Reporte.EstadoEnum, Dictionary<int, int>>();
            _dpa = new Dictionary<ReportesCategorias, TimeSpan>();
            _dpaCount = new Dictionary<ReportesCategorias, int>();
            _dps = new Dictionary<ReportesCategorias, TimeSpan>();
            _dpsCount = new Dictionary<ReportesCategorias, int>();
            _total = 0;

            List<Reporte> reportes = new List<Reporte>();
            foreach (var instalacion in instalaciones)
            {
                reportes.AddRange(instalacion.Reportes);
            }

            if (!self.TienePermiso("REPORTES_VER_TERCEROS"))
                reportes = reportes.Where(r => r.QuienReportaId == self.IdUsuario).ToList();

            ReportesEnProcesoExedente = new List<Reporte>();
            ReportesPendientes = new List<Reporte>();
            ReportesPorAsignar = new List<Reporte>();
            ReportesCambioReciente = new List<Reporte>();

            foreach (var reporte in reportes)
            {
                if (reporte.EsReporteEnProcesoExedente)
                {
                    ReportesEnProcesoExedente.Add(reporte);
                }
                if (reporte.Estado == Reporte.EstadoEnum.Pendiente)
                {
                    ReportesPendientes.Add(reporte);
                }
                if (reporte.VistoBueno != null && (reporte.Estado == Reporte.EstadoEnum.Reportado || reporte.Estado == Reporte.EstadoEnum.Requerimiento))
                {
                    ReportesPorAsignar.Add(reporte);
                }
                if (reporte.TiempoCreacionAEnProceso.HasValue && reporte.ReportesCategorias != null)
                {
                    if (!_dpaCount.ContainsKey(reporte.ReportesCategorias))
                    {
                        _dpaCount[reporte.ReportesCategorias] = 0;
                        _dpa[reporte.ReportesCategorias] = new TimeSpan();
                    }

                    _dpaCount[reporte.ReportesCategorias]++;
                    _dpa[reporte.ReportesCategorias] += reporte.TiempoCreacionAEnProceso.Value;
                }
                if (reporte.TiempoCreacionATerminado.HasValue && reporte.ReportesCategorias != null)
                {
                    if (!_dpsCount.ContainsKey(reporte.ReportesCategorias))
                    {
                        _dpsCount[reporte.ReportesCategorias] = 0;
                        _dps[reporte.ReportesCategorias] = new TimeSpan();
                    }

                    _dpsCount[reporte.ReportesCategorias]++;
                    _dps[reporte.ReportesCategorias] += reporte.TiempoCreacionATerminado.Value;
                }
                if (reporte.CambioReciente != null)
                {
                    ReportesCambioReciente.Add(reporte);
                }
            }

            Categorias = categorias;
            foreach (Reporte.EstadoEnum estado in (Reporte.EstadoEnum[])Enum.GetValues(typeof(Reporte.EstadoEnum)))
            {
                Dictionary<int, int> dictionary = new Dictionary<int, int>();
                foreach (ReportesCategorias categoria in Categorias)
                {

                    var rs = reportes.Where(r => r.ReportesCategorias != null && r.Estado == estado && r.ReportesCategorias.Id == categoria.Id);
                    dictionary.Add(categoria.Id, rs.Count());
                    _total += rs.Count();
                }
                _estadosTotales.Add(estado, dictionary);
            }
        }

        public int Total(ReportesCategorias categoria, Reporte.EstadoEnum estado)
        {
            if (_estadosTotales.ContainsKey(estado) && _estadosTotales[estado].ContainsKey(categoria.Id))
                return _estadosTotales[estado][categoria.Id];
            return 0;
        }
        public int Total(ReportesCategorias categoria)
        {
            int total = 0;
            foreach (Reporte.EstadoEnum estado in (Reporte.EstadoEnum[])Enum.GetValues(typeof(Reporte.EstadoEnum)))
            {
                if (_estadosTotales.ContainsKey(estado) && _estadosTotales[estado].ContainsKey(categoria.Id))
                    total += _estadosTotales[estado][categoria.Id];
            }
            return total;
        }
        public int Total(Reporte.EstadoEnum estado)
        {
            int total = 0;
            using (var db = new ConsultasInstalaciones())
            {
                foreach (ReportesCategorias categoria in db.ObtenerReportesCategorias())
                {
                    if(_estadosTotales.ContainsKey(estado) && _estadosTotales[estado].ContainsKey(categoria.Id))
                        total += _estadosTotales[estado][categoria.Id];
                }
            }

            return total;
        }
        public int Total()
        {
            return _total;
        }

        public double DPA(ReportesCategorias categoria)
        {
            if (!_dpaCount.ContainsKey(categoria) || _dpaCount[categoria] == 0)
            {
                return 0;
            }
            else
            {
                return _dpa[categoria].Days / _dpaCount[categoria];
            }
        }

        public double DPS(ReportesCategorias categoria)
        {
            if (!_dpsCount.ContainsKey(categoria) || _dpsCount[categoria] == 0)
            {
                return 0;
            }
            else
            {
                return _dps[categoria].Days / _dpsCount[categoria];
            }
        }
    }

    public class ReportesListaViewModel
    {
        public int? InstalacionId { get; set; }
        public string Busqueda { get; set; }
        public List<Reporte> Reportes { get; set; }
        public Reporte.EstadoEnum? Estado { get; set; }
        public Reporte.PrioridadEnum? Prioridad { get; set; }

        [Display(Name = "Categoría")]
        public int? CategoriaId { get; set; }
    }

    #endregion

    #region ".  Configuracion"

    public class InstalacionesConfiguracionRutasViewModel
    {
        public string RutaReportesImportados { get; set; }
        public string RutaEvidencias { get; set; }
    }

    #endregion
}
