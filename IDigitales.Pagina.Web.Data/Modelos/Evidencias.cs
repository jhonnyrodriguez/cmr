﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    public class Evidencias
    {
        /// <summary>
        /// Guarda una imágen o vídeo al sistema de evidencias. Si el archivo es grande es posible que se comprima.
        /// </summary>
        /// <param name="rutaBase">El directorio en donde se guarda el archivo.</param>
        /// <param name="uploadFile">El archivo a guardar. Debe tener un formato de imágen o vídeo.</param>
        /// <param name="fileName">El nombre que se le dará al archivo guardado, sin extensión.</param>
        /// <returns>La ruta relativa para encontrar el archivo.</returns>
        public static string SaveFile(string rutaBase, HttpPostedFileBase uploadFile, string fileName)
        {
            //revisa si el archivo está vacío
            if (uploadFile == null || uploadFile.ContentLength == 0)
                throw new Exception("El archivo está vacío.");

            //revisa si tiene la extensión de img, video o una incorrecta            
            string extension = Path.GetExtension(uploadFile.FileName).ToLower();

            string path0 = string.Format("{0}/{1}", rutaBase, fileName + extension);
            string output = string.Empty;

            //crea el directorio si no existe            
            if (!Directory.Exists(path0))
            {
                Directory.CreateDirectory(rutaBase);
            }

            if (TypeByExtension(extension) == EvidenciaType.Image)
            {
                //el archivo es una imagen
                Image img = Image.FromStream(uploadFile.InputStream);
                output = fileName + ".jpeg";
                img.Save(string.Format("{0}/{1}", rutaBase, output), System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            else if (TypeByExtension(extension) == EvidenciaType.Video)
            {
                //el archivo es un video
                //primero guardamos en video temporal
                output = fileName + extension;
                string temp = string.Format("{0}/{1}", rutaBase, output);
                uploadFile.SaveAs(temp);                
            }
            else
            {
                throw new Exception("El archivo tiene un formato no soportado por el sistema. Imágenes: .png, .jpg, .jpeg, .bmp. Video: .mp4, .ogg, .webm");
            }
          
            return output;
        }

        /// <summary>
        /// Recupera una archivo del sistema de evidencias.
        /// </summary>
        /// <param name="rutaBase">El directorio en donde está el archivo.</param>
        /// <param name="rutaRelativa">El nombre y extensión del archivo.</param>
        /// <returns></returns>
        public static FileStream GetFile(string rutaBase, string rutaRelativa)
        {
            string path1 = string.Format("{0}/{1}", rutaBase, rutaRelativa);
            return new FileStream(path1, FileMode.Open);
        }

        public enum EvidenciaType
        {
            None, Image, Video,
        }

        public static EvidenciaType TypeByExtension(string extension)
        {
            string[] validFileTypesImg = { ".png", ".jpg", ".jpeg", ".bmp" };
            string[] validFileTypesVid = { ".mp4", ".ogg", ".webm" };

            if (validFileTypesImg.Contains(extension.ToLower()))
            {
                return EvidenciaType.Image;
            }
            if (validFileTypesVid.Contains(extension.ToLower()))
            {
                return EvidenciaType.Video;
            }
            return EvidenciaType.None;
        }

        public static bool IsValid(HttpPostedFileBase uploadFile)
        {
            if (uploadFile == null || uploadFile.ContentLength == 0)
                return false;         
            string extension = Path.GetExtension(uploadFile.FileName).ToLower();

            if (TypeByExtension(extension) == EvidenciaType.Image)
            {
            }
            else if (TypeByExtension(extension) == EvidenciaType.Video)
            {
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
