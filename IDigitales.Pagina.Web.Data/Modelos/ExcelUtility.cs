﻿using ClosedXML.Excel;
using IDigitales.Pagina.Web.Data.Consultas;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IDigitales.Pagina.Web.Data.Modelos.ExcelViewViewModel;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    public class ExcelUtility
    {
        public static ExcelViewStartViewModel StartExcelView(string strFilePath, ConsultasInstalaciones db)
        {
            XLWorkbook workbook = null;
            ExcelViewStartViewModel model = new ExcelViewStartViewModel();

            try
            {
                workbook = new XLWorkbook(strFilePath);                
                foreach (var pagina in workbook.Worksheets)
                {
                    Modulo categoria = db.ObtenerModuloPorSubcategoria(pagina.Name);
                    ExcelViewStartViewModel.ExcelSheet sheet = categoria != null ? new ExcelViewStartViewModel.ExcelSheet(pagina.Position, categoria) : new ExcelViewStartViewModel.ExcelSheet(pagina.Name);
                    model.Paginas.Add(sheet);
                }
            }
            catch
            {

            }
            finally
            {
                if (workbook != null)
                    workbook.Dispose();
            }
            
            return model;
        }
        public static ExcelViewViewModel ConvertXSLXtoDataTable(string strFilePath, ExcelViewStartViewModel start)
        {
            XLWorkbook workbook = null;
            ExcelViewViewModel model = new ExcelViewViewModel();
            try
            {
                workbook = new XLWorkbook(strFilePath);
                var sheets = start.Paginas.Where(s => s.Activo && s.Correcto);
                foreach(var sheet in sheets)
                {
                    IXLWorksheet worksheet = workbook.Worksheet(sheet.Position);
                    //Range for reading the cells based on the last cell used.  
                    string readRange = "1:1";
                    var rows = worksheet.RowsUsed().Except(worksheet.RowsUsed().Where(r => r == worksheet.FirstRowUsed()));
                    for(int i = 0; i < rows.Count(); i++)
                    {
                        var row = rows.ToList()[i];
                        readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber > 15 ? row.LastCellUsed().Address.ColumnNumber : 16);
                        ReporteExcel reporte = new ReporteExcel();

                        foreach (IXLCell cell in row.Cells(readRange))
                        {
                            try
                            {
                                var columnName = cell.WorksheetColumn().FirstCell().RichText.ToString();

                                string value;
                                if (cell.TryGetValue(out value))
                                {
                                    //fecha de reporte
                                    if (columnName.ToLower().Trim().Contains("fecha") && (columnName.ToLower().Trim().Contains("reporte") || columnName.ToLower().Trim().Contains("solicitud")))
                                    {
                                        reporte.FechaReporte = FechaReporte(value);
                                    }

                                    //descripción
                                    if (columnName.ToLower().Trim().Contains("descripción") || columnName.ToLower().Trim().Contains("descripcion"))
                                    {
                                        reporte.Descripcion = Descripcion(value);
                                    }

                                    //comentarios
                                    if (columnName.ToLower().Trim().Contains("comentarios") || columnName.ToLower().Trim().Contains("observaciones"))
                                    {
                                        reporte.Comentarios = Comentarios(value);
                                    }

                                    //comentarios
                                    if (columnName.ToLower().Trim().Contains("link") || columnName.ToLower().Trim().Contains("evidencia"))
                                    {
                                        reporte.Links = Links(value);
                                    }

                                    //estado
                                    if (columnName.ToLower().Trim().Contains("estado"))
                                    {
                                        reporte.Estado = Estado(value);
                                    }

                                    //prioridad
                                    if (columnName.ToLower().Trim().Contains("prioridad"))
                                    {
                                        reporte.Prioridad = Prioridad(value);
                                    }

                                    //quien reporta
                                    if (columnName.ToLower().Trim().Contains("reporta"))
                                    {
                                        reporte.QuienReporta = QuienReporta(value, null);
                                    }

                                    //visto bueno
                                    if (columnName.ToLower().Trim().Contains("visto") && columnName.ToLower().Trim().Contains("bueno"))
                                    {
                                        reporte.VistoBueno = VistoBueno(value, null);
                                    }

                                    //asignado
                                    if (columnName.ToLower().Trim().Contains("asignado"))
                                    {
                                        reporte.Asignado = Asignado(value, null);
                                    }

                                    //fecha solución
                                    if (columnName.ToLower().Trim().Contains("fecha") &&
                                        (columnName.ToLower().Trim().Contains("solucion") || columnName.ToLower().Trim().Contains("solución")))
                                    {
                                        reporte.FechaSolucion = FechaSolucion(value);
                                    }

                                    //fecha asignacion
                                    if (columnName.ToLower().Trim().Contains("fecha") &&
                                        (columnName.ToLower().Trim().Contains("asignacion") || columnName.ToLower().Trim().Contains("asignación")))
                                    {
                                        reporte.FechaAsignacion = FechaAsignacion(value);
                                    }
                                }
                                else
                                {

                                }
                            }
                            catch
                            {

                            }
                        }
                        reporte = RevisionReporteExcel(reporte);
                        reporte.Row = row.RangeAddress.LastAddress.RowNumber;
                        reporte.SheetId = sheet.Position;
                        reporte.InstalacionId = start.ReporteImportado.InstalacionId;
                        reporte.ModuloId = sheet.Categoria.Id;
                        reporte.ModuloNombre = sheet.Categoria.Subcategoria;
                        model.Reportes.Add(reporte);

                        if (reporte.QuienReporta.CreaNuevoObjeto)
                        {
                            bool usuarioYaCreado = false;
                            foreach(var yaCreado in model.UsuariosPorCrear)
                            {
                                if(yaCreado.Nombre == reporte.QuienReporta.Value.Nombre)
                                {
                                    usuarioYaCreado = true;
                                }
                            }
                            if (!usuarioYaCreado)
                            {
                                model.UsuariosPorCrear.Add(reporte.QuienReporta.Value);
                            }                                
                        }

                        if (reporte.Asignado.CreaNuevoObjeto)
                        {
                            bool usuarioYaCreado = false;
                            foreach (var yaCreado in model.UsuariosPorCrear)
                            {
                                if (yaCreado.Nombre == reporte.Asignado.Value.Nombre)
                                {
                                    usuarioYaCreado = true;
                                }
                            }
                            if (!usuarioYaCreado)
                            {
                                model.UsuariosPorCrear.Add(reporte.Asignado.Value);
                            }
                        }

                        if (reporte.VistoBueno.CreaNuevoObjeto)
                        {
                            bool usuarioYaCreado = false;
                            foreach (var yaCreado in model.UsuariosPorCrear)
                            {
                                if (yaCreado.Nombre == reporte.VistoBueno.Value.Nombre)
                                {
                                    usuarioYaCreado = true;
                                }
                            }
                            if (!usuarioYaCreado)
                            {
                                model.UsuariosPorCrear.Add(reporte.VistoBueno.Value);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if(workbook != null)
                {
                    workbook.Dispose();
                }
            }
            return model;
        }

        public static ExcelViewViewModel RevisarXSLXtoDataTable(ExcelViewViewModel inputModel, Dictionary<string, int?> modifications)
        {
            ExcelViewViewModel model = new ExcelViewViewModel();
            try
            {
                model.ReporteImportado = inputModel.ReporteImportado;                

                foreach (var reporteInput in inputModel.Reportes)
                {
                    var reporte = new ReporteExcel();

                    reporte.Row = reporteInput.Row;
                    reporte.Confirmed = reporteInput.Confirmed;
                    reporte.InstalacionId = reporteInput.InstalacionId;
                    reporte.ModuloId = reporteInput.ModuloId;
                    reporte.ModuloNombre = reporteInput.ModuloNombre;
                    reporte.SheetId = reporteInput.SheetId;

                    //fecha de reporte
                    reporte.FechaReporte = FechaReporte(reporteInput.FechaReporte.RawValue);

                    //descripción
                    reporte.Descripcion = Descripcion(reporteInput.Descripcion.RawValue);

                    //comentarios
                    reporte.Comentarios = Comentarios(reporteInput.Comentarios.RawValue);

                    //links
                    reporte.Links = Links(reporteInput.Links.RawValue);

                    //estado
                    reporte.Estado = Estado(reporteInput.Estado.RawValue);

                    //prioridad
                    reporte.Prioridad = Prioridad(reporteInput.Prioridad.RawValue);

                    //quien reporta
                    reporte.QuienReporta = QuienReporta(reporteInput.QuienReporta.RawValue, modifications);

                    //visto bueno
                    reporte.VistoBueno = VistoBueno(reporteInput.VistoBueno.RawValue, modifications);

                    //asignado
                    reporte.Asignado = Asignado(reporteInput.Asignado.RawValue, modifications);

                    //fecha solución
                    reporte.FechaSolucion = FechaSolucion(reporteInput.FechaSolucion.RawValue);

                    //fecha asignacion
                    reporte.FechaAsignacion = FechaAsignacion(reporteInput.FechaAsignacion.RawValue);
                    
                    reporte = RevisionReporteExcel(reporte);

                    model.Reportes.Add(reporte);

                    if (reporte.QuienReporta.CreaNuevoObjeto)
                    {
                        bool usuarioYaCreado = false;
                        foreach (var yaCreado in model.UsuariosPorCrear)
                        {
                            if (yaCreado.Nombre == reporte.QuienReporta.Value.Nombre)
                            {
                                usuarioYaCreado = true;
                            }
                        }
                        if (!usuarioYaCreado)
                        {
                            model.UsuariosPorCrear.Add(reporte.QuienReporta.Value);
                        }
                    }

                    if (reporte.Asignado.CreaNuevoObjeto)
                    {
                        bool usuarioYaCreado = false;
                        foreach (var yaCreado in model.UsuariosPorCrear)
                        {
                            if (yaCreado.Nombre == reporte.Asignado.Value.Nombre)
                            {
                                usuarioYaCreado = true;
                            }
                        }
                        if (!usuarioYaCreado)
                        {
                            model.UsuariosPorCrear.Add(reporte.Asignado.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
            }

            return model;
        }

        public static void EliminarLineasSobrantesExcel(int reporteImportadoId, string rutaBase)
        {
            XLWorkbook workbook = null;
            ConsultasInstalaciones dbi = null;
            dbi = new ConsultasInstalaciones();
            ReporteImportado reporteImportado = dbi.ObtenerReportesImportados().FirstOrDefault(r => r.Id == reporteImportadoId);

            try
            {
                if(reporteImportado != null)
                {
                    var reportesPorEliminar = reporteImportado.GetReportesImportadosDict();
                    workbook = new XLWorkbook(string.Format("{0}/{1}", rutaBase, reporteImportado.RutaOriginal));
                    foreach (var reporte in reportesPorEliminar)
                    {
                        var row = workbook.Worksheet(reporte.SheetId).Row(reporte.Row);

                        var readRange = string.Format("{0}:{1}", 2, row.LastCellUsed().Address.ColumnNumber > 15 ? row.LastCellUsed().Address.ColumnNumber : 16);

                        foreach (IXLCell cell in row.Cells(readRange))
                        {
                            cell.SetValue("");
                        }
                    }

                    reporteImportado.Estado = ReporteImportado.ImportadoEstadoEnum.Terminado;                    
                    workbook.SaveAs(string.Format("{0}/{1}", rutaBase, reporteImportado.RutaResultadoSobrantes));
                }
                else
                {
                    reporteImportado.Estado = ReporteImportado.ImportadoEstadoEnum.Error;
                }
            }
            catch (Exception ex)
            {
                reporteImportado.Estado = ReporteImportado.ImportadoEstadoEnum.Error;
            }
            finally
            {
                dbi.ActualizarReporteImportado(reporteImportado);
                if (workbook != null)
                    workbook.Dispose();
                if (dbi != null)
                    dbi.Dispose();
            }
        }

        #region ".  Propiedades"

        private static ReporteExcel.ErrorValuePair<string> Descripcion(string input)
        {
            ReporteExcel.ErrorValuePair<string> valuePair = new ReporteExcel.ErrorValuePair<string>(input);

            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input) ||
                    input.ToLower().Trim() == "descripcion" || input.ToLower().Trim() == "descripción")
                {
                    if (input.ToLower().Trim() == "descripcion" || input.ToLower().Trim() == "descripción")
                    {
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                        valuePair.Error = "Texto inválido";
                    }
                    else
                    {
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                        valuePair.Error = "Campo requerido";
                    }
                }
                else
                {
                    valuePair.Value = input;
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<List<string>> Comentarios(string input)
        {
            ReporteExcel.ErrorValuePair<List<string>> valuePair = new ReporteExcel.ErrorValuePair<List<string>>(input);
            valuePair.Value = new List<string>();

            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
                else
                {
                    var parts = input.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    foreach(var comentario in parts)
                    {
                        valuePair.Value.Add(comentario);
                    }
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<List<string>> Links(string input)
        {
            ReporteExcel.ErrorValuePair<List<string>> valuePair = new ReporteExcel.ErrorValuePair<List<string>>(input);
            valuePair.Value = new List<string>();

            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
                else
                {
                    var parts = input.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var comentario in parts)
                    {
                        valuePair.Value.Add(comentario);
                    }
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<UsuarioPorCrear> VistoBueno(string input, Dictionary<string, int?> modifications)
        {
            ReporteExcel.ErrorValuePair<UsuarioPorCrear> valuePair = new ReporteExcel.ErrorValuePair<UsuarioPorCrear>(input);
            ConsultasUsuarios dbu = null;
            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
                else
                {
                    var parts = input.Split(' ');
                    if (parts.Count() > 1)
                    {
                        input = parts.First();
                    }

                    dbu = new ConsultasUsuarios();
                    var existingUsers = dbu.UsuariosRecupera();
                    Usuarios user = null;
                    foreach (var existingUser in existingUsers)
                    {
                        if (existingUser.Nombre == input)
                        {
                            user = existingUser;
                        }
                    }
                    if (user == null)
                    {
                        if (modifications == null)
                            modifications = new Dictionary<string, int?>();

                        if (modifications.ContainsKey(input))
                        {
                            user = existingUsers.FirstOrDefault(u => u.IdUsuario == modifications[input]);
                        }
                        else
                        {
                            user = new Usuarios { Activo = true, Nombre = input, Password = input, Tipo = Usuarios.EnumUsuarioTipo.Jefe, };
                            valuePair.CreaNuevoObjeto = true;
                        }
                    }

                    valuePair.Value = new UsuarioPorCrear
                    {
                        Nombre = user.Nombre,
                        Password = user.Password,
                        RawInput = input,
                        Tipo = user.Tipo,
                        UsuariosId = valuePair.CreaNuevoObjeto ? (int?)null : user.IdUsuario,
                    };
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
            }
            catch (Exception ex)
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            finally
            {
                if (dbu != null)
                {
                    dbu.Dispose();
                }
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<UsuarioPorCrear> Asignado(string input, Dictionary<string, int?> modifications)
        {
            ReporteExcel.ErrorValuePair<UsuarioPorCrear> valuePair = new ReporteExcel.ErrorValuePair<UsuarioPorCrear>(input);
            ConsultasUsuarios dbu = null;
            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
                else
                {
                    var parts = input.Split(' ');
                    if (parts.Count() > 1)
                    {
                        input = parts.First();
                    }

                    dbu = new ConsultasUsuarios();
                    var existingUsers = dbu.UsuariosRecupera();
                    Usuarios user = null;
                    foreach (var existingUser in existingUsers)
                    {
                        if (existingUser.Nombre == input)
                        {
                            user = existingUser;
                        }
                    }
                    if (user == null)
                    {
                        if (modifications == null)
                            modifications = new Dictionary<string, int?>();

                        if (modifications.ContainsKey(input))
                        {
                            user = existingUsers.FirstOrDefault(u => u.IdUsuario == modifications[input]);
                        }
                        else
                        {
                            user = new Usuarios { Activo = true, Nombre = input, Password = input, Tipo = Usuarios.EnumUsuarioTipo.Desarrollo, };
                            valuePair.CreaNuevoObjeto = true;
                        }
                    }

                    valuePair.Value = new UsuarioPorCrear
                    {
                        Nombre = user.Nombre,
                        Password = user.Password,
                        RawInput = input,
                        Tipo = user.Tipo,
                        UsuariosId = valuePair.CreaNuevoObjeto ? (int?)null : user.IdUsuario,
                    };
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
            }
            catch(Exception ex)
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            finally
            {
                if (dbu != null)
                {
                    dbu.Dispose();
                }
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<UsuarioPorCrear> QuienReporta(string input, Dictionary<string, int?> modifications)
        {
            ReporteExcel.ErrorValuePair<UsuarioPorCrear> valuePair = new ReporteExcel.ErrorValuePair<UsuarioPorCrear>(input);
            ConsultasUsuarios dbu = null;
            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                    valuePair.Error = "Campo requerido";
                }
                else
                {
                    var parts = input.Split(' ');
                    if(parts.Count() > 1)
                    {
                        input = parts.First();
                    }

                    dbu = new ConsultasUsuarios();
                    var existingUsers = dbu.UsuariosRecupera();
                    Usuarios user = null;
                    foreach (var existingUser in existingUsers)
                    {
                        if (existingUser.Nombre == input)
                        {
                            user = existingUser;
                        }
                    }
                    if (user == null)
                    {
                        if (modifications == null)
                            modifications = new Dictionary<string, int?>();

                        if (modifications.ContainsKey(input))
                        {
                            user = existingUsers.FirstOrDefault(u => u.IdUsuario == modifications[input]);
                        }
                        else
                        {
                            user = new Usuarios { Activo = true, Nombre = input, Password = input, Tipo = Usuarios.EnumUsuarioTipo.Servicio, };
                            valuePair.CreaNuevoObjeto = true;
                        }
                    }

                    valuePair.Value = new UsuarioPorCrear
                    {
                        Nombre = user.Nombre,
                        Password = user.Password,
                        RawInput = input,
                        Tipo = user.Tipo,
                        UsuariosId = valuePair.CreaNuevoObjeto ? (int?)null : user.IdUsuario,
                    };
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            finally
            {
                if (dbu != null)
                {
                    dbu.Dispose();
                }
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<Reporte.EstadoEnum> Estado(string input)
        {
            ReporteExcel.ErrorValuePair<Reporte.EstadoEnum> valuePair = new ReporteExcel.ErrorValuePair<Reporte.EstadoEnum>(input);
            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                    valuePair.Value = Reporte.EstadoEnum.Pendiente;
                }
                else
                {
                    var estado = Reporte.EstadoFromString(input);
                    if (estado != null)
                    {
                        valuePair.Value = estado.Value;
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                    }
                    else
                    {
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                        valuePair.Value = Reporte.EstadoEnum.Pendiente;
                    }
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<Reporte.PrioridadEnum> Prioridad(string input)
        {
            ReporteExcel.ErrorValuePair<Reporte.PrioridadEnum> valuePair = new ReporteExcel.ErrorValuePair<Reporte.PrioridadEnum>(input);
            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                    valuePair.Value = Reporte.PrioridadEnum.Baja;
                }
                else
                {
                    var prioridad = Reporte.PrioridadFromString(input);
                    if (prioridad != null)
                    {
                        valuePair.Value = prioridad.Value;
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                    }
                    else
                    {
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                        valuePair.Value = Reporte.PrioridadEnum.Baja;
                    }
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<DateTime> FechaReporte(string input)
        {
            ReporteExcel.ErrorValuePair<DateTime> valuePair = new ReporteExcel.ErrorValuePair<DateTime>(input);
            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                    valuePair.Value = DateTime.Now;
                }
                else
                {
                    DateTime fecha;
                    CultureInfo culture = new CultureInfo("es-MX");
                    if (DateTime.TryParse(input, culture, DateTimeStyles.None, out fecha))
                    {
                        if(fecha.Year < 1900)
                        {
                            valuePair.Error = "El año exede el rango permitido.";
                            valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.MissingSomething;
                        }
                        else
                        {
                            valuePair.Value = fecha;
                            valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                        }                        
                    }
                    else
                    {
                        valuePair.Error = "El formato de fecha es incorrecto";
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.MissingSomething;
                    }
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<DateTime?> FechaSolucion(string input)
        {
            ReporteExcel.ErrorValuePair<DateTime?> valuePair = new ReporteExcel.ErrorValuePair<DateTime?>(input);
            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                    valuePair.Value = null;
                }
                else
                {
                    if (DateTime.TryParse(input, new CultureInfo("es-MX"), DateTimeStyles.None, out DateTime fecha))
                    {
                        if (fecha.Year < 1900)
                        {
                            valuePair.Error = "El año exede el rango permitido";
                            valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.MissingSomething;
                        }
                        else
                        {
                            valuePair.Value = fecha;
                            valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                        }
                    }
                    else
                    {
                        valuePair.Error = "El formato de fecha es incorrecto";
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.MissingSomething;
                    }
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            return valuePair;
        }

        private static ReporteExcel.ErrorValuePair<DateTime?> FechaAsignacion(string input)
        {
            ReporteExcel.ErrorValuePair<DateTime?> valuePair = new ReporteExcel.ErrorValuePair<DateTime?>(input);
            try
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                {
                    valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                    valuePair.Value = null;
                }
                else
                {
                    if (DateTime.TryParse(input, new CultureInfo("es-MX"), DateTimeStyles.None, out DateTime fecha))
                    {
                        if (fecha.Year < 1900)
                        {
                            valuePair.Error = "El año exede el rango permitido";
                            valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.MissingSomething;
                        }
                        else
                        {
                            valuePair.Value = fecha;
                            valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                        }
                    }
                    else
                    {
                        valuePair.Error = "El formato de fecha es incorrecto";
                        valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.MissingSomething;
                    }
                }
            }
            catch
            {
                valuePair.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Error;
                valuePair.Error = "Campo inválido";
            }
            return valuePair;
        }

        #endregion

        #region ".  Revisión"

        private static ReporteExcel RevisionReporteExcel(ReporteExcel reporte)
        {
            //fecha solución
            if (reporte.FechaSolucion.Value == null && Reporte.EstadoCompletado(reporte.Estado.Value))
            {
                reporte.FechaSolucion.Value = DateTime.Now;
                reporte.FechaSolucion.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                reporte.FechaSolucion.Error = "El estado del reporte requiere una fecha de solución";
            }
            if (reporte.FechaSolucion.ReporteExcelType == ReporteExcel.ReporteExcelTypeEnum.Error ||
                reporte.FechaSolucion.ReporteExcelType == ReporteExcel.ReporteExcelTypeEnum.MissingSomething)
            {
                if (!Reporte.EstadoCompletado(reporte.Estado.Value))
                {
                    reporte.FechaSolucion.Value = null;
                    reporte.FechaSolucion.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
                else
                {
                    reporte.FechaSolucion.Value = DateTime.Now;
                    reporte.FechaSolucion.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                }
            }

            //fecha asignación
            if (reporte.FechaAsignacion.Value == null && Reporte.EstadoAsignado(reporte.Estado.Value))
            {
                reporte.FechaAsignacion.Value = DateTime.Now;
                reporte.FechaAsignacion.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                reporte.FechaAsignacion.Error = "El estado del reporte requiere una fecha de asignación";
            }
            if(reporte.FechaAsignacion.ReporteExcelType == ReporteExcel.ReporteExcelTypeEnum.Error || 
                reporte.FechaAsignacion.ReporteExcelType == ReporteExcel.ReporteExcelTypeEnum.MissingSomething)
            {
                if (!Reporte.EstadoAsignado(reporte.Estado.Value))
                {
                    reporte.FechaAsignacion.Value = null;
                    reporte.FechaAsignacion.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.Valid;
                }
                else
                {
                    reporte.FechaAsignacion.Value = DateTime.Now;
                    reporte.FechaAsignacion.ReporteExcelType = ReporteExcel.ReporteExcelTypeEnum.ValidAutomatic;
                }
            }

            return reporte;
        }

        #endregion
    }
}
