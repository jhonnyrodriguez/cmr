﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    

    public class VersionArchivosViewModel
    {
        public string NombreArchivo { get; set; }
        public string Tamano { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
