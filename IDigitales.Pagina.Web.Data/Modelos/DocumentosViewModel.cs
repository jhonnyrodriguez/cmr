﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    public class DocumentosBDViewModel
    {
        public int IdDocumento { get; set; }
        public int Categoria { get; set; }
        public int SubCategoria { get; set; }
        public string Nombre { get; set; }
        public string Ruta { get; set; }
        public string Descripcion { get; set; }
        public string RutaTemporal { get; set; }
        public DateTime? Fecha { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Anio { get; set; }
        public int Hrs { get; set; }
        public int Min { get; set; }
    }

    public class TabsViewModel
    {
        public int Inicio { get; set; }
        public int Fin { get; set; }
        public int Seleccionado { get; set; }
    }    

    public class ContenidoTabsViewModel
    {
        public string IdCategoria { get; set; }
        public string Categoria { get; set; }
        public List<SubCategoriaViewModel> SubCategorias { get; set; }
    }

    public class SubCategoriaViewModel
    {
        public List<InfoDocumentosViewModel> Documentos { get; set; }
        public string IdSubcategoria { get; set; }
        public string Subcategoria { get; set; }
        public bool Mostrar { get; set; }
    }

    public class InfoDocumentosViewModel
    {
        public int IdDocumento { get; set; }
        public string Nombre { get; set; }
        public string Tamanio { get; set; }
        public DateTime? Fecha { get; set; }
        public string Extension { get; set; }
        public string clase { get; set; }
    }

}
