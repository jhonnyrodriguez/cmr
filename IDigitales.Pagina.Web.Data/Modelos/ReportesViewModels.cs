﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    #region Reportes

    public class ReporteInventarioViewModel
    {
        public string Fecha { get; set; }
        public string Version { get; set; }
        public List<InformacionProductosViewModel> Productos { get; set; }
        public List<InformacionProductosViewModel> Estructuras { get; set; }
    }

    public partial class InformacionProductosViewModel
    {
        public int IdProducto { get; set; }
        public string NumeroParte { get; set; }
        public string Descripcion { get; set; }
        public string Nombre { get; set; }
        public string NumeroParteNombre { get; set; }
        public double? StockMinimo { get; set; }
        public string ClaveSAE { get; set; }
        public double? Existencia { get; set; }
        public string Clasificacion { get; set; }
        public string Tipo { get; set; }
        public List<InformacionProductosViewModel> Componentes { get; set; }
    }

    public class ReporteListasPrecioViewModel
    {
        public string Fecha { get; set; }
        public string NombreLista { get; set; }
        public string Aplicable { get; set; }
        public string Vigencia { get; set; }
        public List<InformacionPreciosViewModel> Productos { get; set; }
        public string Version { get; set; }
        public string TipoLista { get; set; }
        public List<InformacionPreciosViewModel> ProductosHardwareServidores { get; set; }
        public List<InformacionPreciosViewModel> ProductosMonitoresSeccion1 { get; set; }
        public List<InformacionPreciosViewModel> ProductosLicencias { get; set; }
        public List<InformacionPreciosViewModel> ProductosHardwareEstaciones { get; set; }
        public List<InformacionPreciosViewModel> ProductosMobiliario { get; set; }
        public List<InformacionPreciosViewModel> ProductosServicios { get; set; }
    }

    public class InformacionPreciosViewModel
    {
        public string NumeroParte { get; set; }

        public string Descripcion { get; set; }

        public string Clasificacion { get; set; }

        public double? DatosCrudos { get; set; }

        public double? DatosUsables { get; set; }

        public double? Transfer { get; set; }

        public int IdOrdenPersonalizado { get; set; }
    }

    public class ReporteCotizacionViewModel
    {
        public int IdCotizacion { get; set; }
        public string NombreCliente { get; set; }
        public string NombreEmpresa { get; set; }
        public string NombreContacto { get; set; }
        public string EmailContacto { get; set; }
        public List<InformacionCotizacionViewModel> Productos { get; set; }
        public decimal Descuentos { get; set; }
        public decimal Total { get; set; }
    }

    public class InformacionCotizacionViewModel
    {
        public string Producto { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal Importe { get; set; }
        public decimal Subtotal { get; set; }
        public double PorcentajeDescuento { get; set; }
        public decimal Descuento { get; set; }
        public double IVA { get; set; }
        public double Total { get; set; }
    }

    #endregion
}
