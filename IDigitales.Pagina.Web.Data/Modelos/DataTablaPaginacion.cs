﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    public class DataTablaPaginacion
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public DataTablaSearch search { get; set; }
        public List<DataTablaOrder> order { get; set; }
        public List<DataTablaColumn> columns { get; set; }

    }

    public class DataTablaSearch
    {
        public string value { get; set; }
        public bool regex { get; set; }

    }
    public class DataTablaOrder
    {
        public int column { get; set; }
        public string dir { get; set; }

    }
    public class DataTablaColumn
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public DataTablaSearch search { get; set; }

    }

    public class DataTablaResult<T>
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<T> data { get; set; }
        public string error { get; set; }
        public string errorDetalles { get; set; }

        public DataTablaResult(List<T> datos)
        {
            recordsTotal = datos.Count;
            recordsFiltered = datos.Count;
            data = datos;
        }

        public DataTablaResult(List<T> datos, int total)
        {
            recordsTotal = total;
            recordsFiltered = total;
            data = datos;
        }
        public DataTablaResult(List<T> datos, int total, int filtrado)
        {
            recordsTotal = total;
            recordsFiltered = filtrado;
            data = datos;
        }
        public DataTablaResult(Exception ex)
        {
            recordsTotal = 0;
            recordsFiltered = 0;
            data = new List<T>();
            error = ex.Message;
            if (ex.InnerException != null)
                errorDetalles = ex.InnerException.Message;
        }

    }
}
