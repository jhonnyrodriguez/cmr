﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data.Modelos
{
    public class UsuariosModel
    {
        public List<Usuarios> Usuarios { get; set; }
        public bool HayMasRegistros { get; set; }
        public int Pagina { get; set; }
    }

    public class UsuariosVistasPermisosModel
    {
        public List<Vistas> Vistas { get; set; }
        public List<Permisos> Permisos { get; set; }
        public List<Aplicaciones> Aplicaciones { get; set; }
        public List<Instalacion> Instalaciones { get; set; }
        public List<ReportesCategorias> Categorias { get; set; }
        public Usuarios Usuario { get; set; }
        
    }
}
