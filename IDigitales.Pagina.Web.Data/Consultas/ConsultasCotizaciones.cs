﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using IDigitales.HIS.Web.Data.Utils;
using IDigitales.Pagina.Web.Data.Modelos;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using LinqKit;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasCotizaciones : ConsultasBase
    {
        public async Task<CotizacionModel> ObtenerCotizacion(int idCotizacion)
        {
            var model = new CotizacionModel();
            ctx.Configuration.ProxyCreationEnabled = false;
            model.Cotizacion = await ctx.Cotizaciones.Include(x=>x.ListasPrecios).FirstOrDefaultAsync(x => x.IdCotizacion == idCotizacion);

            if (model.Cotizacion != null)
            {
                model.Iva = model.Cotizacion.ListasPrecios.Iva ?? false;
                model.Cotizacion.ListasPrecios = null;

                model.CotizacionesProductos = await ctx.CotizacionesProductos
                    .Include(x => x.Productos.ListasPrecioProductos)
                    .Where(x => x.IdCotizacion == idCotizacion)
                    .Select(p => new ProductosModel
                    {
                        IdLista = model.Cotizacion.IdLista,
                        IdProducto = p.IdProducto,
                        Porcentaje = p.PorcentajeDescuento,
                        Precio = p.Productos.ListasPrecioProductos
                                     .FirstOrDefault(x => x.IdLista == model.Cotizacion.IdLista).PrecioFinal.HasValue ? 
                            ((Decimal)p.Productos.ListasPrecioProductos.FirstOrDefault(x => x.IdLista == model.Cotizacion.IdLista).PrecioFinal / (Decimal)0.75) 
                            : (p.Productos.Precio / (Decimal)0.75),//se divide el precio por .75
                        Modelo = p.Productos.Modelo,
                        Nombre = p.Productos.Nombre,
                        Cantidad = p.Cantidad,
                    }).ToListAsync();
            }


            return model;
        }

        public async Task<DataTablaResult<ProductosModel>> ObtenerProductosListas(DataTablaPaginacion pag)
        {
            DataTablaResult<ProductosModel> resultado;
            //Crear predicado para agregar condiciones

            var predicado = PredicateBuilder.New<ListasPrecioProductos>(true);

            if (pag.columns !=null && pag.columns.Any() && pag.columns[0]!=null && !string.IsNullOrEmpty(pag.columns[0].data)  )
            {
                var idlista = 0;
                if(int.TryParse(pag.columns[0].data,out idlista))
                    predicado = predicado.And(x => x.IdLista== idlista);
            }

            if (!string.IsNullOrEmpty(pag.search.value))
            {
                predicado = predicado.And(x => x.Productos.Nombre.Contains(pag.search.value) || x.Productos.Modelo.Contains(pag.search.value));
            }

            ctx.Configuration.ProxyCreationEnabled = false;
            var query = ctx.ListasPrecioProductos.Include(x => x.Productos).AsExpandable();
            var total = await ctx.ListasPrecioProductos.AsExpandable().CountAsync(predicado);

            var consultaEle = await query.AsExpandable()
                .Where(predicado)
                .OrderByDescending(x => x.IdProducto)
                .Skip(pag.start).Take(pag.length)
                .Select(x => new ProductosModel
                {
                    IdLista = x.IdLista,
                    IdProducto = x.IdProducto,
                    Precio = x.PrecioFinal.HasValue ? ((Decimal)x.PrecioFinal/ (Decimal)0.75) :(x.Productos.Precio/ (Decimal)0.75), //x.PrecioFinal ?? x.Productos.Precio,//se divide el precio por .75
                    Modelo = x.Productos.Modelo,
                    Nombre = x.Productos.Nombre,
                    Cantidad = 1,
                })
                .ToListAsync();

            //foreach (var c in consultaEle)
            //{
            //    if (c.Clasificaciones != null)
            //        c.Clasificaciones.Productos = null;
            //}

            resultado = new DataTablaResult<ProductosModel>(consultaEle, total);
            return resultado;
        }
        public async Task<ResultadoOperacion> AgregarActualizarCotizacion(Cotizaciones cot)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;

            using (var dbContextTransaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    //productos actuales
                    List<CotizacionesProductos> actuales = null;
                    
                    if (cot != null)
                    {
                        if (cot.CotizacionesProductos != null && cot.CotizacionesProductos.Any())
                        {
                            //prod actuales
                            actuales = cot.CotizacionesProductos.ToList();
                            cot.CotizacionesProductos = null;
                        }
                        //if (cot.IdCotizacion == 0)
                        //{
                        //    //cot.IdEstado = FinanzasOrdenesCompra.EnumFinanzasEstatusOrdenCompra.Generada;
                        //}
                    }

                    result = await AgregarActualizarElemento(cot);

                    //Guardar Productos
                    if (result != null && result.Resultado == OperacionResultado.Ok)
                    {
                        //Actualizar ecistentes
                        ctx.Configuration.ProxyCreationEnabled = false;
                        var existentes = await ctx.CotizacionesProductos.Where(p => p.IdCotizacion == result.Id).ToListAsync();
                        if (actuales != null && actuales.Count > 0)
                        {
                            foreach (var existente in existentes)
                            {
                                foreach (var actual in actuales.Where(a => existente.IdProducto == a.IdProducto))
                                {
                                    existente.Cantidad = actual.Cantidad;
                                    existente.PorcentajeDescuento = actual.PorcentajeDescuento;
                                    //existente.IdCotizacion = actual.IdCotizacion;
                                    //existente.IdProducto = actual.IdProducto;
                                }
                            }
                        }
                        //Agregar nuevos
                        var nuevos = actuales.Where(x => existentes.All(y => x.IdProducto != y.IdProducto)).ToList();
                        if (nuevos.Any())
                        {
                            foreach (var n in nuevos)
                            {
                                n.IdCotizacion = result.Id;
                            }
                            ctx.CotizacionesProductos.AddRange(nuevos);
                        }
                        //Eliminar Borrados
                        var borrados = existentes.Where(x => actuales.All(y => x.IdProducto != y.IdProducto)).ToList();
                        if (borrados.Any())
                        {
                            ctx.CotizacionesProductos.RemoveRange(borrados);
                        }
                        await ctx.SaveChangesAsync();

                    }
                    dbContextTransaction.Commit();
                    result.Resultado = OperacionResultado.Ok;
                }
                catch (DbEntityValidationException ex)
                {
                    dbContextTransaction.Rollback();
                    result = GetResult(ex);
                }

                return result;
            }
        }

        public Productos ObtenerInfoProducto(int idProducto)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.Productos
                   .Include(x => x.ListasPrecioProductos)
                   .FirstOrDefault(x => x.IdProducto == idProducto);

        }
    }



}
