﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.HIS.Web.Data.Utils;
using IDigitales.Pagina.Web.Data.Entidades;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasCatalogos : ConsultasBase
    {

        public async Task<IEnumerable<Catalogos>> ObtenerAplicacionesVersionesAsync(int idAplicacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ent =
                ctx.AplicacionesVersiones.Include(x => x.Aplicaciones).Where(x => x.IdAplicacion == idAplicacion)
                    .GroupBy(x => new { version = x.Version, idAplicacion= x.IdAplicacion}).Select(x => new Catalogos { Id = x.Key.version, Valor = x.Key.version, Tipo = x.Key.idAplicacion.ToString()}).OrderByDescending(x => x.Valor)
                    .ToListAsync();
            return await ent;
        }
        public async Task<IEnumerable<Catalogos>> ObtenerAplicacionesSistemasAsync(int idAplicacion, string version)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ent =
                ctx.AplicacionesVersiones.Include(x => x.Aplicaciones).Where(x => x.Version == version && x.IdAplicacion == idAplicacion).OrderByDescending(x => x.Fecha)
                    .Select(x => new Catalogos { Id = x.IdVersion.ToString(), Valor = x.Sistema == null ? "": x.Sistema, Tipo = x.Aplicaciones.Nombre, Elemento = x })
                    .ToListAsync();
            return await ent;
        }

        public async Task<IEnumerable<Catalogos>> ObtenerAplicacionesAsync()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ent =
                ctx.Aplicaciones.OrderByDescending(x => x.IdAplicacion)
                    .Select(x => new Catalogos { Id = x.IdAplicacion.ToString(), Valor = x.Nombre, Tipo = x.Icono, Elemento = x })
                    .ToListAsync();
            return await ent;
        }
        public async Task<IEnumerable<Catalogos>> ObtenerClasificaciones()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ent =
                ctx.Clasificaciones.
                    Select(x => new Catalogos { Id = x.IdClasificacion.ToString(), Valor = x.Nombre, Tipo = x.Descripcion, Elemento = x })
                    .ToListAsync();
            return await ent;
        }

        public async Task<IEnumerable<Catalogos>> ObtenerInstalaciones()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ent = ctx.Instalaciones.Where(x => x.Activo )
                .Select(x => new Catalogos { Id = x.Id.ToString(), Valor = x.Tag, Tipo = x.Descripcion, Elemento = x })
                    .ToListAsync();
            return await ent;
        }
        
        public async Task<IEnumerable<Catalogos>> ObtenerListasPrecios(bool? lNac=null, bool? lInt = null, bool? lSer = null, bool? lOtras = null)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ent = await
                ctx.ListasPrecios
                    .Select(x => new Catalogos { Id = x.IdLista.ToString(), Valor = x.Nombre, Tipo = x.Descripcion,Codigo=x.Codigo, Elemento = x })
                    .ToListAsync();

            if (lNac.HasValue)
            {
                if (lNac.Value == false)
                {
                    ent.RemoveAll(x => x.Codigo == "LN");
                }
            }

            if (lInt.HasValue)
            {
                if (lInt.Value == false)
                {
                    ent.RemoveAll(x => x.Codigo == "LI");
                }
            }
            if (lSer.HasValue)
            {
                if (lSer.Value == false)
                {
                    ent.RemoveAll(x => x.Codigo == "LS");
                }
            }
            if (lOtras.HasValue)
            {
                if (lOtras.Value == false)
                {
                    ent.RemoveAll(x => x.Codigo != "LN" && x.Codigo != "LI" && x.Codigo != "LS");
                }
            }

            return ent;
        }

        public async Task<List<CatalogoBusqueda>> ObtieneProductos(string strFiltro)
        {

            ctx.Configuration.ProxyCreationEnabled = false;

            var productos =await ctx.Productos
                .Where(x => (x.Modelo.Contains(strFiltro) || x.Nombre.Contains(strFiltro)) && x.Activo!=false)
                .Select(item => new CatalogoBusqueda
                {
                    id = item.IdProducto.ToString(),
                    valor = item.Nombre,
                    codigo = item.Modelo,
                    elemento = item
                }).ToListAsync();

            return productos;
        }
        public async Task<List<CatalogoBusqueda>> ObtieneCotizaciones(string strFiltro,int idUsuario)
        {

            ctx.Configuration.ProxyCreationEnabled = false;

            var productos = await ctx.Cotizaciones
                .Where(x => x.Nombre.Contains(strFiltro) && x.IdUsuario == idUsuario)
                .Select(item => new CatalogoBusqueda
                {
                    id = item.IdCotizacion.ToString(),
                    valor = item.Nombre,
                    codigo = item.IdCotizacion.ToString()
                }).ToListAsync();

            return productos;
        }

        public async Task<List<CatalogoBusqueda>> ObtieneContactos(string strFiltro)
        {

            ctx.Configuration.ProxyCreationEnabled = false;

            var productos = await ctx.Contactos
                .Where(x => (x.Nombre.Contains(strFiltro) || x.Email.Contains(strFiltro)) || x.Empresa.Contains(strFiltro))
                .Select(item => new CatalogoBusqueda
                {
                    id = item.IdContacto.ToString(),
                    valor = item.Nombre,
                    codigo = item.Empresa,
                    elemento = item
                }).ToListAsync();

            return productos;
        }
        public Catalogos ObtenerDescripcion<T>(int id, string nomCampoDes) where T : class, new()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            // var des = nomCampoDes.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList();
            // var sb = new StringBuilder();
            if (id == 0) return null;
            var elemento = new T();
            //Obtener iDS
            //var strIdsElemento = ObtenerKey(elemento);
            //var ids = strIdsElemento.Select(elemento.GetValor).ToList();

            //Buscar
            var elementoBd = ctx.Set<T>().Find(id);
            var resultado = new Catalogos
            {
                Id = id.ToString(),
                Valor = elementoBd.GetValor(nomCampoDes).ToString(),
                Elemento = elementoBd
            };

            return resultado;
        }
    }
}
