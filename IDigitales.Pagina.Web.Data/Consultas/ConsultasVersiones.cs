﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using IDigitales.HIS.Web.Data.Utils;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasVersiones : ConsultasBase
    {
        public List<Aplicaciones> ObtenerListaAplicaciones(int idUsuario)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var user = ctx.Usuarios
              .Include(x => x.Permisos)
              .Include(x => x.Vistas)
              .Include(x => x.Aplicaciones.Select(y => y.AplicacionesVersiones))
              .FirstOrDefault(u => u.IdUsuario == idUsuario);

            if (user !=null)
            {
                return user.Aplicaciones.ToList();
            }
            else
            {
                return null;
            }
        }
        public async Task<Aplicaciones> ObtenerAplicacion(int idAplicacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return await ctx.Aplicaciones.Include(x => x.AplicacionesVersiones)
                .FirstOrDefaultAsync(x => x.IdAplicacion == idAplicacion);
        }
       

        public async Task<ResultadoOperacion> GuardarAplicacion(Aplicaciones aplicacion)
        {
            var result = new ResultadoOperacion(){Resultado = OperacionResultado.Error};
            
            ctx.Configuration.ProxyCreationEnabled = false;

            var app = ctx.Aplicaciones.FirstOrDefault(x => x.IdAplicacion == aplicacion.IdAplicacion);
            if(app == null)
            {
                app= new Aplicaciones();
                Objetos.CopiarObjeto(aplicacion, app);
                ctx.Aplicaciones.Add(app);
            }else
            {
                Objetos.CopiarObjeto(aplicacion, app);
            }

            await ctx.SaveChangesAsync();

            if (aplicacion.AplicacionesVersiones != null && aplicacion.AplicacionesVersiones.Any())
            {
                foreach (var ap in aplicacion.AplicacionesVersiones)
                {
                    var v = ctx.AplicacionesVersiones.FirstOrDefault(x => x.IdVersion == ap.IdVersion);
                    if (v == null)
                    {
                        v = new AplicacionesVersiones();
                        Objetos.CopiarObjeto(ap, v);
                        v.IdAplicacion = app.IdAplicacion;
                        v.Fecha = DateTime.Now;
                        ctx.AplicacionesVersiones.Add(v);
                    }
                    else
                    {
                        
                        Objetos.CopiarObjeto(ap, v);
                        v.IdAplicacion = app.IdAplicacion;
                    }
                    await ctx.SaveChangesAsync();
                    result.Id2 = v.IdVersion;
                }
            }
            result.Id = app.IdAplicacion;
            result.Resultado = OperacionResultado.Ok;
            return result;

        }

        public async Task<ResultadoOperacion> ActualizaRutaAplicacion(int idAplicacion, int idVersion, string ruta)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;

            var app = ctx.AplicacionesVersiones.FirstOrDefault(x => x.IdAplicacion == idAplicacion && x.IdVersion == idVersion );
            if (app != null)
            {

                app.Ruta = ruta;
                await ctx.SaveChangesAsync();
            }
            
            result.Id = idAplicacion;
            result.Resultado = OperacionResultado.Ok;
            return result;

        }
        public AplicacionesVersiones ObtenerVersion(int idAplicacion, int idVersion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var version = ctx.AplicacionesVersiones.Include(x => x.Aplicaciones).FirstOrDefault(x => x.IdAplicacion == idAplicacion && x.IdVersion == idVersion);
            return version;

        }

        public async Task<AplicacionesVersiones> ObtenerVersionAsync(int idAplicacion, int idVersion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var version = await ctx.AplicacionesVersiones.Include(x => x.Aplicaciones).FirstOrDefaultAsync(x => x.IdAplicacion == idAplicacion && x.IdVersion == idVersion);
            return version;

        }

        public Aplicaciones ObtenerAplicacionSinc(int idAplicacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var app = ctx.Aplicaciones.FirstOrDefault(x => x.IdAplicacion == idAplicacion);
            return app;

        }

        public List<AplicacionesVersiones> ObtenerAplicacionVersiones(int idAplicacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return  ctx.AplicacionesVersiones.Where(x => x.IdAplicacion == idAplicacion).ToList();
        }


        public async Task<ResultadoOperacion> EliminarVersion(int idAplicacion, int idVersion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            var ver = await  ctx.AplicacionesVersiones.FirstOrDefaultAsync(x => x.IdAplicacion == idAplicacion && x.IdVersion == idVersion);
            if (ver != null)
            {
                ctx.AplicacionesVersiones.Remove(ver);
                await ctx.SaveChangesAsync();
            }

            result.Resultado = OperacionResultado.Ok;
            return result;
        }
        public async Task<ResultadoOperacion> EliminarAplicacion(int idAplicacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            var app = await ctx.Aplicaciones.Include(x => x.AplicacionesVersiones).FirstOrDefaultAsync(x => x.IdAplicacion == idAplicacion);
            if (app != null)
            {
                ctx.AplicacionesVersiones.RemoveRange(app.AplicacionesVersiones);
                ctx.Aplicaciones.Remove(app);
                await ctx.SaveChangesAsync();
            }

            result.Resultado = OperacionResultado.Ok;
            return result;
        }


        public async Task<ResultadoOperacion> ActualizarAplicacion(Aplicaciones aplicacion)
        {
            
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            var app = await ctx.Aplicaciones.FirstOrDefaultAsync(x => x.IdAplicacion == aplicacion.IdAplicacion);
            if (app == null)
            {
                app = new Aplicaciones();
                Objetos.CopiarObjeto(aplicacion, app);
                ctx.Aplicaciones.Add(app);
            }
            else
            {
                Objetos.CopiarObjeto(aplicacion, app);
            }

            await ctx.SaveChangesAsync();


            result.Resultado = OperacionResultado.Ok;
            result.Id = app.IdAplicacion;
            return result;
        }


        public AplicacionesVersiones ObtenerVersionPorId(int idVersion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var version = ctx.AplicacionesVersiones.FirstOrDefault(x => x.IdVersion == idVersion);
            return version;

        }

        public async Task<ResultadoOperacion> ActualizarVersion(AplicacionesVersiones version)
        {
          
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            var ver = await ctx.AplicacionesVersiones.FirstOrDefaultAsync(x => x.IdVersion == version.IdVersion);
            if (ver != null)
            {
                Objetos.CopiarObjeto(version, ver);
            }
            else
            {
                ver = new AplicacionesVersiones();
                Objetos.CopiarObjeto(version, ver);
                ver.Fecha = DateTime.Now;
                ctx.AplicacionesVersiones.Add(ver);
            }
            await ctx.SaveChangesAsync();
            result.Id = ver.IdVersion;
            result.Resultado = OperacionResultado.Ok;
            return result;
        }
        public async Task<ResultadoOperacion> ActualizaRutaVersion(int idVersion, string ruta)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;

            var app = ctx.AplicacionesVersiones.FirstOrDefault(x => x.IdVersion == idVersion);
            if (app != null)
            {

                app.Ruta = ruta;
                await ctx.SaveChangesAsync();
            }

            result.Resultado = OperacionResultado.Ok;
            return result;

        }

        public async Task<ResultadoOperacion> ActualizaCorreoEnviadoVersion(int idVersion, bool enviado)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;

            var app = ctx.AplicacionesVersiones.FirstOrDefault(x => x.IdVersion == idVersion);
            if (app != null)
            {

                app.CorreoEnviado = enviado;
                await ctx.SaveChangesAsync();
            }

            result.Resultado = OperacionResultado.Ok;
            return result;

        }

        public List<Contactos> ObtieneContactos(int idAplicacion)
        {
           
            ctx.Configuration.ProxyCreationEnabled = false;

            var app = ctx.Aplicaciones.Include(x => x.Contactos).FirstOrDefault(x => x.IdAplicacion == idAplicacion);
            if (app != null)
            {

               var contactos = app.Contactos.ToList();
                return contactos;
            }

            
            return null;

        }
    }
}
