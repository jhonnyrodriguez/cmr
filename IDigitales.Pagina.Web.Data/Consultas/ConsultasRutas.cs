﻿using System;
using System.Collections.Generic;
using System.Linq;
using IDigitales.Pagina.Web.Data.Entidades.Poco;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasRutas : ConsultasBase
    {
        public List<Rutas> RutasRecupera()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.Rutas.ToList();
        }
    }
}
