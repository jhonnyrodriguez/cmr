﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using IDigitales.HIS.Web.Data.Utils;
using IDigitales.Pagina.Web.Data.Modelos;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using LinqKit;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasProductos : ConsultasBase
    {
        public List<Productos> ProductosRecupera()
        {
                ctx.Configuration.ProxyCreationEnabled = false;
                return ctx.Productos.ToList();
           
        }

        
        public async Task<DataTablaResult<Productos>> ObtenerProductos(DataTablaPaginacion pag)
        {
            DataTablaResult<Productos> resultado;
            //Crear predicado para agregar condiciones

            var predicado = PredicateBuilder.New<Productos>(true);

            if (!string.IsNullOrEmpty(pag.search.value))
            {
                predicado = predicado.And(x => x.Nombre.Contains(pag.search.value) || x.Descripcion.Contains(pag.search.value));
            }

            ctx.Configuration.ProxyCreationEnabled = false;
            var query = ctx.Productos.Include(x=>x.Clasificaciones).AsExpandable();

            var total = await ctx.Productos.AsExpandable().CountAsync(predicado);
            var consultaEle = await query.AsExpandable().Where(predicado).OrderByDescending(x => x.IdProducto).Skip(pag.start).Take(pag.length).ToListAsync();
            
            foreach (var c in consultaEle)
            {
                if(c.Clasificaciones!=null)
                c.Clasificaciones.Productos = null;
            }
          
            resultado = new DataTablaResult<Productos>(consultaEle, total);
            return resultado;
        }

        public List<ConfiguracionesGenerales> ObtenerConfiguracionGeneral()
        {
            try
            {
                ctx.Configuration.ProxyCreationEnabled = false;
                var lista = ctx.ConfiguracionesGenerales.ToList();
                return lista;
            }
            catch (Exception)
            { 
            }
            return null;
        }

        public async Task<Productos> ObtenerProducto(int idProducto)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return await ctx.Productos.FirstOrDefaultAsync(x => x.IdProducto == idProducto);
        }

        public async Task<ResultadoOperacion> AgregarActualizarProductos(Productos p)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;

            using (var dbContextTransaction = ctx.Database.BeginTransaction())
            {
                try
                {
                   result = await AgregarActualizarElemento(p);
                    dbContextTransaction.Commit();

                }
                catch (DbEntityValidationException ex)
                {
                    dbContextTransaction.Rollback();
                    result = GetResult(ex);
                }
            }
            return result;

        }

        public async Task<DataTable> ObtenerInventarioProductos()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var productos = await ctx.Productos
                            .Include(x=> x.Clasificaciones).AsExpandable()
                            .Where(x=> x.Tipo == Productos.EnumProductoTipos.Producto || x.Tipo==Productos.EnumProductoTipos.Estructura).ToListAsync();
            DataTable dt = ToDataTable<Productos>(productos);
            return dt;
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    if (Props[i].Name != "Clasificaciones")
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }
                    else
                    {
                        Clasificaciones clas = (Clasificaciones)Props[i].GetValue(item);
                        values[i] = clas.Nombre;
                    }
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public List<ProductosComponentes> ObtenerComponentesProductos()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.ProductosComponentes.Include(x=> x.Productos_IdComponente)
                   .Include(x=>x.Productos_IdComponente.Clasificaciones).AsExpandable().ToList();             
        }

        public async Task<List<ListasPrecioProductos>> ObtenerListasPreciosProductos(int idLista)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var lista = await ctx.ListasPrecioProductos
                .Include(x => x.Productos)
                .Include(x => x.Productos.Clasificaciones)
                .Include(x => x.LIstasPrecios)
                .Where(x => x.IdLista == idLista).ToListAsync(); 
            return lista;
        }

        public async Task<ListasPrecios> ObtenerListas(int idLista)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return await ctx.ListasPrecios.FirstOrDefaultAsync(x => x.IdLista == idLista);
        }

        public async Task<ResultadoOperacion> AgregarActualizarListas(ListasPrecios l)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;

            using (var dbContextTransaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    result = await AgregarActualizarElemento(l);
                    dbContextTransaction.Commit();

                }
                catch (DbEntityValidationException ex)
                {
                    dbContextTransaction.Rollback();
                    result = GetResult(ex);
                }
            }
            return result;

        }
        public async Task<List<ListasPrecioProductos>> ObtenerListaProductos(int idLista, int idPagina = 0)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var lista = await ctx.ListasPrecioProductos
                .Include(x => x.Productos)
                .Where(x => x.IdLista == idLista).ToListAsync();

            //foreach (var l in lista)
            //{
            //   l.Productos.ListasPreciosProductos = null;
            //}
            return lista;
        }

        #region Modal Edicion ListasPreciosProductos

        public async Task<ListasPrecioProductos> ObtenerListaPreciosProducto(int idLista, int idProducto)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var pro = await ctx.ListasPrecioProductos
                .FirstOrDefaultAsync(x => x.IdLista == idLista && x.IdProducto == idProducto);
            return pro;
        }

        public async Task<ResultadoOperacion> AgregarActualizarListaPreciosProducto(ListasPrecioProductos producto)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var result = await AgregarActualizarElemento(producto);
            return result;
        }

        public async Task<ResultadoOperacion> EliminarListaPreciosProducto(int idLista, int idProducto)
        {
            ResultadoOperacion result;
            try
            {
                ctx.Configuration.ProxyCreationEnabled = false;
                result = await EliminarElemento(new ListasPrecioProductos() { IdLista = idLista, IdProducto = idProducto });
            }
            catch (DbUpdateException ex)
            {
                result = GetResult(ex);
            }
            return result;
        }

        #endregion
    }

}
