﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.HIS.Web.Data.Utils;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Modelos;
using LinqKit;
using System.ComponentModel;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasConfiguracion : ConsultasBase
    {

               
        public List<Rutas> ObtenerRutas()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.Rutas.ToList();
        }

        public async Task<ResultadoOperacion> GuardarRuta(Rutas ruta)
        {
            var result = new ResultadoOperacion(){Resultado = OperacionResultado.Error};
            ctx.Configuration.ProxyCreationEnabled = false;
            var rut = ctx.Rutas.FirstOrDefault(x => x.Tipo == ruta.Tipo);
            if (rut != null)
            {
                rut.Directorio = ruta.Directorio;
                
            }
            else
            {
                rut = new Rutas();
                rut.Directorio = ruta.Directorio;
            }

            await ctx.SaveChangesAsync();
            result.Resultado = OperacionResultado.Ok;

            return result;
        }
        public async Task<ResultadoOperacion> GuardarRutas(List<Rutas> rutas)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            using (var dbContextTransaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    foreach (var r in rutas)
                    {
                        result = await AgregarActualizarElemento(r);
                    }
                    result.Resultado = OperacionResultado.Ok;
                    dbContextTransaction.Commit();
                }
                catch (DbEntityValidationException ex)
                {
                    dbContextTransaction.Rollback();
                    result = GetResult(ex);
                }
            }
            return result;
        }

        #region Configuracion - Usuarios

        public async Task<UsuariosModel> ObtenerUsuarios(string filtro, string tipo, string instalacion, string tipoUsuario,  int pagina=1,int num=20)
        {
            
            ctx.Configuration.ProxyCreationEnabled = false;
            var predicate = PredicateBuilder.New<Usuarios>(true);
            //predicate = predicate.And(x => x.Activo == true);
            predicate = predicate.And(x => x.Activo == true);

            if (!string.IsNullOrEmpty(filtro) && !filtro.Trim().Equals(""))
            {
                predicate = predicate.And(x => x.Nombre.Contains(filtro));
            }


            if (tipoUsuario.ToLower() == "jefe")
            {
                predicate = predicate.And(x => x.Tipo != Usuarios.EnumUsuarioTipo.Jefe);
               
            }
            else if (tipoUsuario == "vendedor")
            {
                predicate = predicate.And(x => x.Tipo == Usuarios.EnumUsuarioTipo.Vendedor);
                
            }
            else if (tipoUsuario == "servicio")
            {
                predicate = predicate.And(x => x.Tipo == Usuarios.EnumUsuarioTipo.Servicio);
            }
            else if (tipoUsuario == "desarrollo")
            {
                predicate = predicate.And(x => x.Tipo == Usuarios.EnumUsuarioTipo.Desarrollo);
            }
            else if (tipoUsuario == "aplicaciones")
            {
                predicate = predicate.And(x => x.Tipo == Usuarios.EnumUsuarioTipo.Aplicaciones);
            }


            if (!string.IsNullOrEmpty(tipo))
            {
                var tipos = tipo.Split(',');
                IEnumerable<Usuarios.EnumUsuarioTipo> items = tipos.Select(a => (Usuarios.EnumUsuarioTipo)Enum.Parse(typeof(Usuarios.EnumUsuarioTipo), a));
                predicate = predicate.And(x => items.Any(z => z == x.Tipo));
            }


            if (!string.IsNullOrEmpty(instalacion))
            {
                var instalaciones = instalacion.Split(',');
                predicate = predicate.And(x => x.Proyectos.Any( y => instalaciones.Any( z => z == y.Id.ToString())));
            }

            var query = ctx.Usuarios.AsExpandable().Where(predicate).OrderByDescending(x => x.IdUsuario).AsNoTracking();
            
            int numSkip = (pagina - 1) * num;
            var usuarios = await query.ToListAsync();//await query.Skip(numSkip).Take(num).ToListAsync();
            var model = new UsuariosModel
            {
                Usuarios = usuarios, Pagina = pagina, HayMasRegistros = usuarios.Count == 20
            };

            return model;
        }

        #region Configuracion - Vistas-Permisos

        public List<Permisos> ObtenerPermisos()
        {
            return ctx.Permisos.ToList();
        }
        public List<Vistas> ObtenerVistas()
        {
            return ctx.Vistas.ToList();
        }

        public static string ObtenerDescriptionEnum(Enum value)
        {
            if (value != null)
            {
                var fi = value.GetType().GetField(value.ToString());
                var attributes = (DescriptionAttribute[])
                                 fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                return attributes.Length > 0 ? attributes[0].Description : value.ToString();
            }
            return "";
        }

        public async Task<UsuariosVistasPermisosModel> ObtenerUsuariosVistasPermisos(int idUsuario)
        {
            var model = new UsuariosVistasPermisosModel();
            ctx.Configuration.ProxyCreationEnabled = false;
            
           var Permisos = await ctx.Permisos.AsNoTracking().ToListAsync();
            Permisos.ForEach(x => x.Descripcion = ObtenerDescriptionEnum(x.Seccion));
            model.Permisos = Permisos.OrderBy(x => x.Descripcion).ThenBy(x => x.Nombre).ToList();
            model.Vistas = await ctx.Vistas.AsNoTracking().ToListAsync();
            model.Aplicaciones = await ctx.Aplicaciones.AsNoTracking().ToListAsync();
            model.Instalaciones = await ctx.Instalaciones.AsNoTracking().ToListAsync();
            model.Categorias = await ctx.ReportesCategorias.AsNoTracking().ToListAsync();
            

            model.Usuario = await ctx.Usuarios
                .Include(x=>x.Permisos)
                .Include(x => x.Vistas)
                .Include(x => x.Aplicaciones)
                .Include(x => x.ReportesCategorias)
                .Include(x => x.ReportesCategorias_Responsables)
                .Include(x => x.Proyectos)
                .AsNoTracking().FirstOrDefaultAsync(u=>u.IdUsuario==idUsuario);

            if (model.Usuario != null && model.Usuario.Permisos.Any())
            {
                foreach (var usuarioPermiso in model.Usuario.Permisos)
                {
                    usuarioPermiso.Usuarios = null;
                }
            }

            if (model.Usuario != null && model.Usuario.Vistas.Any())
            {
                foreach (var usuarioVistas in model.Usuario.Vistas)
                {
                    usuarioVistas.Usuarios = null;
                }
            }

            if (model.Usuario != null && model.Usuario.Aplicaciones.Any())
            {
                foreach (var usuarioAplicaciones in model.Usuario.Aplicaciones)
                {
                    usuarioAplicaciones.Usuarios = null;
                }
            }

            if (model.Usuario != null && model.Usuario.Proyectos.Any())
            {
                foreach (var usuarioIntalaciones in model.Usuario.Proyectos)
                {
                    usuarioIntalaciones.Usuarios = null;
                }
            }

            if (model.Usuario != null && model.Usuario.ReportesCategorias.Any())
            {
                foreach (var usuarioCategorias in model.Usuario.ReportesCategorias)
                {
                    usuarioCategorias.Usuarios = null;
                }
            }

            if (model.Usuario != null && model.Usuario.ReportesCategorias_Responsables.Any())
            {
                foreach (var usuarioCategoriasReponsables in model.Usuario.ReportesCategorias_Responsables)
                {
               
                    usuarioCategoriasReponsables.Usuarios = null;
                }
            }

            return model;
        }

        public async Task<ResultadoOperacion> AgregarActualizarVistasPermisos(int idUsuario, List<int> vistas, List<int> permisos, List<int> aplicaciones,
            List<int> instalaciones, List<int> categorias, List<int> responsables)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;

            using (var dbContextTransaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var usu = await ctx.Usuarios
                        .Include(x=>x.Vistas)
                        .Include(x => x.Permisos)
                        .Include(x => x.Aplicaciones)
                        .Include(x => x.Proyectos)
                        .Include(x => x.ReportesCategorias)
                        .Include(x => x.ReportesCategorias_Responsables)
                        .FirstOrDefaultAsync(x => x.IdUsuario == idUsuario);

                    if (usu != null)
                    {
                        if (vistas == null) vistas = new List<int>();
                        if (permisos == null) permisos = new List<int>();
                        if (aplicaciones == null) aplicaciones = new List<int>();
                        if (instalaciones == null) instalaciones = new List<int>();
                        if (categorias == null) categorias = new List<int>();
                        if (responsables == null) responsables = new List<int>();

                        var vistasEnBd = usu.Vistas.ToList();
                        var permisoEnBd = usu.Permisos.ToList();
                        var aplicacionesEnBd = usu.Aplicaciones.ToList();
                        var instalacionesEnBd = usu.Proyectos.ToList();
                        var categoriasEnBd = usu.ReportesCategorias.ToList();
                        var responsablesEnBd = usu.ReportesCategorias_Responsables.ToList();
                        //Eliminar las vistas que ya no estan
                        foreach (var v in vistasEnBd)
                            {
                                if (!vistas.Exists(x => x == v.IdVista))
                                {
                                    usu.Vistas.Remove(v);
                                }
                            }

                            //Agregar las vistas que ya no estan
                            foreach (var idv in vistas)
                            {
                                if (!vistasEnBd.Exists(x => x.IdVista == idv))
                                {
                                    var vista = await ctx.Vistas.FirstOrDefaultAsync(x => x.IdVista == idv);
                                    usu.Vistas.Add(vista);
                                }
                            }
                        
                            
                            //Eliminar los permisos que ya no estan
                            foreach (var p in permisoEnBd)
                            {
                                if (!permisos.Exists(x => x == p.IdPermiso))
                                {
                                    usu.Permisos.Remove(p);
                                }
                            }

                            //Agregar las vistas que ya no estan
                            foreach (var idp in permisos)
                            {
                                if (!permisoEnBd.Exists(x => x.IdPermiso == idp))
                                {
                                    var permiso = await ctx.Permisos.FirstOrDefaultAsync(x => x.IdPermiso == idp);
                                    usu.Permisos.Add(permiso);
                                }
                            }


                        //Eliminar las aplicaciones que ya no estan
                        foreach (var v in aplicacionesEnBd)
                        {
                            if (!aplicaciones.Exists(x => x == v.IdAplicacion))
                            {
                                usu.Aplicaciones.Remove(v);
                            }
                        }

                        //Agregar las aplicaciones  
                        foreach (var idv in aplicaciones)
                        {
                            if (!aplicacionesEnBd.Exists(x => x.IdAplicacion == idv))
                            {
                                var aplicacion = await ctx.Aplicaciones.FirstOrDefaultAsync(x => x.IdAplicacion == idv);
                                usu.Aplicaciones.Add(aplicacion);
                            }
                        }


                        //Eliminar las instalaciones que ya no estan
                        foreach (var v in instalacionesEnBd)
                        {
                            if (!instalaciones.Exists(x => x == v.Id))
                            {
                                usu.Proyectos.Remove(v);
                            }
                        }

                        //Agregar las instalaciones  
                        foreach (var idv in instalaciones)
                        {
                            if (!instalacionesEnBd.Exists(x => x.Id == idv))
                            {
                                var instalacion = await ctx.Instalaciones.FirstOrDefaultAsync(x => x.Id == idv);
                                usu.Proyectos.Add(instalacion);
                            }
                        }


                        //Eliminar las categorias que ya no estan
                        foreach (var v in categoriasEnBd)
                        {
                            if (!categorias.Exists(x => x == v.Id))
                            {
                                usu.ReportesCategorias.Remove(v);
                            }
                        }

                        //Agregar las categorias  
                        foreach (var idv in categorias)
                        {
                            if (!categoriasEnBd.Exists(x => x.Id == idv))
                            {
                                var categoria = await ctx.ReportesCategorias.FirstOrDefaultAsync(x => x.Id == idv);
                                usu.ReportesCategorias.Add(categoria);
                            }
                        }


                        //Eliminar las categorias que ya no estan
                        foreach (var v in responsablesEnBd)
                        {
                            if (!responsables.Exists(x => x == v.Id))
                            {
                                usu.ReportesCategorias_Responsables.Remove(v);
                            }
                        }

                        //Agregar las instalaciones  
                        foreach (var idv in responsables)
                        {
                            if (!responsablesEnBd.Exists(x => x.Id == idv))
                            {
                                var responsable = await ctx.ReportesCategorias.FirstOrDefaultAsync(x => x.Id == idv);
                                usu.ReportesCategorias_Responsables.Add(responsable);
                            }
                        }


                    }

                    await ctx.SaveChangesAsync();
                    dbContextTransaction.Commit();
                    result.Resultado = OperacionResultado.Ok;

                }
                catch (DbEntityValidationException ex)
                {
                    dbContextTransaction.Rollback();
                    result = GetResult(ex);
                }
            }
            return result;

        }

        public async Task<ResultadoOperacion> GuardarCategorias(int idInstalacion, int idCategoria, bool bSeleccion)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };

            ctx.Configuration.ProxyCreationEnabled = false;
            var instalaciones = await ctx.Instalaciones.Include(i => i.ReportesCategorias).FirstOrDefaultAsync(i=> i.Id == idInstalacion);

            using (var dbContextTransaction = ctx.Database.BeginTransaction())
            {
                //Eliminar las categorias que ya no estan
                if (!bSeleccion)
                {
                    if (instalaciones != null)
                    {
                        var itemCategoria = instalaciones.ReportesCategorias.FirstOrDefault(x => x.Id == idCategoria);
                        if (itemCategoria != null)
                            instalaciones.ReportesCategorias.Remove(itemCategoria);
                    }
                }
                else
                {
                    if (!instalaciones.ReportesCategorias.Exists(x => x.Id == idCategoria))
                    {
                        var categoria = await ctx.ReportesCategorias.FirstOrDefaultAsync(x => x.Id == idCategoria);
                        instalaciones.ReportesCategorias.Add(categoria);
                    }
                }
                await ctx.SaveChangesAsync();
                dbContextTransaction.Commit();
            }
            return result;
        }

        #endregion

        public async Task<Usuarios> ObtenerUsuario(int idUsuario)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var usu = await ctx.Usuarios.FirstOrDefaultAsync(x=>x.IdUsuario==idUsuario);
            return usu;
        }

        public async Task<ResultadoOperacion> AgregarActualizarUsuarios(Usuarios  u)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;

          
            var usuario = await ctx.Usuarios.FirstOrDefaultAsync(x => x.Nombre.ToLower() == u.Nombre.ToLower());
            if ((u.IdUsuario == 0 && usuario == null )|| u.IdUsuario != 0)
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {

                        result = await AgregarActualizarElemento(u);
                        dbContextTransaction.Commit();

                    }
                    catch (DbEntityValidationException ex)
                    {
                        dbContextTransaction.Rollback();
                        result = GetResult(ex);
                    }
                }
            }
            else
            {
                result.Mensaje = "Repetido";
            }

            return result;

        }

        public async Task<ResultadoOperacion> BorrarUsuario(int idUsuario)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;
            var usuario = await ctx.Usuarios.FirstOrDefaultAsync(x => x.IdUsuario == idUsuario);
            if (usuario != null)
            {
                usuario.Activo = false;
                await ctx.SaveChangesAsync();
                result.Resultado = OperacionResultado.Ok;
            }
            
            return result;

        }
        public List<Aplicaciones> ObtenerAplicaciones()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.Aplicaciones.ToList();
        }

        public List<Instalacion> ObtenerInstalaciones()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.Instalaciones.ToList();
        }

        public List<ReportesCategorias> ObtenerCategorias()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.ReportesCategorias.Include(c => c.Instalacion).ToList();
        }

        public List<Contactos> ObtenerContactosPorAplicacion(int idAplicacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var app = ctx.Aplicaciones.Include(x => x.Contactos).FirstOrDefault(x => x.IdAplicacion == idAplicacion);
            if (app != null && app.Contactos!= null)
            {
                return app.Contactos.ToList();
            }
            else
            {
                return new List<Contactos>();
            }
        }


        public async Task<Contactos> ObtenerContacto(int idContacto)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return await ctx.Contactos.Include(x => x.Aplicaciones).FirstOrDefaultAsync(x => x.IdContacto == idContacto);
            
        }


        public async Task<ResultadoOperacion> AgregarActualizarContactos(Contactos contactos)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
           
            
            var con = ctx.Contactos.Include(x => x.Aplicaciones).FirstOrDefault(x => x.IdContacto == contactos.IdContacto);
            if (con == null)
            {
                con = new Contactos();
                Objetos.CopiarObjeto(contactos, con);
                AgregarAplicacionesContacto(con, contactos.Aplicaciones);
                ctx.Contactos.Add(con);
            }
            else
            {
                Objetos.CopiarObjeto(contactos, con);
                AgregarAplicacionesContacto(con, contactos.Aplicaciones);
            }
            
            await ctx.SaveChangesAsync();
            result.Resultado = OperacionResultado.Ok;
            result.Id = con.IdContacto;
            
            return result;

        }

        private ResultadoOperacion AgregarAplicacionesContacto(Contactos contacto, ICollection<Aplicaciones> aplicaciones)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            if (aplicaciones != null && contacto != null && contacto.Aplicaciones != null)
            {

                contacto.Aplicaciones.Clear();

                foreach (var aplicacion in aplicaciones)
                {

                    var a = ctx.Aplicaciones.FirstOrDefault(x => x.IdAplicacion == aplicacion.IdAplicacion);
                    contacto.Aplicaciones.Add(a);
                    //var seccionDb = ctx.Secciones.FirstOrDefault(e => e.IdSeccion == seccion.IdSeccion);

                    //if (seccionDb != null)
                    //    triage.Secciones_Multiples.Add(seccionDb);
                }

                ctx.SaveChanges();
            }

            result.Resultado = OperacionResultado.Ok;

            return result;
        }
        public async Task<ResultadoOperacion> BorrarContactos(int idContacto, int idAplicacion)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ctx.Configuration.ProxyCreationEnabled = false;
            var app = ctx.Aplicaciones.Include(x => x.Contactos).FirstOrDefault(x => x.IdAplicacion == idAplicacion);
            if (app != null)
            {
                var con = app.Contactos.FirstOrDefault(x => x.IdContacto == idContacto);
                if (con != null)
                {
                    app.Contactos.Remove(con);
                }

                await ctx.SaveChangesAsync();
                result.Resultado = OperacionResultado.Ok;
            }


            return result;

        }
        #endregion

        #region ".  Configuracion - Correo"

        public ConfiguracionMail ObtenerConfiguracionMail()
        {
            return ctx.ConfiguracionMail.FirstOrDefault();
        }
        #endregion
    }
}
