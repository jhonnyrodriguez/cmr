﻿using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using IDigitales.Pagina.Web.Data.Utils;
using System.Diagnostics;
using Newtonsoft.Json;

namespace IDigitales.Pagina.Web.Data.Consultas
{
    public class ConsultasInstalaciones : ConsultasBase
    {
        #region ".  Instalaciones"

        public List<Instalacion> InstalacionesRecupera()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            //RevisarEstadoPendiente();
            var l = ctx.Instalaciones
                .Include(i => i.Reportes.Select(u => u.QuienReporta))
                .Include(i => i.Reportes.Select(u => u.Modulo).Select(u => u.Aplicaciones))
                .Include(i => i.Reportes.Select(u => u.VistoBueno))
                .Include(i => i.Reportes.Select(u => u.Comentarios))
                .Include(i => i.ReportesCategorias)
                .Include(i => i.Modulo)
                .Include(i => i.Usuarios).ToList()
                ;

            return l.Where(i => i.Activo).ToList();
        }

        public List<Instalacion> InstalacionesRecupera(int userId)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            //RevisarEstadoPendiente();
            var l = ctx.Instalaciones
                .Include(i => i.Reportes.Select(u => u.QuienReporta))
                .Include(i => i.Reportes.Select(u => u.Modulo).Select(u => u.Aplicaciones))
                .Include(i => i.Reportes.Select(u => u.VistoBueno))
                .Include(i => i.Reportes.Select(u => u.Comentarios))
                .Include(i => i.ReportesCategorias)
                .Include(i => i.Modulo)
                .Include(i => i.Usuarios).ToList()
                ;

            return l.Where(i => i.Activo && i.Usuarios.FirstOrDefault(u => u.IdUsuario == userId) != null).ToList();
        }

        public Instalacion ObtenerInstalacion(int id)
        {
            return ctx.Instalaciones.Include(i => i.Reportes.Select(u => u.QuienReporta))
                .Include(i => i.Reportes.Select(u => u.Modulo).Select(u => u.Aplicaciones))
                .Include(i => i.Reportes.Select(u => u.VistoBueno))
                .Include(i => i.ReportesCategorias)
                .Include(i => i.Modulo)
                .Include(i => i.Reportes.Select(u => u.Comentarios)).FirstOrDefault(o => o.Id == id && o.Activo);
        }

        public ResultadoOperacion CrearInstalacion(Instalacion instalacion, List<int> userIds)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Instalaciones.Add(instalacion);
                foreach(int id in userIds)
                {
                    var user = ctx.Usuarios.FirstOrDefault(u => u.IdUsuario == id);
                    nuevo.Usuarios.Add(user);
                }
                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "El formato del texto no es válido";
            }

            return resultado;
        }

        public ResultadoOperacion ActualizarInstalacion(Instalacion instalacion)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Instalaciones.FirstOrDefault(f => f.Id == instalacion.Id);
                nuevo.Activo = instalacion.Activo;
                nuevo.Descripcion = instalacion.Descripcion;
                nuevo.Tag = instalacion.Tag;

                ctx.SaveChanges();
                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "El formato del texto no es válido";
            }

            return resultado;
        }

        public ResultadoOperacion AgregarQuitarModuloInstalacion(int idInstalacion, int idModulo)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Instalaciones.FirstOrDefault(f => f.Id == idInstalacion);

                var modulo = nuevo.Modulo.FirstOrDefault(m => m.Id == idModulo);
                if(modulo == null)
                {
                    modulo = ctx.Modulos.FirstOrDefault(m => m.Id == idModulo);
                    nuevo.Modulo.Add(modulo);
                }
                else
                {
                    nuevo.Modulo.Remove(modulo);
                }

                ctx.SaveChanges();
                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Error al procesar la información";
            }

            return resultado;
        }

        #endregion

        #region ".  Importación"

        public ResultadoOperacion CrearReporteImportado(ReporteImportado importado)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.ReportesImportados.Add(importado);
                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public ResultadoOperacion ActualizarReporteImportado(ReporteImportado importado)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.ReportesImportados.FirstOrDefault(f => f.Id == importado.Id);

                nuevo.Estado = importado.Estado;
                nuevo.FechaImportacion = importado.FechaImportacion;
                nuevo.LineasSobrantesCount = importado.LineasSobrantesCount;
                nuevo.Nombre = importado.Nombre;
                nuevo.ReporteImportadoFuenteId = importado.ReporteImportadoFuenteId;
                nuevo.RutaOriginal = importado.RutaOriginal;
                nuevo.ReportesImportadosDict = importado.ReportesImportadosDict;

                ctx.SaveChanges();
                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        #endregion

        #region ".  Reportes"

        public ResultadoOperacion CrearReporte(Reporte reporte)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Reportes.Add(reporte);
                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.ToString();
            }

            return resultado;
        }

        public ResultadoOperacion EliminarReporte(int id)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var r = ctx.Reportes.FirstOrDefault(i => i.Id == id);
                if(r != null)
                {
                    ctx.Reportes.Remove(r);
                    ctx.SaveChanges();
                }                

                resultado.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.ToString();
            }

            return resultado;            
        }

        public List<Reporte> ObtenerReportes()
        {
            var l = ctx.Reportes.Include(i => i.QuienReporta).
                Include(i => i.Instalacion).
                Include(i => i.Comentarios).
                Include(i => i.Modulo).
                Include(i => i.Modulo.Aplicaciones).
                Include(i => i.DesarrolladorAsignado).
                Include(i => i.Instalacion.Usuarios);

            return l.Where(i => i.Instalacion.Activo).ToList();
        }

        public List<Reporte> ObtenerReportes(int userId)
        {
            var l = ctx.Reportes.Include(i => i.QuienReporta).
                Include(i => i.Instalacion).
                Include(i => i.Comentarios).
                Include(i => i.Modulo).
                Include(i => i.Modulo.Aplicaciones).
                Include(i => i.DesarrolladorAsignado).
                Include(i => i.Instalacion.Usuarios);

            return l.Where(i => i.Instalacion.Activo && i.Instalacion.Usuarios.FirstOrDefault(u => u.IdUsuario == userId) != null).ToList();
        }

        public List<ReporteImportado> ObtenerReportesImportados()
        {
            return ctx.ReportesImportados.Include(i => i.Instalacion).ToList();
        }

        //public void RevisarEstadoPendiente()
        //{
        //    try
        //    {
        //        var reportes = ctx.Reportes.Include(i => i.Comentarios).ToList();
        //        foreach (var reporte in reportes)
        //        {
        //            if (EsEstadoPendiente(reporte))
        //            {
        //                reporte.Comentarios.Add(new Comentario(reporte.Id));
        //                reporte.Estado = Reporte.EstadoEnum.Pendiente;
        //                ctx.SaveChanges();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.WriteNewEntry(ex, EventLogEntryType.Error);
        //    }
        //}

        //bool EsEstadoPendiente(Reporte reporte)
        //{
        //    var ts = ObtenerConfiguracion().InactividadTimeSpan;
        //    if (reporte.Estado == Reporte.EstadoEnum.Reportado)
        //    {
        //        var ultimosCambios = reporte.Comentarios.Where(c => c.NuevoEstado == Reporte.EstadoEnum.Reportado);
        //        if (ultimosCambios.Count() > 1)
        //        {
        //            var ultimoCambio = ultimosCambios.OrderByDescending(c => c.FechaCreacion).FirstOrDefault();
        //            if (DateTime.Now - ultimoCambio.FechaCreacion > ts)
        //            {
        //                return true;
        //            }
        //        }
        //        else if (DateTime.Now - reporte.FechaReporte > ts)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        public ResultadoOperacion ActualizarReporte(Reporte reporte)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Reportes.FirstOrDefault(f => f.Id == reporte.Id);
                nuevo.AsignadoId = reporte.AsignadoId;
                nuevo.Descripcion = reporte.Descripcion;

                nuevo.Estado = reporte.Estado;
                nuevo.FechaSolucion = reporte.FechaSolucion;
                
                ctx.SaveChanges();
                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public ResultadoOperacion ActualizarReporteVistoBueno(Reporte reporte)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Reportes.FirstOrDefault(f => f.Id == reporte.Id);                
                nuevo.VistoBuenoId = reporte.VistoBuenoId;

                ctx.SaveChanges();
                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public Reporte ObtenerReporte(int id)
        {
            var r = ctx.Reportes.
                Include(i => i.Instalacion).
                Include(i => i.Instalacion.Usuarios).
                Include(i => i.Comentarios.Select(u => u.Usuario)).
                Include(i => i.ReportesCategorias).
                Include(i => i.QuienReporta).
                Include(i => i.ReportesCategorias.Usuarios).
                Include(i => i.ReportesCategorias.Usuarios_Responsables).
                Include(i => i.Modulo.Aplicaciones.AplicacionesVersiones).
                Include(i => i.DesarrolladorAsignado).
                Include(i => i.Comentarios.Select(u => u.Evidencia)).ToList();
            return r.FirstOrDefault(o => o.Id == id);
        }

        public Comentario ObtenerComentario(int id)
        {
            var r = ctx.Comentarios.Include(i => i.Usuario).Include(i => i.Evidencia).ToList();
            return r.FirstOrDefault(o => o.Id == id);
        }

        public ResultadoOperacion CrearComentario(Comentario comentario)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Comentarios.Add(comentario);
                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public ResultadoOperacion EliminarComentario(int id)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var r = ctx.Comentarios.FirstOrDefault(i => i.Id == id);
                if(r != null)
                {
                    ctx.Comentarios.Remove(r);
                    ctx.SaveChanges();
                }                

                resultado.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.ToString();
            }

            return resultado;
        }

        public ResultadoOperacion ActualizarComentario(Comentario comentario)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Comentarios.FirstOrDefault(f => f.Id == comentario.Id);
                nuevo.Contenido = comentario.Contenido;
                nuevo.NuevoEstado = comentario.NuevoEstado;
                nuevo.ArchivoRutaRelativa = comentario.ArchivoRutaRelativa;

                ctx.SaveChanges();
                resultado.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public List<Modulo> ObtenerModulos()
        {
            return ctx.Modulos.Include(m => m.Instalacion).Include(i => i.Aplicaciones).OrderBy(m => m.Subcategoria).ToList();
        }

        public List<ReportesCategorias> ObtenerReportesCategorias()
        {
            return ctx.ReportesCategorias.ToList();
        }

        public ReportesCategorias ObtenerReportesCategoria(int id)
        {
            return ctx.ReportesCategorias.FirstOrDefault(m => m.Id == id);
        }

        public List<Aplicaciones> ObtenerCategorias()
        {
            return ctx.Aplicaciones.Include(ctx => ctx.Modulos.Select(s => s.Instalacion)).ToList();
        }

        public Aplicaciones ObtenerCategoria(int id)
        {
            return ctx.Aplicaciones.Include(ctx => ctx.Modulos.Select(s => s.Instalacion)).FirstOrDefault(m => m.IdAplicacion == id);
        }

        public Modulo ObtenerModulo(int id)
        {
            return ctx.Modulos.Include(x => x.Aplicaciones).FirstOrDefault(m => m.Id == id);
        }

        public ResultadoOperacion CrearModulo(Modulo modulo)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Modulos.Add(modulo);
                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.Id;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }
        public ResultadoOperacion CrearCategoria(Aplicaciones categoria)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Aplicaciones.Add(categoria);
                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.IdAplicacion;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public ResultadoOperacion ActualizarModulo(Modulo modulo)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Modulos.FirstOrDefault(f => f.Id == modulo.Id);
                //nuevo.Categoria = modulo.Categoria;
                nuevo.Subcategoria = modulo.Subcategoria;
                nuevo.Aliases = modulo.Aliases;

                ctx.SaveChanges();
                resultado.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public ResultadoOperacion ActualizarCategoria(Aplicaciones categoria)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Aplicaciones.FirstOrDefault(f => f.IdAplicacion == categoria.IdAplicacion);
                nuevo.TAG = categoria.TAG;
                nuevo.Nombre = categoria.Nombre;
                //nuevo.Version = categoria.Version;

                ctx.SaveChanges();
                resultado.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public Modulo ObtenerModuloPorSubcategoria(string subcategoria)
        {
            if (subcategoria.Contains(','))
            {
                var partes = subcategoria.Split(',');
                Dictionary<int, Modulo> count = new Dictionary<int, Modulo>();
                                
                for (int i = 0; i < partes.Count(); i++)
                {
                    string literal = partes[i].ToLower();
                    var modulo = ctx.Modulos.Where(m => m.Subcategoria.Contains(",")).FirstOrDefault(m => m.Subcategoria.ToLower().Contains(literal));
                    if (modulo != null)
                    {
                        count.Add(i, modulo);
                    }
                }
                if(count.Count >= partes.Count())
                {
                    bool correct = true;
                    var comparer = count.First();
                    foreach(var modulo in count)
                    {
                        if(comparer.Value.Id != modulo.Value.Id)
                        {
                            correct = false;
                        }
                    }
                    if (correct)
                    {
                        return comparer.Value;
                    }
                }
            }
            else
            {
                var modulo = ctx.Modulos.FirstOrDefault(m => m.Subcategoria.Contains(subcategoria));
               if(modulo == null)
                {
                    modulo = ctx.Modulos.FirstOrDefault(m => m.Aliases.Contains(subcategoria));
                }
                return modulo;
            }
            return null;
        }

        #endregion

        #region ".  Configuracion"

        public ConfiguracionInstalaciones ObtenerConfiguracion()
        {
            var conf = ctx.ConfiguracionInstalaciones.FirstOrDefault();
            if (conf == null)
            {
                ctx.ConfiguracionInstalaciones.Add(new ConfiguracionInstalaciones { InactividadTimeSpan = TimeSpan.FromDays(30), ProcesoExedenceTimeSpan = TimeSpan.FromDays(30),
                    RutaEvidencias = "D:\\SistemaReportes\\Evidencias\\",
                    RutaReportesImportados = "D:\\SistemaReportes\\ReportesImportados\\"
                });
                ctx.SaveChanges();

                conf = ctx.ConfiguracionInstalaciones.FirstOrDefault();
            }

            return conf;
        }

        public ResultadoOperacion EditarConfiguracion(ConfiguracionInstalaciones configuracion)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                resultado.Resultado = OperacionResultado.Error;

                var conf = ObtenerConfiguracion();
                if (conf == null)
                {
                    resultado.Mensaje = "No existe una configuración actual.";
                }
                else
                {
                    conf.InactividadTimeSpan = configuracion.InactividadTimeSpan;
                    conf.ProcesoExedenceTimeSpan = configuracion.ProcesoExedenceTimeSpan;
                    conf.RutaEvidencias = configuracion.RutaEvidencias;
                    conf.RutaReportesImportados = configuracion.RutaReportesImportados;
                    ctx.SaveChanges();
                    resultado.Resultado = OperacionResultado.Ok;
                }
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        #endregion

        #region Gantt

        public async Task<GridResult<Reporte>> ObtenerReportesProyecto(int idProyecto)
        {
            GridResult<Reporte> data = null;
           
            try
            {
                ctx.Configuration.ProxyCreationEnabled = false;
                var reportes = await ctx.Reportes
                 //.Include(i => i.ReportePredecesor)
                 .Include(i => i.ReporteSucesores)
                 .Include(i => i.DesarrolladorAsignado)
                 .Where(i => i.InstalacionId == idProyecto).ToListAsync();

                data = new GridResult<Reporte>(reportes);
            }
            catch (Exception ex)
            {
                data = new GridResult<Reporte>(ex);
            }
            return data;
        }

        public async Task<ResultadoOperacion> ActualizarReporteProyecto(int idReporte, string valores)
        {
            var result = new ResultadoOperacion { Resultado = OperacionResultado.Error };
            try
            {
                ctx.Configuration.ProxyCreationEnabled = false;
                var ele = await ctx.Reportes
                    .Include(i => i.ReporteSucesores)
                 .Include(i => i.DesarrolladorAsignado)
                    .FirstAsync(l => l.Id == idReporte);

                if (ele != null) //ya existe
                {
                    JsonConvert.PopulateObject(valores, ele);
                    await ctx.SaveChangesAsync();

                    result.Id = ele.Id;
                    result.Resultado = OperacionResultado.Ok;
                    result.Elemento = HIS.Web.Data.Utils.Objetos.QuitarReferenciaCircular<Reporte>(ele, new List<string> { "ReporteSucesores", "DesarrolladorAsignado" });
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                result = GetResult(ex);
            }
            return result;
        }
        #endregion
    }
}
