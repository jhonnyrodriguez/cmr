﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasSAE 
    {
        #region ". Variables "

        private string _connectionString;
      
        #endregion

        #region ". Constructores "

        public ConsultasSAE()
        {
            try
            {
                _connectionString = ObtenerCadenaConexion();
            }
            catch (Exception ex)
            {
                IDigitales.Comunes.Librerias.IO.Bitacora.EscribeLineaError("ConsultasSQLBase", "ConsultasSQLBase", ex);
            }
        }
        #endregion
        
        #region ". Metodos"
        private string ObtenerCadenaConexion()
        {
            var connString = @"server=.\SQLEXPRESS2012;database=SAE;uid=sa;pwd=database;";
            try
            {
                connString = ConfigurationManager.ConnectionStrings["SAE"].ConnectionString;
            }
            catch (Exception ex)
            {
            }
            return connString;
        }
        public DataTable ObtenerProductos()
        {
            DataTable tabla = new DataTable();
            
            try
            {
                string comandoSQL = "SELECT  CVE_ART, EXIST FROM INVE01";
                          
                if (comandoSQL != string.Empty)
                {
                    using (SqlConnection con = new SqlConnection(_connectionString))
                    {
                        con.Open(); 
                        var cmd = new SqlCommand(comandoSQL, con);
                        var da = new SqlDataAdapter(cmd);
                        da.Fill(tabla);
                        con.Close();
                    } 
                }
            }
            catch (Exception ex)
            {
                IDigitales.Comunes.Librerias.IO.Bitacora.EscribeLineaError("ConsultasSAE", "ObtenerProductos", ex);
            }
            return tabla;
        }

        public DataTable ObtenerInfoAlmacenProductos()
        {
            DataTable tabla = new DataTable();

            try
            {
                string comandoSQL = "SELECT CVE_ART, STOCK_MIN, STOCK_MAX FROM MULT01 WHERE CVE_ALM =1";

                if (comandoSQL != string.Empty)
                {
                    using (SqlConnection con = new SqlConnection(_connectionString))
                    {
                        con.Open();
                        var cmd = new SqlCommand(comandoSQL, con);
                        var da = new SqlDataAdapter(cmd);
                        da.Fill(tabla);
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                IDigitales.Comunes.Librerias.IO.Bitacora.EscribeLineaError("ConsultasSAE", "ObtenerInfoAlmacenProductos", ex);
            }
            return tabla;
        }
        #endregion

    }
}
