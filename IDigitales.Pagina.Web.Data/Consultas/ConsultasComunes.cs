﻿using System;
using System.Collections.Generic;
using System.Linq;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Comun.Enums;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using System.Data.Entity;
using LinqKit;
using System.Configuration;


namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasComunes : ConsultasBase
    {
        #region Generales
        public string ObtenerImagenDocumento(int idDocumento)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ruta = "";

            var doc = ctx.Documentos.FirstOrDefault(x => x.IdDocumento == idDocumento);
            if (doc != null)
            {
                ruta = doc.Ruta;
            }
            return ruta;
        }
        public Documentos ObtenerDocumento(int idDocumento)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var doc = ctx.Documentos.FirstOrDefault(x => x.IdDocumento==idDocumento);
            return doc;

        }
        #endregion

        #region "Documentos"
        public string ObtenerRutaDocumentos()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ruta = ctx.Rutas.FirstOrDefault(t => t.Tipo == Rutas.EnumRutaTipo.Documentos);
            return ruta != null ? ruta.Directorio : string.Empty;
        }
        public string ObtenerRutaTemporales()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var ruta = ctx.Rutas.FirstOrDefault(t => t.Tipo == Rutas.EnumRutaTipo.Temporales);
            return ruta != null ? ruta.Directorio : string.Empty;
        }
        public List<Documentos> GuardarDocumentosBD(List<Documentos> docs)
        {
            ctx.Documentos.AddRange(docs);
            ctx.SaveChanges();                     

            return docs;
        }
        public DateTime ObtenerFechaMaxima()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var Max = ctx.Documentos.Max(x => x.Fecha);
            return Max ?? DateTime.Today;
        }
        public DateTime ObtenerFechaMinima()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var Min = ctx.Documentos.Min(x => x.Fecha);
            return Min ?? DateTime.Today;
        }

        public List<Documentos> ObtenerDocumentosPorAnio(int Anio)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var Docs = ctx.Documentos.Where(x => x.Fecha.Value.Year == Anio).ToList();
            return Docs;
        }
        public string EliminarDocumentoBD(int IdDocumento)
        {
            var ruta = "";
            var doc = ctx.Documentos.FirstOrDefault(x => x.IdDocumento == IdDocumento);
            if(doc != null)
            {
                ruta = doc.Ruta;
                ctx.Documentos.Remove(doc);
                ctx.SaveChanges();
            }           

            return ruta;
        }
        #endregion

    }
}
