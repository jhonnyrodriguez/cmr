﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using static IDigitales.Pagina.Web.Data.Entidades.Poco.UsuariosNotificationsConfiguration;

namespace IDigitales.Pagina.Web.Data
{
    public class ConsultasUsuarios : ConsultasBase
    {
        public ResultadoOperacion CrearUsuario(Usuarios usuario, List<int> proyectos)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                usuario.UsuariosNotificationsConfiguration = new UsuariosNotificationsConfiguration();
                var nuevo = ctx.Usuarios.Add(usuario);
                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.IdUsuario;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public ResultadoOperacion AddProjectToUser(int idUser, int idProyecto)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var nuevo = ctx.Usuarios.FirstOrDefault(u => u.IdUsuario == idUser);
                bool existe = nuevo.Proyectos.FirstOrDefault(p => p.Id == idProyecto) != null;

                if (!existe)
                {
                    nuevo.Proyectos.Add(ctx.Instalaciones.FirstOrDefault(i => i.Id == idProyecto));
                    ctx.SaveChanges();
                }                

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = nuevo.IdUsuario;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public List<Usuarios> UsuariosRecupera()
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.Usuarios.
                Include(u => u.ReportesAsignados).
                Include(u => u.ReportesAsignados.Select(s => s.Comentarios)).
                Include(u => u.ReportesAsignados.Select(s => s.Instalacion)).
                Include(u => u.ReportesAsignados.Select(s => s.Modulo)).
                Include(u => u.ReportesAsignados.Select(s => s.Modulo.Aplicaciones)).
                Include(u => u.ReportesAsignados.Select(s => s.ReportesCategorias)).
                Include(u => u.Permisos).
                Include(u => u.UsuariosNotificationsConfiguration).
                Include(u => u.Proyectos).
                Include(u => u.ReportesCategorias).
                ToList();

        }

        public List<Usuarios> UsuariosRecupera(Reporte reporte)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx.Usuarios.
                Include(u => u.ReportesAsignados).
                Include(u => u.ReportesAsignados.Select(s => s.Comentarios)).
                Include(u => u.ReportesAsignados.Select(s => s.Instalacion)).
                Include(u => u.ReportesAsignados.Select(s => s.Modulo)).
                Include(u => u.ReportesAsignados.Select(s => s.Modulo.Aplicaciones)).
                Include(u => u.ReportesAsignados.Select(s => s.ReportesCategorias)).
                Include(u => u.Permisos).
                Include(u => u.UsuariosNotificationsConfiguration).
                Include(u => u.Proyectos).
                Include(u => u.ReportesCategorias).
                ToList();
         
        }

        public string UsuariosRecuperaEmail_All(Reporte reporte, NotificationsEnum tipoNotificacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var destinatariosBcc = String.Empty;


           var administradores = ctx.Usuarios.Where(y => y.Tipo == Usuarios.EnumUsuarioTipo.Administrador &&
                                                     y.UsuariosNotificationsConfiguration.NotificationConfiguration.HasFlag(tipoNotificacion) &&
                                                     !String.IsNullOrEmpty(y.Email))
                                                     .Select(x => x.Email).ToList();

            if (administradores != null && administradores.Count>0)
                destinatariosBcc = String.Join(";", administradores);

             var jefes = ctx.Usuarios.Where(y => y.Tipo != Usuarios.EnumUsuarioTipo.Administrador &&
                                           y.ReportesCategorias_Responsables.Any(c => c.Id == reporte.CategoriaId) &&
                                           y.UsuariosNotificationsConfiguration.NotificationConfiguration.HasFlag(tipoNotificacion) &&
                                           !String.IsNullOrEmpty(y.Email))
                                          .Select(x => x.Email).ToList();

            destinatariosBcc += String.Join(";", jefes);

            var cliente = ctx.Usuarios.FirstOrDefault(y => y.Tipo != Usuarios.EnumUsuarioTipo.Administrador &&
                                                  y.IdUsuario == reporte.QuienReportaId &&
                                                  !String.IsNullOrEmpty(y.Email) &&
                                                  y.UsuariosNotificationsConfiguration.NotificationConfiguration.HasFlag(tipoNotificacion));
            
            if (cliente != null)
                destinatariosBcc += cliente.Email;

            var asignado = ctx.Usuarios.Where(y => y.Tipo != Usuarios.EnumUsuarioTipo.Administrador &&
                                              y.IdUsuario == reporte.AsignadoId &&
                                              !String.IsNullOrEmpty(y.Email) &&
                                              y.UsuariosNotificationsConfiguration.NotificationConfiguration.HasFlag(tipoNotificacion))
                                           .Select(x => x.Email).ToList();

            destinatariosBcc += String.Join(";", asignado);

            return destinatariosBcc;
        }

        public string UsuariosRecuperaEmailJefes(Reporte reporte, NotificationsEnum tipoNotificacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var destinatariosBcc = String.Empty;


            var administradores = ctx.Usuarios.Where(y => y.Tipo == Usuarios.EnumUsuarioTipo.Administrador &&
                                                      y.UsuariosNotificationsConfiguration.NotificationConfiguration.HasFlag(tipoNotificacion) &&
                                                      !String.IsNullOrEmpty(y.Email))
                                                      .Select(x => x.Email).ToList();

            if (administradores != null && administradores.Count > 0)
                destinatariosBcc = String.Join(";", administradores);

            return destinatariosBcc;
        }

        public string UsuariosRecuperaEmailAsignado(Reporte reporte, NotificationsEnum tipoNotificacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var destinatariosBcc = String.Empty;


            var personal = ctx.Usuarios.Where(y => y.Tipo != Usuarios.EnumUsuarioTipo.Administrador &&
                                            y.Tipo != Usuarios.EnumUsuarioTipo.Cliente &&
                                          (y.ReportesCategorias_Responsables.Any(c => c.Id == reporte.CategoriaId) || //Jefe responsable
                                          y.IdUsuario == reporte.AsignadoId ||  //Desarrollador/servicio asignado
                                          y.IdUsuario == reporte.QuienReportaId)    //Quien reporta
                                          &&
                                          y.UsuariosNotificationsConfiguration.NotificationConfiguration.HasFlag(tipoNotificacion) &&
                                          !String.IsNullOrEmpty(y.Email))
                                         .Select(x => x.Email).ToList();
            if (personal != null && personal.Count > 0)
                destinatariosBcc += String.Join(";", personal);

            return destinatariosBcc;
        }

        public string UsuariosRecuperaEmailCliente(Reporte reporte, NotificationsEnum tipoNotificacion)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var destinatariosBcc = String.Empty;
            
            var jefes = ctx.Usuarios.Where(y => y.Tipo == Usuarios.EnumUsuarioTipo.Cliente &&
                                          y.Proyectos.Any(c => c.Id == reporte.InstalacionId) &&
                                          y.UsuariosNotificationsConfiguration.NotificationConfiguration.HasFlag(tipoNotificacion) &&
                                          !String.IsNullOrEmpty(y.Email))
                                         .Select(x => x.Email).ToList();
            if (jefes != null && jefes.Count > 0)
                destinatariosBcc += String.Join(";", jefes);

            return destinatariosBcc;
        }


        public Usuarios ValidarUsuario( string user, string pass)
        {
            
            ctx.Configuration.ProxyCreationEnabled = false;
            var validado = ctx.Usuarios
                .FirstOrDefault(x => x.Nombre == user && 
                                     x.Password == pass && 
                                     x.Activo==true);

            return validado;

        }

        public ResultadoOperacion CambiarEmail(Usuarios user, string email)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                user.Email = email;
                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = user.IdUsuario;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public ResultadoOperacion EditarConfiguracionNotificaciones(Usuarios user, UsuariosNotificationsConfiguration.NotificationsEnum e)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                var conf = ctx.UsuariosNotificationsConfigurations.FirstOrDefault(n => n.Id == user.UsuariosNotificationsConfigurationId);
                if (conf != null)
                {
                    conf.NotificationConfiguration = e;
                }
                else
                {
                    var newConf = new UsuariosNotificationsConfiguration
                    {
                        UsuariosId = user.IdUsuario,
                        Usuarios = user,
                        NotificationConfiguration = e,                        
                    };
                    ctx.UsuariosNotificationsConfigurations.Add(newConf);
                }

                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = user.IdUsuario;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public ResultadoOperacion QuitarAgregarProyecto(Usuarios user, int idProyecto)
        {
            var resultado = new ResultadoOperacion();

            try
            {
                if(user.Proyectos.FirstOrDefault(p => p.Id == idProyecto) != null)
                {
                    //el proyecto existe en la lista
                    user.Proyectos.Remove(user.Proyectos.FirstOrDefault(p => p.Id == idProyecto));
                }
                else
                {
                    //el proyecto no existe
                    user.Proyectos.Add(ctx.Instalaciones.FirstOrDefault(i => i.Id == idProyecto));
                }

                ctx.SaveChanges();

                resultado.Resultado = OperacionResultado.Ok;
                resultado.Id = user.IdUsuario;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }
        
        public List<string> ObtenerPermisos(int idUsuario)
        {
            ctx.Configuration.ProxyCreationEnabled = false;
            var usuario = ctx.Usuarios
                .Include(x => x.Permisos)
                .Include(x => x.Vistas)
                .FirstOrDefault(x => x.IdUsuario == idUsuario);

            if (usuario != null)
            {
                var permisos = usuario.Permisos.Select(x => x.Clave).ToList();
                permisos.AddRange(usuario.Vistas.Select(x => x.Clave).ToList());
                return permisos;
            }
            return new List<string>();
        }
    }
}
