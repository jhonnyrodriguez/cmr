﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.Comunes.Librerias.IO;
using IDigitales.Pagina.Web.Data.Utils;
using Rebex.Mail;
using Rebex.Net;

namespace IDigitales.Pagina.Web.Controllers.Helpers
{
    public class ManejadorCorreos
    {


        public static bool EnviaCorreo(string destinatarios, string asunto, string mensaje, string mensajeHtml)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyText = mensaje;
                mail.BodyHtml = mensajeHtml;
                mail.Subject = asunto;
                mail.To = destinatarios;
                return OnEnviaCorreo(mail);


            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return false;
        }

        private static bool OnEnviaCorreo(MailMessage mensaje)
        {
            try
            {
                var password = "ct2016Mx@";
                var server = "smtp.exchangeadministrado.com";
                var port = 587;
                var sender = "contacto@cmr3.com.mx";
                mensaje.From = sender;
                using (var smtp = new Rebex.Net.Smtp())
                {
                    smtp.Connect(server, port);
                    // authenticate with your email address and password
                    smtp.Login(sender, password, SmtpAuthentication.Login);

                    // send mail
                    smtp.Send(mensaje);

                    // disconnect (not required, but polite)
                    smtp.Disconnect();

                    return true;
                }
            }
            catch (Exception ex)
            {
                Bitacora.EscribeLineaError("MSJTrabajo.cs", "SendMailPrueba", ex);
                return false;
            }
        }
    }
}
