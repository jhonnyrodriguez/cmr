﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Controllers.Helpers
{
  
        public class CustomPrincipal : IPrincipal
        {
            public int Id { get; set; }
            public string Nombre { get; set; }
            public string Tipo { get; set; }
        public List<string> Permisos { get; set; }

        public CustomPrincipal(IIdentity identity)
            {
                Identity = identity;
            }

            public bool IsInRole(string role)
            {
                return Permisos.Contains(role);
                //if (String.Compare(role, "admin", true) == 0)
                //{
                //    return (Identity.Name == "JustinEtheredge");
                //}
                //else
                //{
                //    return false;
                //}
            }

            public IIdentity Identity
            {
                get; private set;
            }

        public bool TienePermiso(string clave)
        {
            if (Permisos.Contains(clave))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    public class CustomPrincipalSerializeModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public List<string> Permisos { get; set; }
    }

}
