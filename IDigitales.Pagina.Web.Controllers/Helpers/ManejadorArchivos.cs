﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Utils;

namespace IDigitales.Pagina.Web.Controllers.Helpers
{
    public class ManejadorArchivos
    {

        
        public static ResultadoOperacion RemoverArchivo(string ruta)
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ConsultasVersiones consulta = null;
            try
            {

                if (System.IO.File.Exists(ruta))
                    System.IO.File.Delete(ruta);

                //Elimina el folder padre
                var carpeta = Path.GetDirectoryName(ruta);
                if (Directory.Exists(carpeta) && !Directory.EnumerateFiles(carpeta).Any())
                {
                    Directory.Delete(carpeta);
                }

                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return result;
        }


  
        public static ResultadoOperacion AgregaArchivo(string ruta, HttpPostedFileBase archivo)
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ConsultasVersiones consulta = null;
            try
            {
                if (!string.IsNullOrEmpty(ruta))
                {
                    var carpeta = Path.GetDirectoryName(ruta);
                    if (!string.IsNullOrEmpty(carpeta))
                    {
                        if (!Directory.Exists(carpeta))
                        {
                            Directory.CreateDirectory(carpeta);
                        }
                    }

                    if (archivo != null && archivo.ContentLength > 0)
                    {
                        var path = ruta;
                        if (archivo.ContentLength > 0)
                        {
                            archivo.SaveAs(path);
                        }

                    }
                }

                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return result;
        }

        public static void LimpiaDirectorio(string startLocation)
        {
          
            try
            {
                foreach (var directory in Directory.GetDirectories(startLocation))
                {
                    LimpiaDirectorio(directory);
                    if (Directory.GetFiles(directory).Length == 0 &&
                        Directory.GetDirectories(directory).Length == 0)
                    {
                        Directory.Delete(directory, false);
                    }
                }
               
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
           
        }

        public static bool ExisteArchivo(string ruta)
        {
            var existe = false;
            try
            {
                existe = System.IO.File.Exists(ruta);
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return existe;
        }
        public static byte[] ObtenerArchivo(string ruta)
        {
            byte[] archivo = null;
            try
            {
                if (ExisteArchivo(ruta))
                    archivo = System.IO.File.ReadAllBytes(ruta);
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return archivo;
        }

        

        public static ResultadoOperacion MoverArchivo(string rutaOrigen, string rutaDestino)
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
     
            try
            {
                if (!string.IsNullOrEmpty(rutaOrigen) 
                    && !string.IsNullOrEmpty(rutaDestino)
                    && File.Exists(rutaOrigen))
                {
                    var directorioDestino = Path.GetDirectoryName(rutaDestino);
                    if (!string.IsNullOrEmpty(directorioDestino))
                    {
                        if (!Directory.Exists(directorioDestino))
                            Directory.CreateDirectory(directorioDestino);
                    }

                    System.IO.File.Move(rutaOrigen, rutaDestino);
                }

                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return result;
        }

        public static ResultadoOperacion MoverArchivosCarpeta(string rutaOrigen, string rutaDestino)
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };

            try
            {
                if (!string.IsNullOrEmpty(rutaOrigen)
                    && !string.IsNullOrEmpty(rutaDestino)
                    && Directory.Exists(rutaOrigen))
                {
                    foreach (var file in Directory.GetFiles(rutaOrigen))
                        MoverArchivo(file, rutaDestino +@"\"+ Path.GetFileName(file));
                }

                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return result;
        }
        public static ResultadoOperacion EliminarCarpeta(string ruta)
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ConsultasVersiones consulta = null;
            try
            {

                if (System.IO.Directory.Exists(ruta))
                    System.IO.Directory.Delete(ruta);

              
                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return result;
        }
    }
}
