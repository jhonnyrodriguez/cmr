﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace IDigitales.Pagina.Web.Controllers.Helpers
{
    public static class Utils
    {
        public static string ToJson<T>(this T elemento)
        {
            JsonSerializerSettings config = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
            var json = JsonConvert.SerializeObject(elemento, Formatting.Indented, config);
            return json;
        }
    }
}
