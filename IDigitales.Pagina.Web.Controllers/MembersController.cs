﻿using Rebex.Mail;
using Rebex.Mime;
using Rebex.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Utils;
using System.Diagnostics;
using IDigitales.Pagina.Web.Data.Modelos;


namespace IDigitales.Pagina.Web.Controllers
{
    [Authorize]
    public partial class MembersController : BaseController
    {
        public ActionResult Index()
        {
            return View("~/Views/Home/Members.cshtml");
        }

        public ActionResult Archivos()
        {
            ConsultasComunes consulta = null;
            var hoy = DateTime.Today;
            var Max = hoy.Year;
            var Min = hoy.Year;
            var model = new TabsViewModel();
            try
            {
                consulta = new ConsultasComunes();
                var Maxima = consulta.ObtenerFechaMaxima();
                var Minima = consulta.ObtenerFechaMinima();
                Max = Maxima.Year;
                Min = Minima.Year;                
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }
            model.Inicio = Min;
            model.Fin = Max;
            model.Seleccionado = Max;
            return View("Archivos/ArchivosPrincipal", model);
        }

       
    }
}
