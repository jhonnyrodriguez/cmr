﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Controllers.Helpers;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Utils;
using IDigitales.Pagina.Web.Data.Modelos;
using System.Data;

namespace IDigitales.Pagina.Web.Controllers
{
    public partial class  MembersController
    {
        public ActionResult Productos()
        { 
            if (((CustomPrincipal)User).TienePermiso("GENERAL_PRODUCTOS_EDITAR"))
            {
                ViewBag.EditarProductos = "true";
            }
            else
            {
                ViewBag.EditarProductos = "false";
            }
            return View("Productos/ProductosPrincipal");
        }

        public async Task<ActionResult> _ObtenerProductos(DataTablaPaginacion pag)
        {
            ConsultasProductos consulta = null;
            DataTablaResult<Productos> data = null;
            try
            {
                consulta = new ConsultasProductos();
                data = await consulta.ObtenerProductos(pag);
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }
           
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #region Edicion Productos
        public async Task<ActionResult> _EdicionProductos(int idProducto)
        {
            ConsultasProductos consulta = null;
            Productos elemento = null;
            try
            {
                if (idProducto != 0)
                {
                    consulta = new ConsultasProductos();
                    elemento = await consulta.ObtenerProducto(idProducto);
                }

                if (elemento == null)
                {
                    elemento = new Productos();
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return PartialView("Productos/ProductosEdicion", elemento);
        }
        
        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> AgregarActualizarProductos(Productos producto)
        {
            ResultadoOperacion result = null;
            try
            {
                using (var con = new ConsultasProductos())
                {
                    var ruta = con.ObtenerRuta(Rutas.EnumRutaTipo.Temporales);
                    var archivo= ruta.Directorio + producto.Ruta;
                    if (ManejadorArchivos.ExisteArchivo(archivo))
                    {
                        producto.Ruta = ruta.Directorio + producto.Ruta;
                    }
                    else
                    {
                        producto.Ruta = null;
                    }
                    result = await con.AgregarActualizarProductos(producto);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }

        #endregion

        #region Manejo Archivos


        [HttpPost, ValidateInput(false)]
        public ActionResult AgregarHojaProducto()
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ConsultasProductos consulta = null;
            try
            {
                var fName = "";
                consulta = new ConsultasProductos();
                var ruta = consulta.ObtenerRuta(Rutas.EnumRutaTipo.Temporales);

                foreach (string fileName in Request.Files)
                {
                    var file = Request.Files[fileName];
                    if (file != null && file.ContentLength > 0)
                    {
                        fName = file.FileName;
                        var path = ruta.Directorio + file.FileName;
                        ManejadorArchivos.AgregaArchivo(path, file);
                    }

                }

                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> RemoverHojaProducto(string nombre, int? idProducto)
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ConsultasProductos consulta = null;
            try
            {

                consulta = new ConsultasProductos();
                var ruta = consulta.ObtenerRuta(Rutas.EnumRutaTipo.Temporales);
                if (idProducto != null && idProducto != 0)
                {
                    var pro = await consulta.ObtenerProducto(idProducto.Value);
                    ManejadorArchivos.RemoverArchivo(pro.Ruta);

                }
                else
                {
                    var path = ruta.Directorio + nombre;
                    ManejadorArchivos.RemoverArchivo(path);
                }

                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ObtenerPdf(int idProducto)
        {
            //byte[] fileBytes = null;
            var ruta = "";
            try
            {
                using (var con = new ConsultasProductos())
                {
                    if (idProducto != 0)
                    {
                        var elemento = await con.ObtenerProducto(idProducto);
                        ruta = elemento.Ruta;
                    }
                }

                //fileBytes= ManejadorArchivos.ObtenerArchivo(ruta);

            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return File(ruta, "application/pdf");
            //    return File(fileBytes, "application/pdf");
            //{ 
            //    FileDownloadName = ld.FileName,
            //};
        }

        #endregion

        #region Reporte Inventarios

        public async Task<ActionResult> ReporteInventario()
        {
            var reporte = new ReportesController();
            var rutaPlantilla = Server.MapPath(reporte.ObtenerRutaPlantillas() + "ReporteInventario.repx");
            //Obtiene datos
            var datos = new List<ReporteInventarioViewModel> { await ObtenerInvetarioProductos() };
            //Genera reporte
            string ruta = reporte.Generar(datos,rutaPlantilla,"ReporteInventario","pdf",true);
            ruta = ruta.Replace("\\", "\\\\");
            return File(ruta, "application/pdf");
        }

        public async Task<ReporteInventarioViewModel> ObtenerInvetarioProductos()
        {
            var model = new ReporteInventarioViewModel();
            try
            {
                var conProductos = new ConsultasProductos();
                DataTable productosSitio = await conProductos.ObtenerInventarioProductos();

                var conSAE = new ConsultasSAE();
                DataTable productosSAE = conSAE.ObtenerProductos();
                DataTable almacenProductoSAE = conSAE.ObtenerInfoAlmacenProductos();
                 
                List<InformacionProductosViewModel> resultadoUnion = (
                    from p in productosSitio.AsEnumerable()
                    join pSAE in productosSAE.AsEnumerable()
                        on p.Field<string>("Modelo") equals pSAE.Field<string>("CVE_ART") into tmpProductos
                    join alm in almacenProductoSAE.AsEnumerable()
                        on p.Field<string>("Modelo") equals alm.Field<string>("CVE_ART") into tmpAlmacen
                    from pSAE in tmpProductos.DefaultIfEmpty()
                    from almacen in tmpAlmacen.DefaultIfEmpty()
                select new InformacionProductosViewModel
                {
                    IdProducto = Convert.ToInt32(p.Field<string>("IdProducto")),
                    NumeroParte = p.Field<string>("Modelo"),
                    Nombre = p.Field<string>("Nombre"),
                    Descripcion = p.Field<string>("Descripcion"),
                    Clasificacion = p.Field<string>("Clasificaciones"),
                    Tipo = p.Field<string>("Tipo"),
                    ClaveSAE = pSAE == null ? "" : pSAE.Field<string>("CVE_ART"),
                    Existencia = pSAE == null ? 0 : pSAE.Field<Double?>("EXIST"),
                    StockMinimo = almacen == null ? 0.0 : almacen.Field<Double?>("STOCK_MIN")
                }).ToList();

                if (resultadoUnion != null)
                {
                    var config = conProductos.ObtenerConfiguracionGeneral();
                    var fecha = DateTime.Today.ToString("dd/MM/yyyy");
                    var version = "Versión 1.0";
                    if (config != null)
                    {
                        var campoVersion = config.Where(x => x.Nombre == "VersionInventario").FirstOrDefault();
                        version = campoVersion != null && campoVersion.Valor !=null? "Versión " + campoVersion.Valor : version;

                        var campoFecha = config.Where(x => x.Nombre == "FechaInventario").FirstOrDefault();
                        fecha = campoFecha != null && campoFecha.Valor !=null? campoFecha.Valor : fecha;
                    }

                    //Escribir la existencias de los productos
                    model.Fecha = fecha;
                    model.Version = version;
                    var productos = resultadoUnion.Where(x => x.Tipo == Data.Entidades.Poco.Productos.EnumProductoTipos.Producto.ToString()).ToList();                                                           
                    if (productos!=null && productos.Count>0)
                    {
                        model.Productos= productos.OrderBy(x => x.Clasificacion).ToList();
                    }
                    //Escribir informacion de estructuras
                    model.Estructuras = ObtenerEstructuras(resultadoUnion);                  
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return model;
        }

        public List<InformacionProductosViewModel> ObtenerEstructuras(List<InformacionProductosViewModel> productosInventario)
        {
            List<InformacionProductosViewModel>  Estructuras = new List<InformacionProductosViewModel>();
            try
            {
                //Escribir las estructuras existentes   
                var conProductos = new ConsultasProductos();
                var componentesProduc = conProductos.ObtenerComponentesProductos();
                foreach (var estruct in productosInventario.ToList())
                {
                    if (ExisteEstructura(Estructuras,estruct.NumeroParte)==false)
                    { 
                        var listaComponentes = componentesProduc.Where(x => x.IdProducto == estruct.IdProducto).OrderBy(x => x.IdProducto).ToList();
                        if (listaComponentes != null && listaComponentes.Count > 0)
                        {
                            estruct.Componentes = ObtieneListaComponentes(estruct.IdProducto, listaComponentes, productosInventario);
                            foreach (var item in estruct.Componentes)
                            {
                                var listaSubComponentes = componentesProduc.Where(x => x.IdProducto == item.IdProducto).OrderBy(x => x.IdComponente).ToList();
                                item.Componentes= ObtieneListaComponentes(item.IdProducto, listaSubComponentes, productosInventario);
                            } 
                            if (estruct.Componentes.Count > 0)
                            {
                                Estructuras.Add(estruct);
                            }
                        }
                    }
                    if (Estructuras != null && Estructuras.Count > 0)
                    {
                        Estructuras = Estructuras.OrderBy(x => x.Clasificacion).ThenBy(y=> y.NumeroParte).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Estructuras;
        }

        public bool ExisteEstructura(List<InformacionProductosViewModel> estructuras, string numeroParte)
        {
            bool resultado = false;
            bool encontrado = false;
            try
            {
                foreach (var item in estructuras)
                {
                    if (item.Componentes!=null)
                    {
                        foreach (var item2 in item.Componentes)
                        {
                            if (item2.NumeroParte == numeroParte)
                            {
                                resultado = true;
                                encontrado = true;
                            }
                            else
                            {
                                if (item2.Componentes!=null)
                                {
                                    foreach (var item3 in item2.Componentes)
                                    {
                                        if (item3.NumeroParte == numeroParte)
                                        {
                                            resultado = true;
                                            encontrado = true;
                                        }
                                        if (encontrado) break;
                                    }
                                } 
                            }
                            if (encontrado) break;
                        }
                    }                   
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return resultado;
        }

        public List<InformacionProductosViewModel> ObtieneListaComponentes(int idProducto, List<ProductosComponentes> listaComponentes, List<InformacionProductosViewModel> productosInventario)
        {
            List<InformacionProductosViewModel> componentes = new List<InformacionProductosViewModel>();
            try
            {
                foreach (var item in listaComponentes)
                {
                    if (item.IdComponente != idProducto)
                    {
                        var prod = new InformacionProductosViewModel
                        {
                            NumeroParte = item.Productos_IdComponente.Modelo,
                            Nombre = item.Productos_IdComponente.Nombre,
                            StockMinimo = productosInventario.Where(x => x.NumeroParte == item.Productos_IdComponente.Modelo).FirstOrDefault() != null
                                            && productosInventario.Where(x => x.NumeroParte == item.Productos_IdComponente.Modelo).Count() > 0 ?
                                            productosInventario.Where(x => x.NumeroParte == item.Productos_IdComponente.Modelo).FirstOrDefault().StockMinimo : null,
                            Existencia = productosInventario.Where(x => x.NumeroParte == item.Productos_IdComponente.Modelo).FirstOrDefault() != null
                                        && productosInventario.Where(x => x.NumeroParte == item.Productos_IdComponente.Modelo).Count() > 0 ?
                                        productosInventario.Where(x => x.NumeroParte == item.Productos_IdComponente.Modelo).FirstOrDefault().Existencia : null,
                            IdProducto = item.Productos_IdComponente.IdProducto,
                            Clasificacion = item.Productos_IdComponente.Clasificaciones.Nombre,
                            NumeroParteNombre = item.Productos_IdComponente.Modelo + " " + item.Productos_IdComponente.Nombre,
                        };
                        componentes.Add(prod);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }           
            return componentes;
        }

        #endregion

        #region Listas Productos

        public async Task<ActionResult> EdicionListasPecios()
        {
            ConsultasProductos consulta = null;
            ListasPrecios elemento = null;
            try
            {
                //if (idLista != 0)
                //{
                //    consulta = new ConsultasProductos();
                //    elemento = await consulta.ObtenerListas(idLista);
                //}

                //if (elemento == null)
                //{
                    elemento = new ListasPrecios();
                //}
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return PartialView("Productos/ListasEdicion", elemento);
        }

        public async Task<ActionResult> AgregarActualizarListasPecios(ListasPrecios lista)
        {
            ResultadoOperacion result = null;
            try
            {
                using (var con = new ConsultasProductos())
                {
                    result = await con.AgregarActualizarListas(lista);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }

        public async Task<ActionResult> ListaPreciosProductos(int idLista)
        {
            List<ListasPrecioProductos> lista = null;
            try
            {
                using (var con = new ConsultasProductos())
                {
                    lista = await con.ObtenerListaProductos(idLista);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            
            return PartialView("Productos/ListasProductos", lista);
        }


        #region Modal Edicion ListasPreciosProductos


        public async Task<ActionResult> EdicionListaPreciosProductos(int idLista, int idProducto)
        {
            ListasPrecioProductos lista = null;
            try
            {
                using (var con = new ConsultasProductos())
                {
                    lista = await con.ObtenerListaPreciosProducto(idLista, idProducto);
                }

                if (lista == null)
                {
                    lista = new ListasPrecioProductos();
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
           
            return PartialView("Productos/EdicionListaPreciosProducto", lista);
        }
       
        public async Task<ActionResult>AgregarActualizarListaPreciosProductos(ListasPrecioProductos producto)
        {
            ResultadoOperacion resultado = null;
            try
            {
                using (var con = new ConsultasProductos())
                {
                    resultado = await con.AgregarActualizarListaPreciosProducto(producto);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(resultado);
        }
     
        public async Task<ActionResult> BorrarListaPreciosProductos(int idLista, int idProducto)
        {
            ResultadoOperacion resultado = null;
            try
            {
                using (var con = new ConsultasProductos())
                {
                    resultado = await con.EliminarListaPreciosProducto(idLista, idProducto);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(resultado);
        }


        #endregion
        #endregion

        #region Reporte Listas Precio

        public async Task<ActionResult> ReporteListasPrecio(int idLista, string formato)
        {
            var reporte = new ReportesController();
            var rutaPlantilla = Server.MapPath(reporte.ObtenerRutaPlantillas() + "ReporteListasPrecio.repx");
            
            //Obtiene la información
            var datos = new List<ReporteListasPrecioViewModel> { await ObtenerInformacionListasPrecios(idLista) };
            
            //Asignar nombre al archivo
            string nombreArchivo = "Lista Precios";
            if (datos!=null && datos.FirstOrDefault() != null)
	        {
                var tipoLista = datos.FirstOrDefault().TipoLista;
                var version= datos.FirstOrDefault().Version;              
                switch (tipoLista)
                {
                    case "LB":
                        nombreArchivo = "Lista Precios Base " + version;
                        break;
                    case "LN":
                         nombreArchivo = "Lista Precios Nacional " + version;
                        break;
                    case "LI":
                         nombreArchivo = "Lista Precios Internacional " + version;
                        break;
                    case "LS":
                         nombreArchivo = "Lista Precios Servicio " + version;
                         break;
                    default:
                         nombreArchivo = datos.FirstOrDefault().NombreLista + " " + version;
                        break;
                }
            }
            //Genera archivo
            var ruta="";
            var formatoArchivo="";
            var ext="";
            switch (formato)
            {
                case "xls":
                    ruta = reporte.Generar(datos, rutaPlantilla, nombreArchivo, "xls", true);
                    ruta = ruta.Replace("\\", "\\\\");
                    formatoArchivo = "application/xlsx";
                    ext = ".xlsx";
                    break;
                case "pdf":
                    ruta = reporte.Generar(datos, rutaPlantilla, nombreArchivo, "pdf", true);
                    ruta = ruta.Replace("\\", "\\\\");
                    formatoArchivo = "application/pdf";
                    ext = ".pdf";
                    break;
                default:
                    ruta = reporte.Generar(datos, rutaPlantilla, nombreArchivo, "pdf", true);
                    ruta = ruta.Replace("\\", "\\\\");
                    formatoArchivo = "application/pdf";
                    ext = ".pdf";
                    break;
            } 
            return File(ruta, formatoArchivo, nombreArchivo + ext); 
        }

        public async Task<ReporteListasPrecioViewModel> ObtenerInformacionListasPrecios(int idLista)
        {
            var model = new ReporteListasPrecioViewModel();
            try
            {
                var conProductos = new ConsultasProductos();
                var config = conProductos.ObtenerConfiguracionGeneral();
                var fecha = DateTime.Today.ToString("dd/MM/yyyy");
                var version = "Versión 1.0";
                if (config!=null)
                {
                    var campoVersion= config.Where(x => x.Nombre == "VersionListasPrecio").FirstOrDefault();
                    version = campoVersion != null && campoVersion.Valor !=null? "Versión "+ campoVersion.Valor : version;

                    var campoFecha = config.Where(x => x.Nombre == "FechaListasPrecio").FirstOrDefault();
                    fecha = campoFecha != null && campoFecha.Valor !=null? campoFecha.Valor : fecha;
                }

                var productos = await conProductos.ObtenerListasPreciosProductos(idLista);
                if (productos!=null && productos.Count>0)
                {
                    model.Productos = new List<InformacionPreciosViewModel>();
                    model.Fecha = fecha;
                    model.Version = version;
                    model.TipoLista = productos[0].LIstasPrecios != null ? productos[0].LIstasPrecios.Codigo : "";
  
                    foreach (var item in productos)
                    {
                        var prod = new InformacionPreciosViewModel
                        {
                            NumeroParte = item.Productos.Modelo,
                            Descripcion = item.Productos.Descripcion,
                            Clasificacion= item.Productos.Clasificaciones !=null? item.Productos.Clasificaciones.Nombre:"",
                            DatosCrudos = item.Productos.DatosCrudos,
                            DatosUsables = item.Productos.DatosUsables,
                            Transfer = item.Porcentaje!=null && item.Porcentaje>0? (double)item.PrecioFinal : ((double)item.Productos.Precio / 0.75),
                            IdOrdenPersonalizado = item.Productos.Clasificaciones != null ? ObtenerOrdenPersonalizado(item.Productos.Clasificaciones) : 0,
                        };
                        model.Productos.Add(prod);
                    }
                    if (model.Productos.Count>0) 
                    {
                        model.NombreLista = productos[0].LIstasPrecios != null ? productos[0].LIstasPrecios.Nombre.ToUpper() : "";
                        model.Productos = model.Productos.OrderBy(x => x.IdOrdenPersonalizado).ToList();
                        model.Aplicable = "APLICABLE PARA " + DateTime.Now.Year.ToString();
                        model.Vigencia = " Enero-Diciembre " + DateTime.Now.Year.ToString();
                        model.ProductosHardwareServidores = model.Productos.Where(x => x.Clasificacion.Contains("HARDWARE HIS")
                        || x.Clasificacion.Contains("HARDWARE PACS CORE") || x.Clasificacion.Contains("HARDWARE PACS ENCORE")
                        || x.Clasificacion.Contains("PACS ENCORE")).ToList();
                       
                        model.ProductosLicencias= model.Productos.Where(x => x.Clasificacion.Contains("LICENCIAS")).ToList();

                        model.ProductosMonitoresSeccion1 = model.Productos.Where(x => x.Clasificacion.Contains("DISCOS DUROS") && x.NumeroParte.Contains("G0199-HD")).ToList();
                        foreach (var item in model.ProductosMonitoresSeccion1 )
                        {
                            item.Clasificacion = "DISCOS DUROS SERVIDORES";
                        }

                        model.ProductosHardwareEstaciones = model.Productos.Where(x => x.Clasificacion.Contains("HARDWARE PARA ESTACIONES")
                        || x.Clasificacion.Contains("MONITORES DIAGNOSTICOS/CLINICOS") || x.Clasificacion.Contains("MONITORES ")
                        || x.Clasificacion.Contains("TERMINALES ADICIONALES") || x.Clasificacion.Contains("DISPOSITIVOS EXTERNOS")
                        || x.Clasificacion.Contains("CONSUMIBLES") || x.Clasificacion.Contains("SISTEMAS DE TURNOS")
                        || x.Clasificacion.Contains("DISPOSITIVOS EXTERNOS") || x.Clasificacion.Contains("UPS")
                        || (x.Clasificacion.Contains("DISCOS DUROS") && x.NumeroParte.Contains("G0199-HD") == false)
                        || x.Clasificacion.Contains("PUBLICADORES")  || x.Clasificacion.Contains("ACCESORIOS")).ToList();

                        foreach (var item in model.ProductosHardwareEstaciones.Where(x=>x.Clasificacion=="DISCOS DUROS"))
                        {
                            item.Clasificacion = "DISCOS DUROS ESTACIONES";
                        }
                        model.ProductosMobiliario = model.Productos.Where(x => x.Clasificacion.Contains("MOBILIARIO")).ToList();
                        model.ProductosServicios = model.Productos.Where(x => x.Clasificacion.Contains("SERVICIOS PROFESIONALES")).ToList();
                    }                                    
                }             
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return model;
        }

        public int ObtenerOrdenPersonalizado(Clasificaciones clasificacion)
        {
            int idOrden = 1000;

            switch (clasificacion.IdClasificacion)
            {
                case 1:// "HARDWARE HIS"
                    idOrden = 1;
                    break;
                case 2://"HARDWARE PACS CORE"
                    idOrden = 2;
                    break;
                case 3://"HARDWARE PACS ENCORE"
                    idOrden = 3;
                    break;
                case 4://"HARDWARE PARA ESTACIONES"
                    idOrden = 11;
                    break;
                case 5://"DISCOS DUROS"
                    idOrden = 20;
                    break;
                case 6://"MONITORES DIAGNOSTICOS/CLINICOS"
                    idOrden = 12;
                    break;
                case 7://"MONITORES"
                    idOrden = 13;
                    break;
                case 9://"PUBLICADORES"
                    idOrden = 15;
                    break;
                case 10://"SISTEMAS DE TURNOS"
                    idOrden = 19;
                    break;
                case 11://"ACCESORIOS"
                    idOrden = 21;
                    break;
                case 12://"TERMINALES ADICIONALES"
                    idOrden = 14;
                    break;
                case 13://"DISPOSITIVOS EXTERNOS"
                    idOrden = 16;
                    break;
                case 14://"CONSUMIBLES"
                    idOrden = 17;
                    break;
                case 15://"UPS"
                    idOrden = 18;// mismo orden que dispositivos externo
                    break;
                case 16://"MOBILIARIO"
                    idOrden = 22;
                    break;
                case 19://"PACS ENCORE (Crecimiento en Unidades de Almacenamiento)"
                    idOrden = 4;
                    break;
                case 21://"LICENCIAS ADMINISTRADOR DE SERVIDORES"
                    idOrden = 5;
                    break;
                case 22://"LICENCIAS VISOR MULTIMODALIDAD PARA RADIÓLOGOS"
                    idOrden = 6;
                    break;
                case 23://"LICENCIAS VISOR WEB"
                    idOrden = 7;
                    break;
                case 24://"LICENCIAS SISTEMA DE INFORMACIÓN RADIÓLOGICA"
                    idOrden = 8;
                    break;
                case 25://"LICENCIAS MODULOS EXTRAS"
                    idOrden = 9;
                    break;
                case 27://"LICENCIAS RECONOCIMIENTO DE VOZ"
                    idOrden = 10;
                    break;
                case 28://"SERVICIOS PROFESIONALES"
                    idOrden = 23;
                    break;
                default:
                    idOrden = 100;
                    break;
            }
            return idOrden;            
        }

        public ActionResult ImpresionListasPrecio()
        {            
            return PartialView("Productos/ImpresionListasPrecio");             
        }

        #endregion
    }
}