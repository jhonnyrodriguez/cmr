﻿using IDigitales.Pagina.Web.Data;
using Rebex.Mail;
using Rebex.Mime;
using Rebex.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using IDigitales.Pagina.Web.Controllers.Helpers;

namespace IDigitales.Pagina.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View("~/Views/Home/Index.cshtml");
        }

        public ActionResult es()
        {
            EstablecerCulturaEs();
            return Redirect("~/");
        }

        public ActionResult en()
        {
            EstablecerCulturaEn();
            return Redirect("~/");
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            if (Request.Cookies["Usuario"] != null)
            {
                Response.Cookies["Usuario"].Expires = DateTime.Now.AddDays(-1);
            }
            Session.Abandon();
            return View("~/Views/Home/Login.cshtml");
        }

        [AllowAnonymous]
        public ActionResult CookieIdioma(string idioma)
        {
            CrearCookie("_cultura", idioma);
            return Json(true);
        }
        [AllowAnonymous]
        public ActionResult AutenticarUsuario(string user, string password)
        {
            var valido = false;
                 ConsultasUsuarios cp = null; 
            try
            {
                cp = new ConsultasUsuarios();
                var usuario = cp.ValidarUsuario(user, password);
                valido = usuario != null;
                HttpCookie cookie = new HttpCookie("Usuario");
                cookie.Expires = DateTime.Now.AddDays(7);

                if (valido)
                {
                    var permisos = cp.ObtenerPermisos(usuario.IdUsuario);

                    var serializeModel = new CustomPrincipalSerializeModel();
                    serializeModel.Id = usuario.IdUsuario;
                    serializeModel.Nombre = usuario.Nombre;
                    serializeModel.Permisos =permisos;
                    serializeModel.Tipo = usuario.Tipo.ToString();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(serializeModel);

                    cookie.Value = userData;
                    ////
                    //var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, user) }, "Cookie");
                    //var permisos = cp.ObtenerPermisos(usuario.IdUsuario);
                    ////agregar roles al identity
                    //foreach (var p in permisos)
                    //{
                    //    identity.AddClaim(new Claim(ClaimTypes.Role, p.ToUpper()));
                    //}

                    //FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                    //    1,
                    //    usuario.Nombre,
                    //    DateTime.Now,
                    //    DateTime.Now.AddMinutes(60),
                    //    false,
                    //    "");
                    ////
                    //string encTicket = FormsAuthentication.Encrypt(authTicket);
                    //HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    //ControllerContext.HttpContext.Response.Cookies.Add(faCookie);
                }
                else
                {
                    cookie.Value = "";
                }
               
                ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ConsultasBase.DisposeIfNotNull(cp);
            }

            return Json(new { existe = valido });
        }

        public ActionResult HIS_INFOSALUD()
        {
            return View("~/Views/Partials/Modal/proj-HIS.cshtml");
        }

        public ActionResult Publicadores_Lite100()
        {
            return View("~/Views/Partials/Modal/proj-Lite100.cshtml");
        }

        public ActionResult Publicadores_Lite20()
        {
            return View("~/Views/Partials/Modal/proj-Lite20.cshtml");
        }

        public ActionResult Radiografia_Movil_RX230()
        {
            return View("~/Views/Partials/Modal/proj-MovilRX230.cshtml");
        }

        public ActionResult Radiografia_Movil_RX230B()
        {
            return View("~/Views/Partials/Modal/proj-MovilRX230B.cshtml");
        }

        public ActionResult Pacs_Encore()
        {
            return View("~/Views/Partials/Modal/proj-PacsEncore.cshtml");
        }

        public ActionResult Radiografia_ARIXRAD()
        {
            return View("~/Views/Partials/Modal/proj-RadiografiaARIXRAD.cshtml");
        }

        public ActionResult Radiografia_INTEGRIX()
        {
            return View("~/Views/Partials/Modal/proj-RadiografiaINTEGRIX.cshtml");
        }

        public ActionResult Radiografia_MRHII()
        {
            return View("~/Views/Partials/Modal/proj-RadiografiaMRHII.cshtml");
        }

        public ActionResult Radiografia_MRF90T()
        {
            return View("~/Views/Partials/Modal/RadiografiaMRF90T.cshtml");
        }

        public ActionResult Languages()
        {
            string cultura = obtenerCulturaCookie();
            return PartialView("~/Views/Partials/Base/Languages.cshtml", cultura);
        }

        public class DataEmail
        {
            public string nombre { get; set; }
            public string correo { get; set; }
            public string tipo { get; set; }
            public string titulo { get; set; }
            public string mensaje { get; set; }
        }

        private bool validarDatos(DataEmail data)
        {
            if (string.IsNullOrWhiteSpace(data.nombre) || string.IsNullOrWhiteSpace(data.correo) || string.IsNullOrWhiteSpace(data.mensaje))
                return false;
            return true;
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult SendMail(DataEmail data)
        {
            MailMessage mailMessage = null;
            Smtp servidorSmtp = null;
            bool success = false;
            string error = string.Empty;

            try
            {
                if (validarDatos(data))
                {
                    mailMessage = new MailMessage();

                    mailMessage.From.Add("contacto@cmr3.com.mx");

                    mailMessage.To.Add("contacto@cmr3.com.mx");
                    mailMessage.To.Add("gmonroy@eymsa.com.mx");
                    mailMessage.To.Add("vendedores1@eymsa.com.mx");
                    //mailMessage.To.Add("armandoserrato987@gmail.com");
                    var mensajeURL = System.Web.HttpUtility.UrlDecode(data.mensaje);
                    var mensaje = string.Empty;

                    if (!string.IsNullOrEmpty(mensajeURL))
                    {
                        var renglones = mensajeURL.Split('\n');

                        foreach (var renglon in renglones)
                        {
                            mensaje += renglon;
                            mensaje += "<br/>";
                        }
                    }
                    string body = " <p>De: " + data.nombre + "<br/>"
                                  + "Tipo: <b>VENTAS</b> <br/>"
                                  + "Mensaje: " + "<br/>" + mensaje
                                  + "<br/>"
                                  + "Responder al correo: " + data.correo + "</p>";

                    mailMessage.Subject = data.titulo ?? "Sin Titulo";
                    mailMessage.BodyHtml = body;

                    mailMessage.Priority = MailPriority.High;
                    //Configuracion del servidor
                    servidorSmtp = new Rebex.Net.Smtp();

                    //Conexión con seguridad explicita.
                    servidorSmtp.Connect("smtp.cmr3.com.mx", 587);

                    servidorSmtp.Login("contacto@cmr3.com.mx", "ct2016Mx@", SmtpAuthentication.Login);

                    servidorSmtp.Send(mailMessage);
                    success = true;
                }
                else
                {
                    success = false;
                    error = "Campos requeridos: Nombre, correo y mensaje.";
                }


                return Json(new { success = success, error = error });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message }); 
            }
        }

        [HttpGet]
        public JsonResult SendMail(string nombre, string correo, string tipo, string titulo, string mensaje)
        {
            MailMessage mailMessage = null;
            Smtp servidorSmtp = null;
            try
            {
                mailMessage = new MailMessage();
                mailMessage.From.Add("contacto@cmr3.com.mx");

                mailMessage.To.Add("contacto@cmr3.com.mx");
                /*mailMessage.To.Add("gmonroy@eymsa.com.mx");
                mailMessage.To.Add("vendedores1@eymsa.com.mx");*/

                string body = "De: " + nombre + "\n"
                              + "Tipo: " + tipo + "\n"
                              + "Mensaje: " + "\n" + mensaje
                              + "\n" + "\n" + " correo: " + correo;

                mailMessage.Subject = titulo;
                mailMessage.BodyText = body;

                //Configuracion del servidor
                servidorSmtp = new Rebex.Net.Smtp();

                //Conexión con seguridad explicita.
                servidorSmtp.Connect("smtp.cmr3.com.mx", 587);

                servidorSmtp.Login("contacto@cmr3.com.mx", "ct2016Mx@", SmtpAuthentication.Login);
                servidorSmtp.Send(mailMessage);

                return Json(String.Format("'Success':'true','Error':''"));
            }
            catch (Exception ex)
            {
                return Json(String.Format("'Success':'false','Error':'{0}'", ex.Message));
            }
        }
    }
}
