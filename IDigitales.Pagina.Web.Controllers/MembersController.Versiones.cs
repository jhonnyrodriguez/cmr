﻿using IDigitales.Pagina.Web.Data.Modelos;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DevExpress.Data.Filtering.Helpers;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Controllers.Helpers;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Utils;


namespace IDigitales.Pagina.Web.Controllers
{
    public partial class  MembersController
    {
        
        public ActionResult Versiones()
        {
            return View("Versiones/VersionesPrincipal");
        }


        public ActionResult VersionesAplicaciones()
        {
            ConsultasVersiones consulta = null;
            var model = new List<Aplicaciones>();
            try
            {
                var usuario = (CustomPrincipal)User;
                var idUsuario = usuario != null ? usuario.Id : 0;
                consulta = new ConsultasVersiones();
                model = consulta.ObtenerListaAplicaciones(idUsuario);
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }
            return View("Versiones/VersionesAplicaciones", model);
        }

        #region Registro Aplicacion

        public async Task<ActionResult> RegistroAplicacion(int idAplicacion)
        {
            ConsultasVersiones consulta = null;
            Aplicaciones app = new Aplicaciones();
            try
            {
                consulta = new ConsultasVersiones();
                if (idAplicacion != 0)
                    app = await consulta.ObtenerAplicacion(idAplicacion);
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return PartialView("Versiones/VersionesAplicacionesRegistro", app);
        }

        [HttpPost]
        public ActionResult AgregaArchivoAplicacion()
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ConsultasVersiones consulta = null;
            var fName = "";
            var path = "";
            try
            {
               
                consulta = new ConsultasVersiones();
                var ruta = consulta.ObtenerRuta(Rutas.EnumRutaTipo.Aplicaciones);
                var directorioTemp = ruta.Directorio + @"\Temp\";
                foreach (string fileName in Request.Files)
                {
                    var file = Request.Files[fileName];
                    if (file != null && file.ContentLength > 0)
                    {
                        fName = file.FileName;
                        path = directorioTemp + fName;
                        ManejadorArchivos.AgregaArchivo(path, file);
                    }

                }

                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(new { Message = fName, Nombre = fName, Ruta = path });
        }


        [HttpPost]
        public ActionResult RemoverArchivo(string nombreAplicacion, int? idVersion)
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            ConsultasVersiones consulta = null;
            try
            {

                consulta = new ConsultasVersiones();
                var ruta = consulta.ObtenerRuta(Rutas.EnumRutaTipo.Aplicaciones);
                if (idVersion != null && idVersion != 0)
                {
                    var ver = consulta.ObtenerVersionPorId((int)idVersion);
                    ManejadorArchivos.RemoverArchivo(ver.Ruta);
                   
                }
                else
                {
                    var path = ruta.Directorio + @"\Temp\"+ nombreAplicacion;
                    ManejadorArchivos.RemoverArchivo(path);
                    
                }

                result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GuardarAplicacion(Aplicaciones aplicacion, string nombreArchivo)
        {
            ConsultasVersiones consulta = null;
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            try
            {
                consulta = new ConsultasVersiones();
                result = await consulta.GuardarAplicacion(aplicacion);
                var ruta = consulta.ObtenerRuta(Rutas.EnumRutaTipo.Aplicaciones);

                if (result.Resultado == OperacionResultado.Ok)
                {

                    if (System.IO.File.Exists(ruta.Directorio + nombreArchivo))
                    {
                        var dir = ruta.Directorio + result.Id + "\\" + result.Id2 + "\\";
                        var rutaApp = dir + "\\" + nombreArchivo;
                        if (!Directory.Exists(dir))
                            Directory.CreateDirectory(dir);
                        System.IO.File.Move(ruta.Directorio + nombreArchivo, rutaApp);

                        result = await consulta.ActualizaRutaAplicacion(result.Id, result.Id2, rutaApp);
                        EnviaCorreo(result.Id, result.Id2);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

  
        public async Task<FileResult> DescargarAplicacionArchivo(int idAplicacion, int idVersion, string nombreArchivo)
        {
            ConsultasVersiones consulta = null;
            byte[] archivo = null;
            var mime = string.Empty;
            var nombre = string.Empty;
            try
            {
                consulta = new ConsultasVersiones();
                var aplicacion = await consulta.ObtenerVersionAsync(idAplicacion, idVersion);
                if (aplicacion != null && !string.IsNullOrEmpty(aplicacion.Ruta) && !string.IsNullOrEmpty(nombreArchivo))
                {
                    var file = aplicacion.Ruta + "\\" + nombreArchivo;
                    if (System.IO.File.Exists(file))
                    {

                        archivo = System.IO.File.ReadAllBytes(file);
                        var ext = System.IO.Path.GetExtension(file);
                        nombre = nombreArchivo;
                        switch (ext)
                        {
                            case ".zip":
                                mime = "application/zip";
                                break;
                            case ".rar":
                                mime = "application/rar";
                                break;
                            default:
                                mime = MimeMapping.GetMimeMapping(aplicacion.Ruta);
                                break;
                        }
                       
                    }

                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return File(archivo, mime, nombre);
        }

        public ActionResult ObtenerRutasAplicacion(int idAplicacion, int idVersion)
        {
            ConsultasVersiones consulta = null;
            string[] archivos = null;
      
            try
            {
                consulta = new ConsultasVersiones();
                var aplicacion = consulta.ObtenerVersion(idAplicacion, idVersion);
                if (aplicacion != null)
                {
                    if (!string.IsNullOrEmpty(aplicacion.Ruta))
                    {
                        if (Directory.Exists(aplicacion.Ruta))
                        {
                            archivos = Directory.GetFiles(aplicacion.Ruta);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(archivos, JsonRequestBehavior.AllowGet);
        }

        public async Task<FileResult> DescargaArchivo(string ruta)
        {
            var file = ManejadorArchivos.ObtenerArchivo(ruta);
            var nombre = Path.GetFileName(ruta);
            var ext = Path.GetExtension(ruta);
           
            var mime = "";
            if (ext == ".zip")
            {
                mime = "application/zip";
            }
            else if (ext == ".rar")
            {
                mime = "application/rar";
            }
            else
            {
                mime = MimeMapping.GetMimeMapping(ruta);
            }

            return File(file, mime, nombre);
        }

      
        #endregion



        #region Editar Aplicacion

        public ActionResult EditarAplicacion(int idAplicacion)
        {
            ConsultasVersiones consulta = null;
            Aplicaciones model = new Aplicaciones();
            try
            {

                consulta = new ConsultasVersiones();
                if(idAplicacion != 0)
                    model =  consulta.ObtenerAplicacionSinc(idAplicacion);

            
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return PartialView("Versiones/VersionesAplicacionesEditar", model);
        }


        public ActionResult ObtenerAplicacionVersiones(int idAplicacion)
        {
            ConsultasVersiones consulta = null;
            List<AplicacionesVersiones> model = null;
            try
            {

                consulta = new ConsultasVersiones();
                model =  consulta.ObtenerAplicacionVersiones(idAplicacion);


            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return PartialView("Versiones/VersionesAplicacionesEditarElem", model);
        }


        public async Task<ActionResult> EliminarVersion(int idAplicacion, int idVersion)
        {
            ConsultasVersiones consulta = null;
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            try
            {
                consulta = new ConsultasVersiones();
                var ver = consulta.ObtenerVersion(idAplicacion, idVersion);
                result = await consulta.EliminarVersion(idAplicacion, idVersion);
               
                if (result.Resultado == OperacionResultado.Ok)
                {

                    if (System.IO.File.Exists(ver.Ruta))
                    {
                        System.IO.File.Delete(ver.Ruta);
                        //Elimina el folder de la version si no contiene nada
                        var carpetaVersion = Path.GetDirectoryName(ver.Ruta);
                        if (Directory.Exists(carpetaVersion) && !Directory.EnumerateFiles(carpetaVersion).Any())
                        {
                            Directory.Delete(carpetaVersion);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> EliminarAplicacion(int idAplicacion)
        {
            ConsultasVersiones consulta = null;
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            try
            {
                consulta = new ConsultasVersiones();
                var versiones = consulta.ObtenerAplicacionVersiones(idAplicacion);

                var ruta  = versiones.FirstOrDefault() != null ? versiones.FirstOrDefault().Ruta : String.Empty;

                result = await consulta.EliminarAplicacion(idAplicacion);

                if (result.Resultado == OperacionResultado.Ok)
                {
                    foreach (var ver in versiones)
                    {
                        ManejadorArchivos.EliminarCarpeta(ver.Ruta);
                       
                    }

                    

                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

       
        public async Task<ActionResult> ActualizarAplicacion(Aplicaciones aplicacion)
        {
            ConsultasVersiones consulta = null;
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            try
            {
                consulta = new ConsultasVersiones();
                result = await consulta.ActualizarAplicacion(aplicacion);
               
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }



        #endregion


        #region Editar Version

        public ActionResult EditarVersion(int idVersion, int idAplicacion)
        {
            ConsultasVersiones consulta = null;
            AplicacionesVersiones model = new AplicacionesVersiones();
            FileInfo fileInfo = null;
            try
            {

                consulta = new ConsultasVersiones();
                if (idVersion != 0)
                {
                    model = consulta.ObtenerVersionPorId(idVersion);
                    if (!string.IsNullOrEmpty(model.Ruta))
                    {

                        model.Archivos =new List<AplicacionesVersionesArchivo>();
                        AplicacionesVersionesArchivo archivo = null;
                        var archivos = Directory.GetFiles(model.Ruta);
                        foreach (var arc in archivos)
                        {
                            archivo = new AplicacionesVersionesArchivo();
                            archivo.Nombre = Path.GetFileName((arc));
                            fileInfo = new FileInfo(arc);
                            archivo.Tamano = fileInfo.Length.ToString();
                            archivo.Url = arc;
                            var ext = Path.GetExtension((arc));
                            archivo.Tipo = ext == ".zip" ? "application/zip" : "application/rar";
                            model.Archivos.Add(archivo);
                        }
                        

                    }
                }
                else
                {
                    model.IdAplicacion = idAplicacion;
                }

            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return PartialView("Versiones/VersionesAplicacionesEditarVersion", model);
        }


        public async Task<ActionResult> GuardarVersion(AplicacionesVersiones version)
        {
            ConsultasVersiones consulta = null;
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            try
            {

                consulta = new ConsultasVersiones();
                result = await consulta.ActualizarVersion(version);
                var ruta = consulta.ObtenerRuta(Rutas.EnumRutaTipo.Aplicaciones);

                if (result.Resultado == OperacionResultado.Ok)
                {
                    var dirTemp = ruta.Directorio + @"\Temp\";
                    if (Directory.Exists(dirTemp))
                    {
                        var dir = ruta.Directorio + version.IdAplicacion + "\\" + result.Id + "\\";
                        ManejadorArchivos.MoverArchivosCarpeta(dirTemp, dir);
                        await consulta.ActualizaRutaVersion(result.Id, dir);
                        var enviado = EnviaCorreo((int) version.IdAplicacion, result.Id);
                        await consulta.ActualizaCorreoEnviadoVersion(result.Id, enviado);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if(consulta != null)
                consulta.Dispose();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> CorreoVersion(int IdAplicacion, int IdVersion)
        {
            ConsultasVersiones consulta = null;
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            try
            {
                consulta = new ConsultasVersiones();

                var enviado = EnviaCorreo((int)IdAplicacion, IdVersion, true);
                await consulta.ActualizaCorreoEnviadoVersion(IdVersion, enviado);
                if (enviado)
                    result.Resultado = OperacionResultado.Ok;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        



        public ActionResult ObtenerVersionArchivos(int idVersion)
        {
            ConsultasVersiones consulta = null;
            List<VersionArchivosViewModel> archivos = new List<VersionArchivosViewModel>();
            VersionArchivosViewModel archivo = null;
            try
            {
                consulta = new ConsultasVersiones();
                var version = consulta.ObtenerVersionPorId(idVersion);
                if (version != null && !string.IsNullOrEmpty(version.Ruta))
                {
                    if (Directory.Exists(version.Ruta))
                    {
                        foreach (var file in Directory.GetFiles(version.Ruta))
                        {
                            archivo = new VersionArchivosViewModel();
                            archivo.NombreArchivo = Path.GetFileName(file);
                            archivo.Tamano = ObtenerTamano(file);
                            archivo.Fecha = System.IO.File.GetLastWriteTime(file);
                            archivos.Add(archivo);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(archivos, JsonRequestBehavior.AllowGet);
        }


        private string ObtenerTamano(string filename)
        {
            string[] sizes = { "B", "KB", "MB", "GB", "TB" };
            double len = new FileInfo(filename).Length;
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }
            string result = String.Format("{0:0.##} {1}", len, sizes[order]);
            return result;
        }
        #endregion


        #region Opciones Descarga

        
        public async Task<ActionResult> OpcionesDescarga(int idAplicacion)
        {
            ConsultasVersiones consulta = null;
            var model = new Aplicaciones();
            try
            {
                consulta = new ConsultasVersiones();
                model = await consulta.ObtenerAplicacion(idAplicacion);
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }
            return View("Versiones/VersionesAplicacionesDescarga", model);
        }

        #endregion
        #region Correo

        private bool EnviaCorreo(int idAplicacion, int idVersion, bool force = false)
        {
            ConsultasVersiones consulta = null;
            bool enviado = false;
            try
            {
                consulta = new ConsultasVersiones();
                var contactos = consulta.ObtieneContactos(idAplicacion);

                if (contactos.Count > 0)
                {
                    var version = consulta.ObtenerVersion(idAplicacion, idVersion);
                    var contactosStr = string.Join(";", contactos.Select(x => x.Email));

                    var asunto = "Actualización de " + version.Aplicaciones.Nombre + " (ver. " + version.Version + ")";
                   

                    var mensajeHtml = "<h1><span style=\"font-family: calibri, sans-serif;\"><strong>Actualizaci&oacute;n " + version.Aplicaciones.Nombre + " versi&oacute;n " +
                        version.Version + "</strong></span></h1>" +
                        "<p><span style=\"font-family: calibri, sans-serif;\">La versi&oacute;n&nbsp;" + version.Version + " contempla los siguientes cambios:</span></p>" +
                        "</br>" +
                        "<span style=\"font-family: calibri, sans-serif; white-space: pre-line;\">" + (version.Cambios != null ? version.Cambios.Replace("\n", "</br>"): string.Empty )+ "</span>" +
                        "<p><span style=\"font-family: calibri, sans-serif;\"> Ingresa a&nbsp;<a href=\"http://www.cmr-rx.com/Home/Login\">www.cmr-rx.com/Home/Login</a> para descargar la versi&oacute;n mas actual.</span></p>";

                    if(force || version.CorreoEnviado == false || version.CorreoEnviado == null)
                        enviado = ManejadorCorreos.EnviaCorreo(contactosStr, asunto, "", mensajeHtml);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return enviado;
        }
        #endregion

    }
}
