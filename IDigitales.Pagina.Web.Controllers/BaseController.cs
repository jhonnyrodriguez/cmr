﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.Mvc;

using System.Globalization;
using System.Threading;
using IDigitales.Pagina.Web.Data.Utils;
using System.Diagnostics;

namespace IDigitales.Pagina.Web.Controllers
{
   public class BaseController : AsyncController

    {
       protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
       {
           var r  = Request.UserLanguages;
           string nombreCultura = null;

           try
           {
               var culturaCookie = Request.Cookies["_cultura"];

               if (culturaCookie != null)
               {
                   var lenguaje = culturaCookie.Value;

                   switch (lenguaje)
                   {
                       case "es":
                           nombreCultura = "es-mx";
                           break;
                       case "us":
                           nombreCultura = "en-us";
                           break;
                       default:
                           nombreCultura = "es";
                           break;
                   }
               }
               else
               {
                   var currentCulture = obtenerCultura();

                   if (currentCulture.Name == "en-us")
                   {
                       nombreCultura = "en-us";
                       CrearCookie("_cultura", "us");
                   }
                   else
                   {
                       nombreCultura = "es";
                       CrearCookie("_cultura", "es");
                   }
               }

              
           }
           catch (Exception ex)
           {
               nombreCultura = "en-us";
               Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

           Thread.CurrentThread.CurrentCulture = new CultureInfo(nombreCultura);
           Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
          

           return base.BeginExecuteCore(callback, state);
       }

       protected void EstablecerCultura(CultureInfo culture)
       {
           var nombreCultura = "es";

           if (culture.Name == "en" || culture.Name.ToLower().StartsWith("en-us"))
           {
               nombreCultura = "en-us";
               CrearCookie("_cultura", "us");
           }
           else
           {
               nombreCultura = "es";
               CrearCookie("_cultura", "es");
           }

           Thread.CurrentThread.CurrentCulture = new CultureInfo(nombreCultura);
           Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
          
       }

       protected void EstablecerCulturaEs()
       {
           EstablecerCultura(new CultureInfo("es"));
       }

       protected void EstablecerCulturaEn()
       {
           EstablecerCultura(new CultureInfo("en-us"));
       }

       protected string obtenerCulturaCookie(){
             var culturaCookie = Request.Cookies["_cultura"];
             var cultura = "es";

           if(culturaCookie != null){
               cultura =culturaCookie.Value;
           }

           return cultura;
       }

       private CultureInfo obtenerCultura()
       {
           var currentCulture = new CultureInfo("en-us"); // default

           if (Request.UserLanguages.Count() > 0)
           {
               var lang = Request.UserLanguages[0];

               if (lang.StartsWith("es-") || (lang == "es"))
               {
                   currentCulture = new CultureInfo("es");
               }

               
           }

           return currentCulture;
       }

        protected void CrearCookie(string nombre, string valor, int dias = 30)
        {
            var c = HttpContext.Request.Cookies[nombre];
            if (c != null)
            {
                c.Value = valor;
                c.Expires = DateTime.Now.AddDays(dias);
            }
            else
            {
                c = new HttpCookie(nombre)
                {
                    Value = valor,
                    Expires = DateTime.Now.AddDays(dias)
                };
            }
            HttpContext.Response.Cookies.Add(c);
        }

    }
}
