﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Utils;
using System.Diagnostics;
using IDigitales.Pagina.Web.Data.Modelos;

namespace IDigitales.Pagina.Web.Controllers
{
    partial class MembersController
    {
        public ActionResult loaderArchivos(int idTab)
        {            
            return View("Archivos/ArchivosContenido", idTab);
        }

        public ActionResult ContenidoArchivos(int idTab)
        {
            ConsultasComunes consulta = null;
            var model = new List<ContenidoTabsViewModel>();
            try
            {
                consulta = new ConsultasComunes();
                var totalDocs = consulta.ObtenerDocumentosPorAnio(idTab);                           
                var Cat = ObtenerCatalogos("CatalogoCategorias", "");
                var subCat = ObtenerCatalogos("CatalogoSubCategorias", "");
                foreach (var c in Cat)
                {
                    var con = new ContenidoTabsViewModel();
                    con.IdCategoria = c.Id;
                    con.Categoria = c.Valor;
                    con.SubCategorias = new List<SubCategoriaViewModel>();
                    foreach (var s in subCat)
                    {
                        var sub = new SubCategoriaViewModel();
                        sub.IdSubcategoria = s.Id;
                        sub.Subcategoria = s.Valor;
                        sub.Documentos = new List<InfoDocumentosViewModel>();
                        var docSub = totalDocs.Where(x => Convert.ToInt32(x.SubCategoria ?? 0).ToString() == s.Id && Convert.ToInt32(x.Categoria ?? 0).ToString() == con.IdCategoria).ToList();
                        if (docSub.Count() == 0)
                        {
                            sub.Mostrar = false;
                        }
                        else
                        {
                            sub.Mostrar = true;
                            foreach (var ds in docSub)
                            {
                                var doc = new InfoDocumentosViewModel();
                                doc.IdDocumento = ds.IdDocumento;
                                doc.Nombre = ds.Nombre;
                                doc.Fecha = ds.Fecha;
                                doc.clase = "ext_base";
                                if (!string.IsNullOrEmpty(ds.Ruta) && System.IO.File.Exists(ds.Ruta))
                                {
                                    var archivo = new FileInfo(ds.Ruta);
                                    var ext = archivo.Extension;
                                    var sinPunto = !string.IsNullOrEmpty(ext) ? ext.Substring(1) : "";
                                    var tam = archivo.Length > 0 ? Convert.ToString(Math.Round(archivo.Length / 1024f)) : "0";
                                    doc.Tamanio = tam + " kb";
                                    doc.Extension = sinPunto.ToUpper();
                                    doc.clase = !string.IsNullOrEmpty(sinPunto) ? "ext_" + sinPunto.ToLower() : "ext_base";
                                }
                                sub.Documentos.Add(doc);
                            }
                        }
                        con.SubCategorias.Add(sub);
                    }
                    model.Add(con);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }
            return View("Archivos/ArchivosElementos", model);
        }
    }
}
