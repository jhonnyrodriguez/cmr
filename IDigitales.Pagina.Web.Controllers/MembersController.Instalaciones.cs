﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Controllers.Helpers;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Utils;
using IDigitales.Pagina.Web.Data.Modelos;
using System.Data;
using IDigitales.Pagina.Web.Data.Consultas;
using System.Web;

namespace IDigitales.Pagina.Web.Controllers
{
    public partial class MembersController
    {
        public ActionResult InstalacionesSeccion()
        {
            return View("Instalaciones/InstalacionesSeccion");
        }
        public ActionResult Instalaciones(string alert)
        {
            ViewBag.Alert = alert;
            return View("Instalaciones/InstalacionesPrincipal");
        }

        public ActionResult InstalacionesHome()
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            List<Instalacion> instalaciones = new List<Instalacion>();
            object model = null;
            string view = null;

            try
            {
                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();
                var users = dbu.UsuariosRecupera();
                var user = users.FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                
                switch (user.Tipo)
                {
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Administrador:
                        instalaciones = db.InstalacionesRecupera();
                        model = new HomeAdminViewModel(instalaciones, user, users);
                        ViewBag.Admin = true;
                        view = "_HomeAdmin";
                        break;
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Desarrollo:
                        model = new HomeDevViewModel(user);
                        view = "_HomeDev";
                        break;
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Jefe:
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Servicio:
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Vendedor:
                    default:
                        instalaciones = db.InstalacionesRecupera(((CustomPrincipal)User).Id);
                        model = new HomeAdminViewModel(instalaciones, user, users);
                        view = "_HomeAdmin";
                        break;
                }

                ViewBag.Total = db.ObtenerReportes().Count;
                ViewBag.InstalacionesCount = instalaciones.Count;
                ViewBag.ExedenteDias = db.ObtenerConfiguracion().ProcesoExedenceTimeSpan.Days;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }
            
            return View("Instalaciones/" + view, model);
        }

        public ActionResult VistoBuenoList(int id)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            List<Reporte> model = null;

            try
            {
                if (((CustomPrincipal)User).Tipo != "Administrador")
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();
                var instalaciones = db.InstalacionesRecupera();
                var users = dbu.UsuariosRecupera();
                var user = users.FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                var instalacion = new HomeAdminViewModel(instalaciones, user, users).InstalacionesReportesVistoBueno.FirstOrDefault(i => i.Id == id);
                model = instalacion.Reportes;
                ViewBag.Instalacion = instalacion.Descripcion;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }

            ViewBag.Perfil = true;
            return View("Instalaciones/_VistoBuenoList", model);
        }

        public ActionResult DevPerfil(int id)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            object model = null;

            try
            {
                if (((CustomPrincipal)User).Tipo != "Administrador")
                    if (((CustomPrincipal)User).Id != id)
                        return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();
                var users = dbu.UsuariosRecupera();
                var user = users.FirstOrDefault(u => u.IdUsuario == id);
                model = new HomeDevViewModel(user);
                
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }

            ViewBag.Perfil = true;
            return View("Instalaciones/_HomeDev", model);
        }

        public ActionResult InstalacionesLista(string sortOrder)
        {
            ConsultasInstalaciones db = null;
            List<Instalacion> model = new List<Instalacion>();

            try
            {
                if (((CustomPrincipal)User).TienePermiso("INSTALACIONES_EDITAR"))
                    ViewBag.Admin = true;

                db = new ConsultasInstalaciones();

                if(((CustomPrincipal)User).Tipo == "Administrador")
                {
                    model = db.InstalacionesRecupera();
                }
                else
                {
                    model = db.InstalacionesRecupera(((CustomPrincipal)User).Id);
                }

                if (!((CustomPrincipal)User).TienePermiso("REPORTES_VER_TERCEROS"))
                {
                    foreach (var instalacion in model)
                    {
                        instalacion.Reportes = instalacion.Reportes.Where(r => r.QuienReportaId == ((CustomPrincipal)User).Id).ToList();
                    }
                }                    
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            return View("Instalaciones/_InstalacionesLista", model);
        }

        public ActionResult InstalacionesCrear()
        {
            ConsultasUsuarios db = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CREAR") && ((CustomPrincipal)User).Tipo != "Administrador")
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);
                db = new ConsultasUsuarios();
                ViewBag.Usuarios = db.UsuariosRecupera().Where(u => u.Tipo != Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Desarrollo && u.Tipo != Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Administrador);
            }
            catch
            {

            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            return View("Instalaciones/_InstalacionesCrear", new Instalacion());            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstalacionesCrear(string Tag, string Descripcion, List<int> usuarios)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            Instalacion nuevo = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CREAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                if (!string.IsNullOrWhiteSpace(Tag) && !string.IsNullOrWhiteSpace(Descripcion))
                {
                    db = new ConsultasInstalaciones();
                    dbu = new ConsultasUsuarios();

                    List<int> userIds = new List<int>();

                    if(usuarios != null)
                    {
                        foreach (var user in dbu.UsuariosRecupera().Where(u => usuarios.Contains(u.IdUsuario)))
                        {
                            userIds.Add(user.IdUsuario);
                        }
                    }                    

                    operacion = db.CrearInstalacion(new Instalacion(Tag, Descripcion, DateTime.Now), userIds);
                    if (operacion.Resultado == OperacionResultado.Ok)
                        nuevo = db.ObtenerInstalacion(operacion.Id);                                        
                }
                else
                {
                    operacion.Resultado = OperacionResultado.Error;
                    operacion.Mensaje = "Todos los campos son requeridos.";
                }
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = ex.Message;
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok && nuevo != null)
                return View("Instalaciones/_InstalacionesDetalles", nuevo);
            return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesEditar(int id)
        {
            ConsultasInstalaciones db = null;
            Instalacion model = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok, Mensaje = "Ocurrió un error al procesar la solicitud." };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_EDITAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                model = db.ObtenerInstalacion(id);
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Instalaciones/_InstalacionesEditar", model);
            return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstalacionesEditar([Bind(Include = "Id, FechaCreacion, Tag, Descripcion")]Instalacion instalacion)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            Instalacion nuevo = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_EDITAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                if (string.IsNullOrEmpty(instalacion.Tag) || string.IsNullOrEmpty(instalacion.Descripcion))
                {
                    operacion.Resultado = OperacionResultado.Error;
                    operacion.Mensaje = "Todos los campos son requeridos.";
                }
                else
                {
                    operacion = db.ActualizarInstalacion(instalacion);
                    if (operacion.Resultado == OperacionResultado.Ok)
                        nuevo = db.ObtenerInstalacion(operacion.Id);
                }
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok && nuevo != null)
                return View("Instalaciones/_InstalacionesDetalles", nuevo);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstalacionesEliminar([Bind(Include = "Id, FechaCreacion, Tag, Descripcion")]Instalacion instalacion)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            Instalacion nuevo = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_EDITAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                instalacion.Activo = false;
                resultado = db.ActualizarInstalacion(instalacion);
                if (resultado.Resultado == OperacionResultado.Ok)
                    nuevo = db.ObtenerInstalacion(resultado.Id);
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return RedirectToAction("Instalaciones");
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesEditarModulos(int id)
        {
            ConsultasInstalaciones db = null;
            Instalacion model = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok, Mensaje = "Ocurrió un error al procesar la solicitud." };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_EDITAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                model = db.ObtenerInstalacion(id);
                ViewBag.Categorias = db.ObtenerCategorias().ToList();
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Instalaciones/_InstalacionesEditarModulos", model);
            return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult InstalacionesEditarModulosAgregarQuitar(int id, int idModulo)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok, Mensaje = "Ocurrió un error al procesar la solicitud." };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_EDITAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                operacion = db.AgregarQuitarModuloInstalacion(id, idModulo);
            }
            catch(Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return RedirectToAction("InstalacionesEditarModulos", new { id });
            return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult InstalacionesConfiguracion()
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ConfiguracionInstalaciones model = new ConfiguracionInstalaciones();
            
            try
            {
                db = new ConsultasInstalaciones();
                ViewBag.Instalaciones = new SelectList(db.InstalacionesRecupera(((CustomPrincipal)User).Id), "Id", "Descripcion");
                ViewBag.Admin = ((CustomPrincipal)User).Tipo == "Administrador" ? true : false;
                model = db.ObtenerConfiguracion();
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Instalaciones/_InstalacionesConfiguracion", model);
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionRutas()
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ConfiguracionInstalaciones model = new ConfiguracionInstalaciones();

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                model = db.ObtenerConfiguracion();
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Instalaciones/_InstalacionesConfiguracionRutas", model);
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstalacionesConfiguracion(int inactividad, int exedente, string rutaEvidencias, string rutaReportes)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ConfiguracionInstalaciones nuevo = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                resultado = db.EditarConfiguracion(new ConfiguracionInstalaciones(inactividad, exedente, rutaEvidencias, rutaReportes));
                if (resultado.Resultado == OperacionResultado.Ok)
                    nuevo = db.ObtenerConfiguracion();
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok && nuevo != null)
                return RedirectToAction("InstalacionesConfiguracion");
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionImportar()
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            List<ReporteImportado> model = new List<ReporteImportado>();

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_IMPORTAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();                
                model = db.ObtenerReportesImportados();
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Instalaciones/_InstalacionesConfiguracionImportar", model);
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionImportarCrear()
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_IMPORTAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                ViewBag.Instalaciones = new SelectList(db.InstalacionesRecupera(), "Id", "Descripcion");
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            return View("Instalaciones/_InstalacionesConfiguracionImportarCrear");
        }

        public ActionResult InstalacionesConfiguracionModulos()
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            List<Aplicaciones> model = new List<Aplicaciones>();

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                model = db.ObtenerCategorias();
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Instalaciones/_InstalacionesConfiguracionModulos", model);
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionModuloEditar(int id)
        {
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            Modulo model = null;
            ConsultasInstalaciones db = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                model = db.ObtenerModulo(id);
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            if (resultado.Resultado == OperacionResultado.Ok && model != null)
                return View("Instalaciones/_InstalacionesConfiguracionModuloEditar", model);
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InstalacionesConfiguracionModuloEditar([Bind(Include = "Id, Subcategoria, Aliases")]Modulo modulo)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);
                               
                db = new ConsultasInstalaciones();
                resultado = db.ActualizarModulo(modulo);
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return RedirectToAction("InstalacionesConfiguracionModulos");
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionCategoriaEditar(int id)
        {
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            Aplicaciones model = null;
            ConsultasInstalaciones db = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                model = db.ObtenerCategoria(id);
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            if (resultado.Resultado == OperacionResultado.Ok && model != null)
                return View("Instalaciones/_InstalacionesConfiguracionCategoriaEditar", model);
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InstalacionesConfiguracionCategoriaEditar([Bind(Include = "IdAplicacion, TAG, Nombre, Version")]Aplicaciones categoria)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                resultado = db.ActualizarCategoria(categoria);
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return RedirectToAction("InstalacionesConfiguracionModulos");
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionModuloCrear(int categoriaId)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                ViewBag.CategoriaId = categoriaId;
                ViewBag.Categoria = db.ObtenerCategoria(categoriaId).Nombre;
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if(resultado.Resultado == OperacionResultado.Ok)
                return View("Instalaciones/_InstalacionesConfiguracionModuloCrear");
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InstalacionesConfiguracionModuloCrear([Bind(Include = "AplicacionesId, Subcategoria, Alias")]Modulo modulo)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                resultado = db.CrearModulo(modulo);                
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return RedirectToAction("InstalacionesConfiguracionModulos");
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionCategoriaCrear()
        {
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return View("Instalaciones/_InstalacionesConfiguracionCategoriaCrear");
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InstalacionesConfiguracionCategoriaCrear([Bind(Include = "TAG, Nombre, Version")]Aplicaciones categoria)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                resultado = db.CrearCategoria(categoria);
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return RedirectToAction("InstalacionesConfiguracionModulos");
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InstalacionesImportexcelStart(HttpPostedFileBase uploadFile, int? instalacionId)
        {
            ExcelViewStartViewModel model = new ExcelViewStartViewModel();
            ConsultasInstalaciones db = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_IMPORTAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                if (uploadFile != null && uploadFile.ContentLength > 0 && instalacionId.HasValue)
                {
                    string extension = Path.GetExtension(uploadFile.FileName).ToLower();
                    string[] validFileTypes = { ".xls", ".xlsx", ".csv" };

                    db = new ConsultasInstalaciones();
                    var now = DateTime.Now;
                    string fileName = uploadFile.FileName.ToLower().Replace(' ', '_').Replace(extension, "") + now.ToString("yyMMdd_HHmmss") + extension;
                    string path1 = string.Format("{0}/{1}", db.ObtenerConfiguracion().RutaReportesImportados, fileName);
                    if (!Directory.Exists(path1))
                    {
                        Directory.CreateDirectory(db.ObtenerConfiguracion().RutaReportesImportados);
                    }
                    if (validFileTypes.Contains(extension))
                    {
                        if (System.IO.File.Exists(path1))
                        { System.IO.File.Delete(path1); }
                        uploadFile.SaveAs(path1);                        
                        model = ExcelUtility.StartExcelView(path1, db);
                        model.ReporteImportado = new ReporteImportado
                        {
                            Nombre = uploadFile.FileName,
                            RutaOriginal = fileName,
                            InstalacionId = instalacionId.Value,                            
                        };

                        ViewBag.Categorias = db.ObtenerCategorias().ToList();
                    }
                    else
                    {
                        operacion.Resultado = OperacionResultado.Error;
                        operacion.Mensaje = "Formato desconocido.";
                    }
                }
                else
                {
                    operacion.Resultado = OperacionResultado.Error;
                    operacion.Mensaje = "Todos los campos son requeridos.";
                }
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ExcelViewStart", model);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
                
        }

        public ActionResult InstalacionesImportexcel(ExcelViewStartViewModel start)
        {
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ExcelViewViewModel model = new ExcelViewViewModel();
            ConsultasUsuarios dbu = null;
            ConsultasInstalaciones db = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_IMPORTAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                if (start.ReporteImportado.Nombre != null && start.ReporteImportado.RutaOriginal != null)
                {
                    db = new ConsultasInstalaciones();
                    string path1 = string.Format("{0}/{1}", db.ObtenerConfiguracion().RutaReportesImportados, start.ReporteImportado.RutaOriginal);
                    if (System.IO.File.Exists(path1))
                    {
                        model = ExcelUtility.ConvertXSLXtoDataTable(path1, start);
                        model.ReporteImportado = start.ReporteImportado;
                    }
                }
                else
                {
                    operacion.Resultado = OperacionResultado.Error;
                    operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                }

                dbu = new ConsultasUsuarios();
                ViewBag.Usuarios = new SelectList(dbu.UsuariosRecupera(), "IdUsuario", "Nombre");
            }
            catch(Exception ex)
            {
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (dbu != null)
                    dbu.Dispose();
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ExcelView", model);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesImportexcelRevisar(ExcelViewViewModel model)
        {
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_IMPORTAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                model = ExcelUtility.RevisarXSLXtoDataTable(model, null);
            }
            catch (Exception ex)
            {
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {

            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ExcelView", model);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InstalacionesImportexcelTerminar(ExcelViewViewModel model)
        {
            Dictionary<string, List<ResultadoOperacion>> result = new Dictionary<string, List<ResultadoOperacion>>();
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            ConsultasConfiguracion dbc = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_IMPORTAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                var modifications = new Dictionary<string, int?>();
                foreach (var usuarioPorCrear in model.UsuariosPorCrear.Where(u => u.UsuariosId.HasValue))
                {
                    modifications.Add(usuarioPorCrear.RawInput, usuarioPorCrear.UsuariosId);
                }
                model = ExcelUtility.RevisarXSLXtoDataTable(model, modifications);

                result["users"] = new List<ResultadoOperacion>();
                result["reports"] = new List<ResultadoOperacion>();

                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();                
                
                if(model.UsuariosPorCrear.Count > 0)
                {
                    dbc = new ConsultasConfiguracion();
                    var permisoDev = dbc.ObtenerPermisos().Where(p => p.Clave == "REPORTES_CAMBIAR_PORVALIDAR").ToList();
                    var permisoSer = dbc.ObtenerPermisos().Where(p => p.Clave == "REPORTES_CREAR").ToList();
                    var vista = dbc.ObtenerVistas().Where(v => v.Clave == "INSTALACIONES").ToList();
                    dbc.Dispose();
                    var instalacionId = model.Reportes.First().InstalacionId;
                    foreach (var usuario in model.UsuariosPorCrear)
                    {
                        var r = new ResultadoOperacion();
                        try
                        {                            
                            var permisos = usuario.Tipo == Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Desarrollo ?
                                permisoDev : permisoSer;
                            
                            Usuarios usuarioNuevo = new Usuarios
                            {
                                Nombre = usuario.Nombre,
                                Password = usuario.Password,
                                Activo = true,
                                Tipo = usuario.Tipo,
                                Permisos = permisos,
                                Vistas = vista,
                            };
                            r = dbu.CrearUsuario(usuarioNuevo, new List<int>());
                        }
                        catch(Exception ex)
                        {
                            r.Resultado = OperacionResultado.Error;
                            r.Mensaje = ex.Message;
                        }
                        
                        result["users"].Add(r);
                    }
                }
                
                var reportesValidos = model.Reportes.Where(r => r.IsValid && !r.RequiereAtencion && r.Confirmed);
                foreach (var reporte in reportesValidos)
                {
                    var r = new ResultadoOperacion();
                    try
                    {
                        var usuarios = dbu.UsuariosRecupera();
                        var quienReporta = usuarios.FirstOrDefault(u => u.Nombre == reporte.QuienReporta.Value.Nombre);

                        if (quienReporta == null)
                        {
                            if (modifications.ContainsKey(reporte.QuienReporta.Value.Nombre))
                            {
                                if (modifications[reporte.QuienReporta.Value.Nombre].HasValue)
                                    quienReporta = usuarios.FirstOrDefault(u => u.IdUsuario == modifications[reporte.QuienReporta.Value.Nombre]);
                            }
                        }

                        Usuarios asignado = null;
                        if (reporte.Asignado.Value != null)
                        {
                            asignado = usuarios.FirstOrDefault(u => u.Nombre == reporte.Asignado.Value.Nombre);
                            if (asignado == null)
                            {
                                if (modifications.ContainsKey(reporte.Asignado.Value.Nombre))
                                {
                                    if (modifications[reporte.Asignado.Value.Nombre].HasValue)
                                        asignado = usuarios.FirstOrDefault(u => u.IdUsuario == modifications[reporte.Asignado.Value.Nombre]);
                                }
                            }
                        }
                        Usuarios vistoBueno = null;
                        if (reporte.VistoBueno.Value != null)
                        {
                            vistoBueno = usuarios.FirstOrDefault(u => u.Nombre == reporte.VistoBueno.Value.Nombre);
                            if (asignado == null)
                            {
                                if (modifications.ContainsKey(reporte.VistoBueno.Value.Nombre))
                                {
                                    if (modifications[reporte.VistoBueno.Value.Nombre].HasValue)
                                        vistoBueno = usuarios.FirstOrDefault(u => u.IdUsuario == modifications[reporte.VistoBueno.Value.Nombre]);
                                }
                            }
                        }

                        if (quienReporta != null)
                        {
                            reporte.QuienReporta.Value.UsuariosId = quienReporta.IdUsuario;

                            if (reporte.Asignado.Value != null)
                                reporte.Asignado.Value.UsuariosId = asignado != null ? asignado.IdUsuario : (int?)null;
                            if (reporte.VistoBueno.Value != null)
                                reporte.VistoBueno.Value.UsuariosId = vistoBueno != null ? vistoBueno.IdUsuario : (int?)null;

                            dbu.AddProjectToUser(reporte.QuienReporta.Value.UsuariosId.Value, reporte.InstalacionId);
                            r = db.CrearReporte(new Reporte(reporte));
                            if (r.Resultado == OperacionResultado.Ok)
                            {
                                db.CrearComentario(new Comentario(r.Id, Comentario.LogEnum.ReporteImportado, ((CustomPrincipal)User).Id));
                                foreach (var comentario in reporte.Comentarios.Value)
                                {
                                    db.CrearComentario(new Comentario(r.Id, comentario, null));
                                }
                                foreach (var comentario in reporte.Links.Value)
                                {
                                    db.CrearComentario(new Comentario(r.Id, comentario, null));
                                }
                            }
                            else
                            {
                                r.Resultado = OperacionResultado.Error;
                            }
                        }
                        else
                        {
                            r.Resultado = OperacionResultado.Error;
                        }
                    }
                    catch
                    {
                        r.Resultado = OperacionResultado.Error;
                    }
                    result["reports"].Add(r);
                }
                reportesValidos = reportesValidos.Where(r => r.Confirmed);

                model.ReporteImportado.Estado = ReporteImportado.ImportadoEstadoEnum.Iniciado;
                model.ReporteImportado.FechaImportacion = DateTime.Now;
                model.ReporteImportado.ReportesImportadosCount = reportesValidos.Count();
                var resultadoOperacion = db.CrearReporteImportado(model.ReporteImportado);
                model.ReporteImportado.Id = resultadoOperacion.Id;
                model.ReporteImportado.SetReportesImportadosDict(reportesValidos);
                db.ActualizarReporteImportado(model.ReporteImportado);

                string rutaBase = db.ObtenerConfiguracion().RutaReportesImportados;
                Task.Run(() => ExcelUtility.EliminarLineasSobrantesExcel(model.ReporteImportado.Id, rutaBase));
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
                if (dbc != null)
                    dbc.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ExcelTerminar", result);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReintentaGenerarExcelResultado(int id)
        {
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ConsultasInstalaciones db = null;
            ReporteImportado model = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_IMPORTAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                ExcelUtility.EliminarLineasSobrantesExcel(id, db.ObtenerConfiguracion().RutaReportesImportados);
                model = db.ObtenerReportesImportados().FirstOrDefault(r => r.Id == id);
            }
            catch (Exception ex)
            {
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok && model != null)
                return View("Instalaciones/_InstalacionesConfiguracionImportarItem", model);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenerExcel(string ruta)
        {
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ConsultasInstalaciones db = null;
            byte[] filedata = null;
            string contentType = null;

            try
            {
                db = new ConsultasInstalaciones();
                var filepath = db.ObtenerConfiguracion().RutaReportesImportados + ruta;
                filedata = System.IO.File.ReadAllBytes(filepath);
                contentType = MimeMapping.GetMimeMapping(filepath);

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = ruta,
                    Inline = true,
                };
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok && filedata != null)
                return File(filedata, contentType, ruta);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionUsuarios()
        {
            ConsultasUsuarios dbu = null;
            ConsultasInstalaciones db = null;
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            List<Usuarios> model = new List<Usuarios>();

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_CONFIGURACION"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                dbu = new ConsultasUsuarios();
                db = new ConsultasInstalaciones();
                model = dbu.UsuariosRecupera();
                ViewBag.Proyectos = db.InstalacionesRecupera();
            }
            catch (Exception ex)
            {
                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (dbu != null)
                    dbu.Dispose();
            }

            if (model != null)
                return View("Instalaciones/_InstalacionesConfiguracionUsuarios", model);
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionUsuarioEditarEmail(int id, string email)
        {
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ConsultasUsuarios db = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_EDITAR"))
                    if(((CustomPrincipal)User).Id != id)
                        return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                if (string.IsNullOrEmpty(email))
                {
                    return Json(new { error = true, message = "Proporciona un nuevo email" }, JsonRequestBehavior.AllowGet);
                }

                db = new ConsultasUsuarios();
                var user = db.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == id);
                if(user != null)
                {
                    if(db.CambiarEmail(user, email).Resultado == OperacionResultado.Error)
                    {
                        operacion.Resultado = OperacionResultado.Error;
                        operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                    }
                }
                else
                {
                    operacion.Resultado = OperacionResultado.Error;
                    operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                }
            }
            catch (Exception ex)
            {
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Accepted);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionUsuarioAgregarQuitarProyecto(int idUser, int idProyecto)
        {
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ConsultasUsuarios db = null;

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("INSTALACIONES_EDITAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);
                
                db = new ConsultasUsuarios();
                var user = db.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == idUser);
                if (user != null)
                {
                    if (db.QuitarAgregarProyecto(user, idProyecto).Resultado == OperacionResultado.Error)
                    {
                        operacion.Resultado = OperacionResultado.Error;
                        operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                    }
                }
                else
                {
                    operacion.Resultado = OperacionResultado.Error;
                    operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                }
            }
            catch (Exception ex)
            {
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return RedirectToAction("InstalacionesConfiguracionUsuarios");
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InstalacionesConfiguracionNotifications()
        {
            ConfiguracionNotificationsViewModel model = null;
            ConsultasUsuarios db = null;

            try
            {                
                db = new ConsultasUsuarios();
                var user = db.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                var configuration = user.UsuariosNotificationsConfiguration;
                if (configuration == null)
                {
                    configuration = new UsuariosNotificationsConfiguration();
                    db.EditarConfiguracionNotificaciones(user, configuration.NotificationConfiguration);
                    user = db.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                    configuration = user.UsuariosNotificationsConfiguration;
                }
                model = new ConfiguracionNotificationsViewModel(configuration);
                ViewBag.User = user;
            }
            catch(Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if(model != null)
                return View("Instalaciones/_InstalacionesConfiguracionNotifications", model);
            else return Json(new { error = true, message = "Error al procesar la solicitud." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InstalacionesConfiguracionNotifications(ConfiguracionNotificationsViewModel input)
        {
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Error };
            ConsultasUsuarios db = null;

            try
            {
                db = new ConsultasUsuarios();
                operacion = db.EditarConfiguracionNotificaciones(db.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id), input.Flag);
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return RedirectToAction("InstalacionesConfiguracionNotifications");
            else return Json(new { error = true, message = "Error al procesar la solicitud." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CategoriasLista()
        {
            ConsultasInstalaciones db = null;
            List<Aplicaciones> model = null;

            try
            {
                db = new ConsultasInstalaciones();
                model = db.ObtenerCategorias();
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
                model = null;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Instalaciones/_CategoriasLista", model);
            else return Json(new { error = true, message = "Error al procesar la solicitud." }, JsonRequestBehavior.AllowGet);
        }

        #region Instalaciones modalGantt

        public ActionResult Gantt(int idProyecto)
        {
            //GridResult<Reporte> data = null;
            
            //try
            //{
               
            //    if (idProyecto>0)
            //    {
            //        using (var con = new ConsultasInstalaciones())
            //        {                        
            //            data = con.ObtenerReportesProyecto(idProyecto);
            //        }
            //    }
            //    else
            //    {                  
            //        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError,"Sin elementos.");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    data.error = ex.Message;
            //    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError, "Ocurrió un error al procesar la solicitud.");
            //}            

            return View("Instalaciones/Gantt/InstalacionesGanttPrincipal", idProyecto);
            
        }
        public async Task<ActionResult> ObtenerReportesProyecto(int idProyecto)
        {
            GridResult<Reporte> data = null;

            try
            {

                if (idProyecto > 0)
                {
                    using (var con = new ConsultasInstalaciones())
                    {
                        data = await con.ObtenerReportesProyecto(idProyecto);
                    }
                    data.QuitarReferenciasCirculares(new List<string> { "ReporteSucesores", "DesarrolladorAsignado"});
                }
                else
                {
                    return Json(new { error = true, message = "Sin elementos." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                data.error = ex.Message;
                return Json(new { error = true, message = "Error al procesar la solicitud." }, JsonRequestBehavior.AllowGet);
            }
            
             var jsonResult=Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public async Task<ActionResult> ActualizarReporteProyecto()
        {
            ResultadoOperacion resultado = new ResultadoOperacion { Resultado = OperacionResultado.Error };

            try
            {
                var form = Request.Form;
                var id = Convert.ToInt32(form.Get("key"));
                var values = form.Get("values");
                var json = values.Replace("\"", "'").Replace("[", "").Replace("]", "");
                using (var con = new ConsultasInstalaciones())
                {
                    resultado = await con.ActualizarReporteProyecto(id, json);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
