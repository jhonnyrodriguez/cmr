﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Modelos;
using IDigitales.Pagina.Web.Data.Utils; 

namespace IDigitales.Pagina.Web.Controllers
{
    public partial class MembersController
    {

        #region Cotizaciones
        public ActionResult Cotizaciones()
        {
            return View("Cotizaciones/CotizacionesPrincipal");
        }

        public async Task<ActionResult> CargarCotizacion(int idCotizacion)
        {
            Cotizaciones cot = new Cotizaciones();
            return PartialView("Cotizaciones/CotizacionesContenido", cot);
        }

        #region Modal seleccion productos
        public ActionResult SeleccionarProductos(int idListaPrecios)
        {
            return View("Cotizaciones/ModalSeleccionProductos");
        }

        public async Task<ActionResult> ObtenerProductosListas(DataTablaPaginacion pag)
        {
            ConsultasProductos consulta = null;
            DataTablaResult<ProductosModel> data = null;
            try
            {
                using (var con = new ConsultasCotizaciones())
                {
                    data = await con.ObtenerProductosListas(pag);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion




        #region Modal Operaciones
        public ActionResult ModalGuardarCotizacion()
        {
            return View("Cotizaciones/ModalOperacion");
        }
        public async Task<ActionResult> AgregarActualizarCotizacion(Cotizaciones cot)
        {
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            try
            {
                using (var con = new ConsultasCotizaciones())
                {
                    result = await con.AgregarActualizarCotizacion(cot);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }

        public async Task<ActionResult> AbrirCotizacion(int idCotizacion)
        {
            CotizacionModel cot = null;
            try
            {
                if (idCotizacion > 0)
                {
                    using (var con = new ConsultasCotizaciones())
                    {
                        cot = await con.ObtenerCotizacion(idCotizacion);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(cot);
        }

        public ActionResult ImpresionCotizacion()
        {
            return PartialView("Cotizaciones/ModalImprimirCotizacion");
        }

        public ActionResult GenerarReporteCotizacion(Cotizaciones cot, string nombreCliente, string nombreEmpresa,
                                                           string nombreContacto, string emailContacto, int compañia)
        {
            var reporte = new ReportesController();
            var rutaPlantilla = "";
            if (compañia==1)
            { 
                rutaPlantilla = Server.MapPath(reporte.ObtenerRutaPlantillas() + "ReporteCotizacionEymsa.repx"); 
            }
            else {
                rutaPlantilla = Server.MapPath(reporte.ObtenerRutaPlantillas() + "ReporteCotizacionCMR.repx");
            }
            //Obtiene datos
            var datos = new List<ReporteCotizacionViewModel>();
            var cotizacion = new ReporteCotizacionViewModel();
            cotizacion.NombreCliente = nombreCliente.ToUpper();
            cotizacion.NombreEmpresa = nombreEmpresa.ToUpper();
            cotizacion.NombreContacto = nombreContacto.ToUpper();
            cotizacion.EmailContacto = emailContacto;
            cotizacion.Productos = new List<InformacionCotizacionViewModel>();
              
            if (cot != null && cot.CotizacionesProductos != null && cot.CotizacionesProductos.Count > 0)
            {
                foreach (var item in cot.CotizacionesProductos)
                {
                    using (var con = new ConsultasCotizaciones())
                    {
                        var prod = new InformacionCotizacionViewModel();
                        var infoProducto = con.ObtenerInfoProducto(item.IdProducto);                                          
                        if (infoProducto!=null)
                        {
                            prod.Producto = infoProducto.Nombre;
                            prod.Precio = infoProducto.ListasPrecioProductos.FirstOrDefault(x => x.IdLista == cot.IdLista) !=null?
                                          infoProducto.ListasPrecioProductos.FirstOrDefault(x => x.IdLista == cot.IdLista).PrecioFinal?? infoProducto.Precio: infoProducto.Precio;
                            prod.Precio =  prod.Precio / (Decimal)0.75;
                            prod.Importe = prod.Precio * item.Cantidad;   
                        }                        
                        prod.Cantidad = item.Cantidad;
                        prod.PorcentajeDescuento = item.PorcentajeDescuento != null ? item.PorcentajeDescuento.Value : 0;
                        prod.Descuento = item.PorcentajeDescuento != null ? prod.Importe * (decimal)(prod.PorcentajeDescuento / 100) : 0;
                        prod.Subtotal = (prod.Importe - prod.Descuento);
                        prod.IVA = (double)prod.Subtotal * 0.16;
                        prod.Total = (double)prod.Subtotal + prod.IVA;
                        cotizacion.Productos.Add(prod);
                    }
                }
                cotizacion.Descuentos = cotizacion.Productos.Sum(x => x.Descuento);
                cotizacion.Descuentos += cot.PorcentajeDescuento != null ?
                                                cotizacion.Productos.Sum(x => x.Subtotal) * (decimal)cot.PorcentajeDescuento.Value / 100 : 0;
                cotizacion.Total = (decimal)cotizacion.Productos.Sum(x => x.Importe) - cotizacion.Descuentos
                                    + (decimal)cotizacion.Productos.Sum(x => x.IVA);
            }          

            datos.Add(cotizacion); 
            //Genera reporte
            string ruta = reporte.Generar(datos, rutaPlantilla,"ReporteCotizacion","pdf",true);
            ruta = ruta.Replace("\\", "\\\\");
            return Json(ruta);               
        }

        public ActionResult ReporteCotizacion(string ruta)
        {
            return File(ruta, "application/pdf");
        }

        #endregion

        #endregion


        
    }
}
