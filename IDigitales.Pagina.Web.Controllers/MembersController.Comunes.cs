﻿using IDigitales.Pagina.Web.Data.Modelos;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Utils;
using IDigitales.Comunes.Librerias.IO;
using System.ComponentModel;

namespace IDigitales.Pagina.Web.Controllers
{
    public partial class  MembersController
    {
        #region Funciones Generales
        public bool CrearDirectorio(string ruta)
        {
            bool resultado = false;

            try
            {
                var originalDirectory = new DirectoryInfo(ruta);
                var directorio = originalDirectory.ToString();

                Directorios.CreaCarpeta(directorio, true);
                resultado = true;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
                resultado = false;
            }
            return resultado;
        }
        private string obtenerNombreNorepetido(string directorio, ref string file, int num)
        {
            var rutanueva = Path.Combine(directorio, file);
            var nArchivo = file;
            while (System.IO.File.Exists(rutanueva))
            {
                var ini = file.LastIndexOf(".", System.StringComparison.Ordinal);
                var nom = file.Substring(0, ini);
                var ext = file.Substring(ini, file.Length - ini);
                num++;
                nArchivo = nom + "_" + num + ext;
                rutanueva = directorio + nArchivo;
            }
            file = nArchivo;
            return rutanueva;
        }
        public static string ObtenerDescriptionEnum(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])
                             fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
        public static List<Catalogos> ObtenerCatalogoEnum<T>()
        {
            try
            {
                var catalogos = (from Enum valor in Enum.GetValues(typeof(T))
                                 select new Catalogos { Id = Convert.ToInt32(valor).ToString(), Valor = ObtenerDescriptionEnum(valor), Elemento = valor }).ToList();
                return catalogos;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return null;
        }
        public List<Catalogos> ObtenerCatalogos(string catalogo, string idFiltro)
        {
            var lista = new List<Catalogos>();
            try
            {
                int id;
                int.TryParse(idFiltro, out id);

                using (var con = new ConsultasComunes())
                {
                    switch (catalogo)
                    {
                        case "CatalogoCategorias":
                            lista = ObtenerCatalogoEnum<EnumDocumentosCategoria>();
                            break;
                        case "CatalogoSubCategorias":
                            lista = ObtenerCatalogoEnum<EnumDocumentosSunCategoria>();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return lista;
        }
        public string ObtieneImagenDocumento(int idDocumento)
        {
            var fotoBase64 = "";
            try
            {
                using (var con = new ConsultasComunes())
                {
                    var ruta = con.ObtenerImagenDocumento(idDocumento);
                    if (!string.IsNullOrEmpty(ruta))
                    {
                        var formato = Path.GetExtension(ruta);
                        if (System.IO.File.Exists(ruta))
                        {
                            using (
                                var sourceStream = new FileStream(ruta, FileMode.Open, FileAccess.Read, FileShare.Read,
                                    bufferSize: 4096, useAsync: true))
                            {
                                byte[] archivo = new byte[sourceStream.Length];
                                sourceStream.ReadAsync(archivo, 0, (int)sourceStream.Length);
                                if (archivo.Length > 0)
                                    fotoBase64 = string.Format("data:{0};base64,{1}", formato,
                                        Convert.ToBase64String(archivo));
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return fotoBase64;
        }

        public FileResult DescargarDocumento(int idDocumento)
        {
            ConsultasComunes consulta = null;
            byte[] archivo = null;
            string mime = string.Empty;
            string nombre = string.Empty;
            try
            {
                consulta = new ConsultasComunes();
                var documento = consulta.ObtenerDocumento(idDocumento);
                if (documento != null)
                {
                    if (!string.IsNullOrEmpty(documento.Ruta) && System.IO.File.Exists(documento.Ruta))
                    {
                        archivo = System.IO.File.ReadAllBytes(documento.Ruta);
                        var ext = System.IO.Path.GetExtension(documento.Ruta);
                        nombre = System.IO.Path.GetFileName(documento.Ruta);
                        if (ext == ".zip")
                        {
                            mime = "application/zip";
                        }
                        else if (ext == ".rar")
                        {
                            mime = "application/rar";
                        }
                        else
                        {
                            mime = MimeMapping.GetMimeMapping(documento.Ruta);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return File(archivo, mime, nombre);
        }
        #endregion

        #region Documentos
        public ActionResult ObtenerCatalogo(string catalogo, string idFiltro)
        {
            var lista = new List<Catalogos>();
            try
            {
                lista = ObtenerCatalogos(catalogo, idFiltro);
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RegistrarDocumentos()
        {
            return View("Comunes/Documentos/RegistrarDocumentos");
        }
        public ActionResult SaveUploadedFile()
        {
            var fName = "";
            var path = "";
            ConsultasComunes consulta = null;
            try
            {
                consulta = new ConsultasComunes();
                //Obtener Ruta Documentos
                var dirRel = consulta.ObtenerRutaDocumentos();
                //Obtener Categoria
                var dir = dirRel; // + (!string.IsNullOrEmpty(Categoria) ? Categoria + "\\":"");
                if (CrearDirectorio(dir))
                {
                    foreach (string fileName in Request.Files)
                    {
                        path = "";
                        var file = Request.Files[fileName];
                        //Save file content goes here
                        if (file != null)
                        {
                            fName = file.FileName;
                            if (file.ContentLength > 0)
                            {
                                path = obtenerNombreNorepetido(dir, ref fName, 0);
                                file.SaveAs(path);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }            

            return Json(new { Message = fName, Nombre = fName, Ruta = path });
        }
        public ActionResult RemoveUploadedFile(string ruta, string archivo)
        {
            if (!string.IsNullOrEmpty(ruta))
            {
                var nombreArchivo = ruta;
                if (!string.IsNullOrEmpty(archivo) && !ruta.Contains(archivo))
                    nombreArchivo = ruta + "\\" + archivo;
                if (System.IO.File.Exists(nombreArchivo))
                {
                    System.IO.File.Delete(nombreArchivo);
                }

                var folder = new DirectoryInfo(ruta);
                if (folder.Exists)
                {
                    if (folder.GetFileSystemInfos().Length == 0)
                    {
                        folder.Delete();
                    }
                }
            }
            return Json(new { Message = "Archivo removido satisfactoriamente" });
        }
        public ActionResult GuardarDocumentosBD(List<DocumentosBDViewModel> archivos)
        {
            ConsultasComunes consulta = null;
            List<int> guardados = new List<int>();
            try
            {
                consulta = new ConsultasComunes();
                var docs = new List<Documentos>();
                foreach (var a in archivos)
                {
                    var d = new Documentos();
                    d.Nombre = a.Nombre;
                    d.Descripcion = a.Descripcion;
                    if (a.Categoria != 0)
                        d.Categoria = (EnumDocumentosCategoria)Enum.Parse(typeof(EnumDocumentosCategoria), a.Categoria.ToString(), true);
                    if (a.SubCategoria != 0)
                        d.SubCategoria = (EnumDocumentosSunCategoria)Enum.Parse(typeof(EnumDocumentosSunCategoria), a.SubCategoria.ToString(), true);
                    d.Ruta = a.Ruta;
                    if(a.Dia!=0 && a.Mes != 0 && a.Anio != 0)
                    {
                        d.Fecha = new DateTime(a.Anio, a.Mes, a.Dia, a.Hrs, a.Min, 0);
                    }
                    else
                    {
                        d.Fecha = DateTime.Now;
                    }                    

                    docs.Add(d);
                }
                var docsGuardados = consulta.GuardarDocumentosBD(docs);
                guardados = docsGuardados.Where(x => x.Fecha != null).OrderBy(x => x.Fecha).Select(x => x.Fecha.Value.Year).Distinct().ToList();
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(guardados, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EliminarDocumentosBD(List<int> documentos)
        {
            ConsultasComunes consulta = null;
            var error = -1;
            try
            {
                consulta = new ConsultasComunes();
                foreach (var d in documentos)
                {
                    var ruta = consulta.EliminarDocumentoBD(d);
                    if(!string.IsNullOrEmpty(ruta))
                        RemoveUploadedFile(ruta,"");
                }
                error = 0;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return Json(error, JsonRequestBehavior.AllowGet);
        }
        #endregion   
    }
}
