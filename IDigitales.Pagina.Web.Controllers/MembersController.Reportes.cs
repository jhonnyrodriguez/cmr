﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Controllers.Helpers;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Utils;
using IDigitales.Pagina.Web.Data.Modelos;
using System.Data;
using IDigitales.Pagina.Web.Data.Consultas;
using System.Web;
using static IDigitales.Pagina.Web.Data.Entidades.Poco.UsuariosNotificationsConfiguration;

namespace IDigitales.Pagina.Web.Controllers
{
    partial class MembersController : BaseController
    {
        public ActionResult Reportes()
        {
            return View();
        }

        public ActionResult ReportesTable(int? proyectoId)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ReportesTableViewModel model = null;
            
            try
            {
                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();
                List<Instalacion> proyectos = new List<Instalacion>();

                var users = dbu.UsuariosRecupera();
                var user = users.FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);

                if (proyectoId.HasValue)
                {
                    var p = db.ObtenerInstalacion(proyectoId.Value);
                    if (user.Tipo == Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Administrador)
                        proyectos.Add(p);
                    else
                    {
                        if (user.TienePermiso(proyectoId.Value))
                            proyectos.Add(p);
                        else return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if(user.Tipo == Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Administrador)
                        proyectos = db.InstalacionesRecupera();
                    else proyectos = db.InstalacionesRecupera(user.IdUsuario);
                }

                ViewBag.Admin = user.Tipo == Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Administrador;
                ViewBag.ExedenteDias = db.ObtenerConfiguracion().ProcesoExedenceTimeSpan.Days;
                model = new ReportesTableViewModel(proyectos, user, users, user.ReportesCategorias.ToList());
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ReportesTable", model);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportesListaContainer(int? id, ReportesListaViewModel model)
        {
            ConsultasUsuarios dbu = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                dbu = new ConsultasUsuarios();
                if (id.HasValue)
                {
                    model = new ReportesListaViewModel();
                    model.InstalacionId = id.Value;
                    model.CategoriaId = null;
                }
                else
                {
                    ViewBag.AdvancedSearch = true;
                }
                var user = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                ViewBag.Categorias = new SelectList(user.ReportesCategorias, "Id", "Nombre");
                ViewBag.Estados = new SelectList(from Reporte.EstadoEnum d in Enum.GetValues(typeof(Reporte.EstadoEnum))
                                                 select new { Id = (int)d, Name = Reporte.EstadoDisplay(d) }, "Id", "Name");
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (dbu != null)
                    dbu.Dispose();
            }

            if(operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ReportesListaContainer", model);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportesLista(ReportesListaViewModel model)
        {
            ConsultasInstalaciones db = null;
            List<Reporte> reportes = new List<Reporte>();
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                db = new ConsultasInstalaciones();
                reportes = db.ObtenerReportes();

                if (!((CustomPrincipal)User).TienePermiso("REPORTES_VER_TERCEROS"))
                    reportes = reportes.Where(r => r.QuienReportaId == ((CustomPrincipal)User).Id).ToList();

                //instalacion
                if (model.InstalacionId.HasValue)
                    reportes = reportes.Where(r => r.InstalacionId == model.InstalacionId).ToList();

                //categoria
                if (model.CategoriaId != null)
                    reportes = reportes.Where(r => r.CategoriaId.HasValue && r.CategoriaId == model.CategoriaId).ToList();

                //prioridad
                if (model.Prioridad.HasValue)
                    reportes = reportes.Where(r => r.Prioridad == model.Prioridad).ToList();

                //estado
                if (model.Estado.HasValue)
                    reportes = reportes.Where(r => r.Estado == model.Estado).ToList();

                //búsqueda
                if (model.Busqueda != null)
                {
                    reportes = reportes.Where(r =>
                    (r.Descripcion == null ? false : r.Descripcion.ToLower().Contains(model.Busqueda.ToLower())) ||
                    (r.DesarrolladorAsignado == null ? false : r.DesarrolladorAsignado.Nombre.ToLower().Contains(model.Busqueda.ToLower())) ||
                    (r.QuienReporta == null ? false : r.QuienReporta.Nombre.ToLower().Contains(model.Busqueda.ToLower())) ||
                    (r.VistoBueno == null ? false : r.VistoBueno.Nombre.ToLower().Contains(model.Busqueda.ToLower())) ||
                    (r.Instalacion == null ? false : r.Instalacion.Tag.ToLower().Contains(model.Busqueda.ToLower())) ||
                    (r.VersionDeCorreccion == null ? false : r.VersionDeCorreccion.Version.ToLower().Contains(model.Busqueda.ToLower())) ||
                    r.Prioridad.ToString().ToLower().Contains(model.Busqueda.ToLower()) ||
                    r.Estado.ToString().ToLower().Contains(model.Busqueda.ToLower())
                    ).ToList();
                }

                model.Reportes = reportes;
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            if(operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ReportesLista", reportes);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportesCrear(int? instalacionId, bool? backToHome)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_CREAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();

                int userId = ((CustomPrincipal)User).Id;
                var instalaciones = new List<Instalacion>();
                var user = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);

                switch (user.Tipo)
                {
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Administrador:
                        instalaciones = db.InstalacionesRecupera();
                        break;
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Desarrollo:
                        instalaciones = db.InstalacionesRecupera();
                        break;
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Jefe:
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Servicio:
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Vendedor:
                    default:
                        instalaciones = db.InstalacionesRecupera(((CustomPrincipal)User).Id);
                        break;
                }

                ViewBag.Id = instalacionId;
                if(instalacionId.HasValue)
                    ViewBag.InstalacionesList = new SelectList(instalaciones, "Id", "Descripcion", instalaciones.FirstOrDefault(i => i.Id == instalacionId));
                else if(instalaciones.Count == 1)
                    ViewBag.InstalacionesList = new SelectList(instalaciones, "Id", "Descripcion", instalaciones.FirstOrDefault());
                else ViewBag.InstalacionesList = new SelectList(instalaciones, "Id", "Descripcion");
                ViewBag.UsersList = new SelectList(dbu.UsuariosRecupera(), "IdUsuario", "Nombre");
                ViewBag.Username = ((CustomPrincipal)User).Nombre;
                ViewBag.BackToHome = backToHome;
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ReportesCrear", new Reporte { Prioridad = Reporte.PrioridadEnum.Media });
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportesCrearCategoria(int instalacionId)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            SelectList model = null;
            string msg = "";

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_CREAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();
                var instalacion = db.ObtenerInstalacion(instalacionId);

                var user = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                List<ReportesCategorias> categorias = new List<ReportesCategorias>();
                switch (user.Tipo)
                {
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Administrador:
                        categorias = instalacion.ReportesCategorias;
                        break;
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Desarrollo:
                        categorias = instalacion.ReportesCategorias.Where(u => u.Usuarios.FirstOrDefault(i => i.IdUsuario == user.IdUsuario) != null).ToList();
                        break;
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Jefe:
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Servicio:
                    case Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Vendedor:
                    default:
                        categorias = instalacion.ReportesCategorias.Where(u => u.Usuarios.FirstOrDefault(i => i.IdUsuario == user.IdUsuario) != null).ToList();
                        break;
                }

                model = new SelectList(categorias, "Id", "Nombre");
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }

            if (model != null)
                return View("Reportes/_ReportesCrearCategoria", model);
            else return Json(new { error = true, message = msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportesCrearAplicacion(int categoriaId, int categoriaInstalacionId)
        {
            ConsultasInstalaciones db = null;
            SelectList model = null;
            string msg = "";

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_CREAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                var categoria = db.ObtenerReportesCategoria(categoriaId);

                ViewBag.EsAplicacion = categoria.EsAplicacion;
                var modulos = db.ObtenerModulos().ToList();
                List<Aplicaciones> aplicaciones = new List<Aplicaciones>();
                foreach(var modulo in modulos.Where(m => m.Instalacion.FirstOrDefault(i => i.Id == categoriaInstalacionId) != null))
                {
                    if(aplicaciones.FirstOrDefault(i => i.IdAplicacion == modulo.AplicacionesId) == null && modulo.Aplicaciones != null)
                    {
                        aplicaciones.Add(modulo.Aplicaciones);
                    }
                }

                model = new SelectList(aplicaciones, "IdAplicacion", "Nombre");
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Reportes/_ReportesCrearAplicacion", model);
            else return Json(new { error = true, message = msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportesCrearModulo(int aplicacionId, int moduloInstalacionId)
        {
            ConsultasInstalaciones db = null;
            SelectList model = null;
            string msg = "";

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_CREAR"))
                    return Json(new { error = true, message = "Permisos insuficientes." }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                var categoria = db.ObtenerCategoria(aplicacionId);
                model = new SelectList(categoria.Modulos.Where(m => m.Instalacion.FirstOrDefault(i => i.Id == moduloInstalacionId) != null), "Id", "Subcategoria");
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Reportes/_ReportesCrearModulo", model);
            else return Json(new { error = true, message = msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportesCrearVersion(int moduloId)
        {
            ConsultasInstalaciones db = null;
            SelectList model = null;
            string msg = "";

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_CREAR"))
                    return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                var modulo = db.ObtenerModulo(moduloId);
                model = new SelectList(modulo.Aplicaciones.AplicacionesVersiones, "IdVersion", "Version");
            }
            catch(Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (model != null)
                return View("Reportes/_ReportesCrearVersion", model);
            else return Json(new { error = true, message = msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReportesCrear([Bind(Include = "Descripcion, Prioridad, InstalacionId, ModuloId, Titulo, VersionActualId, CategoriaId, NumeroSerie")]Reporte reporte, bool? esRequerimiento, HttpPostedFileBase uploadFile)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            ResultadoOperacion result = new ResultadoOperacion { Resultado = OperacionResultado.Ok };
            ResultadoOperacion resultComment = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            try
            {
                if (!((CustomPrincipal)User).TienePermiso("REPORTES_CREAR"))
                    return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);

                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();

                if (reporte.EsValidoParaCrear.Resultado != OperacionResultado.Ok)
                {
                    return Json(new { error = true, message = reporte.EsValidoParaCrear.Mensaje }, JsonRequestBehavior.AllowGet);
                }

                var usuario = (CustomPrincipal)User;

                reporte = new Reporte(reporte.Titulo, reporte.NumeroSerie, reporte.VersionActualId, reporte.Descripcion, reporte.Prioridad,
                        reporte.ModuloId, reporte.InstalacionId, reporte.CategoriaId.Value, usuario.Id, esRequerimiento ?? false);
                result = db.CrearReporte(reporte);

                if(result.Resultado != OperacionResultado.Ok)
                {
                    return Json(new { error = true, message = result.Mensaje }, JsonRequestBehavior.AllowGet);
                }

                //El reporte se creó.
                ViewBag.Id = result.Id;

                //Comentario de inicio de reporte.
                var comment = new Comentario(result.Id, Comentario.LogEnum.ReporteCreado, usuario.Id);
                resultComment = db.CrearComentario(comment);                
                    
                if (resultComment.Resultado != OperacionResultado.Ok)
                {
                    //Algo salió mal, elimina el reporte creado.
                    db.EliminarReporte(result.Id);
                    return Json(new { error = true, message = "Ocurrió un error al procesar la solicitud." }, JsonRequestBehavior.AllowGet);
                }

                //agrega el archivo, si existe                
                if (uploadFile != null)
                {
                    var result2 = db.CrearComentario(new Comentario(result.Id, "", ((CustomPrincipal)User).Id));
                    var comment2 = db.ObtenerComentario(result2.Id);
                    string file = Evidencias.SaveFile(db.ObtenerConfiguracion().RutaEvidencias, uploadFile, comment2.Id.ToString());
                    comment2.ArchivoRutaRelativa = file;
                    db.ActualizarComentario(comment2);
                }

                var instalaciones = db.InstalacionesRecupera(usuario.Id);
                ViewBag.InstalacionesList = new SelectList(instalaciones, "Id", "Descripcion", instalaciones.Find(i => i.Id == reporte.Id));
                ViewBag.UsersList = new SelectList(dbu.UsuariosRecupera(), "IdUsuario", "Nombre");
                ViewBag.Username = ((CustomPrincipal)User).Nombre;


                var usuarioEntity = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                Notifications.NotifyAll(reporte, NotificationsEnum.Creacion, usuarioEntity);


            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
                result.Resultado = OperacionResultado.Error;
                result.Mensaje = ex.Message;

                //Algo salió mal, elimina el reporte creado.
                db.EliminarComentario(resultComment.Id);
                db.EliminarReporte(result.Id);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (result.Resultado == OperacionResultado.Ok)
                return RedirectToAction("ReportesDetalle", new { id = result.Id, backToHome = true });
            else return Json(new { error = true, message = result.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportesDetalle(int id, bool? backToHome)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;

            Reporte model = new Reporte();
            try
            {
                dbu = new ConsultasUsuarios();
                db = new ConsultasInstalaciones();
                model = db.ObtenerReporte(id);
                                
                if(model.QuienReportaId != ((CustomPrincipal)User).Id && ((CustomPrincipal)User).Tipo != "Desarrollo")
                    if (!((CustomPrincipal)User).TienePermiso("REPORTES_VER_TERCEROS"))
                        return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);

                ViewBag.Permisos = ((CustomPrincipal)User).Permisos;
                ViewBag.User = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                ViewBag.UsersList = new SelectList(dbu.UsuariosRecupera(), "IdUsuario", "Nombre");
                ViewBag.BackToHome = backToHome;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            return View("Reportes/_ReportesDetalle", model);
        }

        public ActionResult ReporteComentariosLista(int id)
        {
            ConsultasInstalaciones db = null;
            IEnumerable<Comentario> model;
            try
            {
                db = new ConsultasInstalaciones();
                var reporte = db.ObtenerReporte(id);

                if (reporte.QuienReportaId != ((CustomPrincipal)User).Id && ((CustomPrincipal)User).Tipo != "Desarrollo")
                    if (!((CustomPrincipal)User).TienePermiso("REPORTES_VER_TERCEROS"))
                        return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);

                model = reporte.Comentarios;
            }
            catch (Exception ex)
            {
                model = null;
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if(model != null)
                return View("Reportes/_ComentariosLista", model);
            return Json(new { error = true, message = "Error al procesar la solicitud 'ComentariosLista'" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ComentarioCrear(int reporteId, string mensaje, HttpPostedFileBase uploadFile)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            Comentario comentario = null;
            ResultadoOperacion resultado = new ResultadoOperacion();

            try
            {
                db = new ConsultasInstalaciones();
                var reporte = db.ObtenerReporte(reporteId);

                if (!((CustomPrincipal)User).TienePermiso("REPORTES_COMENTAR_TERCEROS"))
                    if (reporte.QuienReportaId != ((CustomPrincipal)User).Id)
                        return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);
                if (((CustomPrincipal)User).Tipo == "Desarrollo")
                    if (reporte.Estado != Reporte.EstadoEnum.EnProceso)
                        return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);

                if (reporte.Estado == Reporte.EstadoEnum.Terminado)
                {
                    return Json(new { error = true, message = "Este reporte está en estado: 'Finalizado'" }, JsonRequestBehavior.AllowGet);
                }
                
                if (!string.IsNullOrWhiteSpace(mensaje))
                {
                    resultado.Resultado = OperacionResultado.Error;                    

                    var user = (CustomPrincipal)User;
                    comentario = new Comentario(reporteId, mensaje, user.Id);
                    
                    resultado = db.CrearComentario(comentario);

                    if (resultado.Resultado == OperacionResultado.Ok)
                    {
                        //agrega el archivo, si existe
                        if (uploadFile != null)
                        {
                            string file = Evidencias.SaveFile(db.ObtenerConfiguracion().RutaEvidencias, uploadFile, comentario.Id.ToString());
                            comentario.ArchivoRutaRelativa = file;
                            db.ActualizarComentario(comentario);
                        }
                        dbu = new ConsultasUsuarios();
                        comentario = db.ObtenerComentario(resultado.Id);
                        Notifications.NotifyAll(reporte, UsuariosNotificationsConfiguration.NotificationsEnum.NewComments, dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == user.Id), comentario.Contenido);
                    }
                }
                else
                {
                    resultado.Resultado = OperacionResultado.Error;
                    resultado.Mensaje = "Todos los campos son requeridos.";
                }
            }
            catch (Exception ex)
            {
                if (resultado.Resultado == OperacionResultado.Ok)
                {
                    //Elimina el comentario.
                    db.EliminarComentario(comentario.Id);
                }

                resultado.Resultado = OperacionResultado.Error;
                resultado.Mensaje = ex.Message;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return View("Reportes/_Comentario", comentario);
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReporteCambiarEstado(int idReporte, Reporte.EstadoEnum estado, string evidencia, string contenido, int? idDesarrollador, bool? backToHome, bool? advanceVersion, HttpPostedFileBase uploadFileInner, int? idVersion)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            Reporte reporte = null;
            Comentario comentario = null;
            ResultadoOperacion resultado = new ResultadoOperacion();
            
            try
            {
                resultado.Resultado = OperacionResultado.Error;
                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();

                var user = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);

                reporte = db.ObtenerReporte(idReporte);
                Reporte.EstadoEnum estadoInicial = reporte.Estado;
                reporte.Estado = estado;

               
                switch (estado)
                {
                    case Reporte.EstadoEnum.Terminado:
                    case Reporte.EstadoEnum.NoResuelto:
                        if (!((CustomPrincipal)User).TienePermiso("REPORTES_CAMBIAR_TERMINADO_NORESUELTO"))
                            return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);
                        break;
                    case Reporte.EstadoEnum.Pendiente:
                    case Reporte.EstadoEnum.Reportado:
                    case Reporte.EstadoEnum.Cancelado:
                        if (!((CustomPrincipal)User).TienePermiso("REPORTES_CAMBIAR_PENDIENTE_REPORTADO_CANCELADO"))
                            return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);
                        break;
                    case Reporte.EstadoEnum.EnProceso:
                        if (!((CustomPrincipal)User).TienePermiso("REPORTES_CAMBIAR_ENPROCESO"))
                            return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);
                        break;
                    case Reporte.EstadoEnum.PorValidar:
                        if (!((CustomPrincipal)User).TienePermiso("REPORTES_CAMBIAR_PORVALIDAR"))
                            return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);
                        break;
                }

                if (user.ReportesCategorias.FirstOrDefault(r => r.Id == reporte.CategoriaId) == null)
                    return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);

                if (estado == Reporte.EstadoEnum.EnProceso)
                {
                    if (idDesarrollador.HasValue) resultado.Resultado = OperacionResultado.Ok;
                    else resultado.Mensaje = "Seleccione a un asignado.";
                }
                else if (reporte.RequiereEvidencia || reporte.RequiereVersionCorreccion)
                {
                    if (string.IsNullOrEmpty(evidencia) || string.IsNullOrEmpty(contenido) || !idVersion.HasValue)
                    {
                        if (estado == Reporte.EstadoEnum.PorValidar)
                        {
                            if (string.IsNullOrEmpty(evidencia) && reporte.RequiereEvidencia)
                            {
                                resultado.Mensaje = "Estos cambios requieren evidencia.";
                            }
                            else if (!idVersion.HasValue && reporte.RequiereVersionCorreccion)
                            {
                                resultado.Mensaje = "Estos cambios requieren una nueva versión de corrección.";
                            }
                            else
                            {
                                resultado.Resultado = OperacionResultado.Ok;
                            }                               
                        }                        
                        else
                        {
                            if(string.IsNullOrEmpty(evidencia))
                            {
                                resultado.Mensaje = "Estos cambios requieren evidencia.";
                            }
                            else
                            {
                                resultado.Resultado = OperacionResultado.Ok;
                            }
                        }                            
                    }                    
                    else
                    {
                        resultado.Resultado = OperacionResultado.Ok;
                    }
                }                
                else
                {
                    resultado.Resultado = OperacionResultado.Ok;
                }

                if (resultado.Resultado == OperacionResultado.Ok)
                {
                    resultado.Resultado = OperacionResultado.Error;
                    if (estado == Reporte.EstadoEnum.PorValidar)
                    {
                        if (reporte.RequiereVersionCorreccion)
                        {
                            reporte.VersionDeCorreccionId = idVersion;
                        }
                        reporte.FechaSolucion = DateTime.Now;
                    }                        
                    else if (estado == Reporte.EstadoEnum.EnProceso)
                    {
                        reporte.AsignadoId = idDesarrollador;
                        reporte.FechaAsignacion = DateTime.Now;
                    }
                    else if (estado == Reporte.EstadoEnum.Terminado)
                    {                        
                    }
                    resultado = db.ActualizarReporte(reporte);

                    if (resultado.Resultado == OperacionResultado.Ok)
                    {
                        resultado.Resultado = OperacionResultado.Error;
                        if (reporte.RequiereEvidencia)
                        {
                            resultado = db.CrearComentario(new Comentario(reporte.Id, evidencia, ((CustomPrincipal)User).Id));
                            if (resultado.Resultado == OperacionResultado.Ok)
                            {
                                try
                                {
                                    if (uploadFileInner != null)
                                    {
                                        var nuevoComentario = db.ObtenerComentario(resultado.Id);
                                        string file = Evidencias.SaveFile(db.ObtenerConfiguracion().RutaEvidencias, uploadFileInner, resultado.Id.ToString());
                                        nuevoComentario.ArchivoRutaRelativa = file;
                                        db.ActualizarComentario(nuevoComentario);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //Elimina el comentario.
                                    db.EliminarComentario(resultado.Id);
                                    resultado.Resultado = OperacionResultado.Error;
                                    resultado.Mensaje = ex.Message;

                                    //Elimina el cambio.
                                    reporte.Estado = estadoInicial;
                                    db.ActualizarReporte(reporte);
                                }                               

                                if(resultado.Resultado == OperacionResultado.Ok)
                                {
                                    comentario = new Comentario(reporte.Id, estado, user.IdUsuario)
                                    {
                                        EvidenciaId = resultado.Id,
                                    };
                                    if (estado == Reporte.EstadoEnum.PorValidar)
                                        comentario.VersionDeCorreccion = contenido;

                                    resultado = db.CrearComentario(comentario);
                                    if (resultado.Resultado == OperacionResultado.Ok)
                                        comentario = db.ObtenerComentario(resultado.Id);
                                }                                
                            }
                        }
                        else
                        {
                            comentario = new Comentario(reporte.Id, estado, user.IdUsuario);
                            if (estado == Reporte.EstadoEnum.PorValidar)
                                comentario.VersionDeCorreccion = contenido;

                            resultado = db.CrearComentario(comentario);
                            if (resultado.Resultado == OperacionResultado.Ok)
                                comentario = db.ObtenerComentario(resultado.Id);
                        }
                        if(resultado.Resultado == OperacionResultado.Ok)
                        {
                            dbu = new ConsultasUsuarios();
                            var notificationsEnum = Notifications.ConvierteTipo(estado);
                            var usuario = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                            Notifications.NotifyAll(reporte, notificationsEnum, usuario);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (resultado.Resultado == OperacionResultado.Ok)
                {
                    //Elimina el comentario.
                    db.EliminarComentario(comentario.Id);
                }

                if(resultado.Mensaje == null)
                    resultado.Mensaje = ex.Message;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return RedirectToAction("ReportesDetalle", new { id = idReporte, backToHome });
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReporteCambioEstadoEvidencia(int idReporte, Reporte.EstadoEnum estado, int? versionId)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;
            ConsultasVersiones dbv = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Ok };

            Reporte model = new Reporte();
            try
            {
                dbu = new ConsultasUsuarios();
                db = new ConsultasInstalaciones();
                dbv = new ConsultasVersiones();
                model = db.ObtenerReporte(idReporte);
                model.Estado = estado;
                if (model.RequiereEvidencia)
                    ViewBag.Show = true;
                if (estado == Reporte.EstadoEnum.PorValidar)
                {
                    ViewBag.PorValidar = true;
                    if (model.Modulo != null)
                        ViewBag.Versiones = new SelectList(dbv.ObtenerAplicacionVersiones(model.Modulo.AplicacionesId), "IdVersion", "Version");
                }
                if (estado == Reporte.EstadoEnum.EnProceso)
                {
                    ViewBag.EnProceso = true;
                    var devs = dbu.UsuariosRecupera().Where(u => (u.Tipo == Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Desarrollo || u.Tipo == Data.Entidades.Poco.Usuarios.EnumUsuarioTipo.Servicio) &&
                    u.ReportesCategorias.FirstOrDefault(r => r.Id == model.CategoriaId) != null);                    
                    ViewBag.Devs = new SelectList(devs, "IdUsuario", "Nombre");
                }
                
                ViewBag.User = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
                if (dbu != null)
                    dbu.Dispose();
                if (dbv != null)
                    dbv.Dispose();
            }

            if(operacion.Resultado == OperacionResultado.Ok)
                return View("Reportes/_ComentariosEvidencia", model);
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReporteIniciarTerminarEnProceso(int idReporte)
        {
            ConsultasInstalaciones db = null;
            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Error };
            
            try
            {
                db = new ConsultasInstalaciones();
                var reporte = db.ObtenerReporte(idReporte);
                if(reporte != null)
                {
                    reporte.EnProcesoActual = !reporte.EnProcesoActual;
                    operacion = db.ActualizarReporte(reporte);
                }
                else
                {
                    operacion.Resultado = OperacionResultado.Error;
                    operacion.Mensaje = "El reporte no existe.";
                }
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return RedirectToAction("InstalacionesHome");
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReporteCambiarVistoBueno(int id, bool? vistoBueno)
        {
            ConsultasInstalaciones db = null;
            ConsultasUsuarios dbu = null;

            ResultadoOperacion operacion = new ResultadoOperacion { Resultado = OperacionResultado.Error };

            try
            {
                db = new ConsultasInstalaciones();
                dbu = new ConsultasUsuarios();
                var reporte = db.ObtenerReporte(id);

                if(((CustomPrincipal)User).Tipo == "Jefe" || ((CustomPrincipal)User).Tipo == "Administrador") { }
                else return Json(new { error = true, message = "Permisos insuficientes" }, JsonRequestBehavior.AllowGet);
                if (reporte.VistoBueno != null)                
                    if(((CustomPrincipal)User).Tipo != "Administrador" && reporte.VistoBuenoId != ((CustomPrincipal)User).Id)
                        return Json(new { error = true, message = "Permisos insuficientes. Sólo puedes modificar un visto bueno dado por ti mismo." }, JsonRequestBehavior.AllowGet);

                if (reporte.Estado == Reporte.EstadoEnum.Reportado || reporte.Estado == Reporte.EstadoEnum.Requerimiento) { }
                else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);

                if (vistoBueno.HasValue && vistoBueno.Value)
                {
                    reporte.VistoBuenoId = ((CustomPrincipal)User).Id;
                }
                else
                {
                    reporte.VistoBuenoId = null;
                }
                operacion = db.ActualizarReporteVistoBueno(reporte);
                if(operacion.Resultado == OperacionResultado.Ok)
                {
                    if (vistoBueno != null)
                    {
                        if (vistoBueno.Value)
                        {
                            var usuario = dbu.UsuariosRecupera().FirstOrDefault(u => u.IdUsuario == ((CustomPrincipal)User).Id);
                            Notifications.NotifyAll(reporte, UsuariosNotificationsConfiguration.NotificationsEnum.VistoBueno, usuario);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                operacion.Resultado = OperacionResultado.Error;
                operacion.Mensaje = "Ocurrió un error al procesar la solicitud.";
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (db != null)
                    db.Dispose();

                if (dbu != null)
                    dbu.Dispose();
            }

            if (operacion.Resultado == OperacionResultado.Ok)
                return RedirectToAction("ReportesDetalle", new { id = id, });
            else return Json(new { error = true, message = operacion.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReporteAsignar(int idReporte, string asignado)
        {
            ConsultasInstalaciones db = null;
            Reporte reporte = null;
            ResultadoOperacion resultado = new ResultadoOperacion();

            try
            {
                resultado.Resultado = OperacionResultado.Error;
                db = new ConsultasInstalaciones();
                var user = (CustomPrincipal)User;

                resultado.Resultado = OperacionResultado.Error;
                if (asignado != null)
                {
                    resultado.Resultado = OperacionResultado.Ok;
                }
                else
                {
                    resultado.Mensaje = "El contenido no puede estar vacío.";
                }

                if (resultado.Resultado == OperacionResultado.Ok)
                {
                    resultado.Resultado = OperacionResultado.Error;
                    reporte = db.ObtenerReporte(idReporte);
                    //reporte.AsignadoId = asignado;
                    resultado = db.ActualizarReporte(reporte);
                    if (resultado.Resultado == OperacionResultado.Ok)
                    {
                        db.CrearComentario(new Comentario(reporte.Id, Comentario.LogEnum.Asignado, user.Id));
                    }


                    Notifications.NotifyAll(reporte, NotificationsEnum.StateChange_EnProceso);
                }

            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
                if (resultado.Mensaje == null)
                    resultado.Mensaje = ex.Message;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            if (resultado.Resultado == OperacionResultado.Ok)
                return RedirectToAction("ReportesDetalle", new { id = resultado.Id });
            else return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EvidenciaGetFile(string ruta)
        {
            ResultadoOperacion resultado = new ResultadoOperacion();
            ConsultasInstalaciones db = null;
            FileStream file = null;

            try
            {
                db = new ConsultasInstalaciones();
                file = Evidencias.GetFile(db.ObtenerConfiguracion().RutaEvidencias, ruta);
            }
            catch(Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
                resultado.Resultado = OperacionResultado.Error;
                if (resultado.Mensaje == null)
                    resultado.Mensaje = ex.Message;
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }
            
            if(resultado.Resultado == OperacionResultado.Ok)
            {
                if(Evidencias.TypeByExtension(Path.GetExtension(ruta)) == Evidencias.EvidenciaType.Image)
                    return File(file, "image/jpeg");
                if (Evidencias.TypeByExtension(Path.GetExtension(ruta)) == Evidencias.EvidenciaType.Video)
                    return File(file, $"video/{Path.GetExtension(ruta).Replace(".", "")}");
            }
            return Json(new { error = true, message = resultado.Mensaje }, JsonRequestBehavior.AllowGet);

        }
    }
}
