﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IDigitales.Pagina.Web.Controllers.Helpers
{
    public static class UrlExtensions
    {
        public static string CultureContent(this UrlHelper url, string path){
           
          
            var cultura = HttpContext.Current.Request.Cookies["_cultura"];

            if(cultura == null || cultura.Value == "es"){
            }else{

                int indexDot = path.LastIndexOf(".");

                if(indexDot > -1)
                     path = path.Insert(indexDot, ".en");
                
               
            }

            return url.Content(path);
        }
    }
}
