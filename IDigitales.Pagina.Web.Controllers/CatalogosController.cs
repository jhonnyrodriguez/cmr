﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using IDigitales.Pagina.Web.Controllers.Helpers;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Utils;

namespace IDigitales.Pagina.Web.Controllers
{
    public class CatalogosController: BaseController
    {

        public async Task<ActionResult> BuscarEntidad(string catalogo, string filtro)
        {
            List<CatalogoBusqueda> resultado = null;
            try
            {
                var filtros = filtro.Split('-');
                using (var con = new ConsultasCatalogos())
                {
                    switch (catalogo)
                    {
                        case "Productos":
                            resultado= await con.ObtieneProductos(filtro);
                            break;
                        case "Cotizaciones":
                            resultado = await con.ObtieneCotizaciones(filtro, ((CustomPrincipal)User).Id);
                            break;
                        case "Contactos":
                            resultado = await con.ObtieneContactos(filtro);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            if (resultado == null)
            {
                resultado= new List<CatalogoBusqueda>
                {
                    new CatalogoBusqueda() {codigo = "Error", id = "-1", valor = "No se pudo realizar la Busqueda"}
                };
            }

            return Json(new { Catalogo = resultado }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ObtenerListasCatalogosAsync(string catalogos)
        {
            var listaCatalogos = new Hashtable();
            IEnumerable<Catalogos> lista = null;
            ConsultasCatalogos con = null;
            string[] r = null;
            try
            {
                var catalogosArray = catalogos.Split(',');
                con = new ConsultasCatalogos();

                foreach (var cat in catalogosArray)
                {
                    var cata = cat;
                    var id = 0;
                    var fil = "";
                    if (cat.Contains("-"))
                    {
                        r = cat.Split('-');
                        cata = r[0];
                       
                        var result = Int32.TryParse(r[1], out id);
                        if (result == false)
                            fil = r[1];
                    }
                    switch (cata)
                    {
                        case "AplicacionesVersiones":
                            lista = await con.ObtenerAplicacionesVersionesAsync(id);
                            break;
                        case "AplicacionesSistemas":
                            if(r.Length >2)
                            fil = r[2];
                            lista = await con.ObtenerAplicacionesSistemasAsync(id, fil);
                            break;
                        case "Clasificaciones":
                            lista = await con.ObtenerClasificaciones();
                            break; 
                        case "ProductoTipo":
                            lista = ObtenerCatalogoEnum<Productos.EnumProductoTipos>();
                            break;
                        case "ProductoMonedas":
                            lista = ObtenerCatalogoEnum<Productos.EnumMonedas>();
                            break;
                        case "UsuarioTipo":
                            lista = ObtenerCatalogoEnum<Usuarios.EnumUsuarioTipo>();
                            break;
                        case "UsuarioTipoPorUsuario":
                            lista = ObtenerCatalogoEnum<Usuarios.EnumUsuarioTipo>();

                            if (fil.ToLower() == "jefe")
                            {
                                lista = lista.Where(x => x.Id != ((int)Usuarios.EnumUsuarioTipo.Jefe).ToString());
                            }else if (fil == "vendedor" )
                            {
                                lista= lista.Where(x => x.Id == ((int)Usuarios.EnumUsuarioTipo.Vendedor).ToString());
                            }
                            else if (fil == "servicio")
                            {
                                lista = lista.Where(x => x.Id == ((int)Usuarios.EnumUsuarioTipo.Servicio).ToString());
                            }
                            else if (fil == "desarrollo")
                            {
                                lista = lista.Where(x => x.Id == ((int)Usuarios.EnumUsuarioTipo.Desarrollo).ToString());
                            }
                            else if (fil == "aplicaciones")
                            {
                                lista = lista.Where(x => x.Id == ((int)Usuarios.EnumUsuarioTipo.Aplicaciones).ToString());
                            }

                            break;
                        case "ListasPrecios":
                            var lNac = User.IsInRole("GENERAL_LISTAPRECIOS_NACIONAL");
                            var lInt = User.IsInRole("GENERAL_LISTAPRECIOS_INTERNACIONAL");
                            var lSer = User.IsInRole("GENERAL_LISTAPRECIOS_SERVICIO");
                            var lOtras = User.IsInRole("GENERAL_LISTAPRECIOS_OTRAS");
                            lista = await con.ObtenerListasPrecios(lNac, lInt, lSer, lOtras);
                            break;
                        case "Aplicaciones":
                            lista = await con.ObtenerAplicacionesAsync();
                            break;
                        case "InstalacionesUsuario":
                  
                            lista = await con.ObtenerInstalaciones();
                            break;
                    }
                    if (lista != null)
                        listaCatalogos.Add(cata, lista.ToList());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                if(con != null)
                    con.Dispose();
            }
            
  
            return Json(new { ListaCatalogos = listaCatalogos }, JsonRequestBehavior.AllowGet);
        }

        public static List<Catalogos> ObtenerCatalogoEnum<T>()
        {
            try
            {
                var catalogos = (from Enum valor in Enum.GetValues(typeof(T))
                    select new Catalogos { Id = Convert.ToInt32(valor).ToString(), Valor = ObtenerDescriptionEnum(valor), Elemento = valor }).ToList();
                return catalogos;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return null;
        }
       
        public static string ObtenerDescriptionEnum(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])
                fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
        public ActionResult ObtenerDescripcion(string catalogo, string id)
        {
           
            var datos = new Catalogos();
            try
            {
                int idFiltro;
                int.TryParse(id, out idFiltro);
                using (var con = new ConsultasCatalogos())
                {
                    switch (catalogo)
                    {
                        case "Productos":
                            datos = con.ObtenerDescripcion<Productos>(idFiltro, "Nombre");
                            break;
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(new { datos }, JsonRequestBehavior.AllowGet);
        }
    }

}
