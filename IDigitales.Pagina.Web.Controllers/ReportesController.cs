﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.Diagnostics;
using System.IO;
using IDigitales.Comunes.Librerias.IO;
using IDigitales.Pagina.Web.Data;

namespace IDigitales.Pagina.Web.Controllers
{
    public class ReportesController
    { 
        public string ObtenerRutaPlantillas()
        {
            return  "~/Content/reportes/plantillas/";
        }

        public byte[] GenerarReporteStream(dynamic datos, string rutaPlantilla, string extension)
        {
            XtraReport report = null;
            try
            {
                report = new XtraReport();
                var stopWatch = new Stopwatch();
                stopWatch.Start(); 
                report.LoadLayout(rutaPlantilla);
                stopWatch.Stop();
                // Get the elapsed time as a TimeSpan value.
                var ts = stopWatch.Elapsed;

                // Format and display the TimeSpan value.
                var elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                Console.WriteLine("TIEMPO........... " + elapsedTime);
                report.DataSource = datos;
                 
                if (extension.ToLower().Contains("xlsx"))
                {
                    using (var ms = new MemoryStream())
                    {
                        var e = new XlsxExportOptions();
                        e.FitToPrintedPageWidth = true;
                        report.ExportToXlsx(ms, e);
                        return ms.GetBuffer();
                    }
                }
                if (extension.ToLower().Contains("xls"))
                {
                    using (var ms = new MemoryStream())
                    {
                        var e = new XlsExportOptions();
                        e.FitToPrintedPageWidth = true;
                        report.ExportToXls(ms, e);
                        return ms.GetBuffer();
                    }
                }

                if (extension.Contains("rtf"))
                {
                    using (var ms = new MemoryStream())
                    {
                        report.ExportToRtf(ms);
                        return ms.GetBuffer();
                    }
                }
                if (extension.ToLower().Contains("csv"))
                {
                    using (var ms = new MemoryStream())
                    {
                        //Quitar Encabezado y pie de pagina para exportar solo el detalle del reporte
                        Band bandTop = report.Bands.GetBandByType(typeof(TopMarginBand));
                        if (bandTop != null)
                            report.Bands[bandTop.Index].Visible = false;

                        Band bandBottom = report.Bands.GetBandByType(typeof(BottomMarginBand));
                        if (bandBottom != null)
                            report.Bands[bandBottom.Index].Visible = false;

                        CsvExportOptions csvOptions = report.ExportOptions.Csv;
                        csvOptions.TextExportMode = TextExportMode.Text;

                        report.ExportToCsv(ms);
                        return ms.GetBuffer();
                    }
                }
                if (extension.ToLower().Contains("pdf"))
                {

                    using (var ms = new MemoryStream())
                    {
                        report.ExportToPdf(ms);
                        return ms.GetBuffer();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                if (report != null)
                    report.Dispose();
            }
            return null;
        }

        public string GuardaArchivo(byte[] archivoBytes, string nombre, string extencion, bool eliminar = true)
        {
            string ruta = "";
            if (archivoBytes != null && archivoBytes.Any())
            {
                var consulta = new ConsultasComunes();
                var directorio = consulta.ObtenerRutaTemporales() + nombre;
                ruta= GuardaDocumento(directorio, archivoBytes, nombre, extencion, eliminar);
            }
            return ruta;
        }

        public string GuardaDocumento(string directorio, byte[] archivoBytes, string nombre, string extencion, bool eliminar)
        {
            try
            {
                Directorios.CreaCarpeta(directorio, true);
                string rutaDestino = directorio + "\\" + nombre + "." + extencion;
                if (Directory.Exists(directorio))
                {
                    var archivoExiste = System.IO.File.Exists(rutaDestino);
                    if (archivoExiste && eliminar)
                    {
                        try
                        {
                            System.IO.File.Delete(rutaDestino);
                        }
                        catch (Exception ex)
                        {

                        }

                        File.WriteAllBytes(rutaDestino, archivoBytes);
                    }
                    else
                    {
                        File.WriteAllBytes(rutaDestino, archivoBytes);
                    }

                }
                return rutaDestino;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string Generar(dynamic datos, string rutaPlantilla, string nombreArchivo, string extencion, bool soloUnaPagina)
        {
            var report = new XtraReport();
            report.LoadLayout(rutaPlantilla);
            report.DataSource = datos;

            var consulta = new ConsultasComunes();
            string rutaArchivoGenerado = consulta.ObtenerRutaTemporales() + nombreArchivo + "." + extencion;

            if (extencion.ToLower().Contains("png"))
            {
                var s = new ImageExportOptions();
                s.ExportMode = ImageExportMode.DifferentFiles;
                if (soloUnaPagina)
                    s.ExportMode = ImageExportMode.SingleFile;

                s.PageBorderWidth = 0;
                s.Resolution = 380;
                report.ExportToImage(rutaArchivoGenerado, s);
            }
            else if (extencion.ToLower().Contains("xls"))
            {
                report.ExportToXlsx(rutaArchivoGenerado);                 
            }
            else if (extencion.Contains("rtf"))
            {
                report.ExportToRtf(rutaArchivoGenerado);                
            }
            else if (extencion.ToLower().Contains("csv"))
            {
                report.ExportToCsv(rutaArchivoGenerado);
            }
            else
            {
                report.ExportToPdf(rutaArchivoGenerado);                 
            }

            return rutaArchivoGenerado;
        }

    }
}

