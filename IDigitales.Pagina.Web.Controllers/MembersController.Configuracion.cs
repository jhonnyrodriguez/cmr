﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DevExpress.Data.Async;
using DevExpress.Data.Helpers;
using IDigitales.HIS.Web.Data.Utils;
using IDigitales.Pagina.Web.Comun.Enums;
using IDigitales.Pagina.Web.Data;
using IDigitales.Pagina.Web.Data.Entidades;
using IDigitales.Pagina.Web.Data.Entidades.Poco;
using IDigitales.Pagina.Web.Data.Modelos;
using IDigitales.Pagina.Web.Data.Utils;
using Newtonsoft.Json;

namespace IDigitales.Pagina.Web.Controllers
{
    public partial class  MembersController
    {
        public ActionResult Configuracion()
        {
            return View("Configuracion/ConfiguracionPrincipal");
        }

        #region General

        public ActionResult Configuracion_General()
        {
            ConsultasConfiguracion con = null;
            List<Rutas> ruta = new List<Rutas>();
            try
            {
                con = new ConsultasConfiguracion();
                ruta = con.ObtenerRutas();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            return View("Configuracion/ConfiguracionGeneral", ruta);
        }

        public async Task<ActionResult> GuardarConfiguracionGeneral(List<Rutas> rutas)
        {
            ConsultasConfiguracion con = null;
            var result = new ResultadoOperacion() { Resultado = OperacionResultado.Error };
            try
            {
                con = new ConsultasConfiguracion();
                result = await con.GuardarRutas(rutas);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Usuarios
        public ActionResult Usuarios()
        {
           return PartialView("Configuracion/Usuarios/ConfiguracionUsuarios");
        }

        public async Task<ActionResult> ListaUsuarios(string filtro, int? pagina, int? num, string tipo, string instalacion, string tipoUsuario)
        {
            UsuariosModel model = null;
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    var usuarioC = Request.Cookies["Usuario"];

           
                   
                    model = await con.ObtenerUsuarios(filtro, tipo, instalacion, tipoUsuario,  pagina ??1,num??20);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return PartialView("Configuracion/Usuarios/_TarjetasUsuariosPag", model);
        }
        #region   Edicion Permisos - Vistas
        public async Task<ActionResult> ConfiguracionVistasPermisos(int idUsuario)
        {
            UsuariosVistasPermisosModel model=null;
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    model = await con.ObtenerUsuariosVistasPermisos(idUsuario);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return PartialView("Configuracion/Usuarios/ConfiguracionVistasPermisos", model);
        }

        public async Task<ActionResult> AgregarActualizarVistasPermisos(int idUsuario, List<int> vistas, List<int> permisos, List<int> aplicaciones,
            List<int> instalaciones, List<int> categorias, List<int> responsables)
        {
            ResultadoOperacion result = new ResultadoOperacion() { Resultado = OperacionResultado.Error }; 
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    result = await con.AgregarActualizarVistasPermisos(idUsuario, vistas, permisos, aplicaciones, instalaciones, categorias, responsables);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }

        #endregion

        #region   Edicion Usuarios

        public async Task<ActionResult> _EdicionUsuario(int idUsuario)
        {
            ConsultasConfiguracion consulta = null;
            Usuarios elemento = null;
            try
            {
                if (idUsuario != 0)
                {
                    consulta = new ConsultasConfiguracion();
                    elemento = await consulta.ObtenerUsuario(idUsuario);
                }

                if (elemento == null)
                {
                    elemento = new Usuarios();
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return PartialView("Configuracion/Usuarios/UsuariosEdicion", elemento);
        }

        public async Task<ActionResult> AgregarActualizarUsuario(Usuarios usuario)
        {
            ResultadoOperacion result = null;
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    result = await con.AgregarActualizarUsuarios(usuario);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }


        public async Task<ActionResult> _BorrarUsuario(int idUsuario)
        {
            ResultadoOperacion result = null;
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    result = await con.BorrarUsuario(idUsuario);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }
        #endregion

        #endregion

        #region Contactos
        public ActionResult Configuracion_Contactos()
        {
            
            return PartialView("Configuracion/Contactos/ConfiguracionContactos");
        }

        public ActionResult ListaAplicaciones()
        {
            List<Aplicaciones> aplicaciones = new List<Aplicaciones>();
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    aplicaciones = con.ObtenerAplicaciones();
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return PartialView("Configuracion/Contactos/ConfiguracionContactosAplicaciones", aplicaciones);
        }


        public ActionResult ListaContactos(int idAplicacion)
        {
            List<Contactos> contactos = new List<Contactos>();
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    contactos = con.ObtenerContactosPorAplicacion(idAplicacion);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return PartialView("Configuracion/Contactos/ConfiguracionContactosLista", contactos);
        }



        public async Task<ActionResult> _EdicionContacto(int idContacto, int? idAplicacion)
        {
            ConsultasConfiguracion consulta = null;
            Contactos elemento = null;
            try
            {
                if (idContacto != 0)
                {
                    consulta = new ConsultasConfiguracion();
                    elemento = await consulta.ObtenerContacto(idContacto);
                }

                if (elemento == null)
                {
                    elemento = new Contactos();
                }

                ViewBag.IdAplicacion = idAplicacion;
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            finally
            {
                if (consulta != null)
                    consulta.Dispose();
            }

            return PartialView("Configuracion/Contactos/ConfiguracionContactosEdicion", elemento);
        }

        public async Task<ActionResult> AgregarActualizarContacto(Contactos contacto)
        {
            ResultadoOperacion result = null;
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    result = await con.AgregarActualizarContactos(contacto);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }

        public async Task<ActionResult> _BorrarContacto(int idContacto, int idAplicacion)
        {
            ResultadoOperacion result = null;
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    result = await con.BorrarContactos(idContacto, idAplicacion);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }


        #endregion

        #region Instalaciones

        public ActionResult Configuracion_Instalaciones()
        {

            return PartialView("Configuracion/Instalaciones/ConfiguracionInstalacionesGeneral");
        }


        public ActionResult ListaInstalaciones()
        {
            List<Instalacion> instalaciones = new List<Instalacion>();
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    instalaciones = con.ObtenerInstalaciones();
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return PartialView("Configuracion/Instalaciones/ConfiguracionInstalacionesLista", instalaciones);
        }

        public ActionResult ListaCategorias(int idInstalacion)
        {
            List<ReportesCategorias> categorias = new List<ReportesCategorias>();
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    categorias = con.ObtenerCategorias();

                    foreach (var categoria in categorias)
                    {
                        var instalacion = categoria.Instalacion.FirstOrDefault(i => i.Id == idInstalacion);
                        categoria.Seleccionada = !(instalacion == null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }

            return PartialView("Configuracion/Instalaciones/ConfiguracionInstalacionesCategorias", categorias);
        }

        public async Task<ActionResult> GuardarCategorias(int idInstalacion, int idCategoria, bool bSeleccion)
        {
            ResultadoOperacion result = null;
            try
            {
                using (var con = new ConsultasConfiguracion())
                {
                    result = await con.GuardarCategorias(idInstalacion, idCategoria, bSeleccion);
                }
            }
            catch (Exception ex)
            {
                Log.WriteNewEntry(ex, EventLogEntryType.Error);
            }
            return Json(result);
        }

        #endregion
    }
}
