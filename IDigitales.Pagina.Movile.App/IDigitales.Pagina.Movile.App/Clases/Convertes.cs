﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Xml;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDigitales.Pagina.Movile.App.Clases
{
    class ImageSourceConverter : IValueConverter
    { 

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string objeto = value as string;

            if (!String.IsNullOrEmpty(objeto))
            {
                var assembly = GetType().GetTypeInfo().Assembly;
                var assemblyName = assembly.GetName().Name;
                var image = assemblyName + "." + objeto;
                return ImageSource.FromResource(image, assembly);
            }
            else
                return null; 
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        } 

    }

    class LocalizationConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string objeto = value as string;

            if (!String.IsNullOrEmpty(objeto))
            {                 
                ResourceManager manejador = new ResourceManager("IDigitales.Pagina.Movile.App.AppResources.AppResources",
                    typeof(LocalizationConverter).GetTypeInfo().Assembly);
                string textoTraduccion = manejador.GetString(objeto, CultureInfo.CurrentCulture); 
                return textoTraduccion != null ? textoTraduccion.Replace(@"\n", Environment.NewLine) :textoTraduccion; 
            }
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

    }

    class LocalizationConverterHTML : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string objeto = value as string;
                  

            if (!String.IsNullOrEmpty(objeto))
            {
                ResourceManager manejador = new ResourceManager("IDigitales.Pagina.Movile.App.AppResources.AppResources",
                    typeof(LocalizationConverter).GetTypeInfo().Assembly);

                string textoTraduccion = manejador.GetString(objeto, CultureInfo.CurrentCulture);

                if (textoTraduccion != null)
                    textoTraduccion = textoTraduccion.Replace(@"\n", "<br>");

                var text = "<html>" +
                  "<body  style=\"text-align: justify; background-color: transparent; color: rgb(0,146,166); font-size: 11px;\">" +
                  String.Format("<p>{0}</p>", textoTraduccion !=  null ? textoTraduccion : "") +
                  "</body>" +
                  "</html>";
                  
                return text;
            }
            else
                return objeto;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

    }

    class LocalizationConverterHTMLProducts : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Producto objeto = value as Producto;


            if (objeto != null)
            {
                ResourceManager manejador = new ResourceManager("IDigitales.Pagina.Movile.App.AppResources.AppResources",
                    typeof(LocalizationConverter).GetTypeInfo().Assembly);

                string descripcion = manejador.GetString(objeto.Descripcion, CultureInfo.CurrentCulture);

                if (descripcion != null)
                    descripcion = descripcion.Replace(@"\n", "<br>");

                var text = "<html>" +
                  "<body  style=\"text-align: justify; background-color: transparent; color: white; font-size: 11px;\">"  +
                  String.Format("<h2>{0}</h2>", objeto.Subtitulo != null ? objeto.Subtitulo: "") + 
                  String.Format("<p>{0}</p>", descripcion != null ? descripcion : "") +
                  "</body>" +
                  "</html>";

                return text;
            }
            else
                return objeto;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

    }
    class StringVisibility : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string objeto = value as string;


            if (!String.IsNullOrEmpty(objeto))
            {
                return true;
            }
            else
                return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

    }

}
