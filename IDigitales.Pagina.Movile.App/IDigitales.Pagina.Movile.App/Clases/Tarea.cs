﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace IDigitales.Pagina.Movile.App.Clases
{
    class Tarea
    {
        [PrimaryKey,AutoIncrement,Column("IdTarea")]
        public int Id { get; set; }
        public String Nombre { get; set; }
        public DateTime Fecha { get; set; }
        public TimeSpan Hora  { get; set; }

        public bool Completada { get; set; }
    }
}
