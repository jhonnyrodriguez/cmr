﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDigitales.Pagina.Movile.App.Clases
{
    [ContentProperty("Source")]
    public class ImageSourceExtension : IMarkupExtension<ImageSource>
    {
        public string Source { get; set; }

        public ImageSource ProvideValue(IServiceProvider serviceProvider)
        {
            if(String.IsNullOrEmpty(Source))
            {
                IXmlLineInfo lineInfo;
                IXmlLineInfoProvider lineInfoProvider = serviceProvider.GetService(
                    typeof(IXmlLineInfoProvider)) as IXmlLineInfoProvider;
               
                if(lineInfoProvider != null)
                {
                    lineInfo = lineInfoProvider.XmlLineInfo;
                }
                else
                {
                    lineInfo = new XmlLineInfo();
                }
                throw new XamlParseException("No puede dejar vacia la propiedad Source",lineInfo);
            }

            var assembly = GetType().GetTypeInfo().Assembly;
            var assemblyName = assembly.GetName().Name;
            var image = assemblyName + "." + Source;            
            return ImageSource.FromResource(image,assembly);
        }

        object IMarkupExtension.ProvideValue(IServiceProvider serviceProvider)
        {
            return (this as IMarkupExtension<ImageSource>).ProvideValue(serviceProvider);
        }
    }
}
