﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace IDigitales.Pagina.Movile.App.Clases
{
    public class Catalogo: INotifyPropertyChanged
    {
        public string Encabezado { set; get; }
        public string Titulo { set; get; }
        public string Descripcion { set; get; } 
        bool _mostrarIndicadores;
        ObservableCollection<Producto> _listaProductos;
        public ObservableCollection<Producto> ListaProductos
        {
            set
            {
                _listaProductos = value;
                OnPropertyChanged("ListaProductos");
            }
            get {
                   _mostrarIndicadores = (_listaProductos != null && _listaProductos.Count > 1) ? true: false;
                   return _listaProductos;
            }
        }

        public bool MostrarIndicadores
        {
            set { _mostrarIndicadores = value;
                OnPropertyChanged("MostrarIndicadores");
            }
            get { return _mostrarIndicadores; }
        }

        public Command MyCommand { protected set; get; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        { 
             PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)); 
        }

        
        public Catalogo()
        {
            Encabezado = "";
            Titulo = "";
            Descripcion = "";
            ListaProductos = new ObservableCollection<Producto>
            {
                new Producto()
            };
        }

    }

    public class Producto
    { 
        public string Subtitulo { set; get; }
        public string Descripcion { set; get; }
        public string Imagen { set; get; }
         
        public Producto()
        {
            Subtitulo = "";
            Descripcion = "";
            Imagen = "";
        }
    }
}

