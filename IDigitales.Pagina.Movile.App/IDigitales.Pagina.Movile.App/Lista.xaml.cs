﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.Pagina.Movile.App.Clases;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDigitales.Pagina.Movile.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Lista : ContentPage
	{
		public Lista ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //cuando se muestre la pagina agregaremos algunos elementos a nuestra base y lo mostraremos
            //la conexion requiere una ruta, esta ruta es diferente en Android y IOS
            //vamos a modificar la aplicacion para enviarsela como parametro
            using (var con = new SQLiteConnection(App.RutaBd))
            {
                try
                {
                    //crearemos la tabla de tareas, si ya existe se ignorara
                    con.CreateTable<Tarea>();
                    //recuperamos las tareas en la base
                    var lista = con.Table<Tarea>().Where(x => x.Completada == false).ToList();

                    //si no contiene ninguna agregamos unas para mostrarlas en la lista
                    if (!lista.Any())
                    {
                        var t1 = new Tarea()
                        {
                            Nombre = "Tarea 1",
                            Fecha = DateTime.Now,
                            Hora = DateTime.Now.TimeOfDay,
                            Completada = false
                        };
                        var t2 = new Tarea()
                        {
                            Nombre = "Tarea 2",
                            Fecha = DateTime.Now.AddDays(5),
                            Hora = DateTime.Now.TimeOfDay,
                            Completada = false
                        };

                        var r = con.Insert(t1);
                        if(r>0)
                            DisplayAlert("Guardada", "Guardada Tarea 1", "Ok");

                        r = con.Insert(t2);
                        if (r > 0)
                            DisplayAlert("Guardada", "Guardada Tarea 1", "Ok");

                        lista =  con.Table<Tarea>().Where(x => x.Completada == false).ToList();

                    }
                    //para mostrarla agregaremos un listview y asignamos el data source
                    ListViewTareas.ItemsSource = lista;

                   
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        private void MenuItemCompletada_OnClicked(object sender, EventArgs e)
        {
           //para marcarla como completada obtenemos el elemento

           if (sender is MenuItem menuItem)
           {
               var tarea = menuItem.CommandParameter as Tarea;
               using (var con = new SQLiteConnection(App.RutaBd))
               {
                   //se completa la tarea y se actualisa en la base
                   tarea.Completada = true;
                   con.Update(tarea);
                   //modificamos para que solo recupere las tareas sin completar
                   var lista = con.Table<Tarea>().Where(x=>x.Completada==false).ToList();
                   ListViewTareas.ItemsSource = lista;
                }
           }

        }
    }
}