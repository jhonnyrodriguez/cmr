﻿using System;
using IDigitales.Pagina.Movile.App.Paginas;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace IDigitales.Pagina.Movile.App
{
    public partial class App : Application
    {
        public static string RutaBd { get; set; }
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Productos());
        }
        public App(string rutaBd)
        {
            InitializeComponent();
            RutaBd = rutaBd;
            MainPage = new NavigationPage(new Productos());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
