﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.Pagina.Movile.App.Clases;
using IDigitales.Pagina.Movile.App.Controls;
using IDigitales.Pagina.Movile.App.Paginas;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static IDigitales.Pagina.Movile.App.Controls.ButtonXam;
using static IDigitales.Pagina.Movile.App.Paginas.CatalogoGenerico; 

namespace IDigitales.Pagina.Movile.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Productos : ContentPage
	{
        #region ". Constructor"
        public Productos ()
		{
			InitializeComponent(); 
        }
        #endregion

        #region ". Metodos"

        void MnuSuperior_Clicked(object sender, EventArgs e)
        {
            ButtonXam item = sender as ButtonXam;
            MuestraMenus(item);
        }

        void CMRMnuInicial_Clicked(object sender, EventArgs e)
        {
            sbMnuInicial.IsVisible = false;
            menuSuperior.IsVisible = true;
            menuSuperior.Opacity = 1;
            ButtonXam item = sender as ButtonXam;
            MuestraMenus(item); 
        }

        async void MuestraMenus(ButtonXam item)
        {

            ButtonXam btnOculto = null;
            btnCentralCMR.IsVisible = false;

            switch (btnCentral.EtiquetaButtonXam)
            {
                case "Movil":
                    btnOculto = btnMovil;
                    await sbMnuMovil.FadeTo(0, 500, Easing.Linear);
                    sbMnuMovil.IsVisible = false;
                    break;
                case "Fluro":
                    btnOculto = btnFluro;
                    await sbMnuFluro.FadeTo(0, 500, Easing.Linear);
                    sbMnuFluro.IsVisible = false;
                    break;
                case "Radio":
                    btnOculto = btnRadio;
                    await sbMnuRadiografico.FadeTo(0, 500, Easing.Linear);
                    sbMnuRadiografico.IsVisible = false;
                    break;
                case "Apps":
                    btnOculto = btnApps;
                    await sbMnuApps.FadeTo(0, 500, Easing.Linear);
                    sbMnuApps.IsVisible = false;
                    break;
            }

            if (btnOculto != null)
            {
                await ((ImageButton)btnOculto).FadeTo(1, 500, Easing.Linear);
                ((ImageButton)btnOculto).IsVisible = true;
            }

            btnCentral.EtiquetaButtonXam = item.EtiquetaButtonXam;
            btnOculto = null;

            switch (item.EtiquetaButtonXam)
            {
                case "Movil":
                    btnOculto = btnMovil;
                    btnCentral.Source = btnMovil.ImageEnabledButtonXam;
                    imgFondo.Source = ((Image)Application.Current.Resources["fondoMovil"]).Source;
                    IniciaAnimacion(btnCentral, sbMnuMovil);
                    break;
                case "Fluro":
                    btnOculto = btnFluro;
                    btnCentral.Source = btnFluro.ImageEnabledButtonXam;
                    imgFondo.Source = ((Image)Application.Current.Resources["fondoFluro"]).Source;
                    IniciaAnimacion(btnCentral, sbMnuFluro);
                    break;
                case "Radio":
                    btnOculto = btnRadio;
                    btnCentral.Source = btnRadio.ImageEnabledButtonXam;
                    imgFondo.Source = ((Image)Application.Current.Resources["fondoRadio"]).Source;
                    IniciaAnimacion(btnCentral, sbMnuRadiografico);
                    break;
                case "Apps":
                    btnOculto = btnApps;
                    btnCentral.Source = btnApps.ImageEnabledButtonXam;
                    imgFondo.Source = ((Image)Application.Current.Resources["fondoApps"]).Source;
                    IniciaAnimacion(btnCentral, sbMnuApps);
                    break;
            }

            if (btnOculto != null)
            {
                await btnOculto.FadeTo(0, 500, Easing.Linear);
                btnOculto.IsVisible = false;
            }
        }

        async private void IniciaAnimacion(ButtonXam objeto, Grid subMenu)
        {
            subMenu.IsVisible = true;
            objeto.IsVisible = true;
            await Task.WhenAll<bool>
            (
               objeto.FadeTo(1, 200, Easing.Linear),
               objeto.ScaleTo(1.3, 500, Easing.BounceOut), 
               subMenu.FadeTo(0, 500, Easing.Linear)
            ); 
            await Task.WhenAny<bool>
           (objeto.ScaleTo(1, 500, Easing.BounceOut),
            subMenu.FadeTo(1, 500, Easing.Linear),
            imgFondo.FadeTo(1,500,Easing.Linear));
        }
        
        private async void SubMnu_Clicked(object sender, EventArgs e)
        {
            try
            {
                this.IsEnabled = false;
                ctrlIndicador.IsVisible = true;
                ctrlIndicador.IsEnabled = true;
                ctrlIndicador.IsRunning = true;
                CatalogoGenerico pagina = new CatalogoGenerico();
                await Navigation.PushAsync(pagina);
                ButtonXam subMenu = (ButtonXam)sender;
                TipoCatalogo tipo = (TipoCatalogo)Enum.Parse(typeof(TipoCatalogo), subMenu.EtiquetaButtonXam, true);
                pagina.InicializaCatalogo(tipo);
            }
            catch (Exception)
            { 
            }   
            finally
            {
                this.IsEnabled = true;
                ctrlIndicador.IsVisible = false;
                ctrlIndicador.IsEnabled = false;
                ctrlIndicador.IsRunning = false;
            }
        }

        async private void CenteredButtonXam_Clicked(object sender, EventArgs e)
        {           

            ButtonXam btnOculto = null;
            Grid sbMnuActivo = null;

            switch (btnCentral.EtiquetaButtonXam)
            {
                case "Movil":
                    btnOculto = btnMovil;
                    sbMnuActivo = sbMnuMovil;
                    break;
                case "Fluro":
                    btnOculto = btnFluro;
                    sbMnuActivo = sbMnuFluro;
                    break;
                case "Radio":
                    btnOculto = btnRadio;
                    sbMnuActivo = sbMnuRadiografico;
                    break;
                case "Apps":
                    btnOculto = btnApps;
                    sbMnuActivo = sbMnuApps;
                    break;
            }

            if (btnOculto != null)
            {
               
                await Task.WhenAll<bool>
                (sbMnuActivo.FadeTo(0, 200, Easing.Linear),
                btnCentral.FadeTo(0, 200, Easing.Linear),
                btnOculto.FadeTo(1, 400, Easing.Linear));

                btnOculto.IsVisible = true;
            }
            sbMnuActivo.IsVisible = false;
            btnCentral.EtiquetaButtonXam = "";
            btnCentral.IsVisible = false;
            btnCentralCMR.IsVisible = true;

            imgFondo.Opacity = 0; 

            sbMnuInicial.IsVisible = true;
            menuSuperior.IsVisible = false;
        }

        #endregion

        #region ". Resize"

        //private double width = 0;
        //private double height = 0;

        //protected async override void OnSizeAllocated(double width, double height)
        //{
        //    base.OnSizeAllocated(width, height); //must be called
        //    if (this.width != width || this.height != height)
        //    {
        //        await this.FadeTo(0, 500, Easing.CubicIn);
        //        this.width = width;
        //        this.height = height;
        //        this.ForceLayout();
        //        await this.FadeTo(1, 500, Easing.CubicOut);
        //        //reconfigure layout
        //    }
        //}
       
        #endregion

    }
}