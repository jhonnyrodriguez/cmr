﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDigitales.Pagina.Movile.App.Clases;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDigitales.Pagina.Movile.App.Paginas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CatalogoGenerico : ContentPage
	{
        #region ". Variables y enums"
        public enum TipoCatalogo
        {
            Integrix,
            MRH,
            Arix,
            FluroMRF,
            Movil32004,
            Movil32B04,
            RIS,
            Publicador,
            PACS,
            Estacion,
            HIS
        }

        public Catalogo DatosCatalogo { get; set; }

        #endregion

        #region ". Constructor"

        public CatalogoGenerico ()
		{
			InitializeComponent();
            DatosCatalogo = new Catalogo();
            this.BindingContext = DatosCatalogo; 
        }

        #endregion

        #region ". Test"

        void Handle_PositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        { 
             Debug.WriteLine("Position " + e.NewValue + " selected."); 
        } 
  
        void Handle_Scrolled(object sender, CarouselView.FormsPlugin.Abstractions.ScrolledEventArgs e)
         { 
             Debug.WriteLine("Scrolled to " + e.NewValue + " percent."); 
             Debug.WriteLine("Direction = " + e.Direction);  
         }

        #endregion

        #region ". Metodos"

        public void InicializaCatalogo(TipoCatalogo tipo)
        {
            switch (tipo)
            {
                case TipoCatalogo.Arix:
                    //await DisplayAlert("Clicked", "Arix", "OK");
                    Arix();
                    break;
                case TipoCatalogo.Estacion:
                    //await DisplayAlert("Under construction", "Estacion", "OK");
                    Estaciones();
                    break;
                case TipoCatalogo.FluroMRF:
                    //await DisplayAlert("Under construction", "FluroMRF", "OK");
                    Fluro();
                    break;
                case TipoCatalogo.HIS:
                    //await DisplayAlert("Under construction", "HIS", "OK");
                    His();
                    break;
                case TipoCatalogo.Integrix:
                    //await DisplayAlert("Under construction", "Integrix", "OK");
                    Integrix();
                    break;
                case TipoCatalogo.Movil32004:
                    //await DisplayAlert("Under construction", "Movil32004", "OK");
                    Movil32004();
                    break;
                case TipoCatalogo.Movil32B04:
                    //await DisplayAlert("Under construction", "Movil32B04", "OK");
                    Movil32004_B();
                    break;
                case TipoCatalogo.MRH:
                    //await DisplayAlert("Under construction", "MRH", "OK");
                    Mrh();
                    break;
                case TipoCatalogo.PACS:
                    //await DisplayAlert("Under construction", "PACS", "OK");
                    PacsCore();
                    break;
                case TipoCatalogo.Publicador:
                    //await DisplayAlert("Under construction", "Publicador", "OK");
                    Publicador();
                    break;
                case TipoCatalogo.RIS:
                    //await DisplayAlert("Under construction", "RIS", "OK");
                    Ris();
                    break;
            }
            this.BindingContext = DatosCatalogo;
            ctrlIndicador.IsRunning = false;
            ctrlIndicador.IsVisible = false;
            ctrlIndicador.IsEnabled = false;
            crsProductos.IsVisible = true;
        }

        #region ".  Arix"
        private void Arix()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "ARIX",
                Titulo = "DescripcionCatalogo",
                Descripcion = "ARIX_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "ARIX1_Descripcion",
                          Imagen = "Images.arix.ArixRad1.png"
                    },
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "ARIX2_Descripcion",
                          Imagen = "Images.arix.ArixRad2.png"
                    },
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "ARIX4_Descripcion",
                          Imagen = "Images.arix.ArixRad4.png"
                    },
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "ARIX5_Descripcion",
                          Imagen = "Images.arix.ArixRad5.png"
                    },
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "ARIX6_Descripcion",
                          Imagen = "Images.arix.ArixRad6.png"
                    },
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "ARIX7_Descripcion",
                          Imagen = "Images.arix.ArixRad7.png"
                    },
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "ARIX8_Descripcion",
                          Imagen = "Images.arix.ArixRad8.png"
                    }
                }
            }; 
        }
        #endregion

        #region ".  PACS Y RIS"

        private void PacsCore()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "PACS-ENCORE",
                Titulo = "DescripcionCatalogo",
                Descripcion = "PACS_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "PACS1_Subtitulo",
                          Descripcion = "PACS1_Descripcion",
                          Imagen = "Images.estacion.Pacs01.png"
                    }, 
                      new Producto
                    {
                          Subtitulo = "PACS2_Subtitulo",
                          Descripcion = "PACS2_Descripcion",
                          Imagen = "Images.estacion.Pacs02.png"
                    },
                       new Producto
                    {
                          Subtitulo = "PACS3_Subtitulo",
                          Descripcion = "PACS3_Descripcion",
                          Imagen = "Images.estacion.Pacs03.png"
                    },
                        new Producto
                    {
                          Subtitulo = "PACS4_Subtitulo",
                          Descripcion = "PACS4_Descripcion",
                          Imagen = "Images.estacion.Pacs04.png"
                    },
                         new Producto
                    {
                          Subtitulo = "PACS5_Subtitulo",
                          Descripcion = "PACS5_Descripcion",
                          Imagen = "Images.estacion.Pacs05.png"
                    },
                          new Producto
                    {
                          Subtitulo = "PACS6_Subtitulo",
                          Descripcion = "PACS6_Descripcion",
                          Imagen = "Images.estacion.Pacs06.png"
                    },
                           new Producto
                    {
                          Subtitulo = "PACS7_Subtitulo",
                          Descripcion = "PACS7_Descripcion",
                          Imagen = "Images.estacion.Pacs07.png"
                    },
                            new Producto
                    {
                          Subtitulo = "PACS8_Subtitulo",
                          Descripcion = "PACS8_Descripcion",
                          Imagen = "Images.estacion.Pacs08.png"
                    },
                             new Producto
                    {
                          Subtitulo = "PACS9_Subtitulo",
                          Descripcion = "PACS9_Descripcion",
                          Imagen = "Images.estacion.Pacs09.png"
                    },
                              new Producto
                    {
                          Subtitulo = "PACS10_Subtitulo",
                          Descripcion = "PACS10_Descripcion",
                          Imagen = "Images.estacion.Pacs10.png"
                    } 
                }
            };
        }

        private void Estaciones()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "ESTACIONES",
                Titulo = "DescripcionCatalogo",
                Descripcion = "ESTACION_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "ESTACION1_Subtitulo",
                          Descripcion = "ESTACION1_Descripcion",
                          Imagen = "Images.estacion.cpu1.png"
                    },
                      new Producto
                    {
                          Subtitulo = "ESTACION2_Subtitulo",
                          Descripcion = "ESTACION2_Descripcion",
                          Imagen = "Images.estacion.cpu1.png"
                    },
                       new Producto
                    {
                          Subtitulo = "ESTACION3_Subtitulo",
                          Descripcion = "ESTACION3_Descripcion",
                          Imagen = "Images.estacion.cpu2.png"
                    },
                        new Producto
                    {
                          Subtitulo = "ESTACION4_Subtitulo",
                          Descripcion = "ESTACION4_Descripcion",
                          Imagen = "Images.estacion.cpu2.png"
                    }                         
                }
            };
        }

        private void Ris()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "RIS",
                Titulo = "DescripcionCatalogo",
                Descripcion = "RIS_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {                          
                    new Producto
                    {
                          Subtitulo = "RIS_Subtitulo",
                          Descripcion = "RIS1_Descripcion",
                          Imagen = "Images.estacion.Pacs11.png"
                    }
                }
            };
        }

        #endregion

        #region ".  Fluro"

        private void Fluro()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "MRF90T",
                Titulo = "DescripcionCatalogo",
                Descripcion = "MRF90T_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRF90T1_Descripcion",
                          Imagen = "Images.fluro.MRF 90T1.png"
                    },
                     new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRF90T2_Descripcion",
                          Imagen = "Images.fluro.MRF 90T2.png"
                    },
                      new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRF90T3_Descripcion",
                          Imagen = "Images.fluro.MRF 90T3.png"
                    },
                       new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRF90T4_Descripcion",
                          Imagen = "Images.fluro.MRF 90T4.png"
                    },
                        new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRF90T5_Descripcion",
                          Imagen = "Images.fluro.MRF90T.png"
                    },
                           new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRF90T6_Descripcion",
                          Imagen = "Images.fluro.MRF90T.png"
                    },
                              new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRF90T7_Descripcion",
                          Imagen = "Images.fluro.MRF90T.png"
                    }
                }
            };
        }
        #endregion

        #region ".  HIS"

        private void His()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "HIS",
                Titulo = "DescripcionCatalogo",
                Descripcion = "HIS_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "",
                          Imagen = "Images.His.HIS01.png"
                    }
                }
            };
        }

        #endregion

        #region ".  Integrix"

        private void Integrix()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "SISTEMA INTEGRIX",
                Titulo = "DescripcionCatalogo",
                Descripcion = "INTEGRIX_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "INTEGRIX1_Descripcion",
                          Imagen = "Images.integrix.Integrix1.png"
                    },
                     new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "INTEGRIX2_Descripcion",
                          Imagen = "Images.integrix.Integrix2.png"
                    },
                      new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "INTEGRIX3_Descripcion",
                          Imagen = "Images.integrix.Integrix3.png"
                    },
                      new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "INTEGRIX4_Descripcion",
                          Imagen = "Images.arix.ArixRad6.png"
                    },
                       new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "INTEGRIX5_Descripcion",
                          Imagen = "Images.integrix.Integrix4.png"
                    },
                        new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "INTEGRIX6_Descripcion",
                          Imagen = "Images.arix.ArixRad8.png"
                    }
                }
            };
        }

        #endregion

        #region ".  Movil"

        private void Movil32004()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "MÓVIL TMS 320",
                Titulo = "DescripcionCatalogo",
                Descripcion = "TMS320_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "TMS320_1_Descripcion",
                          Imagen = "Images.movil.TMS320_1.png"
                    },
                     new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "TMS320_2_Descripcion",
                          Imagen = "Images.movil.TMS320_2.png"
                    },
                      new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "TMS320_3_Descripcion",
                          Imagen = "Images.movil.TMS320_3.png"
                    }
                }
            };
        }

        private void Movil32004_B()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "MÓVIL TMS 320B",
                Titulo = "DescripcionCatalogo",
                Descripcion = "TMS320B_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "TMS320B_1_Descripcion",
                          Imagen = "Images.movil.TMS320B_1.png"
                    },
                     new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "TMS320B_2_Descripcion",
                          Imagen = "Images.movil.TMS320B_2.png"
                    },
                      new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "TMS320B_3_Descripcion",
                          Imagen = "Images.movil.TMS320B_3.png"
                    },
                       new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "TMS320B_4_Descripcion",
                          Imagen = "Images.movil.TMS320B_4.png"
                    }
                }
            };
        }

        #endregion

        #region ".  Publicador"
        
        private void Publicador()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = @"PUBLICADORES CD\DVD",
                Titulo = "DescripcionCatalogo",
                Descripcion = "Publicador_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "Publicador20_Subtitulo",
                          Descripcion = "Publicador20_Descripcion",
                          Imagen = "Images.publicador.Lite20.png"
                    },
                     new Producto
                    {
                          Subtitulo = "Publicador100_Subtitulo",
                          Descripcion = "Publicador100_Descripcion",
                          Imagen = "Images.publicador.Lite100.png"
                    }
                }
            };
        }

        #endregion

        #region ".  MRH"

        private void Mrh()
        {
            DatosCatalogo = new Catalogo()
            {
                Encabezado = "SISTEMA MRH II",
                Titulo = "DescripcionCatalogo",
                Descripcion = "MRH_Descripcion",
                ListaProductos = new ObservableCollection<Producto>()
                {
                    new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRH1_Descripcion",
                          Imagen = "Images.mrh.MRHII.png"
                    },
                     new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRH2_Descripcion",
                          Imagen = "Images.mrh.MRHII2.png"
                    },
                      new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRH3_Descripcion",
                          Imagen = "Images.mrh.MRHII3.png"
                    },
                       new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRH4_Descripcion",
                          Imagen = "Images.arix.ArixRad4.png"
                    },
                        new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRH5_Descripcion",
                          Imagen = "Images.arix.ArixRad5.png"
                    },
                         new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRH6_Descripcion",
                          Imagen = "Images.arix.ArixRad6.png"
                    },
                          new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRH7_Descripcion",
                          Imagen = "Images.arix.ArixRad7.png"
                    },
                           new Producto
                    {
                          Subtitulo = "",
                          Descripcion = "MRH8_Descripcion",
                          Imagen = "Images.arix.ArixRad8.png"
                    }
                }
            };
        }

        #endregion

        #region ".  Size"

                private double width = 0;
                private double height = 0;

                protected async override void OnSizeAllocated(double width, double height)
                {
                    base.OnSizeAllocated(width, height); //must be called
                    if (this.width != width || this.height != height)
                    {
                        await this.FadeTo(0, 500, Easing.CubicIn);
                        this.width = width;
                        this.height = height;
                        this.ForceLayout();
                        await this.FadeTo(1, 500, Easing.CubicOut);
                        //reconfigure layout
                    }
                }

          #endregion

        #endregion
    }
}