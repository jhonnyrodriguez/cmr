﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDigitales.Pagina.Movile.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Login : ContentPage
	{
		public Login ()
		{
			InitializeComponent ();
		}

        private void Login_OnClicked(object sender, EventArgs e)
        {
            var usuario = entryUsuario.Text;
            var pass = entryPass.Text;

            if (string.IsNullOrEmpty(usuario) || string.IsNullOrEmpty(pass))
            {
                lblResultado.Text = "Debe escribir usuario y contraseña";
            }
            else
            {
                //por el momento con que inserte cualquier cosa simularemos el login
                lblResultado.Text = "Inicio de session exitoso";
                //al iniciar sesion enviaremos a otra pagina
                Navigation.PushAsync(new Productos());
            }
        }
    }
}