﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDigitales.Pagina.Movile.App.AppResources
{
    [ContentProperty("Text")]
    class TranslateExtension : IMarkupExtension
    {
        const string ResourceId = "IDigitales.Pagina.Movile.App.AppResources.AppResources";
        public string Text  { set; get; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return null;
            ResourceManager manejador = new ResourceManager(ResourceId,typeof(TranslateExtension).GetTypeInfo().Assembly);
            string textoTraduccion = manejador.GetString(Text, CultureInfo.CurrentCulture);
            return textoTraduccion != null ? textoTraduccion.Replace(@"\n", Environment.NewLine) : textoTraduccion;
        }
    }
}
