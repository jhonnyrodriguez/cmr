﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using IDigitales.Pagina.Movile.App.Paginas;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDigitales.Pagina.Movile.App.Controls
{ 

    class ButtonXam : ImageButton
    {
        public enum EstadoBotonXam
        {
            Normal,
            Deshabilitado
        }

       
        public static readonly BindableProperty EtiquetaButtonXamProperty =
            BindableProperty.Create("EtiquetaButtonXam",
                typeof(string),
                typeof(ButtonXam));


        public string EtiquetaButtonXam
        {

            set { SetValue(EtiquetaButtonXamProperty, value); }
            get { return (string)GetValue(EtiquetaButtonXamProperty); }
        }


        public static readonly BindableProperty MuestraCatalogoButtonXamProperty =
         BindableProperty.Create("MuestraCatalogoButtonXam",
             typeof(bool),
             typeof(ButtonXam),
             defaultValue:false,
             propertyChanged:MuestraCatalogoChanged);

        private static void MuestraCatalogoChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (ButtonXam)bindable;
            if (control != null)
            {
                control.MuestraCatalogoButtonXam = (bool)newValue;
            }
        }

        public bool MuestraCatalogoButtonXam
        {

            set { SetValue(MuestraCatalogoButtonXamProperty, value); }
            get { return (bool)GetValue(MuestraCatalogoButtonXamProperty); }
        }

        public static readonly BindableProperty IsEnabledButtonXamProperty =
             BindableProperty.Create("IsEnabledButtonXam",
                 typeof(EstadoBotonXam),
                 typeof(ButtonXam),
                 defaultValue: EstadoBotonXam.Normal,
                 propertyChanged: IsEnabledChanged);


        private async static void IsEnabledChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (ButtonXam)bindable;
            if (control != null)
            {
                EstadoBotonXam valor = (EstadoBotonXam)newValue;
                if (valor == EstadoBotonXam.Deshabilitado)
                {
                    control.Source = control.ImageDisabledButtonXam;
                    await control.ScaleTo(0.8, 300, Easing.Linear);
                    control.IsEnabled = false;
                }
                else
                {
                    control.Source = control.ImageEnabledButtonXam;
                    await control.ScaleTo(1, 300, Easing.Linear);
                    control.IsEnabled = true;
                }
            }
        }

        public EstadoBotonXam IsEnabledButtonXam
        {

            set { SetValue(IsEnabledButtonXamProperty, value); }
            get { return (EstadoBotonXam)GetValue(IsEnabledButtonXamProperty); }
        }

        public static readonly BindableProperty ImageDisabledButtonXamProperty =
           BindableProperty.Create("ImageDisabledButtonXam",
               typeof(ImageSource),
               typeof(ButtonXam));

        public ImageSource ImageDisabledButtonXam
        {

            set { SetValue(ImageDisabledButtonXamProperty, value); }
            get { return (ImageSource)GetValue(ImageDisabledButtonXamProperty); }
        }


        public static readonly BindableProperty ImageEnabledButtonXamProperty =
         BindableProperty.Create("ImageEnabledButtonXam",
             typeof(ImageSource),
             typeof(ButtonXam));

        public ImageSource ImageEnabledButtonXam
        {
            set { SetValue(ImageEnabledButtonXamProperty, value); }
            get { return (ImageSource)GetValue(ImageEnabledButtonXamProperty); }
        }
         
        //public ButtonXam()
        //{
        //    //+= ButtonXam_Clicked;
        //   // this.MuestraCatalogoButtonXam = MuestraCatalogoButtonXamProperty;
        //}

        //private void ButtonXam_Clicked(object sender, EventArgs e)
        //{
        //    if(MuestraCatalogoButtonXam)
        //    {
        //        Navigation.PushAsync(new CatalogoGenerico());
        //    }
        //}
    }
}
