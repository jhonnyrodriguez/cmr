﻿using System;
using System.IO;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Environment = System.Environment;
using Xamarin.Forms;
using CarouselView.FormsPlugin.Android; 

namespace IDigitales.Pagina.Movile.App.Droid
{
    [Activity(Label = "IDigitales.Pagina.Movile.App", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(savedInstanceState);
            //SetContentView(Resource.Layout.Main);
            //Forms.SetFlags("CollectionView_Experimental");
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            CarouselView.FormsPlugin.Android.CarouselViewRenderer.Init();

            //aqui se manda llamar nuestra app desde android
            //vamos asignar el nombre a nuestra base
            string nombreArchivo = "baseDatos.sqlite";
            var ruta = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var rutabd = Path.Combine(ruta, nombreArchivo);
            LoadApplication(new App(rutabd));
        }
    }

   
}