﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IDigitales.Pagina.Web.Comun.Idioma.Partials {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ProjRadiografiaARIXRAD {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ProjRadiografiaARIXRAD() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("IDigitales.Pagina.Web.Comun.Idioma.Partials.ProjRadiografiaARIXRAD", typeof(ProjRadiografiaARIXRAD).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alimentación a una toma estándar de 120 V., haciendo innecesarios costosos trabajos de preinstalación (opcional)..
        /// </summary>
        public static string AlimentacionEstandar {
            get {
                return ResourceManager.GetString("AlimentacionEstandar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consola de adquisición sensible al tacto, para una operación fácil e intuitiva (opcional)..
        /// </summary>
        public static string ConsolaAdquisicion {
            get {
                return ResourceManager.GetString("ConsolaAdquisicion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to La consola de control cuenta con programador anatómico por región, proyección y complexión del paciente, lo que facilita una selección de la técnica radiográfica apropiada en forma rápida y precisa..
        /// </summary>
        public static string ConsolaControl {
            get {
                return ResourceManager.GetString("ConsolaControl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Descripción de producto.
        /// </summary>
        public static string DescripcionProducto {
            get {
                return ResourceManager.GetString("DescripcionProducto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Desplazamiento vertical del Bucky hasta el piso lo que permite la toma de radiografías de miembros inferiores en pacientes de pie..
        /// </summary>
        public static string DesplazamientoVertical {
            get {
                return ResourceManager.GetString("DesplazamientoVertical", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Detector digital de estado sólido para Bucky de mesa o pared portátil o fijo..
        /// </summary>
        public static string DetectorDigital {
            get {
                return ResourceManager.GetString("DetectorDigital", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manivelas localizadas en ambos lados del panel de control, las cuales permiten un posicionamiento ágil del tubo de rayos X..
        /// </summary>
        public static string ManivelasLocalizadas {
            get {
                return ResourceManager.GetString("ManivelasLocalizadas", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Panel de control frontal con pantalla touch screen para el control de la mesa y la selección de las técnicas radiográficas..
        /// </summary>
        public static string PanelControl {
            get {
                return ResourceManager.GetString("PanelControl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Si está pensando en adquirir un sistema de radiología, el sistema ARiX RAD de CMR es su mejor opción..
        /// </summary>
        public static string SiEstaPensando {
            get {
                return ResourceManager.GetString("SiEstaPensando", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to La sincronización automática de los movimientos del Bucky y el tubo de rayos X se lleva a cabo tanto para el Bucky de mesa como para el Bucky de pared..
        /// </summary>
        public static string SincronizacionAutomatica {
            get {
                return ResourceManager.GetString("SincronizacionAutomatica", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to El sistema  ARiX RAD está desarrollado para aplicaciones de radiografía general de esqueleto, abdomen, tórax, entre otras. El sistema puede ser configurado con uno o más detectores digitales. Sus movimientos motorizados le ayudan a posicionar automáticamente la altura de la mesa para recibir pacientes en camilla o pacientes que llegan de pie a la sala de examen..
        /// </summary>
        public static string SistemaArixRadDesarrollado {
            get {
                return ResourceManager.GetString("SistemaArixRadDesarrollado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to El sistema ARiX RAD provee movimientos automatizados y seguimiento electrónico del Bucky de mesa y pared lo que le permite realizar estudios en forma rápida, aumentando su productividad más allá que cualquier otro sistema. Una característica común de estos sistemas es su forma de operación simple. Su consola de control cuenta con programador anatómico por región, proyección y complexión del paciente, lo que facilita una selección de la técnica radiográfica apropiada en forma rápida y precisa..
        /// </summary>
        public static string SistemaArixRadProvee {
            get {
                return ResourceManager.GetString("SistemaArixRadProvee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sistema totalmente automatizado con seguimiento electrónico del tubo  de rayos X y el Bucky de mesa y pared. Con sólo presionar un comando la mesa se posiciona a la altura adecuada para recibir pacientes en camilla o bien para recibir pacientes de pie..
        /// </summary>
        public static string SistemaTotalmente {
            get {
                return ResourceManager.GetString("SistemaTotalmente", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En su versión digital los sistemas incorporan funciones avanzadas de post-proceso como son: ángulo de Cobb, ángulo por tres puntos, ángulo doble, líneas paralelas, índice cardiotorácico, medición de Ferguson,  identificación de imágenes clave, notación semi-automática de vértebras, herramienta de stitching para manipulación de imágenes de segmentos largos, trazo de eje mecánico etc..
        /// </summary>
        public static string VersionDigital {
            get {
                return ResourceManager.GetString("VersionDigital", resourceCulture);
            }
        }
    }
}
