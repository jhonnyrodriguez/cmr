﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IDigitales.Pagina.Web.Comun.Idioma.Partials {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ProjRadiografiaINTEGRIX {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ProjRadiografiaINTEGRIX() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("IDigitales.Pagina.Web.Comun.Idioma.Partials.ProjRadiografiaINTEGRIX", typeof(ProjRadiografiaINTEGRIX).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alimentación a una toma estándar de 120 V., haciendo innecesarios costosos trabajos de preinstalación (opcional)..
        /// </summary>
        public static string AlimentacionToma {
            get {
                return ResourceManager.GetString("AlimentacionToma", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Su avanzado diseño y sólida construcción hacen del sistema INTEGRIX un equipo fiable con mínimos requerimientos de mantenimiento..
        /// </summary>
        public static string AvanzadoDiseno {
            get {
                return ResourceManager.GetString("AvanzadoDiseno", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En CMR nos complacemos en presentar el sistema de radiología al alcance de su presupuesto..
        /// </summary>
        public static string CmrComplacemos {
            get {
                return ResourceManager.GetString("CmrComplacemos", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to La consola de control cuenta con programador anatómico por región, proyección y complexión del paciente, lo que facilita una selección de la técnica radiográfica apropiada en forma rápida y precisa..
        /// </summary>
        public static string ConsolaControl {
            get {
                return ResourceManager.GetString("ConsolaControl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Descripción de producto.
        /// </summary>
        public static string DescripcionProducto {
            get {
                return ResourceManager.GetString("DescripcionProducto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Por su diseño compacto, pueden ser instalados en espacios reducidos con mínimos requerimientos de espacio y preinstalación..
        /// </summary>
        public static string DisenoCompacto {
            get {
                return ResourceManager.GetString("DisenoCompacto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to El sistema puede ser configurado con una consola de control sensible al tacto, lo que facilita su operación y permite un manejo fácil e intuitivo (opcional)..
        /// </summary>
        public static string SistemaConfigurado {
            get {
                return ResourceManager.GetString("SistemaConfigurado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to El sistema INTEGRIX está diseñado para obtener imágenes  de la más alta calidad al menor costo. Una característica común de estos sistemas es su forma de operación simple. Su consola de control cuenta con programador anatómico por región, proyección y complexión del paciente, lo que facilita una selección de la técnica radiográfica apropiada en forma rápida y precisa..
        /// </summary>
        public static string SistemaIntegrixDisenado {
            get {
                return ResourceManager.GetString("SistemaIntegrixDisenado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to El sistema INTEGRIX puede instalarse en espacios reducidos y sus requerimientos de preinstalación y mantenimiento son mínimos, además  puede ser configurado con uno o dos detectores digitales..
        /// </summary>
        public static string SistemIntegrixInstalarse {
            get {
                return ResourceManager.GetString("SistemIntegrixInstalarse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En su versión digital los sistemas incorporan funciones avanzadas de post-proceso como son: ángulo de Cobb, ángulo por tres puntos, ángulo doble, líneas paralelas, índice cardiotorácico, medición de Ferguson,  identificación de imágenes clave, notación semi-automática de vértebras, herramienta de stitching para manipulación de imágenes de segmentos largos, trazo de eje mecánico etc..
        /// </summary>
        public static string VersionDigital {
            get {
                return ResourceManager.GetString("VersionDigital", resourceCulture);
            }
        }
    }
}
