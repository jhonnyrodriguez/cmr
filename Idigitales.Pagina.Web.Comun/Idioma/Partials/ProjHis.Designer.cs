﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IDigitales.Pagina.Web.Comun.Idioma.Partials {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ProjHis {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ProjHis() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("IDigitales.Pagina.Web.Comun.Idioma.Partials.ProjHis", typeof(ProjHis).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apegado a las normas mexicanas NOM004-SSA3-2012 y NOM024-SSA3-2012 sobre el Expediente Clínico Electrónico..
        /// </summary>
        public static string Apegado {
            get {
                return ResourceManager.GetString("Apegado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Descripción de producto.
        /// </summary>
        public static string DescripcionProducto {
            get {
                return ResourceManager.GetString("DescripcionProducto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nuestro  Sistema de Información Hospitalaria INFOSALUD está diseñado para gestionar todas las áreas funcionales del hospital con la finalidad de mejorar la atención del paciente. El sistema  registra todos los procesos de la atención del paciente dentro de las áreas administrativa y médica mediante la incorporación de un Expediente Clínico Electrónico EMESALUD..
        /// </summary>
        public static string NuestroSistema {
            get {
                return ResourceManager.GetString("NuestroSistema", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permite la gestión integral de todos los servicios, para mantener centralizada y accesible toda la información médica y administrativa de un paciente.
        ///              Además optimiza los recursos y procesos hospitalarios permitiendo una rápida reorganización y reorientación de los recursos..
        /// </summary>
        public static string PermiteGestion {
            get {
                return ResourceManager.GetString("PermiteGestion", resourceCulture);
            }
        }
    }
}
