﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDigitales.Pagina.Web.Comun.Enums
{
    public enum OperacionResultado
    {
        //Errores negativos
        Existente = -2,
        Error = -1,
        //Positivo correctos
        Ok = 0,
        Actualizado = 1,

    }
}
