﻿//////////////////////////////////////////////////////////////////////
//*********************Fecha*********************
////////////////////////////////////////////////////////////////////////


function fechaJsonMADate(fstr) {
    if (typeof fstr == "undefined" || fstr === "") return "";
    var date;
    if (fstr.indexOf('Date') > -1)
        date = new Date(parseInt(fstr.substr(6)));
    else
        date = new Date(fstr);

    var fecha = "";
    //return date.toLocaleDateString();
    if ($.cookie("_cultura") == 'es') {
        fecha = date.format("dd/mm/yyyy");
    } else {
        fecha = date.format("mm/dd/yyyy");
    }
    return fecha;
}

//////////////////////////////////////////////////////////////////////
//*********************formato segun cultura*********************
////////////////////////////////////////////////////////////////////////
function fechaFormatoCultura(date) {
    var fecha = "";
    if ($.cookie("_cultura") == 'es') {
        fecha = date.format("dd/mm/yyyy");
    } else {
        fecha = date.format("mm/dd/yyyy");
    }
    return fecha;
}

//////////////////////////////////////////////////////////////////////
//*********************Funciones para el data picker*********************
////////////////////////////////////////////////////////////////////////
function ObtenerLenguaje() {
    var lenguaje=$.cookie("_cultura").indexOf('us') > -1 ? "en-us" : "es-mx";
    return lenguaje;
}


//////////////////////////////////////////////////////////////////////
//*********************Diferencia de fechas*********************
////////////////////////////////////////////////////////////////////////
function diferenciafechas(fa, fb) {  //fa y fb dos fechas

    var totdias = fa - fb;
    totdias /= 3600000;
    totdias /= 24;
    totdias = Math.floor(totdias);
    totdias = Math.abs(totdias);

    var ans, meses, dias, m2, m1, d3, d2, d1;
    var f2 = new Date(); var f1 = new Date();

    if (fa > fb) { f2 = fa; f1 = fb; } else { var f2 = fb; f1 = fa; }  //Siempre f2 > f1
    ans = f2.getFullYear() - f1.getFullYear(); // dif de años inicial
    m2 = f2.getMonth();
    m1 = f1.getMonth();
    meses = m2 - m1; if (meses < 0) { meses += 12; --ans; }

    d2 = f2.getDate();
    d1 = f1.getDate();
    dias = d2 - d1;

    var f3 = new Date(f2.getFullYear(), m2, 1);
    f3.setDate(f3.getDate() - 1);
    d3 = f3.getDate();

    if (d1 > d2) {
        dias += d3; --meses; if (meses < 0) { meses += 12; --ans; }
        if (fa > fb) {  //corrección por febrero y meses de 30 días
            f3 = new Date(f1.getFullYear(), m1 + 1, 1);
            f3.setDate(f3.getDate() - 1);
            d3 = f3.getDate();
            if (d3 == 30) dias -= 1;
            if (d3 == 29) dias -= 2;
            if (d3 == 28) dias -= 3;
        }
    }

    return { ans: ans, meses: meses, dias: dias, Tdias: totdias };
}

//////////////////////////////////////////////////////////////////////
//*********************formatos de fechas*********************
////////////////////////////////////////////////////////////////////////

var dateFormat = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: pad(d),
                ddd: dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m: m + 1,
                mm: pad(m + 1),
                mmm: dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy: String(y).slice(2),
                yyyy: y,
                h: H % 12 || 12,
                hh: pad(H % 12 || 12),
                H: H,
                HH: pad(H),
                M: M,
                MM: pad(M),
                s: s,
                ss: pad(s),
                l: pad(L, 3),
                L: pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? "a" : "p",
                tt: H < 12 ? "am" : "pm",
                T: H < 12 ? "A" : "P",
                TT: H < 12 ? "AM" : "PM",
                Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default": "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};

Date.prototype.local = function () {
    return moment(this).format("L");
};

Date.prototype.localHora = function () {
    return moment(this).format("LT");
};

var datepicker_regional_us = { closeText: "Done", prevText: "Prev", nextText: "Next", currentText: "Today", monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], weekHeader: "Wk", dateFormat: "mm/dd/yy", firstDay: 0, isRTL: false, showMonthAfterYear: false, yearSuffix: "" };

var datepicker_regional_es = { closeText: 'Cerrar', prevText: '<Ant', nextText: 'Sig>', currentText: 'Hoy', monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'], dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'], dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'], dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'], weekHeader: 'Sm', dateFormat: 'dd/mm/yy', firstDay: 1, isRTL: false, showMonthAfterYear: false, yearSuffix: '' };

