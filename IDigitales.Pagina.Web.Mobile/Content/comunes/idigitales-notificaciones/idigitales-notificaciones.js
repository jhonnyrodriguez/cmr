﻿ 
(function ($) {
    $.idigitalesNotificaciones = function (titulo, mensaje, tipo, tiempo, callback) {
        
        var idigitalesNotificaciones = Object.create(iDigitalesNotificaciones);
        idigitalesNotificaciones.crearNotificacion($("body"), titulo, mensaje, tipo, tiempo, callback);
        return idigitalesNotificaciones;
 
    };

    $.fn.idigitalesNotificaciones = function (titulo, mensaje, tipo, tiempo, callback) {
        var $el  = this.first();
        var idigitalesNotificaciones = Object.create(iDigitalesNotificaciones);
        idigitalesNotificaciones.crearNotificacion($el, titulo, mensaje, tipo, tiempo, callback, true);
        return idigitalesNotificaciones;  
    };

    $.idigitalesNotificaciones.configurar = function (opts) {
        __Options = $.extend(true, {}, __Options, opts);
    };

    var __Options ={
        baseClass: "idigitales-notificaciones",
        extraClass: "",
        elementClass: "idigitales-notificacion-item",
        customTemplate: false,
        baseTime: 2,
        defaultType: "notification",
        icon: {
            error: "fa fa-exclamation-circle",
            alert: "fa fa-exclamation-triangle",
            success: "fa fa-check-circle-o",
            notification: "fa fa-bell"
        }
    }



    var iDigitalesNotificaciones = {
        crearNotificacion: function ($el, titulo, mensaje, tipo, tiempo, callback, inner) {

            tipo = tipo || __Options.defaultType;
            var wrap = $el.children("." + __Options.baseClass);
            if (wrap.length == 0) {
                wrap = "<div class='" + __Options.baseClass + "' ></div>";
                $el.append(wrap);
                wrap = $el.find("." + __Options.baseClass);
                if (inner) {
                    wrap.css({ "position": "absolute" });
                    wrap.addClass("idigitales-notificacion-wrapped");
                    wrap.parent().css({ "position": "relative", "overflow": "hidden" });
                }
            }

            

            var notificacion = this.getTemplateNotif(titulo, mensaje, tipo);
            this.elemento = notificacion;
            wrap.append(notificacion);
            setTimeout(function () {
                notificacion.addClass("idigitales-notificacion-visible");
            }, 10);
            this.publicFunctions();
            this.addEvents(notificacion, tiempo, callback);
            //.css({left:left})  // Set the left to its calculated position
            // .animate({"left":"0px"}, "slow");
        },

        removeElement: function ($elem, callback, base) {
            try {
                base.elemento = undefined;
                $elem.off();
                clearTimeout($elem.data("timeNotif"));
                $elem.remove();
                $elem = undefined;
                callback && callback();
                callback = undefined;
            } catch (e) {
                console.log(e);
            }
        },

        addEvents: function ($elem,tiempo,callback) {
            var base = this;
            tiempo = tiempo || __Options.baseTime;
            setTimeout(function () {
                base.removeElement($elem, callback, base);
            }, tiempo * 1000);

            $elem.on("click", function () {
                base.removeElement($elem, callback,base); 
            })
        },

        getTemplateNotif: function (titulo, mensaje, tipo) {
            var tmpl =""
            if (!__Options.customTemplate) {
                tmpl = '<div class=" ' + __Options.elementClass +'  '+__Options.extraClass + ' idigitales-notificacion-' + tipo + '">' +
                '  <div><div class="iconoCloseNotificaciones"><div></div></div>' +
                '      <div class="iconoNotificaciones">' +
                '          <div class="' + __Options.icon[tipo] + '"></div>' +
                '      </div>' +
                '      <div class="textoNotificaciones">' +
                '          <div style=" font-weight: bold; ">' + titulo + '</div>' +
                '          <div>' + mensaje + '</div>' +
                '      </div>' +
                '  </div>' +
                '</div>';
            }
            return $(tmpl);
        },

        publicFunctions: function () {
            var base = this;
            base.close = function () {
                if (base.elemento) {
                    try{
                        base.elemento.click();
                    }catch(e){ }
                }
                return undefined;
            }
        }

    }

    //$.idigitalesNotificaciones.defaults = {
    //    position: ["top-right"]
    //};
 

})(jQuery)