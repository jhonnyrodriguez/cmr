﻿var PaginaWeb = PaginaWeb || {};
PaginaWeb.Comunes = PaginaWeb.Comunes || {};
PaginaWeb.Comunes.IDigitalesCuadricula = function (el, params) {

    var _$el = $(el);
    var _$contenedor = null;
   

    var _params = params;

    var _classCuadrito = 'idigitales-cuadricula-cuadrito';
    


    var self = {
        iniciar: function() {
            _$contenedor = self._crearContenedor();
            _$el.append(_$contenedor);

            self._agregarEventos();
        },

        agregarCuadro: function (html) {
            var nuevoCuadrito = $("<div class='" + _classCuadrito + "'></div>");

            nuevoCuadrito.append(html);
            _$contenedor.append(nuevoCuadrito);

            nuevoCuadrito.bind('click', self._clickCuadrito);
        },

        eliminarCuadroSeleccionado: function () {
            var $seleccionado = self._obtenerCuadritoSeleccionado();

            if ($seleccionado.length <= 0) return;

            if (self.onEliminar instanceof Function)
                self.onEliminar($seleccionado);

            self._obtenerCuadritoSeleccionado().remove();
        },

        finalizar: function () {
            self._quitarEventos();
            _$contenedor.empty();
            _$contenedor = null;

        },

        _deseleccionar: function() {
            self._obtenerCuadritos().removeClass('seleccionado');
        },

        _obtenerCuadritos:function() {
            return _$el.find('.' + _classCuadrito);
        },

        _obtenerCuadritoSeleccionado: function() {
            return _$el.find('.' + _classCuadrito + ".seleccionado");
        },

        _crearContenedor:function() {
            return $("<div class='idigitales-cuadricula-contenedor'></div>");
        },

        /*Eventos */
        onEliminar: null,

        _clickCuadrito: function () {
            var cuadrito = $(this);

            if (cuadrito.hasClass("seleccionado")) {
                cuadrito.removeClass("seleccionado");
            } else {
                self._deseleccionar();
                $(this).addClass('seleccionado');
            }
           
        },

        _agregarEventos: function() {
            _$el.find('.' + _classCuadrito).bind('click', self._clickCuadrito);

        },

        _quitarEventos: function() {
            _$el.find('.' + _classCuadrito).unbind('click', self._clickCuadrito);
        }
        /*fin eventos */

    };

    self.iniciar();

    return self;
}