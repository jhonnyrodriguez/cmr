﻿
var His = His || {};
His.Controles = His.Controles || {}
His.Controles.Validador = His.Controles.Validador || function(msgs) {
    //Expresiones regulares utilizadas
    var ruleRegex = /^(.+?)\[(.+)\]$/,
       numericRegex = /^[0-9]+$/,
       integerRegex = /^\-?[0-9]+$/,
       decimalRegex = /^\-?[0-9]*\.?[0-9]+$/,
       emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
       alphaRegex = /^[a-z]+$/i,
       alphaNumericRegex = /^[a-z0-9]+$/i,
       alphaDashRegex = /^[a-z0-9_\-]+$/i,
       naturalRegex = /^[0-9]+$/i,
       naturalNoZeroRegex = /^[1-9][0-9]*$/i,
       ipRegex = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
       base64Regex = /[^a-zA-Z0-9\/\+=]/i,
       numericDashRegex = /^[\d\-\s]+$/,
       urlRegex = /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,
       dateRegex = /\d{4}-\d{1,2}-\d{1,2}/;

    var mensajes = msgs|| {
        required: 'El campo es obligatorio',
        email: 'Inserte un email valido',
        valid_emails: '',
        minlength: 'La longitud debe ser minimo de %s caracteres.',
        maxlength: 'La longitud debe ser maximo de %s caracteres.',
        length: 'La longitud debe ser de %s caracteres.',
        exact_length: 'La longitud debe ser maximo de %s caracteres.',
        min: 'El campo debe contener un numero mayor o igual a %s.',
        max: 'El campo debe contener un numero menor o igual a %s.',
        alpha: 'El campo solo puede contener letras',
        alpha_numeric: 'El campo debe de contener solo caracteres alfanumericos',
        numeric: 'El campo debe contener solo numeros',
        integer: 'El campo debe contener solo numeros enteros',
        decimal: 'El campo debe contener numero decimal',
        ip: 'El campo debe contener una ip valida.',
        base64: 'El campo debe contener una cadena en base64',
        filetype: 'El campo debe contener solo archivos',
        url: 'El campo debe contener una URL valida.',
        greater_than_date: 'La fecha debe ser mayor a %s.',
        less_than_date: 'La fecha debe ser menor a %s.',
        greater_than_or_equal_date: 'La fecha debe ser mayor o igual a %s.',
        less_than_or_equal_date: 'La fecha debe ser menor o igual a %.',
        pattern: "Fomato no valido"
    };

    //Funciones comunes

    /**
     * Recupera el mensaje apartir de un objeto validacion campo
     * @param validacion 
     * @returns mensaje 
     */
    var getMensaje = function(validacion) {
        var msj = "El valor no es valido ";
        try {
            var defaultmsj = mensajes[validacion.nombre];
            if (defaultmsj) {
                msj = defaultmsj.replace("%s", validacion.attr);
            }
        } catch (e) {
            console.log(e);
        } 
        return msj;
    };
    /**
     * Itera sobre alguna coleccion
     * @param {} campos 
     * @param {} operacionCallback 
     * @returns {} 
     */
    var iterar = function(campos, operacionCallback) {
        for (var i = 0; i < campos.length; i++) {
            operacionCallback(campos[i]);
        }
    };
    /**
     * Obtiene un atributo de unn campo
     * @param {} campo 
     * @param {} attr 
     * @returns {} 
     */
    var getAtributo = function (campo, attr) {
        if (!attr)return "";
        return campo.getAttribute(attr);
    };
    
    function isHidden(el) {
        var style = window.getComputedStyle(el);
        return ((style.display === 'none') || (style.visibility === 'hidden'));
    }
   
    /**
     * Clase que contiene la informacion de una validacion del campo
     * @param {} nombre 
     * @param {} funcion 
     * @param {} campo 
     * @returns {} 
     */
    var validacionCampo = function (nombre, funcion,campo) {
        this.nombre = nombre;
        this.campo = campo;
        this.attr = "";
        
        if (funcion && typeof funcion === "function")
            this.funcion = funcion;
        else
            throw "El parametro numero 2 debe de ser una funcion";

        this.verificar = function () {
            this.attr = getAtributo(this.campo, this.nombre);
            return this.funcion.call(this, this.campo, this.attr);
        };
    };

   
    /**
     * Clase para contener todas las validaciones y valida campo
     * @param {} campo 
     * @param {} validaciones 
     * @returns {} 
     */
    var campoValidaciones = function (campo, validaciones,ctx) {
        this.ctx = ctx;
        var self = this;
        if (validaciones && validaciones.lenght > 0 && !(validaciones[0] instanceof validacionCampo))
            throw "El parametro debe de ser de tipo " + Object.prototype.toString.call(validacionCampo);
        this.campo = campo;
        this.validacionesCampo = validaciones || [];
        this.mensajes = [];
        this.agregarValidacion = function (nombre, funcion,campo) {
            var valid = new validacionCampo(nombre, funcion, campo);
            this.validacionesCampo.push(valid);
        }
        this.agregarValidaciones = function (validacionesCampoArray) {
            if (validacionesCampoArray && validacionesCampoArray.lenght > 0 && !(validacionesCampoArray[0] instanceof validacionesCampoArray))
                throw "El parametro debe de ser de tipo " + Object.prototype.toString.call(validacionCampo);
            this.validacionesCampo.concat(validacionesCampoArray);
        }
        this.validarCampo = function () {
            self.mensajes.splice(0, self.mensajes.length);
            iterar(this.validacionesCampo, function (validacion) {
                var valido = validacion.verificar();
                if (!valido) {
                    self.mensajes.push(getMensaje(validacion));
                }
            });
            //Mostrar mensajes
            this.mostrarMensajes();
            return self.mensajes.length === 0;
            //return self.mensajes;
        }
        this.mostrarMensajes = function (mensajesValidacion) {
            var ctrl = $(this.campo);
            var msgs = mensajesValidacion || this.mensajes;
            if (msgs.length > 0) {
                var msg = msgs.toString();

                //Evento para revalidar
                if (this.ctx) {
                    if (this.campo.tagName === "INPUT") {
                        ctrl.off("focusout", this.ctx.RevalidarCampo).on("focusout", this.ctx.RevalidarCampo);
                        ctrl.siblings(".imgErrorValidacion").remove(); //.tooltip("destroy").remove();
                    } else if (this.campo.tagName === "SELECT") {
                        ctrl.off("change", this.ctx.RevalidarCampo).on("change", this.ctx.RevalidarCampo);
                        ctrl = ctrl.siblings(".select2").find(".select2-selection__rendered");
                        ctrl.siblings(".imgErrorValidacion").remove(); //.tooltip("destroy").remove();
                    } else if (this.campo.tagName === "TEXTAREA") {
                        ctrl.off("focusout", this.ctx.RevalidarCampo).on("focusout", this.ctx.RevalidarCampo);
                        ctrl.siblings(".imgErrorValidacion").remove(); //.tooltip("destroy").remove();
                    } else if (this.campo.tagName === "DIV" && this.campo.className.indexOf("idigitales-validador-multiple") > -1 ) {
                        ctrl.off("focusout", this.ctx.RevalidarCampo).on("focusout", this.ctx.RevalidarCampo);
                        ctrl.siblings(".imgErrorValidacion").remove(); //tooltip("destroy").remove();
                    }
                }

                //Tamaño para posicion
                var pos = self.calcularPosicion(ctrl);


                //Se pone esta comparacion por un campo especial
                if (this.campo.tagName === "DIV") {
                    if (this.campo.className.indexOf("idigitales-validador-multiple") > -1)
                    {
                        //Agregar imagen
                        $(' <i class="fa fa-exclamation-triangle imgErrorValidacion tooltipValidador" aria-hidden="true"><span class="tooltiptext">' + msg + '</span></i> ').css("left", pos.left).css("top", pos.top).insertAfter(ctrl);
                        //$(".imgErrorValidacion").data("title", msg).tooltip({ placement: "left", selector: false, trigger: "click hover focus" });
                    }
                } else
                {
                    //Agregar imagen
                    $(' <i class="fa fa-exclamation-triangle imgErrorValidacion tooltipValidador" aria-hidden="true"><span class="tooltiptext">' + msg + '</span></i> ').css("left", pos.left).css("top", pos.top).insertAfter(ctrl);
                   // $(".imgErrorValidacion").data("title", msg).tooltip({ placement: "left", selector: false, trigger: "click hover focus" });
                }
                
                
                return false;

            } else {
                if (this.campo.tagName === "INPUT" && ctrl.siblings(".imgErrorValidacion").length >0) {
                    ctrl.siblings(".imgErrorValidacion").remove();//.tooltip("destroy").remove();
                } else if (this.campo.tagName === "SELECT") {                   
                    ctrl = ctrl.siblings(".select2").find(".select2-selection__rendered");
                    ctrl.siblings(".imgErrorValidacion").remove();//.tooltip("destroy").remove();
                }
                else if (this.campo.tagName === "DIV" && this.campo.className.indexOf("idigitales-validador-multiple") > -1) {
                    ctrl.siblings(".imgErrorValidacion").remove();//.tooltip("destroy").remove();
                }
                return true;
            }
        }

        this.cambiar = function () { self.validarCampo(); }
      
        this.calcularPosicion = function (ctrl) {
            var arriba = 5;
            var izquierda = -15;
            var ancho = 0;
            var posicion= ctrl.position();
            
            if (ctrl.is("select")) {
                var sel = ctrl.siblings(".select2").find(".select2-selection__rendered");
                if (sel)
                    ancho = sel.width();
            }
            else
            ancho = ctrl.width();

           
            var left = posicion.left + ancho + izquierda;
            var top = posicion.top + arriba;
            
            return { left: left, top: top};
        }
    };
    
    /**
     * Objeto Validador
     */
    var obj = {
        validar: function (elemento) {
            var self=this;
            var camposResultadosValidaciones = [], cont;

            if (typeof elemento === 'object')
                cont = elemento; //.getElementsByTagName("*");
            else
                cont = document.getElementById(elemento);

            if (cont === undefined || cont === null)return true;

            var campos = cont.querySelectorAll("input:not([style*='display: none']),select,textarea");
           
            iterar(campos, function (campo) {
                var valCampo = self._getValidaciones(campo, cont);
                camposResultadosValidaciones.push(valCampo.validarCampo());
            });
            return camposResultadosValidaciones.indexOf(false)===-1;
        },
        validarCampos: function (elemento) {
            var self = this;
            var camposResultadosValidaciones = [], cont;

            if (typeof elemento === 'object')
                cont = elemento; //.getElementsByTagName("*");
            else
                cont = document.getElementById(elemento);

            if (cont === undefined || cont === null) return true;
            
            var campos = cont.querySelectorAll("input,select,textarea,div");

            iterar(campos, function (campo) {
                var valCampo = self._getValidaciones(campo, cont);
                var valido = valCampo.validarCampo();
                if (!valido)
                    camposResultadosValidaciones.push(valCampo.campo);
            });
            return camposResultadosValidaciones;
        },
        limpiar: function (elemento) {
            var self = this;
            var cont;

            if (typeof elemento === 'object')
                cont = elemento; //.getElementsByTagName("*");
            else
                cont = document.getElementById(elemento);
            
            //$(cont).find(".imgErrorValidacion").each(function (index, value) { 
            //    $(this).tooltip("destroy").remove();
            //});

            if (cont === undefined || cont === null) return true;
            var campos = cont.querySelectorAll("input,select");

            iterar(campos, function (campo) {
                var ctrl = $(campo);
                if (campo.tagName === "INPUT") {
                    ctrl.off("focusout");
                    ctrl.siblings(".imgErrorValidacion").tooltip("destroy").remove();
                } else if (campo.tagName === "SELECT") {
                    ctrl.off("change", obj.RevalidarCampo);
                    ctrl = ctrl.siblings(".select2").find(".select2-selection__rendered");
                    ctrl.siblings(".imgErrorValidacion").tooltip("destroy").remove();
                }
            });

          
        },
      
        RevalidarCampo: function () {
            var valCampo = obj._getValidaciones(this);
            valCampo.validarCampo();
        },

        _getValidaciones: function (campo) {
            var validaciones = new campoValidaciones(campo, [],this);
       

        //Si es requerido
            if (campo.hasAttribute("required")) {
                validaciones.agregarValidacion("required", this._validaciones.required, campo);
            //return validaciones;//Se descomenta si solo se quiere validar 
            }

            //Caso especial para cantenedor multiple
            if (campo.tagName === "DIV") {
                var subCampo = campo.querySelectorAll(':scope > div>.idigitales-validador-multiple');
                if (subCampo.length > 0 && subCampo[0].hasAttribute("required")) {
                    validaciones.agregarValidacion("required", this._validaciones.required, subCampo[0]);
                }
            }

            //Definir validaciones de acuerdo al tipo
            validaciones = this._validacionesPorTipo(campo, validaciones);

        return validaciones;
    },
        
        _validacionesPorTipo: function (campo, validaciones) {
       
            switch (campo.type) {
            case"textarea":
            case "password":
            case "hidden":
            case"text":
                if (campo.hasAttribute("maxlength")) {
                    validaciones.agregarValidacion("maxlength", this._validaciones.maxlength, campo);
                }
                if (campo.hasAttribute("minlength")) {
                    validaciones.agregarValidacion("minlength", this._validaciones.minlength, campo);
                }
                if (campo.hasAttribute("pattern")) {
                    validaciones.agregarValidacion("pattern", this._validaciones.pattern, campo);
                }
                if (campo.hasAttribute("length")) {
                    validaciones.agregarValidacion("length", this._validaciones.length, campo);
                }
                break;
            case"number":
            case "range":
                validaciones.agregarValidacion("numeric", this._validaciones.numeric, campo);
               
                if (campo.hasAttribute("max")) {
                    validaciones.agregarValidacion("max", this._validaciones.max, campo);
                }
                if (campo.hasAttribute("min")) {
                    validaciones.agregarValidacion("min", this._validaciones.min, campo);
                }
                break;
            case "email":
                validaciones.agregarValidacion("email", this._validaciones.email, campo);
                break;
            case "date":
                validaciones.agregarValidacion("date", this._validaciones.date, campo);
                break;
            case "url":
                validaciones.agregarValidacion("url", this._validaciones.url, campo);
                break;
            case"checkbox":
            case"radio":
            case"file":
            case"datetime":
            case"time":
                break;
            case "select-one":
                break;
            case "select-multiple":
                break;
        }
        return validaciones;
        },

        _validaciones : {
            required: function(field) {
                var value = field.value;

                if ((field.type === 'checkbox') || (field.type === 'radio')) {
                    return (field.checked === true);
                }
                if (field.tagName === "DIV" && field.className.indexOf("idigitales-validador-multiple") > -1) {
                    return field.innerHTML.trim() !== "";
                }

                return (value !== null && value !== '');
            },

            email: function (field) {
                return emailRegex.test(field.value);
            },

            valid_emails: function(field) {
                var result = field.value.split(/\s*,\s*/g);

                for (var i = 0, resultLength = result.length; i < resultLength; i++) {
                    if (!emailRegex.test(result[i])) {
                        return false;
                    }
                }

                return true;
            },

            minlength: function (field, length) {
                if (!numericRegex.test(length)) {
                    return false;
                }

                return (field.value.length >= parseInt(length, 10));
            },

            maxlength: function (field, length) {
                if (!numericRegex.test(length)) {
                    return false;
                }

                return (field.value.length <= parseInt(length, 10));
            },

            length: function(field, length) {
                if (!numericRegex.test(length)) {
                    return false;
                }

                return (field.value.length === parseInt(length, 10) || field.value.length === 0);
            },

            min: function(field, param) {
                if (!decimalRegex.test(field.value)) {
                    return false;
                }

                return (parseFloat(field.value) >= parseFloat(param));
            },

            max: function(field, param) {
                if (!decimalRegex.test(field.value)) {
                    return false;
                }

                return (parseFloat(field.value) < parseFloat(param));
            },

            alpha: function(field) {
                return (alphaRegex.test(field.value));
            },

            alphanumeric: function(field) {
                return (alphaNumericRegex.test(field.value));
            },
            
            numeric: function(field) {
                return (numericRegex.test(field.value));
            },

            integer: function(field) {
                return (integerRegex.test(field.value));
            },

            decimal: function(field) {
                return (decimalRegex.test(field.value));
            },

            ip: function(field) {
                return (ipRegex.test(field.value));
            },

            base64: function(field) {
                return (base64Regex.test(field.value));
            },

            url: function(field) {
                return (urlRegex.test(field.value));
            },
            
            date: function(field) {
                var enteredDate = this._getValidDate(field.value);

                if (!enteredDate) {
                    return false;
                }

                return true;
            },

            maxdate: function(field, date) {
                var enteredDate = this._getValidDate(field.value),
                    validDate = this._getValidDate(date);

                if (!validDate || !enteredDate) {
                    return false;
                }

                return enteredDate > validDate;
            },

            mindate: function(field, date) {
                var enteredDate = this._getValidDate(field.value),
                    validDate = this._getValidDate(date);

                if (!validDate || !enteredDate) {
                    return false;
                }

                return enteredDate < validDate;
            },
            
            pattern: function (field, expr) {
                if (!field.value ||!expr)return true;
                //var patt = new RegExp(expr);
                if (eval(expr).test(field.value))
                    return true;
                return false;

            },

            _getValidDate: function(date) {
                if (!date.match("today") && !date.match(dateRegex)) {
                    return false;
                }

                var validDate = new Date(),
                    validDateArray;

                if (!date.match("today")) {
                    validDateArray = date.split("-");
                    validDate.setFullYear(validDateArray[0]);
                    validDate.setMonth(validDateArray[1] - 1);
                    validDate.setDate(validDateArray[2]);
                }

                return validDate;
            }
        }

    };
    
    return obj;

}



