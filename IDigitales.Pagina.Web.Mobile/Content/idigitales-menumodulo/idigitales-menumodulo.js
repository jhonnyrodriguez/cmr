//var His = His || {};
//His.Controles = His.Controles || {}
//His.Controles.Inspinia = function () {
    
    
(function ($) {
    

    $.fn.menuModulos = function (options) {
        options = options ? options : {};
        var opts = $.extend({}, $.fn.menuModulos.defaults, options);
        var opts_single;
        return this.each(function () {
            if ($(this).data('navMenuModulos')) {
                return true;
            }
            if (!options && $(this).data("idigitalesMenumoduloOptions"))
                opts_single = $.extend({}, $.fn.menuModulos.defaults, $(this).data("idigitalesMenumoduloOptions"));
            else
                opts_single = opts;

            var menuModulos = Object.create(menu);
            menuModulos.inicio(this, opts_single);
            $(this).data('navMenuModulos', menuModulos);
            return true;
        });
    };

    // options por defecto
    $.fn.menuModulos.defaults = {
        uniqueId: ".idigitalesMenuModulo-" + new Date().getTime(),
        id:1, // siempre pasarlo como parametro para guardar en LS
        titulo: "Men�",
        opcion: "",
        secciones: [],
        header: true,
        autoHide: false,
        multiSelection:false, // == auto (cambia la seleccion utomaticamente)
        bodyContainer: false,
        flotating:false,
        startMinimized: false,
        wrapped: true,
        autoSmall:false,
        barWidth : [70, 200],
        onMinimizar: false,
        onMaximizar: false,
        onClicOpcion: false,
        unidad: "rem"
    };

    //Objeto Menu
    var menu = {
        opcionesGlobales: {},
        inicio: function (el, options) {
            //guardar opciones globales
            opcionesGlobales = options;
            //Creamos Menu  
            this.$el = $(el);
            if(!this.$el.attr("id")){
                this.$el.attr("id",uniqueId.substring(1));
            }


            this.options = options; 
            this.crearMenu();
            //Creamos Opciones del menu
            

            this.crearOpciones();
            //Inicializamos Menu
            this.navMenu = this.$el.find("#navMenuModulo");
            var ctrl = this.$el;//.find("#navMenuModulo");
            //this.options.autoSmall && this._bodyMenuModulos(options.id, ctrl, options);
            this._cargarTitles(this);

            //Guardar padre
            var base = this;

            // Minimalize menu

            if (this.options.autoHide) {
                this.$el.mouseenter(function () {
                    ctrl.removeClass("mini-navbar");
                    base.minimizaMenu(true);
                });
                this.$el.mouseleave(function () {
                    ctrl.addClass("mini-navbar");
                    base.minimizaMenu(true);
                });
            } else {
                this.options.autoSmall && this.responsiveMenu();
                this.$el.find(".navMenu-minimalize").off('click').on("click", function () {
                    //if (!ctrl.hasClass('body-small')) {
                    base.autoMinimized = false;
                        ctrl.toggleClass("mini-navbar");
                        base.minimizaMenu();
                    //}
                });
                this.callbackMinMax();
            }

            if (options.onClicOpcion !== undefined) {
                //evento click de opciones
                this.$el.find('.navMenu-opt').click(function (e) {

                    var idopt = this.id;
                    var opt = idopt.split("_");
                    base.confirmaCambioSeccion(opt[1]);
                    options.onClicOpcion(opt[1], base.options.secciones[opt[1]], this);

                });
                if (this.options.opcion !== undefined && base.options.secciones[this.options.opcion])
                    options.onClicOpcion(this.options.opcion, base.options.secciones[this.options.opcion], this);
            }

            // set public;
            this.setSelectMenu = this._setSelectMenu;
            var numMenu = this.$el.find('.navMenu-opt').length;
            this.$el.find(".navMenu-navigation").css("min-height", (numMenu * 50) + 100 + this.options.unidad);
 
        },

        nsEvent: function (ev) {
            return (ev || '') + this.options.uniqueId;
        },

        responsiveMenu: function () {
            var base = this;
            if (Array.isArray(base.options.autoSmall)) {
                base.minH = base.options.autoSmall[1];
                base.minW = base.options.autoSmall[0];
            } else {
                base.minW = base.minH = base.options.autoSmall;
            }
            base.timeOutResize = undefined;
            $(window).bind(base.nsEvent('resize'), function () {
                clearTimeout(base.timeOutResize);
                base.timeOutResize = setTimeout(function () {
                    console.log("resized");
                    base.checkWindowSize(base);
                }, 300);
                
            });

        },
        
        checkWindowSize: function (base) {
           
            var hWin = base.minH ? $(window).outerHeight() : 1;
            var wWin = base.minW ? $(window).outerWidth() : 1;
            if (hWin <= base.minH || wWin < base.minW) {
                if (!base.$el.hasClass("mini-navbar")) {
                    base.$el.addClass("mini-navbar");
                    base.minimizaMenu();
                    base.autoMinimized = true;
                }
            } else if (base.autoMinimized) {
                base.autoMinimized = false;
                base.$el.removeClass("mini-navbar");
                base.minimizaMenu();
            }
        },

        crearStylesheet: function(){
            var opts = this.options;
            var base = this;
            var styleC = '<style>\n';
            if (opts.bodyContainer) {
                if (!opts.flotating) {
                    styleC +=
                     opts.bodyContainer + ' {\n' +
                     '    padding-left: ' + opts.barWidth[1] +  this.options.unidad+' ;\n' +
                     '    background-color: transparent;\n' +
                     '    -webkit-transition: all 0.5s;\n' +
                     '    -moz-transition: all 0.5s;\n' +
                     '    -o-transition: all 0.5s;\n' +
                     '     transition: all 0.5s;\n' +
                     ' }\n' +
                     '.mini-navbar + ' + opts.bodyContainer + ' {\n' +
                     '    padding-left: ' + opts.barWidth[0] +  this.options.unidad+' !important;\n' +
                     '}\n';
                } else {
                    styleC +=
                    opts.bodyContainer + ' {\n' +
                    '    padding-left: ' + opts.barWidth[0] +  this.options.unidad+' ;\n' +
                    '    background-color: transparent;\n' +
                    ' }\n';
                }
            }
            var idEl="#"+base.$el.attr("id");
            
            styleC += idEl + '.mini-navbar .navMenu-navigation {\n' +
                '   width: ' + opts.barWidth[0] +  this.options.unidad+' ;\n' +
                '   overflow: hidden;\n' +
                '}\n';

            styleC += idEl + '.mini-navbar .navMenu-container > li > a i {\n' +
                '   font-size:25' + this.options.unidad + ';\n' +
                '}\n';

            
            styleC += idEl + '.mini-navbar .navMenu-element {\n' +  
                '   display: none;\n' +
                '   opacity:0;\n' +
                '}\n';
           
            styleC += idEl + '.mini-navbar .navMenu-logo {\n' +
                '   display: block;\n' +
                '}\n';
            
            styleC += idEl + '.mini-navbar .navMenu-header {\n' +
                '   padding: 0; \n' +
                '}\n';


            styleC += idEl + '.mini-navbar .navMenu-label {\n' +
                '   margin-left: 15rem; \n' +
                '   width: 0%; \n' +
                '   opacity: 0; \n' +
                '} \n';

            styleC += idEl + ' .navMenu-navigation {\n' +
                '   width: ' + opts.barWidth[1] +  this.options.unidad+' ;\n' +
                '}\n';


            styleC += idEl + ' .navMenu-logo {\n' +
                '   text-align: center;\n' +
                '   font-size: 30' + this.options.unidad + ';\n' +
                '   display: none;\n' +
                '   padding: 10' + this.options.unidad + ' 0 0 0;\n' +
                '}\n';
            styleC += '</style>\n';
            return styleC;
        },

        callbackMinMax: function () {
            var base = this;
            base.state = undefined;
            base.$el.on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function () {
                var newState = base.$el.hasClass("mini-navbar")
                if (base.state !== newState) {
                    base.state = newState;
                    if (newState) {
                        base.options.onMinimizar && base.options.onMinimizar();
                    } else {
                        base.options.onMaximizar && base.options.onMaximizar();
                    } 
                }
            });
            

        },
 

        crearMenu: function () {
            var opts = this.options;
            var base = this;
            
            
            var estadomenu = localStorage.getItem("navMenuModulos_" + base.options.id);
            

            var menuHtml =
                //'<div id="navMenuModulo" >'+
                    '<nav class="navMenu-navigation" role="navigation">' +
                        '<div>' +
                            '<ul class="navMenu-container" id="side-menu">';
            if (opts.header) {
                menuHtml += '<li class="navMenu-header">' +
                 '<div class="navMenu-element">' +
                     '<a class="navMenu-minimalize">' +
                         opts.titulo +
                         '<i id="botonHamburguesa" class="icon-menu c-hamburger c-hamburger--htra">' +
                             '<span id="contenedorHamburguesa"></span>' +
                         '</i>' +
                     '</a>' +
                 '</div>' +
                 '<div class="navMenu-logo">' +
                     '<a class="navMenu-minimalize"><i class="icon-menu"></i></a>' +
                 '</div>' +
            '</li>';
            }


            menuHtml += '</ul>' +
                         '</div>' +
                    '</nav>' + base.crearStylesheet();//+
            //'</div>';
           
            base.$el.html(menuHtml);
            if (opts.startMinimized || estadomenu === "mini") {
                base.$el.addClass("mini-navbar");
                base.minimizaMenu();
            } else {
                base.checkWindowSize(base);
            }
            if (opts.wrapped) {
                base.$el.addClass("wrapped"); 
            }
        },
        crearOpciones: function () {
            var opts = this.options;
            var secciones = opts.secciones;
            var base = this;
            var menu = base.$el.find("#side-menu");
 
            for (var key in secciones) {
                if (secciones[key].activa == true) {
                    var activo = key == opts.opcion ? "active" : "";
                    var opthtml = '<li id="Opcion_' + key + '" class="navMenu-opt ' + activo + '">' +
                                       '<a href="#">' +
                                            '<i class="' + secciones[key].icono + '">  ' +
                                             (secciones[key].innerHtml ? secciones[key].innerHtml:"") +
                                            '  </i>' +
                                            '<span class="navMenu-label">' + secciones[key].nombre + '</span>' +
                                       '</a>' +
                                   '</li>';
                    menu.append(opthtml);
                }
            }
        },
        //onResize: function (recBody) {
        //    var ctrl = this.$el;//$("#navMenuModulo");
        //    var mini = "mini";
        //    try {
        //        if (recBody.width < 850) {
        //            ctrl.addClass('body-small');
        //            if (!ctrl.hasClass('mini-navbar')) {
        //                ctrl.addClass('mini-navbar');
        //                ctrl.addClass('nomini');
        //                //if (typeof opcionesGlobales != "undefined")
        //                //    if (typeof opcionesGlobales.onMinimizar == "function")
        //                //        opcionesGlobales.onMinimizar();
        //            }
        //        } else {
        //            ctrl.removeClass('body-small');
        //            if (ctrl.hasClass('nomini')) {
        //                ctrl.removeClass('mini-navbar');
        //                ctrl.removeClass('nomini');
        //                //if (typeof opcionesGlobales != "undefined")
        //                //    if (typeof opcionesGlobales.onMaximizar == "function")
        //                //        opcionesGlobales.onMaximizar();
        //            }
        //            if (!ctrl.hasClass('mini-navbar'))
        //                mini = "";
        //        }
        //        this._animacionBurguer(ctrl, mini);
        //    } catch (e) {
        //        console.log('Redimensionar Menu Modulo: ' + e);
        //    }
        //},
        minimizaMenu: function (force) {
            var base = this;
            var opts = base.options;
            var ctrl = base.$el;//this.$el.find("#navMenuModulo");;
            try {
                var mini = "";
                //var menuMin = ctrl.find('#side-menu');

                if (!ctrl.hasClass('mini-navbar') ) {
                    //menuMin.hide();
                    //menuMin.fadeIn(500);
                    //opts.onMaximizar();
                    // } else if ($('body').hasClass('fixed-sidebar')) {
                    //menuMin.hide();
                    //menuMin.fadeIn(500);
                    //    opts.onMaximizar();
                } else {
                    mini = "mini";
                    //menuMin.removeAttr('style');
                    //opts.onMinimizar();
                }
                base._cambiarEstadoMenu(ctrl, opts.id, mini);
            } catch (e) {
                console.log('Minimiza menu Menu Modulo: ' + e);
            }
        },
        confirmaCambioSeccion: function (seccion) {
            var base = this;
            switch (base.options.multiSelection) {
                case false:
                    base.$el.find('.navMenu-opt').removeClass("active");
                    base.$el.find('#Opcion_' + seccion).addClass("active");
                    break;
                case "auto":
                    base.$el.find('#Opcion_' + seccion).toggleClass("active");
                    break;
            }
            
        },
        _setSelectMenu: function (seccion, active) {
            if (!active) {
                this.$el.find('#Opcion_' + seccion).removeClass("active");
            } else {
                this.$el.find('#Opcion_' + seccion).addClass("active");
            }
        },
        _cambiarEstadoMenu: function (ctrl, id, mini) {
            try {
                this._animacionBurguer(ctrl, mini);
                this._cargarTitles();
                localStorage.setItem("navMenuModulos_" + id, mini);
                console.log("cambio de estado " + mini);
            } catch (e) {
                console.log(e);
            }
        },

        _cargarTitles: function () {
            try {
                var ctrl = this.$el;//$('#navMenuModulo');
                if ( !ctrl.hasClass('mini-navbar')) {
                    ctrl.find('#side-menu>li>a').removeAttr("title");
                } else {
                    ctrl.find('#side-menu>li>a').each(function (index) {
                        var text = $(this).find(".navMenu-label").text();
                        $(this).attr("title", text);
                    });
                }
            } catch (e) {
                console.log('Carga tiles Menu Modulo: ' + e);
            }
        },

        _animacionBurguer: function (ctrl, mini) {
            try {
                var logo = ctrl.find(".navMenu-logo");
                var boton = ctrl.find("#botonHamburguesa");
                logo.find("i").addClass("c-hamburger");
                setTimeout(function () {
                    if (mini != "") {
                        logo.find("i").addClass("c-hamburger--htra").html("<span></span>").css("margin-top", "-15px");
                        setTimeout(function () {
                            logo.find("i").addClass("is-active");
                            boton.addClass("is-active");
                        }, 150);
                    }
                    else {
                        setTimeout(function () {
                            boton.removeClass("is-active");
                            logo.find("i").removeClass("is-active");
                        }, 150);
                    }
                }, 300);
            } catch (e) {
                console.log('Animacion Burguer Menu Modulo: ' + e);
            }
        },

        //_bodyMenuModulos: function (id, ctrl, options) {
        //    try {
        //        var estadomenu = localStorage.getItem("navMenuModulos_" + id);
        //        if (estadomenu == "mini") {
        //            if (!ctrl.hasClass('body-small')) {
        //                ctrl.addClass('mini-navbar');
        //                options.onMinimizar();
        //                //TODO: algo :P

        //            }
        //        }
        //        this._animacionBurguer(ctrl, estadomenu);
        //    } catch (e) {
        //        console.log('body Menu Modulo: ' + e);
        //    }
        //}


    }
})(jQuery);
 