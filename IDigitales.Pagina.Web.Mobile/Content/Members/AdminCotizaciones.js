﻿var PaginaWeb = PaginaWeb || {}; 
PaginaWeb.Members = PaginaWeb.Members || {}; 

PaginaWeb.Members.Cotizaciones = function (opt) {

    //#region "Variables "
    var ctx = null;
    var util = null;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    var operacion = null;
    var params = {

        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Cotizacion guardada",
            ErrorGuardar: "Ocurrio un problema al guardar el producto",
            Borrado: "Se borro el producto",
            ErrorBorrar: "No se pudo borrar el producto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el producto?",
            ConfirmarTitulo: "Borrar el producto",
            Seleccionar: "Seleccione un producto",
            SinProductos: "No hay productos en la cotización",
            ErrorAbrir: "No se pudo abrir la cotización",
        },
        url: {
            //abrir: urlBase + "/Members/_EdicionProductos",
            cargar: urlBase + "/Members/CargarCotizacion",
            //eliminarArchivo: urlBase + "/Members/RemoverHojaProducto",
            Guardar: urlBase + "/Members/AgregarActualizarCotizacion",
            //Borrar: urlBase + "/Members/_BorraTransaccion",
            abrirCotizacion: urlBase + "/Members/AbrirCotizacion",
        },
        modal: {
            idModal: "contCotizaciones",
            idDatos: "carritoCotizaciones",
            selContPopUp: "#ContenedorPopUp1"
        }

    }

  
    //#endregion "Variables "

    //#region Mapeos
    var objetoCot = {
        IdCotizacion: "idCotizacion",
        Nombre: "nombre",
        IdLista: "idLista",
        PorcentajeDescuento: "cotDescuentoPor",
        idUsuario: "idUsuario",
    }
    //#endregion Mapeos
    //#region "Metodos "

    //#region "Metodos Comunes "
    function mostrarAyuda(mostrar) {
        if(mostrar)
            ctx.find("#contAyuda").show();
        else
            ctx.find("#contAyuda").hide();
    }
    function error(err) {
        console.log(err);
    }
    function moverModalTop() {
        try {
            var ajsDia = ctx.parents(".ajs-dialog");
            var t = ($(".ajs-modal").height() - ctx.parents(".ajs-dialog").height()) / 2;
            ajsDia.animate({ top: t }, 800);
        } catch (err) {
            error(err);
        }
    }

    function prepararDatos(cot) {
        var prods = [];
        try {
            var filas = ctx.find("#carritoCotizaciones .cart-fila");
            if (filas.length > 0) {
                filas.each(function () {
                    var fila = $(this);
                    var producto = fila.data("producto");
                    var p = {};
                    p.IdCotizacion = cot.IdCotizacion;
                    p.IdProducto = producto.IdProducto;
                    p.Cantidad =   parseFloat(fila.find(".cart-cantidad input").val());;
                    p.PorcentajeDescuento = parseFloat(fila.find(".cart-descuento input").val());
                    prods.push(p);
                });
            } else {
                mostrarAyuda(true);
            }

        } catch (err) {
            error(err);
        }
        return prods;
    }
    //#endregion

    //#region Inicializar
    function inicializar() {
        ctx = $("#" + params.modal.idModal);
        util = PaginaWeb.Utils(ctx, _Members.urlBase());
        cargaCotizacion(0);
    }
    function cargaCotizacion(idCotizacion) {
        ctx.find(".inner-tablaCotizaciones").html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
        util.Accion(params.url.cargar, { idCotizacion: idCotizacion }, inicializaControles, "get", "html");
    }
    function inicializaControles(html) {
        ctx.find(".inner-tablaCotizaciones").html(html);
        agregarEventos();
    }
    function agregarEventos() {

        $("#btnCotNueva").off("click", onClickCotNueva).on("click", onClickCotNueva);
        $("#btnCotAbrir").off("click", onClickCotAbrir).on("click", onClickCotAbrir);
        $("#btnCotGuardar").off("click", onClickCotGuardar).on("click", onClickCotGuardar);
        $("#btnCotAgregar").off("click", onClickCotAgregar).on("click", onClickCotAgregar);
        $("#btnImprimirCot").off("click", onClickCotImprimir).on("click", onClickCotImprimir);
        $("#btnImprimirListasPrecioCot").off("click", imprimirListasPrecioCot).on("click", imprimirListasPrecioCot);
        $("#btnImprimirInventarioCot").off("click", imprimirInventarioCot).on("click", imprimirInventarioCot);
        
        $("#cotDescuento").find(".botonInputProducto input").bind("keyup", function () {
            cambiarDescuentoCotizacion(this, '*');
        });
        $("#cotDescuento").find(".botonInputProducto input").bind("focusout", function () {
            cambiarDescuentoCotizacion(this, '?');
        });
        $("#cotDescuento").find("#masDesCot").off("click").on("click", function () {
            cambiarDescuentoCotizacion(this, '+');
        });
        $("#cotDescuento").find("#menosDesCot").off("click").on("click", function () {
            cambiarDescuentoCotizacion(this, '-');
        });
        $("#cotDescuento").find("#ceroDescCot").off("click").on("click", function () {
            var row = $(this).parents("#cotDescuento");
            var input = row.find("#cotDescuentoPor");
            input.val(0); 
            sumarProductos();
        });

        //$("#cotDescuentoPor").off("change").on("change", function () {
        //    var val = $(this).val();
        //    sumarProductos();
        //});
    }
    //#endregion
   

    //#region eventos botones

    function onClickCotNueva() {
        var cot = util.getDatos(objetoCot);
        cot.IdCotizacion = 0;
        cot.Nombre = "";
        cot.IdLista = 0;
        cot.PorcentajeDescuento = 0;
        util.setDatos(objetoCot, cot);
        limpiarTabla();
    }

    function onClickCotAbrir() {
        PaginaWeb.Members.Cotizaciones.Operacion({ callbackCargar: obtenerCotizacion }).cargarCotizacion();
    }

    function onClickCotGuardar() {
        var cot = util.getDatos(objetoCot);
        var productos = prepararDatos(cot);
        if (cot && productos.length > 0) {
            PaginaWeb.Members.Cotizaciones.Operacion({ callbackGuardar: realizarGuardado }).guardar(cot);
        } else {
            ctx.idigitalesNotificaciones('', params.msgs.SinProductos, 'alert');
            mostrarAyuda(true);
        }
    }

    function onClickCotAgregar() {
        var idLista = parseInt(ctx.find("#idLista").val());
        var iva = parseInt(ctx.find("#iva").val());
        if (idLista) {
            listaSeleccionada({ IdLista: idLista, Iva: iva});
        } else {
            PaginaWeb.Members.Cotizaciones.Operacion({ callbackSeleccionarLista: listaSeleccionada }).seleccionarLista();
        }


    }

    function onClickCotImprimir() {
        var cot = util.getDatos(objetoCot);
        //Obtener productos      
        var productos = [];
        var filas = ctx.find("#carritoCotizaciones .cart-fila");
        if (filas.length > 0) {
            filas.each(function () {
                var fila = $(this);
                var producto = fila.data("producto");
                var p = {};
                p.IdCotizacion = cot.IdCotizacion;
                p.IdProducto = producto.IdProducto;
                p.Cantidad = parseFloat(fila.find(".cart-cantidad input").val());;
                p.PorcentajeDescuento = parseFloat(fila.find(".cart-descuento input").val());
                productos.push(p);
            });
        }        
        if (cot && productos.length > 0 && cot.IdLista>0) {            
            cot.CotizacionesProductos = productos;
            PaginaWeb.Members.Cotizaciones.ImpresionCotizacion().abrir(cot);           
        } else {
            ctx.idigitalesNotificaciones('', params.msgs.OperacionError, 'alert');
        }
    }

    function imprimirListasPrecioCot() {       
        PaginaWeb.Members.Impresion.ListasPrecio().abrir();
    }
    function imprimirInventarioCot() {
        try { 
            window.open(urlBase + "Members/ReporteInventario");
        } catch (e) {
            error(e);
        }
    }
    //#endregion


    //#region  Manejo Cotizacion
    function cargarCotizacion(cot) {
        onClickCotNueva();
        setTimeout(function() {
            util.setDatos(objetoCot, cot.Cotizacion);
            util.setVal("iva",cot.Iva);
            //ctx.find("#lblcotDescuentoPor").text(cot.Cotizacion.PorcentajeDescuento + "%");
            cot.CotizacionesProductos.forEach(function (cp) {
                agregarProducto(cp);               
            });           
        }, 500);

        $("#btnImprimirCot").show();
    }

    function limpiarTabla() {
        var filas = $("#carritoCotizaciones .cart-fila");
        var tiempo = 10;
        if (filas.length > 0) {
            filas.each(function () {
                eliminarProducto($(this), tiempo);
                tiempo = tiempo + 10;
            });

        }
        
        setTimeout(sumarProductos, 1000);
    }

    function eliminarProducto(fila, tiempo) {
        
        fila.find(" input").off("keyup");
        fila.find(" input").off("focusout");
        fila.find(" input").off("input");
        
        fila.animate({ padding: 0 })
            .slideUp(tiempo, function () {
                fila.remove();
            });
    }
    
    function realizarGuardado(cotizacion) {
        util.setDatos(objetoCot, cotizacion);

        cotizacion.CotizacionesProductos = prepararDatos(cotizacion);
        if (cotizacion && cotizacion.CotizacionesProductos.length > 0) {
            guardar(cotizacion);
        } else {
            ctx.idigitalesNotificaciones('', params.msgs.SinProductos, 'alert');
            mostrarAyuda(true);
        }
    }

    function listaSeleccionada(lista) {
        try {
            ctx.find("#idLista").val(lista.IdLista);
            ctx.find("#iva").val(lista.Iva);
            PaginaWeb.Members.Cotizaciones.Seleccion({ callback: agregarProducto }).abrir(lista.IdLista); 
        } catch (err) {
            error(err);
        } 
       

    }
    
    function agregarProducto(producto) { 
        ctx.find("#contAyuda").hide();

        var rowTemp = $("#cart-fila_" + producto.IdProducto);

        if (rowTemp.length === 0) {
            var row = ctx.find("#modelRowCotizacion .cart-fila").clone();
            row.attr("id", "cart-fila_" + producto.IdProducto);
            row.data("producto", producto);
            
           
            var porcentaje = producto.Porcentaje || 0;
            var subtProducto = parseFloat(producto.Precio * producto.Cantidad);
            var descuento = subtProducto * (porcentaje / 100);
            var precio = "$ " + parseFloat(producto.Precio).toFixed(2);
            var total = "$ " + parseFloat(subtProducto - descuento).toFixed(2);

            row.find(".cart-name").html(producto.Nombre);
            row.find(".cart-model").html(producto.Modelo);
            row.find(".cart-cantidad .botonInputProducto input").val(producto.Cantidad);
            row.find(".cart-precio").html(precio);
            row.find(".cart-descuento .botonInputProducto input").val(porcentaje);
            row.find(".cart-total").html(total);
            //row.find(".cart-descuento #lblPorcentaje").text(porcentaje + "%");

            $("#carritoCotizaciones").append(row);

            //row.find(".cart-descuento input").on("input", function () {
            //    var r = $(this).parents(".cart-fila");
            //    var val = $(this).val();
            //    r.find(".cart-descuento #lblPorcentaje").text(val + "%");
            //    calcularTotalConDescuento(r);
            //});
            
        
            row.find(".cart-cantidad .botonInputProducto input").bind("keyup", function () {
                cambiarCantidad(this, '*');
            });
            row.find(".cart-cantidad .botonInputProducto input").bind("focusout", function () {
                cambiarCantidad(this, '?');
            });

            row.find(".cart-descuento .botonInputProducto input").bind("keyup", function () {
                cambiarDescuento(this, '*');
            });
            row.find(".cart-descuento .botonInputProducto input").bind("focusout", function () {
                cambiarDescuento(this, '?');
            });

            sumarProductos();
        } else {
            cambiarCantidad(rowTemp, '+', true);
        }
    }
    //#endregion

    //#region Ajax Agregar-Editar-Borrar

    function guardar(datos) {
        try {
            ajax.AccionModal(params.url.Guardar, { cot: datos }, function (data) {
                    if (data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("", params.msgs.Guardado, 'success');
                        ctx.find("#idCotizacion").val(data.Id);
                        $("#btnImprimirCot").show();
                    } else {
                        console.log(data);
                        ctx.idigitalesNotificaciones('', params.msgs.ErrorGuardar, 'error');
                    }
                },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }
    
    function obtenerCotizacion(idCotizacion) {
        try {
            ajax.AccionModal(params.url.abrirCotizacion, { idCotizacion: idCotizacion }, function (data) {
                    console.log(data);
                    if (data) {
                        cargarCotizacion(data);
                    } else {
                        console.log(data);
                        ctx.idigitalesNotificaciones('', params.msgs.ErrorAbrir, 'error');
                    }
                },
                "post", "json");

        } catch (err) {
            error(err);
        }

    }

    //#endregion

    function cambiarDescuentoCotizacion(element, tipo, row) {

        if (row || !$(element).hasClass("desabilitarBoton")) {
           
            var fila = $(element).parents("#cotDescuento");
            var input = fila.find("#cotDescuentoPor");
            var porcentaje = 0;

            var valActual = input.val();
            var value = valActual;

            switch (tipo) {
            case '+':
            case '-':
                value = tipo === '+' ? 1 : -1;
                porcentaje = parseInt(valActual) + value;
                break;
            case '*':
                if (!valActual) {
                    return;
                }
            case '?':
                porcentaje = parseInt(value);
                if (porcentaje < 1 || isNaN(porcentaje)) {
                    porcentaje = 1;
                }
                break;
            }

            if (porcentaje > 0) {
                fila.find("#menosDesc").removeClass("desabilitarBoton");
            } else {
                fila.find("#menosDesc").addClass("desabilitarBoton");
            }
            input.val(porcentaje);
            sumarProductos(); 
        }
    }


    function cambiarDescuento(element, tipo, row) {

        if (row || !$(element).hasClass("desabilitarBoton")) {
            var fila;
            if (row) {
                tipo = "+";
                fila = element;
            } else {
                fila = $(element).parents(".cart-fila");
            }

            var input = fila.find(".cart-descuento input");
            var porcentaje = 0;

            var valActual = input.val();
            var value = valActual;

            switch (tipo) {
            case '+':
            case '-':
                value = tipo === '+' ? 1 : -1;
                porcentaje = parseInt(valActual) + value;
                break;
            case '*':
                if (!valActual) {
                    return;
                }
            case '?':
                porcentaje = parseInt(value);
                    if (porcentaje < 1 || isNaN(porcentaje)) {
                        porcentaje = 1;
                }
                break;
            }

            if (porcentaje > 0) {
                fila.find("#menosDesc").removeClass("desabilitarBoton");
            } else {
                fila.find("#menosDesc").addClass("desabilitarBoton");
            }
            input.val(porcentaje);
            console.log("cambiar cantidad a: " + porcentaje);

            var cantidad = fila.find(".cart-cantidad input").val();
            var precio = parseFloat(fila.data("producto").Precio);

            var descuento = precio * (porcentaje / 100);
            var precioConDescuento = precio - descuento;
            var total = precioConDescuento * cantidad;
            var textPrecio = "$" + total.toFixed(2);
            fila.find(".cart-total").html(textPrecio);
            sumarProductos(); 
        }
    }
    function ceroDescuento(element) {
        var row = $(element).parents(".cart-fila");
        var input = row.find(".cart-descuento input");
        input.val(0);
        var producto = row.data("producto");
        var precio = parseFloat(producto.Precio);
        var cantidad = row.find(".cart-cantidad .botonInputProducto input").val();
        var total = "$ " + parseFloat(precio * cantidad).toFixed(2);

        row.find(".cart-total").html(total);
        sumarProductos(); 
    }

    function cambiarCantidad(element, tipo, row) {
         
        if (row || !$(element).hasClass("desabilitarBoton")) {
            var fila;
            if (row) {
                tipo = "+";
                fila = element;
            } else {
                fila = $(element).parents(".cart-fila");
            } 
           
            var input = fila.find(".cart-cantidad input");
            var cantidad = 1;
            
            var valActual = input.val();
            var value = valActual;
           
            switch (tipo) {
                case '+':
                case '-':
                    value = tipo === '+' ? 1 : -1;
                    cantidad = parseInt(valActual) + value;
                    break; 
                case '*': 
                    if (!valActual) {
                        return;
                    }
                case '?':
                    cantidad = parseInt(value);
                    if (cantidad < 1 || isNaN(cantidad)) { 
                        cantidad = 1;
                    }
                    break;
            }

            if (cantidad > 1) {
                fila.find("#quitar1producto").removeClass("desabilitarBoton");
            } else {
                fila.find("#quitar1producto").addClass("desabilitarBoton");
            }
            input.val(cantidad);
            console.log("cambiar cantidad a: " + cantidad);

            var porcentaje = fila.find(".cart-descuento input").val();
            var precio = parseFloat(fila.data("producto").Precio);

            var descuento = precio * (porcentaje / 100);
            var precioConDescuento = precio - descuento;
            var total = precioConDescuento * cantidad;
            var textPrecio = "$" + total.toFixed(2);
            fila.find(".cart-total").html(textPrecio);
            sumarProductos(); 
        }
    }

    function calcularTotalConDescuento(fila) {
        var cantidad = parseFloat(fila.find(".cart-cantidad input").val());
        var porcentaje = parseFloat(fila.find(".cart-descuento input").val());
        var precio = parseFloat(fila.data("producto").Precio);

        var descuento = precio * (porcentaje / 100);
        var precioConDescuento = precio - descuento;
        var total = precioConDescuento * cantidad;
        var textPrecio = "$" + total.toFixed(2);
        fila.find(".cart-total").html(textPrecio);
        sumarProductos(); 
    }
    
    function sumarProductos() {
        var suma = 0;
        var producto;
        var filas = $("#carritoCotizaciones .cart-fila");
        if (filas.length > 0) {
            filas.each(function () {
                var fila = $(this);
                producto = fila.data("producto");
                var precio = producto.Precio;

                var porcentaje = parseFloat(fila.find(".cart-descuento input").val());
                var descuento = precio * (porcentaje / 100);
                var precioConDescuento = precio - descuento;

                suma += (precioConDescuento * fila.find(".cart-cantidad  input").val());
                //console.log(" + Producto: " + producto.Nombre + " " + suma);
            });

             
        } else {
            mostrarAyuda(true);
        }
        
        var porDes = parseFloat($("#cotDescuentoPor").val());
        var desCot = suma * (porDes / 100);
        var iva = 0.0;
       
        if (ctx.find("#iva").val() === "true") {
            iva = suma * 0.16;
        }
        var totalCot = (suma - desCot)+iva;       
        $("#cotizacionSubtotal >div").html("$ " + suma.toFixed(2));
        $("#cotizacionIVA >div").html("$ " + iva.toFixed(2));
        //$("#cotDescuento >div").html("$ " + desCot.toFixed(2));
        $("#cotizacionTotal >div").html("$ " + totalCot.toFixed(2));        
    }

    function eliminarProductoCarrito(element) {
        var fila = $(element).parents(".cart-fila").children();
        

        fila.find(".botonInputProducto input").unbind();
        fila.find(".botonInputProducto input").unbind();

        fila.animate({ padding: 0 })
        .wrapInner('<div />')
        .children()
        .slideUp(300, function() {
            $(this).parents(".cart-fila").remove();
            sumarProductos(); 
        });

        return false;
    }

    function seleccionarProducto() {
        alertify.modalSeleccionarProducto || alertify.dialog('modalSeleccionarProducto', function () {
            return {
                main: function (content) { this.setContent(content); },
                setup: function () {
                    var options = LayoutManager.optionsModals;
                    options.overflow = true;
                    return {
                        options: options
                    };
                },
                prepare: function () { /* this.elements.footer.style.visibility = "collapse";*/
                    $('#panelAddModalidad > input:text').val("");
                },
                build: function () {
                    this.elements.footer.style.minHeight = "0";
                    this.elements.content.style.bottom = "0";
                    this.elements.dialog.style.minWidth = "650rem";
                    this.elements.dialog.style.maxHeight = "650rem";
                }
            };
        });


        alertify.modalSeleccionarProducto($('#panelSelectPanelProducto').removeClass('hide')[0]).set('title', "Seleccionar Producto");

    }

    function seleccionProductoCarrito(element) {
        var $element = $(element).clone();
        alertify.modalSeleccionarProducto().close();
        var row = $("#modelRowCotizacion .cart-fila").clone();
        var producto = $element.data("producto");

        $(".ayudaCotizaciones").hide();

        var rowTemp = $("#cart-fila_" + producto.IdProducto);
        if (rowTemp.length === 0) {
            row.attr("id", "cart-fila_" + producto.IdProducto);
            row.data("producto", producto);

            row.find(".cart-name").html(producto.Nombre);
            row.find(".cart-model").html(producto.Modelo);

            var precio = "$ " + parseFloat(producto.Precio).toFixed(2);

            row.find(".cart-precio").html(precio);
            //row.find(".cart-descuento").html("30%");
            row.find(".cart-total").html(precio);

            $("#carritoCotizaciones").append(row);

            row.find(".cart-descuento input").on("input", function () {
                var r = $(this).parents(".cart-fila");
                var val = $(this).val();
                r.find(".cart-descuento label").text(val + "%");
                calcularTotalConDescuento(r);
            });


            row.find(".botonInputProducto input").bind("keyup", function () {
                adminCotizaciones.cambiarCantidad(this, '*');
            });
            row.find(".botonInputProducto input").bind("focusout", function () {
                adminCotizaciones.cambiarCantidad(this, '?');
            });

            sumarProductos();           
        } else {
            cambiarCantidad(rowTemp, '+', true);
        } 
    }

    //#endregion "Metodos "

    //#region "Constructores "
    var adminCotizaciones = {
        cambiarCantidad: cambiarCantidad,
        eliminarProductoCarrito: eliminarProductoCarrito,
        seleccionarProducto: seleccionarProducto,
        seleccionProductoCarrito: seleccionProductoCarrito,

        cambiarDescuento: cambiarDescuento,
        ceroDescuento: ceroDescuento,
      
        iniciar: function (onIniciar) {
        try {
                $(".fullWapper").addClass("height100p");
                //adminCotizaciones._agregarEventos();
                inicializar();

            if (onIniciar && typeof onIniciar === "function") {
                onIniciar(undefined);
                }
            } catch (e) {
                adminCotizaciones._error(e);
            }
        },
        finalizar: function (onFinalizar) {
            try {
                $(".fullWapper").removeClass("height100p");
                adminCotizaciones._quitarEventos();
                if (onFinalizar && typeof onFinalizar === "function") {
                    onFinalizar();
                }
            } catch (e) {
                adminCotizaciones._error(e);
            }
        },
         
        onCambioLayout: function (contenedorSize) {

        },
         
        _agregarEventos:function() { 
            $(".botonInputProducto input").bind("keyup", function() {
                adminCotizaciones.cambiarCantidad(this, '*');
            });
            $(".botonInputProducto input").bind("focusout", function () {
                adminCotizaciones.cambiarCantidad(this, '?');
            });
        },

        _quitarEventos: function() { 
            $(".botonInputProducto input").unbind();
        },

        _error: function (e) {
            console.log(e);
        }
    };
    //#endregion "Constructores "


    return adminCotizaciones;

};


PaginaWeb.Members.Cotizaciones.Seleccion = function(pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {};

    var cfg = {
        callback: pars.callback,

        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Producto guardada",
            ErrorGuardar: "Ocurrio un problema al guardar el producto",
            Borrado: "Se borro el producto",
            ErrorBorrar: "No se pudo borrar el producto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el producto?",
            ConfirmarTitulo: "Borrar el producto",
            Seleccionar: "Seleccione un producto"
        },
        url: {
            abrir: urlBase + "/Members/SeleccionarProductos",
            //agregarArchivo: urlBase + "/Members/AgregarHojaProducto",
            //eliminarArchivo: urlBase + "/Members/RemoverHojaProducto",
            //Guardar: urlBase + "/Members/AgregarActualizarProductos",
            //Borrar: urlBase + "/Members/_BorraTransaccion",
            //Descargar: urlBase + "/Members/ObtenerPdf",
            lista: urlBase + "/Members/ObtenerProductosListas",

        },
        modal: {
            idModal: "modalProductos",
            idDatos: "contDatos",
            selContPopUp: "#ContenedorPopUp1"
        },
        dom: {
            cntTabla: "#contTablas",
            cntPlantilla: "#contPlantilla",
            tabla: "#tabla",
            idMenuContextual: "menu-contextual"
        },
        jq: {
            cntTabla: null,
            cntPlantilla: null,
            tabla: null
        },
        text: {
            html: {
                sinElementos: "<div class='centrado sin-elementos'>No se encontraron productos</div>"
            },
            sinElementos: "No existen productos"
        }
    }


    //#region Mapeos
   
    //#endregion

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }

    function moverModalTop() {
        try {
            var ajsDia = ctx.parents(".ajs-dialog");
            var t = ($(".ajs-modal").height() - ctx.parents(".ajs-dialog").height()) / 2;
            ajsDia.animate({ top: t }, 800);
        } catch (err) {
            error(err);
        }
    }
    //#endregion

    //#region Modal

    //#region Inicializar

    function abrirModal(idElemento) {
        cfg.datos.idLista = idElemento;
        LayoutManager.abrirPopUpAjax(cfg.modal.idModal,
            cfg.modal.selContPopUp,
            cfg.url.abrir,
            { idListaPrecios: idElemento },
            "",
            { minWidth: '650rem', minHeight: '480rem', Height: '600rem' },
            inicializarModal,
            "post",
            cerrar,
            {});
    }

    function inicializarModal() {
        try {            
            ctx = $("#cntSubSeccion");
            ctx.find("table tbody").html('<div class="centrado"><div style="font-size: 100rem; color: #446cb3;" class="fa fa-refresh animado lento idigitales-form-icon"></div></div>');
            //cfg.tools.util = PaginaWeb.Utils(ctx, cfg.baseUrl);
            cfg.jq.cntPlantilla = ctx.find(cfg.dom.cntPlantilla);
            cfg.jq.cntTabla = ctx.find(cfg.dom.cntTabla);
            crearTabla();
            inicializarModalEventos();


        } catch (err) {
            error(err);
        }
    }

    function inicializarModalEventos() {
        try {
            ctx.find("#btnCancelarMod").off("click").on("click", onClickCancelarMod);

            ctx.find("#tabla_filter input[type=search]").off().on('keyup change',
                function () {
                    var val = $(this).val();
                    if (val.length % 3 === 0) {
                        cfg.jq.tabla.search(val).draw();
                    }
                });
            ctx.find('#contTablas tbody').on('click','tr',
                function () {
                    var data = cfg.jq.tabla.row($(this)).data();
                    if (cfg.callback)
                        cfg.callback(data);

                });
        } catch (err) {
            error(err);
        }
    }

    function crearTabla() {
        var tblhtml = cfg.jq.cntPlantilla.html();
        var tbl = $(tblhtml);
        cfg.jq.cntTabla.append(tbl);
        cfg.jq.tabla = ctx.find(cfg.dom.tabla).DataTable({
            //data: data,
            //deferLoading: 100,
            //processing: false,
            //scrollY: '65vh',
            //scrollx: false,
            //scrollCollapse: true,
            dom:
                '<"filtros"<"idigitales-renglon"<"search"f>r>>t', // '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',//dom: 'Brtip',//Bfrtip <"toolbar">Brtip
            searching: true,
            //order: [[1, 'asc']],
            colReorder: true,
            serverSide: true,
            ordering: false,
            //paging: false,
            //pageLength: calcularNumeroRegistros(),
            scrollY: 300,
            scroller: {
                loadingIndicator: true
            },
            ajax: {
                url: cfg.url.lista,
                // contentType: "application/x-www-form-urlencoded",
                type: "POST",
                data: function (d) {
                    d.columns[0].data = cfg.datos.idLista;
                    return d;
                }

            },
            columns: [
                {
                    className: 'celda-icono',
                    orderable: false,
                    data: null,
                    defaultContent: '',
                    width: "10rem",
                    render: function (data) {
                        var html = "";
                        if (data)
                            html = '<i class="icon icon-producto"></i>';
                        return html;
                    }
                },
                {
                    "data": "Modelo",
                    width: "50rem",
                    render: function (data, type, row) {
                        var html = '<div><i class="fa fa-minus"></i></div>';
                        if (data) {
                            html = '<div class="tooltip"><span class="elipsis">' + data
                                + '</span><span class="tooltiptext">' + data + '</span>'
                                + '</div>';
                        }
                        return html;
                    }
                },
                {
                    "data": "Nombre",
                    render: function (data, type, row) {
                        var html = '<div><i class="fa fa-minus"></i></div>';
                        if (data) {
                            html = '<div class="tooltip"><span class="elipsis">' + data
                                + '</span><span class="tooltiptext">' + data + '</span>'
                                + '</div>';
                        }
                        return html;
                    }
                },
                {
                    "data": "Precio",
                    width: "50rem",
                    render: function (data, type, row) {
                        var html = '<div><i class="fa fa-minus"></i></div>';
                        if (data) {
                           html = '<div class=""> $ ' +  new Intl.NumberFormat(ObtenerLenguaje()).format(data)  + '</div>';
                        }
                        return html;
                    }
                }

              
            ],
            language: {
                "zeroRecords": cfg.text.sinElementos,
                "oPaginate": {
                    "sFirst": "",
                    "sPrevious": " < ",
                    "sNext": " > ",
                    "sLast": ""
                },
                loadingRecords: "Cargando...",
                "search": "_INPUT_",
                searchPlaceholder: 'Buscar...'
            },
           
            fnInfoCallback: function (oSettings, iStart, iEnd, iMax, iTotal, sPre) {
               
            },
        });
       
    }

   
    //#region Eventos controles
   

    function onClickCancelarMod() {
        try {
            alertify[cfg.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }
    //#endregion

    //#endregion

    //#region Ajax Agregar-Editar-Borrar

    
    //#endregion


   

 
    //#endregion


    //#region "Metodos Publicos"
    var forma = {
        abrir: function (idElemento) {
            abrirModal(idElemento);
        },
        borrar: function (idElemento) {
          
        }

    }
    return forma;
    //#endregion 

}



PaginaWeb.Members.Cotizaciones.Operacion = function (pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {};
    var idModal = null;

    var cfg = {
        callbackGuardar: pars.callbackGuardar,
        callbackSeleccionarLista: pars.callbackSeleccionarLista,
        callbackCargar: pars.callbackCargar,
        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Producto guardada",
            ErrorGuardar: "Ocurrio un problema al guardar el producto",
            Borrado: "Se borro el producto",
            ErrorBorrar: "No se pudo borrar el producto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el producto?",
            ConfirmarTitulo: "Borrar el producto",
            Seleccionar: "Seleccione un producto"
        },
        url: {
            guardarCot: urlBase + "/Members/ModalGuardarCotizacion",
            buscar: urlBase + "/Catalogos/BuscarEntidad",

        },
        modal: {
            idModal: "modalCotizacion",
            idModalGuardar: "modalGuardarCot",
            idModalSeleccion: "modalSeleccionarLista",
            idModalAbrir: "modalAbrirCotizacion",
            selContPopUp: "#ContenedorPopUp3"
        }
    }


    //#region Mapeos
    var objetoCot = {
        IdCotizacion: "idCotizacion",
        Nombre: "nombre",
        IdLista: "selLista",
        PorcentajeDescuento: "porcentajeDescuento",
        idUsuario: "idUsuario",
    }
    var catalogos = {
        ListasPrecios: ["selLista", "idLista"]
    }
   
    //#endregion Mapeos

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }

    function moverModalTop() {
        try {
            var ajsDia = ctx.parents(".ajs-dialog");
            var t = ($(".ajs-modal").height() - ctx.parents(".ajs-dialog").height()) / 2;
            ajsDia.animate({ top: t }, 800);
        } catch (err) {
            error(err);
        }
    }
    function onClickCancelarMod() {
        try {
            alertify[idModal]().close();
        } catch (err) {
            error(err);
        }
    }
    //#endregion
    
    //#region Modal Guardar

    function abrirModalGuardar(elemento) {
        cfg.datos = elemento;
        idModal = cfg.modal.idModalGuardar;
        LayoutManager.abrirPopUpAjax(idModal,
            cfg.modal.selContPopUp,
            cfg.url.guardarCot,
            {},
            "",
            { minWidth: '350rem', minHeight: '300rem'},
            inicializarModalGuardar,
            "post",
            cerrar,
            {});
    }

    function inicializarModalGuardar() {
        try {
            ctx = $("#" + idModal);
            ctx.show();
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            inicializarModalGuardarControles();
            util.getCatalogos(catalogos);
            util.setDatos(objetoCot, cfg.datos);

        } catch (err) {
            error(err);
        }
    }

    function inicializarModalGuardarControles() {
        try {
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });

            ctx.find("#btnCancelarMod").off("click").on("click", onClickCancelarMod);
            ctx.find("#btnGuardarMod").off("click").on("click", onClickGuardarModGuardar);
        } catch (err) {
            error(err);
        }
    }
    
    function onClickGuardarModGuardar() {
        try {
            var cont = ctx[0];
            if (validador.validar(cont)) {
                var datos = util.getDatos(objetoCot);
                if (datos && cfg.callbackGuardar) {
                    cfg.callbackGuardar(datos);
                    onClickCancelarMod();
                }
            }

        } catch (err) {
            error(err);
        }
    }

  
   
    //#endregion

    //#region Modal Seleccionar Lista

    function abrirModalSeleccionLista() {
        idModal = cfg.modal.idModalSeleccion;
        LayoutManager.abrirPopUpAjax(idModal,
            cfg.modal.selContPopUp,
            cfg.url.guardarCot,
            {},
            "",
            { minWidth: '350rem', minHeight: '200rem', maxHeight: '230rem' }, 
            inicializarModalSelListaGuardar,
            "post",
            cerrar,
            {});
    }

    function inicializarModalSelListaGuardar() {
        try {
            ctx = $("#" + idModal);
            ctx.show();
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            inicializarModalSelListaControles();
            util.getCatalogos(catalogos);
        } catch (err) {
            error(err);
        }
    }

    function inicializarModalSelListaControles() {
        try {
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });

            ctx.find("#btnGuardarSel").off("click").on("click", onClickGuardarSelMod);
            ctx.find("#btnCancelarSel").off("click").on("click", onClickCancelarMod);
        } catch (err) {
            error(err);
        }
    }

    function onClickGuardarSelMod() {
        try {
            var cont = ctx[0]; 
            if (validador.validar(cont)) {
                var select = ctx.find("#idLista");
                var valor=select.val();
                var opcion = select.find('option[value="' + valor + '"]');
                var datos = opcion.data("Elemento");
                if (datos && cfg.callbackSeleccionarLista) {
                    cfg.callbackSeleccionarLista(datos);
                    onClickCancelarMod();
                }
            }

        } catch (err) {
            error(err);
        }
    }

    //#endregion


    //#region Modal Cargar

    function abrirModalCargar() {
        idModal = cfg.modal.idModalAbrir;
        LayoutManager.abrirPopUpAjax(idModal,
            cfg.modal.selContPopUp,
            cfg.url.guardarCot,
            {},
            "",
            { minWidth: '350rem', minHeight: '200rem', maxHeight: '230rem' },
            inicializarModalCargar,
            "post",
            cerrar,
            {});
    }

    function inicializarModalCargar() {
        try {
            ctx = $("#" + idModal);
            ctx.show();
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            inicializarModalCargarControles();
        } catch (err) {
            error(err);
        }
    }

    function inicializarModalCargarControles() {
        try {
            var selProductos = ctx.find("#selectCotizacion");
            var placeholderProductos = selProductos.data("placeholder");
            selProductos.data("catalogo", "Cotizaciones"); //llenar descripcion
            selProductos.select2({
                ajax: {
                    url: cfg.url.buscar,
                    dataType: 'json',
                    delay: 200,
                    data: function (par) {
                        return {
                            catalogo: "Cotizaciones",
                            filtro: par.term, // search term
                            page: par.page
                        };
                    },
                    processResults: function (data, page) {
                        return { results: data.Catalogo };
                    },
                    cache: true
                },
                width: '100%',
                allowClear: true,
                placeholder: placeholderProductos,
                escapeMarkup: function (markup) { return markup; },
                minimumInputLength: 3,
                templateResult: util.templateResult,
                templateSelection: util.templateSelection
            });

            ctx.find("#btnAbrirCargar").off("click").on("click", onClickAbrirCargar);
            ctx.find("#btnCancelarCargar").off("click").on("click", onClickCancelarMod);
        } catch (err) {
            error(err);
        }
    }

    function onClickAbrirCargar() {
        try {
            var cont = ctx[0];
            if (validador.validar(cont)) {
                var idCotizacion = ctx.find("#selectCotizacion").val();
                if (idLista && cfg.callbackCargar) {
                    cfg.callbackCargar(idCotizacion);
                    onClickCancelarMod();
                }
            }

        } catch (err) {
            error(err);
        }
    }

    //#endregion
    
    //#region "Metodos Publicos"
    var forma = {
        guardar: function (elemento) {
            abrirModalGuardar(elemento);
        },
        seleccionarLista: function () {
            abrirModalSeleccionLista();
        },
        cargarCotizacion: function () {
            abrirModalCargar();
        }
    }
    return forma;
    //#endregion 

}

PaginaWeb.Members.Cotizaciones.ImpresionCotizacion = function (pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {}; 

    var params = {        
        datos: {},
        msgs: { 
            OperacionesExitosa: "Operación Exitosa",
            ErrorImpresion: "Ocurrió un problema al imprimir la cotización",
        },
        url: {
            abrir: urlBase + "/Members/ImpresionCotizacion",
            imprimir: urlBase + "/Members/GenerarReporteCotizacion",
            leerArchivo: urlBase + "/Members/ReporteCotizacion"
        },
        modal: {
            idModal: "modalImpresionCotizacion",
            idDatos: "contDatosImpresionCotizacion",
            selContPopUp: "#ContenedorPopUp5"
        }
    }

    var mapInformacionCotizacion = {
        NombreCliente: "nombreCliente",
        NombreEmpresa: "nombreEmpresa",          
        Contacto: "contacto",
        EmailContacto: "emailContacto",
        Compañia: "selCompany"
    };

    //#region Mapeos 
    var catalogos = { 
    }
    //#endregion

    //#region Metodos Comunes

    function error(err) {
        console.log(err);
    }

    //#endregion

    //#region Modal

    //#region Inicializar 
    function abrirModal(cot) {
        params.datos = cot;
        LayoutManager.abrirPopUpAjax(params.modal.idModal, params.modal.selContPopUp, params.url.abrir,
            {}, "", { minWidth: '450rem', minHeight: '200rem' }, inicializarModal, "post", cerrar, {});
    }

    function inicializarModal() {
        try {
            ctx = $("#" + params.modal.idModal);
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            //inicializar Controles
            inicializarControlesModal();
            util.getCatalogos(catalogos);
        } catch (err) {
            error(err);
        }
    }

    function inicializarControlesModal() {
        try {
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });
            ctx.find("#selCompany").val(1);
            ctx.find("#selCompany").change();
            //Eventos Botones 
            ctx.find("#btnImprimirCotizacion").off("click").on("click", onClickImprimirCot);
            ctx.find("#btnCancelarCotizacion").off("click").on("click", onClickCerrar);
        }
        catch (err) {
            error(err);
        }
    }

    //#endregion

    //#region Eventos controles   
    function onClickImprimirCot() {
        try {
            ctx.find("#spinCarga").show();
            var cont = ctx.find("#" + params.modal.idDatos)[0];
            if (validador.validar(cont)) {               
                var datos = util.getDatos(mapInformacionCotizacion);                            
                ajax.AccionModal(params.url.imprimir, {
                    cot: params.datos, nombreCliente: datos.NombreCliente,
                    nombreEmpresa: datos.NombreEmpresa, nombreContacto: datos.Contacto,
                    emailContacto: datos.EmailContacto, compañia: datos.Compañia
                }, function (data) {                   
                    if (data) { 
                        const encodedURL = encodeURI(data); 
                        window.open(params.url.leerArchivo + "?ruta=" + encodedURL);
                        ctx.find("#spinCarga").hide();
                    } else {
                        ctx.idigitalesNotificaciones('', params.msgs.ErrorImpresion, 'error');
                        ctx.find("#spinCarga").hide();
                    }
                },
               "post", "json"); 
            }
        } catch (err) {
            error(err);
        }
    }

    function onClickCerrar() {
        try {
            debugger;
            alertify[params.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }

    //#endregion

    //#endregion

    //#region Cerrar
    function cerrar() {
        try {
        } catch (e) {
            error(e);
        }
    }
    //#endregion 

    //#region Metodos Publicos
    var forma = {
        abrir: function (cot) {
            abrirModal(cot);
        }
    }
    return forma;
    //#endregion 

}