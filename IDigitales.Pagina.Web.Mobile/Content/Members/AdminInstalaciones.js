﻿var PaginaWeb = PaginaWeb || {};
PaginaWeb.Members = PaginaWeb.Members || {};
PaginaWeb.Members.Instalaciones = function (opt) {


    var ctx = null;
    var util = PaginaWeb.Utils();
    var rutaBase = _Members.urlBase();

    var params = {
        baseUrl: "",
        urls: {
            instalaciones: rutaBase + "/Members/Instalaciones",

        },
        modal: {
            idModal: "contInstalaciones",          
            idContenedor:"contInstalaciones"
        }
    };
  

    $.extend(true, params, opt);
    function error(err) {
        console.log(err);
    }

   
    //#region Inicializar
    function inicializar() {
        ctx = $("#" + params.modal.idModal);
        util = PaginaWeb.Utils(ctx, _Members.urlBase());
        agregarEventos();
    }

    function agregarEventos() {

        $('#error-container .close').click(function () {
            $('#error-container').removeClass('show');
            $('#error-container').addClass('collapse');
        });

        bindEventsInstalaciones();
    }

    function bindEventsInstalaciones() {
        $('.loading-container').hide();
        $("form.instalaciones-form").unbind();
        $("form.instalaciones-form").submit(function (event) {
            event.preventDefault();
            event.stopPropagation();
            var post_url = $(this).attr("action");
            var request_method = $(this).attr("method");
            var targetId = $(this).data("target");
            var form_data = $(this).serialize();
            var success = $(this).data("success");
            var append = $(this).data("append");
            var modal = $(this).data("modal");
            var file = $(this).data("file");
            var wait = $(this).data("wait");
            var content_type = "application/x-www-form-urlencoded";

            if (modal) {
                $('.modal').modal('hide');
            }

            if (file) {
                form_data = new FormData($(this)[0]);
                content_type = false;
            }

            if (wait !== true) {
                $('.loading-container').show();
            }

            $.ajax({
                processData: false,
                url: post_url,
                type: request_method,
                data: form_data,
                done: success,
                contentType: content_type,
                cache: false
            }).success(function (response) {
                if (append)
                    $(targetId).prepend(response);
                else $(targetId).html(response);
            }).error(function (response) {
                showError(response);
            }).complete(function () {
                bindEventsInstalaciones();
                return false;
            });
        });
    }

    //#endregion


    //#region Iframe

    function iniFrame() {
        $("#" + params.modal.idContenedor).append('<iframe id="instalacionesFrame" src="' + params.urls.instalaciones + '" class="iframeModulo"></iframe>');
       
        $('#instalacionesFrame').load(function () {
            $("#" + params.modal.idContenedor).find("#spin").remove();
        });
    }

    //#endregion

    //#region Public
    var subSeccion = {
        iniciar: function (onIniciar) {
            try {
                $(".fullWapper").addClass("height100p");
                //Inicializar si no es iframe
                //inicializar();
                //inicializar iframe
                iniFrame();
                
                if (onIniciar && typeof onIniciar === "function") {
                    onIniciar(undefined);
                }
            } catch (e) {
                error(e);
            }
        },

        finalizar: function (onFinalizar) {
            try {
                $(".fullWapper").removeClass("height100p");

                if (onFinalizar && typeof onFinalizar === "function") {
                    onFinalizar();
                }
            } catch (e) {
                error(e);
            }
        },

        onCambioLayout: function (contenedorSize) {
            try {

            } catch (e) {
                error(e);
            }
        },

    }
    //#endregion Public


    return subSeccion;




};


