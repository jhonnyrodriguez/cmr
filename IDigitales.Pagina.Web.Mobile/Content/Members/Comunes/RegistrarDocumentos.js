﻿var PaginaWeb = PaginaWeb || {};
PaginaWeb.Members = PaginaWeb.Members || {};
PaginaWeb.Members.Comunes = PaginaWeb.Members.Comunes || {};
PaginaWeb.Members.Comunes.RegistrarDocumentos = function (opt) {

    var listaGeneralArchivos = [];
    var ctx = null;
    var utilDoc = null;
    var optionsModals = { maximizable: false, resizable: false, padding: false, transition: 'fade', overflow: false, modal: true, closable: true, closableByDimmer: false };
    var myDropzoneArchivos;
    var archivos = [];
    var fileinfo = null;    
    var callBackGuardar = null;

    var _params = {
        baseUrl: ""
    }
    $.extend(_params, opt);    


    //#region Inicializacion
    function abrirPopUpAgregarDocumentos() {
        
        alertify.modalAgregarDocumentos || alertify.dialog('modalAgregarDocumentos', function () {
            return {
                main: function (content) {

                    this.setContent(content);

                },
                setup: function () { return { options: optionsModals }; },
                prepare: function () { this.elements.footer.style.visibility = "collapse"; },
                build: function () {
                    this.elements.content.style.overflow = "visible";
                    this.elements.dialog.style.minWidth = "700rem";
                    this.elements.footer.style.minHeight = "0";
                    this.elements.content.style.bottom = "0";
                    this.elements.dialog.style.minHeight = "535rem";
                    this.elements.dialog.style.maxHeight = "690rem";
                    
                },
                callback: function (closeEvent) {
                    //alert("antes de cerrar");
                },
                settings: {
                    guardar:guardarDocumentosBd,
                    cancelar: cancelarAlert,
                    cerrar: cerrarAlert,
                    archivoAgregado: archivoAgregadoAlert,
                    archivoRemovido: archivoRemovidoAlert
                },
                // AlertifyJS will invoke this each time a settings value gets updated.
                settingUpdated: function (key, oldValue, newValue) {
                    switch (key) {
                        case 'myProp':

                            break;
                    }
                },
                hooks: {
                    onshow: function () {

                    },

                    onclose: function () {                      

                        
                    },
                    // triggered when a dialog option gets updated.
                    // IMPORTANT: This will not be triggered for dialog custom settings updates ( use settingUpdated instead).
                    onupdate: function () {
                    }
                }
            };

        });

        $('#modalAgregarDocumentos').removeClass('hide');
        alertify.modalAgregarDocumentos($('#modalAgregarDocumentos')[0], function () { })
            .set('title', txtTitulo).set(
                {
                    closable: false,
                    guardar: guardarDocumentosBd,
                    cancelar: cancelarAlert,
                    cerrar: cerrarAlert,
                    archivoAgregado: archivoAgregadoAlert,
                    archivoRemovido: archivoRemovidoAlert
                });
    }
    function cancelarAlert() {
        if (typeof limpiarListaArchivos === "function") {
            limpiarListaArchivos();
        }
        listaGeneralArchivos = [];
        alertify.modalAgregarDocumentos().close();
    }
    function cerrarAlert () {
        // Si hay archivos pendientes notifica
        if (listaGeneralArchivos.length > 0) {
            alertify.confirm(txtConfirmarCerrar).set({
                'title': txtTitulo,
                'labels': { ok: txtAceptar, cancel: txtCancelar },
                'onok': function (closeEvent) {
                    cancelarAlert();
                }
            });
        }
        else {
            alertify.modalAgregarDocumentos().close();
        }
    }
    function archivoAgregadoAlert(archivoAgregado) {
        listaGeneralArchivos.push(archivoAgregado);
    }
    function archivoRemovidoAlert(archivoRemovido) {

        for (val in listaGeneralArchivos) {
            //fileinfo = archivos[val];
            fileinfo = listaGeneralArchivos[val];
            if (archivoRemovido === fileinfo.file) {
                listaGeneralArchivos.splice(val, 1);
                break;
            }
        }
    } 
    //#endregion 

    //#region catalogo
    function obtenerCatalogo(catalogo, idSelect) {
        utilDoc.Accion(_params.baseUrl + "/Members/ObtenerCatalogo",
            {
                catalogo: catalogo, idFiltro: ''
            },
            function (data) {
                var select = $("#" + idSelect);
                select.empty();
                if (data != null) {
                    $.each(data, function (index, value) {
                        select.append("<option value='" + value.Id + "'>" + value.Valor + "</option>");
                    });
                    select.val("").trigger("change");
                }
            });        
    }
    //#endregion
   
    //#region Documentos
    function inicializaEventos() {
        try { 
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });

            ctx.find(".fecha").datetimepicker({
                format: 'L',
                locale: ObtenerLenguaje(),
                //keepOpen: true,
                showClose: true,
                //maxDate: new Date(),
                defaultDate: new Date(),
                showTodayButton: true,
                //widgetPositioning: {
                //    horizontal: 'auto',
                //    vertical: 'auto'
                //}
            });

            $(document.body).bind("dragover", function (e) {
                e.preventDefault();
                return false;
            });

            $(document.body).bind("drop", function (e) {
                e.preventDefault();
                return false;
            });            

            $(":input").blur(function () {
                screen2Model();
            });

            $(":input").css("padding-left", "5px");

            ctx.find("#btnGuardar").off("click").on("click", guardarDocumentosBd);
            ctx.find('#btnCancelar').off("click").on("click", cerrarAlert);

            obtenerCatalogo("CatalogoCategorias", "cmbCategoria");
            obtenerCatalogo("CatalogoSubCategorias", "cmbSubCategoria");

           
        } catch (e) {
            console.log(e);
        } 
    }
    function incializaCarrucel() {
        try {
            ctx.find("#myCarouselArchivos").owlCarousel({
                singleItem: true,
                navigation: true,
                navigationText: [
                    "<",
                    ">"
                ],
                pagination: false,
                afterAction: afterAction
            });

            if (myDropzoneArchivos == null) {
                myDropzoneArchivos = new Dropzone("#SeccionArchivos #drop_zone",
                    {
                        url: _params.baseUrl + "/Members/SaveUploadedFile",
                        //previewsContainer: document.getElementById("fotosFrame"),
                        autoProcessQueue: true,
                        //previewTemplate: document.getElementById('preview-template').innerHTML,
                        init: function () {
                            this.on("maxfilesexceeded", function (data) {
                                var res = eval('(' + data.xhr.responseText + ')');

                            });
                            this.on("success", function (file, response) {
                                var val;
                                for (val in archivos) {
                                    fileinfo = archivos[val];
                                    if (file === fileinfo.file) {
                                        fileinfo.estado = "Uploaded";
                                        fileinfo.nombreArchivo = response.Nombre; //Se regresa el nombre por si se cambia
                                        ctx.find("#txtNombre").val(response.Nombre);
                                        fileinfo.descripcion = "";
                                        fileinfo.categoria = "";
                                        fileinfo.subCat = "";
                                        fileinfo.fechaCreacion = new Date();
                                        fileinfo.ruta = response.Ruta;                                        
                                        alertify.modalAgregarDocumentos().settings.archivoAgregado(archivos[val]);
                                        break;
                                    }
                                }
                            });
                            this.on("addedfile", function (file) {
                                // Create the remove button
                                var removeButton = Dropzone.createElement("<button>" + txtRemover +"</button>");
                                // Capture the Dropzone instance as closure.
                                var _this = this;
                                // Listen to the click event
                                removeButton.addEventListener("click", function (e) {
                                    // Make sure the button click doesn't submit the form:
                                    e.preventDefault();
                                    e.stopPropagation();


                                    clearScreen();
                                    var owl = ctx.find(".owl-carousel").data('owlCarousel');
                                    var rutaAarchivo = archivos[owl.currentItem].ruta;
                                    var nombreArchivo = archivos[owl.currentItem].nombreArchivo;
                                    alertify.modalAgregarDocumentos().settings.archivoRemovido(file);

                                    console.log('archivos: ' + owl.currentItem);
                                    archivos.splice(owl.currentItem, 1);
                                    owl.removeItem(owl.currentItem);

                                    removerArchivo(rutaAarchivo, nombreArchivo);
                                    if (archivos.length === 0) {
                                        clearScreen();
                                    }

                                });
                                file.previewElement.appendChild(removeButton);
                                switch (file.type) {
                                    case "application/pdf":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/pdf-icon-150x150.jpg"));
                                        break;
                                    case "application/msword":
                                    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/word-viewer.png"));
                                        break;
                                    case "application/excel":
                                    case "application/vnd.ms-excel":
                                    case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/xls-icon-150x150.jpg"));
                                        break;
                                    case "application/mspowerpoint":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/ppt-icon.jpg"));
                                        break;
                                    case "text/plain":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "~/Content/images/icons-app/txt-icon.jpg"));
                                        break;
                                    case "text/xml":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/xml-icon.jpg"));
                                        break;
                                    case "application/vnd.ms-xpsdocument":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/xps-icon.jpg"));
                                        break;
                                    case "audio/wav":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/wav-icon.jpg"));
                                        break;
                                    case "audio/mp3":
                                        this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/mp3-icon.png"));
                                        break;
                                    default:
                                        switch (file.name.split('.').pop()) {
                                            case "dcm":
                                                this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/dcm-icon.jpg"));
                                                break;
                                            default:
                                        }
                                }

                                var owl = ctx.find(".owl-carousel").data('owlCarousel');
                                owl.addItem(file.previewElement);
                                owl.goTo(owl.itemsAmount);

                                fileinfo = new fileInfoClass();
                                fileinfo.nombreArchivo = file.name;
                                fileinfo.type = file.type;
                                fileinfo.size = file.size;
                                fileinfo.indice = owl.itemsAmount;
                                fileinfo.extension = file.name.split('.').pop();
                                fileinfo.descripcion = "";
                                fileinfo.categoria = "";
                                fileinfo.fechaCreacion = new Date();
                                fileinfo.subCat = "";
                                fileinfo.file = file;

                                console.log('archivos: ' + fileinfo);
                                archivos.push(fileinfo);
                                model2Screen(archivos[owl.currentItem]);
                            });
                            this.on("thumbnail", function (file, thumbnail) {
                            });
                        }
                    });
            }
            $(document.body).bind("dragover", function (e) {
                e.preventDefault();
                return false;
            });
            $(document.body).bind("drop", function (e) {
                e.preventDefault();
                return false;
            });
            clearScreen();
        } catch (e) {
            console.log(e);
        } 
    }
    function fileInfoClass() {
        this.subCat;
        this.indice;
        this.nombreArchivo;
        this.tipoArchivo;
        this.descripcion;
        this.fechaCreacion;
        this.Extension;
        this.type;
        this.notas;
        this.ruta;
        this.size;
        this.agregaadoPor;
        this.origen;
        this.estado;
        this.file;
        this.categoria;        
    }
    function limpiarListaArchivos() {
        //var owl = ctx.find(".owl-carousel").data('owlCarousel');
        for (val in archivos) {
            var rutaAarchivo = archivos[val].ruta;
            var nombreArchivo = archivos[val].nombreArchivo;
            // owl.removeItem(owl.currentItem);
            removerArchivo(rutaAarchivo, nombreArchivo);
        }
        archivos = [];
        clearScreen();
        limpiarDropZone();
    }
    function limpiarDropZone() {
        if (myDropzoneArchivos !== null) {
            myDropzoneArchivos.destroy();
            //myDropzoneArchivos = null;
        }
    }
    function removerArchivo(ruta, archivo) {
        utilDoc.Accion(_params.baseUrl + "/Members/RemoveUploadedFile",
        {
            ruta: ruta, archivo: archivo
        },
        function (data) { });
        //ctx.idigitalesNotificaciones(txtTitulo, txtErrorEliminar, txtError);          
        
    }

    //#region Guardar Documentos a BD
    function guardarDocumentosBd() {
        screen2Model();
        if (listaGeneralArchivos.length > 0) {
            //Crear Objeto Paciente
            var archivos = [];
            listaGeneralArchivos.forEach(function (arch) { 
                if (arch.fechaCreacion != null && arch.fechaCreacion != "" &&
                    arch.categoria != null && arch.categoria != "" &&
                    arch.subCat != null && arch.subCat != "") {
                    var dia = 0;
                    var mes = 0;
                    var anio = 0;
                    var hrs = 0;
                    var min = 0;
                    if (arch.fechaCreacion != null) {
                        var fec = new Date(arch.fechaCreacion);
                        dia = fec.getDate();
                        mes = fec.getMonth();
                        anio = fec.getFullYear();
                        hrs = fec.getHours();
                        min = fec.getMinutes();
                    }
                    var a = {
                        Categoria: arch.categoria,
                        IdDocumento: 0,
                        SubCategoria: arch.subCat,
                        Nombre: arch.nombreArchivo,
                        Descripcion: arch.descripcion,
                        Ruta: arch.ruta,
                        RutaTemporal: arch.rutaTemp,
                        Dia: dia,
                        Mes: mes,
                        Anio: anio,
                        Hrs: hrs,
                        Min: min
                    };
                    archivos.push(a);
                }
            });
            if (archivos.length == listaGeneralArchivos.length) {
                guardaDocumentos(archivos);
            }                
            else {
                ctx.idigitalesNotificaciones(txtTitulo, txtValidacion, txtError);
            }
        } else {
            ctx.idigitalesNotificaciones(txtTitulo, txtNoDocumentos, txtError);
        }
    }
    
    function guardaDocumentos(archivosGuardar) {
        utilDoc.Accion(_params.baseUrl + "/Members/GuardarDocumentosBD",
        {
            archivos: archivosGuardar
        },
        function (data) {
            if (data !== null) {
                listaGeneralArchivos = [];
                archivos = [];
                clearScreen();
                limpiarDropZone();
                alertify.modalAgregarDocumentos().close();
                if (typeof callBackGuardar === "function") {
                    callBackGuardar(data);
                }
            } else {
                ctx.idigitalesNotificaciones(txtTitulo, txtErrorGuardar, txtError);
            }
        });
        //ctx.idigitalesNotificaciones(txtTitulo, txtOcurrioError, txtError);               
    }
    
    //#endregion

    //#endregion

    //#region Carousel
    function afterAction(el) {
        var owl = ctx.find(".owl-carousel").data('owlCarousel');
        if (owl != null) {
            if (owl.currentItem < archivos.length) {
                model2Screen(archivos[owl.currentItem]);
            }
        }
     }

    function model2Screen(fileInfo) {
        if (fileInfo) {
            if (ctx.find("#divCaptura").css("visibility") === "collapse") {
                ctx.find("#divCaptura").css("visibility", "visible");
                ctx.find("#divCarousel").css("display", "table");
                ctx.find("#panelCarousel .emptyDataCarusel").hide();
            }
            ctx.find("#txtNombre").val(fileInfo.nombreArchivo);
            ctx.find("#txtDescripcion").val(fileInfo.descripcion);
            ctx.find("#dtpFecha").data("DateTimePicker").date(fileInfo.fechaCreacion);
            ctx.find("#cmbCategoria").val(fileInfo.categoria).trigger("change");
            ctx.find("#cmbSubCategoria").val(fileInfo.subCat).trigger("change");
            
        }
    }

    function screen2Model() {
        var owl = ctx.find(".owl-carousel").data('owlCarousel');
        if (owl != null) {
            if (owl.currentItem < archivos.length) {
                fileInfo = archivos[owl.currentItem];
                fileInfo.nombreArchivo = ctx.find("#txtNombre").val();
                fileInfo.descripcion = ctx.find("#txtDescripcion").val();
                fileInfo.categoria = ctx.find("#cmbCategoria").val();
                fileInfo.subCat = ctx.find("#cmbSubCategoria").val();
                fileInfo.fechaCreacion = ctx.find("#dtpFecha").data("DateTimePicker").date();
            }
        }
    }

    function clearScreen() {
        ctx.find("#divCaptura").css("visibility", "collapse");
        if (typeof listaGeneralArchivos != "undefined")
            if (listaGeneralArchivos.length === 0) {
                ctx.find("#panelCarousel .emptyDataCarusel").show();
            }
        ctx.find("#txtNombre").val("");
        ctx.find("#txtDescripcion").val("");
        ctx.find("#cmbCategoria").val("").trigger("change");
        ctx.find("#cmbSubCategoria").val("").trigger("change");
        ctx.find("#dtpFecha").data("DateTimePicker").date(null);
     }
    //#endregion

     var doc = {
        abrir: function (parametros) {
            if (parametros && parametros.callBackGuardar) {
                callBackGuardar = parametros.callBackGuardar;
            }
            
            //var url = LayoutManager.obtenerRootApp(_params.baseUrl + "/Members/RegistrarDocumentos");
            $('#modalAgregarDocumentos').load(_params.baseUrl + "/Members/RegistrarDocumentos", {}, function () {
                ctx = $('#modalAgregarDocumentos');
                utilDoc = PaginaWeb.Utils();
                abrirPopUpAgregarDocumentos();                
                inicializaEventos();
                incializaCarrucel();
            });
        }

    }
    return doc;
}


