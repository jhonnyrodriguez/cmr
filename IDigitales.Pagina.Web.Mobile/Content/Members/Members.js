﻿/// <reference path="../comunes/dropzone/dropzone.min.js" />

 

var PaginaWeb = PaginaWeb || {}; 
PaginaWeb.Members = function (paramsConstructorSecciones, nombresSeccionesParam, seccionesParam, baseUrlParam) {
   
    //#region ". Variables "
    var _seccionSolicitada = null;
    var _secciones = [];
    var _seccionesManager = null;
    var _menuManager = null;
    var _manejadorSeccion = null;
    var _urlBase = baseUrlParam;
    //#endregion

    
  

//#region ". Constructores y métodos públicos"

   var  Members = {
        ajax:His.Manejadores.Ajax.AjaxManager(),
        manejadorSeccion: null,
        obtenerSecciones: function () {
            return _secciones;
       },
        urlBase: function () {
            return _urlBase;
        },

        obtenerManejadorSeccion: function()
        {
           Members.manejadorSeccion = _manejadorSeccion;
            //return _manejadorSeccion;
        },

        onInicioCambioSeccion: null,

        onFinalizoCambioSeccion: null,

        cambiarSeccion: function (seccion) {
            var self = this;
            _seccionSolicitada = seccion;
       
            _seccionesManager.cargaSeccion(seccion, function (seccionCargada, error) {

                if (error != null) {
                    alert(error)
                }
                else {
                    
                    Members.manejadorSeccion = _secciones[seccionCargada].manejador;

                    localStorage.setItem("Members_Menu_Opcion", seccionCargada);
                    if (_menuManager.confirmaCambioSeccion && typeof _menuManager.confirmaCambioSeccion === "function") {
                        _menuManager.confirmaCambioSeccion(seccionCargada);
                    }
                }

            });
        },
        _init: function (paramsConstructores, nombresSecciones, secciones, baseUrl) {

            secciones = secciones || {};
            
            var rutaModulo = baseUrl + "Content/Members/";

            var scriptsBase = [                                                  // Arreglo con los scripts comunes para el modulo
                baseUrl + "Content/comunes/alertifyjs/alertify.min.js",
                baseUrl + "Content/Members/Comunes/RegistrarDocumentos.js",
                baseUrl + "Content/Members/Impresion.js",
                baseUrl + "Content/comunes/owl-carousel_v_1_3_3/owl.carousel.js"

                        //{
                        //    src: ruta + "idigitales-uiradialprogress/idigitales-uiradialprogress.js",
                        //    key: "radial",
                        //    dependencias: ["dropzone", "carousel"]
                        //}, 
                        //{ src: "/content/desktop/plugins/idigitales-navegacion/idigitales-navegacion.js" }
            ];

            //scriptsBase = testScripts;

            _secciones["BaseModulo"] =                                      // En la seccion BaseModulo se pondrán scripts y css comunes en el modulo
                {
                    scripts: scriptsBase,       // Arreglo con los scripts comunes para el modulo                       
                    styles: [                                                   // Arreglo con los css comunes para el modulo
                        //"/Content/desktop/fonts/font-hisweb/fontHISWEB.css",
                        //"/Content/desktop/fonts/font-awesome/css/font-awesome.css",
                        baseUrl + "Content/comunes/alertifyjs/alertify.min.css",
                        baseUrl + "Content/comunes/alertifyjs/alertify-comun.css",
                        baseUrl + "Content/comunes/alertifyjs/alertify-themes-gray.css",
                        baseUrl + "Content/Members/Comunes/RegistrarDocumentos.css", 
                        baseUrl + "Content/comunes/owl-carousel_v_1_3_3/owl.carousel.css"
                        ],
                    activa: false
                };

             
            var seccionArchivos =
                {
                constructorParams: paramsConstructores['Archivos'] || {},
                    //url: "/Members/Archivos",                              // Es la url que inicia la seccion
                    codigo: "PaginaWeb.Members.Archivos",             // Js con la lógica de la secc
                    html: null,                                                     // Aqui se conserva el html de la seccion para evitar llamadas a servidor
                    scripts: [
                        rutaModulo +"AdminArchivos.js"
                    ],
                    styles: [                                                      // Arreglo con los css necesarios en la seccion
                        rutaModulo + "AdminArchivos.css"
                            ] ,
                    icono: " ",                               // Icono que se presenta en el menu
                nombre: nombresSecciones['Archivos'] || 'Archivos',                                           // Nombre que se presenta ewn el menú 
                activa: false                                               // Indica si la seccion esta activa para el usuario (permisos)
                };
            seccionArchivos.constructorParams.baseUrl = baseUrl;
            secciones.Archivos = secciones.Archivos || {};
            $.extend(seccionArchivos, secciones.Archivos);
            
            var seccionCotizaciones =
                {
                    constructorParams: paramsConstructores['Cotizaciones'] || {},
                    codigo: "PaginaWeb.Members.Cotizaciones",             // Js con la lógica de la secc
                    html: null,                                                     // Aqui se conserva el html de la seccion para evitar llamadas a servidor
                    scripts: [
                        rutaModulo + "AdminCotizaciones.js"
                    ],
                    styles: [
                        rutaModulo + "AdminCotizaciones.css"             
                    ],
                    icono: " ",                               // Icono que se presenta en el menu
                    nombre: nombresSecciones['Cotizaciones'] || 'Cotizaciones',                                           // Nombre que se presenta ewn el menú 
                activa: false                                                   // Indica si la seccion esta activa para el usuario (permisos)
                };
            seccionCotizaciones.constructorParams.baseUrl = baseUrl;
            secciones.Cotizaciones = secciones.Cotizaciones || {};
            $.extend(seccionCotizaciones, secciones.Cotizaciones);


            var seccionProductos =
            {
                constructorParams: paramsConstructores['Productos'] || {},
                codigo: "PaginaWeb.Members.Productos", // Js con la lógica de la secc
                html: null, // Aqui se conserva el html de la seccion para evitar llamadas a servidor
                scripts: [
                    rutaModulo + "AdminProductos.js?2.1.0.0"
                ],
                styles: [
                    rutaModulo + "AdminProductos.css"
                ],
                icono: " ", // Icono que se presenta en el menu
                nombre: nombresSecciones['Productos'] || 'Productos', // Nombre que se presenta ewn el menú 
                activa: false , // Indica si la seccion esta activa para el usuario (permisos)
            };
            seccionProductos.constructorParams.baseUrl = baseUrl;
            secciones.Productos = secciones.Productos || {};
            $.extend(seccionProductos, secciones.Productos);

            var seccionConfiguracion =
            {
                constructorParams: paramsConstructores['Configuracion'] || {},
                codigo: "PaginaWeb.Members.Configuracion",             // Js con la lógica de la secc
                html: null,                                                     // Aqui se conserva el html de la seccion para evitar llamadas a servidor
                scripts: [
                    rutaModulo + "AdminConfiguracion.js"
                ],
                styles: [
                    rutaModulo + "AdminConfiguracion.css",
                    rutaModulo + "AdminConfiguracion-theme-gray.css"
                ],
                icono: " ",                               // Icono que se presenta en el menu
                nombre: nombresSecciones['Configuracion'] || 'Configuración',                                           // Nombre que se presenta ewn el menú 
                activa:false                                                  // Indica si la seccion esta activa para el usuario (permisos)
            };
            seccionConfiguracion.constructorParams.baseUrl = baseUrl;
            secciones.Configuracion = secciones.Configuracion || {};
            $.extend(seccionConfiguracion, secciones.Configuracion);

            var seccionVersiones =
            {
                constructorParams: paramsConstructores['Versiones'] || {},
                codigo: "PaginaWeb.Members.Versiones",             // Js con la lógica de la secc
                html: null,                                                     // Aqui se conserva el html de la seccion para evitar llamadas a servidor
                scripts: [

                    rutaModulo + "AdminVersiones.js"
                ],
                styles: [
                    rutaModulo + "AdminVersiones.css",
                    rutaModulo + "AdminVersiones-theme-gray.css"
                ],
                icono: " ",                               // Icono que se presenta en el menu
                nombre: nombresSecciones['Versiones'] || 'Versiones',                                           // Nombre que se presenta ewn el menú 
                activa: false                                                 // Indica si la seccion esta activa para el usuario (permisos)
            };
            seccionVersiones.constructorParams.baseUrl = baseUrl;
            secciones.Versiones = secciones.Versiones || {};
            $.extend(seccionVersiones, secciones.Versiones);


            var seccionInstalaciones =
            {
                constructorParams: paramsConstructores['Instalaciones'] || {},
                codigo: "PaginaWeb.Members.Instalaciones",             // Js con la lógica de la secc
                html: null,                                                     // Aqui se conserva el html de la seccion para evitar llamadas a servidor
                scripts: [

                    rutaModulo + "AdminInstalaciones.js",
                ],
                styles: [                   
                    rutaModulo + "AdminInstalaciones.css"
                ],
                icono: " ",                               // Icono que se presenta en el menu
                nombre: nombresSecciones['Instalaciones'] || 'Instalaciones',                                           // Nombre que se presenta ewn el menú 
                activa: false                                                 // Indica si la seccion esta activa para el usuario (permisos)
            };
            seccionInstalaciones.constructorParams.baseUrl = baseUrl;
            secciones.Instalaciones = secciones.Instalaciones || {};
            $.extend(seccionInstalaciones, secciones.Instalaciones);

            _secciones["Archivos"] = seccionArchivos;
            _secciones["Cotizaciones"] = seccionCotizaciones;
            _secciones["Productos"] = seccionProductos;
            _secciones["Versiones"] = seccionVersiones;
            _secciones["Instalaciones"] = seccionInstalaciones;
            _secciones["Configuracion"] = seccionConfiguracion;            
            

            _seccionesManager = new His.Manejadores.Secciones.SeccionesManager("Members", _secciones,"Archivos",
                {panelLoad: "#CotenidoWeb #innerCotenidoWeb"  });
            
            _seccionesManager.onInicioCarga = function () {
                if (Members.onInicioCambioSeccion instanceof Function) 
                    Members.onInicioCambioSeccion();                
            }

            _seccionesManager.onFinalizaCarga = function () {
                if (Members.onFinalizoCambioSeccion instanceof Function) 
                    Members.onFinalizoCambioSeccion();                
            }
        },
        setMenu: function(menu)
        {
            _menuManager = menu;
        },

        cambiarTema: function (tema) {
            _seccionesManager.cambiarTema(tema);
       },
        editarUsuario:function(idUsuario) {
            try {
                PaginaWeb.Members.Configuracion.Usuarios.Edicion({ callbackRefrescar: _menuManager.onEditarUsuarios }).abrir({ idUsuario: idUsuario, permisoEditar: false});
            } catch (e) {
                console.log(e);
            }
       },
        onEditarUsuarios: function () {
            try {
                    
            } catch (e) {
                console.log(e);
            } 
        }


    };

   Members._init(paramsConstructorSecciones, nombresSeccionesParam, seccionesParam,baseUrlParam);
    return Members;

    //#endregion

};


 