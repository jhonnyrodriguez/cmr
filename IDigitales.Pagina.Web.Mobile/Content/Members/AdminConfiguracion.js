﻿var PaginaWeb = PaginaWeb || {}; 
PaginaWeb.Members = PaginaWeb.Members || {};
PaginaWeb.Members.Configuracion = function (pars) {

 

    //#region "Variables "
    var ctx=null;
    var params = {
        contenedor:"wrapper-configuracion",
        
    };
    var timeOutSize = false;
    var resizeFunction4Menu = false;
    var ultimaOpcion = "";
    $.extend(true, params, pars);
    var subSeccion = null;
    //#endregion "Variables "

    //#region "Metodos "

    //#region inicializacion
    function inicializar() {
        try {
            ctx = $("." + params.contenedor);
            inicializaEventos();
           
        } catch (e) {
            self._error(e);
        } 
    }


    function inicializaEventos() {
        try {
            ctx.find('.link-menu-config').off("click", onClickSeleccion).on("click", onClickSeleccion);
            $("#btnAtras").off("click", onClickAtras).on("click", onClickAtras);
            $("#btnRegresar").off("click", onClickRegresar).on("click", onClickRegresar);
            configuraBotonRegresar();
        } catch (e) {
            self._error(e);
        } 
    }
    //#endregion

    //#region Eventos
    function onClickAtras() {
        try {
            botonBackMenuConf();
            return false;
        } catch (e) {
            self._error(e);
        } 
    }


    function botonBackMenuConf() {
        try {
            $(".panel-botones-menu .ca-menu.cc_menu.minimizado").removeClass('minimizado');
            $(".panel-botones-menu").removeClass('minimizado');
            $('ul.ca-menu > li').removeClass("selected");
            $('#contenido').removeClass("visible");
            $('#btnAtras').hide();

            if (typeof UnloadConf !== "undefined")
                UnloadConf(); 
        } catch (e) {
            self._error(e);
        } 
    }



    function onClickRegresar() {
        try {
            window.history.go(-1); return false;
        } catch (e) {
            self._error(e);
        } 
    }
    function onClickSeleccion() {
        try {
            var ul = $(event.currentTarget).parent().parent();
            event.preventDefault();

            var nombreMenu = $(this).attr('id');

            menuSeleccionado(nombreMenu);

            if ($('#contenido > .ca_content_' + nombreMenu).length > 0) {
                $('#btnAtras').hide();
                $(ul).addClass('minimizado').one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",
                    function() {


                        if (timeOutSize) {
                            clearTimeout(timeOutSize);
                        }
                        timeOutSize = setTimeout(function() {
                                var contenedor = $("#contenido");
                                contenedor.css("padding-top",
                                    $(".panel-botones-menu .ca-menu.cc_menu.minimizado").outerHeight(true) + "px");

                                timeOutSize = false;
                            },
                            300);
                    });
                $(".panel-botones-menu").addClass('minimizado');
                $('#contenido').addClass("visible");
                //$(event.currentTarget).parents('.middle').addClass('minimizado');

                $.each($('#contenido > div'),
                    function(i, e) {
                        if ($(e).attr('class').indexOf(nombreMenu) > -1) {
                            $(e).addClass("visible");
                            $(e).show();
                        } else {
                            $(e).removeClass("visible");
                            $(e).hide();
                        }
                    });

                $.each($('.link-menu-config'),
                    function(i, e) {
                        if ($(e).attr('id') === nombreMenu) {
                            $(e).parent().addClass("selected");
                        } else {
                            $(e).parent().removeClass("selected");
                        }
                    });
            }

            configuraBotonRegresar();
        } catch (e) {
            self._error(e);
        } 
    }

    function menuSeleccionado(menu) {
        if (ultimaOpcion === menu) return;

        //var menu = $(elem).attr("id");
        switch (menu) {
        case "itemGeneral":
            subSeccion = PaginaWeb.Members.Configuracion.General(params);
            subSeccion.iniciar();
            resizeFunction4Menu = false;
            break;
        case "itemUsuarios":
                subSeccion = PaginaWeb.Members.Configuracion.Usuarios(params);
                subSeccion.iniciar();
            resizeFunction4Menu = false;
                break;
        case "itemContactos":
            subSeccion = PaginaWeb.Members.Configuracion.Contactos(params);
            subSeccion.iniciar();
            resizeFunction4Menu = false;
            break;
        case "itemInstalaciones":
            subSeccion = PaginaWeb.Members.Configuracion.Instalaciones(params);
            subSeccion.iniciar();
            resizeFunction4Menu = false;
            break;
        }

        ultimaOpcion = menu;
    }

    function configuraBotonRegresar(hideB) {

        if ($(".panel-botones-menu .ca-menu.cc_menu.minimizado").length === 1) {
            $('#btnAtras').show().css("z-index", 2);
        } else {
            $('#btnAtras').hide();
        }
    }

    //#endregion
    //#endregion "Metodos "

    //#region "Constructores "
    var self = {
        funcionesPublicas: {
             
        },
        iniciar: function (onIniciar) {
             
            try {
                $(".wrapper-configuracion").show();
                $(".fullWapper").addClass("height100p");

            if (onIniciar && typeof onIniciar === "function") {
                onIniciar(undefined);
                }
          
                 inicializar();
            } catch (e) {
                self._error(e);
            }
        },
        finalizar: function (onFinalizar) {
            try {
                $(".wrapper-configuracion").hide();
                $(".fullWapper").removeClass("height100p");
               
                if (subSeccion && subSeccion.Finalizar)
                    subSeccion.Finalizar();

                if (onFinalizar && typeof onFinalizar === "function") {
                    onFinalizar();
                }
            } catch (e) {
                self._error(e);
            }
        },
        onCambioLayout: function (contenedorSize) {

        },
                     
        _error: function (e) {
            console.log(e);
        }
    };
    //#endregion "Constructores "


    return self;

};


PaginaWeb.Members.Configuracion.General = function (parms) {


    //#region "Variables"
    var ctx = $('#General');
    var util = null;
    var params = 
    {
        baseUrl:"",
        url: {
            cargar: parms.baseUrl +"/Members/Configuracion_General", 
            guardar: parms.baseUrl +"/Members/GuardarConfiguracionGeneral"
        }
    }
    $.extend(true, params, parms);
    //#endregion

    //#region "Metodos "


    //#region Mapeos
    var map = {
        Directorio:"RutaAplicaciones"
    };
    //#endregion

    //#region Comunes
    function error(err) {
        console.log(err);
    }
    function recuperarDatos() {
        var rutas = [];
        ctx.find(".ruta").each(function (index) {
            var el = {}
            var ctrl = $(this);
            el.Tipo = ctrl.data("tipo");
            el.Descripcion = ctrl.data("des");
            el.Directorio = ctrl.val();
            rutas.push(el);
        });
        return rutas;
    }
    
    //#endregion

    //#region Inicializar
    function inicializar(data) {
        ctx.html(data);
        util = PaginaWeb.Utils(ctx);

        inicializaControles();
        var datos = ctx.find("#datosConf").data("elemento");
        if (datos) util.setDatos(map, datos);

    }

    function inicializaControles() {
        try {
            
            ctx.find("#btnGuardar").off("click", guardar).on("click", guardar);

           
        } catch (e) {
            error(e);
        }

    }
    //#endregion
    
    //#region

    function guardar() {
        try {
            var datos = recuperarDatos(map);
            //var ruta = ctx.find("#datosConf").data("elemento");
            //ruta.Directorio = datos.Directorio;
            
            util.Accion(params.baseUrl + params.url.guardar, { rutas: datos }, function (data) {
                if (data.Resultado === 0) {
                    ctx.find("#datosConf").idigitalesNotificaciones('Configuración', "Se guardo la configuración", 'success');
                } else {
                    ctx.find("#datosConf").idigitalesNotificaciones('Configuración', "Ocurrio un error al guardar la configuracion", 'error');
                }
            },
                "post", "json");
        } catch (e) {
            ord._error(e);
        }

    }

    //#endregion


    //#endregion

    var objConf = {
        iniciar: function () {
            var ut = PaginaWeb.Utils();
            ctx.html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
            ut.Accion(params.url.cargar,
                {}, inicializar, "get", "html");
        },

        finalizar: function (onFinalizar) {

        },

    };
    return objConf;
};

PaginaWeb.Members.Configuracion.Usuarios = function (parms) {


    //#region "Variables"
    var ctx = $('#confUsu');
    var util = PaginaWeb.Utils(ctx, _Members.urlBase());
    var params =
    {
        baseUrl: "",
        url: {
            cargar: parms.baseUrl + "/Members/Usuarios",
            lista: parms.baseUrl +"/Members/ListaUsuarios"
        },
        pagina:1
    }
    $.extend(true, params, parms);

    var catalogos = {
        UsuarioTipoPorUsuario: ["busquedaTipo"],
        InstalacionesUsuario: ["busquedaInstalacion"]
    }

  
    var catalogosParametros = {
        UsuarioTipoPorUsuario: [JSON.parse($.cookie("Usuario")).Tipo]
    }
    //#endregion

    //#region "Metodos "


    //#region Mapeos
    var map = {
        Directorio: "RutaAplicaciones"
    };
    //#endregion

    //#region Comunes
    function error(err) {
        console.log(err);
    }
    //#endregion

    //#region Inicializar
    function inicializar(data) {
        ctx.html(data);
        cargaUsuarios();
        util.getCatalogos(catalogos, catalogosParametros);
    }

    function cargaUsuarios() {

        ctx.find(".multiple-select").multipleSelect({
            position: "bottom",
            container: "#confUsu",
            dropWidth: "300rem",
            selectAll: false,
            selectAllText: 'Seleccionar todos',
            allSelected: 'Todos',
            countSelected: '# de % seleccionado',
            noMatchesFound: 'Sin resultados',
            onClick: function (view) {
            },
            filter: true,
            styler: function (value) {
                return 'font-size: 13rem;';
            }
        });

        ctx.find("#busquedaNombre").off("keyup", filtrarUsuarios).on("keyup", filtrarUsuarios);
        ctx.find("#busquedaTipo").off("change", filtrarUsuarios).on("change", filtrarUsuarios);
        ctx.find("#busquedaInstalacion").off("change", filtrarUsuarios).on("change", filtrarUsuarios);


        ctx.find(".listaUsuarios").html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
        util.Accion(params.url.lista, { pagina: params.pagina, tipoUsuario: JSON.parse($.cookie("Usuario")).Tipo }, inicializaControles, "get", "html");
    }

    function inicializaControles(data) {
        try {
            ctx.find(".listaUsuarios").html(data);
            //util = PaginaWeb.Utils(ctx);
            ctx.find(".dropdown a.dropdown-item").off("click", clickMenu).on("click", clickMenu);
            ctx.find("#btnNuevoUsuario").off("click", clickNuevo).on("click", clickNuevo);

          
        } catch (e) {
            error(e);
        }

    }
    //#endregion

    //#region

    function clickMenu() {
        var data = $(this).find("i").data();
        if (data.opcion === "editar") {
            PaginaWeb.Members.Configuracion.Usuarios.Edicion({ callbackRefrescar:cargaUsuarios}).abrir({ idUsuario: data.id });
        } else if (data.opcion === "vistas") {
            PaginaWeb.Members.Configuracion.Usuarios.VistasPermisos().abrir({ idUsuario: data.id });
        } else if (data.opcion === "eliminar") {
            PaginaWeb.Members.Configuracion.Usuarios.Edicion({ callbackRefrescar: cargaUsuarios }).borrar({ idUsuario: data.id });
        }
    }
    
    function clickNuevo() {
        PaginaWeb.Members.Configuracion.Usuarios.Edicion({ callbackRefrescar: cargaUsuarios }).abrir({ idUsuario: 0});
    }

    function filtrarUsuarios() {
        try {
            var nombre = ctx.find("#busquedaNombre").val();
            var tipo = ctx.find("#busquedaTipo").multipleSelect('getSelects');
            var instalacion = ctx.find("#busquedaInstalacion").multipleSelect('getSelects');
            tipo = tipo != null ? tipo.join(",") : null;
            instalacion = instalacion != null ? instalacion.join(",") : null;
            recargarUsuarios(nombre, tipo, instalacion);
        } catch (e) {
            console.log(e);
        } 
    }


    function recargarUsuarios(nombre, tipo, instalacion) {
        ctx.find(".listaUsuarios").html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
        util.Accion(params.url.lista, { pagina: params.pagina, filtro: nombre, tipo: tipo, instalacion: instalacion, tipoUsuario: JSON.parse($.cookie("Usuario")).Tipo}, inicializaControles, "get", "html");
    }
    //#endregion


    //#endregion

    var objConf = {
        iniciar: function () {
            var ut = PaginaWeb.Utils();
            ctx.html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
            util.Accion( params.url.cargar,{ }, inicializar, "get", "html");
        },

        finalizar: function (onFinalizar) {

        },

    };
    return objConf;
};

PaginaWeb.Members.Configuracion.Usuarios.Edicion = function (pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {};

    var params = {
        callbackRefrescar: pars.callbackRefrescar,

        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Usuario guardado",
            ErrorGuardar: "Ocurrio un problema al guardar el usuario",
            Borrado: "Se borro el producto",
            ErrorBorrar: "No se pudo borrar el producto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el usuario?",
            ConfirmarTitulo: "Borrar el producto",
            Seleccionar: "Seleccione un producto",
            ErrorRepetido: "Ya existe un usuario con ese nombre"
        },
        url: {
            abrir: urlBase + "/Members/_EdicionUsuario",
            Guardar: urlBase + "/Members/AgregarActualizarUsuario",
            Borrar: urlBase + "/Members/_BorrarUsuario"
        },
        modal: {
            idModal: "modalUsuarios",
            idDatos: "contDatos",
            selContPopUp: "#ContenedorPopUp1"
        }

    }


    //#region Mapeos
    var objeto = {
        IdUsuario: "idUsuario",
        Activo: "activo",
        Nombre: "nombre",
        Password: "pass",
        Confirmacion: "confirmacion",
        Tipo: "tipo",
        Email:"email"
    }

    var catalogos = {
        UsuarioTipoPorUsuario: ["tipo"]
    }
    var catalogosParametros = {
        UsuarioTipoPorUsuario: [JSON.parse($.cookie("Usuario")).Tipo] 
    }
    var editar = true;

    //#endregion

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }

    //#endregion

    //#region Modal

    //#region Inicializar

    function abrirModal(elemento) {
        editar = elemento.permisoEditar;
        LayoutManager.abrirPopUpAjax(params.modal.idModal, params.modal.selContPopUp, params.url.abrir,
            elemento, "", { minWidth: '500rem', minHeight: '300rem' }, inicializarModal, "post", cerrar, {});
    }
    function inicializarModal() {
        try {
            ctx = $("#" + params.modal.idModal);
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            //inicializar Controles
            inicializarControlesModal();
            util.getCatalogos(catalogos, catalogosParametros);
            //cargar datos iniciales
            var datos = ctx.find("#" + params.modal.idDatos).data("elemento");
            if (datos) {
                datos.Confirmacion = datos.Password;
                util.setDatos(objeto, datos);
            }

            if (editar === false) {
                ctx.find("#datosTipo").hide();
            } else {
                ctx.find("#datosTipo").show();
            }
        } catch (err) {
            error(err);
        }
    }

    function inicializarControlesModal() {
        try {
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });

            ctx.find("div#activoWrap").idigitalesCheckboxes();

            //Eventos Botones
            ctx.find("#btnGuardarMod").off("click").on("click", onClickGuardarMod);
            ctx.find("#btnCancelarMod").off("click").on("click", onClickCancelarMod);


        }
        catch (err) {
            error(err);
        }
    }

    
    function onClickGuardarMod() {
        try {
            var cont = ctx.find("#" + params.modal.idDatos)[0];
            if (validador.validar(cont)) {
                var datos = util.getDatos(objeto);
                if (datos) {
                    if (datos.Password === datos.Confirmacion) {
                        guardar(datos);
                    } else {
                        ctx.idigitalesNotificaciones('', "La Contraseña no coincide", 'error');
                    }
                }
            }
        } catch (err) {
            error(err);
        }
    }
   

    function onClickCancelarMod() {
        try {
            alertify[params.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }
    //#endregion

    //#endregion

    //#region Ajax Agregar-Editar-Borrar

    function guardar(datos) {
        try {
            ajax.AccionModal(params.url.Guardar, { usuario: datos }, function (data) {
                if (data.Resultado === 0) {
                    ctx.idigitalesNotificaciones("", params.msgs.Guardado, 'success');
                    ctx.find("#idUsuario").val(data.Id);
                    if (typeof params.callbackRefrescar === "function") {
                        params.callbackRefrescar();
                    }
                    setTimeout(function () { alertify[params.modal.idModal]().close();}, 1000);
                } else {
                    console.log(data);
                    if (data.Mensaje === "Repetido")
                        ctx.idigitalesNotificaciones('', params.msgs.ErrorRepetido, 'error');
                    else{
                        ctx.idigitalesNotificaciones('', params.msgs.ErrorGuardar, 'error');
                    }
                }
            },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

    function confirmarUsuarioBorrar(idElemento) {
        if (idElemento) {
            alertify.confirm(params.msgs.ConfirmarTitulo, params.msgs.Confirmar,
                function () { borrarUsuario(idElemento); },
                function () { });
        }
    }

    function borrarUsuario(idUsuario) {
        try {
            ctx = ctx || $("#" + params.modal.idModal);

            ajax.Accion(params.url.Borrar, idUsuario,
                function (data) {
                    if (data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("", params.msgs.ProductoBorrado, 'success');
                        if (typeof params.callbackRefrescar === "function") {
                            params.callbackRefrescar();
                        }
                    } else {
                        console.log(data.Mensaje);
                        $("#cntListaPrecios").idigitalesNotificaciones('', params.msgs.ErrorBorrar, 'error');
                    }
                },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

    
    //#endregion


   
    //#region Cerrar
    function cerrar() {
        try {
            // ctx.find("#btnGuardar").off("click", onClickGuardar);
            // ctx.find("#btnCancelar").off("click", onClickCancelar);
        } catch (e) {
            error(e);
        }
    }
    //#endregion
    //#endregion


    //#region "Metodos Publicos"
    var forma = {
        abrir: function (elemento) {
            abrirModal(elemento);
        },
        borrar: function (idElemento) {
            confirmarUsuarioBorrar(idElemento);
        }

    }
    return forma;
    //#endregion 

}

PaginaWeb.Members.Configuracion.Usuarios.VistasPermisos = function (pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {};

    var params = {
        callbackRefrescar: pars.callbackRefrescar,

        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Cambios guardados",
            ErrorGuardar: "Ocurrio un problema al guardar los cambios",
            Borrado: "Se borro el producto",
            ErrorBorrar: "No se pudo borrar el producto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el producto?",
            ConfirmarTitulo: "Borrar el producto",
            Seleccionar: "Seleccione un producto"
        },
        url: {
            abrir: urlBase + "/Members/ConfiguracionVistasPermisos",
            Guardar: urlBase + "/Members/AgregarActualizarVistasPermisos",
            Borrar: urlBase + "/Members/_BorraTransaccion",
        },
        modal: {
            idModal: "modalConfigUsuarios",
            idDatos: "contDatos",
            selContPopUp: "#ContenedorPopUp1"
        }

    }


    //#region Mapeos
    

    //#endregion

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }

    //#endregion

    //#region Modal

    //#region Inicializar

    function abrirModal(elemento) {
       
        LayoutManager.abrirPopUpAjax(params.modal.idModal, params.modal.selContPopUp, params.url.abrir,
            elemento, "", { minWidth: '600rem', minHeight: '550rem', Height: '550rem' }, inicializarModal, "post", cerrar, {});
    }
    function inicializarModal() {
        try {
            ctx = $("#" + params.modal.idModal);
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            //inicializar Controles
            inicializarControlesModal();
            //util.getCatalogos(catalogos);
           //cargar datos iniciales
           var datos = ctx.find("#" + params.modal.idDatos).data("elemento");
           if (datos)
               cargaSeleccion(datos);
        } catch (err) {
            error(err);
        }
    }

    function inicializarControlesModal() {
        try {

            //tabs
            ctx.find(".tabuladoresConfiguracionModulo").off("change").on("change", cambioCheckTab);
            ctx.find("input:radio[name='TConfiguracionModulo']:first").attr('checked', true).change();

            ctx.find("div.activoWrap").idigitalesCheckboxes();
            
            //Eventos Botones
            ctx.find("#btnGuardarMod").off("click").on("click", onClickGuardarMod);
            ctx.find("#btnCancelarMod").off("click").on("click", onClickCancelarMod);
            ctx.find("#listaCategorias").find(".icono-responsable").off("click").on("click", onClickActivarResponsable);
        }
        catch (err) {
            error(err);
        }
    }

    function cambioCheckTab() { 
        if (this.checked == true) {
            ctx.find(".checkSelected").removeClass("checkSelected");
            ctx.find(".checkTransition").removeClass("checkTransition");
            var id = this.value;
            ctx.find('#labelTab_' + id).addClass("checkSelected");
            ctx.find('#articulo_' + id).addClass("checkTransition");

            // obtenerDatos(id);
        }
    }

   
    //#region Eventos controles
    function cargaSeleccion(datos) {
        try {
            if (datos.Usuario && datos.Usuario.Vistas) {
                var ctrlista = ctx.find("#listaVista");
                datos.Usuario.Vistas.forEach(function(element) {
                    ctrlista.find("#" + element.IdVista + " .eActivo").prop("checked", true).trigger('change');
                });
            }
            if (datos.Usuario && datos.Usuario.Permisos) {
                var ctrlistap = ctx.find("#listaPermisos");
                datos.Usuario.Permisos.forEach(function (element) {
                    ctrlistap.find("#" + element.IdPermiso +" .eActivo").prop("checked", true).trigger('change');
                });
            }
           
            if (datos.Usuario && datos.Usuario.Aplicaciones) {
                var ctrAplicaciones = ctx.find("#listaAplicaciones");
                datos.Usuario.Aplicaciones.forEach(function (element) {
                    ctrAplicaciones.find("#" + element.IdAplicacion + " .eActivo").prop("checked", true).trigger('change');
                });
            }

            if (datos.Usuario && datos.Usuario.Proyectos) {
                var ctrProyectos = ctx.find("#listaInstalaciones");
                datos.Usuario.Proyectos.forEach(function (element) {
                    ctrProyectos.find("#" + element.Id + " .eActivo").prop("checked", true).trigger('change');
                });
            }

            if (datos.Usuario && datos.Usuario.ReportesCategorias) {
                var ctrCategorias = ctx.find("#listaCategorias");
                datos.Usuario.ReportesCategorias.forEach(function (element) {
                    ctrCategorias.find("#" + element.Id + " .eActivo").prop("checked", true).trigger('change');

                    var res = datos.Usuario.ReportesCategorias_Responsables.find(x => x.Id === element.Id);
                    var clase = res != null ? "activo" : "";
                    ctrCategorias.find("#" + element.Id + " .icono-responsable").addClass(clase);
                });
            }
        } catch (err) {
            error(err);
        }
    }
    function obtenerSeleccion() {
        try {
           
            var datos = ctx.find("#" + params.modal.idDatos).data("elemento");
            var usuario = {};
            usuario.idUsuario = datos.Usuario.IdUsuario;
            var vistas = [];
            ctx.find("#listaVista .permiso").each(function (index) {
                var checked = $(this).find(".eActivo").prop("checked");
                if (checked)
                    vistas.push($(this).data("id"));
            });
            var permisos = [];
            ctx.find("#listaPermisos .permiso").each(function (index) {
                var permitido = $(this).find(".eActivo").prop("checked");
                if (permitido)
                    permisos.push($(this).data("id"));
            });
        
            var aplicaciones = [];
            ctx.find("#listaAplicaciones .permiso").each(function (index) {
                var permitido = $(this).find(".eActivo").prop("checked");
                if (permitido)
                    aplicaciones.push($(this).data("id"));
            });


            var instalaciones = [];
            ctx.find("#listaInstalaciones .permiso").each(function (index) {
                var permitido = $(this).find(".eActivo").prop("checked");
                if (permitido)
                    instalaciones.push($(this).data("id"));
            });

            var categorias = [];
            var responsables = [];
            ctx.find("#listaCategorias .permiso").each(function (index) {
                var permitido = $(this).find(".eActivo").prop("checked");
                if (permitido)
                    categorias.push($(this).data("id"));
                var responsable = $(this).find(".icono-responsable").hasClass("activo");
                if (responsable === true)
                    responsables.push($(this).data("id"));

            });

            usuario.vistas = vistas;
            usuario.permisos = permisos;
            usuario.Aplicaciones = aplicaciones;
            usuario.Instalaciones = instalaciones;
            usuario.Categorias = categorias;
            usuario.Responsables = responsables;
            return usuario;
        } catch (err) {
            error(err);
        }
        return {};
    }

    function onClickGuardarMod() {
        try {
            var datos = obtenerSeleccion();
            if (datos)
                guardar(datos);
            
        } catch (err) {
            error(err);
        }
    }
  
    function onClickCancelarMod() {
        try {
            alertify[params.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }

    function onClickActivarResponsable() {
        try {
            $(this).toggleClass("activo");
        } catch (e) {
            error(err);
        } 
    }
    //#endregion

    //#endregion

    //#region Ajax Agregar-Editar-Borrar

    function guardar(datos) {
        try {
            ajax.AccionModal(params.url.Guardar, datos, function (data) {
                if (data.Resultado === 0) {
                    ctx.idigitalesNotificaciones("", params.msgs.Guardado, 'success');
                    ctx.find("#idCuenta").val(data.Id);
                    if (typeof params.callbackRefrescar === "function") {
                        params.callbackRefrescar();
                    }
                    setTimeout(function () { alertify[params.modal.idModal]().close(); }, 1000);

                } else {
                    console.log(data);
                    ctx.idigitalesNotificaciones('', params.msgs.ErrorGuardar, 'error');
                }
            },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

    function confirmarListaPreciosBorrar(idElemento) {
        if (idElemento) {
            alertify.confirm(params.msgs.ConfirmarTitulo, params.msgs.Confirmar,
                function () { borrarListaPrecios(idElemento); },
                function () { });
        }
    }

    function borrarListaPrecios(idLista) {
        try {
            ctx = ctx || $("#" + params.modal.idModal);

            ajax.Accion(params.url.Borrar, { idLista: idLista },
                function (data) {
                    if (data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("", params.msgs.ProductoBorrado, 'success');
                        if (typeof params.callbackRefrescar === "function") {
                            params.callbackRefrescar();
                        }
                    } else {
                        console.log(data.Mensaje);
                        $("#cntListaPrecios").idigitalesNotificaciones('', params.msgs.ErrorBorrar, 'error');
                    }
                },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

   
    //#endregion


   
    //#region Cerrar
    function cerrar() {
        try {
            // ctx.find("#btnGuardar").off("click", onClickGuardar);
            // ctx.find("#btnCancelar").off("click", onClickCancelar);
        } catch (e) {
            error(e);
        }
    }
    //#endregion
    //#endregion


    //#region "Metodos Publicos"
    var forma = {
        abrir: function (elemento) {
            abrirModal(elemento);
        },
        borrar: function (idElemento) {
            confirmarListaPreciosBorrar(idElemento);
        }

    }
    return forma;
    //#endregion 

}

PaginaWeb.Members.Configuracion.Contactos = function (parms) {


    //#region "Variables"
    var ctx = $('#confContactos');
    var util = PaginaWeb.Utils(ctx);
    var params =
    {
        baseUrl: "",
        url: {
            cargar: parms.baseUrl + "/Members/Configuracion_Contactos",
            lista: parms.baseUrl + "/Members/ListaContactos",
            eliminar: parms.baseUrl + "/Members/EliminarContacto",
        },
        pagina: 1
    }
    $.extend(true, params, parms);
    //#endregion

    //#region "Metodos "


    //#region Mapeos
    var map = {
        Directorio: "RutaAplicaciones"
    };
    //#endregion

    //#region Comunes
    function error(err) {
        console.log(err);
    }
    //#endregion

    //#region Inicializar
    function inicializar(data) {
        ctx.html(data);
        inicializaEventos();
    }


    function inicializaEventos() {
        ctx.find(".itemAplicacion").off("click", onClickAplicaciones).on("click", onClickAplicaciones);
        ctx.find("#btnAgregarContacto").off("click", onClickAgregarContacto).on("click", onClickAgregarContacto);
    }

    function onClickAplicaciones() {
        
            $(this).find("input.confSeleccionAplicaciones").prop("checked", true);
            var idApp = $(this).attr("data-id");
            cargaContactos(idApp);
      
    }


    function onClickAgregarContacto() {
        PaginaWeb.Members.Configuracion.Contactos.Edicion({ callbackRefrescar: recarga }).abrir({ idContacto: 0 });
    }


    function recarga() {
        if ($(".confPanelAplicaciones input[type='radio']:checked").length > 0) {
            var idApp = $(".confPanelAplicaciones input[type='radio']:checked").parent().data('id');
            cargaContactos(idApp);
        }
    }
    function cargaContactos(idApp) {
        ctx.find("#panelContactos").html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
        util.Accion(params.url.lista, { idAplicacion: idApp }, inicializaControles, "get", "html");
    }

    function inicializaControles(data) {
        try {
            ctx.find("#panelContactos").html(data);
            //util = PaginaWeb.Utils(ctx);
            ctx.find(".dropdown a.dropdown-item").off("click", clickMenu).on("click", clickMenu);
            
        } catch (e) {
            error(e);
        }

    }
    //#endregion

    //#region

    function clickMenu() {
        var data = $(this).find("i").data();
        var idApp = 0;
        if ($(".confPanelAplicaciones input[type='radio']:checked").length > 0) {
            idApp = $(".confPanelAplicaciones input[type='radio']:checked").parent().data('id');
        }
        if (data.opcion === "editar") {
            PaginaWeb.Members.Configuracion.Contactos.Edicion({ callbackRefrescar: recarga }).abrir({ idContacto: data.id, idAplicacion: idApp });
        } else if (data.opcion === "eliminar") {
            PaginaWeb.Members.Configuracion.Contactos.Edicion({ callbackRefrescar: recarga }).borrar({ idContacto: data.id, idAplicacion: idApp });
        }
    }


    
  
    //#endregion


    //#endregion

    var objConf = {
        iniciar: function () {
            var ut = PaginaWeb.Utils();
            ctx.html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
            util.Accion(params.url.cargar, {}, inicializar, "get", "html");
        },

        finalizar: function (onFinalizar) {

        },

    };
    return objConf;
};

PaginaWeb.Members.Configuracion.Contactos.Edicion = function (pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {};
    var _callback = null;
    var params = {
        callbackRefrescar: pars.callbackRefrescar,

        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Contacto guardado",
            ErrorGuardar: "Ocurrio un problema al guardar el contacto",
            Borrado: "Se borro el contacto",
            ErrorBorrar: "No se pudo borrar el contacto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el contacto?",
            ConfirmarTitulo: "Borrar el contacto",
            Seleccionar: "Seleccione un contacto"
        },
        url: {
            abrir: urlBase + "/Members/_EdicionContacto",
            Guardar: urlBase + "/Members/AgregarActualizarContacto",
            Borrar: urlBase + "/Members/_BorrarContacto",
            buscar: urlBase + "/Catalogos/BuscarEntidad"
        },
        modal: {
            idModal: "modalContactos",
            idDatos: "contDatos",
            selContPopUp: "#ContenedorPopUp1"
        }

    }
    var _aplicacionesSeleccionadas = [];


    //#region Mapeos
    var objeto = {
        IdContacto: "IdContacto",
        Nombre: "Nombre",
        Empresa: "Empresa",
        Email: "Email",
        Aplicaciones:"Aplicacion-IdAplicacion"
    }

    var catalogos = {
        Aplicaciones: ["Aplicacion"]
    }


    //#endregion

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }

    //#endregion

    //#region Modal

    //#region Inicializar

    function abrirModal(elemento) {
        
        LayoutManager.abrirPopUpAjax(params.modal.idModal, params.modal.selContPopUp, params.url.abrir,
            elemento, "", { minWidth: '500rem', minHeight: '310rem' }, inicializarModal, "post", cerrar, {});
    }
    function inicializarModal() {
        try {
            ctx = $("#" + params.modal.idModal);
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            //inicializar Controles
            inicializarControlesModal();
            util.getCatalogos(catalogos);
            //cargar datos iniciales
            var datos = ctx.find("#" + params.modal.idDatos).data("elemento");
            if (datos) {
                datos.IdAplicacion = ctx.find("#" + params.modal.idDatos).data("idapp");
                util.setDatos(objeto, datos);
            }

        } catch (err) {
            error(err);
        }
    }

    function inicializarControlesModal() {
        try {
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });

            var selContactos = ctx.find("#contacto");
            var placeholderProductos = selContactos.data("placeholder");
            selContactos.data("catalogo", "Contactos"); //llenar descripcion
            selContactos.select2({
                ajax: {
                    url: params.url.buscar,
                    dataType: 'json',
                    delay: 200,
                    data: function (par) {
                        return {
                            catalogo: "Contactos",
                            filtro: par.term, // search term
                            page: par.page
                        };
                    },
                    processResults: function (data, page) {
                        return { results: data.Catalogo };
                    },
                    cache: true
                },
                width: '100%',
                allowClear: true,
                placeholder: placeholderProductos,
                escapeMarkup: function (markup) { return markup; },
                minimumInputLength: 3,
                templateResult: util.templateResult,
                templateSelection: util.templateSelection
            });



            ctx.find(".multiple-select").multipleSelect({
                position: "top",
                container: "#modalContactos",
                dropWidth: "202rem",
                selectAllText: 'Seleccionar todas',
                allSelected: 'Todas las aplicaciones',
                countSelected: '# de % seleccionado',
                noMatchesFound: 'Sin resultados',
                onClick: function (option) {
                    var value = option.value;
                    var indexPrevio = _aplicacionesSeleccionadas.indexOf(value);

                    if (indexPrevio > -1) {
                        _aplicacionesSeleccionadas.splice(indexPrevio, 1);
                    } else {
                        _aplicacionesSeleccionadas.push(value);
                        indexPrevio = _aplicacionesSeleccionadas.length - 1;
                    }

                    setTimeout(function () {
                        ctx.find("#Aplicacion").multipleSelect("refresh");
                    }, 0);

                                            
                },
                onCheckAll: function () {
                    setTimeout(function () {
                        var seleccionados = ctx.find("#Aplicacion").multipleSelect("getSelects");

                        seleccionados.forEach(function (val) {
                            if (_aplicacionesSeleccionadas.indexOf(val) === -1)
                                _aplicacionesSeleccionadas.push(val);
                        });
                        ctx.find("#Aplicacion").multipleSelect("refresh");
                 
                    }, 0);

                },
                onUncheckAll: function () {
                    _aplicacionesSeleccionadas = [];
                    setTimeout(function () {
                        ctx.find("#Aplicacion").multipleSelect("refresh");
                    }, 0);
                },

                textTemplate: function ($elm) {
                    var val = $elm.val();
                    var index = _aplicacionesSeleccionadas.indexOf(val);

                    if (index > -1)
                        return "<span class='order'>" + (index + 1) + "</span> " + $elm.html();

                    return $elm.html();
                }

            });
            selContactos.off("change", cambiaContacto).on("change", cambiaContacto);
            //Eventos Botones
            ctx.find("#btnGuardar").off("click").on("click", onClickGuardarMod);
            ctx.find("#btnCancelar").off("click").on("click", onClickCancelarMod);

           
        }
        catch (err) {
            error(err);
        }
    }


    function cambiaContacto() {
        try {
            var contacto = $(this).val();
            if (contacto !== null) {
                var data = $("#contacto").select2("data")[0].elemento;
                util.setDatos(objeto, data);
            } else {
                var contacto = {
                    Aplicaciones: [],
                    Email: "",
                    Empresa: "",
                    IdContacto: 0,
                    Nombre: ""
                }
                util.setDatos(objeto, contacto);
            }
        } catch (e) {
            error(err);
        } 
    }

    function onClickGuardarMod() {
        try {
            var cont = ctx.find("#" + params.modal.idDatos)[0];
            if (validador.validar(cont)) {
                var datos = util.getDatos(objeto);
                if (datos) {
                    guardar(datos);
                    
                }
            }
        } catch (err) {
            error(err);
        }
    }


    function onClickCancelarMod() {
        try {
            alertify[params.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }
    //#endregion

    //#endregion

    //#region Ajax Agregar-Editar-Borrar

    function guardar(datos) {
        try {
            ajax.AccionModal(params.url.Guardar, { contacto: datos, idAplicacion: ctx.find("#Aplicacion").val() }, function (data) {
                if (data.Resultado === 0) {
                    ctx.idigitalesNotificaciones("", params.msgs.Guardado, 'success');
                    ctx.find("#IdContacto").val(data.Id);
                    if (typeof params.callbackRefrescar === "function") {
                        params.callbackRefrescar();
                    }
                    setTimeout(function () { alertify[params.modal.idModal]().close(); }, 1000);
                } else {
                    console.log(data);
                    ctx.idigitalesNotificaciones('', params.msgs.ErrorGuardar, 'error');
                }
            },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

    function confirmarContacto(elemento) {
        if (elemento) {
            alertify.confirm(params.msgs.ConfirmarTitulo, params.msgs.Confirmar,
                function () { borrarContacto(elemento); },
                function () { });
        }
    }

    function borrarContacto(elemento) {
        try {
            ctx = ctx || $("#" + params.modal.idModal);

            ajax.Accion(params.url.Borrar, elemento,
                function (data) {
                    if (data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("", params.msgs.ContactoBorrado, 'success');
                        if (typeof params.callbackRefrescar === "function") {
                            params.callbackRefrescar();
                        }
                    } else {
                        console.log(data.Mensaje);
                        $("#confContactos").idigitalesNotificaciones('', params.msgs.ErrorBorrar, 'error');
                    }
                },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }


    //#endregion



    //#region Cerrar
    function cerrar() {
        try {
            // ctx.find("#btnGuardar").off("click", onClickGuardar);
            // ctx.find("#btnCancelar").off("click", onClickCancelar);
        } catch (e) {
            error(e);
        }
    }
    //#endregion
    //#endregion


    //#region "Metodos Publicos"
    var forma = {
        abrir: function (elemento, callback) {
            _callback = callback;
            abrirModal(elemento);
        },
        borrar: function (elemento) {
            confirmarContacto(elemento);
        }

    }
    return forma;
    //#endregion 

}

PaginaWeb.Members.Configuracion.Instalaciones = function (parms) {


    //#region "Variables"
    var ctx = $('#confInstalaciones');
    var util = PaginaWeb.Utils(ctx);
    var params =
    {
        baseUrl: "",
        url: {
            cargar: parms.baseUrl + "/Members/Configuracion_Instalaciones",
            lista: parms.baseUrl + "/Members/ListaCategorias",
            eliminar: parms.baseUrl + "/Members/EliminarContacto",
            guardar: parms.baseUrl + "/Members/GuardarCategorias",
        },
        pagina: 1
    }
    $.extend(true, params, parms);
    //#endregion

    //#region "Metodos "


    //#region Mapeos
    var map = {
        Directorio: "RutaAplicaciones"
    };
    //#endregion

    //#region Comunes
    function error(err) {
        console.log(err);
    }
    //#endregion

    //#region Inicializar
    function inicializar(data) {
        ctx.html(data);
        inicializaEventos();
    }


    function inicializaEventos() {
        ctx.find(".itemAplicacion").off("click", onClickAplicaciones).on("click", onClickAplicaciones);
    }

    function onClickAplicaciones() {

        $(this).find("input.confSeleccionAplicaciones").prop("checked", true);
        var idApp = $(this).attr("data-id");
        cargaContactos(idApp);

    }

    
    function cargaContactos(idApp) {
        ctx.find("#panelContactos").html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
        util.Accion(params.url.lista, { idInstalacion: idApp }, inicializaControles, "get", "html");
    }

    function inicializaControles(data) {
        try {
            ctx.find("#panelContactos").html(data);
            //util = PaginaWeb.Utils(ctx);
            ctx.find(".categoriaCheckInput").off("click", clickMenu).on("click", clickMenu);

        } catch (e) {
            error(e);
        }

    }

    function resultadoGuardarCategoria(data) {
        try {
            var a = 0;
        } catch (e) {
            error(e);
        }

    }
    //#endregion

    //#region

    function clickMenu() {
        try {
            var idCategoria = $(this).attr("data-id");
            var idApp = 0;
            if ($(".confPanelAplicaciones input[type='radio']:checked").length > 0) {
                idApp = $(".confPanelAplicaciones input[type='radio']:checked").parent().data('id');
            }


            var isChecked = $(this).hasClass("checked");
            if (isChecked === true) { $(this).removeClass("checked");}
            else { $(this).addClass("checked");}

            util.Accion(params.url.guardar, { idInstalacion: idApp, idCategoria: idCategoria, bSeleccion: !isChecked  }, resultadoGuardarCategoria, "post", "json");

           

        } catch (e) {
            error(e);
        }

       
    }




    //#endregion


    //#endregion

    var objConf = {
        iniciar: function () {
            var ut = PaginaWeb.Utils();
            ctx.html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
            util.Accion(params.url.cargar, {}, inicializar, "get", "html");
        },

        finalizar: function (onFinalizar) {

        },

    };
    return objConf;
};
