﻿var PaginaWeb = PaginaWeb || {}; 
PaginaWeb.Members = PaginaWeb.Members || {};
PaginaWeb.Members.Productos = function (params) {
  
    var ctx = null;
    var util = PaginaWeb.Utils();
    var rutaBase = _Members.urlBase();
    var cfg = {
        ajax: _Members.ajax,
      
        baseUrl:"",
        dom: {
            cntTabla: "#contTablas",
            cntPlantilla: "#contPlantilla",
            tabla: "#tabla",
            idMenuContextual: "menu-contextual"
        },
        jq: {
            cntTabla: null,
            cntPlantilla: null,
            tabla: null
        },
        tools: {
            util: util
        },
        urls: {
            lista: rutaBase + "/Members/_ObtenerProductos",
           
        },
        text: {
            html: {
                sinElementos: "<div class='centrado sin-elementos'>No se encontraron productos</div>"
            },
            sinElementos: "No existen productos"
        }
    };


    //enums
    var enumProductoTipos ={
        1:"Estructura",
        2: "Producto",
        3:"Licencia"
    }

    var enumMonedas ={
        1:"MN",
        2:"USD"
    }


    $.extend(true, cfg, params);
   
    function error(e) {
        console.log(e);
    }

    function aleatorio(inferior, superior) {
        var numPosibilidades = superior - inferior;
        var aleat = Math.random() * numPosibilidades;
        aleat = Math.floor(aleat);
        return parseInt(inferior) + aleat;
    }

    function colorAleatorio() {
        var hexadecimal = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F")
        var color_aleatorio = "#";
        for (i = 0; i < 6; i++) {
            var posarray = aleatorio(0, hexadecimal.length);
            color_aleatorio += hexadecimal[posarray];
        }
        return color_aleatorio
    }

    //#region Incializacion

    function iniciaCarga() {
        try {
            iniCfg();
            crearTabla();
            inicializaEventos();
            //cargaTooltipsMenu();            
            crearMenuContextual();                         
        } catch (e) {
            error(e);
        }
    }

    //#region Config

    function iniCfg() {
        ctx = $("#cntSubSeccion");
        cfg.tools.util = PaginaWeb.Utils(ctx, cfg.baseUrl);
        cfg.jq.cntPlantilla = ctx.find(cfg.dom.cntPlantilla);
        cfg.jq.cntTabla = ctx.find(cfg.dom.cntTabla);
        //cfg.tools.util.getCatalogos(catalogos);
        //cargar datos iniciales
    }
    //#endregion

    function inicializaEventos() { 
        $("#btnAgregaProductos").off("click", agregarProductos).on("click", agregarProductos);
        $("#btnAgregaLista").off("click", agregarLista).on("click", agregarLista);
        $("#btnImprimirInventario").off("click", imprimirInventario).on("click", imprimirInventario);
        $("#btnImprimirListasPrecio").off("click", imprimirListasPrecio).on("click", imprimirListasPrecio);        
    };

    function cargaTooltipsMenu() {
        try {
            var m = cfg.msgs;
            var hoverHtml = '<p> ' + ' <h5 class="texto-simple">  ' + m.TooltipTituloRegresar + ' </h5>' + m.TooltipDescRegresar + '</p>';
            LayoutManager.creaTooltipMenu('btnRegresar', hoverHtml, 250);

            hoverHtml = '<p> ' + ' <h5 class="texto-simple">  ' + m.TooltipNuevaTrx + ' </h5>' + m.TooltipDescNuevaTrx + '</p>';
            LayoutManager.creaTooltipMenu('btnNuevaTrx', hoverHtml, 250);

        } catch (e) {
            error(e);
        }
    };

    function agregarProductos() {
        PaginaWeb.Members.Productos.Edicion({ callbackRefrescar: refrescarLista }).abrir(0);
    }
    function agregarLista() {
        PaginaWeb.Members.Productos.Listas({ }).abrir(0);
    }
    

    //#endregion

    //#region Tabla

    //#region Crear Tabla
    function crearTabla() {
        var tblhtml = cfg.jq.cntPlantilla.html();
        var tbl = $(tblhtml);
        cfg.jq.cntTabla.append(tbl);
        cfg.jq.tabla = ctx.find(cfg.dom.tabla).DataTable({
            //data: data,
            //deferLoading: 100,
            //processing: false,
            //scrollY: '65vh',
            //scrollx: false,
            //scrollCollapse: true,
            dom:
                '<"filtros"<"idigitales-renglon"<"search"f>r>>t<"tabla-pie"ip>', // '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',//dom: 'Brtip',//Bfrtip <"toolbar">Brtip
            searching: true,
            order: [[1, 'asc']],
            colReorder: true,
            serverSide: true,
            paging: true,
            pageLength: calcularNumeroRegistros(),
            ajax: {
                url: cfg.urls.lista,
                // contentType: "application/x-www-form-urlencoded",
                type: "POST",
                data: function (d) {                 
                    return d;
                }

            },
            columns: [
                {
                    className: 'celda-icono',
                    orderable: false,
                    data: null,
                    defaultContent: '',
                    width: "10rem",
                    render: function (data) {
                        var html = "";
                        if (data)
                            html = '<i class="icon icon-producto"></i>';
                        return html;
                    }
                },
                {
                    "data": "Modelo",
                    width: "15%",
                    render: function (data, type, row) {
                        var html = '<div><i class="fa fa-minus"></i></div>';
                        if (data) {
                            html = '<div class="tooltip"><span class="elipsis">' + data
                                + '</span><span class="tooltiptext">' + data + '</span>'
                                + '</div>';
                        }
                        return html;
                    }
                },
                {
                    "data": "Nombre",
                    render: function (data, type, row) {
                        var html = '<div><i class="fa fa-minus"></i></div>';
                        if (data) {
                            html = '<div class="tooltip"><span class="elipsis">' + data
                                + '</span><span class="tooltiptext">' + data + '</span>'
                                + '</div>';
                        }
                        return html;
                    }
                },
               
                {
                    data: "Descripcion",
                    //orderable: false,
                    render: function (data) {
                        var html = "";
                        if (data)
                            html = '<div class="tooltip"><span class="elipsis">' + data
                                +'</span><span class="tooltiptext">'+ data+'</span>'
                                +'</div>';
                        return html;
                    },

                },
               
                {
                    data: "Clasificacion",
                    className: '', 
                    width: "15%",
                    //orderable: false,
                    render: function (data,t, r) {
                        var html = "<i class='fa fa-minus'></i>";
                        var color=colorAleatorio();
                        if (r && r.Clasificaciones)
                            html = '<div class="tooltip"><span class="elipsis" style="color:' + color + '">' + r.Clasificaciones.Nombre
                                + '</span><span class="tooltiptext" >' + r.Clasificaciones.Nombre + '</span>'
                                + '</div>';
                            //html = '<span class="badge inline pildora ' + r.Clasificaciones.Nombre + '">'
                            //    + r.Clasificaciones.Nombre + '</span>';
                        return html;
                    }

                },
                {
                    data: "Tipo",
                    width: "10%",
                    className: 'centrada', 
                    //orderable: false,
                    render: function (data) {
                        var html = "<i class='fa fa-minus'></i>";
                        if (data) {
                            var tipo=enumProductoTipos[data];
                            //html = '<span class="badge punto ' + tipo + '"></span>' +
                            //    '<span class="fuente-bold ' + tipo + '">' + tipo + '</span>';
                            html = '<span class="badge inline pildora ' + tipo + '">'
                                + tipo + '</span>';
                        }
                        return html;
                    },

                },
                {
                    data: "Precio",
                    width: "8%",
                    //orderable: false,
                    render: function (data) {
                        var html = "<i class='fa fa-minus'></i>";
                        if (data)
                            html = '<div class=""> $ ' +  new Intl.NumberFormat(ObtenerLenguaje()).format(data)  + '</div>';
                        return html;
                    },

                },
                {
                    data: "Moneda",
                    width: "8%",
                    className: 'centrada', 
                    //orderable: false,
                    render: function (data) {
                        var html = "<i class='fa fa-minus'></i>";
                        if (data) {
                            var moneda = enumMonedas[data];
                            html = '<span class="fuente-bold ' + moneda + '">' + moneda+'</span>';
                        }
                        return html;
                    },

                },
                //{
                //    data: "",
                //    width: "5%",
                //    className: 'centrada',
                //    //orderable: false,
                //    render: function () {
                //        var html = '<div class="dropdown">'
                //            +'<div  class="dropbtn"> <i class="fa fa-ellipsis-h"></i></div>'
                //            +'<div class="dropdown-content">'
                //            +'<a class="dropdown-item" href="#"><i class="fa fa-pencil"></i>Editar</a>'
                //            //+ '<a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit Details</a>'
                //            //+ '<a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit Details</a>'
                //            +'</div>'
                //            +'</div>';
                //        return html;
                //    },

                //}
            ],
            language: {
                "zeroRecords": cfg.text.sinElementos,
                "oPaginate": {
                    "sFirst": "",
                    "sPrevious": " < ",
                    "sNext": " > ",
                    "sLast": ""
                },
                "search": "_INPUT_",
                searchPlaceholder: 'Buscar...'
            },
            //buttons: [{
            //    extend: 'colvis',
            //    text: '',
            //    titleAttr: 'Mostrar/Ocultar Columnas',
            //    collectionLayout: 'fixed one-column',
            //    className: 'pl-circle-datagrid iconCuadrado fa fa-columns'
            //}],
            //para poner el mensaje 
            fnInfoCallback: function (oSettings, iStart, iEnd, iMax, iTotal, sPre) {
                var info = cfg.jq.tabla.page.info();
                if (info && info.pages === 0) {
                   
                    ctx.find(".dataTables_paginate").hide();
                    ctx.find(".dataTables_info").hide();
                } else if (info && info.pages === 1) {

                    ctx.find(".dataTables_paginate").hide();
                    ctx.find(".dataTables_info").show();
                }
                else {
                    ctx.find(".dataTables_paginate").show();
                    ctx.find(".dataTables_info").show();
                }
                return "Productos " + iStart + " a " + iEnd + " de " + iTotal + " registros";
            },
        });
        //cfg.jq.tabla.buttons().container().appendTo('.pl-tools-datagrid');
      
        //Inicializar barra de busqueda
        //ctx.find("#tabla_filter").html(ctx.find("#plantillaFiltros").html());

       
        //$(table.table().container()).on('keyup', 'tfoot input', function () {

        //});

        //ctx.find("input[type=radio][name=toggle]").off().on('change',
        //    function() {
        //        cfg.jq.tabla.draw();
        //    });

        //$("#tabla_filter .filtro input").clearSearch();
        ctx.find("#tabla_filter input[type=search]").off().on('keyup change', function () {
            var val = $(this).val();
            if (val.length % 3 === 0) {
                cfg.jq.tabla.search(val).draw();
            }
        });
    }
    function calcularNumeroRegistros() {
        var px = LayoutManager.remtoPx(1);
        var rh = 62 * px;
        var h = ctx.find(cfg.dom.cntTabla).height();
        var ht = h - (150 * px);//titulo del contenedor + cabecera de la tabla +seccion de busqueda+pie de tabla
        var rows = ht / rh;
        return parseInt(rows);
    }

    function crearMenuContextual() {
        
        var opciones = [];
        
        var permiso = ctx.find("#permiteEdicionProductos").val();
        if (permiso == "true") {
            opciones.push({ id: "editar", clase: "fa fa-pencil", texto: "Editar" });
        }        
 
        if (opciones.length>0) { 
            var parametros = {
                idMenuContextual: "menu-contextual",
                opciones: opciones,
                onOpcionSeleccionada: opcionSeleccionada,
                mostrarMenu: function mostrarMenu(data) {
                    var mostrar = true;
                    return mostrar;
                }, //regresa true o false
                ocultarOpciones: function () {
                    var ocultar = [];
                    return ocultar;
                }, //lista de opciones a ocultar
                selPadLeft: "#wraperCotenidoWeb",
                selPadTop: "",
                selTrigger: "#tabla",
                triggerFiltro: "tr",
                obtenerDatos: function (el) {
                    var data = {};
                    try {
                        data = $(el).data();
                        var dataRow = cfg.jq.tabla.row(el).data();
                        data.row = dataRow;
                    } catch (e) {
                        error(e);
                    }
                    return data;
                }
            };
                ctx.MenuCtx(parametros);
        } else {
            ctx.MenuCtx(null);
        }
    }

    function opcionSeleccionada(idOpcion, data) {
        try {
            if (idOpcion === "editar") {
                PaginaWeb.Members.Productos.Edicion({ callbackRefrescar: refrescarLista }).abrir(data.row.IdProducto);
            }
            console.log(data);
        } catch (e) {
            error(e);
        }
    }
    //#endregion

    //#endregion

    //#region Menu Principal

    function abrirModalNueva() {
        His.Modulos.Finanzas.FINCompras.ComprasOrdenes.EdicionOrdenes({ callbackRefrescar: refrescarLista }).abrir(0);
    }

    function refrescarLista() {
        cfg.jq.tabla.ajax.reload();

    }

    //#endregion
    
    //#region Eventos
    function imprimirInventario() {
        try {            
            window.open(rutaBase + "Members/ReporteInventario");
        } catch (e) {
            error(e);
        }        
    }

    function imprimirListasPrecio() {        
        PaginaWeb.Members.Impresion.ListasPrecio().abrir();
    }
    //#endregion 

    //#region Modal Productos

    //#endregion

    //#region Public
    var subSeccion = {

        cargar: function () {
            iniCfg();
            var dcont = $('#ContenedorSecciones #contenido');
            dcont.html(PaginaWeb.Utils(dcont).htmlSpin);
            cfg.ajax.Accion(cfg.urls.carga, {},
                function (data) {
                    dcont.html(data);
                    subSeccion.iniciar();
                },
                "post",
                "html");
        },

        iniciar: function (onIniciar) {
            try {
              
                $(".fullWapper").addClass("height100p");

                //adminCotizaciones._agregarEventos();
                iniciaCarga();

                if (onIniciar && typeof onIniciar === "function") {
                    onIniciar(undefined);
                }
            } catch (e) {
                error(e);
            }
        },

        finalizar: function (onFinalizar) {
            try {
                $(".fullWapper").removeClass("height100p");
                //adminCotizaciones._quitarEventos();
                if (onFinalizar && typeof onFinalizar === "function") {
                    onFinalizar();
                }
            } catch (e) {
                adminCotizaciones._error(e);
            }
        },

        onCambioLayout: function (contenedorSize) {
            try {

            } catch (e) {
                error(e);
            }
        }
    }
    //#endregion Public

    return subSeccion;

};

PaginaWeb.Members.Productos.Edicion = function (pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {};

    var params = {
        callbackRefrescar: pars.callbackRefrescar,

        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Producto guardada",
            ErrorGuardar: "Ocurrio un problema al guardar el producto",
            Borrado: "Se borro el producto",
            ErrorBorrar: "No se pudo borrar el producto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el producto?",
            ConfirmarTitulo: "Borrar el producto",
            Seleccionar: "Seleccione un producto"
        },
        url: {
            abrir: urlBase + "/Members/_EdicionProductos",
            agregarArchivo: urlBase + "/Members/AgregarHojaProducto",
            eliminarArchivo: urlBase + "/Members/RemoverHojaProducto",
            Guardar: urlBase +"/Members/AgregarActualizarProductos",
            Borrar: urlBase + "/Members/_BorraTransaccion",
            Descargar: urlBase + "/Members/ObtenerPdf",
            //ListaProductos: LayoutManager.obtenerRootApp("/Finanzas/_ListaPreciosProductos")
        },
        modal: {
            idModal: "modalProductos",
            idDatos: "contDatos",
            selContPopUp: "#ContenedorPopUp1"
        }

    }

      
    //#region Mapeos
    var objeto = {
        IdProducto:"idProducto",
        EsInternacional:"internacional",
        Modelo:"modelo",
        Nombre:"nombre",
        Descripcion:"descripcion",
        Clasificacion:"clasificacion",
        Tipo:"tipo",
        Precio:"precio",
        Moneda:"moneda",
        NumeroParte:"noParte",
        DatosCrudos:"datosCrudos",
        DatosUsables:"datosUsables",
        Ruta:"ruta",
    }

    var catalogos = {
        Clasificaciones: ["clasificacion"],
        ProductoTipo: ["tipo"],
        ProductoMonedas: ["moneda"]
    }

    var catalogosDep = {
        FinanzasCuentasPorPagar: {
            idProveedor: ["idCxp"]
        },
        FinanzasGastos: {
            idAcreedor: ["idGastos"]
        }
    }


    //#endregion

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }
    function moverModalTop() {
        try {
            var ajsDia = ctx.parents(".ajs-dialog");
            var t = ($(".ajs-modal").height() - ctx.parents(".ajs-dialog").height()) / 2;
            ajsDia.animate({ top: t }, 800);
        } catch (err) {
            error(err);
        }
    }
    //#endregion

    //#region Modal

    //#region Inicializar

    function abrirModal(idElemento) {
        params.datos.idProducto = idElemento;
        LayoutManager.abrirPopUpAjax(params.modal.idModal, params.modal.selContPopUp, params.url.abrir,
            { idProducto: idElemento }, "", { minWidth: '600rem', minHeight: '480rem', Height: '600rem' }, inicializarModal,"post",cerrar,{});
    }
    function inicializarModal() {
        try {
            ctx = $("#" + params.modal.idModal);
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            //inicializar Controles
            inicializarControlesModal();
            //util.getCatalogosDependientes(catalogosDep);
            util.getCatalogos(catalogos);
            //cargar datos iniciales
            var datos = ctx.find("#" + params.modal.idDatos).data("elemento");
            if (datos) util.setDatos(objeto, datos);
            manejoUi();
        } catch (err) {
            error(err);
        }
    }

    function inicializarControlesModal() {
        try {
            //Inicializar controles
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });

            ctx.find("div#drop_zone").dropzone({
                url:  params.url.agregarArchivo,
                maxFiles: 1,
                clickable: true,
                addRemoveLinks: true,
                autoProcessQueue: true,
                maxFilesize: 500,
                maxThumbnailFilesize: 500,
                acceptedFiles: ".pdf",
                init: function () {
                    this.on("addedfile",
                        function (file) {                           
                            inicioCargaArchivo(file);
                            //archivo = file;
                        });
                    this.on("removedfile",
                        function (file) {                            
                            ctx.find(".dz-message").show();
                            util.Accion(params.url.eliminarArchivo,
                                {
                                    nombre: file.name
                                },
                                function (data) {
                                    //archivo = null;
                                    ctx.find("#ruta").val();
                                });
                        });
                    this.on("success",
                        function (file, response) {                           
                            finCargaAplicacion(file);
                        });

                    this.on("error",
                        function (file, response) {                           
                            finCargaAplicacion(file, { idAplicacion: -1 });
                        });

                    this.on('sending',
                        function (file, xhr, formData) {                           
                        });
                },

                //dictDefaultMessage: "Drop files here to upload",
                dictFallbackMessage: "El navegador no soporta soltar ya gregar archivos.",
                dictFallbackText: "Utilice el formulario de reserva a continuación para cargar sus archivos como en los días anteriores.",
                dictFileTooBig: "El archivo es muy grande ({{filesize}}MiB). Max tamaño de archivo: {{maxFilesize}}MiB.",
                dictInvalidFileType: "No puedes subir archivos de este tipo.",
                dictResponseError: "Servidor responde con el código {{statusCode}}.",
                dictCancelUpload: "Cancelar carga",
                dictCancelUploadConfirmation: "¿Estás seguro de que quieres cancelar esta carga?",
                dictRemoveFile: "Remover archivo",
                dictRemoveFileConfirmation: null,
                dictMaxFilesExceeded: "No puedes subir más archivos.",
            });

            $(document.body).bind("dragover", function (e) {
                e.preventDefault();
                return false;
            });

            $(document.body).bind("drop", function (e) {
                e.preventDefault();
                return false;
            });
            //moneda
            //ctx.find('#moneda').monedaFormatNumber({ currency: "$" });
            ctx.find("#reposicionWrap").idigitalesCheckboxes({ markType:1});
            //ctx.find$('.decimal').monedaFormatNumber({ currency: "" });

            ctx.find("#tipoTrx").off("change", onChangeTipoTrx).on("change", onChangeTipoTrx);
            //Eventos Botones
            ctx.find("#btnDescargaPdf").off("click").on("click", onClickDescargarPdf);
            ctx.find("#btnEliminarPdf").off("click").on("click", onClickEliminarPdf);
            ctx.find("#btnGuardarMod").off("click").on("click", onClickGuardarMod);
            ctx.find("#btnCancelarMod").off("click").on("click", onClickCancelarMod);


        }
        catch (err) {
            error(err);
        }
    }

    function inicioCargaArchivo(file) {       
        ctx.find(".dz-message").hide();
        if (file && file.name)
            ctx.find("#ruta").val(file.name);
    }

    function finCargaAplicacion() { }
    
    //#region Eventos controles
    function onChangeTipoTrx() {
        try {
            var val = $(this).val();
            manejoUi(val);
        } catch (err) {
            error(err);
        }
    }

    function onClickGuardarMod() {
        try {
            var cont = ctx.find("#" + params.modal.idDatos)[0];
            if (validador.validar(cont)) {
                var datos = util.getDatos(objeto);
                if (datos) guardar(datos);
            }
        } catch (err) {
            error(err);
        }
    }
    function onClickDescargarPdf() {
        try {
            var datos = util.getDatos(objeto);
            if (datos && datos.IdProducto && datos.Ruta)
                descargarPdf(datos.IdProducto);
        } catch (err) {
            error(err);
        }
    }
    function onClickEliminarPdf() {
        try {
            ctx.find("#ruta").val("");
            manejoUi();

        } catch (err) {
            error(err);
        }
    }

    

    function onClickCancelarMod() {
        try {
            alertify[params.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }
    //#endregion

    //#endregion

    //#region Ajax Agregar-Editar-Borrar

    function guardar(datos) {
        try {
            ajax.AccionModal(params.url.Guardar, { producto: datos }, function (data) {
                if (data.Resultado === 0) {
                    ctx.idigitalesNotificaciones("", params.msgs.Guardado, 'success');
                    ctx.find("#idCuenta").val(data.Id);
                    if (typeof params.callbackRefrescar === "function") {
                        params.callbackRefrescar();
                    }
                    alertify[params.modal.idModal]().close();

                } else {
                    console.log(data);
                    ctx.idigitalesNotificaciones('', params.msgs.ErrorGuardar, 'error');
                }
            },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

    function confirmarListaPreciosBorrar(idElemento) {
        if (idElemento) {
            alertify.confirm(params.msgs.ConfirmarTitulo, params.msgs.Confirmar,
                function () { borrarListaPrecios(idElemento); },
                function () { });
        }
    }

    function borrarListaPrecios(idLista) {
        try {
            ctx = ctx || $("#" + params.modal.idModal);

            ajax.Accion(params.url.Borrar, { idLista: idLista },
                function (data) {
                    if (data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("", params.msgs.ProductoBorrado, 'success');
                        if (typeof params.callbackRefrescar === "function") {
                            params.callbackRefrescar();
                        }
                    } else {
                        console.log(data.Mensaje);
                        $("#cntListaPrecios").idigitalesNotificaciones('', params.msgs.ErrorBorrar, 'error');
                    }
                },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

    function descargarPdf(idProducto) {
        try {
            var myWindow = window.open(params.url.Descargar +"?idProducto=" + idProducto, "", "width=200,height=100");

        } catch (err) {
            error(err);
        }
    }
    //#endregion


    function manejoUi()
    {
        ctx.find("#rowAddFile").hide();
        ctx.find(".btnPDF").hide();
        var ruta = ctx.find("#ruta").val();
        if (ruta) {
            ctx.find("#rowAddFile").hide();
            ctx.find(".btnPDF").show();
        } else {
            ctx.find("#rowAddFile").show();
            ctx.find(".btnPDF").hide();
        }
        //moverModalTop();
        
    }

    //#region Cerrar
    function cerrar() {
        try {
           // ctx.find("#btnGuardar").off("click", onClickGuardar);
           // ctx.find("#btnCancelar").off("click", onClickCancelar);
        } catch (e) {
            error(e);
        }
    }
    //#endregion
    //#endregion


    //#region "Metodos Publicos"
    var forma = {
        abrir: function (idElemento) {
            abrirModal(idElemento);
        },
        borrar: function (idElemento) {
            confirmarListaPreciosBorrar(idElemento);
        }

    }
    return forma;
    //#endregion 

}

PaginaWeb.Members.Productos.Listas = function (pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {};

    var params = {
        callbackRefrescar: pars.callbackRefrescar,

        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Lista guardada",
            ErrorGuardar: "Ocurrio un problema al guardar el producto",
            Borrado: "Se borro el producto",
            ErrorBorrar: "No se pudo borrar el producto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el producto?",
            ConfirmarTitulo: "Borrar el producto",
            Seleccionar: "Seleccione un producto"
        },
        url: {
            abrir: urlBase + "/Members/EdicionListasPecios",
            Guardar: urlBase + "/Members/AgregarActualizarListasPecios",
            Borrar: urlBase + "/Members/_BorraTransaccion",
            ListaProductos: urlBase + "/Members/ListaPreciosProductos",
            ImprimirListaPrecios: urlBase + "/Members/ListaPrecios"
        },
        modal: {
            idModal: "modalLista",
            idDatos: "contDatos",
            selContPopUp: "#ContenedorPopUp1"
        }

    }


    //#region Mapeos
    var objeto = {
        IdLista: "idLista",
        Codigo: "codigo",
        Nombre: "nombre",
        Descripcion: "descripcion",
        IdListaReferencia: "idListaReferencia",
        EsBase: "esBase",
        Porcentaje: "porcentaje"
    }

    var catalogos = {
        ListasPrecios: ["selLista"]
    }
    
    //#endregion

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }

    //#endregion

    //#region Modal

    //#region Inicializar

    function abrirModal(idElemento) {
        params.datos.idProducto = idElemento;
        LayoutManager.abrirPopUpAjax(params.modal.idModal, params.modal.selContPopUp, params.url.abrir,
            { idProducto: idElemento }, "", { minWidth: '500rem', minHeight: '400rem' }, inicializarModal, "post", cerrar, {});
    }
    function inicializarModal() {
        try {
            ctx = $("#" + params.modal.idModal);
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            //inicializar Controles
            inicializarControlesModal();
            //util.getCatalogosDependientes(catalogosDep);
            util.getCatalogos(catalogos);
            //cargar datos iniciales
            //var datos = ctx.find("#" + params.modal.idDatos).data("elemento");
            //if (datos) util.setDatos(objeto, datos);
            manejoUi();
        } catch (err) {
            error(err);
        }
    }

    function inicializarControlesModal() {
        try {
            //Inicializar controles
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });

            //moneda
            //ctx.find('#moneda').monedaFormatNumber({ currency: "$" });
            ctx.find("#reposicionWrap").idigitalesCheckboxes({ markType: 1 });
            //ctx.find$('.decimal').monedaFormatNumber({ currency: "" });

            ctx.find("#selLista").off("change", onChangeLista).on("change", onChangeLista);
            //Eventos Botones
            ctx.find("#btnListaEditar").off("click").on("click", onClickEditarLista);
            ctx.find("#btnListaNueva").off("click").on("click", onClickNuevaLista);
            ctx.find("#btnGuardarMod").off("click").on("click", onClickGuardarMod);
            ctx.find("#btnCancelarMod").off("click").on("click", onClickCancelarMod);

            //Eventos de Botones panel
            ctx.find("#btnBorrar").off("click").on("click", onClickBorrarProducto);
            ctx.find("#btnNuevo").off("click").on("click", onClickNuevoProducto);
            ctx.find("#btnEditar").off("click").on("click", onClickEditarProducto);
        }
        catch (err) {
            error(err);
        }
    }

   
    //#region Eventos controles
    function onChangeLista() {
        try {
            var val = $(this).val();
            if (val) {
                ctx.find("#btnListaEditar").show(); 
            } else {
                ctx.find("#btnListaEditar").hide(); 
            }
        } catch (err) {
            error(err);
        }
    }
    function onClickEditarLista() {
        try {
            var select = ctx.find("#selLista");
            var valor = select.val();
            var opcion = select.find('option[value="' + valor + '"]');
            var datos = opcion.data("Elemento") || {};
            util.setDatos(objeto, datos);
            manejoUi();
            cargarProductos();
        } catch (err) {
            error(err);
        }
    }
    function onClickNuevaLista() {
        try {
            var select = ctx.find("#selLista");
            select.val("").change();
            var datos = {IdLista: 0,Codigo: "",Nombre: "",Descripcion: "",IdListaReferencia: "",EsBase: "",Porcentaje: ""};
            util.setDatos(objeto, datos);
            manejoUi();
            cargarProductos();
        } catch (err) {
            error(err);
        }
    }
    function onClickImprimirLista() {
        try {
            debugger;
            var select = ctx.find("#selLista");
            var valor = select.val();
             window.open(params.url.ImprimirListaPrecios + "?idLista=" + valor);                        
        } catch (err) {
            error(err);
        }
    }
    

    function onClickGuardarMod() {
        try {
            var cont = ctx.find("#" + params.modal.idDatos)[0];
            if (validador.validar(cont)) {
                var datos = util.getDatos(objeto);
                if (datos) guardar(datos);
            }
        } catch (err) {
            error(err);
        }
    }
   
    function onClickCancelarMod() {
        try {
            alertify[params.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }

    //#region Eventos Panel Productos Borrar-Nuevo-Editar
    function onClickBorrarProducto() {
        var plan = ctx.find("#listaProductos input[type='radio']:checked").parent();
        borrarElemento(plan.data("id"));
    }
    function onClickEditarProducto() {
        var idLista = ctx.find("#idLista").val();
        var plan = ctx.find("#listaProductos input[type='radio']:checked").parent();
        if (plan.length > 0) {
            PaginaWeb.Members.Productos.Listas.EdicionListaPreciosProducto({ callbackRefrescar: cargarProductos }).abrir(idLista, plan.data("id"));
        } else {
            ctx.find("#listaProductos").idigitalesNotificaciones("", params.msgs.Seleccionar, 'alert');
        }
    }
    function onClickNuevoProducto() {
        var idLista = ctx.find("#idLista").val();
        PaginaWeb.Members.Productos.Listas.EdicionListaPreciosProducto({ callbackRefrescar: cargarProductos }).abrir(idLista, 0);
    }
    function borrarElemento(idElemento) {
        if (idElemento > 0) {
            var idLista = ctx.find("#idLista").val();
            PaginaWeb.Members.Productos.Listas.EdicionListaPreciosProducto({ callbackRefrescar: cargarProductos }).borrar(idLista, idElemento);
        } else {
            ctx.find("#listaProductos").idigitalesNotificaciones("", params.msgs.Seleccionar, 'alert');
        }
    }
    //#endregion

    //#endregion

    //#endregion

    //#region Ajax Agregar-Editar-Borrar

    function guardar(datos) {
        try {
            ajax.AccionModal(params.url.Guardar, { lista: datos }, function (data) {
                if (data.Resultado === 0) {
                    ctx.idigitalesNotificaciones("", params.msgs.Guardado, 'success');
                    ctx.find("#idLista").val(data.Id);
                    if (typeof params.callbackRefrescar === "function") {
                        params.callbackRefrescar();
                    }
                    if (CATALOGOS && CATALOGOS.ListasPrecios) {
                        delete CATALOGOS.ListasPrecios;
                        util.getCatalogos(catalogos);
                    }
                    //alertify[params.modal.idModal]().close();
                    manejoUi();
                    cargarProductos();

                } else {
                    console.log(data);
                    ctx.idigitalesNotificaciones('', params.msgs.ErrorGuardar, 'error');
                }
            },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

    function confirmarListaPreciosBorrar(idElemento) {
        if (idElemento) {
            alertify.confirm(params.msgs.ConfirmarTitulo, params.msgs.Confirmar,
                function () { borrarListaPrecios(idElemento); },
                function () { });
        }
    }

    function borrarListaPrecios(idLista) {
        try {
            ctx = ctx || $("#" + params.modal.idModal);

            ajax.Accion(params.url.Borrar, { idLista: idLista },
                function (data) {
                    if (data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("", params.msgs.ProductoBorrado, 'success');
                        if (typeof params.callbackRefrescar === "function") {
                            params.callbackRefrescar();
                        }
                    } else {
                        console.log(data.Mensaje);
                        $("#cntListaPrecios").idigitalesNotificaciones('', params.msgs.ErrorBorrar, 'error');
                    }
                },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }

    function cargarProductos() {
        try {
            var idLista = util.getVal("idLista");
            if (idLista) {
                ctx.find("#listaProductos").html(LayoutManager.htmlSpin);
                ajax.Accion(params.url.ListaProductos,
                    { idLista: idLista },
                    function (data) {
                        var lista = ctx.find("#listaProductos");
                        lista.html(data);
                        refrescarListaProductos();
                    },
                    "get",
                    "html");
            }

        } catch (err) {
            error(err);
        }
    }
    var listaProductos;
    function refrescarListaProductos() {
        try {
            //Busqueda
            listaProductos = $("<div/>");
            var lproductos = ctx.find('#listaProductos').find(".itemProducto");
            if (lproductos.length > 0) {
                lproductos.each(function (i, e) {
                    listaProductos.append($(e).clone());
                });
            }
            ////Inicializar scroll
            //var scroll = ctx.find("#antiscrollListaProductos");
            //if (!scroll.data("antiscroll")) {
            //    scroll.antiscroll({ x: false, initialDisplay: false });
            //} else {
            //    scroll.data("antiscroll").refresh();
            //}

            //Boton de busqueda
            //ctx.find("#filtro").clearSearch({ callback: buscarElemento });
            ctx.find("#filtro").on("keyup", buscarElemento);

            //Inicializar elementos del panel productos
            ctx.find(".itemProducto").off("click").on("click", onClickProducto);
            ctx.find("#listaProductos .eliminarPro").off("click").on("click", onClickImgEliminarProducto);
            ctx.find(".itemProducto").off("dblclick").on("dblclick", onDobleClickProducto);
        } catch (err) {
            error(err);
        }
    }
    //#region Eventos Lista Productos
    function onClickImgEliminarProducto(event) {
        event.stopPropagation();
        var plan = $(this).parents(".itemProducto");
        borrarElemento(plan.data("id"));
    }

    function onClickProducto() {
        $(this).find("input.lpSeleccionProducto").prop("checked", true);
    }

    function onDobleClickProducto() {
        onClickEditarProducto();
    }

    //#endregion
    //#region Filtrado
    function buscarElemento(evt) {
        var listaElementos = $("#listaProductos");
        try {
            var buscar = [];
            if (evt) {
                buscar = $(evt.target).val().trim().toLowerCase().split(' ');
            }
            //listaElementos = $("#listaOrdenesDia");

            if (buscar.length > 0 && buscar[0].length > 1) {

                var resultado = [];
                if (listaProductos && listaProductos.children() != null) {
                    for (var i = 0; i < listaProductos.children().length; i++) {
                        var des = $(listaProductos.children()[i]).find("#divDescripcion").text();
                        //var nombre = $(listaProductos.children()[i]).find(".nombreCompleto").text();
                        for (var j = 0; j < buscar.length; j++) {

                            if (des.toLowerCase().indexOf(buscar[j].toLowerCase()) >= 0) {
                                resultado.push(listaProductos.children()[i]);
                                break;
                            }
                        }
                    }
                }

                listaElementos.children().hide();
                $(resultado).each(function (index, elem) {
                    var id = $(elem).data("id");
                    listaElementos.find("[data-id='" + id + "']").show();
                });
            } else {
                listaElementos.children().show();
            }
        } catch (e) {
            listaElementos.children().show();
            console.log(e);
        }
    }
    //#endregion
    //#endregion


    function manejoUi() {
        try {

            var lprod=ctx.find("#contProductos");
            //var datos = util.getDatos(objeto);
            if (parseInt(ctx.find("#idLista").val()) > 0) {
                //ctx.find("#botones").hide();
                lprod.show();
                //moverModalTop();
                
            } else {
                //ctx.find("#botones").show();
               lprod.hide();
               //moverModalTop();
            }
            //if (datos && datos.Base) {
            //    ctx.find("#base").off('change');
            //}
        } catch (err) {
            error(err);
        }
    }
    function moverModalTop(nTop) {
        try {
            var ajsDia = ctx.parents(".ajs-dialog");
            var t = ($(".ajs-modal").height() - ctx.parents(".ajs-dialog").height()) / 2;
            ajsDia.animate({ top: t }, 800);
        } catch (err) {
            error(err);
        }
    }

    //#region Cerrar
    function cerrar() {
        try {
            // ctx.find("#btnGuardar").off("click", onClickGuardar);
            // ctx.find("#btnCancelar").off("click", onClickCancelar);
        } catch (e) {
            error(e);
        }
    }
    //#endregion
    //#endregion


    //#region "Metodos Publicos"
    var forma = {
        abrir: function (idElemento) {
            abrirModal(idElemento);
        },
        borrar: function (idElemento) {
            confirmarListaPreciosBorrar(idElemento);
        }

    }
    return forma;
    //#endregion 

}

PaginaWeb.Members.Productos.Listas.EdicionListaPreciosProducto = function (_params) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    _params = _params || {};
    var idLista = 0;

    var params = {
        callbackRefrescar: _params.callbackRefrescar,

        datos: {},
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "producto guardado",
            ErrorGuardar: "Ocurrio un problema al guardar el producto",
            Borrado: "Se borro el producto",
            ErrorBorrar: "No se pudo borrar el producto",
            Confirmar: "¿Seguro que desea borrar el producto?",
            ConfirmarTitulo: "Borrar el producto"
        },
        url: {
            abrir: urlBase + "/Members/EdicionListaPreciosProductos",
            Guardar: urlBase + "/Members/AgregarActualizarListaPreciosProductos",
            Borrar: urlBase + "/Members/BorrarListaPreciosProductos",
            buscar: urlBase + "/Catalogos/BuscarEntidad"
        },
        modal: {
            idModal: "modalProducto",
            idDatos: "datosProducto",
            selContPopUp: "#ContenedorPopUp2"
        }

    }

    //#region Mapeos
    var objetoProducto = {
        IdLista: "idLista",
        IdProducto: "selProductos",
        Porcentaje: "porcentaje",
        PrecioFinal: "precio",
    }


    //#endregion

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }
    function soloNumeros(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode === 46) return true;
        if (charCode !== 32 && charCode < 48 || charCode > 57) {
            return false;
        }
        return true;
    }
    //#endregion

    //#region Modal

    //#region Inicializar

    function abrirModal(idListap, idProducto) {
        idLista = idListap;
        LayoutManager.abrirPopUpAjax(params.modal.idModal, params.modal.selContPopUp, params.url.abrir,
            { idProducto: idProducto, idLista: idLista }, "", { minWidth: '400rem', height: '250rem' }, inicializarModal);
    }
    function inicializarModal() {
        try {
            ctx = $("#" + params.modal.idModal);
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            //inicializar Controles
            inicializarControlesModal();
            //cargar datos iniciales
            var datos = ctx.find("#" + params.modal.idDatos).data("elemento");
            if (datos) {
                util.setDatos(objetoProducto, datos);
                util.setVal("idLista", idLista);
                //if (datos.Descuento == 0)
                //    util.setVal("descuento", "0");
                //if (datos.PrecioFinal == 0)
                //    util.setVal("precioFinal", "0.00");
            }
        } catch (err) {
            error(err);
        }
    }

    function inicializarControlesModal() {
        try {
            //Inicializar controles
            var selProductos = ctx.find("#selProductos");
            var placeholderProductos = selProductos.data("placeholder");
            selProductos.data("catalogo", "Productos"); //llenar descripcion
            selProductos.select2({
                ajax: {
                    url: params.url.buscar,
                    dataType: 'json',
                    delay: 200,
                    data: function (par) {
                        return {
                            catalogo: "Productos",
                            filtro: par.term, // search term
                            page: par.page
                        };
                    },
                    processResults: function (data, page) {
                        return { results: data.Catalogo };
                    },
                    cache: true
                },
                width: '100%',
                allowClear: true,
                placeholder: placeholderProductos,
                escapeMarkup: function (markup) { return markup; },
                minimumInputLength: 3,
                templateResult: util.templateResult,
                templateSelection: util.templateSelection
            });

            //ctx.find("#idLista").val(idLista);

            //ctx.find("#descuento").off("keypress").on("keypress", soloNumeros);
            //ctx.find("#precioFinal").off("keypress").on("keypress", soloNumeros);

            //ctx.find(".numerico-precios").idigitalesNumerico({
            //    minValue: 0,
            //    maxValue: 100000,
            //    start: 0, step: .01
            //});
            //Eventos Botones
            ctx.find("#btnGuardarMod").off("click").on("click", onClickGuardarMod);
            ctx.find("#btnCancelarMod").off("click").on("click", onClickCancelarMod);
        } catch (err) {
            error(err);
        }
    }

    function onClickGuardarMod() {
        try {
            var cont = ctx.find("#" + params.modal.idDatos)[0];
            if (validador.validar(cont)) {
                var datos = util.getDatos(objetoProducto);
                if (datos) guardar(datos);
            }
        } catch (err) {
            error(err);
        }
    }

    function onClickCancelarMod() {
        try {
            alertify[params.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }
    //#endregion

    //#region Agregar-Editar-Borrar

    function guardar(datos) {
        try {
            ajax.Accion(params.url.Guardar, { producto: datos }, function (data) {
                if (data.Resultado === 0) {
                    ctx.idigitalesNotificaciones("", params.msgs.Guardado, 'success');
                    if (typeof params.callbackRefrescar === "function") {
                        params.callbackRefrescar();
                    }
                    alertify[params.modal.idModal]().close();
                } else {
                    ctx.idigitalesNotificaciones('', params.msgs.ErrorGuardar, 'error');
                }
            },
                "post", "json");
        } catch (err) {
            error(err);
        }
    }

    function confirmarBorrar(idLista, idElemento) {
        if (idLista && idElemento) {
            alertify.confirm(params.msgs.ConfirmarTitulo, params.msgs.Confirmar,
                function () { borrar(idLista, idElemento); },
                function () { });
        }
    }

    function borrar(idLista, idElemento) {
        try {
            ctx = ctx || $("#" + params.modal.idModal);

            ajax.Accion(params.url.Borrar, { idLista: idLista, idProducto: idElemento },
                function (data) {
                    if (data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("", params.msgs.Borrado, 'success');
                        if (typeof params.callbackRefrescar === "function") {
                            params.callbackRefrescar();
                        }
                    } else {
                        ctx.idigitalesNotificaciones('', params.msgs.ErrorBorrar, 'error');
                    }
                },
                "post", "json");

        } catch (err) {
            error(err);
        }
    }
    //#endregion

    //#endregion


    //#region "Metodos Publicos"
    var forma = {

        abrir: function (idLista, idElemento) {
            abrirModal(idLista, idElemento);
        },
        borrar: function (idLista, idElemento) {
            confirmarBorrar(idLista, idElemento);
        }

    }
    return forma;
    //#endregion 

}

