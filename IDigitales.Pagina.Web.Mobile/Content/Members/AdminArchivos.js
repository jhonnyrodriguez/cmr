﻿var PaginaWeb = PaginaWeb || {}; 
PaginaWeb.Members = PaginaWeb.Members || {};
PaginaWeb.Members.Archivos = function (opt) {

 

    //#region "Variables "
    var _params = {
        baseUrl: ""
    }
    $.extend(_params, opt);
    
    var tabsPlugin = undefined;
    var _cuadricula = {};
    var util = null;
    //#endregion "Variables "

    //#region "Metodos "

    function agregarEventos() { 
        $(".busqueda-select-cat").select2({
            width: '100%',
            minimumResultsForSearch: Infinity
        });
        $(".busqueda-select-sub").select2({
            width: '100%',
            minimumResultsForSearch: Infinity
        });
        
        obtenerCatalogo("CatalogoCategorias", "busqueda-select-cat");
        obtenerCatalogo("CatalogoSubCategorias", "busqueda-select-sub");

        $('.busqueda-input').keyup(function (e) {
            var val = $(this).val();
            if (val == "") {
                var id = $(this).data("id");
                $("#Tab" + id + " .itemDocumento").show();
                $("#Tab" + id + " .subpanel").show();
            }
        });

        $('.contenedor-tab.nuevaTab').each(function (index, item) {
            var contenedor = $(item);
            var id = contenedor.data("id");
            contenedor.find("#btn-eliminar").bind('click', {idTab: id}, adminImagenes._clickEliminar);
            contenedor.find("#btn-agregar").bind('click', { idTab: id }, adminImagenes._clickAgregar);
            var cuadricula = contenedor.find("#cuerpo-tab-imagenes"); 
            _cuadricula[id] = PaginaWeb.Comunes.IDigitalesCuadricula(cuadricula.get(0), {});
            //_cuadricula[id].onEliminar = function (elemeto) {
            //    //borrar todo lo necesario
            //}
            contenedor.removeClass("nuevaTab");
        });
       
        $(".busqueda-btn").off("click").on("click", onClickBusqueda);
        $(".itemDocumento").off("click").on("click", onClickElemento);
        $(".item-descarga").off("click").on("click", onDownloadLinkClick);
        $(".idigitales-panel-encabezado").off("click").on("click", panelesMoviles);
        $(".idigitales-subpanel-encabezado").off("click").on("click", subPanelesMoviles);
        MinimizarsubPanelesMoviles();
    }
    function quitarEventos() {
        $('.contenedor-tab').each(function (item, index) {
            var contenedor = $(item);
            var id = contenedor.data("id");
            contenedor.find("#btn-eliminar").unbind();
            contenedor.find("#btn-agregar").unbind();
            //_cuadricula[id] && _cuadricula[id].finalizar();
        });
        

        for (var key in _cuadricula) {
            _cuadricula[key].finalizar();
            _cuadricula[key] = undefined;
        }
        _cuadricula = {};
    }
    function onClickElemento() {
        $(this).toggleClass('elementoSeleccionado');
    }
    function onDownloadLinkClick(e) {
        try {
            var idDocumento = $(this).data("iddoc");
            window.location = _params.baseUrl + "/Members/DescargarDocumento" +
                "?idDocumento=" +
                idDocumento; 
        } catch (e) {
            error(e);
        } 
    }
    function AgregarTabs(idTab) {
        try {     
            var contTabs = $(".cd-tabs");
            var inicio = contTabs.data("inicio");
            var fin = contTabs.data("fin");
            //Agregar al Inicio
            if (idTab < inicio) {
                var cont = inicio-1;
                while (cont >= idTab){
                    var nuevoEncabezado = "<li><a data-content='tabImagen_" + cont + "' data-id='" + cont + "' href='#0'>" + cont + "</a></li>";
                    var nuevoContenido =
                        "<li data-content='tabImagen_" + cont + "' data-id='" + cont + "' id='tabImagen_" + cont + "'>" +                            
                            "<div class='contenedor-tab nuevaTab' id='Tab" + cont + "' data-id='" + cont + "'>" +
                                "<div class='menu-tab'>" +
                                    "<div id='btn-eliminar' class='btn-menu-tab'><i class='fa fa-minus' aria-hidden='true'></i></div>" +
                                    "<div id='btn-agregar' class='btn-menu-tab'><i class='fa fa-plus' aria-hidden='true'></i></div>" +
                                    //"<div class='ayudaCuadricula'>" +
                                    //    "<object data='~/content/images/Members/imagenes.svg' style='height:100%' type='image/svg+xml'></object>" +
                                    //"</div>" +
                                "</div >" +
                                "<div class='itemBusqueda'>" +
                                    "<div class='fa fa-search busqueda-icon'></div>" +
                                    "<input type='text' id='txtBusqueda" + cont + "' class='busqueda-input' data-id='" + cont + "' value='' />" +
                                    "<div class='busqueda-select'><select id='cmbBusquedaCat" + cont + "' class='busqueda-select-cat'></select></div>" +
                                    "<div class='busqueda-select'><select id='cmbBusquedaSub" + cont + "' class='busqueda-select-sub'></select></div>" +
                                    "<div class='busqueda-btn' data-id='" + cont + "'><div>" + txtBuscar +"</div></div>" +
                                "</div>" +
                                "<div id='cuerpo-tab-imagenes' class='cuerpo-tab'></div>" +
                                "<input id='tmp_filechooser' accept='.jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*' type='file' style='visibility: hidden' />" +
                            "</div >" +
                        "</li > ";
                    var contEncabezados = $(".cd-tabs-navigation");
                    contEncabezados.prepend(nuevoEncabezado);
                    var contContenidos = $(".cd-tabs-content");
                    contContenidos.prepend(nuevoContenido);
                    cont--;
                }
                contTabs.data("inicio",cont);
            }
            //Agregar al final
            if (idTab > fin) {
                var cont = fin+1;
                while (cont <= idTab) {
                    var nuevoEncabezado = "<li><a data-content='tabImagen_" + cont + "' data-id='" + cont + "' href='#0'>" + cont + "</a></li>";
                    var nuevoContenido =
                        "<li data-content='tabImagen_" + cont + "' data-id='" + cont + "' id='tabImagen_" + cont + "'>" +                            
                            "<div class='contenedor-tab nuevaTab' id='Tab" + cont + "' data-id='" + cont + "'>" +
                                "<div class='menu-tab'>" +
                                    "<div id='btn-eliminar' class='btn-menu-tab'><i class='fa fa-minus' aria-hidden='true'></i></div>" +
                                    "<div id='btn-agregar' class='btn-menu-tab'><i class='fa fa-plus' aria-hidden='true'></i></div>" +
                                    //"<div class='ayudaCuadricula'>" +
                                    //    "<object data='~/content/images/Members/imagenes.svg' style='height:100%' type='image/svg+xml'></object>" +
                                    //"</div>" +
                                "</div >" +
                                "<div class='itemBusqueda'>" +
                                    "<div class='fa fa-search busqueda-icon'></div>" +
                                    "<input type='text' id='txtBusqueda" + cont + "' class='busqueda-input' data-id='" + cont + "' value='' />" +
                                    "<div class='busqueda-select'><select id='cmbBusquedaCat" + cont + "' class='busqueda-select-cat'></select></div>" +
                                    "<div class='busqueda-select'><select id='cmbBusquedaSub" + cont + "' class='busqueda-select-sub'></select></div>" +
                                    "<div class='busqueda-btn' data-id='" + cont + "'><div>" + txtBuscar+"</div></div>" +
                                "</div>" +
                                "<div id='cuerpo-tab-imagenes' class='cuerpo-tab'></div>" +
                                "<input id='tmp_filechooser' accept='.jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*' type='file' style='visibility: hidden' />" +
                            "</div >" +
                        "</li > ";
                    var contEncabezados = $(".cd-tabs-navigation");
                    contEncabezados.append(nuevoEncabezado);
                    var contContenidos = $(".cd-tabs-content");
                    contContenidos.append(nuevoContenido);
                    cont++;
                }
                contTabs.data("fin", idTab);
            }    
            tabsPlugin._inicializar();
            agregarEventos();
        } catch (e) {
            console.log(e);
        }
    }
    function refrescarTabs(idTab) {
        try {
            var Tab = $("#Tab" + idTab);
            if (Tab.length <= 0) {
                AgregarTabs(idTab);
            }
                        
            util.Accion(_params.baseUrl + "/Members/ContenidoArchivos",
                {
                    idTab: idTab
                },
                function (data) {
                    if (data) {
                        Tab = $("#Tab" + idTab + " #cuerpo-tab-imagenes");
                        Tab.html("").html(data);
                        agregarEventos();
                        $("#tabImagen_" + idTab + " .ayudaCuadricula").hide();
                    } 
                },"Post","html");
        } catch (e) {
            console.log(e);
        }
    }
    function panelesMoviles() {
        var ibox = $(this).closest('div.panel-default');
        var content = ibox.find('div.panel-body');
        content.slideToggle(200);      
        $(this).toggleClass('panel-head-min').toggleClass('panel-head-max'); 
        content.toggleClass('panel-body-min').toggleClass('panel-body-max'); 
    }    
    function subPanelesMoviles() {
        var ibox = $(this).closest('div.panel-default');
        var content = ibox.find('div.panel-body');
        content.slideToggle(200);
        $(this).toggleClass('subpanel-head-min').toggleClass('subpanel-head-max'); 
        content.toggleClass('subpanel-body-min').toggleClass('subpanel-body-max'); 
    } 
    function MinimizarsubPanelesMoviles() {
        var ibox = $(".idigitales-subpanel-encabezado").closest('div.panel-default');
        var content = ibox.find('div.panel-body');
        content.slideToggle(200);
        $(".idigitales-subpanel-encabezado").toggleClass('subpanel-head-min').toggleClass('subpanel-head-max');
        content.toggleClass('subpanel-body-min').toggleClass('subpanel-body-max');
    }
    function onClickBusqueda() {
        var id = $(this).data('id');
        var cat = $("#cmbBusquedaCat" + id).val();
        var sub = $("#cmbBusquedaSub" + id).val();
        var texto = $("#txtBusqueda" + id).val();
        if (texto != null && texto != "") {
            //cerrar todos los paneles y subpaneles
            $("#Tab" + id + " .panel-head-max").click();           
            $("#Tab" + id + " .subpanel-head-max").click();
            //Abrir panel seleccionado
            $("#Tab" + id + " .panelDatos").each(function (i, e) {
                var p = $(e);
                var idC = p.data("id");                
                if (idC == cat || cat == "-1") {
                    p.find(".idigitales-panel-encabezado").click();
                    //Abrir subpanel seleccionado
                    p.find(".idigitales-panel-contenido .panelDatos").each(function (is, es) {
                        var s = $(es);
                        var idS = s.data("id");
                        if (idS == sub || sub == "-1") {
                            var contEle = 0;
                            s.find(".idigitales-subpanel-contenido .itemDocumento").each(function (it, et) {
                                var ele = $(et);
                                var nom = ele.find(".item-titulo .item-nombre").html();
                                if (nom.toLowerCase().indexOf(texto.toLowerCase()) >= 0) {
                                    ele.show();
                                    contEle++;
                                }
                                else
                                    ele.hide();
                            });
                            if (contEle > 0) {
                                s.find(".idigitales-subpanel-encabezado").click();
                            } else {
                                s.hide();
                            }
                        }
                    });
                }
            });
            
        } else {
            $("#Tab" + id + " .itemDocumento").show();
            $("#Tab" + id + " .subpanel").show();
            //$.idigitalesNotificaciones("Busqueda", "Debes insertar un texto a buscar", "error");
        } 
    }
    //#endregion "Metodos "

    //#region catalogo
    function obtenerCatalogo(catalogo, idSelect) {
        util.Accion(_params.baseUrl + "/Members/ObtenerCatalogo",
        {
            catalogo: catalogo, idFiltro: ''
        },
        function (data) {
            var select = $("." + idSelect);
            select.empty();
            if (data != null) {
                select.append("<option value='-1'> TODOS </option>");
                $.each(data, function (index, value) {
                    select.append("<option value='" + value.Id + "'>" + value.Valor + "</option>");
                });
                select.val("-1").trigger("change");
            }                
        });        
    }
    //#endregion

    //#region "Constructores "
    var adminImagenes = {
        funcionesPublicas: {
             
        },
        iniciar: function (onIniciar) {
             
        try {
                _callBackIniciar = onIniciar;
                $(".fullWapper").addClass("height100p");
                tabsPlugin = LayoutManager.CrearTabs();
                tabsPlugin._inicializar();
                util = PaginaWeb.Utils();
                agregarEventos();                  

                if (_callBackIniciar && typeof _callBackIniciar === "function") {
                    _callBackIniciar(undefined);
                }
            } catch (e) {
                adminImagenes._error(e);
            }
        },
        finalizar: function (onFinalizar) {
            try {
                $(".fullWapper").removeClass("height100p");
                _callBackFinalizar = onFinalizar;

                 quitarEventos();                

                tabsPlugin._destroy();

                if (_callBackFinalizar && typeof _callBackFinalizar === "function") {
                    _callBackFinalizar();
                }
            } catch (e) {
                adminImagenes._error(e);
            }
        },

        onCambioLayout: function (contenedorSize) {

        },
        

        _clickEliminar: function (event) {
            var tabSeleccionada = $(".cd-tabs-navigation .selected");
            var idTab = tabSeleccionada.data("id");
            var contenidoTab = $("#Tab" + idTab);
            var elementosAEliminar = contenidoTab.find(".itemDocumento.elementoSeleccionado");
            if (elementosAEliminar.length > 0) {
                var idsDocumentos = [];
                elementosAEliminar.each(function () {
                    var ele = $(this);
                    var idD = ele.data("id");
                    idsDocumentos.push(idD);
                });
                    
                util.Accion(_params.baseUrl + "/Members/EliminarDocumentosBD",
                    {
                        documentos: idsDocumentos
                    },
                    function (data) {
                        if (data && data == 0) {
                            elementosAEliminar.remove();
                        }
                    }, "Post", "html");
            } else {
                $.idigitalesNotificaciones("Eliminar Imagen", "Debes seleccionar al menos un documento, en el año seleccionado", "error");
            }            
        },

        _clickAgregar: function (event) {
            var idTab = event.data.idTab;
            PaginaWeb.Members.Comunes.RegistrarDocumentos(_params).abrir({ callBackGuardar: adminImagenes.agregarCuadro});           
        },
        agregarCuadro: function (doc) {
            doc.forEach(function (d) {
                refrescarTabs(d);
            });       

            //$("#tabImagen_" + idTab + " .ayudaCuadricula").hide();      
        },

        _readImage: function(img, opt) {                      
            var $chooser = $('#tmp_filechooser'),
                chooser = $chooser[0];

            $chooser.val("");

            chooser.onchange = function (ev) {
                if (ev.target.files.length <= 0)
                    return;

                var file = ev.target.files[0]; // FileList object                
                var reader = new FileReader();

                reader.onload = function() {                   
                    img.onload = function () {
                        if (opt.success instanceof Function)
                            opt.success(file.name, img);                       
                    }

                    img.src = reader.result;                  
                };

                reader.onprogress = opt.progress;
                reader.onerror = opt.error;
                reader.readAsDataURL(file);

            }

            $chooser.click();
        },
      
        _error: function (e) {
            console.log(e);
        }
    };
    //#endregion "Constructores "


    return adminImagenes;

};

