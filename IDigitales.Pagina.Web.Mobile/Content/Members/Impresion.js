﻿var PaginaWeb = PaginaWeb || {};
PaginaWeb.Members = PaginaWeb.Members || {}; 
PaginaWeb.Members.Impresion = PaginaWeb.Members.Impresion || {};

PaginaWeb.Members.Impresion.ListasPrecio = function (pars) {

    var ctx = "";
    var validador = His.Controles.Validador();
    var util;
    var ajax = _Members.ajax;
    var urlBase = _Members.urlBase();
    pars = pars || {};

    var params = {
        datos: {},
        msgs: {
            Seleccionar: "Seleccione una lista",
            OperacionesExitosa: "Operación Exitosa",
            ErrorImpresion: "Ocurrió un problema al imprimir la lista",
        },
        url: {
            abrir: urlBase + "/Members/ImpresionListasPrecio",
            imprimir: urlBase + "/Members/ReporteListasPrecio"
        },
        modal: {
            idModal: "modalImpresionLista",
            idDatos: "contDatosImpresionLista",
            selContPopUp: "#ContenedorPopUp4"
        }
    }

    //#region Mapeos 
    var catalogos = {
        ListasPrecios: ["selListaPrecios"]
    }
    //#endregion

    //#region Metodos Comunes

    function error(err) {
        console.log(err);
    }

    //#endregion

    //#region Modal

    //#region Inicializar

    function abrirModal() {        
        LayoutManager.abrirPopUpAjax(params.modal.idModal, params.modal.selContPopUp, params.url.abrir,
            {}, "", { minWidth: '450rem', minHeight: '200rem' }, inicializarModal, "post", cerrar, {});
    }

    function inicializarModal() {
        try {
            ctx = $("#" + params.modal.idModal);
            util = PaginaWeb.Utils(ctx, _Members.urlBase());
            //inicializar Controles
            inicializarControlesModal();
            util.getCatalogos(catalogos);
        } catch (err) {
            error(err);
        }
    }

    function inicializarControlesModal() {
        try {
            //Inicializar controles
            ctx.find(".ctrlSelect2").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });
            //Eventos Botones 
            ctx.find("#btnImprimir").off("click").on("click", onClickImprimirLista);
            ctx.find("#btnCancelar").off("click").on("click", onClickCerrar);
        }
        catch (err) {
            error(err);
        }
    }

    //#endregion

    //#region Eventos controles   
    function onClickImprimirLista() {
        try { 
            var cont = ctx.find("#" + params.modal.idDatos)[0];
            if (validador.validar(cont)) {
                var idLista = ctx.find("#selListaPrecios").val();
                var formato = ctx.find("#selFormato").val();
                window.open(params.url.imprimir + "?idLista= " + idLista + "&formato=" + formato);
            }
        } catch (err) {
            error(err);
        }
    }

    function onClickCerrar() {
        try { 
            alertify[params.modal.idModal]().close();
        } catch (err) {
            error(err);
        }
    }

    //#endregion

    //#endregion

    //#region Cerrar
    function cerrar() {
        try {
        } catch (e) {
            error(e);
        }
    }
    //#endregion 

    //#region Metodos Publicos
    var forma = {
        abrir: function () {
            abrirModal();
        }
    }
    return forma;
    //#endregion 

}