﻿var PaginaWeb = PaginaWeb || {};
PaginaWeb.Members = PaginaWeb.Members || {};
PaginaWeb.Members.Proyectos = function (opt) {

    var ctx = null;

    var rutaBase = opt ?.rutaBase;  

    var _refreshTimer;
        
    var params = {
        baseUrl: "",
        urls: {
            instalaciones: rutaBase + "/Members/Instalaciones",

        },
        modal: {
            idModal: "contenedorInstalaciones",          
            idContenedor:"contInstalaciones"
        }
    };

    $.extend(true, params, opt);
    function error(err) {
        console.log(err);
    }

    //#region Inicializar
    function inicializar() {
        ctx = $("#" + params.modal.idModal);
        util = PaginaWeb.Utils(ctx, rutaBase);
        agregarEventos();
        bindEventsInstalaciones();
    }

    function agregarEventos() {
        ctx.find("#btnGantt").off("click", abrirModalGantt).on("click", abrirModalGantt);
        console.log("vincular controles");
       
    }

    function abrirModalGantt() {
        PaginaWeb.Members.Proyectos.ModalGantt({ rutaBase: rutaBase}).abrir();       
    }
  
    //#endregion

    //#region RefreshPage


    var idleRefreshPage = (function () {
        //window.onunload = unloadPage;
        //window.onmousemove = resetTimer;
        //window.onmousedown = resetTimer;  // catches touchscreen presses as well
        //window.ontouchstart = resetTimer; // catches touchscreen swipes as well
        //window.onclick = resetTimer;      // catches touchpad clicks as well
        //window.onkeydown = resetTimer;
        //window.addEventListener('scroll', resetTimer, true); // improved; see comments

        var _ajaxCallObj;        

        var isOn = false;

        function off () {
            $(window).off("unload", unloadPage);
            $(window).off("mousemove", resetTimer);
            $(window).off("mousedown", resetTimer);
            $(window).off("touchstart", resetTimer);
            $(window).off("click", resetTimer);
            $(window).off("keydown", resetTimer);
            $(window).off("scroll", resetTimer);

            unloadPage();
            isOn = false;
        }

        function on(ajaxCallObj) {
            $(window).on("unload", unloadPage);
            $(window).on("mousemove", resetTimer);
            $(window).on("mousedown", resetTimer);
            $(window).on("touchstart", resetTimer);
            $(window).on("click", resetTimer);
            $(window).on("keydown", resetTimer);
            $(window).on("scroll", resetTimer);

            _ajaxCallObj = ajaxCallObj;
            initTimer();
            isOn = true;
        }

        function refreshPageFunction() {
            $('.svg-icon').attr("class", "svg-icon-selected");
            ajaxCall(_ajaxCallObj);
        }

        function initTimer() {
            clearTimeout(_refreshTimer);
            var time = _ajaxCallObj.refreshTime !== undefined ? _ajaxCallObj.refreshTime / 2 : 20000;
            console.log("TIME" + time);
            _refreshTimer = setTimeout(refreshPageFunction, time);
        }


        function resetTimer() {
            clearTimeout(_refreshTimer);

            $('.svg-icon-selected').attr("class", "svg-icon");
            var time = _ajaxCallObj.refreshTime !== undefined ? _ajaxCallObj.refreshTime / 2 : 10000;
            console.log("TIME" + time);
            _refreshTimer = setTimeout(refreshPageFunction, time);
        }

        function unloadPage() {
            clearTimeout(_refreshTimer);
        }

        return {
            on: on,
            off: off
        };
    }());

    //#endregion Actualizacion


    //#region EventosInstalaciones

    function bindEventsInstalaciones() {
        $('.loading-container').hide();
        $("form.instalaciones-form").unbind();
        $("form.instalaciones-form").submit(function (event) {
            event.preventDefault();
            event.stopPropagation();
            var post_url = $(this).attr("action");
            var request_method = $(this).attr("method");
            var targetId = $(this).data("target");
            var form_data = $(this).serialize();
            var success = $(this).data("success");
            var append = $(this).data("append");
            var modal = $(this).data("modal");
            var refresh = $(this).data("refresh");
            var file = $(this).data("file");
            var wait = $(this).data("wait");
            var refreshTime = $(this).data("refresht");
            var content_type = "application/x-www-form-urlencoded";            

            if (modal) {
                $('.modal').modal('hide');
            }

            if (file) {
                form_data = new FormData($(this)[0]);
                content_type = false;
            }

            if (wait !== true) {
                $('.loading-container').show();
            }

            //Limpia la ventana de errores.
            hideError();            

            var callObj = {
                post_url: post_url,
                request_method: request_method,
                targetId: targetId,
                form_data: form_data,
                append: append,
                success: success,
                refresh: refresh,
                refreshTime: refreshTime,
                content_type: content_type
            };

            ajaxCall(callObj);
        });
    }

    function ajaxCall(callObj) {        
        idleRefreshPage.off();
        if (callObj.refresh) {
            idleRefreshPage.on(callObj);
        }
        console.log("calling " + callObj.post_url);
        $.ajax({
            processData: false,
            url: callObj.post_url,
            type: callObj.request_method,
            data: callObj.form_data,
            done: callObj.success,
            contentType: callObj.content_type,
            cache: false
        }).success(function (response) {
            if (response.error !== undefined && response.error) {
                showError(response.message);
            }
            else {
                if (callObj.append)
                    $(callObj.targetId).prepend(response);
                else $(callObj.targetId).html(response);
            }
        }).complete(function () {
            bindEventsInstalaciones();
            return false;
        });
    }

    function hideError() {
        $('#error-container').removeClass('show');
        $('#error-container').addClass('collapse');
        $('#error-container .msg').html("");
    }
    function showError(error) {
        $('#error-container').removeClass('collapse');
        $('#error-container').addClass('show');
        $('#error-container .msg').html(error);
    }
    $('#error-container .close').click(function () {
        $('#error-container').removeClass('show');
        $('#error-container').addClass('collapse');
    });

    //#endRegion EventosInstalaciones



    //#region Public
    var subSeccion = {
        iniciar: function (onIniciar) {
            try {
                inicializar();
            } catch (e) {
                error(e);
            }
        },

        finalizar: function (onFinalizar) {
            try {
               //
            } catch (e) {
                error(e);
            }
        },


        idleRefreshPage: idleRefreshPage,
        ajaxCall: ajaxCall,

        onCambioLayout: function (contenedorSize) {
            try {
                //

            } catch (e) {
                error(e);
            }
        },

    }
    //#endregion Public




    return subSeccion;




};

PaginaWeb.Members.Proyectos.ModalGantt = function (pars) {

    var ctx = "";
    //var validador = His.Controles.Validador();
    var util;
    //var ajax = His.Manejadores.Ajax.AjaxManager();
    var urlBase = pars.rutaBase;
    pars = pars || {};
    var _callback = null;
    var params = {
        callbackRefrescar: pars.callbackRefrescar,

        datos: {
            tareas: null,
            dependencias: null,
            recursos: null,
            recursosAsignacion: null
        },
        msgs: {
            OperacionesExitosa: "Operación Exitosa",
            OperacionError: "No se pudo realizar la operación",
            Guardado: "Contacto guardado",
            ErrorGuardar: "Ocurrio un problema al guardar el contacto",
            Borrado: "Se borro el contacto",
            ErrorBorrar: "No se pudo borrar el contacto, revisar relaciones",
            Confirmar: "¿Seguro que desea borrar el contacto?",
            ConfirmarTitulo: "Borrar el contacto",
            Seleccionar: "Seleccione un contacto"
        },
        urls: {
            abrir: urlBase + "/Members/ModalGantt",
            Guardar: urlBase + "/Members/AgregarActualizarContacto",
            listaReportes: urlBase + "/Members/ObtenerReportesProyecto",           
            actualizarReporte: urlBase + "/Members/ActualizarReporteProyecto", 
        },
        modal: {
            idModal: "datosGantt",
            idDatos: "contDatos",
            selContPopUp: "#ContenedorPopUp1"
        }

    }
    var _aplicacionesSeleccionadas = [];


    //#region Mapeos
    var objeto = {
        IdContacto: "IdContacto",
        Nombre: "Nombre",
        Empresa: "Empresa",
        Email: "Email",
        Aplicaciones: "Aplicacion-IdAplicacion"
    }

    var catalogos = {
        Aplicaciones: ["Aplicacion"]
    }


    //#endregion

    //#region "Metodos Comunes "

    function error(err) {
        console.log(err);
    }

    function prepararDatos(tareas) {

        var dependencias = [];
        var recursos = [];
        var recursosAsignacion = [];
        //preparar datos
        tareas.forEach(function (t,it) {

            t.FechaAsignacion= new Date(parseInt(t.FechaAsignacion.slice(6, -2)));
            t.FechaProgramada= new Date(parseInt(t.FechaProgramada.slice(6, -2)));
            t.FechaReporte= new Date(parseInt(t.FechaReporte.slice(6, -2)));
            t.FechaSolucion = new Date(parseInt(t.FechaSolucion.slice(6, -2)));

            t.ReporteSucesores.forEach(function (s) {
                if (t.ReportePredecesorId) {
                    dependencias.push({
                        "id": dependencias.length+1,
                        "predecessorId": t.ReportePredecesorId,
                        "successorId": s.Id,
                        "type": 0
                    });
                }
            });

            recursosAsignacion.push({
                'id': recursosAsignacion.length + 1,
                'taskId': t.Id,
                'resourceId':t.AsignadoId
            })

            var existe = recursos.find(r => r.id == t.AsignadoId);
            if (t.DesarrolladorAsignado && !existe) {
                recursos.push({
                    'id': t.AsignadoId,
                    'text': t.DesarrolladorAsignado.Nombre
                })
            }

        });
       
        params.datos.tareas = tareas;
        params.datos.dependencias = dependencias;
        params.datos.recursos = recursos;
        params.datos.recursosAsignacion = recursosAsignacion;       
        refrescarDataSource();//setTimeout(refrescarDataSource, 1000);
        return tareas;
    }


    function refrescarDataSource() {
        var diag = ctx.find("#gantt").dxGantt("instance");       
        diag.option("resources.dataSource", params.datos.recursos);        
        diag.option("dependencies.dataSource", params.datos.dependencias);
        diag.option("resourceAssignments.dataSource", params.datos.recursosAsignacion);
        //diag.repaint();
    }

    //#endregion

    //#region Modal

    //#region Inicializar

    function enviarPeticion(url, method, data, callbackDatos) {
        var d = $.Deferred();

        method = method || "GET";

        $.ajax(url, {
            method: method || "GET",
            data: data,
            cache: false,
            //xhrFields: { withCredentials: true }
        }).done(function (result) {
            if (callbackDatos && typeof callbackDatos === "function") {
                var datos = callbackDatos(result);
                d.resolve(datos);
            } else {
                d.resolve(method === "GET" ? result : result);
            }
        }).fail(function (xhr) {
            d.reject(xhr.responseJSON ? xhr.responseJSON.Message : xhr.statusText);
        });

        return d.promise();
    }


    function iniciarGantt() {

        ctx = $("#" + params.modal.idModal);
        var datos = ctx.data()
        var customStore = new DevExpress.data.CustomStore({            
            load: function () {
                if (!params.datos.tareas) {
                    return enviarPeticion(params.urls.listaReportes, "GET", datos,
                        function (result) {
                            return prepararDatos(result.data);
                        });
                }
                var d = $.Deferred();
                d.resolve({ data: params.datos.tareas });
                return d.promise();               
            },
            //insert: function (values) {
            //    debugger;
            //    return enviarPeticion(URL + "/InsertOrder", "POST", {
            //        values: JSON.stringify(values)
            //    });
            //},
            update: function (key, values) {
                return enviarPeticion(params.urls.actualizarReporte, "POST", {
                    key: key,
                    values: JSON.stringify(values)
                }, function (result) {                   
                    return result.Elemento;
                });
            },
            //remove: function (key) {
            //    debugger;
            //    return enviarPeticion(URL + "/DeleteOrder", "DELETE", {
            //        key: key
            //    });
            //}
        });

        var recursosStore = new DevExpress.data.CustomStore({            
            load: function () {              
                if (!params.datos.tareas) {
                    return enviarPeticion(params.urls.listaReportes, "GET", datos,
                        function (result) {
                            prepararDatos(result.data);    
                            return params.datos.recursos;
                    });
                }
                var d = $.Deferred();
                d.resolve({ data: params.datos.recursos   });                
                return d.promise();
            },
        });
              
        var dependenciasStore = new DevExpress.data.CustomStore({            
            load: function () {   
                if (!params.datos.tareas) {
                    return enviarPeticion(params.urls.listaReportes, "GET", datos,
                        function (result) {
                            prepararDatos(result.data);
                            return params.datos.dependencias;
                        });
                }
                var d = $.Deferred();
                d.resolve({ data: params.datos.dependencias });
                return d.promise();
            },
        });

        var recursosAsignacionStore = new DevExpress.data.CustomStore({           
            load: function () {         
                if (!params.datos.tareas) {
                    return enviarPeticion(params.urls.listaReportes, "GET", datos,
                        function (result) {
                            prepararDatos(result.data);
                            return params.datos.recursosAsignacion;
                        });
                }
                var d = $.Deferred();
                d.resolve({ data: params.datos.recursosAsignacion });
                return d.promise();
            },
        });

        ctx.find("#gantt").dxGantt({
            toolbar: {
                items: [                    
                    "collapseAll",
                    "expandAll",                                  
                    "separator",
                    "zoomIn",
                    "zoomOut",                    
                ]
            },
            //resources: {
            //    dataSource: customStore,
            //    keyExpr: "Id",
            //    textExpr: "Titulo",
            //    //colorExpr: "resourceColor"
            //},
            tasks: {
                dataSource: customStore,
                keyExpr: "Id",
                parentIdExpr: "ReportePredecesorId",
                titleExpr:"Titulo",               
                progressExpr: "Progreso",
                startExpr: "FechaAsignacion",
                endExpr: "FechaSolucion",
                //colorExpr: "taskColor"
            },
            //dependencies: {
            //    dataSource: dependenciasStore,
            //    keyExpr: "id",
            //    typeExpr: "type",
            //    predecessorIdExpr: "predecessorId",
            //    successorIdExpr: "successorId"              
            //},
            //resources: {
            //    dataSource: recursosStore,
            //    keyExpr: "id",
            //    textExpr: "text",
            //    //colorExpr: "resourceColor"
            //},
            //resourceAssignments: {
            //    dataSource: recursosAsignacionStore ,
            //    keyExpr: "id",
            //    resourceIdExpr: "resourceId",
            //    taskIdExpr: "taskId"                
            //},
            editing: {
                enabled: true,                             
                allowTaskAdding: false,
                allowTaskUpdating: true,
                allowTaskDeleting: false
            },
            columns: [{
                dataField: "Titulo",
                caption: "Titulo",
                width: 300
            }, {
                    dataField: "FechaAsignacion",
                caption: "Inicio"
            }, {
                    dataField: "FechaSolucion",
                caption: "Fin"
            }],
            scaleType: "weeks",
            taskListWidth: 500,
            height: function () {                
                return window.innerHeight*.87;
            },
           

        });
    }
    //#endregion

    //#endregion




    //#region Cerrar
    function cerrar() {
        try {
            // ctx.find("#btnGuardar").off("click", onClickGuardar);
            // ctx.find("#btnCancelar").off("click", onClickCancelar);
        } catch (e) {
            error(e);
        }
    }
    //#endregion
    //#endregion


    //#region "Metodos Publicos"
    var forma = {       
        iniGantt() {
            iniciarGantt();
        }

    }
    return forma;
    //#endregion 

}

