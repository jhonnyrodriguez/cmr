﻿var PaginaWeb = PaginaWeb || {}; 
PaginaWeb.Members = PaginaWeb.Members || {};
PaginaWeb.Members.Versiones = function (opt) {

    var _params = {
        baseUrl: "",
        url: {
            versionesAplicaciones: "/Members/VersionesAplicaciones",
            obtenerRutasAplicacion: "/Members/ObtenerRutasAplicacion",
            versionArchivos: "/Members/ObtenerVersionArchivos",
            descargaAplicacion: "/Members/DescargarAplicacionArchivo",
            descargaArchivo: "/Members/DescargaArchivo",
            
        }
    };
    var util = null;
    var ctx = null;
    
    $.extend(true, _params, opt);
    function error(err) {
        console.log(err);
    }

    var catologo = {
        AplicacionesVersiones: ["Version"]
    };
    var catologoParametros= {
        AplicacionesVersiones: []
    }

    function inicioVentana() {
        try {
            ctx = $(".wrapper-versiones");
            util = PaginaWeb.Utils(ctx, _params.baseUrl);
            $("#btnAgregarAplicacion").off("click", agregarAplicacion).on("click", agregarAplicacion);
            inicializaEventos();
           
        } catch (e) {
            error(e);
        } 
    }


    function inicializaEventos() {
        try {
            ctx.find(".versiones-aplicacion").off("click", abreDescargaManual).on("click", abreDescargaManual);

            $(".select-adjust-container").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                inputSelect.select2({
                    width: '100%',
                    allowClear: false,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                });
            });
            
           
            ctx.find(".versiones-descarga").off("click", onClickDescarga).on("click", onClickDescarga);
            
            ctx.find(".eliminar-aplicacion").off("click", onClickEliminar).on("click", onClickEliminar);
            ctx.find(".editar-aplicacion").off("click", onClickEditar).on("click", onClickEditar);
        } catch (e) {
            error(e);
        } 
    }
    function onClickEliminar() {
        try {
            var idAplicacion = $(this).data("idaplicacion");
            PaginaWeb.Members.VersionesEditarAplicaciones(_params).abrir(idAplicacion, refrescaAplicaciones);
        } catch (e) {
            error(e);
        } 
    }
    function onClickEditar() {
        try {
            var idAplicacion = $(this).data("idaplicacion");
            PaginaWeb.Members.VersionesEditarAplicaciones(_params).abrir(idAplicacion, refrescaAplicaciones);
        } catch (e) {
            error(e);
        }
    }
    function onClickDescargaManual() {
        try {
            var item = $(this).closest(".item-aplicacion");
            var idAplicacion = item.data("idaplicacion");
            var version = item.find("#Version").val();
            util.Accion(_params.baseUrl + _params.url.obtenerRutasAplicacion, { idVersion: version, idAplicacion: idAplicacion },
                function (data) {
                    $.each(data,
                        function (i, e) {
                            downloadURL(_params.baseUrl + _params.url.descargaArchivo +
                                "?ruta=" +
                                e);
                            
                        });
                }, "post", "json");  
        } catch (e) {
            error(e);
        } 
    }


    function onClickDescarga() {
        try {
            var item = $(this).closest(".item-aplicacion");
            var idaplicacion = item.data("idaplicacion");
            var ultimaversion = item.data("ultimaversion");
            util.Accion(_params.baseUrl + _params.url.obtenerRutasAplicacion, { idVersion: ultimaversion, idAplicacion: idaplicacion },
                function (data) {
                    $.each(data,
                        function (i, e) {
                            downloadURL(_params.baseUrl + _params.url.descargaArchivo +
                                "?ruta=" +
                                e);
                            //window.location = _params.baseUrl + _params.url.descargaArchivo +
                            //    "?ruta=" +
                            //    e ;
                        });
                }, "post", "json");  
        } catch (e) {
            error(e);
        }
    }

    var count = 0;
    function downloadURL(url) {
        var hiddenIframeId = 'hiddenDownloader' + count++;
        var iframe = document.createElement('iframe');
        iframe.id = hiddenIframeId;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
        iframe.src = url;
    }
   
   

    function abreDescargaManual() {
        try {
            var item = $(this).closest(".item-aplicacion");
            var idAplicacion = item.data("idaplicacion");
            PaginaWeb.Members.VersionesOpcionesDescarga(_params).abrir({ idAplicacion: idAplicacion });
        } catch (e) {
            error(e);
        } 
    }

    function agregarAplicacion() {
        try {
            PaginaWeb.Members.VersionesEditarAplicaciones(_params).abrir(0, refrescaAplicaciones);
        } catch (e) {
            error(e);
        } 
    }

    function refrescaAplicaciones() {
        try {
            ctx.find("#cntAplicaciones").empty();
           
            util.Accion(_params.baseUrl + _params.url.versionesAplicaciones, {},
                function (data) {
                    ctx.find("#cntAplicaciones").empty();
                    ctx.find("#cntAplicaciones").html(data);
                    inicializaEventos();
                }, "post","html");
        } catch (e) {
            error(e);
        } 
    }

   
    //#region Public
    var subSeccion = {
        iniciar: function (onIniciar) {
            try {
              
                $(".fullWapper").addClass("height100p");

                //adminCotizaciones._agregarEventos();
               

                if (onIniciar && typeof onIniciar === "function") {
                    onIniciar(undefined);
                }

                inicioVentana();
            } catch (e) {
                error(e);
            }
        },

        finalizar: function (onFinalizar) {
            try {
                $(".fullWapper").removeClass("height100p");
                //adminCotizaciones._quitarEventos();
                if (onFinalizar && typeof onFinalizar === "function") {
                    onFinalizar();
                }
            } catch (e) {
                error(e);
            }
        },

        onCambioLayout: function (contenedorSize) {
            try {

            } catch (e) {
                error(e);
            }
        },
        
    }
    //#endregion Public


    return subSeccion;
 

   

};



PaginaWeb.Members.VersionesEditarAplicaciones = function (opt) {

    var idModal = "modalEditarAplicacion";
    var ctx = null;
    var util = null;
    var validador = His.Controles.Validador();

    var params = {
        baseUrl: "",
        msj: {
            agregarAplicacion: "Agregar Aplicación",
            eliminarOk: "Se ha eliminado la aplicación correctamente",
            eliminarError: "Ha ocurrido un error al eliminar la aplicación",
            eliminarVerOk: "Se ha eliminado la versión correctamente",
            eliminarVerError: "Ha ocurrido un error al eliminar la versión",
            confirmacionAplicacion: "¿Esta seguro que desea eliminar la aplicación?",
            continuar: "Continuar",
            cancelar: "Cancelar",
            confirmacionVersion: "¿Esta seguro que desea eliminar la versión?",
            guardarOk: "Se ha guardado la aplicación correctamente",
            error: "Ha ocurrido un error al guardar la aplicación ",
            remover: "Remover"
        },
        url: {
            editarAplicacion: "/Members/EditarAplicacion",
            cargaVersiones: "/Members/ObtenerAplicacionVersiones",
            eliminarVersion: "/Members/EliminarVersion",
            correoVersion: "/Members/CorreoVersion",
            eliminarAplicacion: "/Members/EliminarAplicacion",
            actualizarAplicacion: "/Members/ActualizarAplicacion"
        }
    };
    var archivo = null;

    $.extend(true, params, opt);
    var _callback = null;

    function onAbrirVentana(idAplicacion) {
        try {
            var minHeight = '580rem';
            //var height = '580rem';
            if (idAplicacion === 0) {
                minHeight = '250rem';
                // height = '250rem';
            }
            LayoutManager.abrirPopUpAjax(idModal, "#ContenedorPopUp1", params.baseUrl + params.url.editarAplicacion,
                { idAplicacion: idAplicacion },
                "",
                { minWidth: '700rem', minHeight: minHeight },
                inicio,
                "post");
        } catch (e) {
            forma.error(e);
        }
    }


    function inicio() {
        try {
            ctx = $("#" + idModal);
            util = PaginaWeb.Utils(ctx);
            inicializarControles();
            cargarComboIconos();
            cargaVersiones();
            var datos = getDatos();
            if (datos.IdAplicacion === 0)
                alertify["modalEditarAplicacion"]().setHeader(params.msj.agregarAplicacion);
            util.setDatos(forma.objeto, datos);
            habilitar(datos);
        } catch (e) {
            forma.error(e);
        }

    }

    function habilitar(datos) {
        try {
            if (datos.IdAplicacion === 0) {
                ctx.find("#fieldVersiones").hide();
                ctx.find("#btnEliminar").hide();
            } else {
                ctx.find("#fieldVersiones").show();
                ctx.find("#btnEliminar").show();
            }
        } catch (e) {
            forma.error(e);
        }
    }

    function getDatos() {
        try {
            return ctx.data("elemento");
        } catch (e) {
            forma.error(e);
        }
        return null;
    }

    function inicializarControles() {
        try {
            ctx.find("#btnAgregarVersion").off("click", onClickAgregarVersion).on("click", onClickAgregarVersion);
            ctx.find("#btnGuardar").off("click", onClickGuardar).on("click", onClickGuardar);
            ctx.find("#btnEliminar").off("click", onClickEliminar).on("click", onClickEliminar);
            cargarComboIconos();

        } catch (e) {
            forma.error(e);
        }
    }

    function cerrar() {
        try {
            ctx.find("#btnGuardar").off("click", onClickGuardar);
            ctx.find("#btnEliminar").off("click", onClickEliminar);
        } catch (e) {
            forma.error(e);
        }
    }

    function onCargaVersiones() {
        try {
            cargaVersiones();
            if (_callback)
                _callback();
        } catch (e) {
            forma.error(e);
        }
    }
    function cargaVersiones() {
        try {
            var id = ctx.data("idaplicacion");
            util.Accion(params.baseUrl + params.url.cargaVersiones,
                {
                    idAplicacion: id
                },
                function (data) {
                    ctx.find("#contenedorVersiones").empty();
                    ctx.find("#contenedorVersiones").html(data);
                    inicializaEventosVersiones();
                }, "post", "html");
        } catch (e) {
            forma.error(e);
        }
    }


    function inicializaEventosVersiones() {
        try {
            var ctxApp = ctx.find("#contenedorVersiones");

            ctxApp.find(".editar-version").off("click", onClickEditarVersion).on("click", onClickEditarVersion);
            ctxApp.find(".eliminar-version").off("click", onClickEliminarVersion).on("click", onClickEliminarVersion);
            //Función enviar por correo.
            ctxApp.find(".correo-version").off("click", onClickEditarVersion).on("click", onClickCorreoVersion);

        } catch (e) {
            forma.error(e);
        }
    }
    //#region Eventos
    function onClickAgregarVersion() {
        try {
            var idAplicacion = ctx.data("idaplicacion");
            PaginaWeb.Members.VersionesEditarVersion(params).abrir({ idAplicacion: idAplicacion, idVersion: 0 }, onCargaVersiones);
        } catch (e) {
            forma.error(e);
        }
    }

    function onClickGuardar() {
        try {

            var cont = ctx.find("#" + idModal)[0];
            if (validador.validar(cont)) {
                var aplicacion = util.getDatos(forma.objeto);
                util.Accion(params.baseUrl + params.url.actualizarAplicacion,
                    {
                        aplicacion: aplicacion
                    },
                    function (data) {
                        if (data && data.Resultado === 0) {
                            ctx.idigitalesNotificaciones("Guardar", params.msj.guardarOk, "success");
                            ctx.find("#IdAplicacion").val(data.Id);
                            aplicacion.IdAplicacion = data.Id;
                            habilitar(aplicacion);
                            if (_callback)
                                _callback();

                        } else {
                            ctx.idigitalesNotificaciones("Guardar", params.msj.guardarError, "error");
                        }
                    });
            }
        } catch (e) {
            forma.error(e);
        }
    }

    function readableBytes(bytes) {
        var i = Math.floor(Math.log(bytes) / Math.log(1024)),
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
    }

    function onClickEliminar() {
        try {
       
            alertify.confirm(params.msj.confirmacionAplicacion).set({
                'title': params.msj.continuar,
                'labels': { ok: params.msj.continuar, cancel: params.msj.cancelar },
                'onok': onEliminarAplicacion,
                'oncancel': function () { }
            });

        } catch (e) {
            forma.error(e);
        }
    }

    function onEliminarAplicacion() {
        try {
            util.Accion(params.baseUrl + params.url.eliminarAplicacion,
                {
                    idAplicacion: ctx.data("idaplicacion"),
                },
                function (data) {
                    if (data && data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("Guardar", params.msj.guardarOk, "success");
                        if (_callback)
                            _callback();
                        alertify[idModal]().close();
                    } else {
                        ctx.idigitalesNotificaciones("Guardar", params.msj.guardarError, "error");
                    }
                });
        } catch (e) {
            forma.error(e);
        }
    }

    function onClickEditarVersion() {
        try {
            var idVersion = $(this).data("idversion");
            var idAplicacion = ctx.data("idaplicacion");
            PaginaWeb.Members.VersionesEditarVersion(params).abrir({ idAplicacion: idAplicacion, idVersion: idVersion }, cargaVersiones);
        } catch (e) {
            forma.error(e);
        }
    }

    function onClickCorreoVersion() {
        try {
            var domElement = this;
            $(domElement).removeClass("fa-envelope-o").addClass("fa-rocket");
            $(domElement).off("click", onClickEditarVersion);

            util.Accion(params.baseUrl + params.url.correoVersion,
                {
                    idVersion : $(this).data("idversion"),
                    idAplicacion: ctx.data("idaplicacion")
                },
                function (data) {
                    $(domElement).off("click", onClickEditarVersion).on("click", onClickCorreoVersion);
                    $(domElement).removeClass("fa-rocket").addClass("fa-envelope-o");

                    if (data && data.Resultado === 0) {
                        
                        domElement.setAttribute('style','text-shadow: 0 0 4px green;');
                    }
                    else {
                        domElement.setAttribute('style', 'text-shadow: 0 0 4px red;');
                    }
                });
        } catch (e) {
            forma.error(e);
        }
    }


    var idVerElim = null;
    function onClickEliminarVersion() {
        try {
            idVerElim = $(this).data("idversion");
            alertify.confirm(params.msj.confirmacionVersion).set({
                'title': params.msj.continuar,
                'labels': { ok: params.msj.continuar, cancel: params.msj.cancelar },
                'onok': onEliminarVersion,
                'oncancel': function () { }
            });

        } catch (e) {
            forma.error(e);
        }
    }

    function onEliminarVersion() {
        try {

            var id = ctx.data("idaplicacion");
            util.Accion(params.baseUrl + params.url.eliminarVersion,
                {
                    idAplicacion: id,
                    idVersion: idVerElim
                },
                function (data) {
                    if (data && data.Resultado === 0) {
                        ctx.idigitalesNotificaciones("Eliminar", params.msj.eliminarOk, "success");
                        cargaVersiones();
                    } else {
                        ctx.idigitalesNotificaciones("Eliminar", params.msj.eliminarError, "error");
                    }
                }, "post", "json");
        } catch (e) {
            forma.error(e);
        }
    }

    //#endregion

    //#region Catalogo iconos
    function cargarComboIconos() {

        //cargar combo con iconos
        var inputSelect = ctx.find('#Icono');
        var readonly = inputSelect.is('[readonly]');
        var placeholder = inputSelect.data("placeholder");
        inputSelect.select2({
            width: '100%',
            allowClear: true,
            placeholder: placeholder,
            minimumResultsForSearch: 10,
            disabled: readonly,
            templateResult: templateResultado,
            templateSelection: templateResultado,
            escapeMarkup: function (m) { return m; }
        });

        var iconos = obtenerIconosCss();
        if (iconos && $.isArray(iconos) && iconos.length > 0) {
            var select = ctx.find("#Icono").html("").append("<option></option>");
            iconos.forEach(function (value) {
                var option = $('<option value="' + value + '">' + value + '</option>');
                select.append(option);
            });
            select.val(select.data("valor")).trigger("change");
        }
    }
    function templateResultado(repo, container, otro) {
        if (repo.loading) return repo.text;

        var markup =
            '<div class="idigitales-col-11"  style="padding: 0rem;">' +
            '<i style="margin-right:5rem;font-size:15rem;" class=" icon ' + repo.text + '"></i>' +
            '<label style="font-weight:100;" class="">' + repo.text + '</label>' +
            '</div>';


        return $(markup);
    }

    function obtenerIconosCss() {
        var iconos;
        try {

            if (window.iconosCmrPage)
                return window.iconosCmrPage;

            iconos = [];
            var stylesheet;

            for (var i in document.styleSheets) {
                if (document.styleSheets.hasOwnProperty(i)) {
                    if (document.styleSheets[i].href && document.styleSheets[i].href.indexOf("fontCMRPage.css") !== -1) {
                        stylesheet = document.styleSheets[i];
                        break;
                    }
                }
            }

            if (stylesheet && stylesheet.cssRules) {
                for (var j in stylesheet.cssRules) {
                    if (stylesheet.cssRules.hasOwnProperty(j)) {
                        if (stylesheet.cssRules[j] &&
                            stylesheet.cssRules[j].type === CSSRule.STYLE_RULE &&
                            stylesheet.cssRules[j].selectorText.indexOf(".icon-") !== -1) {
                            var regla = stylesheet.cssRules[j].selectorText.split(":")[0];
                            iconos.push(regla.replace(".", ""));
                        }
                    }
                }
            }

        } catch (err) {
            error(err);
        }
        window.iconosHis = iconos;
        return iconos;

    }

    //#endregion 


    var forma = {
        abrir: function (idAplicacion, callback) {

            try {
                _callback = callback;
                onAbrirVentana(idAplicacion);
            } catch (e) {
                forma.error(e);
            }

        },
        error: function (e) {
            console.log(e);
        },
        objeto: {
            IdAplicacion: "IdAplicacion",
            TAG: "TAG",
            Nombre: "Nombre",
            Descripcion: "Descripcion",
            Icono: "Icono",
        },
        objetoVersion: {
            IdVersion: "IdVersion",
            Cambios: "Cambios",
            Sistema: "Sistema",
            Versiones: "Versiones",
            IdAplicacion: "IdAplicacion",
        },
        catalogos: {

        }
    }
    return forma;

}


PaginaWeb.Members.VersionesEditarVersion = function (opt) {

    var idModal = "modalEditarVersion";
    var ctx = null;
    var util = null;
    var validador = His.Controles.Validador();
    var params = {
        baseUrl: "",
        msj: {
            agregarAplicacion: "Agregar Aplicación",
            guardarOk: "Se ha guardado la versión correctamente",
            error: "Ha ocurrido un error al guardar la versión ",
        },
        url: {
            editarVersion: "/Members/EditarVersion",
            guardarVersion: "/Members/GuardarVersion",
            agregarArchivo: "/Members/AgregaArchivoAplicacion",
            removerArchivo: "/Members/RemoverArchivo",
        }
    };
    var archivo = null;
    var listaGeneralArchivos = [];
    var archivos = [];
    $.extend(true, params, opt);
    var _callback = null;
    var dropzone = null;


    function onAbrirVentana(datos) {
        try {
            if ($("#ContenedorPopUp2").length === 0) {
                $("body").append("<div id='ContenedorPopUp2' style='min-height:150rem;' class='hide'></div>");
            }
    
            LayoutManager.abrirPopUpAjax(idModal, "#ContenedorPopUp2", params.baseUrl + params.url.editarVersion,
                {
                    idVersion: datos.idVersion,
                    idAplicacion: datos.idAplicacion
                },
                "",
                { minWidth: '600rem', minHeight: '750rem' },
                inicio,
                "post",
                cerrar);
        } catch (e) {
            forma.error(e);
        }
    }


    function inicio() {
        try {
            ctx = $("#" + idModal);
            util = PaginaWeb.Utils(ctx);
            inicializarControles();
            var datos = getDatos();
            util.setDatos(forma.objetoVersion, datos);
            setVersion(datos);
            setArchivo(datos);

        } catch (e) {
            forma.error(e);
        }

    }

    function getDatos() {
        try {
            return ctx.data("elemento");
        } catch (e) {
            forma.error(e);
        }
        return null;
    }

    function setVersion(datos) {
        try {
            if (datos.Version !== null && datos.Version !== "") {
                var versionArray = datos.Version.split(".");
                ctx.find("#Version1").val(versionArray[0]);
                ctx.find("#Version2").val(versionArray[1]);
                ctx.find("#Version3").val(versionArray[2]);
                ctx.find("#Version4").val(versionArray[3]);
            }
        } catch (e) {
            forma.error(e);
        }
    }

    function setArchivo(datos) {
        try {           
            if (datos.Ruta !== null && datos.Ruta !== "" && dropzone !== null) {
                if (datos.Archivos !== null) {
                    $.each(datos.Archivos,
                        function (i, e) {
                            var mockFile = { name: e.Nombre, size: e.Tamano, type: e.Tipo };
                            dropzone.emit("addedfile", mockFile);
                        });
                }

            }
        } catch (e) {
            forma.error(e);
        }
    }


    function archivoRemovido(archivoRemovido) {

        for (val in listaGeneralArchivos) {
            //fileinfo = archivos[val];
            fileinfo = listaGeneralArchivos[val];
            if (archivoRemovido === fileinfo.file) {
                listaGeneralArchivos.splice(val, 1);
                break;
            }
        }
    }

    function inicializarControles() {
        try {
            ctx.find(".select-adjust-container").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });

          
            $("#myCarousel").owlCarousel({
                singleItem: true,
                navigation: true,
                navigationText: [
                    "<",
                    ">"
                ],
                pagination: false,
                afterAction: afterAction
            });


            if (dropzone === null) {
                dropzone = new Dropzone("div#drop_zone",
                    {
                        url: params.baseUrl + params.url.agregarArchivo,
                        autoProcessQueue: true,
                        init: function () {
                            this.on("maxfilesexceeded", function (data) {
                                var res = eval('(' + data.xhr.responseText + ')');

                            });
                            this.on("success", function (file, response) {                               
                                var val;
                                for (val in archivos) {
                                    fileinfo = archivos[val];
                                    if (file === fileinfo.file) {
                                        fileinfo.estado = "Uploaded";
                                        fileinfo.nombreArchivo = response.Nombre; //Se regresa el nombre por si se cambia
                                        ctx.find("#txtNombre").val(response.Nombre);
                                        fileinfo.descripcion = "";
                                        fileinfo.categoria = "";
                                        fileinfo.subCat = "";
                                        fileinfo.fechaCreacion = null;
                                        fileinfo.ruta = response.Ruta;
                                        listaGeneralArchivos.push(archivos[val]);
                                        break;
                                    }
                                }
                            });
                            this.on("addedfile", function (file) {
                                // Create the remove button
                                var removeButton = Dropzone.createElement("<button class='idigitales-boton-chico idigitales-boton'>" + params.msj.remover + "</button>");
                                // Capture the Dropzone instance as closure.
                                var _this = this;
                                // Listen to the click event
                                removeButton.addEventListener("click", function (e) {
                                    // Make sure the button click doesn't submit the form:
                                    e.preventDefault();
                                    e.stopPropagation();


                                    clearScreen();
                                    var owl = $(".owl-carousel").data('owlCarousel');
                                    var rutaAarchivo = archivos[owl.currentItem].ruta;
                                    var nombreArchivo = archivos[owl.currentItem].nombreArchivo;

                                    archivoRemovido(file);

                                    console.log('archivos: ' + owl.currentItem);
                                    archivos.splice(owl.currentItem, 1);
                                    owl.removeItem(owl.currentItem);

                                    removerArchivo(rutaAarchivo, nombreArchivo);
                                    if (archivos.length === 0) {
                                        clearScreen();
                                    }

                                });
                                file.previewElement.appendChild(removeButton);
                                switch (file.type) {
                                    case "application/pdf":
                                        this.emit("thumbnail", file, _params.baseUrl + "/Content/images/icons-app/pdf-icon-150x150.jpg");
                                        break;
                                    case "application/msword":
                                    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                                        this.emit("thumbnail", file, _params.baseUrl + "/Content/images/icons-app/word-viewer.png");
                                        break;
                                    case "application/excel":
                                    case "application/vnd.ms-excel":
                                    case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                        this.emit("thumbnail", file, _params.baseUrl + "/Content/images/icons-app/xls-icon-150x150.jpg");
                                        break;
                                    case "application/mspowerpoint":
                                        this.emit("thumbnail", file, _params.baseUrl + "/Content/images/icons-app/ppt-icon.jpg");
                                        break;
                                    case "text/plain":
                                        this.emit("thumbnail", file, _params.baseUrl + "~/Content/images/icons-app/txt-icon.jpg");
                                        break;
                                    case "text/xml":
                                        this.emit("thumbnail", file, _params.baseUrl + "/Content/images/icons-app/xml-icon.jpg");
                                        break;
                                    case "application/vnd.ms-xpsdocument":
                                        this.emit("thumbnail", file, _params.baseUrl + "/Content/images/icons-app/xps-icon.jpg");
                                        break;
                                    case "audio/wav":
                                        this.emit("thumbnail", file, _params.baseUrl + "/Content/images/icons-app/wav-icon.jpg");
                                        break;
                                    case "audio/mp3":
                                        this.emit("thumbnail", file, _params.baseUrl + "/Content/images/icons-app/mp3-icon.png");
                                        break;
                                    default:
                                        switch (file.name.split('.').pop()) {
                                            case "dcm":
                                                this.emit("thumbnail", file, LayoutManager.obtenerRootApp(_params.baseUrl + "/Content/images/icons-app/dcm-icon.jpg"));
                                                break;
                                            default:
                                        }
                                }

                                var owl = $(".owl-carousel").data('owlCarousel');
                                owl.addItem(file.previewElement);
                                owl.goTo(owl.itemsAmount);

                                fileinfo = new fileInfoClass();
                                fileinfo.nombreArchivo = file.name;
                                fileinfo.type = file.type;
                                fileinfo.size = file.size;
                                fileinfo.indice = owl.itemsAmount;
                                fileinfo.extension = file.name.split('.').pop();
                                //fileinfo.fechaCreacion = null;
                                fileinfo.file = file;

                                console.log('archivos: ' + fileinfo);
                                archivos.push(fileinfo);
                                model2Screen(archivos[owl.currentItem]);
                            });
                            this.on("thumbnail", function (file, thumbnail) {
                            });
                        }
                    });
            }
            $(document.body).bind("dragover", function (e) {
                e.preventDefault();
                return false;
            });
            $(document.body).bind("drop", function (e) {
                e.preventDefault();
                return false;
            });
            clearScreen();
            ctx.find("#btnGuardar").off("click", onClickGuardar).on("click", onClickGuardar);
            ctx.find("#btnCancelar").off("click", onClickCancelar).on("click", onClickCancelar);

            $(".version-val").keydown(function (event) {
                //alert(event.keyCode);
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !== 190 && event.keyCode !== 110 && event.keyCode !== 8 && event.keyCode !== 9) {
                    return false;
                }
            });
        } catch (e) {
            forma.error(e);
        }
    }



    //#region Carousel
    function afterAction(el) {
        var owl = ctx.find(".owl-carousel").data('owlCarousel');
        if (owl !== null) {
            if (owl.currentItem < archivos.length) {
                model2Screen(archivos[owl.currentItem]);
            }
        }
    }

    function model2Screen(fileInfo) {
        if (fileInfo) {
            if (ctx.find("#divCaptura").css("visibility") === "collapse") {
                ctx.find("#divCaptura").css("visibility", "visible");
                ctx.find("#divCarousel").css("display", "table");
                ctx.find("#panelCarousel .emptyDataCarusel").hide();
            }
            ctx.find("#txtNombre").val(fileInfo.nombreArchivo);
            ctx.find("#txtTamano").val(readableBytes(fileInfo.size));
            // ctx.find("#dtpFecha").data("DateTimePicker").date(fileInfo.fechaCreacion);
            ctx.find("#txtTipo").val(fileInfo.extension);
            ctx.find("#txtTipo").val(fileInfo.extension);
        }
    }

    function screen2Model() {
        var owl = ctx.find(".owl-carousel").data('owlCarousel');
        if (owl !== null) {
            if (owl.currentItem < archivos.length) {
                fileInfo = archivos[owl.currentItem];
                fileInfo.nombreArchivo = ctx.find("#txtNombre").val();
                //fileInfo.fechaCreacion = ctx.find("#dtpFecha").data("DateTimePicker").date();
            }
        }
    }

    function clearScreen() {
        ctx.find("#divCaptura").css("visibility", "collapse");
        if (typeof listaGeneralArchivos != "undefined")
            if (listaGeneralArchivos.length === 0) {
                ctx.find("#panelCarousel .emptyDataCarusel").show();
            }
        ctx.find("#txtNombre").val("");
        ctx.find("#txtTamano").val("");
        ctx.find("#txtTipo").val("");
        //ctx.find("#dtpFecha").data("DateTimePicker").date(null);
    }
    //#endregion

    function fileInfoClass() {
        this.subCat;
        this.indice;
        this.nombreArchivo;
        this.tipoArchivo;
        this.descripcion;
        this.fechaCreacion;
        this.Extension;
        this.type;
        this.notas;
        this.ruta;
        this.size;
        this.agregaadoPor;
        this.origen;
        this.estado;
        this.file;
        this.categoria;
    }


    function removerArchivo(ruta, archivo) {
        var url = params.baseUrl + params.url.removerArchivo;
        $.ajax({
            type: "POST",
            url: url,
            data: { nombreAplicacion: archivo, idVersion: ctx.data("idversion") },
            success: function (data, status) {
            },
            error: function (error) {
                // ctx.idigitalesNotificaciones(txtTitulo, txtErrorEliminar, txtError);
            }
        });
    }

    function cerrar() {
        try {
            ctx.find("#btnGuardar").off("click", onClickGuardar);
            ctx.find("#btnCancelar").off("click", onClickCancelar);

            if (dropzone !== null) {
                dropzone.off();
                dropzone.destroy();
                dropzone = null;
            }
            if (alertify[idModal] !== undefined) {
                return setTimeout((function () {
                    return alertify[idModal]().destroy();
                }), 400);
                
            }

        } catch (e) {
            forma.error(e);
        }
    }

    //#region Eventos
    var guardando = false;
    function onClickGuardar() {
        try {
            if (guardando === false) {
                guardando = true;
                var cont = ctx.find("#" + idModal)[0];
                if (validador.validar(cont)) {
                    var v1 = ctx.find("#Version1").val() !== "" ? ctx.find("#Version1").val() : 0;
                    var v2 = ctx.find("#Version2").val() !== "" ? ctx.find("#Version2").val() : 0;
                    var v3 = ctx.find("#Version3").val() !== "" ? ctx.find("#Version3").val() : 0;
                    var v4 = ctx.find("#Version4").val() !== "" ? ctx.find("#Version4").val() : 0;

                    var version = util.getDatos(forma.objetoVersion);

                    var size = 0;
                    if (listaGeneralArchivos.length > 0) {
                        $.each(listaGeneralArchivos,
                            function(i, e) {
                                size = size + e.size;
                            });
                    }
                    version.Tamano = readableBytes(size);
                    version.Version = v1 + "." + v2 + "." + v3 + "." + v4;


                    util.Accion(params.baseUrl + params.url.guardarVersion,
                        {
                            version: version,
                        },
                        function(data) {
                            if (data && data.Resultado === 0) {
                                ctx.idigitalesNotificaciones("Guardar", params.msj.guardarOk, "success");
                                if (_callback)
                                    _callback();
                                onClickCancelar();
                                guardando = false;
                            } else {
                                ctx.idigitalesNotificaciones("Guardar", params.msj.guardarError, "error");
                                guardando = false;
                            }
                        });
                }
            }
        } catch (e) {
            forma.error(e);
            guardando = false;
        }
    }

    function readableBytes(bytes) {
        var i = Math.floor(Math.log(bytes) / Math.log(1024)),
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
    }

    function onClickCancelar() {
        try {
            alertify[idModal]().close();
            //if (alertify[idModal] !== undefined)
            //    alertify[idModal]().destroy();
        } catch (e) {
            forma.error(e);
        }
    }



    //#endregion
    var forma = {
        abrir: function (datos, callback) {

            try {
                _callback = callback;
                onAbrirVentana(datos);
            } catch (e) {
                forma.error(e);
            }

        },
        error: function (e) {
            console.log(e);
        },
        objetoVersion: {
            IdVersion: "IdVersion",
            Cambios: "Cambios",
            Sistema: "Sistema",
            Versiones: "Versiones",
            IdAplicacion: "IdAplicacion",
        },
        catalogos: {

        }
    }
    return forma;

}


PaginaWeb.Members.VersionesOpcionesDescarga = function (opt) {

    var idModal = "modalOpcionesDescarga";
    var ctx = null;
    var util = null;
    var validador = His.Controles.Validador();
    var params = {
        baseUrl: "",
        msj: {
            agregarAplicacion: "Agregar Aplicación",

            guardarOk: "Se ha guardado la versión correctamente",
            error: "Ha ocurrido un error al guardar la versión ",
        },
        url: {
            editarVersion: "/Members/OpcionesDescarga",
            obtenerRutasAplicacion: "/Members/ObtenerRutasAplicacion",
            descargaAplicacion: "/Members/DescargarAplicacionArchivo",
            versionArchivos: "/Members/ObtenerVersionArchivos",
        }
    };
    var archivo = null;
    var listaGeneralArchivos = [];
    var archivos = [];
    $.extend(true, params, opt);
    var _callback = null;
 

    var catologo = {
        AplicacionesVersiones: ["Version"],
        

    };
    var catologoParametros = {
        AplicacionesVersiones: []
    }
    var catalogosDependientes = {
        AplicacionesSistemas: {
            Version: "Sistema"

        }
    };
    var catalogoDependiente={}

        function onAbrirVentana(datos) {
        try {
            catologoParametros.AplicacionesVersiones = [datos.idAplicacion];
            if ($("#ContenedorPopUp3").length === 0) {
                $("body").append("<div id='ContenedorPopUp3' style='min-height:150rem;' class='hide'></div>");
            }

            LayoutManager.abrirPopUpAjax(idModal, "#ContenedorPopUp3", params.baseUrl + params.url.editarVersion,
                datos,
                "",
                { minWidth: '800rem', minHeight: '600rem' },
                inicio,
                "post",
                cerrar);
        } catch (e) {
            forma.error(e);
        }
    }


    function inicio() {
        try {
            ctx = $("#" + idModal);
            util = PaginaWeb.Utils(ctx, params.baseUrl);
            inicializarControles();

            util.getCatalogos(catologo, catologoParametros, inicializaCatalogo);

        } catch (e) {
            forma.error(e);
        }

    }



    function onSelectChangeVersion  () {
        var select = $(this);

        var callbackCatCargados = function() {
            ctx.find("#Sistema").val(ctx.find("#Sistema").find("option:first-child").val()).trigger("change");
        };

        var valor = select.val();
        if (valor) {
            var catalogosParametros = {};
            var catalogos = {};

            catalogosParametros["AplicacionesSistemas"] = [ctx.find(ctx.find("#Version").select2("data")[0].element).attr("tipo") + "-" + valor];
            catalogos["AplicacionesSistemas"] = ["Sistema"];
           util.getCatalogos(catalogos, catalogosParametros, callbackCatCargados);
        } else {
            ctx.find("#Sistema").val(ctx.find("#Sistema").find("option:first-child").val()).trigger("change");
        }
    }

    function inicializaCatalogo() {
        try {
            var ver = ctx.find("#Version");
            ver.val(ver.find("option:first-child").val()).trigger("change");
        } catch (e) {
            forma.error(e);
        } 
    }

    function getDatos() {
        try {
            return ctx.data("elemento");
        } catch (e) {
            forma.error(e);
        }
        return null;
    }

    function setVersion(datos) {
        try {
            if (datos.Version !== null && datos.Version !== "") {
                var versionArray = datos.Version.split(".");
                ctx.find("#Version1").val(versionArray[0]);
                ctx.find("#Version2").val(versionArray[1]);
                ctx.find("#Version3").val(versionArray[2]);
                ctx.find("#Version4").val(versionArray[3]);
            }
        } catch (e) {
            forma.error(e);
        }
    }



    function inicializarControles() {
        try {
            ctx.find(".select-adjust-container").each(function (i, inputSelect) {
                inputSelect = $(inputSelect);
                var placeholder = inputSelect.data("placeholder");
                var readonly = inputSelect.attr("readonly");
                inputSelect.select2({
                    width: '100%',
                    allowClear: false,
                    placeholder: placeholder,
                    minimumResultsForSearch: 10,
                    disabled: readonly
                });
            });
            

            ctx.find("#Version").off("change", onSelectChangeVersion).on("change", onSelectChangeVersion);
            ctx.find("#Sistema").off("change", onCambiaSistema).on("change", onCambiaSistema);
           
        } catch (e) {
            forma.error(e);
        }
    }



 



  

    function cerrar() {
        try {
            ctx.find("#btnGuardar").off("click", onClickGuardar);
            ctx.find("#btnCancelar").off("click", onClickCancelar);

          
            if (alertify[idModal] !== undefined) {
                return setTimeout((function () {
                    return alertify[idModal]().destroy();
                }), 400);

            }

        } catch (e) {
            forma.error(e);
        }
    }

    //#region Eventos

    function onClickDescargaManual() {
        try {
            var idAplicacion = ctx.find("#idAplicacion").val();
            var version = ctx.find("#Sistema").val();
            util.Accion(params.baseUrl + params.url.obtenerRutasAplicacion, { idVersion: version, idAplicacion: idAplicacion },
                function (data) {
                    $.each(data,
                        function (i, e) {
                            downloadURL(params.baseUrl + params.url.descargaArchivo +
                                "?ruta=" +
                                e);

                        });
                }, "post", "json");
        } catch (e) {
            forma.error(e);
        }
    }

    var count = 0;
    function downloadURL(url) {
        var hiddenIframeId = 'hiddenDownloader' + count++;
        var iframe = document.createElement('iframe');
        iframe.id = hiddenIframeId;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
        iframe.src = url;
    }

    function obteberDatos()
    {
        try {
            var datos = ctx.data("elemento");
            return datos;
        } catch (e) {
            forma.error(e);
        }
    }

    function obtenerDatosVersion(idVersion) {
        try {
            var datos = ctx.data("elemento");
            if (datos != null) {
                if (typeof (idVersion) == 'string')
                    idVersion = parseInt(idVersion);
                var fil = datos.AplicacionesVersiones.filter(x => x.IdVersion === idVersion);
                return fil[0];
            }
        } catch (e) {
            forma.error(e);
        }
        return null;
    }
    function onCambiaSistema() {
        try {
           
            
            var idVersion = ctx.find("#Sistema").val();
            ctx.find("#cntInformacionManual").empty();
            if (idVersion !== null) {

                var plantilla = ctx.find("#plantillaDescarga>div").clone();
                ctx.find("#cntInformacionManual").html(plantilla);

                ctx.find(".versiones-descarga-manual").off("click", onClickDescargaManual).on("click", onClickDescargaManual);

                //muestra spinner
                ctx.find("#cntInformacionManual").find("#InformacionManual").hide();
                ctx.find("#cntInformacionManual").after(LayoutManager.htmlSpin);

                var version = obtenerDatosVersion(idVersion);
                var aplicacion = obteberDatos();
                ctx.find(".sistema").text(version.Sistema);
                ctx.find(".aplicacion").text(aplicacion.Nombre + " (" + version.Version + ")");
          
                ctx.find(".tamano").text(version.Tamano);
                ctx.find(".cambios").text(version.Cambios);
                var fec = "";
                if (version.Fecha !== null)
                    fec = fechaFormatoCultura(new Date(version.Fecha));
                ctx.find(".fecha").text(fec);
                ctx.find(".version-informacion").off("mouseover", muestraTarjeta)
                    .on("mouseover", muestraTarjeta);
                ctx.find(".version-informacion").off("mouseout", ocultaTarjeta)
                    .on("mouseout", ocultaTarjeta);


                
                obtieneArchivosPorVersion(idVersion);
            } else {
                var plantilla = ctx.find("#plantillaSinVersiones>").clone();
                ctx.find("#cntInformacionManual").html(plantilla);
            }

        } catch (e) {
            forma.error(e);
        }
    }

    function ocultaTarjeta() {
        try {
            $(this).siblings(".card-info-version").css("opacity", 0);
        } catch (e) {
            forma.error(e);
        }
    }
    function muestraTarjeta() {
        try {
            var pos = $(event.target).position();
            if (pos) {
                var t = pos.top;
                $(this).siblings(".card-info-version").css("top", t + 30);
            } else {
                $(this).siblings(".card-info-version").css("top", event.clientY - 67);
            }
            
            $(this).siblings(".card-info-version").css("opacity", 1);
        } catch (e) {
            forma.error(e);
        }
    }

    function obtieneArchivosPorVersion(idVersion) {
        try {
            ctx.find("#cntArchivos").empty();
            util.Accion(params.baseUrl + params.url.versionArchivos, { idVersion: idVersion },
                function (data) {
                    if (data === null || (data !== null && data.length === 0)) {

                        ctx.find(".spin").remove();
                        var plantillaS = ctx.find("#plantillaSinVersiones>").clone();
                        ctx.find("#cntInformacionManual").html(plantillaS);
                       
                    } else {
                        $.each(data,
                            function (i, e) {
                                var plantilla = ctx.find("#plantillaArchivo").clone();

                                plantilla.find(".nombre").text(e.NombreArchivo);
                                plantilla.find(".tamano").text(e.Tamano);

                                var fec = "";
                                if (e.Fecha !== null)
                                    fec = getDateIfDate(e.Fecha);
                                plantilla.find(".fecha").text(fec);
                                plantilla.css("display", "inline-flex");
                                plantilla.find(".versiones-descarga-manual-archivo")
                                    .attr("data-nombre", e.NombreArchivo);
                                plantilla.find(".versiones-descarga-manual-archivo")
                                    .off("click", onClickDescargaManualArchivo)
                                    .on("click", onClickDescargaManualArchivo);
                                ctx.find("#cntArchivos").append(plantilla);
                                //oculta spinner
                                ctx.find("#cntInformacionManual").find("#InformacionManual").show();

                                ctx.find(".spin").remove();
                            });

                       
                    }

                }, "post", "json");
        } catch (e) {
            forma.error(e);
        }
    }

    function onClickDescargaManualArchivo() {
        try {
     
            var idaplicacion = ctx.find("#idAplicacion").val();
            var version = ctx.find("#Sistema").val();
            var nombre = $(this).data("nombre");
            window.location = params.baseUrl + params.url.descargaAplicacion +
                "?idAplicacion=" +
                idaplicacion +
                "&idVersion=" +
                version +
                "&nombreArchivo=" +
                nombre;
        } catch (e) {
            forma.error(e);
        }
    }
    function getDateIfDate(d) {
        var m = d.match(/\/Date\((\d+)\)\//);
        return m ? (new Date(+m[1])).toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' }) : d;
    }
    var guardando = false;
    function onClickGuardar() {
        try {
            if (guardando === false) {
                guardando = true;
                var cont = ctx.find("#" + idModal)[0];
                if (validador.validar(cont)) {
                    var v1 = ctx.find("#Version1").val() !== "" ? ctx.find("#Version1").val() : 0;
                    var v2 = ctx.find("#Version2").val() !== "" ? ctx.find("#Version2").val() : 0;
                    var v3 = ctx.find("#Version3").val() !== "" ? ctx.find("#Version3").val() : 0;
                    var v4 = ctx.find("#Version4").val() !== "" ? ctx.find("#Version4").val() : 0;

                    var version = util.getDatos(forma.objetoVersion);

                    var size = 0;
                    if (listaGeneralArchivos.length > 0) {
                        $.each(listaGeneralArchivos,
                            function (i, e) {
                                size = size + e.size;
                            });
                    }
                    version.Tamano = readableBytes(size);
                    version.Version = v1 + "." + v2 + "." + v3 + "." + v4;


                    util.Accion(params.baseUrl + params.url.guardarVersion,
                        {
                            version: version,
                        },
                        function (data) {
                            if (data && data.Resultado === 0) {
                                ctx.idigitalesNotificaciones("Guardar", params.msj.guardarOk, "success");
                                if (_callback)
                                    _callback();
                                onClickCancelar();
                                guardando = false;
                            } else {
                                ctx.idigitalesNotificaciones("Guardar", params.msj.guardarError, "error");
                                guardando = false;
                            }
                        });
                }
            }
        } catch (e) {
            forma.error(e);
            guardando = false;
        }
    }

    function readableBytes(bytes) {
        var i = Math.floor(Math.log(bytes) / Math.log(1024)),
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
    }

    function onClickCancelar() {
        try {
            alertify[idModal]().close();
            //if (alertify[idModal] !== undefined)
            //    alertify[idModal]().destroy();
        } catch (e) {
            forma.error(e);
        }
    }



    //#endregion
    var forma = {
        abrir: function (datos, callback) {

            try {
                _callback = callback;
                onAbrirVentana(datos);
            } catch (e) {
                forma.error(e);
            }

        },
        error: function (e) {
            console.log(e);
        },
        objetoVersion: {
            IdVersion: "IdVersion",
            Cambios: "Cambios",
            Sistema: "Sistema",
            Versiones: "Versiones",
            IdAplicacion: "IdAplicacion",
        },
        catalogos: {

        }
    }
    return forma;

}






//PaginaWeb.Members.VersionesAgregarAplicaciones = function (opt) {

//    var idModal = "modalAgregarAplicacion";
//    var ctx = null;
//    var util = null;
//    var validador = His.Controles.Validador();
//    var params = {
//        baseUrl: "",
//        msj: {
//            agregarAplicacion: "Agregar Aplicación",
//            guardarOk: "Se ha guardado la aplicación correctamente",
//            error: "Ha ocurrido un error al guardar la aplicación ",
//        },
//        url: {
//            registroAplicacion: "/Members/RegistroAplicacion",
//            agregarArchivo:  "/Members/AgregaArchivoAplicacion",
//            removerArchivo: "/Members/RemoverArchivo",
//            guardarAplicacion: "/Members/GuardarAplicacion"
//        }
//    };
//    var archivo = null;
   
//    $.extend(true, params, opt);
//    var _callback = null;

//    function onAbrirVentana(idAplicacion) {
//        try {
//            LayoutManager.abrirPopUpAjax(idModal, "#ContenedorPopUp1", params.baseUrl + params.url.registroAplicacion,
//                { idAplicacion: idAplicacion },
//                "",
//                { minWidth: '600rem', minHeight: '580rem' },
//                inicio,
//                "post",
//                cerrar,
//                {

//                });
//        } catch (e) {
//            forma.error(e);
//        } 
//    }


//    function inicio() {
//        try {
//            ctx = $("#" + idModal);
//            util = PaginaWeb.Utils(ctx);
//            inicializarControles();
//            cargarComboIconos();
//        } catch (e) {
//            forma.error(e);
//        }

//    }


//    function inicializarControles() {
//        try {
//            $("div#drop_zone").dropzone({
//                url: params.baseUrl + params.url.agregarArchivo,
//               // maxFiles: 15,
//                clickable: true,
//                addRemoveLinks: true,
//                autoProcessQueue: true,
//                maxFilesize: 500,
//                maxThumbnailFilesize:500,
//                acceptedFiles: ".zip,.rar",
//                init: function () {
//                    this.on("addedfile",
//                        function (file) {

//                            inicioCargaAplicacion(file);
//                            archivo = file;
//                        });
//                    this.on("removedfile",
//                        function (file) {

//                            ctx.find(".dz-message").show();
//                            util.Accion(params.baseUrl + params.url.removerArchivo,
//                                {
//                                    nombreAplicacion: file.name
//                                },
//                                function(data) {
//                                    archivo = null;
//                                });
//                        });
//                    this.on("success",
//                        function (file, response) {

//                            finCargaAplicacion(file);
//                        });

//                    this.on("error",
//                        function (file, response) {
//                            finCargaAplicacion(file, { idAplicacion: -1 });
//                        });

//                    this.on('sending',
//                        function (file, xhr, formData) {

//                        });
//                },
//                //dictDefaultMessage: "Drop files here to upload",
//                dictFallbackMessage: "El navegador no soporta soltar ya gregar archivos.",
//                dictFallbackText: "Utilice el formulario de reserva a continuación para cargar sus archivos como en los días anteriores.",
//                dictFileTooBig: "El archivo es muy grande ({{filesize}}MiB). Max tamaño de archivo: {{maxFilesize}}MiB.",
//                dictInvalidFileType: "No puedes subir archivos de este tipo.",
//                dictResponseError: "Servidor responde con el código {{statusCode}}.",
//                dictCancelUpload: "Cancelar carga",
//                dictCancelUploadConfirmation: "¿Estás seguro de que quieres cancelar esta carga?",
//                dictRemoveFile: "Remover archivo",
//                dictRemoveFileConfirmation: null,
//               // dictMaxFilesExceeded: "No puedes subir más archivos.",
//            });


//            $(document.body).bind("dragover", function (e) {
//                e.preventDefault();
//                return false;
//            });

//            $(document.body).bind("drop", function (e) {
//                e.preventDefault();
//                return false;
//            });

//            ctx.find("#btnGuardar").off("click", onClickGuardar).on("click", onClickGuardar);
//            ctx.find("#btnCancelar").off("click", onClickCancelar).on("click", onClickCancelar);

//            $(".version-val").keydown(function (event) {
//                //alert(event.keyCode);
//                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !== 190 && event.keyCode !== 110 && event.keyCode !== 8 && event.keyCode !== 9) {
//                    return false;
//                }
//            });
//        } catch (e) {
//            forma.error(e);
//        } 
//    }
//    function inicioCargaAplicacion() {
//        ctx.find(".dz-message").hide();
//    }
//    function finCargaAplicacion() {}
//    function cerrar() {
//        try {
//            ctx.find("#btnGuardar").off("click", onClickGuardar);
//            ctx.find("#btnCancelar").off("click", onClickCancelar);
//        } catch (e) {
//            forma.error(e);
//        }
//    }

//    //#region Eventos
//    function onClickGuardar() {
//        try {
//            var cont = ctx.find("#" + idModal)[0];
//            if (validador.validar(cont)) {

//                var aplicacion = util.getDatos(forma.objeto);
//                var v1 = ctx.find("#Version1").val() !== "" ? ctx.find("#Version1").val() : 0;
//                var v2 = ctx.find("#Version2").val() !== "" ? ctx.find("#Version2").val() : 0;
//                var v3 = ctx.find("#Version3").val() !== "" ? ctx.find("#Version3").val() : 0;
//                var v4 = ctx.find("#Version4").val() !== "" ? ctx.find("#Version4").val() : 0;

//                var version = util.getDatos(forma.objetoVersion);
//                version.Tamano = readableBytes(archivo.size);
//                version.Version = v1 + "." + v2 + "." + v3 + "." + v4;

//                aplicacion.AplicacionesVersiones = [];
//                aplicacion.AplicacionesVersiones.push(version);

//                util.Accion(params.baseUrl + params.url.guardarAplicacion,
//                    {
//                        aplicacion: aplicacion,
//                        nombreArchivo: archivo.name
//                    },
//                    function(data) {
//                        if (data && data.Resultado === 0) {
//                            ctx.idigitalesNotificaciones("Guardar", params.msj.guardarOk, "success");
//                            if (_callback)
//                                _callback();
//                            onClickCancelar();
//                        } else {
//                            ctx.idigitalesNotificaciones("Guardar", params.msj.guardarError, "error");
//                        }
//                    });
//            }
//        } catch (e) {
//            forma.error(e);
//        } 
//    }

//    function readableBytes(bytes) {
//        var i = Math.floor(Math.log(bytes) / Math.log(1024)),
//            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

//        return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
//    }

//    function onClickCancelar() {
//        try {
//            alertify[idModal]().close();
//        } catch (e) {
//            forma.error(e);
//        } 
//    }

//    //#region Catalogo iconos
//    function cargarComboIconos() {

//        //cargar combo con iconos
//        var inputSelect = ctx.find('#Icono');
//        var readonly = inputSelect.is('[readonly]');
//        var placeholder = inputSelect.data("placeholder");
//        inputSelect.select2({
//            width: '100%',
//            allowClear: true,
//            placeholder: placeholder,
//            minimumResultsForSearch: 10,
//            disabled: readonly,
//            templateResult: templateResultado,
//            templateSelection: templateResultado,
//            escapeMarkup: function (m) { return m; }
//        });

//        var iconos = obtenerIconosCss();
//        if (iconos && $.isArray(iconos) && iconos.length > 0) {
//            var select = ctx.find("#Icono").html("").append("<option></option>");
//            iconos.forEach(function (value) {
//                var option = $('<option value="' + value + '">' + value + '</option>');
//                select.append(option);
//            });
//            select.val(select.data("valor")).trigger("change");
//        }
//    }
//    function templateResultado(repo, container, otro) {
//        if (repo.loading) return repo.text;

//        var markup =
//            '<div class="idigitales-col-11"  style="padding: 0rem;">' +
//            '<i style="margin-right:5rem;font-size:15rem;" class=" icon ' + repo.text + '"></i>' +
//            '<label style="font-weight:100;" class="">' + repo.text + '</label>' +
//            '</div>';


//        return $(markup);
//    }

//    function obtenerIconosCss() {
//        var iconos;
//        try {

//            if (window.iconosHis)
//                return window.iconosHis;

//            iconos = [];
//            var stylesheet;

//            for (var i in document.styleSheets) {
//                if (document.styleSheets.hasOwnProperty(i)) {
//                    if (document.styleSheets[i].href && document.styleSheets[i].href.indexOf("fontCMRPage.css") !== -1) {
//                        stylesheet = document.styleSheets[i];
//                        break;
//                    }
//                }
//            }

//            if (stylesheet && stylesheet.cssRules) {
//                for (var j in stylesheet.cssRules) {
//                    if (stylesheet.cssRules.hasOwnProperty(j)) {
//                        if (stylesheet.cssRules[j] &&
//                            stylesheet.cssRules[j].type === CSSRule.STYLE_RULE &&
//                            stylesheet.cssRules[j].selectorText.indexOf(".icon-") !== -1) {
//                            var regla = stylesheet.cssRules[j].selectorText.split(":")[0];
//                            iconos.push(regla.replace(".", ""));
//                        }
//                    }
//                }
//            }

//        } catch (err) {
//            error(err);
//        }
//        window.iconosHis = iconos;
//        return iconos;

//    }

//    //#endregion 

//    //#endregion
//    var forma = {
//        abrir: function (idAplicacion, callback) {

//            try {
//                _callback = callback;
//                onAbrirVentana(idAplicacion);
//            } catch (e) {
//                forma.error(e);
//            } 
            
//        },
//        error: function(e) {
//            console.log(e);
//        },
//        objeto: {
//            IdAplicacion:"IdAplicacion",
//            Nombre: "Nombre",
//            Descripcion: "Descripcion",
//            Icono:"Icono",
//        },
//        objetoVersion: {
//            IdVersion:"IdVersion",
//            Cambios: "Cambios",
//            Sistema: "Sistema",
//            Versiones: "Versiones",
//            IdAplicacion: "IdAplicacion",
//        },
//        catalogos: {

//        }
//    }
//    return forma;

//}




