var fb_api = "284475251905962";

$.ajaxSetup({ cache: true });
$.getScript('//connect.facebook.net/en_US/sdk.js', function () {
    FB.init({
        appId: fb_api,
        xfbml: true,
        version: 'v2.6'
    })
});

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function sharePost(shareText) {
    FB.ui({
        method: 'share',
        href: window.location.href,
        display: 'popup',
        quote: shareText,
    }, function (response) {
    });
}

function shareTweet(shareText) {
    var twitterUrl = "https://twitter.com/intent/tweet?text=" + shareText + "&url=" + window.location.href;
    var width = 600, height = 255, top = (window.innerHeight - height) / 2, left = (window.innerWidth - width) / 2;
    window.open(twitterUrl, "_blank", "width=" + width + ",height=" + height + ",top=" + top + ",left=" + left + ",scrollbars=no");
}