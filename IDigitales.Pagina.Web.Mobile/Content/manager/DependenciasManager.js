﻿
var His = His || {};
His.Manejadores = His.Manejadores || {};


/**
* Función clase para ordenar un conjunto de llaves tomando en cuenta la dependencia.
* Ejemplo, si a depende de b y de c, 
*/
His.Manejadores.DependenciasManager = function () {
   
    /**
    * Ordena un conjunto de ids por dependencia
    * @param keys {Array} El arreglo debe contener el conjunto de ids que se van a ordenar
    * @param depends {object} Es un objeto cuyas propiedades son los ids a ordenar, y el valor de la propiedad es un arreglo con las dependencias.
    *       Ejemplo: {
                        "a": ["b", "c"],  //el id a depende de los ids b y c
                        "b": ["c"]        //el id b depende del id c
                        }
    * @return {Array} Retorna un arreglo con las llaves ordenadas por dependencia.
    */
    this.ordenar = function (keys, depends) {
        var nodes = {},
            i,
            l;


        for (i = 0, l = keys.length; i < l; i += 1) {//TODO validar que no se repitan las llaves
            nodes[keys[i]] = new Nodo(keys[i]);
        }


        //agregar las dependencias
        for (var key in depends) {
            if (depends.hasOwnProperty(key)) {
                var dependencies = depends[key];

                if (dependencies instanceof Array) {
                    var node = nodes[key];

                    if (node) {
                        dependencies.forEach(function (dependency) {

                            if (!nodes[dependency])
                                throw "No existe la dependencia " + dependency;

                            node.depends.push(dependency);
                            var nodeDependency = nodes[dependency];
                            nodeDependency.incomingEdges = true;
                        });
                    }
                }
            }
        }


        var arregloOrdenado = _ordenar(keys, nodes);
        return arregloOrdenado;
    }

    this.ordenarJsCss = function (jsArray) {
        var keys = [],
            dependencias = {},
            hashJs = {},
            finalArray = jsArray,
            tieneDependencias = false;


        jsArray.forEach(function (script) {
            keys.push(script.key);
            dependencias[script.key] = script.dependencias;
            hashJs[script.key] = script;

            if (script.dependencias.length > 0)
                tieneDependencias = true;
        });

        if (tieneDependencias == true) {

            var llavesOrdenadas = this.ordenar(keys, dependencias);

            finalArray = [];
            llavesOrdenadas.forEach(function (key) {
                finalArray.push(hashJs[key]);
            });
        }

        return finalArray;
    };

    this.crearObjetosParaOrdenar = function (arrayJs) {
        var jsArray = [],
                Script = His.Manejadores.DependenciasManager.NodoOrdenable,
                js,
                i = 0,
                l = arrayJs.length,
                esOrdenable = false;

        for (i = 0; i < l; i += 1) {
            js = arrayJs[i];

            if (typeof js == "string") {
                jsArray.push(new Script(js, i));
            } else {
                esOrdenable = true;
                var newScript = new Script(js.src, js.key);

                if (newScript.key == undefined)
                    newScript.key = i;

                if (js.dependencias != undefined)
                    newScript.dependencias = js.dependencias;

                jsArray.push(newScript);
            }
        }

        return { esOrdenable: esOrdenable , nodos: jsArray};
    }

    var Nodo = function (key) {
        this.incomingEdges = false;
        this.key = key;
        this.depends = [];

        this.addDependency = function (dependKey) {
            this.depends.push(dependKey);
        }
    }

    var _ordenar = function (keys, nodos) {
        if (keys.length <= 0) return [];

        var i, l;
        var nodesNoincomingEdges = [];

        for (i = 0, l = keys.length; i < l; i += 1) {
            var nodo = nodos[keys[i]];

            if (nodo.incomingEdges == false)
                nodesNoincomingEdges.push(nodo);

        }

        var array = [];
        var visited = {};

        for (i = 0, l = nodesNoincomingEdges.length; i < l; i += 1) {
            var nodeStart = nodesNoincomingEdges[i];
            _ordenarRecursivo(nodeStart, nodos, array, visited);
        }


        return array;

    }

    //Añade a array ordenado, los nodos de abajo hacia arriba, empezando por las hojas 
    var _ordenarRecursivo = function (nodo, nodos, arrayOrdenado, visited) {

        if (visited[nodo.key]) {
            throw "Depencia circular sin resolver en " + nodo.key;
        } else {
            visited[nodo.key] = true;
        }

        for (var i = 0, l = nodo.depends.length; i < l; i += 1) {
            var nodoVecinoKey = nodo.depends[i];

            if (nodos[nodoVecinoKey]) {//si sigue en el grafo
                _ordenarRecursivo(nodos[nodoVecinoKey], nodos, arrayOrdenado, visited);
            }
        }

        //si llega aqui es porque todas las dependencias del nodo, ya estan en el array, entonces puedo meter con confianza este nodo al arreglo

        arrayOrdenado.push(nodo.key);
        nodos[nodo.key] = null;

    }

}

His.Manejadores.DependenciasManager.NodoOrdenable = function (source, key) {
    this.src = source;
    this.key = key;
    this.dependencias = [];
}