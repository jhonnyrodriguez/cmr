﻿var LayoutManager = {
    //#region ". Variables privadas "
    htmlSpin: '<div class="centrado spin"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>',
    _navegador: '@Request.Browser.Browser',
    _moduloActual: null,
    _callBacksResize: [],
    _menuColapsado: false,
    _pendientes: 0,
    _bodySize: { height: 0, width: 0 },
    _timeOutResize: undefined,
    optionsModals: { maximizable: false, resizable: false, padding: false, transition: "fade", overflow: false, modal: true, closable: true, closableByDimmer: false },
    optionsModalsPin: { maximizable: false, resizable: false, padding: false, transition: "fade", overflow: false, modal: false, closable: true, closableByDimmer: false, pinnable:true },
    listaCamposRequeridos:null,
    pin: null,
   _nombreHospital:"",
    //#endregion

    //#region ". Métodos Publicos "

    encuentraZIndex: function() {
        try {
            var index_highest = 0;
            $('div').each(function() {
                var index_current = parseInt($(this).css("z-index"), 10);
                if (index_current > index_highest) {
                    index_highest = index_current;
                }
            });
            return index_highest;
        } catch(e) {
            _onError(e);
        }
    },

    calculaLayout: function() {
        //debugger;
        clearTimeout(LayoutManager._timeOutResize);
        LayoutManager._timeOutResize = setTimeout(function() {
            try {
                console.log("calculando");
                $(".fullWapper").css("height", "100%");
                LayoutManager._bodySize.width = false; // si se requiere hay que calcularlo
                LayoutManager._bodySize.height = $(".fullWapper").height();

                $(".fullWapper").css("height", "");
                //$(window).off("resize", LayoutManager.calculaLayout);
                LayoutManager.recalculaContenedorPrincipal();
                LayoutManager.notificaWindowResize();
                //$(window).on("resize", LayoutManager.calculaLayout);
            } catch(e) {
                console.log(e.message);
            }
        }, 100);
    },
    cambiaMenu: function(colapsarMenu) {
        this._menuColapsado = colapsarMenu;
        this.recalculaContenedorPrincipal();
        this.notificaWindowResize();
    },
    recalculaContenedorPrincipal: function() {
        try {
            //debugger;
            // Calcula y asigna dimensiones  del cuerpo
            //var rectanguloEncabezado = document.getElementById("ContenedorEncabezado").getBoundingClientRect();
            //$("#ContenedorCuerpo").css('height', 0);
            //var rectanguloPaddingCuerpo = document.getElementById("ContenedorCuerpo").getBoundingClientRect(); //se usa para considerar el padding
            //$("#ContenedorCuerpo").css('top', rectanguloEncabezado.height);
            //$("#ContenedorCuerpo").css('left', 0);
            //$("#ContenedorCuerpo").height($(window).height() - rectanguloEncabezado.height - rectanguloPaddingCuerpo.height);

            // Dimensiona menu modulo
            /////////////// se cambio para el tamaño en CSS
            //if (this._menuColapsado == true) {
            //    $("#ContenedorMenuModulo").width(LayoutManager.remtoPx(70));
            //}
            //else {
            //    $("#ContenedorMenuModulo").width(LayoutManager.remtoPx(200));
            //}
            //$("#ContenedorMenuModulo").css('top', rectanguloEncabezado.height);
            //$("#ContenedorMenuModulo").css('left', 0);
            //$("#ContenedorMenuModulo").height($(window).height() - rectanguloEncabezado.height);

            // Dimensiona contenedor informacion

            //var rectanguloMenu = document.getElementById("ContenedorMenuModulo").getBoundingClientRect();
            //$("#ContenedorSecciones").css('height', 0);
            //$("#ContenedorSecciones").css('width', 0);
            //var rectanguloInformacion = document.getElementById("ContenedorSecciones").getBoundingClientRect();
            //$("#ContenedorSecciones").width($(window).width() - rectanguloMenu.width - rectanguloInformacion.width - 1);
            //$("#ContenedorSecciones").height($(window).height() - rectanguloEncabezado.height - rectanguloInformacion.height);
            //var rectanguloInfo = document.getElementById("ContenedorSecciones").getBoundingClientRect();

        } catch(e) {
            console.log(e.message);
        }
    },
    pxtoRem: function(length) {
        var resultado = 0;
        try {
            var fontSize = parseFloat($("html").css("font-size"));
            resultado = (parseFloat(length) / fontSize);
        } catch(e) {
            console.log(e.message);
        }
        return resultado;
    },
    remtoPx: function(length) {
        var resultado = 0;
        try {
            var fontSize = parseFloat($("html").css("font-size"));
            resultado = (parseFloat(length) * fontSize);
        } catch(e) {
            console.log(e.message);
        }
        return resultado;
    },
    registraClienteNotificarWindowResize: function(callBack) {
        try {
            if (callBack != null) {
                if (typeof callBack === 'function') {
                    if (LayoutManager._callBacksResize.indexOf(callBack) == -1) {
                        LayoutManager._callBacksResize.push(callBack);
                    }
                }
            }
        } catch(e) {
            console.log(e.message);
        }
    },
    desRegistraClienteNotificarWindowResize: function(callBack) {
        try {
            if (callBack != null) {
                if (typeof callBack === 'function') {
                    var index = LayoutManager._callBacksResize.indexOf(callBack);
                    if (index > -1) {
                        LayoutManager._callBacksResize.splice(index, 1);
                    }
                }
            }
        } catch(e) {
            console.log(e.message);
        }
    },
    notificaWindowResize: function() {
        try {
            LayoutManager._callBacksResize.forEach(function(callback) {
                //debugger;
                if (callback != null) {
                    //var rectanguloPaddingCuerpo = document.getElementById("ContenedorCuerpo").getBoundingClientRect()
                    callback(LayoutManager._bodySize);
                }
            });
        } catch(e) {
            console.log(e.message);
        }
    },
    mostrarSpin: function(res) {

        this._pendientes += res;

        if (this._pendientes <= 0) {
            setTimeout(function() {
                LayoutManager.loadSpin('off');
                this._pendientes = 0;
            }, 500);
        } else {
            this.loadSpin('on');
        }

    },
    loadSpin: function(estado) {
        if (estado == "on") {
            $('#spinnerEsc').show();
        } else {
            $('#spinnerEsc').hide();
        }
    },
    creaTooltipMenu: function(idBoton, mensajeHtml, ancho) {
        $("#" + idBoton).hovercard({ detailsHTML: mensajeHtml, width: ancho });
    },
    bloquearUI: function() {
        try {
            var panelBloqueo = $("#ContenedorBloqueoUI");
            panelBloqueo.show();
            var maxZIndex = this.encuentraZIndex() + 1;
            panelBloqueo.css("z-index", maxZIndex);


            //$('#spinnerEsc').css("z-index", panelBloqueo.css("z-index") + 1);
            //$('#spinnerEsc').show();
        } catch(e) {
            throw (e);
        }
    },
    desbloquearUI: function() {
        try {
            var panelBloqueo = $("#ContenedorBloqueoUI");

            //$('#spinnerEsc').hide();
            panelBloqueo.hide();
        } catch(e) {
            throw (e);
        }
    },

    //#region ". Métodos Publicos "
     initAlertify: function() {
        alertify.genericAlert || alertify.dialog('genericAlert', function() {
            return {
                main: function(content, title) {

                    this.setContent(content);
                    if (title != null && title != '')
                        this.setHeader(title);
                    //Centrar Modal
                    var hd = this.elements.body.clientHeight;
                    var wd = this.elements.body.clientWidth;
                    var hs = $(window).height();
                    var ws = $(window).width();
                    var posh = hd >= hs ? hs / 2 - 30 : ((hs - hd) / 2) - 67;
                    var posw = ((ws - wd) / 2);
                    this.moveTo(posw, posh);

                },
                setup: function() {
                    return {
                        options: {
                            maximizable: false,
                            resizable: false,
                            padding: false,
                            transition: 'fade',
                            overflow: false,
                            modal: true,
                            closable: true,
                            closableByDimmer: false
                        },

                        buttons: [
                            {
                                text: 'Aceptar', /* button label defaul */
                                key: 27, /*bind a keyboard key to the button */
                                invokeOnClose: true, /* indicate if closing the dialog should trigger this button action */
                                className: alertify.defaults.theme.ok, /* custom button class name  */
                                attrs: { attribute: 'value' }, /* custom button attributes  */
                                scope: 'auxiliary', /* Defines the button scope, either primary (default) or auxiliary */
                                element: undefined /* The will conatin the button DOMElement once buttons are created */
                            }
                        ]
                    };
                }
            };
        });
    },
    abrirMensaje: function(titulo, mensaje, callBack) {
        alertify.genericAlert(mensaje).set({ 'title': titulo, "onclose": callBack });
    },

    abrirPopUp: function(idDialog, selectorContenido, titulo, callbackOpen, styleProperties) {
        alertify[idDialog] || alertify.dialog(idDialog, function() {
            return {
                main: function(content) { this.setContent(content); },
                setup: function() { return { options: LayoutManager.optionsModals }; },
                prepare: function() { /* this.elements.footer.style.visibility = "collapse";*/
                },
                build: function() {
                    this.elements.footer.style.minHeight = "0";
                    this.elements.content.style.bottom = "0";

                    if (styleProperties) {
                        var style = this.elements.dialog.style;

                        for (var styleProp in styleProperties) {
                            if (styleProperties.hasOwnProperty(styleProp)) {
                                style[styleProp] = styleProperties[styleProp];
                            }
                        }

                    }
                },
                hooks: {
                    onshow: callbackOpen,
                    onclose: function() {
                    },
                    onupdate: function() {
                    }
                }
            };
        });
        // funcion($(selectorContenido).removeClass('hide')[0]).set('title', titulo);
        alertify[idDialog]($(selectorContenido).removeClass("hide")[0]).set("title", titulo);
    },
    moverModalTop: function (idDialog) {
        try {
            var ajsDia = $("#" + idDialog).parents(".ajs-dialog");
            var t = ($("#" + idDialog).parents(".ajs-modal").height() - $("#" + idDialog).parents(".ajs-dialog").height()) / 2;
            ajsDia.animate({ top: t }, 800);
        } catch (err) {
            error(err);
        }
    },
    abrirPopUpAjax: function (idDialog, selectorContenido, urlContenido, data, callbackOpen, dialogStyle, callbackSuccess, type, callbackClose) {
        
       // $(selectorContenido).html($("#spinnerEsc").show().get(0).outerHTML);
        $(selectorContenido).html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
        alertify[idDialog] || alertify.dialog(idDialog, function () {
            return {
                main: function(content) { this.setContent(content); },
                setup: function () { return { options: { ignore_dragging: ".idigitales-contenedo-boton-modal,.idigitales-boton-rectangular,i", maximizable: false, resizable: false, padding: false, transition: "fade", overflow: false, modal: true, closable: true, closableByDimmer: false } }; },
                prepare: function() { this.elements.footer.style.visibility = "collapse"; },
                build: function () {
                    this.elements.footer.style.minHeight = "0";
                    this.elements.footer.style.height = "0";
                    this.elements.content.style.bottom = "0";
                    this.elements.content.style.overflow = "visible";

                    if (dialogStyle) {
                        if (dialogStyle instanceof Object) {
                            if (dialogStyle) {
                               
                                if (dialogStyle instanceof Object) {
                                    var dialog = this.elements.dialog.style;
                                    for (var style in dialogStyle) {
                                        if (dialogStyle.hasOwnProperty(style)) {
                                            if (style === "_class")
                                                $(this.elements.dialog).addClass(dialogStyle[style]);
                                            else
                                                dialog[style] = dialogStyle[style];
                                        }
                                    }
                                } else {
                                    var minWidthrem = dialogStyle; //por default es el min del dialog
                                    this.elements.dialog.style.minWidth = minWidthrem + "rem";
                                }

                            }
                        } else {
                            var minWidthrem = dialogStyle;//por default es el min del dialog
                            this.elements.dialog.style.minWidth = minWidthrem + "rem";
                        }

                    }
                },
                hooks: {
                    onshow: function () {
                        if (callbackOpen)callbackOpen();
                    },
                    onclose: callbackClose,
                    onupdate: function() {
                    }
                }
            };
        });

        var url = urlContenido;
        // funcion($(selectorContenido).removeClass('hide')[0]).set('title', titulo);
        $.ajax({
            async: true,
            type: type == undefined ? "get" : type,
            url: url,
            data: data,
            dataType: "html",
            //beforeSend: antes,
            //complete: completo,
            success: function (data) {
                $(selectorContenido).html(data);

                var titulo = "Cargando....";
                if ($(selectorContenido + " #titulo").length > 0)
                    titulo = $(selectorContenido + " #titulo").text();
                else if (($(selectorContenido + " .titulo").length > 0))//se lo puse para no repetir ids #titulo
                    titulo = $(selectorContenido + " .titulo").text();
                alertify[idDialog]().set('title', titulo);

                LayoutManager.moverModalTop(idDialog);
         
                if (callbackSuccess && typeof callbackSuccess === "function") callbackSuccess();
            }
            //error: this._onError
        });
        alertify[idDialog]($(selectorContenido).removeClass("hide")[0]).set("title", "Cargando....");
    },

    AbrePopUpExpediente: function (url, idPaciente) {
        //debugger;
        alertify.genericDialogExpediente || alertify.dialog('genericDialogExpediente', function () {
            return {
                main: function (content) {

                    this.setContent(content);
                },
                setup: function () {
                    return {
                        options: {
                            //disable both padding and overflow control.
                            ignore_dragging: "li>a, .iconmensup, .elementoCircular ",
                            padding: false,
                            overflow: true,
                            transition: 'zoom',
                            startMaximized: true,
                            frameless: false,
                            resizable:true,
                            title: "Expediente",
                            closableByDimmer: false,

                            //onmaximized:onResizeExp, 
                            //onrestored:onResizeExp,
                            //onresized: onResizeExpAjax,
                            //onshow: onResizeExpAjax
                        }
                    };
                },
                prepare: function () {
                    this.elements.footer.style.visibility = "collapse";
                    
                },
                build: function () {
                    this.elements.dialog.style.minWidth = "600rem";
                    this.elements.dialog.style.height = "550rem";
                    this.elements.header.style.padding = "0";
                    this.elements.header.style.zIndex = "10";
                    this.elements.commands.container.style.zIndex = "11";
                     
                    //this.elements.content.style.backgroundColor = "#f1f2f2";
                    $(this.elements.dialog).addClass("ExpedienteModal");
                     
                    // quitar footer
                    this.elements.footer.style.minHeight = "0";
                    this.elements.content.style.bottom = "0";
                    this.elements.content.style.zIndex = "1";
                    
                    var reportePdf=LayoutManager.obtenerRootApp("/Reporte/VisorPdf")+"?nombre=ECE&idp="+ idPaciente;

                    var headerB = '<div id="buttonsMenuPrincipalExp" class="botonesBloqueados">' +
'        <div id="lockDiv"></div>' +
'        <ul id="menuPrincipal">' +
'            <li>' +
'                <a id="btnDefaultWidgets" onclick="CargarDashboardServer()">' +
'                    <div class="userimageinfo_hl elementoCircular">' +
'                        <i class="icon-paneles-basedatos iconmensup repBtn"></i>' +
'                    </div>' +
'                </a>' +
'            </li>' +
'            <li>' +
'                <a id="btnReloadWidgets" onclick="CargarDashboardContent()">' +
'                    <div class="userimageinfo_hl elementoCircular">' +
'                        <i class="icon-paneles-actualiza iconmensup repBtn"></i>' +
'                    </div>' +
'                </a>' +
'            </li>' +
'            <li>' +
'                <a id="btImprimir" href="'+reportePdf+'">' +
'                    <div class="userimageinfo_hl elementoCircular">' +
'                        <i class="icon-reportes iconmensup repBtn"></i>' +
'                    </div>' +
'                </a>' +
'            </li>' +
'        </ul>' +
'    </div>';
                    this.setHeader(headerB);
                },
                settings: {
                    refrescar: function () { }
                }
            };
        });
//debugger;
var ExpLoad = $('<div id="ExpedienteLoad">' +
            '<div id="spinnerEsc" style="display: block;"> <div class="spinnerImg"></div> <div class="mensajeSpinner">CARGANDO</div> </div>'+
    '</div>');
alertify.genericDialogExpediente(ExpLoad[0]);
ExpLoad.load(url, {}, function () { inicializarExpediente(alertify.genericDialogExpediente) });
},

    abrirPopUpFrame: function(idDialog, url, titulo, onloadFrame) {
        var iframe;
        url=LayoutManager.obtenerRootApp(url);
        alertify[idDialog] || alertify.dialog(idDialog, function() {
            return {
                main: function(url) {
                    return this.set({ 'url': url });
                },
                // we only want to override two options (padding and overflow).
                setup: function() {
                    return {
                        options: {
                            //disable both padding and overflow control.
                            padding: false,
                            overflow: false,
                            transition: 'zoom',
                            title: titulo
                        }
                    };
                },
                // This will be called once the DOM is ready and will never be invoked again.
                // Here we create the iframe to embed the video.
                build: function() {
                    // create the iframe element
                    iframe = document.createElement('iframe');
                    iframe.frameBorder = "no";
                    iframe.width = "100%";
                    iframe.height = "100%";
                    iframe.id = "iframe" + idDialog;
                    iframe.onload = function() {
                        // debugger;
                        if (onloadFrame) onloadFrame();
                        //this.style.display ='block';
                    }
                    this.elements.content.appendChild(iframe);
                    this.elements.body.style.minHeight = screen.height * .5 + 'rem';
                    this.elements.dialog.style.maxWidth = screen.width * .4 + 'rem';
                    // iframe.style.display = 'none';
                },
                hooks: {
                    // triggered when the dialog is shown, this is seperate from user defined onshow
                    onshow: function() {
                        // this.elements.dialog.style.display = 'block';
                        //debugger;
                        //$(this.elements.content.getElementsByTagName("iframe")).show();
                    }
                },
                settings: {
                    url: undefined
                },
                settingUpdated: function(key, oldValue, newValue) {
                    switch (key) {
                    case 'url':
                        iframe.src = newValue;
                        break;
                    }
                }
            };

        });

        //alertify($(selectorContenido).removeClass('hide')[0]).set('title', titulo);
        alertify[idDialog](url).set({ frameless: false });
    },

    abrirConfirmacion: function(titulo, mensaje, onOk, onCancel, lblAceptar, lblCancelar) {
        alertify.confirm(titulo, mensaje, onOk, onCancel);
        alertify.confirm(mensaje).set({
            'title': titulo,
            'labels': { ok: lblAceptar, cancel: lblCancelar },
            'onok': onOk,
            'oncancel': onCancel,
        });
    },
    

    abrirPopUpBusqueda: function (funcion, idDialog, selectorContenido, titulo, callbackOpen, callbackClose, functionPin, minHeight, maxHeight) {
        alertify[idDialog] || alertify.dialog(idDialog, function() {
            return {
                main: function(content) { this.setContent(content); },
                setup: function() { return { options: LayoutManager.optionsModalsPin }; },
                prepare: function() { /* this.elements.footer.style.visibility = "collapse";*/
                },
                build: function() {
                    this.elements.footer.style.minHeight = "0";
                    this.elements.content.style.bottom = "0";
                    this.elements.body.style.overflow = "visible";
                    this.elements.content.style.overflow = "visible";
                    this.elements.commands.pin.onclick = functionPin;
                    if (minHeight != null) this.elements.body.style.minHeight = minHeight + "rem";
                    if (maxHeight != null) this.elements.body.style.maxHeight = maxHeight + "rem";
                    
                },
                hooks: {
                    onshow: callbackOpen,
                    onclose: callbackClose,
                    onupdate: function() { }
                }
            };
        });
        // funcion($(selectorContenido).removeClass('hide')[0]).set('title', titulo);
        alertify[idDialog]($(selectorContenido).removeClass("hide")[0]).set("title", titulo);
    },
    mostrarPortadaCarga: function() {
        $("html").css("overflow", "hidden");
        $("#portada").show();
        $('#portadaContenedor').show();
        $("#portadaSplash").addClass("portadaSplash-load");
        $('#portada-modulo').textillate({
            loop: true, autoStart: false,
            in: { effect: 'flipInX', sync: true, delay:1},
            out: { effect: 'flipOutX', sync: true, delay: 1 }
           
        });

        $('#portada-modulo').textillate('start');
          
        this.portadaHeartBeat.iniciar();
       
        this.ocultarContenido();
    },

    ocultarPortadaCarga: function(onfinish) {
        var self = this;

        this.portadaHeartBeat.detener();
        
        $('#portada-modulo').textillate('stop');
      

        $("#portadaSplash").removeClass("portadaSplash-loading");
        $("#portadaSplash").addClass("portadaSplash-loaded");

        setTimeout(function() {
            $("#portadaSplash").stop(true);
            //("#LoadingSplash").animate({ "left": "140%" }, 300);
            $("#portada").hide();
            $("html").css("overflow", "")
            self.mostrarContenido();
            if (onfinish && onfinish instanceof Function)
                onfinish();
        }, 400);
    },

    mostrarContenido: function() {
        //$('#contenedorBody').show();
    },

    ocultarContenido: function() {
        // $('#contenedorBody').hide();
    },

    mostrarBotonesBarra: function(clase) {
        $('.menuOpc').hide();
        if (clase) $('.' + clase).show();
    },
   
    obtenerRootApp: function(url) {
        var root = "/";
        var path = window.location.pathname;
        var indices = [];
        for (var i = 0; i < path.length; i++) {
            if (path[i] === "/") indices.push(i);
        }
        if (indices.length > 2) {
            root = path.substring(indices[0], indices[1]);
        }

        return window.location.protocol + "//" + window.location.host + root + (url || "");
    },
    obtenerRoot: function (url) {
        var root = "/";
        var path = window.location.pathname;
        var indices = [];
        for (var i = 0; i < path.length; i++) {
            if (path[i] === "/") indices.push(i);
        }
        if (indices.length > 2) {
            root = path.substring(indices[0], indices[1]);
        }

        if (url.indexOf(root) !== -1 && root != '/')
            return url;
        return root == "/" ? url : root + (url || "");
    },

    pacienteRadialProgress: function(nombrecontrol, idPaciente, img) {
        var url = LayoutManager.obtenerRootApp("/Paciente/CalcularPorcentajePerfilPaciente"); //'@Url.Action("CalcularPorcentajePerfilPaciente", "Paciente")';
        var urlBase = LayoutManager.obtenerRootApp();
        var radialprogress = $(nombrecontrol).RadialProgress();
        $.post(url, { idPaciente: idPaciente }, function(data) {
            var path = "";
            if (img)
                path = img;
            else {
                if (data.datos != undefined && data.datos.Imagen != undefined && data.datos.Imagen.indexOf("data:image/") > -1) {
                    path = data.datos.Imagen;
                } else {
                    path = urlBase + "/" +data.datos.Imagen;
                }
            }
            radialprogress[0] && radialprogress[0].SetProgress(data.datos.Porcentaje, path, false);
            if (!radialprogress[0]) {
                //console.error("fix radialprogress[0] ");
            }
        });
    },

    agregarCamposRequeridos : function(lista) {
        if (!window.msgcamporequerido) window.camporequerido = "Campo Requerido";
        //listaCamposRequeridos = lista;
        $.each(lista, function (index, campo) {
            if (campo.Tipo === "grupo") { //Conjunto de datos el nombre del campo debe de ser unna clase
                $('.' + campo.NombreCampo).each(function () {
                    LayoutManager.campoRequerido($(this));
                });
            } else if (campo.Tipo === 'multi') { //Conjunto de campos en el cual se va repetir el nombre y el inicio es igual por ejemplo campo1 campo2
           
                $('input[id^=' + campo.NombreCampo + ']').each(function () {
                    LayoutManager.campoRequerido($(this));
                });
                $('textarea[id^=' + campo.NombreCampo + ']').each(function () {
                    LayoutManager.campoRequerido($(this));
                });
                $('select[id^=' + campo.NombreCampo + ']').each(function () {
                    LayoutManager.campoRequerido($(this));
                });
            } else {
                LayoutManager.campoRequerido($('#' + campo.NombreCampo));
            }
            // console.log(campo.NombreCampo);
        });
    },

    portadaHeartBeat: (function () {
        var svgpath = null;
        var dashoffset = 2000;
        var increment = 5;
        var stop = null;

        var inicioOffset = -2000;
        var finOffset = 2000;
        var increment = 5;

        //var inicioOffset = 0;
        //var finOffset = 2000;;
        var increment = 5;

        var _iniciarPintado = function () {
            if (stop)
                return;            

            setTimeout(function () {
                if (dashoffset < inicioOffset)
                    dashoffset = finOffset;
                dashoffset -= increment;
                svgpath.style.strokeDashoffset = dashoffset;
                window.requestAnimationFrame(_iniciarPintado);
            }, 10);
        }

        return {
          
            iniciar: function () {
                if (stop === false)
                    return;//ya esta corriendo
                dashoffset = finOffset;
                stop = false;
                svgpath = document.getElementById('play-heart-beat');
                _iniciarPintado();
            },

            detener: function () {
                stop = true;
            }
        };
    }()),

    campoRequerido:function(campo) {
        campo.prop('required', true).data("msg", window.msgcamporequerido);
        // console.log(campo);
    },
    inicializaPin: function (idUsuario, urlsPin, txtPin) {
        this.pin = His.Controles.Pin(idUsuario, urlsPin, txtPin);
    },
    obtieneNombreHospital: function (callBack) {
        var url = LayoutManager.obtenerRootApp("/Account/ObtenerNombreHospital");
        if (this._nombreHospital == "") {
            $.ajax({
                async: true,
                type: "get",
                url: url,
                data: {},
                dataType: "json",
                success: function (data) {
                    this._nombreHospital = data;
                    if (callBack)
                        callBack(this._nombreHospital);

                }
            });
        } else {
            if (callBack)
                callBack(this._nombreHospital);
        }
    },
    //TODO: limpiar codigo de arriba
    /**************** 
        NUEVOS 
    **************/

    CrearTabs : function() {

        var tabs = undefined;

        function checkScrolling(_tabs) {
            var totalTabWidth = parseInt(_tabs.children('.cd-tabs-navigation').width()),
                tabsViewport = parseInt(_tabs.width());
            if (_tabs.scrollLeft() >= totalTabWidth - tabsViewport) {
                _tabs.parent('.cd-tabs').addClass('is-ended');
            } else {
                _tabs.parent('.cd-tabs').removeClass('is-ended');
            }
        }

        var functionTabs = {
            _destroy: function() {
                //$(window).unbind('resize');

                tabs.each(function() {
                    var tab = $(this),
                        tabItems = tab.find('ul.cd-tabs-navigation'),
                        tabNavigation = tab.find('nav');

                    tabItems.unbind();

                    tabNavigation.unbind();
                });
                tabs = undefined;
                return undefined;
            },

            _inicializar: function() {
                tabs = $('.cd-tabs');
                tabs.each(function() {
                    var tab = $(this),
                        tabItems = tab.find('ul.cd-tabs-navigation'),
                        tabContentWrapper = tab.children('ul.cd-tabs-content'),
                        tabNavigation = tab.find('nav');

                    tabItems.on('click', 'a', function(event) {
                        event.preventDefault();
                        var selectedItem = $(this);
                        if (!selectedItem.hasClass('selected')) {
                            var selectedTab = selectedItem.data('content'),
                                selectedContent = tabContentWrapper.find('li[data-content="' + selectedTab + '"]'),
                                slectedContentHeight = selectedContent.innerHeight();

                            tabItems.find('a.selected').removeClass('selected');
                            selectedItem.addClass('selected');
                            selectedContent.addClass('selected').siblings('li').removeClass('selected');
                            //animate tabContentWrapper height when content changes 
                            //tabContentWrapper.animate({
                            //    'height': slectedContentHeight
                            //}, 200);
                        }
                    });

                    //hide the .cd-tabs::after element when tabbed navigation has scrolled to the end (mobile version)
                    checkScrolling(tabNavigation);
                    tabNavigation.on('scroll', function() {
                        checkScrolling($(this));
                    });
                });

                //$(window).on('resize', function() {
                //    tabs.each(function() {
                //        var tab = $(this);
                //        checkScrolling(tab.find('nav'));
                //        tab.find('.cd-tabs-content').css('height', 'auto');
                //    });
                //});

            }
        }

        return functionTabs;
    }

};

var PaginaWeb = PaginaWeb || {};
PaginaWeb.Utils = PaginaWeb.Utils || function (contexto, baseUrl) {
    var ctx = contexto;
    var urlBase = baseUrl;
    var _key = "cmrservice05";

    //#region Funciones Privadas

    function mostrarSpin(estado) {
        if (estado === 1) {
            $('#spinnerEsc').show();
        } else {
            $('#spinnerEsc').hide();
        }
    }

    //Funciones utilizadas para ocultar campos configuracion de vistas 
    //#region Configurar Vista


    function ajustarRenglones(contenedor) {

        var renglones = contenedor.children("[class^='idigitales-renglon']:visible");

        if (renglones.length === 0)
            return;

        var rOrigen, rDestino;
        renglones.each(function (index) {
            rOrigen = $(this);
            ajustarRenglon(rOrigen);

            //ReordenarEntreRenglones
            if (rOrigen.css('display') !== 'none') {
                if (rOrigen && rDestino) {
                    reordenarColumnas(rOrigen, rDestino);
                }

                //si es ultimo renglon ajustarlo tambien
                if (rOrigen && index === renglones.length - 1) {
                    reajustarColumnas(rOrigen);
                }

                rDestino = rOrigen;
            }
            //Si se quiere verificar al final el renglon por si se muevenlos elementos a otro renglon
            ocultarRenglonSinElementosVisibles(rOrigen);
        });

        //ocultar fieldset si esta vacio
        ocultarFieldsetSinElementosVisibles(contenedor);
    }

    function ajustarRenglon(renglon) {
        //Buscar las columnas que se ocultaron dinamicamente
        //var elementosOcultos=renglon.children("[class^='idigitales-col']:hidden");
        var elementosOcultos = renglon.children(".idigitales-col-oculta:hidden");
        //Buscar las columnas visibles
        var elementosVisibles = renglon.children("[class^='idigitales-col']:visible");

        //Si no hay elementos que se ocultaron omitir
        if (elementosOcultos.length > 0 && elementosVisibles.length > 0) {
            //Obtener numero de espacios para ajustar
            if (renglon.children("[class^='idigitales-col-por']:visible").length > 0) {

            } else {
                //var ocultas = obtenerNumeroColumnas(elementosOcultos);
                ////distribuir el espacio de columnas entre las columnas visibles
                //distribuirCol(elementosVisibles, ocultas);
            }
        } else if (elementosVisibles.length > 0) {

            elementosVisibles.each(function () {
                var contenedor = $(this);
                if (contenedor.children("[class^='idigitales-renglon']:visible").length > 0) {
                    ajustarRenglones($(this));
                } else if (contenedor.children("[class^='idigitales-col']").length > 0) {
                    ajustarRenglon($(this));
                } else {
                    //contenedor.hide();
                }
            });
        }
        //Ocultar renglones sin elementos visibles
        ocultarRenglonSinElementosVisibles(renglon);
    }

    function obtenerNumeroColumnas(elementos) {
        //calcular el numero de columnas disponibles
        //por los elementos ocultos
        var col = 0;

        elementos.each(function () {
            var classes = this.classList;
            classes.forEach(function (value) {
                col += obtenerNumeroCol(value);
            });

        });
        return col;
    }

    function obtenerNumeroCol(elemento) {

        var col = 0;
        if (elemento && elemento.indexOf("idigitales-col-oculta") === -1 && elemento.indexOf("idigitales-col-") > -1) {
            var num = elemento.replace("idigitales-col-", "");
            col = parseInt(num);
        }
        return col;
    }

    function reordenarColumnas(rorigen, rdestino) {

        var elementosVisibles = rdestino.children("[class^='idigitales-col']:visible");
        //si hay elementos visibles en el renglon dstino
        if (elementosVisibles.length > 0) {
            //obtener el espacio disponible
            var elementosOcultos = rdestino.children(".idigitales-col-oculta:hidden");
            if (elementosOcultos.length > 0) {
                var espacios = obtenerNumeroColumnas(elementosOcultos);
                var temp = espacios;
                while (espacios > 0 && temp > 0) {
                    var colAMover = rorigen.children(".idigitales-col-" + temp + ":visible");
                    if (colAMover.length > 0 && espacios >= temp) {
                        rdestino.append(colAMover.first());
                        espacios = espacios - temp;
                    } else {
                        temp--;
                    }
                }
                //Despues de mover ajustar el tamaño que sobro
                if (espacios > 0) {
                    reajustarColumnas(rdestino, espacios);
                }
            }
        }

    }

    function reajustarColumnas(contenedor, espacios) {

        if (!espacios) {
            var elementosOcultos = contenedor.children("[class^='idigitales-col']:visible");//.children(".idigitales-col-oculta:hidden");
            if (elementosOcultos.length > 0) {
                espacios = obtenerNumeroColumnas(elementosOcultos);
                espacios = 12 - espacios;
            }
        }
        if (espacios > 0) {
            //buscar elementos a ajustar excepto los de 1 col
            var colVisibles = contenedor.children("[class^='idigitales-col']:visible").not('.idigitales-col-1');
            distribuirCol(colVisibles, espacios);
        }

    }

    function distribuirCol(elementos, total) {
        //dividir el tamano de columnas entre los elementos visibles
        while (total > 0) {
            if (elementos.length === 0) {
                total = 0;
                return;
            }

            elementos.each(function () {
                var celda = $(this);
                //ajustar renglones anidados 
                ajustarRenglones(celda);

                var borrar;
                var numNuevo = 0;
                var classes = this.classList;
                if (total > 0) {//Si aun quedan columnas
                    classes.forEach(function (value) {
                        if (value.indexOf("idigitales-col-oculta") === -1 && value.indexOf("idigitales-col-") > -1) {
                            borrar = value;
                            var num = value.replace("idigitales-col-", "");
                            numNuevo = parseInt(num);
                        }

                    });

                    //Crear nueva clase
                    if (numNuevo > 0) {
                        if (borrar)
                            this.classList.remove(borrar);
                        numNuevo++;
                        total--;
                        this.classList.add("idigitales-col-" + numNuevo);
                    }
                }

            });
        }
    }


    //Ocultar renglones sin elementos visibles
    function ocultarRenglonSinElementosVisibles(renglon) {
        var columnasVisibles = renglon.children("[class*='idigitales-col']:visible").not('.mostrar-input-visible');
        if (columnasVisibles.length === 0)
            renglon.hide();
    }

    //Ocultar fieldset o contenedor de renglones sin elementos visibles
    function ocultarFieldsetSinElementosVisibles(contenedor) {
        var elementosVisibles = contenedor.find("[class^='idigitales-renglon']:visible");
        if (elementosVisibles.length === 0) {
            contenedor.hide();
            contenedor.addClass("idigitales-renglon-oculto");
        }
    }


    //#endregion

    function iniMoneda() {

        (function ($) {

            $.fn.monedaFormatNumber = function (options) {

                // Default options.
                var settings = $.extend({
                    // These are the defaults.
                    decimal_separator: ",",
                    number_separator: ".",
                    currency: "$",
                    to_fixed: 2
                }, options);

                // Format
                var el = $(this);
                var val = el.val() || "0.0";
                el.data("currency", settings.currency);
                el.data("separador", settings.decimal_separator);
                el.val(formatNumber(val.replace(settings.decimal_separator, settings.number_separator), settings.currency));

                el.off("change", onChange).on("change", onChange);

                function onChange(event) {
                    var val = $(this).val();
                    $(this).val(formatNumber(val, settings.currency));
                }

                // Functions
                function formatNumber(number, currency) {
                    number = number.replace(currency, '');
                    number = number.replace(settings.decimal_separator, "");
                    var resultado = new Intl.NumberFormat(ObtenerLenguaje()).format(number);
                    //number = number.replace(currency, '');
                    //number = number.replace(settings.decimal_separator, "");
                    //number = number.replace(settings.number_separator, settings.decimal_separator);
                    //number = Number(number.trim());
                    //var resultado = number.toFixed(settings.to_fixed);
                    //resultado = resultado.replace(/(\d)(?=(\d{3})+\,)/g, '$1' + settings.number_separator);
                    //resultado = resultado.replace(settings.decimal_separator, settings.number_separator);
                    //resultado = resultado.replace(settings.number_separator, settings.decimal_separator);

                    resultado = currency + ' ' + resultado;
                    return resultado;
                }

            };

        }(jQuery));
    }
    //#endregion

    var f = {

        //#region Funciones Generales

        setContext: function (cont) {
            ctx = cont;
        },

        iterar: function (campos, operacionCallback) {
            if (campos) {
                for (var i = 0; i < campos.length; i++) {
                    operacionCallback(campos[i]);
                }
            }
        },

        iterarObjeto: function (elemento, operacionCallback) {
            for (var propiedad in elemento) {
                if (elemento.hasOwnProperty(propiedad)) {
                    operacionCallback(propiedad);
                }
            }
        },
        aviso: function (mensaje, titulo) {
            f._mensaje("A", mensaje, titulo);
        },
        error: function (mensaje, titulo) {
            f._mensaje("E", mensaje, titulo);
        },
        
        _mensaje: function (tipo, mensaje, titulo) {
            try {
                if (titulo == undefined || titulo === null)
                    titulo = '';
                if (tipo === 'E')
                    ctx.idigitalesNotificaciones(titulo, mensaje, 'error');
                else
                    ctx.idigitalesNotificaciones(titulo, mensaje, 'success');
                //var clase = "idigitales-notificacion mensaje";
                //if (tipo === "E") {
                //    clase = "idigitales-notificacion error";
                //}
                //ctx.find(".idigitales-notificacion").remove();
                //ctx.append("<div class='" + clase + "' style='display:none'>" + mensaje + "</div>");
                //ctx.find(".idigitales-notificacion").stop().fadeIn(500).delay(1500).fadeOut(1000);
            } catch (e) {
                console.log(e);
            }

        },

        cargarCamposRequeridos: function (vistas, modulo, servicio, callback) {
            try {
                f.ajaxAccion(f.obtenerRootApp("/Configuracion/ObtenerListaCamposRequeridos"), { claveVista: vistas, idModulo: modulo, servicio: servicio },
                    function (lista) {
                        f.agregarCamposRequeridos(lista);
                        if (typeof callback === "function") {
                            callback();
                        }
                    }, "get");
            } catch (e) {
                console.log(e);
            }
        },

        agregarCamposRequeridos: function (lista) {
            ctx.find("input,select").prop("required", false);
            //listaCamposRequeridos = lista;
            $.each(lista, function (index, campo) {
                if (campo.Tipo === "grupo") { //Conjunto de datos el nombre del campo debe de ser unna clase
                    ctx.find("." + campo.NombreCampo).each(function () {
                        f.campoRequerido($(this), campo.Requerido);
                    });
                } else if (campo.Tipo === "multi") { //Conjunto de campos en el cual se va repetir el nombre y el inicio es igual por ejemplo campo1 campo2

                    ctx.find("input[id^=" + campo.NombreCampo + "]").each(function () {
                        f.campoRequerido($(this), campo.Requerido);
                    });
                    ctx.find("textarea[id^=" + campo.NombreCampo + "]").each(function () {
                        f.campoRequerido($(this), campo.Requerido);
                    });
                    ctx.find("select[id^=" + campo.NombreCampo + "]").each(function () {
                        f.campoRequerido($(this), campo.Requerido);
                    });
                } else if (campo.Tipo === "date") { //Conjunto de campos en el cual se va repetir el nombre y el inicio es igual por ejemplo campo1 campo2
                    var campofecha = ctx.find("#" + campo.NombreCampo);
                    if (campofecha.is("div")) {
                        campofecha = campofecha.find("input");
                    }
                    f.campoRequerido($(campofecha), campo.Requerido);
                } else {
                    f.campoRequerido(ctx.find("#" + campo.NombreCampo), campo.Requerido);
                }
                // console.log(campo.NombreCampo);
            });
        },

        campoRequerido: function (campo, requerido) {
            if (requerido == undefined) requerido = true;
            campo.prop('required', requerido);//.data("msg", "Campo Requerido");
        },
        htmlSpin: '<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>',

      
        mostrarMensajeValidacion: function (idCampo, mensajesValidacion) {
            var ctrl = $("#" + idCampo);
            var campo = $("#" + idCampo)[0];
            var msgs = mensajesValidacion || this.mensajes;
            if (msgs.length > 0) {
                var msg = msgs.toString();

                //Evento para revalidar

                //if (campo.tagName === "INPUT") {
                //    ctrl.off("focusout", mostrarMensajeValidacion).on("focusout", mostrarMensajeValidacion);
                //    ctrl.siblings(".imgErrorValidacion").tooltip("destroy").remove();
                //} else if (campo.tagName === "SELECT") {
                //    ctrl.off("change", mostrarMensajeValidacion).on("change", mostrarMensajeValidacion);
                //    ctrl = ctrl.siblings(".select2").find(".select2-selection__rendered");
                //    ctrl.siblings(".imgErrorValidacion").tooltip("destroy").remove();
                //}


                //Tamaño para posicion
                var calcularPosicion = function () {
                    var arriba = 5;
                    var izquierda = -15;
                    var ancho = 0;
                    var posicion = ctrl.position();

                    if (ctrl.is("select")) {
                        var sel = ctrl.siblings(".select2").find(".select2-selection__rendered");
                        if (sel)
                            ancho = sel.width();
                    } else
                        ancho = ctrl.width();


                    var left = posicion.left + ancho + izquierda;
                    var top = posicion.top + arriba;

                    return { left: left, top: top };
                };

                var pos = calcularPosicion(ctrl);

                //Agregar imagen
                $(' <i class="fa fa-exclamation-triangle imgErrorValidacion" aria-hidden="true"></i> ').css("left", pos.left).css("top", pos.top).insertAfter(ctrl);
                $(".imgErrorValidacion").data("title", msg).tooltip({ placement: "left", selector: false, trigger: "click hover focus" });

                return false;

            } else {
                if (campo.tagName === "INPUT") {
                    ctrl.siblings(".imgErrorValidacion").tooltip("destroy").remove();
                } else if (campo.tagName === "SELECT") {
                    ctrl = ctrl.siblings(".select2").find(".select2-selection__rendered");
                    ctrl.siblings(".imgErrorValidacion").tooltip("destroy").remove();
                }
                return true;
            }
        },

        //#endregion

        //#region Manipulacion de Datos

        setDatos: function (elemento, datos, change) {
            try {
                for (var propiedad in elemento) {
                    if (elemento.hasOwnProperty(propiedad) && datos.hasOwnProperty(propiedad)) {
                        var idCampo = undefined;
                        var idMulti = undefined;
                        //si hay una subpropiedad
                        if (typeof elemento[propiedad] === 'string' && elemento[propiedad].indexOf("-") > -1) {
                            var split = elemento[propiedad].split("-");
                            if (split.length === 2) {
                                idCampo = split[0]; //Asignar el select
                                idMulti = split[1]; //id a obtener de una [] de elementos
                            }
                        } else {
                            idCampo = elemento[propiedad]; //obtener id campo
                        }

                        var valor = datos[propiedad]; //obtener valor del campo

                        if (valor && (typeof idCampo === "object") && (idCampo !== null)) {
                            //Si el valor es un objeto
                            if (Array.isArray(valor)) {
                                if (valor.length > 0) {
                                    f.setDatos(idCampo, valor[0]);
                                    continue;
                                }
                            }
                            f.setDatos(idCampo, valor);
                            continue;
                        }
                        f.setVal(idCampo, valor, idMulti);
                    }
                }

            } catch (err) {
                console.log(err);
            }
            return elemento;
        },

        getDatos: function (elemento) {
            var datos = {};
            try {

                for (var propiedad in elemento) {
                    if (elemento.hasOwnProperty(propiedad)) {
                        var valor = undefined;
                        var idCampo = undefined;
                        //Se utiliza para en caso que lleva - 
                        //significa que con ese es el nombre del campo del objeto que se selecciono
                        //En obtener datos es el campo que se debe llenar
                        var idMulti = undefined;
                        //si hay una subpropiedad
                        if (typeof elemento[propiedad] === 'string' && elemento[propiedad].indexOf("-") > -1) {
                            var split = elemento[propiedad].split("-");
                            if (split.length === 2) {
                                idCampo = split[0]; //Asignar el select
                                idMulti = split[1]; //id a obtener de una [] de elementos
                            }
                        } else {
                            idCampo = elemento[propiedad]; //obtener id campo
                        }

                        if ((typeof idCampo === "object") && (idCampo !== null)) {
                            //Si el valor es un objeto
                            valor = f.getDatos(idCampo);
                            datos[propiedad] = valor;
                            continue;
                        }
                        datos[propiedad] = f.getVal(idCampo, idMulti);
                    }
                }
            } catch (err) {
                console.log(err);
            }
            return datos;
        },

        setDatosPorClass: function (elemento, datos, change) {
            try {
                for (var propiedad in elemento) {
                    if (elemento.hasOwnProperty(propiedad) && datos.hasOwnProperty(propiedad)) {
                        var idCampo = undefined;
                        var idMulti = undefined;
                        //si hay una subpropiedad
                        if (typeof elemento[propiedad] === 'string' && elemento[propiedad].indexOf("-") > -1) {
                            var split = elemento[propiedad].split("-");
                            if (split.length === 2) {
                                idCampo = split[0]; //Asignar el select
                                idMulti = split[1]; //id a obtener de una [] de elementos
                            }
                        } else {
                            idCampo = elemento[propiedad]; //obtener id campo
                        }

                        var valor = datos[propiedad]; //obtener valor del campo

                        if (valor && (typeof idCampo === "object") && (idCampo !== null)) {
                            //Si el valor es un objeto
                            if (Array.isArray(valor)) {
                                if (valor.length > 0) {
                                    f.setDatos(idCampo, valor[0]);
                                    continue;
                                }
                            }
                            f.setDatos(idCampo, valor);
                            continue;
                        }
                        f.setValPorClass(idCampo, valor, idMulti);
                    }
                }

            } catch (err) {
                console.log(err);
            }
            return elemento;
        },

        getDatosPorClass: function (elemento) {
            var datos = {};
            try {

                for (var propiedad in elemento) {
                    if (elemento.hasOwnProperty(propiedad)) {
                        var valor = undefined;
                        var idCampo = undefined;
                        //Se utiliza para en caso que lleva - 
                        //significa que con ese es el nombre del campo del objeto que se selecciono
                        //En obtener datos es el campo que se debe llenar
                        var idMulti = undefined;
                        //si hay una subpropiedad
                        if (typeof elemento[propiedad] === 'string' && elemento[propiedad].indexOf("-") > -1) {
                            var split = elemento[propiedad].split("-");
                            if (split.length === 2) {
                                idCampo = split[0]; //Asignar el select
                                idMulti = split[1]; //id a obtener de una [] de elementos
                            }
                        } else {
                            idCampo = elemento[propiedad]; //obtener id campo
                        }

                        if ((typeof idCampo === "object") && (idCampo !== null)) {
                            //Si el valor es un objeto
                            valor = f.getDatos(idCampo);
                            datos[propiedad] = valor;
                            continue;
                        }
                        datos[propiedad] = f.getValPorClass(idCampo, idMulti);
                    }
                }
            } catch (err) {
                console.log(err);
            }
            return datos;
        },
        //recupera un valor de un contro
        getVal: function (idCampo, idMulti) {
            var valor = "";
            //Buscar control
            var ctrl = ctx.find("#" + idCampo); //Buscar por id
            if (!ctrl) return valor;
            //Buscar tipo control y recuperar valor
            if (ctrl.data("selectList")) {
                valor = His.Controles.SelectList(idCampo).getDatos();
            } else if (ctrl.data("DateTimePicker") && ctrl.data("DateTimePicker").date()) {
                var formato = ctrl.data("DateTimePicker").format();
                valor = ctrl.data("DateTimePicker").date().format();
                if (formato != null && formato.trim() == "LT") {
                    //if (!moment(valor).isValid())
                    valor = ctrl.data("DateTimePicker").date().format(formato);
                }
            } else if (ctrl.attr("type") === "range" || ctrl.attr("type") === "password" || ctrl.attr("type") === "text" || ctrl.attr("type") === "number" || ctrl.attr("type") === "hidden" || ctrl.is("textarea") || ctrl.attr("type") === "email" ) {
                valor = ctrl.val();
                if (ctrl.data("currency")) {
                    valor = valor.replace(ctrl.data("currency"), "");
                }
                if (ctrl.data("separador")) {
                    valor = valor.replace(ctrl.data("separador"), "").trim();
                }
            } else if (ctrl.is("select")) {
                if (ctrl.data("multipleSelect")) {
                    var valor = ctrl.multipleSelect("getSelects");
                    var seleccionados = [];
                    f.iterar(valor,
                        function (seleccionado) {
                            if (idMulti) {
                                var objseleccionado = {};
                                objseleccionado[idMulti] = seleccionado;
                                seleccionados.push(objseleccionado);
                            }
                        });
                    valor = seleccionados;

                } else if (ctrl.data("select2")) {
                    valor = ctrl.select2("val");

                    //Es select Multi
                    if (ctrl.data("select2").options["options"].multiple) {
                        var seleccionados = [];
                        f.iterar(valor,
                            function (seleccionado) {
                                if (idMulti) {
                                    var objseleccionado = {};
                                    objseleccionado[idMulti] = seleccionado;
                                    seleccionados.push(objseleccionado);
                                } else {
                                    seleccionados.push(seleccionado);
                                }
                            });
                        valor = seleccionados;
                    }
                } else {
                    valor = ctrl.val();
                }
                //if (!ctrl.data("select2")) {
                //    valor = ctrl.val();
                //} else {
                //    valor = ctrl.select2("val");

                //    //Es select Multi
                //    if (ctrl.data("select2").options["options"].multiple) {
                //        var seleccionados = [];
                //        f.iterar(valor,
                //            function(seleccionado) {
                //                if (idMulti) {
                //                    var objseleccionado = {};
                //                    objseleccionado[idMulti] = seleccionado;
                //                    seleccionados.push(objseleccionado);
                //                }
                //            });
                //        valor = seleccionados;
                //    }
                //}
            } else if (ctrl.attr("type") === "checkbox" || ctrl.attr("type") === "radio") {
                valor = ctrl.prop("checked");
            }

            return valor;
        },

        setVal: function (idCampo, valor, idMulti) {
            if (valor != undefined && valor != null) {
                //Buscar control por id
                var ctrl = ctx.find("#" + idCampo);
                //Buscar tipo control y asignarle valor
                if (ctrl.data("selectList")) {
                    //controles[idCampo].setDatos(valor);
                    His.Controles.SelectList(idCampo).setDatos(valor);
                } else if (ctrl.data("DateTimePicker")) {
                    if (valor) {
                        var formato = ctrl.data("DateTimePicker").format();
                        if (formato != null && formato.trim() == "LT" && !moment(valor).isValid()) {
                            var hoy = new Date();
                            var anio = hoy.getFullYear().toString();
                            var mes = (hoy.getMonth() + 1) < 10 ? "0" + (hoy.getMonth() + 1).toString() : (hoy.getMonth() + 1).toString();
                            var dia = hoy.getDate() < 10 ? "0" + hoy.getDate().toString() : hoy.getDate().toString();
                            valor = anio + "-" + mes + "-" + dia + "T" + valor;
                        }
                        var fecha = moment(valor);
                        ctrl.data("DateTimePicker").date(fecha);
                        ctrl.find("input").val(fecha.format(formato));
                    } else {
                        ctrl.find("input").val(valor);
                    }
                } else if (ctrl.attr("type") === "range" || ctrl.attr("type") === "password" || ctrl.attr("type") === "text" || ctrl.attr("type") === "number" || ctrl.attr("type") === "hidden" || ctrl.is("textarea") || ctrl.attr("type") === "email" ) {
                    ctrl.val(valor);
                    if (ctrl.data("currency")) {
                        ctrl.change();
                    }
                } else if (ctrl.is("select")) {
                    //Es select de busqueda ajax
                    if (ctrl.data("catalogo") && ctrl.data("select2") && ctrl.data("select2").options["options"].ajax) {
                        var cat = ctrl.data("catalogo");

                        if (cat) {
                            var onGetDescripcion = function (ctrl) {
                                return function (data, textStatus, jqXHR) {

                                    var opcion = $("<option selected></option>").text(data.datos.Valor)
                                        .val(data.datos.Id).attr("title", data.datos.Valor);

                                    if (data.datos.Elemento) opcion.data("Elemento", data.datos.Elemento);
                                    ctrl.append(opcion).trigger("change");
                                };
                            };
                            //verificar que no sea del catalogo dependiente si no buscar en catalogoDes
                            var cas = ctrl.data("ctrlCascada");
                            if (cat && cas) {
                                cat = ctrl.data("catalogoDes");
                            }
                            if (valor)
                                f.getDescripcion(cat, valor, onGetDescripcion(ctrl));
                        }
                    }
                    //Es select Multi
                    if ((ctrl.data("multipleSelect") || (ctrl.data("select2") && ctrl.data("select2").options["options"].multiple)) && $.isArray(valor)) {
                        var seleccionados = [];
                        f.iterar(valor,
                            function (algo) {
                                if (idMulti && algo.hasOwnProperty(idMulti)) {
                                    seleccionados.push(algo[idMulti]);
                                } else {
                                    seleccionados.push(algo);
                                }
                            }
                        );
                        valor = seleccionados;
                    }
                    if (typeof (valor) === "boolean") valor = valor.toString();
                    ctrl.data("valor", valor);
                    //por si no lo a cargado el combo toma el valor de aqui cuando cargue el catalogo
                    ctrl.val(valor).trigger("change"); //Si ya cargo el combo tomara el valor

                } else if (ctrl.attr('type') === "checkbox") {
                    if (valor === true || valor === "true")
                        ctrl.prop("checked", true).trigger('change');
                    else
                        ctrl.prop("checked", false).trigger('change');
                } else if (ctrl.attr('type') === "radio") {
                    if (valor === true || valor === "true")
                        ctrl.attr("checked", true);
                    else
                        ctrl.attr("checked", false);
                }
                else if (ctrl.is("label") || ctrl.is("span")) {
                    ctrl.text(valor);
                }
            } else if (typeof idCampo === 'string' || idCampo instanceof String) { //limpiar
                var c = ctx.find("#" + idCampo);
                if (c.is("select")) c.val("").trigger("change");
            }
        },

        getValPorClass: function (idCampo, idMulti) {
            var valor = "";
            //Buscar control
            var ctrl = ctx.find("." + idCampo).first(); //Buscar por id
            if (!ctrl) return valor;
            //Buscar tipo control y recuperar valor
            if (ctrl.data("selectList")) {
                valor = His.Controles.SelectList(idCampo).getDatos();
            } else if (ctrl.data("DateTimePicker") && ctrl.data("DateTimePicker").date()) {
                var formato = ctrl.data("DateTimePicker").format();
                valor = ctrl.data("DateTimePicker").date().format();
                if (formato != null && formato.trim() == "LT") {
                    //if (!moment(valor).isValid())
                    valor = ctrl.data("DateTimePicker").date().format(formato);
                }
            } else if (ctrl.attr("type") === "text" || ctrl.attr("type") === "number" || ctrl.attr("type") === "hidden" || ctrl.is("textarea")) {
                valor = ctrl.val();
                if (ctrl.data("currency")) {
                    valor = valor.replace(ctrl.data("currency"), "");
                }
                if (ctrl.data("separador")) {
                    valor = valor.replace(ctrl.data("separador"), "").trim();
                }
            } else if (ctrl.is("select")) {
                if (ctrl.data("multipleSelect")) {
                    var valor = ctrl.multipleSelect("getSelects");
                    var seleccionados = [];
                    f.iterar(valor,
                        function (seleccionado) {
                            if (idMulti) {
                                var objseleccionado = {};
                                objseleccionado[idMulti] = seleccionado;
                                seleccionados.push(objseleccionado);
                            }
                        });
                    valor = seleccionados;

                } else if (ctrl.data("select2")) {
                    valor = ctrl.select2("val");

                    //Es select Multi
                    if (ctrl.data("select2").options["options"].multiple) {
                        var seleccionados = [];
                        f.iterar(valor,
                            function (seleccionado) {
                                if (idMulti) {
                                    var objseleccionado = {};
                                    objseleccionado[idMulti] = seleccionado;
                                    seleccionados.push(objseleccionado);
                                }
                            });
                        valor = seleccionados;
                    }
                } else {
                    valor = ctrl.val();
                }
                //if (!ctrl.data("select2")) {
                //    valor = ctrl.val();
                //} else {
                //    valor = ctrl.select2("val");

                //    //Es select Multi
                //    if (ctrl.data("select2").options["options"].multiple) {
                //        var seleccionados = [];
                //        f.iterar(valor,
                //            function(seleccionado) {
                //                if (idMulti) {
                //                    var objseleccionado = {};
                //                    objseleccionado[idMulti] = seleccionado;
                //                    seleccionados.push(objseleccionado);
                //                }
                //            });
                //        valor = seleccionados;
                //    }
                //}
            } else if (ctrl.attr("type") === "checkbox" || ctrl.attr("type") === "radio") {
                valor = ctrl.prop("checked");
            }

            return valor;
        },

        setValPorClass: function (idCampo, valor, idMulti) {
            if (valor != undefined && valor != null) {
                //Buscar control por id
                var ctrl = ctx.find("." + idCampo).first();
                //Buscar tipo control y asignarle valor
                if (ctrl.data("selectList")) {
                    //controles[idCampo].setDatos(valor);
                    His.Controles.SelectList(idCampo).setDatos(valor);
                } else if (ctrl.data("DateTimePicker")) {
                    if (valor) {
                        var formato = ctrl.data("DateTimePicker").format();
                        if (formato != null && formato.trim() == "LT" && !moment(valor).isValid()) {
                            var hoy = new Date();
                            var anio = hoy.getFullYear().toString();
                            var mes = (hoy.getMonth() + 1) < 10 ? "0" + (hoy.getMonth() + 1).toString() : (hoy.getMonth() + 1).toString();
                            var dia = hoy.getDate() < 10 ? "0" + hoy.getDate().toString() : hoy.getDate().toString();
                            valor = anio + "-" + mes + "-" + dia + "T" + valor;
                        }
                        var fecha = moment(valor);
                        ctrl.data("DateTimePicker").date(fecha);
                        ctrl.find("input").val(fecha.format(formato));
                    } else {
                        ctrl.find("input").val(valor);
                    }
                } else if (ctrl.attr("type") === "text" ||
                    ctrl.attr("type") === "number" ||
                    ctrl.attr("type") === "hidden" ||
                    ctrl.is("textarea")) {
                    ctrl.val(valor);
                    if (ctrl.data("currency")) {
                        ctrl.change();
                    }
                } else if (ctrl.is("select")) {
                    //Es select de busqueda ajax
                    if (ctrl.data("catalogo") && ctrl.data("select2") && ctrl.data("select2").options["options"].ajax) {
                        var cat = ctrl.data("catalogo");
                        if (cat) {
                            var onGetDescripcion = function (ctrl) {
                                return function (data, textStatus, jqXHR) {

                                    var opcion = $("<option selected></option>").text(data.datos.Valor)
                                        .val(data.datos.Id).attr("title", data.datos.Valor);

                                    if (data.datos.Elemento) opcion.data("Elemento", data.datos.Elemento);
                                    ctrl.append(opcion).trigger("change");
                                };
                            };
                            if (valor)
                                f.getDescripcion(cat, valor, onGetDescripcion(ctrl));
                        }
                    }
                    //Es select Multi
                    if ((ctrl.data("multipleSelect") || ctrl.data("select2").options["options"].multiple) && $.isArray(valor)) {
                        var seleccionados = [];
                        f.iterar(valor,
                            function (algo) {
                                if (idMulti && algo.hasOwnProperty(idMulti)) {
                                    seleccionados.push(algo[idMulti]);
                                }
                            });
                        valor = seleccionados;
                    }
                    if (typeof (valor) === "boolean") valor = valor.toString();
                    ctrl.data("valor", valor);
                    //por si no lo a cargado el combo toma el valor de aqui cuando cargue el catalogo
                    ctrl.val(valor).trigger("change"); //Si ya cargo el combo tomara el valor

                } else if (ctrl.attr('type') === "checkbox") {
                    if (valor === true || valor === "true")
                        ctrl.prop("checked", true).trigger('change');
                    else
                        ctrl.prop("checked", false).trigger('change');
                } else if (ctrl.attr('type') === "radio") {
                    if (valor === true || valor === "true")
                        ctrl.attr("checked", true);
                    else
                        ctrl.attr("checked", false);
                }
                else if (ctrl.is("label") || ctrl.is("span")) {
                    ctrl.text(valor);
                }
            } else if (typeof idCampo === 'string' || idCampo instanceof String) { //limpiar
                var c = ctx.find("." + idCampo);
                if (c.is("select")) c.val("").trigger("change");
            }
        },

        getValCarrusel: function (classCampo, idMulti, itemCarrusel) {
            var valor = "";
            //Buscar control
            var ctrl = itemCarrusel.find("." + classCampo); //Buscar por id

            //Buscar tipo control y recuperar valor
            //if (ctrl.data("selectList")) {
            //    valor = His.Controles.SelectList(classCampo).getDatos();
            /*} else*/ if (ctrl.data("DateTimePicker") && ctrl.data("DateTimePicker").date()) {
                valor = ctrl.data("DateTimePicker").date().format();
            } else if (ctrl.attr("type") === "text" || ctrl.attr("type") === "number" || ctrl.attr("type") === "hidden" || ctrl.is("textarea")) {
                valor = ctrl.val();
            } else if (ctrl.is("select")) {
                valor = ctrl.select2("val");
                //Es select Multi
                if (ctrl.data("select2").options["options"].multiple) {
                    var seleccionados = [];
                    f.iterar(valor, function (seleccionado) {
                        if (idMulti) {
                            var objseleccionado = {};
                            objseleccionado[idMulti] = seleccionado;
                            seleccionados.push(objseleccionado);
                        }
                    });
                    valor = seleccionados;
                }

            } else if (ctrl.attr("type") === "checkbox") {
                valor = ctrl.prop("checked");
            }
            return valor;
        },

        getDatosCarrusel: function (elemento, carrusel) {
            var carruselDatos = [];

            try {
                $.each(carrusel.find('.idigitalesCarousel-multiItem'),
                    function (i, carruselItem) {
                        var datos = {};
                        for (var propiedad in elemento) {
                            if (elemento.hasOwnProperty(propiedad)) {
                                var valor = undefined;
                                var classCampo = undefined;
                                //Se utiliza para en caso que lleva - 
                                //significa que con ese es el nombre del campo del objeto que se selecciono
                                //En obtener datos es el campo que se debe llenar
                                var idMulti = undefined;
                                //si hay una subpropiedad
                                if (typeof elemento[propiedad] === 'string' && elemento[propiedad].indexOf("-") > -1
                                ) {
                                    var split = elemento[propiedad].split("-");
                                    if (split.length === 2) {
                                        classCampo = split[0]; //Asignar el select
                                        idMulti = split[1]; //id a obtener de una [] de elementos
                                    }
                                } else {
                                    classCampo = elemento[propiedad]; //obtener id campo
                                }

                                if (classCampo != null) {
                                    datos[propiedad] = f.getValCarrusel(classCampo, idMulti, $(carruselItem));
                                } else {
                                    datos[propiedad] = null;
                                }
                            }
                        }
                        carruselDatos.push(datos);
                    });
            } catch (err) {
                console.log(err);
            }
            return carruselDatos;
        },

        limpiarCampos: function (elemento) {
            try {
                for (var propiedad in elemento) {
                    if (elemento.hasOwnProperty(propiedad)) {
                        var idCampo = undefined;
                        //si hay una subpropiedad
                        if (typeof elemento[propiedad] === 'string' && elemento[propiedad].indexOf("-") > -1) {
                            var split = elemento[propiedad].split("-");
                            if (split.length === 2) {
                                idCampo = split[0]; //Asignar el select
                            }
                        } else {
                            idCampo = elemento[propiedad]; //obtener id campo
                        }

                        if ((typeof idCampo === "object") && (idCampo !== null)) {
                            //Si el valor es un objeto
                            f.setDatos(idCampo, "");
                            continue;
                        }
                        //Buscar control por id
                        var ctrl = ctx.find("#" + idCampo);
                        //Buscar tipo control y asignarle valor
                        if (ctrl.data("selectList")) {
                            //controles[idCampo].setDatos(valor);
                            His.Controles.SelectList(idCampo).setDatos("");
                        } else if (ctrl.data("DateTimePicker")) {
                            var fecha = new Date();
                            ctrl.data("DateTimePicker").date(null);
                            ctrl.find("input").val("");
                        } else if (ctrl.attr("type") === "text" || ctrl.attr("type") === "hidden" || ctrl.is("textarea")) {
                            ctrl.val("");
                        } else if (ctrl.is("select")) {
                            ctrl.data("valor", undefined); //por si no lo a cargado el combo toma el valor de aqui cuando cargue el catalogo
                            ctrl.val("").trigger("change"); //Si ya cargo el combo tomara el valor

                        } else if (ctrl.attr('type') === "checkbox") {
                            ctrl.prop("checked", false);
                        }
                    }
                }

            } catch (err) {
                console.log(err);
            }
            return elemento;
        },

        //#endregion Manipulacion de Datos

        //#region Cargar Catalogos

        getCatalogos: function (catalogos, catalogosParametros, callbackCatCargados) {
            var arrayCatalogo = [];
            window.CATALOGOS = window.CATALOGOS || {};

            //Si existe el catalog y no tiene parametros ya no PEDIRLO AL Servidor
            f.iterarObjeto(catalogos,
                function (propiedad) {
                    //Validar carga de catalogos //Estructura  Catalogo 
                    var catalogoCargado = window.CATALOGOS[propiedad];

                    if (!catalogoCargado || (catalogosParametros && catalogosParametros[propiedad])) {
                        var param = "";
                        if (catalogosParametros && catalogosParametros[propiedad]) {
                            param = catalogosParametros[propiedad].join("-");
                            if (param) param = "-" + param;
                        }
                        arrayCatalogo.push(propiedad + param);
                    }
                });

            //Si no se ha cargado un catalogo pedir al servidor 
            if (arrayCatalogo.length) {
                f.ajaxAccion(urlBase + "/Catalogos/ObtenerListasCatalogos",
                    { catalogos: arrayCatalogo.join(",") },
                    function (data) {
                        //guardar catalogos cargados
                        $.each(data.ListaCatalogos,
                            function (nombreCatalogo, infoCatalogo) {
                                window.CATALOGOS[nombreCatalogo] = infoCatalogo;
                            });

                        f.cargarCombos(catalogos, callbackCatCargados);
                    },
                    "get",
                    "json");

            } else {
                f.cargarCombos(catalogos, callbackCatCargados);
            }


        },
        cargarCombos: function (catalogos, callbackCatCargados) {
            //crear tareas para cargar catalogos SIMULAR ASYNCRONIA
            var tareas = [];
            f.iterarObjeto(catalogos,
                function (nombreCatalogo) {
                    //Obtener Datos y ids
                    var infoCatalogo = window.CATALOGOS[nombreCatalogo];
                    var ids = catalogos[nombreCatalogo];

                    //por cada id cargar datos
                    $.each(ids, function (key, idCtrl) {
                        tareas.push(function () {
                            setTimeout(function () {
                                f.llenarCombo(idCtrl, infoCatalogo, nombreCatalogo, callbackCatCargados);
                            }, 0);
                        });


                    });
                });

            //Ejecutartareas de carga
            var index = 0;
            while (index < tareas.length) {
                var tarea = tareas[index];
                tarea();
                // console.log(tarea);
                ++index;
            }
        },
        llenarCombo: function (idSelect, datosCatalogo, catalogo, callbackOnCatCargado) {

            var select = ctx.find("#" + idSelect);
            var valor = select.data("valor");
            var seleccionados = {};

            if (valor instanceof Array) { //es multi  
                valor.forEach(function (e) {
                    seleccionados[e] = true;
                });
            }

            if (select.is("select")) {
                select.data("datos", datosCatalogo);
                select.data("nombrecatalogo", catalogo);

                select.empty();
                if (datosCatalogo) {
                    $.each(datosCatalogo,
                        function (index, value) {
                            var option = $("<option codigo='" +
                                value.Codigo +
                                "' tipo='" +
                                value.Tipo +
                                "' value='" +
                                value.Id +
                                "'>" +
                                value.Valor +
                                "</option>");

                            if (seleccionados[value.Id]) {
                                option.prop("selected", true);
                            } else {
                                option.prop("selected", false);
                            }

                            if (value.Elemento) option.data("Elemento", value.Elemento);
                            select.append(option);
                        });
                    select.val(select.data("valor")).trigger("change");
                }

            }

            var data = select.data();

            if (data && data.multipleSelect) {
                select.multipleSelect("refresh");
            }

            if (callbackOnCatCargado && typeof callbackOnCatCargado === "function") {
                callbackOnCatCargado(idSelect, catalogo, datosCatalogo);
            }

        },
        llenarComboClass: function (classSelect, datosCatalogo, catalogo, callbackOnCatCargado) {

            var select = ctx.find("." + classSelect);
            var valor = select.data("valor");
            var seleccionados = {};

            if (valor instanceof Array) { //es multi  
                valor.forEach(function (e) {
                    seleccionados[e] = true;
                });
            }

            if (select.is("select")) {
                select.data("datos", datosCatalogo);
                select.data("nombrecatalogo", catalogo);

                select.empty();
                if (datosCatalogo) {
                    $.each(datosCatalogo,
                        function (index, value) {
                            var option = $("<option codigo='" +
                                value.Codigo +
                                "' tipo='" +
                                value.Tipo +
                                "' value='" +
                                value.Id +
                                "'>" +
                                value.Valor +
                                "</option>");

                            if (seleccionados[value.Id]) {
                                option.prop("selected", true);
                            } else {
                                option.prop("selected", false);
                            }

                            if (value.Elemento) option.data("Elemento", value.Elemento);
                            select.append(option);
                        });
                    select.val(select.data("valor")).trigger("change");
                }

            }

            var data = select.data();

            if (data && data.multipleSelect) {
                select.multipleSelect("refresh");
            }

            if (callbackOnCatCargado && typeof callbackOnCatCargado === "function") {
                callbackOnCatCargado(idSelect, catalogo, datosCatalogo);
            }

        },

        getCatalogosMulti: function (catalogos, servidor, catalogosParametros, callbackCatCargados) {
            var arrayCatalogo = [];
            f.iterarObjeto(catalogos, function (propiedad) {
                var param = "";
                if (catalogosParametros && catalogosParametros[propiedad]) {
                    param = catalogosParametros[propiedad].join("-");
                    if (param) param = "-" + param;
                }
                arrayCatalogo.push(propiedad + param);
            });

            f.ajaxAccion(f.obtenerRootApp("/Catalogos/ObtenerListasCatalogosDesdeServidor"), { catalogos: arrayCatalogo.join(","), servidor: servidor }, function (data) {
                //_hospitalizacion.ajax.Accion(LayoutManager.obtenerRootApp("/Catalogos/ObtenerListasCatalogos"), { catalogos: arrayCatalogo.join(",") }, function(data) {
                $.each(data.ListaCatalogos, function (nombreCatalogo, datosCatalogo) {
                    var ids = catalogos[nombreCatalogo];
                    $.each(ids, function (key, idSelect) {
                        var select = ctx.find("#" + idSelect);
                        if (select.is("select")) {
                            select.empty();
                            $.each(datosCatalogo, function (index, value) {
                                var option = $("<option codigo='" + value.Codigo + "' value='" + value.Id + "'>" + value.Valor + "</option>");
                                if (value.Elemento) option.data("Elemento", value.Elemento);
                                select.append(option);
                            });
                            select.val(select.data("valor")).trigger("change");
                        }

                        if (callbackCatCargados && typeof callbackCatCargados === "function") {
                            callbackCatCargados(idSelect, nombreCatalogo, datosCatalogo);
                        }


                    });
                });
            }, "get", "json");
        },
        getCatalogosMultiPatologia: function (catalogos, servidor, catalogosParametros, callbackCatCargados) {
            var arrayCatalogo = [];
            f.iterarObjeto(catalogos, function (propiedad) {
                var param = "";
                if (catalogosParametros && catalogosParametros[propiedad]) {
                    param = catalogosParametros[propiedad].join("-");
                    if (param) param = "-" + param;
                }
                arrayCatalogo.push(propiedad + param);
            });

            f.ajaxAccion(f.obtenerRootApp("/Catalogos/ObtenerListasCatalogosDesdeServidorPato"), { catalogos: arrayCatalogo.join(","), servidor: servidor }, function (data) {
                //_hospitalizacion.ajax.Accion(LayoutManager.obtenerRootApp("/Catalogos/ObtenerListasCatalogos"), { catalogos: arrayCatalogo.join(",") }, function(data) {
                $.each(data.ListaCatalogos, function (nombreCatalogo, datosCatalogo) {
                    var ids = catalogos[nombreCatalogo];
                    $.each(ids, function (key, idSelect) {
                        var select = ctx.find("#" + idSelect);
                        if (select.is("select")) {
                            select.empty();
                            $.each(datosCatalogo, function (index, value) {
                                var option = $("<option codigo='" + value.Codigo + "' value='" + value.Id + "'>" + value.Valor + "</option>");
                                if (value.Elemento) option.data("Elemento", value.Elemento);
                                select.append(option);
                            });
                            select.val(select.data("valor")).trigger("change");
                        }

                        if (callbackCatCargados && typeof callbackCatCargados === "function") {
                            callbackCatCargados(idSelect, nombreCatalogo, datosCatalogo);
                        }


                    });
                });
            }, "get", "json");
        },

        //Funciones para llenar combos que dependen de otro
        //#region Combos_dependientes
        getCatalogosDependientes: function (catalogos, callbackCatCargados) {

            f.iterarObjeto(catalogos, function (catalogo) {
                if (catalogos[catalogo]) {
                    var controles = catalogos[catalogo];
                    if (typeof controles === 'object') {
                        f.iterarObjeto(controles, function (idSelect) {
                            var select = ctx.find("#" + idSelect);
                            if (select.length > 0) {
                                var ctrlCascada = controles[idSelect];
                                //Modificaciones para varios dependientes del mismo
                                //select.data("catalogo", catalogo);
                                //select.data("ctrlCascada", ctrlCascada);
                                var cat = select.data("catalogo");
                                var cas = select.data("ctrlCascada");
                                if (!cat || !Array.isArray(cat)) {
                                    cat = [];
                                }
                                cat.push(catalogo);

                                if (!cas || !Array.isArray(cas)) {
                                    cas = [];
                                }
                                cas.push(ctrlCascada);

                                select.data("catalogo", cat);
                                select.data("ctrlCascada", cas);
                                select.data("callbackCatCargados", callbackCatCargados);
                                select.off("change", f._onSelectChange).on("change", f._onSelectChange);
                            }
                        });
                    }
                }
            });
        },
        _onSelectChange: function () {
            var select = $(this);
            var cats = select.data("catalogo");
            var ctrles = select.data("ctrlCascada");
            var callbackCatCargados = select.data("callbackCatCargados");

            var valor = select.val();
            if (valor) {
                var catalogosParametros = {};
                var catalogos = {};
                cats.forEach(function (catalogo, index) {
                    var ctrlCascada = ctrles[index];
                    catalogosParametros[catalogo] = [valor];
                    catalogos[catalogo] = [ctrlCascada];
                });
                f.getCatalogos(catalogos, catalogosParametros, callbackCatCargados);
            } else {
                //limpiar
                ctrles.forEach(function (ctrlCascada, index) {
                    ctx.find("#" + ctrlCascada).empty();
                });
            }
        },
        //Se agrego esto para el caso especial de tiempo ya que cuando se limpia el select padre se necesita que se haga la busqueda

        getCatalogosDependientesSinLimpiar: function (catalogos) {

            f.iterarObjeto(catalogos, function (catalogo) {
                if (catalogos[catalogo]) {
                    var controles = catalogos[catalogo];
                    if (typeof controles === 'object') {
                        f.iterarObjeto(controles, function (idSelect) {
                            var select = ctx.find("#" + idSelect);
                            if (select.length > 0) {
                                var ctrlCascada = controles[idSelect];
                                //Modificaciones para varios dependientes del mismo
                                //select.data("catalogo", catalogo);
                                //select.data("ctrlCascada", ctrlCascada);
                                var cat = select.data("catalogo");
                                var cas = select.data("ctrlCascada");
                                if (!cat || !Array.isArray(cat)) {
                                    cat = [];
                                }
                                cat.push(catalogo);

                                if (!cas || !Array.isArray(cas)) {
                                    cas = [];
                                }
                                cas.push(ctrlCascada);

                                select.data("catalogo", cat);
                                select.data("ctrlCascada", cas);
                                select.off("change", f._onSelectChangeSinLimpiar).on("change", f._onSelectChangeSinLimpiar);
                            }
                        });
                    }
                }
            });
        },
        _onSelectChangeSinLimpiar: function () {
            var select = $(this);
            var cats = select.data("catalogo");
            var ctrles = select.data("ctrlCascada");

            var valor = select.val();

            var catalogosParametros = {};
            var catalogos = {};
            cats.forEach(function (catalogo, index) {
                var ctrlCascada = ctrles[index];
                catalogosParametros[catalogo] = [valor];
                catalogos[catalogo] = [ctrlCascada];
            });
            f.getCatalogos(catalogos, catalogosParametros);

        },


        getCatalogosBusqueda: function (catalogos, callbackCatCargados) {
            f.iterarObjeto(catalogos, function (catalogo) {
                if (catalogos[catalogo]) {
                    var controles = catalogos[catalogo];
                    if (typeof controles === 'object') {
                        $.each(controles, function (i, idSelect) {
                            var select = ctx.find("#" + idSelect);
                            if (select.length > 0) {
                                var placeholder = select.data("placeholder");
                                select.data("catalogo", catalogo); //llenar descripcion
                                select.select2({
                                    ajax: {
                                        url: LayoutManager.obtenerRootApp("/Catalogos/BuscarEntidad"),
                                        dataType: 'json',
                                        delay: 200,
                                        data: function (params) { return { catalogo: catalogo, filtro: params.term, page: params.page }; },
                                        processResults: function (data, page) { return { results: data.Catalogo }; },
                                        cache: true
                                    },
                                    width: '60%',
                                    allowClear: true,
                                    placeholder: placeholder,
                                    escapeMarkup: function (markup) { return markup; },
                                    minimumInputLength: 3,
                                    templateResult: f.templateResult,
                                    templateSelection: f.templateSelection
                                });
                            }
                        });
                    }
                }

            });





        },
        //#endregion Combos_dependientes


        //#region SoloLectura
        soloLectura: function (idCampo, sololectura) {
            if (sololectura === undefined) sololectura = true;
            var ctrl = ctx.find("#" + idCampo); //Buscar por id

            //Buscar tipo control 
            if (ctrl.data("DateTimePicker")) {
                ctrl = ctrl.find("input");
                ctrl.prop("disabled", sololectura);
            }
            if (ctrl.is("select")) {
                ctrl.prop("disabled", sololectura);
            } else {
                ctrl.prop("readonly", sololectura);
                if (sololectura === true)
                    ctrl.addClass("readonly");
                else
                    ctrl.removeClass("readonly");
            }
        },

        soloLecturaElemento: function (sololectura) {
            if (sololectura === undefined) sololectura = true;
            var datos = {};
            try {
                if (sololectura) {
                    ctx.find('select').attr('disabled', 'disabled');
                    ctx.find('input').not('.idigitales-tab').attr('disabled', 'disabled');
                    ctx.find('input:text').addClass('readonly');
                    ctx.find('textarea').attr('disabled', 'disabled');
                    ctx.find('button').hide();
                } else {
                    ctx.find('select').attr('disabled', false);
                    ctx.find('input').not('.idigitales-tab').attr('disabled', false);
                    ctx.find('input:text').removeClass('readonly');
                    ctx.find('textarea').attr('disabled', false);
                    ctx.find('button').show();
                }

            } catch (err) {
                console.log(err);
            }
            return datos;
        },
        soloLecturaContenedor: function (contenedor, sololectura) {
            if (sololectura === undefined) sololectura = true;
            var datos = {};
            try {
                if (sololectura) {
                    contenedor.find('select').attr('disabled', 'disabled');
                    contenedor.find('input').not('.idigitales-tab').attr('disabled', 'disabled');
                    contenedor.find('input:text').addClass('readonly');
                    contenedor.find('textarea').attr('disabled', 'disabled');
                    contenedor.find('button').hide();
                } else {
                    contenedor.find('select').attr('disabled', false);
                    contenedor.find('input').not('.idigitales-tab').attr('disabled', false);
                    contenedor.find('input:text').removeClass('readonly');
                    contenedor.find('textarea').attr('disabled', false);
                    contenedor.find('button').show();
                }

            } catch (err) {
                console.log(err);
            }
            return datos;
        },

        requerido: function (idCampo, requerido) {
            if (requerido === undefined) requerido = true;
            var ctrl = ctx.find("#" + idCampo); //Buscar por id

            //Buscar tipo control 
            ctrl.prop("required", requerido);
        },
        //#endregion SoloLectura
        //para llenar campos apartir de la opcion seleccionada en un select
        //#region Valores desde Select
        cargarValoresDesdeSelect: function (mapeos) {
            f.iterarObjeto(mapeos, function (idSelect) {
                var mapeo = mapeos[idSelect];
                if (typeof mapeo === "object") {
                    var select = ctx.find("#" + idSelect);
                    select.off("change", f._onCargarValorDesdeSelect).on("change", f._onCargarValorDesdeSelect);
                    select.data("mapeo", mapeo);
                }
            });
        },

        _onCargarValorDesdeSelect: function () {
            try {
                var select = $(this);
                var valor = select.val();

                var mapeo = select.data("mapeo");
                if (typeof mapeo === "object") {
                    var opcion = select.find('option[value="' + valor + '"]');
                    if (opcion) {
                        //Si solo es un mapeo
                        var arrayMapeo = Object.entries(mapeo);
                        if (arrayMapeo.length === 1) {

                            var cod = opcion.attr("Codigo"); //buscar el codigo
                            if (cod && cod !== "null") {
                                ctx.find("#" + mapeo["Codigo"]).val(cod);
                            } else {

                                var datos = undefined;
                                if (opcion.data) {
                                    datos = opcion.data("Elemento"); //Buscar en data elemento- para select2
                                }
                                if (!datos && select.select2("data")[0]) {
                                    datos =
                                        select.select2("data")[0]
                                            .elemento; //Buscar en el valor seleccionado para ver si tiene objeto-para select de busqueda
                                }
                                if (datos)
                                    f.setDatos(mapeo, datos);
                                else
                                    f.limpiarCampos(mapeo);
                            }
                        } else if (arrayMapeo.length > 1) {



                            Object.keys(mapeo).forEach(function (key, index) {
                                // key: the name of the object key
                                // index: the ordinal position of the key within the object 
                                var elem = opcion.attr(key);
                                if (elem)
                                    ctx.find("#" + mapeo[key]).val(elem);
                                else
                                    f.limpiarCampos(mapeo[key]);
                            });




                        }
                    } else
                        f.limpiarCampos(mapeo);
                }
            } catch (e) {
                console.log(e);
            }
        },
        //#endregion Valores desde Select
        getDescripcion: function (cat, id, onGetDescription) {
            f.ajaxAccion(urlBase+"/Catalogos/ObtenerDescripcion", { catalogo: cat, id: id }, onGetDescription, "get", "json");
            //_hospitalizacion.ajax.Accion(LayoutManager.obtenerRootApp("/Catalogos/ObtenerDescripcion"), { catalogo: cat, id: id }, onGetDescription, "get", "json");
        },
        //#endregion Cargar Catalogos

        //#region Templatres-Select
        templateResultCount: function formatRepoCount(repo) {
            if (repo.loading) return repo.text;

            var markup = '<div class="clearfix">' +
                '<div class="SubSelect" style="font-size:11rem;">' + repo.codigo + '</div>' +
                '<div class="SubSelectCount">' + repo.d1 + '</div>' +
                '<div >' + repo.valor + '</div>' +
                '</div>';

            return markup;
        },
        templateResult: function formatRepoPacientes(repo) {
            if (repo.loading) return repo.text;

            var markup = '<div class="clearfix">' +
                '<div class="col-sm-11 SubSelect" style="font-size:11rem;">' + repo.codigo + '</div>' +
                '<div class="col-sm-11">' + repo.valor + '</div>' +
                '</div>';

            return markup;
        },
        templateSelection: function formatRepoSelection(repo) {
            return repo.valor || repo.text;
        },
        //#endregion Templatres-Select

        //#region Fechas
        calcularEdad: function (fn) {
            var result = "";
            try {

                var fa = new Date();
                var df = diferenciafechas(fa, fn);
                var anos = df.ans;
                var meses = df.meses;

                if (isNaN(anos))
                    anos = 0;
                if (isNaN(meses))
                    meses = 0;
                result = anos + " " + "Años" + ", " + meses + " " + "meses";

            } catch (e) {
                console.log(e);
            }
            return result;
        },
        calcularObjEdad: function (fn) {
            var result = {};
            try {

                var fa = new Date();
                var df = diferenciafechas(fa, fn);
                var anos = df.ans;
                var meses = df.meses;
                var dias = df.dias;

                if (isNaN(anos))
                    anos = 0;
                if (isNaN(meses))
                    meses = 0;
                if (isNaN(dias))
                    dias = 0;
                // result = anos + " " + "Años" + ", " + meses + " " + "meses";
                result.a = anos;
                result.m = meses;
                result.d = dias;
            } catch (e) {
                console.log(e);
            }
            return result;
        },
        //Diferencia de fechas
        diferenciafechas: function (fa, fb) { //fa y fb dos fechas

            var totdias = fa - fb;
            totdias /= 3600000;
            totdias /= 24;
            totdias = Math.floor(totdias);
            totdias = Math.abs(totdias);

            var ans, meses, dias, m2, m1, d3, d2, d1;
            var f2 = new Date();
            var f1 = new Date();

            if (fa > fb) {
                f2 = fa;
                f1 = fb;
            } else {
                var f2 = fb;
                f1 = fa;
            } //Siempre f2 > f1
            ans = f2.getFullYear() - f1.getFullYear(); // dif de años inicial
            m2 = f2.getMonth();
            m1 = f1.getMonth();
            meses = m2 - m1;
            if (meses < 0) {
                meses += 12;
                --ans;
            }

            d2 = f2.getDate();
            d1 = f1.getDate();
            dias = d2 - d1;

            var f3 = new Date(f2.getFullYear(), m2, 1);
            f3.setDate(f3.getDate() - 1);
            d3 = f3.getDate();

            if (d1 > d2) {
                dias += d3;
                --meses;
                if (meses < 0) {
                    meses += 12;
                    --ans;
                }
                if (fa > fb) { //corrección por febrero y meses de 30 días
                    f3 = new Date(f1.getFullYear(), m1 + 1, 1);
                    f3.setDate(f3.getDate() - 1);
                    d3 = f3.getDate();
                    if (d3 == 30) dias -= 1;
                    if (d3 == 29) dias -= 2;
                    if (d3 == 28) dias -= 3;
                }
            }

            return { ans: ans, meses: meses, dias: dias, Tdias: totdias };
        },
        //#endregion Fechas

        //#region Visualizacion de campos


        cargarConfiguracionVistas: function (claveVista, modulo, servicio, callback) {
            try {
                if (!servicio || servicio < 0) servicio = 0;
                f.ajaxAccion(f.obtenerRootApp("/Configuracion/ObtenerCamposRequeridosModuloServicio"), { claveVista: claveVista, idModulo: modulo, servicio: servicio },
                    function (lista) {
                        f.configurarVistas(lista);
                        //f.ajustarFieldset();
                        if (typeof callback === "function") {
                            callback();
                        }
                    }, "get");
            } catch (e) {
                console.log(e);
            }
        },

        configurarVistas: function (lista) {
            ctx.find("input,select").prop("required", false);
            //listaCamposRequeridos = lista;
            $.each(lista, function (index, campo) {
                if (campo.Tipo === "grupo") { //Conjunto de datos el nombre del campo debe de ser unna clase
                    ctx.find("." + campo.Nombre).each(function () {
                        f.configurarCampo($(this), campo.Visible, campo.Requerido);
                    });
                } else if (campo.Tipo === "multi") { //Conjunto de campos en el cual se va repetir el nombre y el inicio es igual por ejemplo campo1 campo2

                    ctx.find("input[id^=" + campo.Nombre + "]").each(function () {
                        f.configurarCampo($(this), campo.Visible, campo.Requerido);
                    });
                    ctx.find("textarea[id^=" + campo.Nombre + "]").each(function () {
                        f.configurarCampo($(this), campo.Visible, campo.Requerido);
                    });
                    ctx.find("select[id^=" + campo.Nombre + "]").each(function () {
                        f.configurarCampo($(this), campo.Visible, campo.Requerido);
                    });
                } else if (campo.Tipo === "date") { //Conjunto de campos en el cual se va repetir el nombre y el inicio es igual por ejemplo campo1 campo2
                    var campofecha = ctx.find("#" + campo.Nombre);
                    if (campofecha.is("div")) {
                        campofecha = campofecha.find("input");
                    }
                    f.configurarCampo($(campofecha), campo.Visible, campo.Requerido);
                } else if (campo.Tipo === "editor") {
                    f.configurarCampo(ctx.find("#" + campo.Nombre), campo.Visible, campo.Requerido);
                    if (ctx.find("#" + campo.Nombre).parents("div[class^='idigitales-col']").next().find(".barraLateral").length > 0) {
                        if (campo.Visible === false)
                            ctx.find("#" + campo.Nombre).parents("div[class^='idigitales-col']").next().find(".barraLateral").hide();
                        else
                            ctx.find("#" + campo.Nombre).parents("div[class^='idigitales-col']").next().find(".barraLateral").show();
                    }
                } else if (campo.Tipo === "div") {
                    var campoDiv = ctx.find("#" + campo.Nombre);
                    f.configurarCampoDiv($(campoDiv), campo.Visible, campo.Requerido);
                } else {
                    f.configurarCampo(ctx.find("#" + campo.Nombre), campo.Visible, campo.Requerido);
                }
                // console.log(campo.NombreCampo);
            });
        },

        configurarCampo: function (campo, visible, requerido) {

            if (requerido == undefined) requerido = true;
            if (visible == undefined) visible = false;

            //Si el campo esta oculto ignorar el requerido
            if (visible === false) {
                //campo.hide();
                campo.parents("div[class^='idigitales-col']").first().addClass("idigitales-col-oculta").hide();
            }
            else {
                campo.prop('required', requerido); //.data("msg", "Campo Requerido");
            }
        },
        configurarCampoDiv: function (campo, visible, requerido) {

            if (requerido === undefined) requerido = true;
            if (visible === undefined) visible = false;

            //Si el campo esta oculto ignorar el requerido
            if (visible === false) {
                //campo.hide();
                campo.addClass("idigitales-col-oculta").hide();
            }
            else {
                campo.find('.sol-contenedor').attr('required', requerido); //.data("msg", "Campo Requerido");
            }
        },

        ajustarFieldset: function (idModal) {
            try {

                var procesando = ctx.find("#procesando");
                procesando.show();

                var grupos = ctx.find("fieldset");

                grupos.each(function () {
                    var field = $(this);
                    field.show();
                    ajustarRenglones(field);
                });
                ctx.find("#pieBotones").show();
                procesando.hide();
                alertify[idModal]().elements.dialog.style.height = "auto";
            } catch (e) {
                console.log(e);
            }

        },
        ajustarRenglonesContenedor: function (idModal) {
            try {
                var contenedor = $("#" + idModal);
                ajustarRenglones(contenedor);
            } catch (e) {
                console.log(e);
            }
        },

        cargaPlantillasDefault: function (claveVista, modulo, servicio, callback) {
            try {
                if (!servicio || servicio < 0) servicio = 0;
                f.ajaxAccion(f.obtenerRootApp("/Configuracion/ObtenerCamposPlantillasModuloServicio"), { claveVista: claveVista, idModulo: modulo, servicio: servicio },
                    function (lista) {

                        if (typeof callback === "function") {
                            callback(lista);
                        }
                    }, "get");
            } catch (e) {
                console.log(e);
            }
        },

        //#endregion

        //#region Modales y Popup
        ajaxAccionModal: function (url, data, succes, type, dataType, antes, completo, error, extraData, dxHeaders) {
            if (!antes) antes = function () { $(".divmodalAccion").show(); $('#spinnerEsc').show(); }
            if (!completo) completo = function () { setTimeout(function () { $(".divmodalAccion").hide(); $('#spinnerEsc').hide(); }, 100); }
            if (!type) type = "Post";
            if (!dataType) dataType = "json";
            if (!error) error = this._onError;
            if (dxHeaders == undefined) dxHeaders = false;


            var params = {
                async: true,
                type: type,
                url: url,
                data: data,
                dataType: dataType,
                beforeSend: antes,
                complete: completo,
                success: succes,
                error: error,
                extra: extraData,
                xhr: function () {
                    var xhr = jQuery.ajaxSettings.xhr();
                    if (!dxHeaders) {//borrar headers
                        var setRequestHeader = xhr.setRequestHeader;
                        xhr.setRequestHeader = function (name, value) {//esto es para quitar unas cabeceras que agrega el devexpress
                            if (name && name.toLowerCase() == 'dxscript') return;//removes dxscript headers
                            if (name && name.toLowerCase() == 'dxcss') return;//removes dxscript headers
                            setRequestHeader.call(this, name, value);
                        }
                    }
                    return xhr;
                }
            };


            return $.ajax(params);
        },
        ajaxAccion: function (url, data, succes, type, dataType, antes, completo, error, extraData, dxHeaders) {
            if (!antes) antes = function (request) {
                var token = f.getToken(window.usuario);
                if (token)
                    request.setRequestHeader("Authorization", 'Bearer ' + token);
            }
            if (!completo) completo = function () { /*setTimeout(function () { $(".divmodal").hide(); }, 100);*/ }
            if (!type) type = "Post";
            if (!dataType) dataType = "json";
            if (!error) error = this._onError;
            if (dxHeaders == undefined) dxHeaders = false;


            var params = {
                async: true,
                type: type,
                url: url,
                data: data,
                dataType: dataType,
                beforeSend: antes,
                complete: completo,
                success: succes,
                error: error,
                extra: extraData,
                xhr: function () {
                    var xhr = jQuery.ajaxSettings.xhr();
                    if (!dxHeaders) {//borrar headers
                        var setRequestHeader = xhr.setRequestHeader;
                        xhr.setRequestHeader = function (name, value) {//esto es para quitar unas cabeceras que agrega el devexpress
                            if (name && name.toLowerCase() == 'dxscript') return;//removes dxscript headers
                            if (name && name.toLowerCase() == 'dxcss') return;//removes dxscript headers
                            setRequestHeader.call(this, name, value);
                        }
                    }
                    return xhr;
                }
            };


            return $.ajax(params);
        },

        Accion: function (url, data, success, type, dataType, antes, completo, error, extraData, dxHeaders) {

            if (typeof (url) == "object") {
                var obj = url;
                var url1 = obj.url,
                    data1 = obj.data,
                    success1 = obj.success,
                    type1 = obj.type,
                    dataType1 = obj.dataType,
                    antes1 = obj.antes,
                    completo1 = obj.completo,
                    error1 = obj.error,
                    extraData1 = obj.extra,
                    dxHeaders1 = obj.dxHeaders;

                return this._accion(url1, data1, success1, type1, dataType1, antes1, completo1, error1, extraData1, dxHeaders1);

            } else {
                return this._accion(url, data, success, type, dataType, antes, completo, error, extraData, dxHeaders);
            }
        },
        _accion: function (url, data, succes, type, dataType, antes, completo, onError, extraData, dxHeaders, contentType) {

            //if (!antes) antes = function () { LayoutManager.mostrarSpin(1); }
            //if (!completo) completo = function () { LayoutManager.mostrarSpin(-1); }
            if (!type) type = "Post";
            if (!dataType) dataType = "json";

            var params = {
                async: true,
                type: type,
                url: url,
                data: data,
                dataType: dataType,
                beforeSend: antes,
                complete: completo,
                success: succes,
                error: (onError instanceof Function) ? onError : this._onError,

                extra: extraData,
                xhr: function () {
                    var xhr = jQuery.ajaxSettings.xhr();
                    if (!dxHeaders) {//borrar headers
                        var setRequestHeader = xhr.setRequestHeader;
                        xhr.setRequestHeader = function (name, value) {//esto es para quitar unas cabeceras que agrega el devexpress
                            if (name && name.toLowerCase() === 'dxscript') return;//removes dxscript headers
                            if (name && name.toLowerCase() === 'dxcss') return;//removes dxscript headers
                            setRequestHeader.call(this, name, value);
                        }
                    }
                    return xhr;
                },
            };

            if (dataType == "default") {
                delete params.dataType;
            }

            if (contentType != undefined) {
                params.contentType = contentType;
            }


            return $.ajax(params);
        },
        abrirPopUpAjax: function (idDialog, selectorContenido, urlContenido, data, callbackOpen, dialogStyle, callbackSuccess, type, callbackClose, extraCommands) {
            var appendExtraCommands = function (container, extraCommands) {
                extraCommands.forEach(function (cmd) {
                    var newCommand = document.createElement('button');
                    var className = (cmd.className != undefined) ? cmd.className : "";

                    if (cmd.click instanceof Function)
                        newCommand.onclick = cmd.click;
                    if (cmd.id != undefined)
                        newCommand.id = cmd.id;

                    newCommand.className = "ajs-custom-cmd" + " " + className;
                    container.insertBefore(newCommand, container.firstChild);
                });
            };

            // $(selectorContenido).html($("#spinnerEsc").show().get(0).outerHTML);
            $(selectorContenido).html('<div class="centrado"><div style="font-size: 100rem;" class="fa fa-refresh animado idigitales-form-icon"></div></div>');
            alertify[idDialog] || alertify.dialog(idDialog, function () {
                return {
                    main: function (content) {
                        this.setContent(content);
                    },
                    setup: function () {
                        var maximizable = false;
                        var resizable = false;
                        if (dialogStyle) {
                            maximizable = dialogStyle.maximizable || false;
                            resizable = dialogStyle.resizable || false;
                        }

                        return {

                            options: {
                                ignore_dragging: ".idigitales-contenedo-boton-modal,.idigitales-boton-rectangular,i",
                                maximizable: maximizable,
                                resizable: resizable,
                                padding: false,
                                transition: "fade",
                                overflow: false,
                                modal: true,
                                closable: true,
                                closableByDimmer: false
                            }
                        };
                    },
                    prepare: function () {
                        this.elements.footer.style.visibility = "collapse";
                    },
                    build: function () {
                        this.elements.footer.style.minHeight = "0";
                        this.elements.footer.style.height = "0";
                        this.elements.content.style.bottom = "0";
                        this.elements.content.style.overflow = "visible";

                        //var newCommand = document.createElement("button");
                        //newCommand.className = "ajs-custom-cmd icon-imprimir-hoja label_hl";
                        //var cmdContainer =  this.elements.commands.container;
                        //cmdContainer.insertBefore(newCommand, cmdContainer.firstChild);

                        if (extraCommands) {
                            appendExtraCommands(this.elements.commands.container, extraCommands);
                        }

                        if (dialogStyle) {
                            if (dialogStyle instanceof Object) {
                                var dialog = this.elements.dialog.style;
                                for (var style in dialogStyle) {
                                    if (dialogStyle.hasOwnProperty(style)) {
                                        if (style === "_class")
                                            $(this.elements.dialog).addClass(dialogStyle[style]);
                                        else
                                            dialog[style] = dialogStyle[style];
                                    }
                                }
                            } else {
                                var minWidthrem = dialogStyle; //por default es el min del dialog
                                this.elements.dialog.style.minWidth = minWidthrem + "rem";
                            }

                        }
                    },
                    hooks: {
                        onshow: callbackOpen,
                        onclose: callbackClose,
                        onupdate: function () {
                        }
                    }
                };
            });

            var url = f.obtenerRootApp(urlContenido);
            // funcion($(selectorContenido).removeClass('hide')[0]).set('title', titulo);
            $.ajax({
                async: true,
                type: type == undefined ? "get" : type,
                url: url,
                data: data,
                dataType: "html",
                //beforeSend: antes,
                //complete: completo,
                success: function (data) {
                    $(selectorContenido).html(data);
                    var titulo = "Cargando....";
                    if ($(selectorContenido + " #titulo").length > 0)
                        titulo = $(selectorContenido + " #titulo").text();
                    else if (($(selectorContenido + " .titulo").length > 0)) //se lo puse para no repetir ids #titulo
                        titulo = $(selectorContenido + " .titulo").text();
                    alertify[idDialog]().set('title', titulo);
                    if (callbackSuccess && typeof callbackSuccess === "function") callbackSuccess();
                }
                //error: this._onError
            });
            alertify[idDialog]($(selectorContenido).removeClass("hide")[0]).set("title", "Cargando....");
            if (dialogStyle.height) {
                var apop = alertify[idDialog]();
                apop.elements.dialog.style.minHeight = dialogStyle.height;
                apop.elements.dialog.style.maxHeight = dialogStyle.height;
            }
        },


        abrirPopUp: function (idDialog, selectorContenido, titulo, callbackOpen, styleProperties) {
            alertify[idDialog] || alertify.dialog(idDialog, function () {
                return {
                    main: function (content) { this.setContent(content); },
                    setup: function () { return { options: { maximizable: false, resizable: false, padding: false, transition: "fade", overflow: false, modal: true, closable: true, closableByDimmer: false } }; },
                    prepare: function () { /* this.elements.footer.style.visibility = "collapse";*/
                    },
                    build: function () {
                        this.elements.footer.style.minHeight = "0";
                        this.elements.content.style.bottom = "0";
                        this.elements.content.style.overflow = "visible";
                        if (styleProperties) {
                            var style = this.elements.dialog.style;

                            for (var styleProp in styleProperties) {
                                if (styleProperties.hasOwnProperty(styleProp)) {
                                    style[styleProp] = styleProperties[styleProp];
                                }
                            }

                        }
                    },
                    hooks: {
                        onshow: callbackOpen,
                        onclose: function () {
                        },
                        onupdate: function () {
                        }
                    }
                };
            });
            // funcion($(selectorContenido).removeClass('hide')[0]).set('title', titulo);
            alertify[idDialog]($(selectorContenido).removeClass("hide")[0]).set("title", titulo);
        },
        abrirConfirmacion: function (titulo, mensaje, onOk, onCancel, lblAceptar, lblCancelar) {
            alertify.confirm(titulo, mensaje, onOk, onCancel);
            alertify.confirm(mensaje).set({
                'title': titulo,
                'labels': { ok: lblAceptar, cancel: lblCancelar },
                'onok': onOk,
                'oncancel': onCancel,
            });
        },


        //#region Cifrado
        encriptPhrase: function (phrase) {
            var response = "";

            try {
                var j = 0;
                for (var i = 0; i < phrase.length; i++) {
                    var newChar = phrase[i].charCodeAt(0) + _key[j].charCodeAt(0);
                    //newChar = newChar > 126 ? ((newChar - 126) + 32) : newChar;
                    response += String.fromCharCode(newChar);
                    j = j == (_key.length - 1) ? 0 : (j + 1);
                }
                response = Base64.encode(response);
            } catch (error) {

            }

            return response;
        },

        decriptPhrase: function (phrase) {
            var response = "";

            try {
                phrase = Base64.decode(phrase);

                var j = 0;
                for (var i = 0; i < phrase.length; i++) {
                    var newChar = phrase[i].charCodeAt(0) - _key[j].charCodeAt(0);
                    //newChar = newChar < 32 ? ((newChar + 126) - 32) : newChar;
                    response += String.fromCharCode(newChar);
                    j = j == (_key.length - 1) ? 0 : (j + 1);
                }
            } catch (error) {

            }

            return response;
        },

        saveToken: function (user, token) {
            try {
                user = user.toLowerCase();
                var stringUsuarios = localStorage.getItem('UsuariosToken');
                var usuarios = JSON.parse(stringUsuarios) || {};
                usuarios[user] = token;
                localStorage.setItem('UsuariosToken', JSON.stringify(usuarios));
            } catch (error) {
                console.log(error);
            }
        },

        getToken: function (user) {
            var token = "";
            try {
                if (user) {
                    user = user.toLowerCase();
                    var stringUsuarios = localStorage.getItem('UsuariosToken');
                    var usuarios = JSON.parse(stringUsuarios) || {};
                    token = usuarios[user];
                }
            } catch (error) {
                console.log(error);
            }

            return token;
        },
        //#endregion
        guidVisor: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + s4() + s4() + s4();
        },

        ajustarTexto: function (elem) {
            var fontstep = 5;
            if ($(elem).height() > $(elem).parent().height() || $(elem).width() > $(elem).parent().width()) {
                $(elem).css('font-size', (($(elem).css('font-size').substr(0, 2) - fontstep)) + 'rem')
                    .css('line-height', (($(elem).css('font-size').substr(0, 2))) + 'rem');
                f.ajustarTexto(elem);
            }
        }


        //#endregion  Metodos Privados

    };
    function iniMenuCtx() {

        (function ($) {

            $.fn.MenuCtx = function (options) {

                // Default options.
                var cfg = $.extend({
                    idMenuContextual: "menu-contextual",
                    opciones: [],
                    onOpcionSeleccionada: null,
                    mostrarMenu: null, //regresa true o false
                    ocultarOpciones: null, //lista de opciones a ocultar
                    selPadLeft: "#fondo",
                    selPadTop: ".fullWapperInner",
                    selTrigger: null,
                    triggerFiltro: null,
                    obtenerDatos: null
                }, options);

                var ctx = $(this);
                var menu = null;

                //el.change(function (event) {
                //    var val = $(this).val();
                //    $(this).val(formatNumber(val, settings.currency));
                //});


                //#region Menu contextual
                function crearMenuContextual() {
                    document.oncontextmenu = function () { return false; };

                    //crear menu contextual
                    if (cfg.opciones) {
                        //si no existe el contenedor se agrega
                        var cont = ctx.find("#" + cfg.idMenuContextual).length;
                        if (cont === 0) {
                            ctx.append('<div id="' + cfg.idMenuContextual + '" class="dropdown-menu"> </div>');

                            //se recuperan las opciones y se agregan
                            menu = ctx.find("#" + cfg.idMenuContextual);

                            if (!menu.data("iniciado")) {
                                cfg.opciones.forEach(function (item, index, arr) {
                                    menu.append("<a id='opcion" + item.id +"'><i data-idopcion=" + item.id +" class='" + item.clase + "'> </i>" + item.texto + "</span></a>");
                                });
                                menu.find("a").off("click", onClickMenuContextual).on("click", onClickMenuContextual);
                                menu.data("iniciado", true);
                            }
                        }
                    }
                    if (cfg.selTrigger && cfg.triggerFiltro) {

                        ctx.find(cfg.selTrigger).off('mouseup').on('mouseup', cfg.triggerFiltro, onMouseClickPresionado);
                    }
                    else if (cfg.selTrigger) {

                        ctx.find(cfg.selTrigger).off('mouseup').on('mouseup', onMouseClickPresionado);
                    }
                }
                function onClickMenuContextual(evt) {
                    //var menu = ctx.find("#" + cfg.dom.idMenuContextual);
                    var data = menu.data("elemento");
                    data.idopcion = $(this).find("i").data("idopcion");
                    ocultarMenucontextual(menu);
                    if (cfg.onOpcionSeleccionada)
                        cfg.onOpcionSeleccionada(data.idopcion, data);
                }

                function onMouseClickPresionado(evt) {
                    //1: izquierda, 2: medio/ruleta, 3: derecho
                    if (evt.which == 3) {
                        var data = {};
                        evt.stopPropagation();
                        evt.preventDefault();
                        if (cfg.obtenerDatos) {
                            data = cfg.obtenerDatos(this);
                        } else {
                            data = $(this).data();
                        }
                        //data.rowObj = tbls[id].tabla.row(this);
                        data.top = evt.clientY;
                        data.left = evt.clientX;
                        data.renglon = $(this);
                        //solo mostrar 
                        if (mostrarMenu(data)) {
                            mostrarMenuContextual(data);
                        }
                    }
                }

                function mostrarMenu(data) {
                    if (!cfg.mostrarMenu) return true;
                    return cfg.mostrarMenu(data);
                }
                function ocultarOpciones(data) {
                    var opciones = cfg.ocultarOpciones(data);
                    menu = ctx.find("#" + cfg.idMenuContextual);
                    menu.find("a").show();
                    if (opciones && opciones.length > 0) {
                        opciones.forEach(function (element, index, array) {
                            menu.find("#opcion" + element).hide();
                        });
                    }
                }

                function mostrarMenuContextual(data) {
                    ocultarOpciones(data);
                    var pl = obtenerPadding(cfg.selPadLeft, "padding-left");
                    var pt = 100;//obtenerPadding(cfg.selPadTop, "padding-top");
                    try {
                        //obtener el id evento
                        if (data) {
                            var menu = ctx.find("#" + cfg.idMenuContextual);
                            //verificar si no esta visible
                            if (!menu.data("visible")) {
                                menu.data("elemento", data);
                                menu.css({
                                    top: data.top - pt,
                                    left: data.left - pl,
                                    display: "block"
                                });
                                menu.data("visible", true);
                                menu.off('mouseleave').on('mouseleave', ocultarMenucontextual);
                            } else {
                                menu.hide();
                                menu.data("visible", false);
                                menu.data("elemento", null);
                            }
                        }
                    } catch (e) {
                        error(e);
                    }
                }

                function ocultarMenucontextual() {
                    try {
                        //var menu = ctx.find("#" + cfg.dom.idMenuContextual);
                        //verificar si no esta visible
                        if (menu.data("visible")) {
                            menu.hide();
                            menu.data("visible", false);
                            menu.data("elemento", null);
                        }

                    } catch (e) {
                        error(e);
                    }
                }

                function obtenerPadding(selector, cssPropiedad) {
                    if (!selector) return 0;
                    var pl = "0";
                    try {
                        pl = $(selector).css(cssPropiedad);
                        if (pl.indexOf("px") !== -1) {
                            pl = pl.replace("px", "");
                        } else if (pl.indexOf("rem") !== -1) {
                            pl = pl.replace("rem", "");
                        } else if (pl.indexOf("%") !== -1) {
                            pl = pl.replace("%", "");
                        }
                    } catch (e) {
                        error(e);
                    }
                    return parseInt(pl)+15;
                }


                function error(e) {
                    console.log(e);
                }

                //function opcionSeleccionada(data) {
                //    try {

                //        if (data.idopcion === "Conciliar") {
                //            His.Modulos.Finanzas.FINBancos.Conciliaciones.EdicionConciliar({ callbackRefrescar: refrescarLista }).conciliar(data.row.IdTransaccion);
                //        } else if (data.idopcion === "reporte") {

                //        } else if (data.idopcion === "ocultar") {

                //        } else if (data.idopcion === "mostrar") {

                //        }
                //        console.log(data);
                //    } catch (e) {
                //        error(e);
                //    }
                //}

                crearMenuContextual();
                //#endregion


            };

        }(jQuery));
    }

    iniMoneda();
    iniMenuCtx();
    return f;
}

function ObtenerLenguaje() {
    var lenguaje = $.cookie("_cultura").indexOf('us') > -1 ? "en-us" : "es-mx";
    return lenguaje;
}

 