﻿var His = His || {};
His.Manejadores = His.Manejadores || {};
His.Manejadores.Ajax = His.Manejadores.Ajax || {};

His.Manejadores.Ajax.AjaxManager = function () {
    var gestor = {
        AccionModal: function (url, data, succes, type, dataType, antes, completo, onError) {
            if (!antes) antes = function () {
                $(".divmodal").show();
            }
            if (!completo) completo = function () {

            }
            var funcionCompleto = function (data1, data2, data3) {
                if (succes) {
                    succes(data1, data2, data3);
                };
                setTimeout(function () { $(".divmodal").hide(); }, 100);
            }
            if (!onError)
                onError = this._onError;

            this.Accion(url, data, funcionCompleto, type, dataType, antes, completo, onError);
        },

        Accion: function (url, data, succes, type, dataType, antes, completo, onError) {
            //if (!antes) antes = function () { mostrarSpin(1); }
            //if (!completo) completo = function () { mostrarSpin(-1); }
            if (!type) type = "Post";
            if (!dataType) dataType = "json";

            var params = {
                async: true,
                type: type,
                url: url,
                data: data,
                dataType: dataType,
                beforeSend: antes,
                complete: completo,
                success: succes,
                error: this._onError
            };

            if (dataType == "default") {
                delete params.dataType;
            }
           
            $.ajax(params);
            
           
        },

        
        _onError: function(error, textStatus)
        {
            debugger;
            if (error.status === 401) {// Unauthorized;
                var login = LayoutManager.obtenerRootApp("account/logoff");
                window.location = login;
            }else if (error.responseText != null)
            {
                ManejadorErrores.settings.Excepcion = error.responseText;
                ManejadorErrores.settings.Descripcion = "El servidor no fué capaz de procesar la solicitud";
            }
            else
            {
                if(error.message != null)
                {
                    ManejadorErrores.settings.Excepcion = error.message;
                    ManejadorErrores.settings.Descripcion = error.message;
                }
            }
            debugger;
            console.log(error);
            if (error != null) {
                ManejadorErrores.settings.Titulo = "Error de comunicaciones";
                ManejadorErrores.settings.TipoError = "Comunicaciones";
                if (error.stack != null) {
                    ManejadorErrores.settings.Excepcion += "<p>" + error.stack.toString() + "</p>";
                }
                ManejadorErrores.settings.MostrarBotonReintentar = false;
                ManejadorErrores.settings.MostrarBotonDetalles = true;
                ManejadorErrores.settings.CallBackCancelar = function () {
                }
                ManejadorErrores.Mostrar();
            }
        }
    }
    return gestor;
}