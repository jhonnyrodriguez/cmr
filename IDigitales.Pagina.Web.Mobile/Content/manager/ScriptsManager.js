﻿
var His = His || {};
His.Manejadores = His.Manejadores || {};
His.Manejadores.Scripts = His.Manejadores.Scripts || {};



His.Manejadores.Utils = function () {
   var self = {
        agregarVersionSrc: function (src, version) {
            if (typeof version == "string") {
                src = src + "?v=" + version;
            }
            return src;
        },

        agregarVersionSrcArray: function (array, version) {
            var ar = array.map(function (each) {
                return self.agregarVersionSrc(each, version);
            });
            return ar;
        }
    };
   return self;
}

His.Manejadores.Scripts.ScriptsManager = function (scripts, callback, params) {

   
    var managers = {};
    var gestor = {
        callbackGestor: callback,

        crearAgregaManager: function (seccion) {
            seccion.jsArray.forEach(function (element, index, js) {

                if (typeof js[index] === "object") {
                    js[index].src = LayoutManager.obtenerRoot(element.src);
                } else {
                    js[index] = LayoutManager.obtenerRoot(element);
                }
                
            });
            var mang = His.Manejadores.Scripts.SeccionScriptsManager(seccion,this._onScriptsCargados, params);
            this.agregaManager(seccion.nombre, mang);
        },

        agregaManager: function(nombre, mng) {
            managers[nombre] = mng;
        },

        eliminarManager: function(nombre) {
            delete managers[nombre];
        },

        cargaScripts: function(nombre, params) {
            this._operacion(nombre, "cargaScripts", true, params);
        },

        removerScripts: function (nombre) {
            this._operacion(nombre, "removerScripts");
        },

        establecerCallback: function (fn) {
            if (fn && fn instanceof Function)
                gestor.callbackGestor = fn;
        },

        _init: function (secciones) {
            try {
                var selft = this;
                secciones.forEach(function (seccion, index) {
                    if (!(seccion instanceof His.Manejadores.Scripts.SeccionScripts))
                        throw "El parametro debe de ser de tipo " + Object.prototype.toString.call(seccion);
                    selft.crearAgregaManager(seccion);
                });
            } catch (err) {
                this._onError(err);
            }
        },
        _operacion: function (nombre, funcionSi, eliminar, params) {
            try {
                this._iterarManagers(function(manager) {
                    if (nombre === manager.getNombre() || manager.esModulo()) {
                        gestor._invocar(manager, funcionSi, params);
                    } else {
                        if (eliminar)
                            gestor._invocar(manager, "removerScripts");
                    }
                });
            } catch (err) {
                this._onError(err);
            }
        },
        _iterarManagers: function (operacionCallback){
            for (var key in managers) {
                if (managers.hasOwnProperty(key)) {
                    var m = managers[key];
                    operacionCallback(m);
                }
            }
        },
        _invocar: function (elemento, funcion, parametros) {
            try{
                elemento[funcion].apply(elemento, [parametros]);
            }
            catch (err) {
                this._onError(err);
            }
        },
        _onScriptsCargados: function (seccion, error) {           
            if (gestor.callbackGestor && typeof gestor.callbackGestor === "function")
                gestor.callbackGestor(seccion, error);
        },
        _onError: function(error) {
            console.log(error);
            if (gestor.callbackGestor && typeof gestor.callbackGestor === "function")
                gestor.callbackGestor("ScriptsManager", error);
        }
    };

    gestor._init(scripts);

    return gestor;

};

His.Manejadores.Scripts.SeccionScriptsManager = function (seccionScripts, callback, params ) {

   /**
    * Esta clase funcion se encarga de cargar Js y de descargarlos
    */

    var _version = (params && typeof params.version == "string") ? params.version : "";
    var _archivosCargados = [];   //archivos descargados

    var _utils = new His.Manejadores.Utils();
    var _dependenciasManager = new His.Manejadores.DependenciasManager();//objeto para resolver dependencias
    var _jsArray = []; //arreglo con los scripts por cargar
   
    var mngr = {
       

        //funcion que se ejecutara cuando se carguen los scripts
        _scriptsCargados: callback,

        esModulo: function () {
            return seccionScripts.esModulo;
        },
        getNombre: function () {
            return seccionScripts.nombre;
        },
        
        cargaScripts: function () {
            var self = this;
            
            try {
                if (_archivosCargados.length === _jsArray.length) {
                    self._onJsCargado();
                } else {

                  

                    _jsArray.forEach(function (js) {
                        if (_archivosCargados.indexOf(js.src) === -1)
                            self._cargarJs(js);
                        else
                            self._onJsCargado(js);
                    });
                }
            }
            catch (err) {
                this._onError(err);
            }
        },

        removerScripts: function () {
            try {
                var self = this;
                _jsArray.forEach(function (js, index, array) {
                    if (_archivosCargados.indexOf(js.src) !== -1)
                    self._removerJs(js);
                });
            }
            catch (err) {
                this._onError(err);
            }
        },

        setCallback: function (callback) {
            if(typeof this._scriptsCargados === "function")
            _scriptsCargados = callback;
        },
        //carga un js
        _cargarJs: function (js) {
            var self = this;
            try {
                var archivoRef = document.createElement("script");
                archivoRef.setAttribute("type", "text/javascript");
                archivoRef.setAttribute("src", js.src);
                archivoRef.onerror = function (error) { self._onError(js,error); };
                archivoRef.onload = function () { self._onJsCargado(js); }
                document.head.appendChild(archivoRef);
            } 
            catch (err) {
                this._onError(err);
            }
        },
        //eliminar un archivo js
        _removerJs: function (js) {
            try {
                var cssatributo = "src"; //obtener el atributo para iterar
                var csselementos = document.getElementsByTagName("script");
                for (var i = csselementos.length; i >= 0; i--) { //Buscar elementos para eliminar
                    if (csselementos[i] && csselementos[i].getAttribute(cssatributo) != null && csselementos[i].getAttribute(cssatributo).indexOf(js.src) !== -1) {
                        csselementos[i].parentNode.removeChild(csselementos[i]); //remover el elemento apartir del padre
                        i = _archivosCargados.indexOf(js.src);
                        if(i!==-1)_archivosCargados.splice(i, 1);
                    }
                }
            }
            catch (err) {
                this._onError(err);
            }
        },
        //Se ejecutara cada que se cargue un js
        _onJsCargado: function (js) {
            try {
                //Agregar archivo cargado
                if (js && _archivosCargados.indexOf(js.src) === -1)
                    _archivosCargados.push(js.src);

                //Si se cargaron todos notificar
                if (_archivosCargados.length === _jsArray.length)
                    if (this._scriptsCargados && typeof this._scriptsCargados === "function")
                        this._scriptsCargados(seccionScripts.nombre);
            }
            catch (err) {
                this._onError(err);
            }
        },       

        _ordenarPorDependencias: function (jsArray) {
            var res = _dependenciasManager.crearObjetosParaOrdenar(jsArray);

            var finalArray;
            if (res.esOrdenable === true)
                finalArray = _dependenciasManager.ordenarJsCss(res.nodos);
            else
                finalArray = res.nodos;

            
           
            return finalArray;
        },

        _onError: function(js,error) {
            console.log(error );
            if (mngr._scriptsCargados && typeof mngr._scriptsCargados === "function")
                mngr._scriptsCargados(seccionScripts.nombre, error);
        },

        _agregarVersion: function (jsArray) {
            if (_version.length > 0) {
                jsArray.forEach(function (each) {
                    each.src = _utils.agregarVersionSrc(each.src, _version);
                });
            }

            return jsArray;
        },

       _iniciar: function (arrayJs) {           
           try {
                                   
               _jsArray = this._ordenarPorDependencias(arrayJs);
               _jsArray = this._agregarVersion(_jsArray);

           } catch (e) {
               _jsArray = [];
               this._onError("", e);
           }
            
        }        
    }

    mngr._iniciar(seccionScripts.jsArray);

    return mngr;
};

His.Manejadores.Scripts.SeccionScripts = function (nombre, js, esModulo) {
    this.nombre = nombre;
    this.jsArray = js; 
    this.esModulo = esModulo;
}




