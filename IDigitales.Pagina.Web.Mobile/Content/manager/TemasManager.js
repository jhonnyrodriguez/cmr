﻿
var His = His || {};
His.Manejadores =His.Manejadores||{}
His.Manejadores.Temas = His.Manejadores.Temas || {}

His.Manejadores.Temas.TemasManager = function (temas, callback, params) {

    /**
   * Esta clase funcion se encarga de manejar los diferentes manejadores
   * es decir por cada seccion habra un manejador que se tendra que registrar.
   * Y esta clase se encargara de comunicarle a cada unno de los manejadores lo que debe de hacer
   */
    var managers = {};
    var gestor = {
        callbackGestor: callback,

        crearAgregaManager: function (seccion) {

            seccion.css.forEach(function (element, index, css) {
                css[index] = LayoutManager.obtenerRoot(element);
            });
            var mang = His.Manejadores.Temas.TemaManager(seccion,this._onTemaCambiado, params);
            this.agregaManager(seccion.nombre, mang);
        },

        agregaManager: function(nombre, mng) {
            managers[nombre] = mng;
        },

        eliminarManager: function(nombre) {
            delete managers[nombre];
        },


        habilitarSeccion: function(nombre) {
            this._operacion(nombre, "cargarTemaSession");
        },
        cargarTemaSession: function(nombre) {
            this._operacion(nombre, "cargarTemaSession");
        },
        cargarTemaLight: function(nombre) {
            this._operacion(nombre, "cargarTemaLight");
        },
        cargarTemaMedium: function(nombre) {
            this._operacion(nombre, "cargarTemaMedium");
        },
        cargarTemaDark: function(nombre) {
            this._operacion(nombre, "cargarTemaDark");
        },

        establecerCallback: function (fn) {
            if (fn && fn instanceof Function) 
                gestor.callbackGestor = fn;            
        },

        _init: function (secciones) {
            try {
                var selft = this;
                secciones.forEach(function(seccion, index) {
                    if (!(seccion instanceof His.Manejadores.Temas.SeccionTema))
                        throw "El parametro debe de ser de tipo " + Object.prototype.toString.call(seccionTema);
                    selft.crearAgregaManager(seccion);
                });
            } catch (err) {
                this._onError(err);
            }
        },
        _operacion: function (nombre, funcionSi, params) {
            try {
                //primero deshabilitar los css que no estan seleccionados
                this._iterarManagers(function (manager) {
                    var esManagerSeleccionado = nombre === manager.getNombre() || manager.esModulo();

                    if (!esManagerSeleccionado) {
                        gestor._invocar(manager, "desHabilitar");
                    }
                });

                //cargar los css nuevos al final
                this._iterarManagers(function (manager) {
                    var esManagerSeleccionado = nombre === manager.getNombre() || manager.esModulo();

                    if (esManagerSeleccionado) {
                        gestor._invocar(manager, funcionSi, params);
                    }
                });
                
                //this._iterarManagers(function(manager) {
                //    if (nombre === manager.getNombre() || manager.esModulo()) {
                //        gestor._invocar(manager, funcionSi, params);
                //    } else {
                //        gestor._invocar(manager, "desHabilitar");
                //    }
                //});
            } catch (err) {
                this._onError(err);
            }

        },
        _iterarManagers: function (operacionCallback){
            for (var key in managers) {
                if (managers.hasOwnProperty(key)) {
                    var m = managers[key];
                    operacionCallback(m);
                }
            }
        },
        _invocar: function (elemento, funcion, parametros) {
            try {
                elemento[funcion].apply(elemento, [parametros]);
            }
            catch (err) {
                this._onError(err);
            }
        },
        _onTemaCambiado: function (seccion, tema, error) {
           
            if (gestor.callbackGestor && typeof gestor.callbackGestor === "function" && tema !== "base") {
                gestor.callbackGestor(seccion, tema, error);
                console.log(seccion+" -- "+tema);
            }
        },
        _onError: function (error) {
            console.log(error);
            if (gestor.callbackGestor && typeof gestor.callbackGestor === "function")
                gestor.callbackGestor("TemasManager", "Todos", error);
        }
    };

    gestor._init(temas);

    return gestor;

};

His.Manejadores.Temas.TemaManager = function (seccionTema, callback, params) {

    this.seccionTema = seccionTema;
    var temaActual = "";//Contiene el tema actual
    var titCssGeneral = "base";//Contiene el titulo de los css que no pertenezcan a ningun tema

    var _version = (params && typeof params.version == "string") ? params.version : "";
    var _dependenciasManager = new His.Manejadores.DependenciasManager();//objeto para resolver dependencias
    var _utils = new His.Manejadores.Utils();
    /**
     * Esta clase funcion se encarga de manejar los css, los divide en 4 partes
     * 1.-base:Contiene los estilos generales o que no pertenecen a ninguntema
     * 2.-light:contiene los elementos de este tema
     * 3.-medium:contiene los elementos de este tema
     * 4.-dark:contiene los elementos de este tema   
     */
    var cssManager = function(cssTema) {
        function claseTema() {
            return {
                css: [],
                num: function() { return this.css.length; },
                cargado: false,
                cssCargados: [],
                numCssCargados:function() { return this.cssCargados.length; }
            };
        }

        var todosCss = cssTema;
        var temas = {
            base: claseTema(),
            light: claseTema(),
            medium: claseTema(),
            dark: claseTema()
        };
        temas.getCss = function (tema) {
            try {
                if (tema)
                    return this[tema].css;
                else
                    return todosCss;
            } catch (err) {
                console.log(err);
                return [];
            }
        }
        temas.temasCargados = function (tema) {
            try {
                return this.temaCargado(titCssGeneral) && this.temaCargado("light") && this.temaCargado("medium") && this.temaCargado("dark");
            }
            catch (err) {
                console.log(err);
                return false;
            }
        }

        temas.temaCargado = function (tema) {
            try{
                return this[tema].num() === this[tema].numCssCargados();
            }
            catch (err) {
                console.log(err);
                return false;
            }
        }
        temas.cssCargado = function (css) {
            try {
                if (this[titCssGeneral].cssCargados.indexOf(css) !== -1 || this["light"].cssCargados.indexOf(css) !== -1 || this["medium"].cssCargados.indexOf(css) !== -1 || this["dark"].cssCargados.indexOf(css) !== -1)
                    return true;
                return false;
            }
            catch (err) {
                console.log(err);
                return false;
            }
        }
        temas.agregaCss = function (css, tema) {
            try{
                this[tema].cssCargados.push(css);
            }
            catch (err) {
                console.log(err);
            }
        },
        temas.eliminaCss = function (css, tema) {
            try{
                var i;
                if (tema) {
                    i = this[tema].indexOf(css);
                    this[tema].splice(i, 1);
                } else {
                    var ltemas = [titCssGeneral, "light", "medium", "dark"];
                    for ( var j = 0; j < ltemas.length; j++) {
                         i = this[ltemas[j]].cssCargados.indexOf(css);
                        if (i !== -1) {
                            this[ltemas[j]].cssCargados.splice(i, 1);
                            return false;
                        }
                    }
                }
            }
            catch (err) {
                console.log(err);
            }
            return false;
        }
        temas._init = function () {
            this.base.css = this._obtenerCssPorTema(titCssGeneral);
            this.light.css = this._obtenerCssPorTema("light");
            this.medium.css = this._obtenerCssPorTema("medium");
            this.dark.css = this._obtenerCssPorTema("dark");
        };
        temas._obtenerCssPorTema = function (tema) {
            var csstema = [];
            try
            {
                todosCss.forEach(function (element, index, array) {
                    if ((element.indexOf("light") > -1 || element.indexOf("medium") > -1 || element.indexOf("dark") > -1) && element.indexOf(".css") > -1) {
                        if (element.indexOf(tema) !== -1)
                            csstema.push(element);
                    } else {//Tema base
                        if (tema === titCssGeneral)
                            csstema.push(element);
                    }
                });
            }
            catch (err) {
                console.log(err);
            }
            return csstema;
        },
        
        //Inicializar
        temas._init();
       
        return temas;
    }

    var _ordenarPorDependencias = function (cssArray) {
        var res = _dependenciasManager.crearObjetosParaOrdenar(cssArray);

        var finalArray;
        if (res.esOrdenable === true) {
            var nodosOrdenados = _dependenciasManager.ordenarJsCss(res.nodos);
            finalArray = [];
            nodosOrdenados.forEach(function (nodo) {
                finalArray.push(nodo.src);
            });
        }            
        else
            finalArray = cssArray;

        return finalArray;
    };

    var _iniciarCss = function (cssArray) {
      

        cssArray = _ordenarPorDependencias(cssArray);

        if (_version.length > 0) {
            cssArray = cssArray.map(function (each) {
                return _utils.agregarVersionSrc(each, _version);
            });
        }

        return cssArray;
    }
    
    /**
    * Esta clase funcion se encarga de cambiar los temas, utilizando las funciones de carga y descarga,
    *  habilitar y deshabilitar carga.    * 
    */

    
    var mngr = {

        _onTemasCargados: callback,
        //Clase para manejo de css
        _css: cssManager(_iniciarCss(seccionTema.css)),
        //Funciones utilizadas para cambiar los distintos temas
        cargarTemaSession: function(params) {
            var tema = this._obtenerTemaSession();
            this._cargarTema(tema, params);
        },
        cargarTemaLight: function(params) {
            this._cargarTema("light", params);
        },
        cargarTemaMedium: function(params) {
            this._cargarTema("medium", params);
        },
        cargarTemaDark: function(params) {
            this._cargarTema("dark",params);
        },
        
        habilitar:function() {
            this._init();
        },
        desHabilitar: function () {
            var csstodos = this._css.getCss();
            this._deshabilitarCss(csstodos);
        },
        //Limpiara todos los css agregados o los de algun tema (quitara los css de la pagina)
        eliminar: function (tema) {
            try
            {
                var self = this;
                var csss = this._css.getCss(tema);
                csss.forEach(function (element, index, array) {
                    self._removerCss(element, tema); //Eliminar de todos los temas
                });
            }
            catch (err) {
                this._onError(err);
            }
        },
        recargarCss: function () {
            try {
                this._colaCallbacksFinish = [];
                mngr._cargarCss(this._onCssCargado, this._onTodosCargados);
            } catch (err) {
                this._onError(err);
            }
        },
        esModulo: function () {
            return seccionTema.esModulo;
        },
        getNombre: function () {
            return seccionTema.nombre;
        },

       
        _colaCallbacksFinish:[],
        //Cargar todos los css y seleccionar el tema
        _init: function () {           
                        
            if (!(seccionTema instanceof His.Manejadores.Temas.SeccionTema))
                throw "El parametro debe de ser de tipo " + Object.prototype.toString.call(seccionTema);
            try {
                this._colaCallbacksFinish = [];
                mngr._cargarCss(this._onCssCargado, this._onTodosCargados);
                //mngr.cargarTemaSession();
            }
            catch (err) {
                this._onError(err);
            }
        },
        //Cargar los archivos segun el tema
        _cargarTema: function (tema) {
            try
            {
                var self = this;
                temaActual = tema;
                //self._init();
                self._deshabilitarTodosMenos(tema);
                self._temaCambiado(tema);
            }
            catch (err) {
                this._onError(err);
            }
        },
        //Clasifica los css segun el tema
        _cargarCss: function (callback, todosCargados) {
            var cargados = true;
            try{
                var self = this;
                var csstodos = this._css.getCss();
                var cssCargados = 0;
                

                var onCssCargado = function () {
                    cssCargados++;

                    if (cssCargados == csstodos.length) {
                        if (todosCargados instanceof Function)
                            todosCargados();
                    }
                }

                csstodos.forEach(function (element) {
                    var tema = element.indexOf("light") !== -1 ? "light" : (element.indexOf("medium") !== -1 ? "medium" : (element.indexOf("dark") !== -1 ? "dark" : titCssGeneral));
                    if (!self._cssCargado(element,tema)) {//Evita carga repetida de css
                        self._crearCss(element, tema, function (filename, tema) {
                            callback(filename, tema);
                            onCssCargado();
                        });
                         cargados = false;
                    } else {
                        onCssCargado();
                    }
                });
            }
            catch (err) {
                this._onError(err);
            }
            return cargados;
        },
        _cargarCssxTema: function (tema,callback) {
            var habilitar = true;
            try{
                var self = this;
                var csstodos = this._css.getCss(tema);
                csstodos.forEach(function (element) {
                    if (!self._cssCargado(element,tema)) {//Evita carga repetida de css
                        self._crearCss(element, tema, callback);
                        habilitar = false;
                    }
                });
            }
            catch (err) {
                this._onError(err);
            }
            return habilitar;
        },
        //Agrega los archivos css al documento, los del tema base estaran activos
        _crearCss: function (filename, tema, callback) {
            try {
             
                    var archivoRef = document.createElement("link");
                    archivoRef.setAttribute("rel", "stylesheet");
                    archivoRef.setAttribute("type", "text/css");
                    archivoRef.setAttribute("href", filename);
                    if (titCssGeneral!==tema) archivoRef.setAttribute("title", tema);

                    archivoRef.onload = archivoRef.onreadystatechange = function () {
                        if (archivoRef.readyState) {
                            if (archivoRef.readyState === 'complete' || archivoRef.readyState === 'loaded') {
                                archivoRef.onreadystatechange = null;
                                callback(filename, tema);
                            }
                        } else {
                            callback(filename, tema);
                        }
                    };

                    archivoRef.onerror = function(error) {
                        mngr._onError(error, filename);
                    };
                    document.head.appendChild(archivoRef);
                    //if (tema !== titCssGeneral) archivoRef.disabled = true;
            } catch (err) {
                this._onError(err);
            }
        },
        //Quitar los archivos css de la pagina 
        _removerCss: function (css, tema) {
            try {
                var cssatributo = "href"; //obtener el atributo para iterar
                var csselementos = document.getElementsByTagName("link");
                for (var i = csselementos.length; i >= 0; i--) { //Buscar elementos para eliminar
                    if (csselementos[i] && csselementos[i].getAttribute(cssatributo) != null && csselementos[i].getAttribute(cssatributo).indexOf(css) !== -1) {
                        csselementos[i].parentNode.removeChild(csselementos[i]); //remover el elemento apartir del padre
                        this._css.eliminaCss(css, tema);
                    }
                }
            }
            catch (err) {
                this._onError(err);
            }
        },
        //Revisr que no este cargado nien el dom ni en el dom ni en el controller
        _cssCargado: function (css, tema) {
            if (this._css.cssCargado(css))
                return true;
            else {//Si ya esta en el dom
                if (this._existeCssDom(css)) {
                    if (!mngr._css.cssCargado(css))
                        mngr._css.agregaCss(css, tema);
                    return true;
                }
            }
            return false;
        },
        _existeCssDom: function (css) {
            try {
                var cssatributo = "href"; //obtener el atributo para iterar
                var csselementos = document.getElementsByTagName("link");
                for (var i = csselementos.length; i >= 0; i--) { //Buscar elementos para eliminar
                    if (csselementos[i] && csselementos[i].getAttribute(cssatributo) != null && csselementos[i].getAttribute(cssatributo).indexOf(css) !== -1) {
                        return true;
                    }
                }
                return false;
            }
            catch (err) {
                this._onError(err);
            }
        },
        //Habilitar los css 
        _habilitarCss: function (cssArray) {
            this._activaDesactivaCss(cssArray, false);
        },
        //Deshabilitar los css 
        _deshabilitarTodos: function () {
            this._deshabilitarCss(this._css.getCss("light"));
            this._deshabilitarCss(this._css.getCss("medium"));
            this._deshabilitarCss(this._css.getCss("dark"));
        },

        _deshabilitarTodosMenos: function (tema) {
            this._deshabilitarTodos();
            this._habilitarCss(this._css.getCss(titCssGeneral));
            this._habilitarCss(this._css.getCss(tema));
            },
        _deshabilitarCss: function (cssArray) {
            this._activaDesactivaCss(cssArray,true);
        },
        //Activa o desactiva el css
        _activaDesactivaCss: function (cssArray, desactivar) {
            try {
                var css = cssArray.toString();
                var csselementos = document.getElementsByTagName("link");
                for (var i = csselementos.length; i >= 0; i--) { //Buscar elementos para desactivar
                    if (csselementos[i] && csselementos[i].rel.indexOf("stylesheet") !== -1) { //Si es hoja de estilo desactivar
                        var atributo = csselementos[i].getAttribute("href");
                        if (atributo && css.indexOf(atributo) !== -1) {
                            if (desactivar == true)
                                csselementos[i].disabled = desactivar; //habilitar el css 
                            else {
                                csselementos[i].disabled = desactivar;
                                csselementos[i].removeAttribute("disabled");
                            }
                                
                        }
                    }
                }
            }
            catch (err) {
                this._onError(err);
            }
        },
       //Recupera el tema que esta en session
        _obtenerTemaSession: function obtenerTemaSession() {
            var tema = "light";
            try { 
                var user = $.cookie("UserName");
                if (user != null) {
                    user = user.toLowerCase();
                    var cookieUsers = $.cookie("UserListCookie");
                    if (cookieUsers != null) {
                        var usuarios = cookieUsers.split("&");
                        for (var i = 0; i < usuarios.length; i++) {
                            var valores = usuarios[i].split("=");
                            if (valores[0].toLowerCase() == user) {
                                if (valores[1] != null && valores[1] != "")
                                    tema = valores[1];
                                else
                                    tema = "light";
                                continue;
                            }
                        }
                    }
                }
            }
            catch (err) {
                this._onError(err);
            }
            return tema;
        },
        //Se ejecutara cada que se cargue un css
        _onCssCargado: function (css, tema) {
            try {
                //console.log(css);
                if (css && !mngr._css.cssCargado(css))
                    mngr._css.agregaCss(css, tema);

                //mngr._temaCambiado(""); //descomentar si se va a realizar el init en cambio de tema
            }
            catch (err) {
                mngr._onError(err);
            }
        },

        _temaCambiado: function (tema,css) {
            try {

                var nombre = seccionTema.nombre;
                if (mngr._css.temasCargados()) {
                    if (mngr._onTemasCargados && typeof mngr._onTemasCargados === "function")
                        mngr._onTemasCargados(seccionTema.nombre, tema);
                } else {
                    var callback = mngr._onTemasCargados;

                    if (mngr._onTemasCargados && typeof mngr._onTemasCargados === "function") {
                        this._colaCallbacksFinish.push(function () {
                            callback(nombre, tema);
                        });
                    }
                    
                      
                }
            }
            catch (err) {
                mngr._onError(err);
            }
        },

        _onTodosCargados:function(){
            mngr._colaCallbacksFinish.forEach(function (callback) {
                callback();
            });

            mngr._colaCallbacksFinish = [];
        },

        _onError: function (error, filename) {
            console.log(error);
            if (mngr._onTemasCargados && typeof mngr._onTemasCargados === "function")
                mngr._onTemasCargados(seccionTema.nombre, temaActual, error);
        }
    }

    //Cargar todos los css y seleccionar el tema
    mngr._init();

    return mngr;
};

His.Manejadores.Temas.SeccionTema = function (nombre, css, esModulo) {
    this.nombre = nombre;
    this.css = css;
    this.esModulo = esModulo;

}
