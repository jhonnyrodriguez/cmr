﻿/// <reference path="LayoutModulo.js" />
var His = His || {};
His.Manejadores = His.Manejadores || {};
His.Manejadores.Secciones = His.Manejadores.Secciones || {};

His.Manejadores.Secciones.SeccionesManager = function (modulo, secciones, seccionDefault, options) {

    
    /**
    *
    *  Gestiona la carga de secciones dentro del módulo
    *
    **/

    //#region ". Variables "
 
    var _secciones = secciones;
    var _moduloActual = modulo;
    var _seccionActual = null;
    var _seccionACargar = null;
    var _seccionDefault = seccionDefault;
    var _manejadorTemas = null;
    var _manejadorScripts = null;
    var _callBackSeccionCargada = null;
    var _enProceso = false;
    var _manejadorAjax = null;
    var _timeOutProceso = 40000;             // Si el proceso de carga excede este tiempo se genera una exepcion
    var _handlerTimeOutProceso = null;      // Manejador del timer proceso
    var _contadorJSs = 0;
    var _timeOutPruebas = 0;                // Simula un retraso para poder probar 
    var _timeOutBloqueo = 0;                // Si el proceso tarda mas de nnn milisegundos se bloqueara la UI;
    var _handlerTimeOutBloqueo = null;      // Manejador timer bloqueo
    var _scriptsCargados = null;
    var _seccionSolicitada = null;
    var _controles ={tareas:"#cntTareasDia",
                    pacientes:"#pacientesSeleccionados",
                    camas:"#lista-camas",
                    temas: ".theme-config"
    };
    var _options = options || {};
    var _panelLoad = _options.panelLoad;

    
  
    //#endregion

    //#region *. Metodos privados "

    var _onError = function (error) {
        clearTimeout(_handlerTimeOutProceso);
        clearTimeout(_handlerTimeOutBloqueo);
        console.log(error);
        _enProceso = false;
       // LayoutManager.desbloquearUI();
        if (_callBackSeccionCargada && typeof _callBackSeccionCargada === "function")
            _callBackSeccionCargada(_seccionACargar, error);
    }
    var _validaSeccion = function (seccion) {
        try {
            _seccionACargar = seccion;
            if (_seccionACargar == "" || _seccionACargar == null) {
                _seccionACargar = localStorage.getItem(_moduloActual + "_Menu_Opcion");
                 
                if (_seccionACargar == "" || _seccionACargar == "undefined" || _seccionACargar == null) {
                    _seccionACargar = _seccionDefault;
                }
            }

        } catch (e) {
            console.log(e.message);
        }
    };
    var _cargaHTML = function () {
        try {
            if (_secciones[_seccionACargar].html != null) {
                _onHTMLCargado(_secciones[_seccionACargar].html);
            }
            else {
                var url = LayoutManager.obtenerRootApp(_secciones[_seccionACargar].url);
                _handlerTimeOutProceso = setTimeout(function () {
                    _onError({ message: "Error al cargar el HTML" });
                }, _timeOutProceso);
                setTimeout(function () {
                    _manejadorAjax.AccionModal(url, {}, _onHTMLCargado, 'get', 'html', null, null, _onError);
                }, _timeOutPruebas);
            }

           
        } catch (e) {
            _onError(e);
        }
    };
    var _onHTMLCargado = function (html) {
        try {
            console.log("HTL Cargado");
            clearTimeout(_handlerTimeOutProceso);
            _contadorJSs = 0;
            if (_enProceso) {
                _secciones[_seccionACargar].html = html;
                //$("#ContenedorSecciones").html(html); //cargar hasta finalizar la seccion previa
                console.log("--> Solicita carga Scripts");
                _handlerTimeOutProceso = setTimeout(function () {
                    _onError( { message: "Timeout error al cargar scripts" });
                }, _timeOutProceso);
               
               

                //_manejadorScripts.cargaScripts(_seccionACargar);
                
                _iniciarCargaScriptsTemas();

              
            }
        } catch (e) {
            _onError(e);
        }
    };
    var _onScriptsCargados = function (seccion, error) {
        try {
            console.log("Script Cargado: "+ seccion );
            _contadorJSs++;
            if (_enProceso) {
                if (error == null) {
                    if (_contadorJSs == 2) {//por qué 2?
                    //if (_contadorJSs == 1) {
                        clearTimeout(_handlerTimeOutProceso);
                        _handlerTimeOutProceso = setTimeout(function () {
                            _onError({ message: "Timeout error al cargar temas" });
                        }, _timeOutProceso);
                        console.log("--> Solicita carga de temas");

                     

                        //_manejadorTemas.cargarTemaSession(_seccionACargar);
                        if (_seccionACargar == "Configuracion") {
                            _manejadorTemas.cargarTemaDark(_seccionACargar);
                        }
                        else {
                            _manejadorTemas.cargarTemaLight(_seccionACargar);
                            //_manejadorTemas.cargarTemaSession(_seccionACargar);
                        }
                    }
                }
                else {
                    _onError(e);
                }
            }
        } catch (e) {
            _onError(e);
        }
    };
    var _onTemasCargados = function (seccion, tema, error) {
        try {
            console.log("Tema Cargado: " + seccion + " " +tema);
            if (seccion != "BaseModulo") {
                clearTimeout(_handlerTimeOutProceso);
                _contadorJSs = 0;
                if (_enProceso) {
                    if (error == null) {
                        //console.log("Tema Cargado: " + seccion);
                        _onTodoCargado();
                    }
                    else {
                        console.log("Error: " + error);
                        _onError(error);
                    }
                }
            }
        } catch (e) {
            _onError(e);
        }
    };

    var _iniciarCargaScriptsTemas = function () {
        try {                         
              var callbackbase = null;

          
                async.parallel({
                        scriptsBase: function(cal1) {
                            callbackbase = cal1;
                        },

                        scripts: function(cal2) {
                            _iniciarCargaScripts(callbackbase, cal2)
                        },

                        temas: function(finishtemas) {
                            _iniciarCargaTemas(finishtemas);
                        }
                    },
                    _finishCarga);
             


        } catch (e) {
            _onError(e);
        }
    }

    var _iniciarCargaScripts = function(finishBase, finishSeccion){
        _manejadorScripts.establecerCallback(function (seccion, error) {
            if (!_enProceso)
                return;

            if (typeof error == "undefined")
                error = null;

            _contadorJSs++;
           
            if (_enProceso && error == null && _contadorJSs == 2) {
                clearTimeout(_handlerTimeOutProceso);
                _handlerTimeOutProceso = setTimeout(function () {
                    _onError({ message: "Timeout error al cargar temas" });
                }, _timeOutProceso);
            }

            var seccionNombre = seccion.toLowerCase();

            if (_contadorJSs == 1) {//TODO tal vez sea mejor carga la seccion base, como cualquier otra
                finishBase(error, seccion);
            } else {
                finishSeccion(error, seccion);
            }
        });

        _manejadorScripts.cargaScripts(_seccionACargar);
    }

    var _iniciarCargaTemas = function (finish) {
        
        _manejadorTemas.establecerCallback(function (seccion, tema, error) {
            if (!_enProceso)
                return;

            if (typeof error == "undefined")
                error = null;

        
            if (seccion != "BaseModulo") {
                finish(error, seccion);
            }
            
        });

        _manejadorTemas.cargarTemaSession(_seccionACargar);       
    }

    var _finishCarga = function (error, result) {

      

        clearTimeout(_handlerTimeOutProceso);
        _contadorJSs = 0;
        
        async.parallel({});
        if (error) {
            _onError(error);
        }
        else {
            _onTodoCargado()
        }
    }

    var _onTodoCargado = function () {
        try {
            clearTimeout(_handlerTimeOutProceso);
            if (_enProceso) {
                console.log("Todo Cargado");
                if (_seccionActual != null) {


                    console.log("--> Solicita finalizacion seccion actual");
                    _handlerTimeOutProceso = setTimeout(function () {
                        _onError({ message: "Timeout error al finalizar la seccion anterior" });
                    }, _timeOutProceso);

                  
                    var manejadorSeccionActual = _secciones[_seccionActual].manejador;
                    if (manejadorSeccionActual.finalizar && typeof manejadorSeccionActual.finalizar === "function") {
                        manejadorSeccionActual.finalizar(_onSeccionFinalizada);
                    }
                    else {
                        console.log("Es necesario implementar el metodo finalizar en: " + _secciones[_seccionActual].codigo);
                        _onSeccionFinalizada();
                    }
                }
                else {
                    _onSeccionFinalizada();
                }
            }
        } catch (e) {
            _onError(e);
        }
    };
    var _onSeccionFinalizada = function (error) {
        try {
            clearTimeout(_handlerTimeOutProceso);
            if (_enProceso) {
                console.log("Seccion finalizada");

                $(_panelLoad).html(_secciones[_seccionACargar].html);

                var parametrosConstructor = _secciones[_seccionACargar].constructorParams;
                var _codigoSeccionACargar = eval("new " + _secciones[_seccionACargar].codigo + "(parametrosConstructor)");
                _secciones[_seccionACargar].manejador = _codigoSeccionACargar;
                //_codigoSeccionACargar.iniciar(_onSeccionIniciada);
                console.log("--> Solicita inicio seccion ");
                _handlerTimeOutProceso = setTimeout(function () {
                    _onError({ message: "Timeout error al iniciar la seccion" });
                }, _timeOutProceso);


               
                _secciones[_seccionACargar].manejador.iniciar(_onSeccionIniciada)
                //inicializar(_onSeccionIniciada);
            }

        } catch (e) {
            _onError(e);
        }
    }
    var _onSeccionIniciada= function(error,params)
    {
        try {
            params = params ? params : {};
            clearTimeout(_handlerTimeOutProceso);
            if (_enProceso) {
                console.log("Seccion Nueva Iniciada");
                if (_handlerTimeOutBloqueo != null) {
                    clearTimeout(_handlerTimeOutBloqueo);
                    _handlerTimeOutBloqueo = null;
                }
                LayoutManager.notificaWindowResize();
                _seccionActual = _seccionACargar;
                
                //ocultarControles === true (oculta todos)
                //ocultarControles = objeto con las "key" (de _controles) que se quieren ocultar
                _ocultarControles(params.ocultarControles);

                
                //LayoutManager.desbloquearUI();
                _enProceso = false;
                if (_callBackSeccionCargada && typeof _callBackSeccionCargada === "function")
                    _callBackSeccionCargada(_seccionActual, null);

                if (gestor.onFinalizaCarga instanceof Function)
                    gestor.onFinalizaCarga();
            }
        } catch (e) {
            _onError(e)
        }
    }

 
    var _ocultarControles = function (ocultar) {
         
        for (var control in _controles) {
            if (ocultar===true || (ocultar && ocultar.hasOwnProperty(control))) {
                $(_controles[control]).hide();
            } else {
                $(_controles[control]).show();
            }
        }
    }

    //#endregion

    //#region ". Constructor Metodos y propiedades publicas "

    var gestor = {
        onInicioCarga: null,
        onFinalizaCarga: null,
        cambiarTema: function (tema) {
            if (_seccionActual == null)
                return;

            if (tema == "light") 
                _manejadorTemas.cargarTemaLight(_seccionActual);
             else if (tema == "medium") 
                _manejadorTemas.cargarTemaMedium(_seccionActual);
             else if (tema == "dark") 
                _manejadorTemas.cargarTemaDark(_seccionActual);
            
        },
        cargaSeccion: function (seccion, onSeccionCargada) {
            try {
                _seccionSolicitada = seccion;
               // LayoutManager.desbloquearUI();
                if (_handlerTimeOutProceso != null) {
                    clearTimeout(_handlerTimeOutProceso);
                    _handlerTimeOutProceso = null;
                }

                _validaSeccion(seccion);
                _callBackSeccionCargada = onSeccionCargada;
                if (_seccionACargar != _seccionActual) {
                    _enProceso = true;

                    if (gestor.onInicioCarga instanceof Function)
                        gestor.onInicioCarga();

                    _handlerTimeOutBloqueo = setTimeout(function () {
                        if (_seccionSolicitada != null) {
                            //LayoutManager.bloquearUI();
                        }
                    }, _timeOutBloqueo);

                    console.log("--> Solicita carga HTML");
                    _cargaHTML();
                }
                
            } catch (e) {
                _onError(e);
            }
        },
        _inicializaScriptsManager: function () {
            try {
               
                var seccionesJs = [];
                for (var key in _secciones) {
                    if (_secciones.hasOwnProperty(key)) {
                        var scriptsBase = (key == "BaseModulo" ? true : false)
                        var seccionArchivo = new His.Manejadores.Scripts.SeccionScripts(key, _secciones[key].scripts, scriptsBase);
                        seccionesJs.push(seccionArchivo);
                    }
                }
                _manejadorScripts = His.Manejadores.Scripts.ScriptsManager(seccionesJs, _onScriptsCargados);

            } catch (e) {
                _onError(e);
            }
        },
        _inicializaTemasManager: function () {
            try {
                var seccionesCss = [];
                for (var key in _secciones) {
                    if (_secciones.hasOwnProperty(key)) {
                        var temasBase = (key == "BaseModulo" ? true : false)
                        var seccionTemaArchivo = new His.Manejadores.Temas.SeccionTema(key, _secciones[key].styles, temasBase);
                        seccionesCss.push(seccionTemaArchivo);
                    }
                }
                
                _manejadorTemas = His.Manejadores.Temas.TemasManager(seccionesCss, _onTemasCargados);
            } catch (e) {
                _onError(e);
            }
        },
        _inicializaAjaxManager: function () {
            try {
                _manejadorAjax = new His.Manejadores.Ajax.AjaxManager();
            } catch (e) {
                _onError(e);
            }
        }
    };
 //   LayoutManager.desbloquearUI();
    gestor._inicializaScriptsManager();
    gestor._inicializaTemasManager();
    gestor._inicializaAjaxManager();
    return gestor;

    //#endregion

}

