﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web.Script.Serialization;
using System.Web.Security;
using IDigitales.Pagina.Web.Controllers.Helpers;

namespace IDigitales.Pagina.Web.Mobile
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            MinifiedConfig.RegisterBundles(BundleTable.Bundles);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            //AuthenticateRequest +=OnAuthenticateRequest;
        }

        public override void Init()
        {
            base.Init();
            AuthenticateRequest += OnAuthenticateRequest;
        }

        private void OnAuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies["Usuario"];
            CustomPrincipalSerializeModel serializeModel = null;
            var usuario = "";
            if (authCookie != null)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                try
                {
                    serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authCookie.Value);
                }
                catch (Exception )
                {
                    usuario = authCookie.Value;
                }


                CustomPrincipal principal = null;
                if (serializeModel != null)
                {
                    var identity = new GenericIdentity(serializeModel.Nombre, "cookie");
                    principal = new CustomPrincipal(identity);
                    principal.Id = serializeModel.Id;
                    principal.Nombre = serializeModel.Nombre;
                    principal.Permisos = serializeModel.Permisos;
                    principal.Tipo = serializeModel.Tipo;
                }
                else
                {
                    var identity = new GenericIdentity(usuario, "cookie");
                    principal = new CustomPrincipal(identity);
                }

                HttpContext.Current.User = principal;
            }
        }

        
    }
}