﻿using System.Web;
using System.Web.Optimization;

namespace IDigitales.Pagina.Web.Mobile
{
    public class MinifiedConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;
            bundles.IgnoreList.Clear();

           
            var css = new StyleBundle("~/Content/css/sitio.css").Include(
                "~/Content/css/bootstrap.css",
                "~/Content/css/paris.css",
                "~/Content/masterslider/style/masterslider.css",
                "~/Content/masterslider/skins/default/style.css",
                "~/Content/masterslider/skins/designare/style.css",
                "~/Content/rs-plugin/css/designare-settings.css"
            );

            var cssSkin = new StyleBundle("~/Content/css/skin.css").Include(
                "~/Content/css/resize.css",

                "~/Content/css/jquery-ui-1.12.1.min.css",
                "~/Content/css/color-variations/paris.css",
                "~/Content/css/jquery.selectBox.css",
                "~/Content/css/animate.css",
                "~/Content/css/icons-font.css"
                //"~/Content/css/jquery-ui.structure.min.css",
                //"~/Content/css/jquery-ui.theme.min.css"
            );
            bundles.Add(cssSkin);
            bundles.Add(css);

            bundles.Add(new ScriptBundle("~/Content/js/jqueryScrips.js").Include(
                "~/Content/js/jquery-2.1.4.js"
            ));
            bundles.Add(new ScriptBundle("~/Content/js/jqueryScripsUi.js").Include(
                "~/Content/js/jquery-ui-1.12.1.min.js"
            ));

            bundles.Add(new ScriptBundle("~/Content/js/script.js").Include(
                "~/Content/rs-plugin/js/jquery.themepunch.tools.min.js",
                "~/Content/rs-plugin/js/jquery.themepunch.revolution.min.js",
                "~/Content/masterslider/masterslider.js",
                "~/Content/js/utils.js",
                "~/Content/js/paris.js",
                "~/Content/js/jquery.selectBox.js"
            ));

            bundles.Add(new ScriptBundle("~/Content/js/Otros.js").Include(
               // "~/Content/js/SocialNetworks.js",
                "~/Content/js/pdfobject.js"
            ));
            
          

        }

    }
}