﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IDigitales.Pagina.Web.Data;

namespace IDigitales.Pagina.Web.Pruebas
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnUsuariosClick(object sender, RoutedEventArgs e)
        {
            ConsultasUsuarios cu = null;
            try
            {
                cu = new ConsultasUsuarios();
                DataGrid.ItemsSource = cu.UsuariosRecupera(); ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ConsultasBase.DisposeIfNotNull(cu);
            }
        }

        private void btnProductosClick(object sender, RoutedEventArgs e)
        {
            ConsultasProductos cp = null;
            try
            {
                cp = new ConsultasProductos();
                DataGrid.ItemsSource = cp.ProductosRecupera();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ConsultasBase.DisposeIfNotNull(cp);
            }
        }


        private void btnRutasClick(object sender, RoutedEventArgs e)
        {
            ConsultasRutas cp = null;
            try
            {
                cp = new ConsultasRutas();
                DataGrid.ItemsSource = cp.RutasRecupera();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ConsultasBase.DisposeIfNotNull(cp);
            }
        }

    }
}
